const computeYearlyInterval = (data, rruleObj) => {
  if (rruleObj.freq !==0) {
    return data.repeat.yearly.interval;
  }

  return rruleObj.interval;
};

export default computeYearlyInterval;
