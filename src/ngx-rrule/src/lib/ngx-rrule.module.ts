import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxRruleComponent} from './ngx-rrule.component';
import {StartComponent} from './components/start/start.component';
import {NgbButtonsModule, NgbDateAdapter, NgbDateNativeAdapter} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EndComponent} from './components/end/end.component';
import {RepeatComponent} from './components/repeat/repeat.component';
import {WeeklyComponent} from './components/repeat/weekly/weekly.component';
import {MonthlyComponent} from './components/repeat/monthly/monthly.component';
import {YearlyComponent} from './components/repeat/yearly/yearly.component';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatTabsModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {CalendarModule, DateInputsModule, TimePickerModule} from '@progress/kendo-angular-dateinputs';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {BlockUIModule} from 'ng-block-ui';
import {GridModule} from '@progress/kendo-angular-grid';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {IntlModule} from '@progress/kendo-angular-intl';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {DailyComponent} from './components/repeat/daily/daily.component';
import {AppCalenderPopupModule} from '../../../app/layout/content-full-layout/common/calender-popup/app-calender-popup.module';
import {TimeFormatPipe} from '../../../app/pipe/time-format.pipe';
import {AppTimePipeModule} from '../../../app/pipe/module/app-time-pipe.module';
import {OnlyNumberModule} from '../../../app/directive/only-number/only-number.module';

@NgModule({
  declarations: [NgxRruleComponent, StartComponent, EndComponent, RepeatComponent, WeeklyComponent, MonthlyComponent, YearlyComponent, DailyComponent],
  imports: [
    CommonModule,
    AppCalenderPopupModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatFormFieldModule,
    RouterModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    TimePickerModule,
    MatRadioModule,
    InputsModule,
    MatDialogModule,
    BlockUIModule,
    MatDatepickerModule,
    GridModule,
    DropDownsModule,
    IntlModule,
    ButtonsModule,
    DateInputsModule,
    CalendarModule,
    MatPaginatorModule,
    MatExpansionModule,
    DialogModule,
    MatButtonToggleModule,
    MatTabsModule,
    NgbButtonsModule,
    AppTimePipeModule,
    OnlyNumberModule
  ],
  providers: [
    {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter},
  ],
  exports: [NgxRruleComponent]
})
export class NgxRruleModule {
}
