import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import {APP_DATE_FORMATS, AppDateAdapter} from 'src/app/shared/dt-format';

@Component({
  selector: 'ngx-end',
  templateUrl: './end.component.html',
  styleUrls: ['../../ngx-rrule.component.css'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => EndComponent), multi: true},
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class EndComponent implements OnInit, ControlValueAccessor {
  @ViewChild('endByDateInput', {static: false}) endByDateInput: ElementRef;
  @Input()
  frequency: string = 'Never';
  @Output() onChange = new EventEmitter();
  public form: FormGroup;
  private propagateChange;
  apperance = 'outline';
  minDate = new Date();
  maxDate: Date = new Date();

  constructor(private formBuilder: FormBuilder) {
    this.maxDate.setDate(365);
  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      after: 1,
      endAt: new Date(),
      mode: 'After'
    });

    setTimeout(() => {
      this.form.valueChanges.subscribe(() => {
        this.onFormChange();
      });
      this.onFormChange();
    }, 100);
  }

  writeValue = (input: any): void => {
    this.form.patchValue({
      ...input,
      mode: this.frequency != 'ROSD' ? input.mode : 'After',
      endAt: new Date(input.onDate.date)
    });
  };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onFormChange = () => {
    const endAt = this.form.value.endAt;
    const param = {
      ...this.form.value,
      onDate: {
        date: endAt
      }
    };
    this.propagateChange(param);
    this.onChange.emit();
  };

  radioChange = (event) => {
    if (event.target.value === 'on the') {
      this.form.patchValue({
        onDay: '',
      });
    } else {
      this.form.patchValue({
        onTheWhich: '',
        onTheDay: ''
      });
    }
    this.onFormChange();
  };

  public range = (start, end) => Array.from({length: (end - start)}, (v, k) => k + start);

  get getEndByInput() {
    return this.endByDateInput;
  }

  dateSelect($event: Date | number | string) {
    this.form.get('endAt').setValue(new Date($event));
  }

  get maxOccurence(): number {
    return this.frequency == 'Yearly' ? 5 : this.frequency == 'Monthly' ? 12 : 365;
  }
}
