import {Component, OnInit, Output, forwardRef, EventEmitter, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'ngx-yearly',
  templateUrl: './yearly.component.html',
  styleUrls: ['../../../ngx-rrule.component.css', '../monthly/monthly.component.css', 'yearly.component.css'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => YearlyComponent), multi: true}],
  encapsulation: ViewEncapsulation.None
})
export class YearlyComponent implements OnInit, ControlValueAccessor {
  @Output() onChange = new EventEmitter();
  public form: FormGroup;
  private propagateChange;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      interval: 0,
      mode: 'on',
      on: this.formBuilder.group({
        month: 'Jan',
        day: 1
      }),
      onThe: this.formBuilder.group({
        month: 'Jan',
        day: 'Monday',
        which: 'First'
      })
    });

    this.form.get('mode').valueChanges.subscribe(val => {
      this.radioChange(val);
    });

    this.form.valueChanges.subscribe(() => {
      this.onFormChange();
    });

    setTimeout(() => {
      this.onFormChange();
    }, 100);
  }

  writeValue = (input: any): void => {
    this.form.patchValue(input);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onFormChange = () => {
    if (this.propagateChange) {
      this.propagateChange(this.form.value);
    }
    this.onChange.emit();
  }

  radioChange = (value) => {
    if (value === 'on The') {
      this.form.get('on').get('day').disable();
      this.form.patchValue({
        onDay: '',
        onMonth: 'Jan',
        interval: 1
      });
    } else {
      this.form.get('on').get('day').enable();
      this.form.patchValue({
        onTheWhich: 'First',
        onTheDay: 'Monday',
        interval: 1
      });
    }
    this.onFormChange();
  };

  allowedMinMaxValue($event, min: number, max: number) {
    if ($event.which == 101){
      $event.preventDefault();
      return;
    }
    const value = parseInt($event.target.value+""+String.fromCharCode($event.which));
    if (value < min || value > max) {
      $event.preventDefault();
    }
  }

  disableMouseWheel($event: any) {
    $event.preventDefault();
  }

  public range = (start, end) => Array.from({length: (end - start)}, (v, k) => k + start);
}
