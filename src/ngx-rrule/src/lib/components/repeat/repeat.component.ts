import {Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'ngx-repeat',
  templateUrl: './repeat.component.html',
  styleUrls: ['../../ngx-rrule.component.css'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => RepeatComponent), multi: true}],
  encapsulation: ViewEncapsulation.None
})
export class RepeatComponent implements OnInit, ControlValueAccessor {
  @Output() onChange = new EventEmitter();
  @Input() frequency;

  @ViewChild('frequencySelect', {static: false}) frequencySelect;

  public form: FormGroup;
  private propagateChange;
  apperance = 'outline';

  constructor(private formBuilder: FormBuilder) {
  }

  value = 'Never';

  ngOnInit() {
    this.form = this.formBuilder.group({
      yearly: {},
      monthly: {},
      weekly: {},
      hourly: {},
      daily: {},
      interval: 1,
      frequency: 'Never'
    });

    this.form.valueChanges.subscribe(() => {
      this.onFormChange();
    });

    setTimeout(() => {
      this.onFormChange();
    }, 100);
  }

  onOptionChange() {
    this.form.patchValue({
      yearly: {
        interval: 1,
        mode: 'on',
        on: {
          month: 'Jan',
          day: '1'
        },
        onThe: {
          which: 'First',
          day: 'Monday',
          month: 'Jan'
        }
      },
      monthly: {
        interval: 1,
        mode: 'on',
        on: {
          day: 1
        },
        onThe: {
          which: 'First',
          day: 'Monday'
        }
      },
      weekly: {
        interval: 1,
        days: {
          mon: false,
          tue: false,
          wed: false,
          thu: false,
          fri: false,
          sat: false,
          sun: false,
        }
      },
      hourly: {
        interval: 1
      },
      daily: {
        type: 'daily',
        interval: 1
      }
    });
    // this.onFormChange();
  }

  writeValue = (input: any): void => {
    this.form.patchValue({
      ...input,
      interval: input.frequency.toLowerCase() != 'never' && input.frequency.toLowerCase() != 'rosd' ? input[input.frequency.toLowerCase()].interval : 1
    });
  };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onFormChange = () => {
    const params = {
      ...this.form.value
    };
    if (this.form.value.frequency.toLowerCase() != 'never' && this.getDailyData.type != 'workWeekDays') {
      if (this.form.value.frequency.toLowerCase() == 'daily') {
        params.interval = this.getDailyData.interval;
      } else if (this.form.value.frequency.toLowerCase() != 'monthly' &&
        this.form.value.frequency.toLowerCase() != 'rosd') {
        params[this.form.value.frequency.toLowerCase()].interval = this.form.value.interval;
      }
    } else if (this.form.value.frequency.toLowerCase() != 'never') {
      params['frequency'] = 'Weekly';
      params['weekly']['days'] = {
        mon: true,
        tue: true,
        wed: true,
        thu: true,
        fri: true,
        sat: false,
        sun: false,
      };
      params[params.frequency.toLowerCase()]['interval'] = this.getDailyData['interval'];
    }
    if (this.propagateChange) {
      this.propagateChange(params);
    }
    this.onChange.emit();
  };

  get getDailyData() {
    return this.form.get('daily').value;
  }

  restricateE($event: KeyboardEvent) {
    if ($event.which == 101) {
      $event.preventDefault();
    }
  }

  allowedMinMaxValue($event, min: number) {
    if ($event.which == 101){
      $event.preventDefault();
      return;
    }
    const value = parseInt($event.target.value+""+String.fromCharCode($event.which));
    if (value < min) {
      $event.preventDefault();
    }
  }
}
