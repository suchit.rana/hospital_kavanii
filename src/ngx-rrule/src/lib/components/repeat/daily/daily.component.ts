import {Component, EventEmitter, forwardRef, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatRadioChange} from '@angular/material';

@Component({
  selector: 'ngx-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.css', '../../../ngx-rrule.component.css'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DailyComponent), multi: true}],
  encapsulation: ViewEncapsulation.None
})
export class DailyComponent implements OnInit, ControlValueAccessor {
  @Output() onChange = new EventEmitter();
  public form: FormGroup;
  private propagateChange;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      type: 'daily',
      interval: 1
    });

    this.form.valueChanges.subscribe(() => {
      this.onFormChange();
    });

    setTimeout(() => {
      this.onFormChange();
    }, 100);
  }


  writeValue = (input: any): void => {
    this.form.patchValue({...input, interval: input.interval});
  };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onFormChange = () => {
    if (this.propagateChange) {
      this.propagateChange(this.form.value);
      this.onChange.emit();
    }
  };

  restricateE($event: KeyboardEvent) {
    if ($event.which == 101) {
      $event.preventDefault();
    }
  }

  changeType($event: MatRadioChange) {
    if (!$event.value) {
      this.form.get('interval').enable();
      return;
    }
    if ($event.value == 'daily') {
      this.form.get('interval').enable();
    } else {
      this.form.get('interval').disable();
    }
  }
}
