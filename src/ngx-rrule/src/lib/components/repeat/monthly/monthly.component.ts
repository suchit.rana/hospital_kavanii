import {Component, EventEmitter, forwardRef, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';


@Component({
  selector: 'ngx-monthly',
  templateUrl: './monthly.component.html',
  styleUrls: ['../../../ngx-rrule.component.css', 'monthly.component.css'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MonthlyComponent), multi: true}],
  encapsulation: ViewEncapsulation.None
})
export class MonthlyComponent implements OnInit, ControlValueAccessor {
  @Output() onChange = new EventEmitter();
  public form: FormGroup;
  private propagateChange;
  selectedType = 'on';
  onTheInterval = new FormControl({value: 1, disabled: true});
  isFormUpdating: boolean = false;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      interval: new FormControl(1, [Validators.min(1), Validators.max(12)]),
      mode: 'on',
      on: this.formBuilder.group({
        day: new FormControl('1', [Validators.min(1), Validators.max(31)])
      }),
      onThe: this.formBuilder.group({
        which: new FormControl({value: 'First', disabled: true}),
        day: new FormControl({value: 'Monday', disabled: true}),
      }),
    });

    this.form.get('mode').valueChanges.subscribe(val => {
      this.radioChange(val);
    });

    this.form.valueChanges.subscribe(val => {
      this.onFormChange();
    });

    this.onTheInterval.valueChanges.subscribe(value => {
      this.onFormChange();
    });


    setTimeout(() => {
      this.onFormChange();
    }, 100);
  }

  writeValue = (input: any): void => {
    this.form.patchValue(input);
  };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onFormChange = () => {
    let params = JSON.parse(JSON.stringify(this.form.value));

    if (params.mode === 'on The') {
      params['interval'] = this.onTheInterval.value;
    }

    if (this.propagateChange) {
      this.propagateChange(params);
    }
    this.onChange.emit();
  };

  radioChange = (value) => {
    if (value === 'on The') {
      this.form.get('on').get('day').disable();
      this.form.get('interval').disable();
      this.form.get('onThe').get('which').enable();
      this.form.get('onThe').get('day').enable();
      this.onTheInterval.enable();
      this.form.patchValue({
        onDay: '',
        interval: 1
      });
    } else {
      this.form.get('on').get('day').enable();
      this.form.get('interval').enable();

      this.form.get('onThe').get('which').disable();
      this.form.get('onThe').get('day').disable();
      this.onTheInterval.disable();
      this.form.patchValue({
        onTheWhich: '',
        onTheDay: '',
        interval: 1
      });
    }
    this.onFormChange();
  };

  allowedMinMaxValue($event, min: number, max: number) {
    if ($event.which == 101){
      $event.preventDefault();
      return;
    }
    const value = parseInt($event.target.value+""+String.fromCharCode($event.which));
    if (value < min || value > max) {
      $event.preventDefault();
    }
  }

  disableMouseWheel($event: any) {
    $event.preventDefault();
  }

  public range = (start, end) => Array.from({length: (end - start)}, (v, k) => k + start);

}
