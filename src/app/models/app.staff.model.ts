export class StaffModel {
  id: string;
  title: string;
  firstName: string;
  lastName: string;
  middleName: string;
  nickName: string;
  position: string;
  emailId: string;
  qualification: string;
  phone: string;
  mobile: string;
  address: string;
  country: string;
  state: string;
  city: string;
  photo: string;
  postCode: string;
  timeZone: string;
  dob: Date;
  gender: string;
  signature: string;
  emailSignature: string;
  biography: string;
  notes: string;
  isStatus: boolean;
  isTermOfServicee: boolean;
  isOwnerForAnyLocation: boolean;
  isTreatmentPrivateNote: boolean;
  isTreatmentSharedNote: boolean;
  isPractitionerCalendar: boolean;
  isDoubleBooking: boolean;
  isPatientOnlineBooking: boolean;
  isCancelledAppointment: boolean;
  isOnlineBooking: boolean;
  practitionerCalendarColor: string;
  userLocationRoles: UserLocationRoleModel[];
  userLocationRolesPractitoner: UserLocationRolesPractitonerModel[];
  userPractitonerAccess: PractitionerLocationModel[];
  isAccess: boolean;
  applicationUsersLocationModel: ApplicationUsersLocationModel[];
}

export class ApplicationUsersLocationModel {
  id: string;
  applicationUsersId: string;
  locationId: string;
  appointmentColour: string;
  includeInScheduler: boolean;
  treatmentNotesPrivacy: boolean;
  doubleBooking: boolean;
  onlineBooking: boolean;
  applicationUsersLocationRolesModel: ApplicationUsersLocationRolesModel[];
  applicationUsersPractApptAccessModel: ApplicationUsersPractApptAccessModel[];
}

export class ApplicationUsersLocationRolesModel {
  applicationUsersPractSpltyandOthsModel: ApplicationUsersPractSpltyandOthsModel[];
  id: string;
  applicationUsersLocationId: string;
  roleId: string;
}

export class ApplicationUsersPractSpltyandOthsModel {
  id: string;
  applicationUsersLocationRolesId: string;
  type: number;
  serviceTypeId: number;
  specialityId: string;
  providerNumber: string;
  otherIdName: string;
  otherID: string;
}

export class ApplicationUsersPractApptAccessModel {
  id: string;
  applicationUsersLocationId: string;
  practitonerId: string;
}

export class UserLocationRoleModel {
  locationId: string;
  roleId: string;
  locationName: string;
  roleName: string;
}

export class UserLocationRolesPractitonerModel {
  locationId: string;
  specialityId: string;
  roleId: string;
  specialityName: string;
  providerNumber: string;
  otherID: string;
}

export class PractitionerLocationModel {
  locationId: string;
  practitonerId: string;
  name: string;
}

export class PractitionerModel {
  id: string;
  firstName: string;
  lastName: string;
  locationId: string;
  specialityId: string;
  speciality: string;
  fullName: string;
  initials: string;
  photo: string;
  selected?: boolean;
  locations: string[];
  specilities: string[];
  isStatus: boolean;
}


export class PractitionerSpecialityModel {
  specialityId: string;
  specialityName: string;
  firstName: string;
  lastName: string;
  middleName: string;
  practitionerId: string;
  practitionerName: string;
  color: string;
  type: string;
  photo: string;
  id: string;
  initial: string;
  practitionerRoleSpecialityModel: PractitionerspecialitiesName[];
  status: boolean;
  nickName: string;

  // EXTRA KEY
  displayOrder: number;
  displayName: string;
  practitionerspecialitiesNamesString?: string;
  selected?: boolean;
  mouseOver: boolean;
  isPractioner: boolean = true;
  index: number;
  isShowOnlyThis: boolean = false;
  uuid: any;
}

export class PractitionerspecialitiesName {
  specialityId: string;
  specialityName: string;
}

export class GroupSpecialityPractitonerModel {
  specialityId: string;
  specialityName: string;
  practitioners: any[];
  expand: boolean;
}
