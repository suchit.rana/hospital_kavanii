export class TreatmentNotesModel {
  id: string;
  parentBusinessId: string;
  templateName: string;
  specialityPreview: string;
  isStatus: boolean;
  isLibrary: boolean;
  itemType: string;
  isAllowAllLocation: boolean;
  treatmentNotesTemplateLocation: TreatmentNotesLocationModel[] = [];
  treatmentNotesTemplateSpeciality: TreatmentNotesSpecialityModel[] = [];
  itemTypeModel: ItemTypeModel[] = [];
}
export class PatinetTreatmentNotesModel {
  id: string;
  parentBusinessId: string;
  treatmentName: string;
  specialityPreview: string;
  isStatus: boolean;
  isLibrary: boolean;
  itemType: string;
  casesId: string;
  treatmentNotesFor: string;
  isAllowAllLocation: boolean;
  appointmentId: string;
  treatmentNotesTemplateLocation: TreatmentNotesLocationModel[] = [];
  treatmentNotesTemplateSpeciality: TreatmentNotesSpecialityModel[] = [];
  treatmentData: ItemTypeModel[] = [];
}
export class ItemTypeModel {
  id: number;
  itemLabel: string;
  itemLabelDuplicate: string;
  itemTypeId: number;
  itemTypeText: string;
  setAsDefault: boolean;
  atatchfileData: string;
  setAsDuplicateDefault: boolean;
  selectedDropdown: any;
  selectedDuplicateDropdown: any;
  dropdowns: DropdownModel[] = [];
  chartTextbox: any[] = [];
  tabletext: any[][] = [[]];
  pointercount: number;
  duplicateDropdowns: DropdownModel[] = [];
  radioButtons: RadioButtonModel[] = [];
  duplicateRadioButtons: RadioButtonModel[] = [];
  checkBoxes: CheckBoxModel[] = [];
  checkBoxesDuplicate: CheckBoxModel[] = [];
  optionListStyleId: number;
  optionListSubjects: KeySubjectModel[] = [];
  optionListChoices: KeyValueModel[] = [];
  optionListStyleDuplicateId: any;
  optionListSubjectsDuplicate: KeySubjectModel[] = [];
  optionListChoicesDuplicate: KeyValueModel[] = [];
  number: NumberModel;
  numberDuplicate: NumberModel;
  rangeOrScale: NumberModel;
  rangeOrScaleDuplicate: NumberModel;
  dateTime: DateTimeModel;
  dateTimeDuplicate: DateTimeModel;
  table: TableModel;
  tableDuplicate: TableModel;
  text: string;
  textDuplicate: string;
  fileDescription: string;
  fileDuplicateDescription: string;
  fileImage: string;
  instruction: string;
  instructionDuplicate: string;
  chartImage: ChartModel;
  chartDuplicateImage: ChartModel;
  vitals: VitalsModel;
  chiefComplaint: string;
  spineImage: string;
  spineNotes: string;
  helpText: string;
  helpTextDuplicate: string;
  showMirrored: boolean;
  isMandatory: boolean;
  isOpen: boolean;
  isValid: boolean;
  error: string;
  includeNotes: boolean;
  os1: string;
  os2: string;
  os3: string;
  os4: string;
  os5: string;
  os6: string;
  os7: string;
  od1: string;
  od2: string;
  od3: string;
  od4: string;
  od5: string;
  od6: string;
  od7: string;
  OpticalNotes: string;
  leftData: spineModel[] = [];
  rightData: spineModel[] = [];
  inputnotes: string;
  inputnotesDuplicate: string;
}

export class DropdownModel {
  id: number;
  value: string;
  default: boolean;
}

export class RadioButtonModel {
  id: number;
  value: string;
  default: boolean;
}

export class CheckBoxModel {
  id: number;
  value: string;
  textValue: string;
  default: boolean;
}

export class KeyValueModel {
  selectedIndex: number;
  id: number;
  value: string;
  text: string;
  selected: boolean;
}

export class KeySubjectModel {
  id: number;
  value: string;
  optionListChoices: KeyValueModel[] = [];
  selectedModel: any = [];
  selectedValue: any;
  selectedIndex: number;
  optionListChoicesDuplicate: KeyValueModel[] = [];
  selectedModelDuplicate: any = []
}
export class spineModel {
  label: string;
  value: boolean;
}
export class TableModel {
  id: number;
  columns: number;
  rows: number;
  rowHeader: KeyValueModel[] = [];
  columnHeader: KeyValueModel[] = [];
}

export class DateTimeModel {
  date: any;
  time: any;
}

export class NumberModel {
  minimum: number;
  maximum: number;
  default: number;
  length: number;
}

export class ChartModel {
  constructor(public img: string) {
    this.image = img;
  }
  image: string;
  tempImage: string;
  imageSize: string;
}

export class VitalsModel {
  vitalType: string;
  measurements: any[] = [];
}

export class TreatmentNotesLocationModel {
  id: string;
  treatmentNotesId?: string;
  locationId: string;
  idAndLocationId: string;
  locationName: string;
  isStatus?: boolean;
}

export class TreatmentNotesSpecialityModel {
  id: string;
  treatmentNotesId?: string;
  locationId: string;
  specialityId: string;
  idAndLocationId: string;
  specialityName: string;
  isStatus?: boolean;
}

export class LocationModel {
  locationId: string;
  locationName: string;
}

export class SpecialityModel {
  locationId: string;
  specialityId: string;
  idAndLocationId: string;
  specialityName: string;
}

export class TreatmentNotesTemplateLibrarySpecialityModel {
  id: string;
  treatmentNotesTemplateLibraryId?: string;
  specialityId: string;
  specialityName: string;
}

export class TreatmentNotesTemplateLibraryModel {
  id: string;
  businessName: string;
  templateName: string;
  specialityName: any;
  itemType: string;
  userName: string;
  description: string;
  originalTreatmentNotesTemplateId: string;
  treatmentNotesTemplateLibrarySpeciality: TreatmentNotesTemplateLibrarySpecialityModel[];
  cssName: string;
  createdDate: Date;
  usedCount: number;
}

export class TotalTreatmentNotesTemplateLibraryModel {
  specialityId: string;
  specialityName: string;
  templateCount: string;
}

export class AddTreatmentNotesTemplateModel {
  locationId: string;
  templateName: string;
  specialityName: any;
  treatmentNotesTemplateLibraryId: string;
  treatmentNotesTemplateSpeciality: TreatmentNotesSpecialityModel[] = [];
}
