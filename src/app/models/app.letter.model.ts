export class PatientLetterModel {
  id: string;
  patientId: string;
  name: string;
  casesId: string;
  byAppointment: boolean;
  appointmentId: string;
  entryDatetime: string;
  description: string;
  subject: string;
  body: string;
  isActive: boolean;
  editStatus: number;
  businessTemplateId: string;
  createdBy: string;
  createdDate: string;
  modifiedBy: string;
  modifiedDate: string;
}