
export class CalenderConfigurationModel {
  timeZone: string;
  ianaTimeZone: string;
  startTime: string;
  endTime: string;
  timeSlot: number;
  timeSlotSize: string;
  calendarColor: string = "#ffffff";
  isDoubleBooking: boolean;
  isPatientOnlineBooking: boolean;
  isAllowBookingOutsideScheduleHours: boolean;
  isShowWaitRoom: boolean;
}
