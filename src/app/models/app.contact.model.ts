export class ContactModel {
  id: string;
  parentBusinessId: string;
  organisationName: string;
  departmentName: string;
  contactType: number;
  categoryId: number;
  titleId: number;
  name: string;
  firstName: string;
  lastName: string;
  providerNo: string;
  referenceNo: string;
  address: string;
  address2: string;
  country: string;
  state: string;
  city: string;
  postCode: string;
  workPhone: string;
  mobile: string;
  fax: string;
  emailId: string;
  website: string;
  notes: string;
  status: boolean;
  isAllowAllLocation: boolean;
  locationName: any;
  specialityName: any;
  contactLocation: ContactLocationModel[];
  contactSpeciality: ContactSpecialityModel[];
}

export class ContactLocationModel {
  contactId?: string;
  locationId: string;
  isStatus?: boolean;
}

export class ContactSpecialityModel {
  contactId?: string;
  locationId: string;
  specialityId: string;
  isStatus?: boolean;
}
