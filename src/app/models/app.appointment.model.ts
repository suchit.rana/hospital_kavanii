import {SchedulerEvent} from '@progress/kendo-angular-scheduler';
import {AppointmentType, Frequency, ScheduleMode} from '../enum/application-data-enum';
import {AppUtils} from '../shared/AppUtils';
import {
  AppointmentConfirmationStatus,
  AppointmentStatus
} from '../layout/content-full-layout/appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';

export interface Appointment {
  TaskID?: number;
  OwnerID?: number;
  Title?: string;
  Description?: string;
  Start?: Date;
  End?: Date;
  StartTimezone?: string;
  EndTimezone?: string;
  IsAllDay?: boolean;
  RecurrenceException?: any;
  RecurrenceID?: number;
  RecurrenceRule?: string;
}

export interface ScheduleEvent extends SchedulerEvent {
  specialityPractitionerID?: number;
  practitionerId?: string;
  note?: string;
  locationId: string;
  bookOnline: boolean;
}

export interface RoomEvent extends SchedulerEvent {
  roomId?: number;
  locationId: string;
}

export interface BreakEvent extends SchedulerEvent {
  breakId: string;
  breakName: string;
  notes: string;
  practitionerId: string;
  specialityPractitionerId: number;
  type: string;
}

export interface AppointmentEvent extends SchedulerEvent {

}

export interface WaitListEvent {
  id?: string;
  parentBusinessId?: string,
  locationId?: string,
  patientId?: string;
  patientName?: string;
  practitionerId?: string;
  practitionerName?: string;
  serviceId?: string;
  serviceName?: string;
  firstAvailableDate: Date;   // - 15/10/2020
  availableFrom: Date;        // - 9:00AM
  availableUntil: Date;       // - 5:00PM
  availableDays?: string;      // - This will store as Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
  keepInWaitListUntil: Date;  // - 15/11/2020
  isHighPriority: boolean;    // true/false
  notes?: string;
  waitListDays?: Date[];
  display?: boolean;
}

export class Schedule {
  id: string;
  title: string;
  practitionerId: string;
  startDateUTC: any;
  endDateUTC: any;
  recurrenceRule?: string;
  isAllDay: string;
  recurrenceException: string;
  bookOnline: boolean;
  note: string;
  locationId: string;
  // relateTo: string;
  frequencyType: Frequency;
  allDay: boolean;
  isHoliday: boolean;
  // EXTRA KEYS
  startDate: any;
  endDate: any;
  type: AppointmentType = AppointmentType.SCHEDULE;
  mode: ScheduleMode = ScheduleMode.NORMAL;
  _startDate: Date;
  _endDate: Date;

  constructor(value: Schedule) {
    this.id = value.id;
    this.title = value.title;
    this.startDate = value.startDateUTC;
    this.endDate = value.endDateUTC;
    this.practitionerId = value.practitionerId;
    this.recurrenceRule = value.recurrenceRule;
    this.isAllDay = value.isAllDay;
    this.recurrenceException = value.recurrenceException;
    this.bookOnline = AppUtils.getBoolean(value.bookOnline);
    this.note = value.note;
    this.locationId = value.locationId;
    this.frequencyType = value.frequencyType;
    this.allDay = value.allDay;
    this.isHoliday = value.type == AppointmentType.Holiday;
    this.type = value.type ? value.type : AppointmentType.SCHEDULE;
    this.mode = value.mode ? value.mode : ScheduleMode.NORMAL;
  }
}

export class Break extends Schedule {
  breakTypeId: number;

  constructor(value: Break) {
    super(value);
    this.breakTypeId = value.breakTypeId;
  }
}

export class Appointment extends Schedule {
  treatmentDetails: TreatmentDetails;
  appointmentType: string;
  treatmentRoomId: string;
  telehealth: boolean;
  description: string;
  patientId: string;
  caseId: string;
  referralId: string;
  payerId: string;
  appointmentsExtraDetails:AppointmentsExtraDetails;
  status: AppointmentStatus;
  confirmationStatus: AppointmentConfirmationStatus;
  currentStatus: string;
  appointmentsEntry: AppointmentEntry[];
  apptRecurringId: null;
  isNeedAttention: false;
  plannedService: null;
  timezone: string;
  practitionerName: string;

  constructor(value: Appointment) {
    super(value);
    this.treatmentDetails = value.treatmentDetails;
    this.locationId = value.locationId;
    this.appointmentType = value.appointmentType;
    this.treatmentRoomId = value.treatmentRoomId;
    this.telehealth = value.telehealth;
    this.title = value.title;
    this.description = value.description;
    this.note = value.note;
    this.isAllDay = value.isAllDay;
    this.startDateUTC = value.startDateUTC;
    this.endDateUTC = value.endDateUTC;
    this.patientId = value.patientId;
    this.caseId = value.caseId;
    this.referralId = value.referralId;
    this.payerId = value.payerId;
    this.appointmentsExtraDetails = value.appointmentsExtraDetails;
    this.confirmationStatus = value.confirmationStatus;
    this.currentStatus = value.currentStatus;
    this.status = value.status;
    this.appointmentsEntry = value.appointmentsEntry;
    this.apptRecurringId = value.apptRecurringId;
    this.isNeedAttention = value.isNeedAttention;
    this.plannedService = value.plannedService;
    this.practitionerName = value.practitionerName;
  }
}

export interface AppointmentEntry {
  id: string;
  practitionerId: string;
  startDateUTC: Date;
  endDateUTC: Date;
  confirmationStatus: AppointmentConfirmationStatus;
  status: AppointmentStatus;
  isNeedAttention: boolean;
  waitingTimeStartUTC: Date
}

export class AppointmentsExtraDetails {
  appointmentTypesValue: string;
  appointmentTypesColor: string;
  casesValue: string;
  payerValue: string;
  referralValue: string;
  concessionValue: string;
  patientFullNameWithTitle: string;
  isTreatmentNotesFlag: number;
  isNewPatientFlag: boolean;
  isPaymentFlag: boolean;
  isLetterFlag: boolean;
  mobile: string;
}

export class TreatmentDetails {
  treatmentServiceProduct: TreatmentServiceProduct[];
  id: string;
  practitionerId: string;
  appointmentsId: string;
  concessionId: string;
  isStatus: boolean;
  createdBy: string;
  createdDate: Date;
  modifiedBy: string;
  modifiedDate: Date;
}

export class TreatmentServiceProduct {
  id: string;
  treatmentDetailsId: string;
  treatementType: TreatmentType; // 0 = Service, 1 = Product
  productId: string;
  serviceId: string;
  units: string;
  duration: number;
  price: number;
  tax: number;
  isStatus: boolean;
  createdBy: string;
  createdDate: Date;
  modifiedBy: string;
  modifiedDate: Date;
}

export class ScheduleResponse {
  practitionerSchedules: Schedule;
  recurringDateResults: RecurringDates[];
  response: string;
}

export class RecurringDates {
  dateTimeUTC: Date;
  startTime: string;
  endTime: string;
  isAvailable: boolean;
}

export class ScheduleCheck {
  practitionerId: string;
  isSchduled: boolean;
}

export class AppointmentResponse {
  appointmentsModel: Appointment;
  recurringDateResults: RecurringDates[];
  response: string;
}

export enum TreatmentType {
  Service,
  Product
}
