import { Type } from "@angular/core";

export enum CaseContactType {
  Referral = 1,
  ThridParty = 2
}

export class CaseContactsModel {
  id: string;
  casesId: string;
  type: number[];
  contactId: string;
  practitionerReferredTo: string;
  referenceNo: string;
  startDate: string;
  endDate: string;
  count: number;
  ordered: number;
  notes?: string;
  isDefault: boolean;
  documentId?: string;
  status: number;
  contactType: string;
  index: number;
  files: any;
}

export class PatientCaseModel {
  id: string;
  caseName?: string;
  locationId: string;
  patientId?: string;
  primaryPractitionerId?: string;
  caseStatus: number;
  mergedToCaseId?: string;
  injuryDate?: string;
  notes?: string;
  isdefault?: boolean;
  caseContacts?: any = new CaseContactsModel();
  treatmentData: any;
}

export class PatientModel {
  id: string;
  parentBusinessId: string;
  patientPhoto: string;
  patientStatus: number;
  patientId: string;
  firstName: string;
  lastName: string;
  middleName: string;
  preferredName: string;
  locationId: string;
  title: number;
  gender: string;
  dob: string;
  firstVisitDate: string;
  position: number;
  status: number;
  nationality: string;
  language: string;
  address: string;
  country: string;
  state: string;
  city: string;
  postCode: string;
  homePhone: string;
  workPhone: string;
  mobile: string;
  email: string;
  relationship: number;
  emergencyContactName: string;
  emergencyContactHomePhone: string;
  emergencyContactWorkPhone: string;
  emergencyContactMobile: string;
  emergencyContactEmail: string;
  patientClassification: number;
  medicalCondition: string;
  allergy: string;
  medication: string;
  occupation: number;
  designation: string;
  employer: number;
  marketingSource: string;
  concession: string;
  invoiceNotes: string;
  creditCardType: number;
  cardHolderName: string;
  creditCardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvv: number;
  accountName: string;
  bsbCode: number;
  accountNumber: string;
  patientLocation: PatientLocationModel[];
  patientHealthFund: PatientHealthFundModel[];
  patientCommunication: PatientCommunicationModel[];
  locationName: any;
  practitionerId: any;
  isStatus?: boolean;
}
export class PatientLocationModel {
  patientId?: string;
  locationId: string;
  isStatus?: boolean;
}

export class PatientHealthFundModel {
  patientId?: string;
  healthFundId: number;
  membershipNumber: string;
  irnUpi: string;
  cardType: string;
  expiryMonth: string;
  expiryYear: string;
  claimantFirstName: string;
  claimantLastName: string;
  claimantDOB: string;
  claimantMedicareNo: string;
  claimantIrnUpi: number;
  isStatus?: boolean;
}

export class PatientCommunicationModel {
  patientId?: string;
  communicationType: string;
  reminder: boolean;
  marketing: boolean;
}

export class PatientTab {
  public title: string;
  public active: boolean = false;
  public component: Type<any>;
  public addUrl: string;
  public IsFrom: any;
  public moduleName: string;

  // constructor(component: Type<any>, title: string, active: boolean = false, IsFrom: any = null) {
  //   this.component = component;
  //   this.title = title;
  //   this.active = active;
  //   this.IsFrom = IsFrom;
  // }
}

export class PatientPersonalizeModel {
  id: string;
  locationId: string;
  personalizeType: number;
  prefix: string;
  suffix: string;
  startingNumber: string;
}

export class MergePatientModel {
  id: string;
  patientId: string;
  name: string;
  image: string;
  gender: string;
  mobile: string;
  phone: string;
  email: string;
  address: string;
  dob: string;
  totalAppointments: string;
  totalOutstanding: string;
}

export class DocumentListModel {
  id: string;
  patientId: string;
  locationId: string;
  documentName: string;
  uploadedDate: string;
  documentTypes: string;
  caseName: string;
  note: string;
  fileTypes: string;
  size: string;
  streamedFileContent: string;
  lockUnlock: string;
}
export class PatientDocumentUpdateModel {
  documentId: string;
  patientId: string;
  locationId: string;
  documentName: string;
  uploadedDate: string;
  documentTypes: string;
  caseName: string;
  note: string;
}

export class MergePatientsModel {
  originalId: string;
  duplicateId: string;
}
export class PatientDocumentUpdateLockModel {
  id: string;
  patientId: string;
  lockUnlock: number;
}

export class PractitonerLocationSpecialityModel {
  locationId: string;
  specialityId: string;
  locationName: string;
  specialityName: string;
  templateNameList: TemplateNameList[];
}

export class PatientTreatmentNotesModel {
  eligibleTreatment: EligibleTreatmentNoteTemplatesModel[];
  patientTreatmentNote: PatientTreatmentNoteModel[];
}

export class EligibleTreatmentNoteTemplatesModel {
  id: string;
  treatmentName: string;
  treatmentCaseStatus: number;
  specialityName: string;
  specialityId: string;
  locationId: string;
  locationName: string;
}

export class PatientTreatmentNoteModel {
  id: string;
  treatmentName: string;
  status: number;
  modifiedDate: string;
  practitionerName: string;
  practitionerId: string;
  casesName: string;
  casesId: string;
  service: string[] = [];
  isPinned: boolean;
  i: number;
  traetDatalist: any;
}

export class TemplateNameList {
  treatmentId: string;
  treatmentName: string;
}

export class PatientTreatmentDetailsModel {
  locationId: string;
  id: string;
  patientId: string;
  treatmentName: string;
  status: number;
  modifiedDate: string;
  practitionerId: string;
  casesId: string;
  service: string[] = [];
  treatmentNotesFor: number;
  appointmentId: string;
  entryDatetime: string;
  treatmentData: string
  finalizeOn: string;
  entryOrAppointmentDate: string;
  isPinned: boolean;
  IsDischarge: boolean;
  IsReopen: boolean;
}
export class PatientTreatmentNotesDetailsFinalizeModel {
  locationId: string;
  id: string;
  patientId: string;
  newPatientId: string;
  treatmentName: string;
  casesId: string;
  treatmentNotesFor: number
  appointmentId: string;
  entryDatetime: string;
  isPinned: boolean
  TreatmentNotesFor: number;
}

export class PatientMinInfoModel {
  Id: string
  PatientPhoto: string
  PatientId: string
  PractitionerId: string
  FirstName: string
  LastName: string
  MiddleName: string
  PreferredName: string
  Title: string
  PatientStatus: number
  Gender: string
  DOB: string
  Address: string
  Country: string
  State: string
  City: string
  PostCode: string
  HomePhone: string
  WorkPhone: string
  Mobile: string
  Email: string
  PatientClassification: number
  patientLocation: PatientLocationModel[];
  SelectedPractitionerName: string
  AllLocationName: string
  SelectedPatientClassificationName: string
}
