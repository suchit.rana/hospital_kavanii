export class RoleModel {
    id: string;
    roleName: string;
}

//use for getRoleAndPermissionsByUserLocation
export class LocationRoleAndPermissionModel {
    isFullAccess: boolean;
    moduleName: string;
    appSubModules: LocationRoleAndSubPermissionModel[];
}

export class LocationRoleAndSubPermissionModel {
    subModuleName: string;
    accessLevel: string;
}

/**
 * New Model as per new APIs structure
 */

export interface RoleModule {
  moduleId: string,
  moduleName: string,
  parentModuleId: string,
  roles: ModuleRoles[]
}

export interface ModuleRoles {
  businessRoleModulesAccessId: string,
  roleName: string,
  moduleRoleAccess: ModuleAccess,
  modulePartialAccess: string
}

export interface Roles {
  id: string,
  roleName: string,
  field: string,
  isCustomRole: boolean
}

export enum ModuleAccess {
  None = -1,
  Partial,
  Full
}
