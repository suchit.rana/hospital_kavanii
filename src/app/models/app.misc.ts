import {AppUtils} from '../shared/AppUtils';

export class ImageSnippet {
  status: string;
  pending: string;
  plainBase64: string;

  constructor(public src: string, public file: File) {
    this.plainBase64 = AppUtils.getPlainBase64str(src);
  }
}

export enum MessageType {
  error,
  success,
  warning,
  info,
}

export class ChartImageSnippet {
  status: string;
  pending: string;

  constructor(public src: string, public file: File) {}
}
