export class BillingSummary {
  totalInvoiced: number;
  totalUnvoiced: number;
  totalPaid: number;
  totalCredit: number;
  totalBalance: number;
}

export class invoiceBatch {
  batchNo: string;
  customBatchNo: string;
  batchDate: Date;
  thirdPartyName: string;
  amount: any;
  batchReceivedAmount: any;
  status: string;
  emailedDate: Date;
  notes: string;
  batchId: string;
  invoices: InvoiceModel[];
}

export class InvoiceModel {
  invoiceNumber: string;
  invoiceDate: Date;
  bookingDate: Date;
  practitioner: string;
  offerings: string;
  case: string;
  payer: string;
  total: any;
  paid: any;
  balance: any;
  status: any;
  locked: boolean;
  logs?: CreditLogModel[];

  /*Credit & Refund Start*/
  id: string;
  parentBusinessId: string;
  locationId: string;
  patientId: string;
  billingId: string;
  billingDate: string;
  creditType: string;
  cardNumber: string;
  expiryDay: any;
  expiryMonth: any;
  cvv: any;
  chequeNo: string;
  bankBranch: string;
  referenceNo: any;
  payerName: any;
  amount: number;
  notes: string;
  statusAsText: string;
  action: number;
  creditId: any;
  isVoid: boolean;
  refundHistory: Array<RefundHistory>;
  refundCreditSummary: RefundCreditSummary = new RefundCreditSummary();
  /*Credit & Refund End */
}

export class InvoiceItemModel {
  staff: string;
  offerings: string;
  fee: any;
  qty: any;
  rate: any;
  discount: any;
  taxRate: any;
  amount: any;
  paid: any;
}

export enum CreditType {
  Credit = 0,
  Refund
}

export class CreditModel {
  id: string;
  patientId: string;
  parentBusinessId: string;
  locationId: string;
  creditType: string;
  cardNumber: string;
  expiryDate: string;
  cvv: number;
  chequeNo: string;
  bankBranch: string;
  referenceNo: string;
  payerName: string;
  amount: number;
  notes: string;
  status: CreditType;
  statusAsText: string;
  billingDate: Date;
  creditId: string;
  billingId:string;
  billingCreditId:string;
  refundHistory: Array<RefundHistory>;
  refundCreditSummary: RefundCreditSummary = new RefundCreditSummary();
}

export class RefundHistory {

}

export class RefundCreditSummary {
  totalCreditReceived: number;
  totalCreditRefunded: number;
  totalCreditUsed: number;
  totalCreditBalance: number;
}


export class CreditLogModel {
  date: Date;
  logText: string;
  staff: string;
}



export class BILLING_DATA {
  static Invoices: InvoiceModel[] = [
    // {
    //   invoiceNumber: '0011',
    //   invoiceDate: new Date('12/01/2022'),
    //   bookingDate: new Date('12/01/2022'),
    //   practitioner: 'Raj Kumar',
    //   offerings: '103 - test',
    //   case: 'General',
    //   payer: 'Bupa',
    //   total: 120,
    //   paid: 80,
    //   balance: 20,
    //   status: 'UnPaid',
    //   locked: false

    // },
    // {
    //   invoiceNumber: '0011',
    //   invoiceDate: new Date('12/01/2022'),
    //   bookingDate: new Date('12/01/2022'),
    //   practitioner: 'Raj Kumar',
    //   offerings: '103 - test',
    //   case: 'General',
    //   payer: 'Bupa',
    //   total: 120,
    //   paid: 80,
    //   balance: 20,
    //   status: 'UnPaid',
    //   locked: false

    // },
    // {
    //   invoiceNumber: '0011',
    //   invoiceDate: new Date('12/01/2022'),
    //   bookingDate: new Date('12/01/2022'),
    //   practitioner: 'Raj Kumar',
    //   offerings: '103 - test',
    //   case: 'General',
    //   payer: 'Bupa',
    //   total: 120,
    //   paid: 80,
    //   balance: 20,
    //   status: 'UnPaid',
    //   locked: false

    // },
    // {
    //   invoiceNumber: '0011',
    //   invoiceDate: new Date('12/01/2022'),
    //   bookingDate: new Date('12/01/2022'),
    //   practitioner: 'Raj Kumar',
    //   offerings: '103 - test',
    //   case: 'General',
    //   payer: 'Bupa',
    //   total: 120,
    //   paid: 80,
    //   balance: 20,
    //   status: 'UnPaid',
    //   locked: false

    // },
    // {
    //   invoiceNumber: '0011',
    //   invoiceDate: new Date('12/01/2022'),
    //   bookingDate: new Date('12/01/2022'),
    //   practitioner: 'Raj Kumar',
    //   offerings: '103 - test',
    //   case: 'General',
    //   payer: 'Bupa',
    //   total: 120,
    //   paid: 80,
    //   balance: 20,
    //   status: 'UnPaid',
    //   locked: true,
    //   logs: [
    //     {
    //       date: new Date(),
    //       logText: "Text",
    //       staff: "Sylendra"
    //     }
    //   ]

    // }
  ];


  static Batches: invoiceBatch[] = [
    {
      batchId: '1',
      batchNo: '123456',
      customBatchNo: '1223323',
      batchDate: new Date(),
      thirdPartyName: 'Balaji Medicals',
      amount: '20',
      batchReceivedAmount: '120',
      status: 'Created',
      emailedDate: new Date(),
      notes: 'Test Notes',
      invoices: BILLING_DATA.Invoices
    }
  ];

}

