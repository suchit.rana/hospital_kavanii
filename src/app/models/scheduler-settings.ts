import {SchedulerViewEnum} from '../enum/scheduler-view-enum';
import {SchedulerGroupByenum} from '../enum/scheduler-group-byenum';

export interface SchedulerSettings {
  id: string,
  schedulerView: SchedulerViewEnum,
  groupBy: SchedulerGroupByenum,
  isCustomPractitionerSort: boolean,
  isShowPractitionerStatic: boolean,
  isShowPublicHoliday: boolean,
  isShowHourLineLabel: boolean,
  isShowMinutesLineLabel: boolean,
  isShowTimeToolTip: boolean,
  isShowPatientName: boolean,
  completedApoointmentColor: string;
}
