import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';


@Injectable()
export class AppErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        return throwError(this.onCatchError(err));
      }));
  }

  onCatchError(err): string {
    if (err.status === 401) {
      this.router.navigate(['login']);
      return "Unauthorized";
    }
    if (err.status == 0) {
      return ((err['message'] ? err['message'] : err['statusText']) || err['statusText']);
    }
    return err ? err.error ? err.error['message'] : 'Failed to fetch data. Try Again'  : 'Failed to fetch data. Try Again';
  }
}
