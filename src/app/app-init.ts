import {AppState} from './app.state';

export function AppInit(appState: AppState): () => Promise<void> {
    return () => new Promise<void>(async (resolve, _) => {
        await appState.fetchRolesAndPermission();
        resolve();
    });
}
