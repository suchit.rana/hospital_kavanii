import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/app.authenication.service';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {onSideNavChange, animateText} from '../animations/animations';
import {AppState} from '../app.state';
import {LocationGridModel} from '../models/app.location.model';
import {BusinessService} from '../services/app.business.service';
import {RoleService} from '../services/app.role.service';
import {MatSelectChange} from '@angular/material';
import {Observable} from 'rxjs';
import {BusinessFormModel} from '../models/app.business.model';
import {BaseItemComponent} from '../shared/base-item/base-item.component';
import {hasPermission} from '../app.component';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.css'],
    animations: [onSideNavChange, animateText]
})
export class SidenavComponent implements OnInit {
    private sideNavState: boolean;
    isLocationPopupOpen: boolean = false;

    constructor(public appState: AppState,
    ) {
    }

    ngOnInit() {
        this.sideNavState = this.appState.sideNavToggle;
    }

    onLocationChanged(value: string) {
        this.appState.selectedUserLocationIdState.next(value);
        this.toggleLocationSelection();
    }

    onSidenavToggle() {
        this.sideNavState = !this.appState.sideNavToggle;
        this.appState.sideNavState.next(this.sideNavState);
    }

    toggleLocationSelection() {
        this.isLocationPopupOpen = !this.isLocationPopupOpen;
    }

    hasPermission(moduleName: string, permission: string | string[]) {
        return hasPermission(this.appState, moduleName, permission);
    }

    get businessName(): Observable<BusinessFormModel> {
        return this.appState.businessDetailsSubject.asObservable();
    }
}
