import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../services/app.authenication.service';
import {ForgotPasswordModel} from '../../../models/app.user.model';
import {AppState} from '../../../app.state';
import {FormControl, Validators} from '@angular/forms';
import {email} from '../../../shared/validation';
import {ActivatedRoute} from '@angular/router';
import {BaseItemComponent} from '../../../shared/base-item/base-item.component';
import {ERROR_CODE} from '../../../shared/Constant';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css', '../../../forgot-password-flow-header.css'],
  encapsulation: ViewEncapsulation.None
})
export class ForgotPasswordComponent extends BaseItemComponent implements OnInit, OnDestroy {
  successLinkSentMsg = 'Password reset link has been sent to the email you\'ve provided.\nPlease check your inbox or junk folder.';
  accNExistLinkSentMsg = 'Sorry, we are unable to locate your account. Please try again or contact your Business administrator for further assistance.';
  lnkExpireMsg = 'The link you are trying to access is no longer available.\nPlease enter your email address below to request a new password link';

  forgotModel: ForgotPasswordModel = new ForgotPasswordModel();
  emailCtrl = new FormControl('', [Validators.required, email]);

  userid: string;
  username: string;
  forgotSubmitted: boolean = false;
  submitting: boolean = false;
  // errorMessage: string;
  closable: boolean = false;
  // messageType: MessageType;
  private isLnkExpire: boolean = false;

  constructor(private authService: AuthService,
              protected appState: AppState,
              private route: ActivatedRoute) {
    super(null);
    let params = this.route.snapshot.queryParams;
    let status = params['status'];
    if (status && status == 'timeout') {
      this.isLnkExpire = true;
      this.displayErrorMessage(this.lnkExpireMsg);
    }
  }

  ngOnInit() {
    this.appState.contrastBackgroundSubject.next(true);
  }

  ngOnDestroy() {
    this.appState.contrastBackgroundSubject.next(false);
  }

  forgotPassword(event: Event) {
    if (this.emailCtrl.invalid) {
      return;
    }
    this.submitting = true;
    event.preventDefault();

    this.authService.forgotPassword(this.forgotModel).subscribe(response => {
      console.log(response)
      if (response.code == ERROR_CODE) {
        this.displayErrorMessage(response.message);
      }else{
        this.displaySuccessMessage(this.successLinkSentMsg);
      }
      this.forgotSubmitted = false;
      this.submitting = false;
    }, error => {
      // console.log(error)
      // if (error.status == 400 && error.error.includes('Invalid account')) {
      //   this.errorMessage = this.accNExistLinkSentMsg;
      // } else {
      //   this.errorMessage = error.error;
      // }
      this.displayErrorMessage(error)
      this.forgotSubmitted = false;
      this.submitting = false;
    });
  }
}
