import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState} from '../../../app.state';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FinaliseModel} from '../../../models/app.user.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../services/app.authenication.service';
import {AppUtils} from '../../../shared/AppUtils';
import {MessageType} from '../../../models/app.misc';

@Component({
  selector: 'app-set-reset-password',
  templateUrl: './set-reset-password.component.html',
  styleUrls: ['./set-reset-password.component.css', '../../../forgot-password-flow-header.css']
})
export class SetResetPasswordComponent implements OnInit, OnDestroy {

  isDisplayNewPass = false;
  isDisplayConfirmPass = false;

  resetPassModel: FinaliseModel = new FinaliseModel();
  passwordForm = new FormGroup({
    password: new FormControl('', Validators.compose([
      // 1. Password Field is Required
      Validators.required,
      // 2. check whether the entered password has a number
      AppUtils.patternValidator(/\d/, {hasNumber: true}),
      // 3. check whether the entered password has upper case letter
      AppUtils.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
      // 4. check whether the entered password has a lower-case letter
      AppUtils.patternValidator(/[a-z]/, {hasSmallCase: true}),
      // 5. check whether the entered password has a special character
      AppUtils.patternValidator(/(?=.*?[#?!@$%^&*-])/, {hasSpecialCharacters: true}),
      // 6. Has a minimum length of 8 characters
      Validators.minLength(8)
    ])),
    confirmPassword: new FormControl('', Validators.required)
  });

  userid: string;
  token: string;
  username: string;
  formSubmitted: boolean = false;
  submitting: boolean = false;
  errorMessage: string;
  messageType: MessageType;
  isResetPassScreen: boolean = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private appState: AppState,
              private authService: AuthService) {
    let params = this.route.snapshot.queryParams;
    this.userid = params['userid'];
    this.token = params['token'];

    this.route.url.subscribe(value => {
      // console.log(value[0].path)
      if (value[0].path == 'resetPassword') {
        this.isResetPassScreen = true;
        let data = new FinaliseModel();
        data.userid = this.userid;
        data.token = this.token;
        delete data.password;
        this.authService.validateResetPasswordToken(this.userid, this.token).subscribe(response => {
          if (response && response['code'] == '0001') {
            this.router.navigate(['/account/forgotpassword'], {queryParams: {status: 'timeout'}});
          }
        }, error => {
          // if (error.status == 400) {
          //   if (error.error && error.error.toLowerCase().includes('invalid token')) {
          //     this.router.navigate(['/account/forgotpassword'], {queryParams: {status: 'timeout'}});
          //   }
          // }
        });
      }
    }).unsubscribe();
  }

  ngOnInit() {
    this.appState.contrastBackgroundSubject.next(true);
    this.setValidators();
  }

  ngOnDestroy() {
    this.appState.contrastBackgroundSubject.next(false);
  }

  setValidators() {
    this.passwordForm.get('confirmPassword').valueChanges.subscribe(value => {
      if (!value || value != this.resetPassModel.password) {
        this.passwordForm.get('confirmPassword').markAsTouched();
        this.passwordForm.get('confirmPassword').markAsDirty();
        this.passwordForm.get('confirmPassword').setErrors({'passwordNotMatched': true});
      } else {
        this.passwordForm.get('confirmPassword').setErrors(null);
      }
    });
  }

  submitForm(event: Event) {
    if (this.resetPassModel.password != this.passwordForm.get('confirmPassword').value) {
      this.passwordForm.get('confirmPassword').setErrors({'passwordNotMatched': true});
    } else {
      this.passwordForm.get('confirmPassword').setErrors(null);
    }
    if (this.passwordForm.invalid) {
      AppUtils.checkFormAndDisplayError(this.passwordForm);
      return;
    }
    this.resetPassModel.userid = this.userid;
    this.resetPassModel.token = this.token;
    this.submitting = true;
    this.errorMessage = null;
    event.preventDefault();

    if (this.isResetPassScreen) {
      this.resetPassword();
    } else {
      this.accountSetup();
    }

  }

  resetPassword() {
    this.authService.resetPassword(this.resetPassModel).subscribe(response => {
      if (response && response['code'] == '0001') {
        this.messageType = MessageType.error;
        this.errorMessage = "Something went wrong. Try Again."
        this.formSubmitted = false;
        this.submitting = false;
      } else {
        this.router.navigate(['account/loginFull'], {queryParams: {screen: 'resetPassword', code: 200}});
        this.formSubmitted = true;
        this.submitting = false;
      }
    }, error => {
      if (error.status == 400) {
        if (error.error && error.error.toLowerCase().includes('invalid token')) {
          this.router.navigate(['account/forgotpassword'], {queryParams: {status: 'timeout'}});
        }
      } else {
        this.errorMessage = error.error.message;
      }
      this.errorMessage = error.error.text;
      this.formSubmitted = false;
      this.submitting = false;
    });
  }

  accountSetup() {
    this.submitting = true;
    this.errorMessage = null;

    if (!this.passwordForm.invalid) {
      // let finalise: FinaliseModel = new FinaliseModel();
      // finalise.userid = this.userid;
      // finalise.password = this.passwordForm.get('password').value;

      if (this.token && this.userid) {
        this.authService.confirmEmail(this.token, this.userid).subscribe(d => {
          if (d && d['code'] == 'E0002') {
            this.messageType = MessageType.error;
            this.errorMessage = d['message'];
            this.formSubmitted = false;
            this.submitting = false;
          } else {
            delete this.resetPassModel.token;
            this.authService.createPassword(this.resetPassModel).subscribe(response => {
              if (response && response['code'] == '0001') {
                this.messageType = MessageType.error;
                this.errorMessage = "Something went wrong. Try Again."
                this.formSubmitted = false;
                this.submitting = false;
              } else {
                this.router.navigate(['account/loginFull'], {queryParams: {screen: 'register', code: 200}});
                this.formSubmitted = false;
                this.submitting = false;
              }
            }, error => {
              this.errorMessage = error.error;
              this.formSubmitted = false;
              this.submitting = false;
            });
          }
        }, error => {
          this.errorMessage = error.error;
          this.messageType = MessageType.error;
          this.formSubmitted = false;
          this.submitting = false;
        });
      }
    }
  }
}
