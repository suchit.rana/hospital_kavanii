import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFullScreenComponent } from './login-full-screen.component';

describe('LoginFullScreenComponent', () => {
  let component: LoginFullScreenComponent;
  let fixture: ComponentFixture<LoginFullScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginFullScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFullScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
