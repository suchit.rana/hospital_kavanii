import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AppState} from '../../../app.state';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageType} from '../../../models/app.misc';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../services/app.authenication.service';

@Component({
  selector: 'app-login-full-screen',
  templateUrl: './login-full-screen.component.html',
  styleUrls: ['./login-full-screen.component.css', '../login/login.component.css', '../../../forgot-password-flow-header.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginFullScreenComponent implements OnInit, OnDestroy {

  loginForm = new FormGroup({
    userid: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required)
  });

  errorMessage: string;
  messageType: MessageType;
  signInMessage: string = "Log In";
  signing: boolean = false;
  isDisplayPass: boolean = false;

  constructor(private appState: AppState,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {
    let params = this.route.snapshot.queryParams;
    let screen = params['screen'];
    let code = params['code'];
    if (code == 200){
      this.messageType = MessageType.success;
      if (screen == 'resetPassword') {
        this.errorMessage = "Password is reset successfully.\nPlease use this credential to login.";
      } else if(screen == 'register'){
        this.errorMessage = "Password is set successfully.\nPlease use this credential to login.";
      }
    }
  }

  ngOnInit() {
    this.appState.contrastBackgroundSubject.next(true);
  }

  ngOnDestroy() {
    this.appState.contrastBackgroundSubject.next(false);
  }

  signIn($event) {
    this.signInMessage = "Logging in...";
    this.signing = true;
    this.errorMessage = null;
    $event.preventDefault();

    // let userid = this.loginForm.get("userid").value;
    // let password = this.loginForm.get("password").value;

    if(this.loginForm.valid) {
      // this.router.navigate(["authorisation", ""]);
      this.authService.login(this.loginForm.value).subscribe(result => {
        if(result) {
          localStorage.setItem("token", result.token);
          localStorage.setItem("username", result.username);
          localStorage.setItem("firstName", result.firstName);
          localStorage.setItem("lastName", result.lastName);
          localStorage.setItem("token_id", result.id);
          this.router.navigate(["authorisation", result.token]);
          this.signInMessage = "Sign in";
          this.signing = false;
        }
      }, error => {
        this.messageType = MessageType.error;
        this.errorMessage = error.error.message;
        this.signInMessage = "Log in";
        this.signing = false;
      });
    }
  }

}
