import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContentLayoutComponent} from './content-layout.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {LoginFullScreenComponent} from './login-full-screen/login-full-screen.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {SetResetPasswordComponent} from './set-reset-password/set-reset-password.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {
    path: '',
    component: ContentLayoutComponent,
    children: [
      {path: 'login', component: LoginComponent},
      {path: 'loginFull', component: LoginFullScreenComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'forgotpassword', component: ForgotPasswordComponent},
      {path: 'activate', component: SetResetPasswordComponent},
      {path: 'resetPassword', component: SetResetPasswordComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentLayoutRoutingModule { }
