import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Register} from '../../../models/app.user.model';
import {AuthService} from '../../../services/app.authenication.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from 'src/environments/environment';
import {BaseItemComponent} from '../../../shared/base-item/base-item.component';
import {Router} from '@angular/router';
import {InvisibleReCaptchaComponent, ReCaptchaV3Service} from 'ngx-captcha';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})

export class RegisterComponent extends BaseItemComponent implements OnInit, AfterViewInit {
  registerModel: Register = new Register();
  enableRegister: boolean = true;
  showErrorMessage: boolean;
  registered: boolean = false;
  // message:string;
  // messageType: MessageType;
  createButtonText: string = 'Register';
  @ViewChild('captchaElem', {static: false}) captchaElem: InvisibleReCaptchaComponent;

  registerForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    username: new FormControl('', [Validators.required, Validators.email]),
    businessname: new FormControl('', Validators.required),
    isTermOfService: new FormControl(false, Validators.requiredTrue),
    recaptcha: new FormControl('', Validators.required)
  });

  recaptchaKey: string;
  recaptcha: any;

  constructor(
    // private appService: AppService,
    private reCaptchaV3Service: ReCaptchaV3Service,
    private authService: AuthService,
    protected router: Router,
  ) {
    super(null);
  }


  ngOnInit() {
    this.recaptchaKey = environment.recaptchaKey;
  }

  ngAfterViewInit(): void {
    if (this.captchaElem) {
      this.captchaElem.execute();
    }
  }

  loadCaptcha() {
    if (this.captchaElem) {
      this.captchaElem.execute();
    }
  }

  handleSuccess($event) {

  }

  register($event: Event) {

    if (this.registerForm.invalid) {
      return;
    }

    this.enableRegister = false;
    $event.preventDefault();
    // this.message = null;
    // this.messageType = MessageType.error;

    if (!this.registerForm.invalid) {
      this.createButtonText = 'Registering...';
      this.authService.register(this.registerForm.value).subscribe(data => {
        this.registered = true;
        this.createButtonText = 'Registered';
        this.router.navigate(['/account/login'], {queryParams: {'screen': 'register', 'code': 0}});
      }, error => {
        this.enableRegister = true;
        this.createButtonText = 'Register';
        this.displayErrorMessage(error);
      });
    }
  }
}
