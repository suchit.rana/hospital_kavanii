import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../services/app.authenication.service';
import {FormControl, FormGroup, Validators,FormBuilder} from '@angular/forms';
import {BaseItemComponent} from '../../../shared/base-item/base-item.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class LoginComponent extends BaseItemComponent implements OnInit {
  hide = true;
  showErrorMessage : boolean;
  loginForm = new FormGroup({
    // userid: new FormControl("", [Validators.required, Validators.email]),
    userid: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  });

  // errorMessage: string;
  // messageType = MessageType;
  signInMessage: string = 'Log In';
  signing: boolean = false;

  constructor(
    // private appService: AppService,
    protected router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,

    private authService: AuthService) {
    super(null);
    let qParams = this.route.snapshot.queryParams;
    if (qParams && qParams['screen'] == 'register' && qParams['code'] == 0) {
      this.displaySuccessMessage("Registered successfully. A message has been sent to the email you've provided. Please check your inbox or junk folder.");
    }
    if (qParams && qParams['screen'] == 'logout' && qParams['code'] == 0) {
      this.displaySuccessMessage("You have logout Succesfully");
    }
  }

  ngOnInit() {

  }

  signIn($event) {
    if(this.loginForm.invalid){
      console.log("enter");
      // this.showErrorMessage = true;
      // setTimeout(() => this.showErrorMessage = false, 5000);
      // this.signInMessage = 'Log In';
      return;
    }

    this.signInMessage = 'Logging in...';
    this.signing = true;
    this.message = null;
    $event.preventDefault();


    // let userid = this.loginForm.get("userid").value;
    // let password = this.loginForm.get("password").value;

    if (this.loginForm.valid) {
      // this.router.navigate(["authorisation", ""]);
      this.authService.login(this.loginForm.value).subscribe(result => {
        if (result) {
          localStorage.setItem('token', result.token);
          localStorage.setItem('username', result.username);
          localStorage.setItem('firstName', result.firstName);
          localStorage.setItem('lastName', result.lastName);
          localStorage.setItem('token_id', result.id);
          this.router.navigate(['authorisation', result.token]);
          this.signInMessage = 'Login in';
          this.signing = false;
        }
      }, error => {
        this.displayErrorMessage(error);
        this.signInMessage = 'Log in';
        this.signing = false;
      });
    }
  }

}
