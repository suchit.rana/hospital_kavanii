import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentLayoutRoutingModule } from './content-layout-routing.module';
import {LoginComponent} from './login/login.component';
import {ContentLayoutComponent} from './content-layout.component';
import {SharedModuleModule} from '../../shared/shared-module.module';
import {RegisterComponent} from './register/register.component';
import {LoginFullScreenComponent} from './login-full-screen/login-full-screen.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {SetResetPasswordComponent} from './set-reset-password/set-reset-password.component';
import {NgxCaptchaModule} from 'ngx-captcha';
import {CharacterLimitModule} from '../../directive/character-limit/character-limit.module';


@NgModule({
  declarations: [
    ContentLayoutComponent,
    LoginComponent,
    RegisterComponent,
    LoginFullScreenComponent,
    ForgotPasswordComponent,
    SetResetPasswordComponent,
    SetResetPasswordComponent
  ],
  imports: [
    CommonModule,
    SharedModuleModule,
    ContentLayoutRoutingModule,
    NgxCaptchaModule,
    CharacterLimitModule
  ]
})
export class ContentLayoutModule { }
