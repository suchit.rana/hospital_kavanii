import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DropDownListModule, MultiSelectModule} from '@progress/kendo-angular-dropdowns';
import {GridModule} from '@progress/kendo-angular-grid';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {MatButtonToggleModule, MatDividerModule, MatExpansionModule, MatTabsModule} from '@angular/material';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    MultiSelectModule,
    DropDownListModule,
    GridModule,
    InputsModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatExpansionModule,
    MatDividerModule
  ]
})
export class ManagingModule { }
