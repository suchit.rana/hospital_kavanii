import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContentFullLayoutComponent} from './content-full-layout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SecurityGuard} from '../../authorisation/authorisation.guard';
import { KnowledgeBaseComponent } from './knowledge-base/knowledge-base.component';


const routes: Routes = [
  {
    path: '',
    component: ContentFullLayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'tools',
        loadChildren: () => import("../content-full-layout/tools/tools.module").then(m => m.ToolsModule)
      },
      {
        path: 'staffs',
        loadChildren: () => import("../content-full-layout/staffs/users/users.module").then(m => m.UsersModule)
      },
      {
        path: 'offerings',
        loadChildren: () => import("../content-full-layout/offerings/offering.module").then(m => m.OfferingModule)
      },
      {
        path: 'contacts',
        loadChildren: () => import("../content-full-layout/contacts/contacts.module").then(m => m.ContactsModule)
      },
      {
        path: 'appointment',
        loadChildren: () => import("../content-full-layout/appointment/appoinment.module").then(m => m.AppoinmentModule),
        data: { preload: true}
      },
      {
        path: 'patients',
        loadChildren: () => import('../content-full-layout/patients/patients.module').then(m => m.PatientsModule),
        data: { preload: true}
      },
      {
        path: 'settings',
        loadChildren: () => import('../content-full-layout/settings/settings.module').then(m => m.SettingsModule),
      },
      {
        path: 'reports',
        loadChildren: () => import('../content-full-layout/reports/reports.module').then(m => m.ReportsModule),
      },
      { path: 'bills', loadChildren: () => import('../content-full-layout/billing/billing.module').then(m => m.BillingModule) },
      { path: 'communications', loadChildren: () => import('../content-full-layout/communications/communications.module').then(m => m.CommunicationsModule) },
      { path: 'knowledgebase', component: KnowledgeBaseComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentFullLayoutRoutingModule { }
