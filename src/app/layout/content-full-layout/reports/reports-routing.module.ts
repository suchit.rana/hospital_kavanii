import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReportsComponent} from './reports.component';
import {PreDefinedReportsComponent} from './component/pre-defined-reports/pre-defined-reports.component';
import {ReportDisplayComponent} from './component/report-display/report-display.component';

const routes: Routes = [
  {path: '', redirectTo: 'pre-defined', pathMatch: 'full'},
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: 'pre-defined',
        component: PreDefinedReportsComponent
      },
    ]
  },
  {
    path: 'pre-defined/display/:reportId/:subReportId/:reportType',
    component: ReportDisplayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
