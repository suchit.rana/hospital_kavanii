import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportsRoutingModule} from './reports-routing.module';
import {ReportsComponent} from './reports.component';
import {BlockUIModule} from 'ng-block-ui';
import {AppCommonModule} from '../common/app-common.module';
import {ManagingModule} from '../../module/managing.module';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {RouterModule} from '@angular/router';
import { PreDefinedReportsComponent } from './component/pre-defined-reports/pre-defined-reports.component';
import {MatCardModule, MatExpansionModule, MatListModule, MatSelectModule} from '@angular/material';
import { ReportDisplayComponent } from './component/report-display/report-display.component';
import {DropdownTreeViewModule} from '../../../shared/dropdown-treeview/dropdown-tree-view.module';
import { ReportFilterComponent } from './component/core/report-filter/report-filter.component';
import {AppKendoMultiSelectModule} from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import {AppDatePickerModule} from '../common/app-date-picker/app-date-picker.module';
import {AppDateRangePickerModule} from '../common/app-date-range-picker/app-date-range-picker.module';
import {AppTimePickerModule} from '../common/time-picker/app-time-picker.module';
import {OnlyNumberModule} from '../../../directive/only-number/only-number.module';


@NgModule({
    declarations: [
        ReportsComponent,
        PreDefinedReportsComponent,
        ReportDisplayComponent,
        ReportFilterComponent
    ],
    imports: [
        CommonModule,
        ReportsRoutingModule,
        AppCommonModule,
        ManagingModule,
        SharedContentFullLayoutModule,
        RouterModule,
        BlockUIModule,
        MatListModule,
        MatExpansionModule,
        MatCardModule,
        DropdownTreeViewModule,
        MatSelectModule,
        AppKendoMultiSelectModule,
        AppDatePickerModule,
        AppDateRangePickerModule,
        AppTimePickerModule,
        OnlyNumberModule
    ]
})
export class ReportsModule {
}
