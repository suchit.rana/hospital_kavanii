import {TextValue} from '../../../../shared/interface';

export const patientReportId = 1;
export const AGE_RANGE = 'AGE_RANGE';
export const DATE_RANGE = 'DATE_RANGE';

export interface ReportList {
    id: number;
    title: string;
    type: ReportType;
    children?: ReportList[];
}

export interface ReportFilterControlDetails {
    title: string,
    control: ReportControl,
    type: ReportFilterControlType,
    data: TextValue[],
    controls?: { type: ReportFilterControlType, data?: TextValue[] }[];
}

export interface ReportFilter {
    reportId: number;
    filters: {
        control: ReportControl;
        title?: string;
        controlDetails?: ReportFilterControlDetails
    }[];
}

export interface ReportColumns {
    title: string;
    key: string;
    reportIds: number[]
}

export enum ReportType {
    Custom,
    PreDefined,
    Child
}

export enum ReportFilterControlType {
    MultiSelect,
    DropDown,
    DatePicker,
    DateRangePicker,
    TimePicker,
    Text,
    DropDownDateRangePicker,
    MultiControl,
    NumericInput,
}

export enum ReportControl {
    Gender,
    Location,
    Practitioner,
    PatientStatus,
    Position,
    FirstVisit,
    Dob,
    State,
    City,
    Age,
    PatientWith,
    PatientWithout,
}

export const preDefinedReports: ReportList[] = [
    {
        id: patientReportId,
        title: 'Patient Reports',
        type: ReportType.PreDefined,
        children: [
            {
                id: 2,
                title: 'Patient General Report',
                type: ReportType.Child,
            },
            {
                id: 3,
                title: 'New Patient Procedure Code (Service & Class) Report',
                type: ReportType.Child,
            },
            {
                id: 4,
                title: 'Patients With Referral Details Report',
                type: ReportType.Child,
            },
            {
                id: 5,
                title: 'Referral Details Report',
                type: ReportType.Child,
            },
            {
                id: 6,
                title: 'Patient-Practitioner Retention Report',
                type: ReportType.Child,
            }
        ]
    }
];

export const reportFilter: ReportFilter[] = [
    {
        reportId: 2,
        filters: [
            {
                control: ReportControl.Gender
            },
            {
                control: ReportControl.Location
            },
            {
                control: ReportControl.Practitioner
            },
            {
                control: ReportControl.PatientStatus
            },
            {
                control: ReportControl.Position
            },
            {
                control: ReportControl.FirstVisit
            },
            {
                control: ReportControl.Dob
            },
            {
                control: ReportControl.State
            },
            {
                control: ReportControl.City
            },
            {
                control: ReportControl.Age
            },
            {
                control: ReportControl.PatientWith
            },
        ]
    }
];

export const filterData: ReportFilterControlDetails[] = [
    {
        title: 'Gender',
        control: ReportControl.Gender,
        type: ReportFilterControlType.MultiSelect,
        data: [
            {text: 'Male', value: 'Male'},
            {text: 'Female', value: 'Female'},
            {text: 'Other', value: 'Other'},
        ]
    },
    {
        title: 'Location',
        control: ReportControl.Location,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'Primary Practitioner',
        control: ReportControl.Practitioner,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'Patient Status',
        control: ReportControl.PatientStatus,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'Position',
        control: ReportControl.Position,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'First Visit',
        control: ReportControl.FirstVisit,
        type: ReportFilterControlType.DropDown,
        data: []
    },
    {
        title: 'DOB',
        control: ReportControl.Dob,
        type: ReportFilterControlType.DropDown,
        data: [
            {text: 'Before', value: 'Before'},
            {text: 'After', value: 'After'},
            {text: 'Equals', value: 'Equals'},
            {text: 'Between', value: DATE_RANGE},
        ]
    },
    {
        title: 'State',
        control: ReportControl.State,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'City',
        control: ReportControl.City,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'Age',
        control: ReportControl.Age,
        type: ReportFilterControlType.DropDown,
        controls: [
            {
                type: ReportFilterControlType.DropDown,
                data: [
                    {text: 'All', value: 'All'},
                    {text: '=', value: '='},
                    {text: '>=', value: '>='},
                    {text: '<=', value: '<='},
                    {text: 'Age Range', value: AGE_RANGE},
                ]
            },
            {
                type: ReportFilterControlType.NumericInput,
            }
        ],
        data: []
    },
    {
        title: 'Patient With',
        control: ReportControl.PatientWith,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
    {
        title: 'Patient Without',
        control: ReportControl.PatientWithout,
        type: ReportFilterControlType.MultiSelect,
        data: []
    },
];

// export const reportColumns: ReportColumns[] = [
//     {
//         reportId: 2,
//         columns: [
//             {title: 'Title', key: ''},
//             {title: 'First Name', key: ''},
//             {title: 'Last Name', key: ''},
//             {title: 'Middle Name', key: ''},
//             {title: 'Preferred Name', key: ''},
//             {title: 'Full Name', key: ''},
//             {title: 'Gender', key: ''},
//             {title: 'DOB', key: ''},
//             {title: 'Age', key: ''},
//             {title: 'Position', key: ''},
//             {title: 'Nationality', key: ''},
//             {title: 'Language', key: ''},
//             {title: 'Location', key: ''},
//             {title: 'Primary Practitioner', key: ''},
//             {title: 'Address 1', key: ''},
//             {title: 'Address 2', key: ''},
//             {title: 'Country', key: ''},
//             {title: 'State', key: ''},
//             {title: 'City', key: ''},
//             {title: 'Post Code', key: ''},
//             {title: 'Full Address', key: ''},
//             {title: 'Home Phone', key: ''},
//             {title: 'Work Phone', key: ''},
//             {title: 'Mobile', key: ''},
//             {title: 'Email', key: ''},
//             {title: 'Patient Status', key: ''},
//             {title: 'Emergency Contact Relationship', key: ''},
//             {title: 'Emergency Contact Name', key: ''},
//             {title: 'Emergency Contact Home Phone', key: ''},
//             {title: 'Emergency  Contact  Work Phone', key: ''},
//             {title: 'Emergency  Contact  Mobile', key: ''},
//             {title: 'Emergency  Contact Email', key: ''},
//             {title: 'Patient Classification', key: ''},
//             {title: 'Medical Condition', key: ''},
//             {title: 'Allergy', key: ''},
//             {title: 'Medication', key: ''},
//             {title: 'Patient Occupation', key: ''},
//             {title: 'Patient Designation', key: ''},
//             {title: 'Employer', key: ''},
//             {title: 'Department', key: ''},
//             {title: 'Employer Contact Person', key: ''},
//             {title: 'Employer Work Phone', key: ''},
//             {title: 'Employer  Contact Email', key: ''},
//             {title: 'Marketing Referral Source', key: ''},
//             {title: 'Communication Consent', key: ''},
//             {title: 'Concession', key: ''},
//             {title: 'Invoice Notes', key: ''},
//             {title: 'Account Name', key: ''},
//             {title: 'BSB Code', key: ''},
//             {title: 'Account Number', key: ''},
//             {title: 'Health Fund Name', key: ''},
//             {title: 'Membership Number', key: ''},
//             {title: 'IRN / UPI', key: ''},
//             {title: 'HF Card Expiry Month', key: ''},
//             {title: 'HF Card Expiry Year', key: ''},
//         ]
//     }
// ];

export const reportColumns: ReportColumns[] = [
    {
        title: 'Title',
        key: '',
        reportIds: [2]
    },
    {
        title: 'First Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Last Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Middle Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Preferred Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Full Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Gender',
        key: '',
        reportIds: [2]
    },
    {
        title: 'DOB',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Age',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Position',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Nationality',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Language',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Location',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Primary Practitioner',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Address 1',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Address 2',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Country',
        key: '',
        reportIds: [2]
    },
    {
        title: 'State',
        key: '',
        reportIds: [2]
    },
    {
        title: 'City',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Post Code',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Full Address',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Home Phone',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Work Phone',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Mobile',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Email',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Patient Status',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency Contact Relationship',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency Contact Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency Contact Home Phone',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency  Contact  Work Phone',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency  Contact  Mobile',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Emergency  Contact Email',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Patient Classification',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Medical Condition',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Allergy',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Medication',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Patient Occupation',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Patient Designation',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Employer',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Department',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Employer Contact Person',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Employer Work Phone',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Employer  Contact Email',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Marketing Referral Source',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Communication Consent',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Concession',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Invoice Notes',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Account Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'BSB Code',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Account Number',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Health Fund Name',
        key: '',
        reportIds: [2]
    },
    {
        title: 'Membership Number',
        key: '',
        reportIds: [2]
    },
    {
        title: 'IRN / UPI',
        key: '',
        reportIds: [2]
    },
    {
        title: 'HF Card Expiry Month',
        key: '',
        reportIds: [2]
    },
    {
        title: 'HF Card Expiry Year',
        key: '',
        reportIds: [2]
    },
];
