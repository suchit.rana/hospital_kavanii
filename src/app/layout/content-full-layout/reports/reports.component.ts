import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

    activeLinkIndex = -1;
    navLinks: any[] = [];

    constructor(
        private router: Router
    ) {
        this.navLinks = [
            {
                label: 'Custom',
                link: 'custom',
                index: 0
            }, {
                label: 'Pre-defined',
                link: 'pre-defined',
                index: 1
            }
        ];
    }

    ngOnInit(): void {
        this.router.events.subscribe((res) => {
            this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
        });
    }

}
