import { Component, OnInit } from '@angular/core';
import {preDefinedReports, ReportList} from '../../data/data-provider';

@Component({
  selector: 'app-pre-defined-reports',
  templateUrl: './pre-defined-reports.component.html',
  styleUrls: ['./pre-defined-reports.component.css']
})
export class PreDefinedReportsComponent implements OnInit {

  readonly reports: ReportList[] = preDefinedReports;

  constructor() { }

  ngOnInit(): void {
  }

}
