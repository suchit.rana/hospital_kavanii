import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
    preDefinedReports,
    reportColumns,
    ReportColumns,
    reportFilter,
    ReportFilter,
    ReportList,
    ReportType
} from '../../data/data-provider';
import {AppState} from '../../../../../app.state';

@Component({
    selector: 'app-report-display',
    templateUrl: './report-display.component.html',
    styleUrls: ['./report-display.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ReportDisplayComponent implements OnInit, OnDestroy {

    readonly PredefinedReport = ReportType.PreDefined;

    constructor(
        private route: ActivatedRoute,
        private appState: AppState,
    ) {
        this.appState.sideNavState.next(false);
        const params = route.snapshot.params;
        this.reportId = params['reportId'];
        this.subReportId = parseInt(params['subReportId']);
        this.reportType = params['reportType'];

        this.report = preDefinedReports.find(r => r.id == this.reportId);
    }

    report: ReportList;
    reportId: number;
    subReportId: number;
    reportType: ReportType;
    reportColumn: ReportColumns[];
    selectedColumns: ReportColumns[] = [];


    ngOnInit(): void {
        this.reportColumn = reportColumns.filter(rc => rc.reportIds.indexOf(this.subReportId) > -1);
    }

    ngOnDestroy() {
        this.appState.sideNavState.next(true);
    }

}
