import {Component, Input, OnInit} from '@angular/core';
import {filterData, reportFilter, ReportFilter, ReportFilterControlType} from '../../../data/data-provider';

@Component({
    selector: 'app-report-filter',
    templateUrl: './report-filter.component.html',
    styleUrls: ['./report-filter.component.css']
})
export class ReportFilterComponent implements OnInit {

    readonly MultiSelect = ReportFilterControlType.MultiSelect;
    readonly DropDown = ReportFilterControlType.DropDown;
    readonly DatePicker = ReportFilterControlType.DatePicker;
    readonly DateRangePicker = ReportFilterControlType.DateRangePicker;
    readonly TimePicker = ReportFilterControlType.TimePicker;
    readonly Text = ReportFilterControlType.Text;
    readonly DropDownDateRangePicker = ReportFilterControlType.DropDownDateRangePicker;
    readonly MultiControl = ReportFilterControlType.MultiControl;
    readonly NumericInput = ReportFilterControlType.NumericInput;

    constructor() {
    }

    @Input()
    reportId: number;
    @Input()
    subReportId: number;

    reportFilter: ReportFilter;
    filters: ReportFilter;

    ngOnInit(): void {
        this.reportFilter = reportFilter.find(rf => rf.reportId == this.subReportId);

        this.reportFilter.filters.map(filter => {
            filter.controlDetails = filterData.find(fd => fd.control == filter.control);
        });
    }

}
