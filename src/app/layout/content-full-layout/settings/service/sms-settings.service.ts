import { Injectable } from '@angular/core';
import {BaseService} from '../../../../services/app.base.service';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../app.state';
import {EmailSettings} from '../interface/email-settings';
import {SmsSettings} from '../interface/sms-settings';

@Injectable({
  providedIn: 'root'
})
export class SmsSettingsService extends BaseService{

  constructor(public http: HttpClient) {
    super(http);
  }

  createSmsSettings(data: SmsSettings) {
    delete data.id;
    return this.http.post(this.environmentSettings.apiBaseUrl + '/createsmssettings', data);
  }

  updateSmsSettings(data: SmsSettings) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/updatesmssettings', data);
  }

  getSmsSettingsByLocationId() {
    return this.http.get<SmsSettings>(this.environmentSettings.apiBaseUrl + '/getsmssettings');
  }
}
