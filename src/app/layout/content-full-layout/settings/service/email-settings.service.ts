import { Injectable } from '@angular/core';
import {BaseService} from '../../../../services/app.base.service';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../app.state';
import {EmailSettings} from '../interface/email-settings';

@Injectable({
  providedIn: 'root'
})
export class EmailSettingsService extends BaseService{

  constructor(public http: HttpClient) {
    super(http);
  }

  createEmailSettings(data: EmailSettings) {
    delete data.id;
    return this.http.post(this.environmentSettings.apiBaseUrl + '/createemailsettings', data);
  }

  updateEmailSettings(data: EmailSettings) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/updateemailsettings', data);
  }

  getEmailSettingsByLocationId(locationId: string) {
    return this.http.get<EmailSettings>(this.environmentSettings.apiBaseUrl + '/getemailsettingsbylocationId', {params: {locationId}});
  }
}
