import {Injectable} from '@angular/core';
import {BaseService} from '../../../../services/app.base.service';
import {HttpClient} from '@angular/common/http';
import {EmailSettings} from '../interface/email-settings';
import {ReminderSettings} from '../reminder-settings/interface';

@Injectable({
    providedIn: 'root'
})
export class ReminderSettingsService extends BaseService {

    constructor(public http: HttpClient) {
        super(http);
    }

    create(data: ReminderSettings) {
        delete data.id;
        data.reminderConfiguration.map(obj => {
            delete obj.id;
            delete obj.index;
            delete obj.reminderSettingsId;
            delete obj['type'];
            delete obj['time'];
        });
        return this.http.post(this.environmentSettings.apiBaseUrl + '/createremindersettings', data);
    }

    update(data: ReminderSettings) {
        data.reminderConfiguration.map(obj => {
            if (!obj.id) {
                delete obj.id;
            }
            delete obj.index;
            obj.reminderSettingsId = data.id;
        });
        return this.http.post(this.environmentSettings.apiBaseUrl + '/updateremindersettings', data);
    }

    delete(id: string) {
        return this.http.delete(this.environmentSettings.apiBaseUrl + '/deleteremindersettings', {params: {id}});
    }

    getByLocationId(locationId: string) {
        return this.http.get<ReminderSettings>(this.environmentSettings.apiBaseUrl + '/getremindersettingsbylocationId', {params: {locationId}});
    }
}
