
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { ApplicationDataComponent } from './application-data/application-data.component';
import { PersonalizeComponent } from './personalize/personalize.component';
import { TreatmentNotesTemplateComponent } from './treatment-notes-template/treatment-notes-template.component';
import { AddTreatmentNotesTemplateComponent } from './treatment-notes-template/add-treatment-notes-template/add-treatment-notes-template.component';
import { TemplateLibraryComponent } from './treatment-notes-template/template-library/template-library.component';
import { EmailtemplateComponent } from './emailtemplate/emailtemplate.component';
import { LettertemplateComponent } from './lettertemplate/lettertemplate.component';
import { AddemailtemplateComponent } from './emailtemplate/addemailtemplate/addemailtemplate.component';
import { AddlettertemplateComponent } from './lettertemplate/addlettertemplate/addlettertemplate.component';
import { TemplateLibraryBySpecialityComponent } from './treatment-notes-template/template-library-byspeciality/template-library-byspeciality.component';
import { SmstemplateComponent } from './smstemplate/smstemplate.component';
import { AddsmstemplateComponent } from './smstemplate/addsmstemplate/addsmstemplate.component';
import {SmsSettingsComponent} from './sms-settings/sms-settings.component';
import {EmailSettingsComponent} from './email-settings/email-settings.component';
import {ReminderSettingsComponent} from './reminder-settings/reminder-settings.component';
const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'personalize/:option',
        component: PersonalizeComponent,
      },
      {
        path: 'applicationdata',
        component: ApplicationDataComponent,
      },
      {
        path: 'treatmentnotestemplate',
        component: TreatmentNotesTemplateComponent,
      },
      {
        path: 'treatmentnotestemplate/edit/:treatmentNotesTemplateId',
        component: AddTreatmentNotesTemplateComponent,
      },
      {
        path: 'treatmentnotestemplate/add',
        component: AddTreatmentNotesTemplateComponent,
      },
      {
        path: 'templatelibrary',
        component: TemplateLibraryComponent,
      },
      {
        path: 'templatelibrary/speciality/:specialityId',
        component: TemplateLibraryBySpecialityComponent,
      },
      {
        path: 'emailtemplate',
        component: EmailtemplateComponent,
      },
      {
        path: 'lettertemplate',
        component: LettertemplateComponent,
      },
      {
        path: 'emailtemplate/add',
        component: AddemailtemplateComponent,
      },
      {
        path: 'lettertemplate/add',
        component: AddlettertemplateComponent,
      },
      {
        path: 'emailtemplate/edit/:emailTemplateId',
        component: AddemailtemplateComponent,
      },
      {
        path: 'lettertemplate/edit/:letterTemplateId',
        component: AddlettertemplateComponent,
      },
      {
        path: 'smstemplate',
        component: SmstemplateComponent,
      },
      {
        path: 'smstemplate/add',
        component: AddsmstemplateComponent,
      },
      {
        path: 'smstemplate/edit/:smsTemplateId',
        component: AddsmstemplateComponent,
      },
      {
        path: 'smsSettings',
        component: SmsSettingsComponent,
      },
      {
        path: 'emailSettings',
        component: EmailSettingsComponent,
      },
      {
        path: 'reminder-settings',
        component: ReminderSettingsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsRoutingModule { }
