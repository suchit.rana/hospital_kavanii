export enum ReminderType {
    Email,
    Sms
}

export interface ReminderSettings {
    id: string;
    locationId: string;
    enableAutoReminder: boolean;
    enableWeekendReminder: boolean;
    enableAutoCancel: boolean;
    reminderConfiguration: ReminderConfiguration[];
}
export interface ReminderConfiguration {
    id: string;
    reminderSettingsId: string;
    reminderName: string;
    reminderType: ReminderType;
    templateId: string;
    reminderTime: Date;
    reminderDays: number;

    // Extra Keys
    index?: number;
}
