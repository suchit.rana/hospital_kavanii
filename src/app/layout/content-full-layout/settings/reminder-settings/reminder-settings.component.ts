import {AfterViewInit, Component, NgZone, OnInit, Renderer2, ViewEncapsulation} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {BaseGridComponent} from '../../shared-content-full-layout/base-grid/base-grid.component';
import {TextValue} from '../../../../shared/interface';
import {ReminderConfiguration, ReminderType} from './interface';
import {SettingsService} from '../../../../services/app.settings.service';
import {ReminderSettingsService} from '../service/reminder-settings.service';
import {AppState} from '../../../../app.state';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {DummyGUID} from '../../../../app.component';

@Component({
    selector: 'app-reminder-settings',
    templateUrl: './reminder-settings.component.html',
    styleUrls: ['./reminder-settings.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ReminderSettingsComponent extends BaseGridComponent implements OnInit, AfterViewInit {

    constructor(
        protected appState: AppState,
        protected alertService: AppAlertService,
        private reminderSettingsService: ReminderSettingsService,
        private settingsService: SettingsService,
        private renderer: Renderer2
    ) {
        super();
    }

    formGroup: FormGroup = new FormGroup({
        id: new FormControl(),
        locationId: new FormControl(),
        enableAutoReminder: new FormControl(),
        enableWeekendReminder: new FormControl(),
        enableAutoCancel: new FormControl(),
        reminderConfiguration: new FormArray([])
    });
    selectedRowIds: any[] = [];
    type: TextValue[] = [
        {text: 'Email', value: ReminderType.Email},
        {text: 'Sms', value: ReminderType.Sms},
    ];
    templates: any[] = [];
    emailTemplates: any[] = [];
    smsTemplates: any[] = [];
    sendDaysList: TextValue[] = [
        {text: '1', value: 1},
        {text: '2', value: 2},
        {text: '3', value: 3},
        {text: '4', value: 4},
        {text: '5', value: 5},
        {text: '6', value: 6},
        {text: '7', value: 7}
    ];
    private actionItem: any;

    /**
     * Drag & Drop Rows
     * */
    public dropIndex;
    public noRecordsMsg = 'No records available yet. Please drop a row.';
    // public currentGridRef: GridComponent;
    public targetCells: NodeListOf<HTMLTableDataCellElement>;

    ngOnInit(): void {
        this.formGroup.get('reminderConfiguration').valueChanges.subscribe(value => {
            this.gridData = value;
            this.state.filter = null;
            this.loadItems();
        });
        this.getByLocation();
        this.getEmailTemplate();
        this.getSmsTemplate();
    }

    reminderConfigurationFG(): FormGroup {
        return new FormGroup({
            id: new FormControl(),
            reminderSettingsId: new FormControl(),
            reminderName: new FormControl(),
            reminderType: new FormControl(),
            type: new FormControl(),
            templateId: new FormControl(),
            template: new FormControl(''),
            reminderTime: new FormControl(),
            time: new FormControl(''),
            reminderDays: new FormControl(),
            index: new FormControl((this.formGroup.get('reminderConfiguration') as FormArray).length)
        });
    }

    public getFormControl(dataItem: ReminderConfiguration, field: string): FormControl {
        // return the FormControl for the respective column editor
        return <FormControl> (
            (this.formGroup.get('reminderConfiguration') as FormArray).controls
                .find((i) => i.value.index === dataItem.index)
                .get(field)
        );
    }

    addRow(index: number = -1) {
        if (index > -1) {
            (this.formGroup.get('reminderConfiguration') as FormArray).insert(index, this.reminderConfigurationFG());
        } else {
            (this.formGroup.get('reminderConfiguration') as FormArray).push(this.reminderConfigurationFG());
        }
        this.rearrangeIndex();
    }

    deleteRow(index: number) {
        (this.formGroup.get('reminderConfiguration') as FormArray).removeAt(index);
        this.rearrangeIndex();
    }

    onTypeChange(type: ReminderType) {
        if (type === ReminderType.Email) {
            this.templates = this.emailTemplates;
        } else {
            this.templates = this.smsTemplates;
        }
    }

    cancel() {

    }

    save() {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            return;
        }
        const data = this.formGroup.value;
        data.locationId = this.appState.selectedUserLocationId;

        if (!data.id) {
            this.reminderSettingsService.create(data).subscribe(() => {
                this.alertService.displaySuccessMessage('Reminder Settings Saved Successfully.');
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        } else {
            this.reminderSettingsService.update(data).subscribe(() => {
                this.alertService.displaySuccessMessage('Reminder Settings Updated Successfully.');
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    public ngAfterViewInit() {
    }

    public ngOnDestroy() {
    }

    public getAllRows(): Array<HTMLTableRowElement> {
        return Array.from(document.querySelectorAll('.k-grid tbody tr'));
    }

    // Prevents dragging header and 'no records' row.
    public onDragStart(e: DragEvent, index: number): void {
        const draggedElement: string = (e.target as HTMLElement).innerText;
        if (draggedElement.includes('ID') || draggedElement === this.noRecordsMsg) {
            e.preventDefault();
        }

        const row = this.getAllRows()[index];
        let selectedItem: ReminderConfiguration = this.reminderConfig[index];
        let dataItem = JSON.stringify(selectedItem);
        e.dataTransfer.setData('text/plain', dataItem);
        try {
            e.dataTransfer.setDragImage(row, 0, 0);
        } catch (err) {
            // IE doesn't support setDragImage
        }
    }

    public onDrop(e: DragEvent): void {
        let data = e.dataTransfer.getData('text/plain');
        let droppedItem: ReminderConfiguration = JSON.parse(data);


        this.updateGridsData(droppedItem);

        this.removeLineIndicators();
    }

    public onDragOver(e: DragEvent): void {
        e.preventDefault();
        if (this.targetCells) {
            this.removeLineIndicators();
        }

        const targetEl = e.target as HTMLElement;
        if (
            (targetEl.tagName === 'TD' || targetEl.tagName === 'TH')
        ) {
            // Set drop line indication
            this.targetCells = targetEl.parentElement.querySelectorAll('td, th');
            this.targetCells.forEach((td: HTMLElement) => {
                const gridData: any[] = this.reminderConfig;
                if (td.tagName === 'TH' && gridData.length !== 0) {
                    this.renderer.addClass(td, 'th-line');
                    this.dropIndex = 0;
                } else if (td.tagName === 'TD') {
                    this.renderer.addClass(td, 'td-line');
                    this.dropIndex = closest(e.target, tableRow).rowIndex + 1;
                }
            });
        }
    }

    public removeLineIndicators() {
        this.targetCells.forEach((td: HTMLElement) => {
            this.renderer.removeAttribute(td, 'class');
        });
    }

    public updateGridsData(droppedItem: ReminderConfiguration): void {

        let index: number = droppedItem.index;
        const fg = (this.formGroup.get('reminderConfiguration') as FormArray).at(index);
        (this.formGroup.get('reminderConfiguration') as FormArray).removeAt(index);
        (this.formGroup.get('reminderConfiguration') as FormArray).insert(this.dropIndex - 1, fg);
        this.rearrangeIndex();
    }

    get reminderConfig(): ReminderConfiguration[] {
        return (this.formGroup.get('reminderConfiguration') as FormArray).value;
    }

    private rearrangeIndex() {
        (this.formGroup.get('reminderConfiguration') as FormArray).controls.forEach((fg, index) => {
            fg.get('index').setValue(index);
        });
    }

    private getEmailTemplate() {
        this.settingsService.getAllBussinessTemplate(2).subscribe(response => {
            this.emailTemplates = response;
        });
    }

    private getSmsTemplate() {
        this.settingsService.getAllBussinessTemplate(3).subscribe(response => {
            this.smsTemplates = response;
        });
    }

    private getByLocation() {
        this.reminderSettingsService.getByLocationId(this.appState.selectedUserLocationId).subscribe(response => {
            if (response) {
                this.formGroup.patchValue(response);
                response.reminderConfiguration && response.reminderConfiguration.forEach((value, index) => {
                   this.addRow();
                    const fg = (this.formGroup.get('reminderConfiguration') as FormArray).at(index);
                    fg.patchValue(value);
                });
            }
            if (!response.id || response.id === DummyGUID) {
                this.addRow();
                this.addRow();
                this.addRow();
            }
        });
    }

    onSelectTemplate($event: any, index: number) {
        const template = this.templates.find(item => item.id === $event);
        if (template) {
            const fg = (this.formGroup.get('reminderConfiguration') as FormArray).at(index);
            fg.get('template').setValue(template.name);
        }
        console.log((this.formGroup.get('reminderConfiguration') as FormArray).value);
    }
}

const tableRow = (node) => node.tagName.toLowerCase() === 'tr';

export const closest = (node, predicate) => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }
    return node;
};
