import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppState } from 'src/app/app.state';
import { BusinessService } from 'src/app/services/app.business.service';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { ActivatedRoute } from '@angular/router';
import { PatientModel } from 'src/app/models/app.patient.model';
import { SettingsService } from 'src/app/services/app.settings.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BusinessTemplateLocations, BusinessTemplateModel } from 'src/app/models/app.settings.model';
import { LocationGridModel } from 'src/app/models/app.location.model';
import { Router } from '@angular/router'
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import {LocationModel} from 'src/app/models/app.treatmentnotes.model';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-addlettertemplate',
  templateUrl: './addlettertemplate.component.html',
  styleUrls: ['./addlettertemplate.component.css'],
})
export class AddlettertemplateComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;
  @BlockUI() blockUI: NgBlockUI;
  ckeConfig: any;
  patients: PatientModel;
  pokemonControl = new FormControl();
  public expanded = false;
  public liked = false;
  public btnText = 'More';
  duplicateform: string;
  textdata: string = 'expand';
  public expandedKeys = [];
  public toggleRecipe(): void {
    this.expanded = !this.expanded;
    this.btnText = this.expanded ? 'Less' : 'More';
    this.heartIcon();
  }
  public heartIcon(): string {
    return this.liked ? 'k-icon k-i-arrow-60-up' : 'k-icon k-i-arrow-60-down';
  }
  public toggleLike(): void {
    this.liked = !this.liked;
  }
  public data: any[] = [
    {
      id: 2, text: 'Business Details', items: [
        { id: 3, text: 'Business Name', class1: '{', class2: '}' },
        { id: 4, text: 'Contact Person', class1: '{', class2: '}' },
        { id: 5, text: 'Address 1', class1: '{', class2: '}' },
        { id: 6, text: 'Address 2', class1: '{', class2: '}' },
        { id: 7, text: 'country', class1: '{', class2: '}' },
        { id: 8, text: 'state', class1: '{', class2: '}' },
        { id: 9, text: 'city', class1: '{', class2: '}' },
        { id: 10, text: 'Post code', class1: '{', class2: '}' },
        { id: 11, text: 'Phone', class1: '{', class2: '}' },
        { id: 12, text: 'Mobile', class1: '{', class2: '}' },
        { id: 13, text: 'Fax', class1: '{', class2: '}' },
        { id: 14, text: 'Email', class1: '{', class2: '}' },
        { id: 15, text: 'Website', class1: '{', class2: '}' },
        { id: 16, text: 'Bank Name', class1: '{', class2: '}' },
        { id: 17, text: 'BSB No', class1: '{', class2: '}' },
        { id: 18, text: 'Account No', class1: '{', class2: '}' },
        { id: 19, text: 'Account Name', class1: '{', class2: '}' },
      ]
    },
    {
      id: 20, text: 'Location Details', items: [
        { id: 21, text: 'Location  Name', class1: '{', class2: '}' },
        { id: 22, text: 'Contact Person', class1: '{', class2: '}' },
        { id: 23, text: 'Address 1', class1: '{', class2: '}' },
        { id: 24, text: 'Address 2', class1: '{', class2: '}' },
        { id: 25, text: 'country', class1: '{', class2: '}' },
        { id: 26, text: 'state', class1: '{', class2: '}' },
        { id: 27, text: 'city', class1: '{', class2: '}' },
        { id: 28, text: 'Post code', class1: '{', class2: '}' },
        { id: 29, text: 'Phone', class1: '{', class2: '}' },
        { id: 30, text: 'Mobile', class1: '{', class2: '}' },
        { id: 31, text: 'Fax', class1: '{', class2: '}' },
        { id: 32, text: 'Email', class1: '{', class2: '}' },
        { id: 33, text: 'Website', class1: '{', class2: '}' },
      ]
    },
    {
      id: 34, text: 'Patient Details', items: [
        { id: 35, text: 'Patient ID', class1: '{', class2: '}' },
        { id: 36, text: 'Title', class1: '{', class2: '}' },
        { id: 37, text: 'Full Name', class1: '{', class2: '}' },
        { id: 38, text: 'Fname', class1: '{', class2: '}' },
        { id: 39, text: 'Lname', class1: '{', class2: '}' },
        { id: 40, text: 'Gender', class1: '{', class2: '}' },
        { id: 41, text: 'DOB', class1: '{', class2: '}' },
        { id: 42, text: 'Age', class1: '{', class2: '}' },
        { id: 43, text: 'First Visit DAte', class1: '{', class2: '}' },
        { id: 44, text: 'Primary Practitioner', class1: '{', class2: '}' },
        { id: 45, text: 'Address', class1: '{', class2: '}' },
        { id: 46, text: 'country', class1: '{', class2: '}' },
        { id: 47, text: 'State', class1: '{', class2: '}' },
        { id: 48, text: 'city', class1: '{', class2: '}' },
        { id: 49, text: 'Post code', class1: '{', class2: '}' },
        { id: 50, text: 'Home Ph', class1: '{', class2: '}' },
        { id: 51, text: 'Work Ph', class1: '{', class2: '}' },
        { id: 52, text: 'Mobile', class1: '{', class2: '}' },
        { id: 53, text: 'Email', class1: '{', class2: '}' },
        {
          id: 54, text: 'Employer Details', items: [
            { id: 55, text: 'Occupation', class1: '{', class2: '}' },
            { id: 56, text: 'Employer', class1: '{', class2: '}' },
            { id: 57, text: 'Contact Person', class1: '{', class2: '}' },
            { id: 58, text: 'Work Phone', class1: '{', class2: '}' },
            { id: 59, text: 'eMail', class1: '{', class2: '}' }
          ]
        },
        {
          id: 60, text: 'Billing', items: [
            { id: 61, text: 'Invoice Notes', class1: '{', class2: '}' }
          ]
        },
        {
          id: 62, text: 'Health Fund', items: [
            { id: 63, text: 'HL', class1: '{', class2: '}' },
            { id: 64, text: 'HL Medicare Membership No', class1: '{', class2: '}' },
            { id: 65, text: 'HL Medicare IRN', class1: '{', class2: '}' },
            { id: 66, text: 'HL DVA Membership No', class1: '{', class2: '}' },
            { id: 67, text: 'HL L DVA IRN', class1: '{', class2: '}' }
          ]
        }
      ]
    },
    {
      id: 68, text: 'Case', items: [
        { id: 69, text: 'Case Name', class1: '{', class2: '}' },
      ]
    },
    {
      id: 70, text: 'Appointment Details', items: [
        { id: 71, text: 'Show DAte and Time', class1: '{', class2: '}' },
        { id: 72, text: 'Patient First Appt Date Time', class1: '{', class2: '}' },
        { id: 73, text: 'Patient Last Appt', class1: '{', class2: '}' },
        { id: 74, text: 'Patient Current Appt', class1: '{', class2: '}' },
        { id: 75, text: 'Patient Next Appt', class1: '{', class2: '}' },
        { id: 76, text: 'Patient All Past Appointments', class1: '{', class2: '}' },
        { id: 77, text: 'Patient All Suture Appointments', class1: '{', class2: '}' },
      ]
    },
    {
      id: 78, text: 'Practitioner Details', items: [
        { id: 79, text: 'Title', class1: '{', class2: '}' },
        { id: 80, text: 'First Name', class1: '{', class2: '}' },
        { id: 81, text: 'Last Name', class1: '{', class2: '}' },
        { id: 82, text: 'Name (Fname + LName)', class1: '{', class2: '}' },
        { id: 83, text: 'qualification', class1: '{', class2: '}' },
        { id: 84, text: 'position', class1: '{', class2: '}' },
        { id: 85, text: 'Phone', class1: '{', class2: '}' },
        { id: 86, text: 'Mobile', class1: '{', class2: '}' },
        { id: 87, text: 'Email', class1: '{', class2: '}' },
        { id: 88, text: 'Signature', class1: '{', class2: '}' },
        { id: 89, text: 'speciality', class1: '{', class2: '}' },
        { id: 90, text: 'Provider No', class1: '{', class2: '}' },
        { id: 91, text: 'Notes', class1: '{', class2: '}' },
        { id: 92, text: 'Patient First Appt Practitioner', class1: '{', class2: '}' },
        { id: 93, text: 'Patient Last Appt Practitioner', class1: '{', class2: '}' },
        { id: 94, text: 'Patient Current Appt Practitioner', class1: '{', class2: '}' },
        { id: 95, text: 'Patient Next Appt Practitioner', class1: '{', class2: '}' }

      ]
    },
    {
      id: 96, text: 'Referral Details', items: [
        { id: 97, text: 'Organization Name', class1: '{', class2: '}' },
        { id: 98, text: 'Title', class1: '{', class2: '}' },
        { id: 99, text: 'First Name', class1: '{', class2: '}' },
        { id: 100, text: 'Last Name', class1: '{', class2: '}' },
        { id: 101, text: 'Name (Fname + LName)', class1: '{', class2: '}' },
        { id: 102, text: 'speciality', class1: '{', class2: '}' },
        { id: 103, text: 'Provider No', class1: '{', class2: '}' },
        { id: 104, text: 'Phone', class1: '{', class2: '}' },
        { id: 105, text: 'Mobile', class1: '{', class2: '}' },
        { id: 106, text: 'FAX', class1: '{', class2: '}' },
        { id: 107, text: 'Email', class1: '{', class2: '}' },
        { id: 108, text: 'Notes', class1: '{', class2: '}' },
        { id: 109, text: 'start Date', class1: '{', class2: '}' },
        { id: 110, text: 'End Date', class1: '{', class2: '}' },
        { id: 111, text: 'Appointment Count', class1: '{', class2: '}' },
        { id: 112, text: 'Referral Notes', class1: '{', class2: '}' }

      ]
    },
    {
      id: 113, text: 'Third Party Details', items: [
        { id: 114, text: 'Organization Name', class1: '{', class2: '}' },
        { id: 115, text: ' Department Name', class1: '{', class2: '}' },
        { id: 116, text: 'Title', class1: '{', class2: '}' },
        { id: 117, text: 'First Name', class1: '{', class2: '}' },
        { id: 118, text: 'Last Name', class1: '{', class2: '}' },
        { id: 119, text: 'Name (Fname + LName)', class1: '{', class2: '}' },
        { id: 120, text: 'Reference No', class1: '{', class2: '}' },
        { id: 121, text: 'Phone', class1: '{', class2: '}' },
        { id: 122, text: 'Mobile', class1: '{', class2: '}' },
        { id: 123, text: 'FAX', class1: '{', class2: '}' },
        { id: 124, text: 'Email', class1: '{', class2: '}' },
        { id: 125, text: 'Notes', class1: '{', class2: '}' },
        { id: 126, text: 'start Date', class1: '{', class2: '}' },
        { id: 127, text: 'End Date', class1: '{', class2: '}' },
        { id: 128, text: 'Appointment Count', class1: '{', class2: '}' }

      ]
    },
    {
      id: 129, text: 'Invoice Details', items: [
        { id: 130, text: 'Invoice No', class1: '{', class2: '}' },
        { id: 131, text: 'Date', class1: '{', class2: '}' },
        { id: 132, text: 'Invoice Amt', class1: '{', class2: '}' },
        { id: 133, text: 'GST', class1: '{', class2: '}' },
        { id: 134, text: 'Total', class1: '{', class2: '}' },
        { id: 135, text: 'Paid', class1: '{', class2: '}' },
        { id: 136, text: 'Balance', class1: '{', class2: '}' }

      ]
    },
    {
      id: 137, text: 'Misc', items: [
        { id: 138, text: 'Today’s Date', class1: '{', class2: '}' },
        { id: 139, text: 'Current Time', class1: '{', class2: '}' },
        { id: 140, text: 'He \ She', class1: '{', class2: '}' },
        { id: 141, text: ' His or her', class1: '{', class2: '}' },

      ]
    }
  ];

  private show: boolean = false;
  private selectedKeys = ['Tags'];

  public onToggle(): void {
    this.show = !this.show;
  }
  public handleSelection({ index }: any): void {
    // console.log(document.getElementById("thing"));
    this.show = false;
  }
  logPan(evt: any) {
    console.log(evt);
  }
  apperance = 'outline';
  addClass = true;
  locationList: any[];
  public opened: boolean = false;
  openedduplicate: boolean = false;
  locationData: LocationModel[] = [];
  bussinesstemplate: BusinessTemplateModel;
  locationMdata: any = [];
  selectedData: any;
  letterForm: FormGroup = new FormGroup({
    name: new FormControl("", Validators.required),
    locationName: new FormControl(""),
    description: new FormControl(""),
    tag: new FormControl(""),
    status: new FormControl(true),
    subject: new FormControl("", Validators.required),
    body: new FormControl("", Validators.required),
  });



  constructor(
    private businessService: BusinessService,
    protected router: Router,
    protected appState: AppState,
    public location: Location,
    public _route: ActivatedRoute,
    public settingsService: SettingsService,
  ) {
    super(location);
    this.patients = new PatientModel()
    console.log(this.patients)
  }
  locationInterval: any;

  ngOnInit() {
    this.ckeConfig = {
      removeButtons: 'Save,NewPage,Templates,Scayt,HiddenField,CreateDiv,Language,Anchor,Flash,Iframe,About,ImageButton,Image',
      extraAllowedContent: 'img[width,height,align]',

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets-all
      extraPlugins: 'easyimage',
      removePlugins: 'elementspath',
      height: 350,

      // An array of stylesheets to style the WYSIWYG area.
      // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
      contentsCss: ['https://cdn.ckeditor.com/4.9.2/full-all/contents.css', 'https://ckeditor.com/docs/ckeditor4/4.16.0/examples/assets/stylesheetparser/stylesheetparser.css'],
      stylesSet: [
        /* Inline Styles */
        { name: 'Marker', element: 'span', attributes: { 'class': 'marker' } },
        { name: 'Cited Work', element: 'cite' },
        { name: 'Inline Quotation', element: 'q' },

        /* Object Styles */
        {
          name: 'Special Container',
          element: 'div',
          styles: {
            padding: '5px 10px',
            background: '#eee',
            border: '1px solid #ccc'
          }
        },
        {
          name: 'Compact table',
          element: 'table',
          attributes: {
            cellpadding: '1',
            cellspacing: '0',
            border: '1',
            bordercolor: '#ccc'
          },
          styles: {
            'border-collapse': 'collapse'
          }
        },
        { name: 'Borderless Table', element: 'table', styles: { 'border-style': '1px', 'background-color': '#E6E6FA' } },
        { name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }
      ]
    };


    this.bussinesstemplate = new BusinessTemplateModel();
    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((data) => {
        data.map((d) => {
          const m = new LocationModel();
          m.locationId = d.id;
          m.locationName = d.locationName;
          this.locationData.push(m);
        });

        const locationsModel: BusinessTemplateLocations[] = [];
        this.locationData.forEach(
          (l: { locationId: string; locationName: string }) => {
            console.log("#############################", this.appState.selectedUserLocation.id)
            if (this.appState.selectedUserLocation.id === l.locationId) {
              const m = new BusinessTemplateLocations();
              m.locationId = l.locationId;
              m.locationName = l.locationName;
              locationsModel.push(m);
              this.letterForm.get("locationName").patchValue(locationsModel);
            }
          }
        );
        this.bussinesstemplate.businessTemplateLocations = locationsModel;
        this.locationMdata = locationsModel;
      });
    if (this.locationMdata != '') {
      this.letterForm.get('locationName').setValidators([Validators.required]);
    }
    this._route.params.subscribe(params => {
      if (params.letterTemplateId) {
        this.blockUI.start();
        this.addItem = false;
        this.itemid = params.letterTemplateId;
        this.settingsService.getallbusinesstemplatesById(this.itemid).subscribe(s => {
          this.letterForm.patchValue(s);
          let locationName = [];
          s.businessTemplateLocations.forEach(sl => {
            locationName.push(this.locationData.find(l => l.locationId == sl.locationId));
          });

          this.bussinesstemplate.businessTemplateLocations = locationName;
          this.blockUI.stop();
        });
      }
    });
    console.log(this.bussinesstemplate.businessTemplateLocations);
  }
  expand() {
    if (this.textdata == 'expand') {
      this.textdata = 'collapse';
      this.data.forEach((i, idx) => {
        this.expandedKeys.push(idx.toString());
      })
    }
    else {
      this.textdata = 'expand';
      this.expandedKeys = [];
    }
  }
  onLocationChange(locationData: any[]) {
    if (locationData.length === 0) {
      this.bussinesstemplate.businessTemplateLocations = [];
    } else {

      const locationsModel: BusinessTemplateLocations[] = [];

      locationData.forEach(
        (l: { locationId: string; locationName: string }) => {
          const m = new BusinessTemplateLocations();
          m.locationId = l.locationId;
          m.locationName = l.locationName;
          locationsModel.push(m);
        }
      );
      this.bussinesstemplate.businessTemplateLocations = locationsModel;
    }
  }
  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: 'startsWith'
  };
  submitform() {
    let bussinesstemplate: BusinessTemplateModel = this.letterForm.value;
    if (!this.letterForm.invalid) {
      this.blockUI.start();
      this.submitting = true;
      bussinesstemplate.businessTemplateLocations = this.bussinesstemplate.businessTemplateLocations;
      bussinesstemplate.type = 1;
      console.log(bussinesstemplate);
      // return;
      if (this.addItem) {
        this.settingsService.createbusinesstemplate(bussinesstemplate).subscribe(d => {
          this.submitting = false;
          if (this.duplicateform == 'Duplicate') {
            this.settingsService.sharedData = 'Your template has been duplicated successfully';
          }
          else {

            this.settingsService.sharedData = 'Your template has been created successfully';

          }
          this.router.navigate(['/settings/lettertemplate/']);
          this.blockUI.stop();
          this.itemid = d;
          this.addItem = false;
        }, error => {
          if (this.duplicateform == 'Duplicate') {
            this.displayErrorMessage("The name entered already exists, please try a different name!");
          }
          else {

            this.displayErrorMessage("Error occurred while adding template, please try again.");

          }
          this.submitting = false;
          this.blockUI.stop();
          console.log(error);
        });
      }
      else {
        bussinesstemplate.id = this.itemid;
        this.settingsService.updatebusinesstemplate(bussinesstemplate).subscribe(d => {
          this.submitting = false;
          this.settingsService.sharedData = 'Your template has been updated successfully';
          this.router.navigate(['/settings/lettertemplate/']);
          this.blockUI.stop();
        }, error => {
          console.log(error);
          this.displayErrorMessage("Error occurred while updating template, please try again.");
          this.submitting = false;
          this.blockUI.stop();
        });
      }
    }
  }
  delete() {
    this.opened = true;

  }
  public close() {
    this.opened = false;
    this.openedduplicate = false;
  }
  duplicate() {
    this.openedduplicate = true;
  }
  confirdelete(status, name) {
    if (name == 'delete') {
      if (status == "yes") {
        this.settingsService
          .deletebusinesstemplatesById(this.itemid)
          .subscribe(
            () => {
              this.settingsService.sharedData = 'Your template has been deleted successfully';
              this.router.navigate(['/settings/lettertemplate/']);
            },
            () => {
              this.displayErrorMessage(
                "Error occurred while deleting Treatment Notes Template, please try again."
              );
            }
          );
      }
    }
    else {
      this.blockUI.start();
      let bussinesstemplate: BusinessTemplateModel = this.letterForm.value;
      this.letterForm.controls.name.setValue('');
      this.letterForm.controls.description.setValue(bussinesstemplate.description);
      this.letterForm.controls.subject.setValue(bussinesstemplate.subject);
      this.letterForm.controls.body.setValue(bussinesstemplate.body);
      let locations: BusinessTemplateLocations[] = [];
      console.log("$$$$$$$$$$$$$$$$$$$$$$$$$", this.letterForm.get("locationName").value)
      let locationSelections: LocationGridModel[] = this.letterForm.get("locationName").value;
      if (locationSelections) {
        locationSelections.forEach(l => {
          let m = new BusinessTemplateLocations();
          m.locationId = l.id;
          m.locationName = l.locationName;
          locations.push(m);
        });
      }
      bussinesstemplate.businessTemplateLocations = locations;
      bussinesstemplate.type = 1;
      this.blockUI.stop();
      this.addItem = true;
      this.duplicateform = "Duplicate"
      this.openedduplicate = false;
    }
  }

  allowDrop(ev): void {
    ev.preventDefault();
  }
  drag(ev): void {
    this.selectedData = ev.target.nodeValue
    console.log(ev.target.nodeValue)
    // this.render.addClass(ev.target.nodeValue, "selected");
  }
  drop(ev): void {
    var newValue = this.selectedData;

    ev.target.nodeValue =
      '<span class="dragdata">' + newValue + '</span>';

    console.log(ev.target.nodeValue);
  }
  cleartext(fieldname) {
    if (fieldname == 'templatename') {
      this.letterForm.controls.name.setValue('');
    }
    if (fieldname == 'subject') {
      this.letterForm.controls.subject.setValue('');
    }
  }
}
