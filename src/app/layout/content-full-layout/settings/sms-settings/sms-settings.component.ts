import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseItemComponent} from '../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {AppState} from '../../../../app.state';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {SmsSettingsService} from '../service/sms-settings.service';
import {SmsSettings} from '../interface/sms-settings';

@Component({
  selector: 'app-sms-settings',
  templateUrl: './sms-settings.component.html',
  styleUrls: ['./sms-settings.component.scss']
})
export class SmsSettingsComponent extends BaseItemComponent implements OnInit {

  constructor(
      public location: Location,
      protected appState: AppState,
      protected alertService: AppAlertService,
      private appDialogService: AppDialogService,
      private smsSettingsService: SmsSettingsService,
  ) {
    super(location);
  }

  @ViewChild('paymentMethodNotAddedDialog', {static: false}) paymentMethodNotAddedDialog;

  formGroup: FormGroup = new FormGroup({
    id: new FormControl(),
    parentBusinessId: new FormControl(),
    clientId: new FormControl(),
    apiKey: new FormControl(),
    apiSecret: new FormControl(),
    autoRecharge: new FormControl(false),
    rechargeSmsCount: new FormControl(),
    rechargeBelowSmsCount: new FormControl(),
    emailAlert: new FormControl(false),
    emailAlertBelowSmsCount: new FormControl(),
    balanceSms: new FormControl('', [Validators.min(100)]),
    status: new FormControl(true),
  });
  balance = 0;

  ngOnInit(): void {
    this.getSmsSettings();
  }

  buySms() {
    const ref = this.appDialogService.open(this.paymentMethodNotAddedDialog, {
      title: 'You can not purchase credits',
      width: '778px',
      height: '300px',
      hideOnSave: false,
      saveBtnTitle: 'Update Payment Details',
    });
    ref.clickSave.subscribe(() => {

    });
  }

  save() {
    const data: SmsSettings = this.formGroup.value;

    data.parentBusinessId = this.appState.UserProfile.parentBusinessId;

    if (!data.id) {
      // if (!data.location) {
      //     data.location = [];
      // }
      // data.location.push({locationId: data.locationId});
      this.smsSettingsService.createSmsSettings(data).subscribe(response => {
        if (response) {
          this.alertService.displaySuccessMessage('SMS Settings Saved Successfully.');
        }
      }, error => {
        this.alertService.displayErrorMessage(error);
      });
    } else {
      this.smsSettingsService.updateSmsSettings(data).subscribe(response => {
        if (response) {
          this.alertService.displaySuccessMessage('SMS Settings Updated Successfully.');
        }
      }, error => {
        this.alertService.displayErrorMessage(error);
      });
    }
  }

  private getSmsSettings() {
    this.smsSettingsService.getSmsSettingsByLocationId().subscribe(data => {
      if (data && data.id !== '00000000-0000-0000-0000-000000000000') {
        this.formGroup.patchValue(data);
      }
    }, error => {
      this.alertService.displayErrorMessage(error);
    });
  }

}
