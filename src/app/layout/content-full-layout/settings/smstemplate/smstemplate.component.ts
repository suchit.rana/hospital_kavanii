import {Component, OnInit} from '@angular/core';
import {BaseGridComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {SettingsService} from 'src/app/services/app.settings.service';
import {MatButtonToggleChange} from '@angular/material';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {NavigationEnd, Router} from '@angular/router';
import {MessageType} from 'src/app/models/app.misc';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-smstemplate',
  templateUrl: './smstemplate.component.html',
  styleUrls: ['./smstemplate.component.css']
})
export class SmstemplateComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;

  smsTitle = '';
  smsRouteLink = '';
  smsRouteName = '';
  smsDescription = '';
  isRoot = false;
  dataLoaded: boolean;
  isLoading: boolean;
  displaySuccessMessage: string;
  listViewSMSShow = false;
  messageType = MessageType;
  status: 'active';
  @BlockUI() blockUI: NgBlockUI;

  constructor(private SettingService: SettingsService, protected router: Router) {
    super();
  }

  ngOnInit() {
    this.isRoot = this.router.url === '/settings/smstemplate';
    if (this.isRoot && !this.dataLoaded) {
      this.populateLanding();
    }
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isRoot = event.url === '/settings/smstemplate';
        if (this.isRoot) {
          this.populateLanding();
        }
      }
    });
    this.displayMessage();
  }

  populateLanding() {
    this.blockUI.start();
    this.dataLoaded = true;
    this.SettingService.getAllBussinessTemplate(3).subscribe((data) => {
      console.log(data);
      if (data.length === 0) {
        this.smsTitle = 'You haven\'t added a SMS Template.';
        this.smsRouteLink = '/settings/smstemplate/add';
        this.smsRouteName = 'Add Template';
        this.smsDescription = 'You can create user-friendly Templates to save time while drafting SMS for patients.';
        this.listViewSMSShow = false;
      } else {
        this.listViewSMSShow = true;
        data.forEach(d => {
          d['isStatus'] = d.status;
          if (d.description.length > 90) {
            d.description = d.description.substring(0, 70);
          } else {
            d.description = d.description;
          }
          d.locationName = d.businessTemplateLocations.map(l => l).join(',');
        });
      }
      this.gridData = data;
      this.loadItemstatus('name');
    });
    this.isLoading = false;
    this.blockUI.stop();
    this.displayMessage();
  }

  SmsActiveChanged(event: MatButtonToggleChange) {
    if (event.value == 'active') {
      this.setActiveFilterstatus();
    } else {
      this.setInactiveFilterstatus();
    }
    this.loadItemstatus('name');
  }

  displayMessage() {
    if (
      this.SettingService.sharedData != undefined &&
      this.SettingService.sharedData != ''
    ) {
      this.displaySuccessMessage = this.SettingService.sharedData;
      setInterval(
        (a) => {
          this.displaySuccessMessage = '';
          this.SettingService.sharedData = '';
        },
        10000,
        []
      );
    }
  }
}
