import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsmstemplateComponent } from './addsmstemplate.component';

describe('AddsmstemplateComponent', () => {
  let component: AddsmstemplateComponent;
  let fixture: ComponentFixture<AddsmstemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsmstemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsmstemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
