import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BusinessService } from "src/app/services/app.business.service";
import { AppState } from "src/app/app.state";
import { LocationModel } from 'src/app/models/app.treatmentnotes.model';
import { BusinessTemplateLocations, BusinessTemplateModel } from 'src/app/models/app.settings.model';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { SettingsService } from 'src/app/services/app.settings.service';
import { LocationGridModel } from 'src/app/models/app.location.model';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';
@Component({
  selector: 'app-addsmstemplate',
  templateUrl: './addsmstemplate.component.html',
  styleUrls: ['./addsmstemplate.component.css']
})
export class AddsmstemplateComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;

  @BlockUI() blockUI: NgBlockUI;
  locationData: LocationModel[] = [];
  public value = false;
  bussinesstemplate: BusinessTemplateModel;
  maxChars = 612;
  smsForm: FormGroup = new FormGroup({
    name: new FormControl("", Validators.required),
    locationName: new FormControl("", Validators.required),
    description: new FormControl(""),
    status: new FormControl(true),
    body: new FormControl("", Validators.required),
    isAllowAllLocation: new FormControl(false),
  });
  templateName: string;
  templatelocation: any;
  templateDescription: any;
  templatestatus: any;
  templatebody: any;
  public expanded = false;
  public liked = false;
  public btnText = 'More';
  textdata: string = 'expand';
  duplicateform: string;
  public expandedKeys = [];
  characterlength: number = 0;
  errormessage: any;
  smscount: number = 1;
  public opened: boolean = false;
  openedduplicate: boolean = false;
  openconfirmScreen: boolean = false;
  public toggleRecipe(): void {
    this.expanded = !this.expanded;
    this.btnText = this.expanded ? 'Less' : 'More';
    this.heartIcon();
  }
  public heartIcon(): string {
    return this.liked ? 'k-icon k-i-arrow-60-up' : 'k-icon k-i-arrow-60-down';
  }
  public toggleLike(): void {
    this.liked = !this.liked;
  }
  public data: any[] = [
    {
      id: 2, text: 'Business Details', items: [
        { id: 3, text: 'Business Name', class1: '{', class2: '}' },
        { id: 4, text: 'Contact Person', class1: '{', class2: '}' },
        { id: 5, text: 'Address 1', class1: '{', class2: '}' },
        { id: 6, text: 'Address 2', class1: '{', class2: '}' },
        { id: 7, text: 'country', class1: '{', class2: '}' },
        { id: 8, text: 'state', class1: '{', class2: '}' },
        { id: 9, text: 'city', class1: '{', class2: '}' },
        { id: 10, text: 'Post code', class1: '{', class2: '}' },
        { id: 11, text: 'Phone', class1: '{', class2: '}' },
        { id: 12, text: 'Mobile', class1: '{', class2: '}' },
        { id: 13, text: 'Fax', class1: '{', class2: '}' },
        { id: 14, text: 'Email', class1: '{', class2: '}' },
        { id: 15, text: 'Website', class1: '{', class2: '}' }
      ]
    },
    {
      id: 16, text: 'Location Details', items: [
        { id: 17, text: 'Location  Name', class1: '{', class2: '}' },
        { id: 18, text: 'Contact Person', class1: '{', class2: '}' },
        { id: 19, text: 'Address 1', class1: '{', class2: '}' },
        { id: 20, text: 'Address 2', class1: '{', class2: '}' },
        { id: 21, text: 'country', class1: '{', class2: '}' },
        { id: 22, text: 'state', class1: '{', class2: '}' },
        { id: 23, text: 'city', class1: '{', class2: '}' },
        { id: 24, text: 'Post code', class1: '{', class2: '}' },
        { id: 25, text: 'Phone', class1: '{', class2: '}' },
        { id: 26, text: 'Mobile', class1: '{', class2: '}' },
        { id: 27, text: 'Fax', class1: '{', class2: '}' },
        { id: 28, text: 'Email', class1: '{', class2: '}' },
        { id: 29, text: 'Website', class1: '{', class2: '}' },
      ]
    },
    {
      id: 30, text: 'Patient Details', items: [
        { id: 31, text: 'Patient ID', class1: '{', class2: '}' },
        { id: 32, text: 'Title', class1: '{', class2: '}' },
        { id: 33, text: 'Full Name', class1: '{', class2: '}' },
        { id: 34, text: 'Fname', class1: '{', class2: '}' },
        { id: 35, text: 'Lname', class1: '{', class2: '}' },
        { id: 36, text: 'Gender', class1: '{', class2: '}' },
        { id: 37, text: 'DOB', class1: '{', class2: '}' },
        { id: 38, text: 'Age', class1: '{', class2: '}' },
        { id: 54, text: 'He \ She', class1: '{', class2: '}' },
        { id: 55, text: ' His or her', class1: '{', class2: '}' },
      ]
    },
    {
      id: 39, text: 'Appointment Details', items: [
        { id: 40, text: 'Appointment DateTime', class1: '{', class2: '}' },
        { id: 41, text: 'Appointment Practitioner', class1: '{', class2: '}' },
        { id: 42, text: 'Appointment Location', class1: '{', class2: '}' },
      ]
    },
    {
      id: 43, text: 'Invoice Details', items: [
        { id: 44, text: 'Invoice No', class1: '{', class2: '}' },
        { id: 45, text: 'Date', class1: '{', class2: '}' },
        { id: 46, text: 'Invoice Amt', class1: '{', class2: '}' },
        { id: 47, text: 'GST', class1: '{', class2: '}' },
        { id: 48, text: 'Total', class1: '{', class2: '}' },
        { id: 49, text: 'Paid', class1: '{', class2: '}' },
        { id: 50, text: 'Balance', class1: '{', class2: '}' },

      ]
    },
    {
      id: 51, text: 'Misc', items: [
        { id: 52, text: 'Today’s Date', class1: '{', class2: '}' },
        { id: 53, text: 'Current Time', class1: '{', class2: '}' },
      ]
    }
  ];
  private selectedKeys = ['Tags'];
  constructor(public location: Location, private businessService: BusinessService, protected appState: AppState, public _route: ActivatedRoute, public settingsService: SettingsService, protected router: Router,) { super(location); }

  ngOnInit() {
    this.bussinesstemplate = new BusinessTemplateModel();
    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((data) => {
        data.map((d) => {
          const m = new LocationModel();
          m.locationId = d.id;
          m.locationName = d.locationName;
          this.locationData.push(m);
        });

        const locationsModel: BusinessTemplateLocations[] = [];
        this.locationData.forEach(
          (l: { locationId: string; locationName: string }) => {
            if (this.appState.selectedUserLocation.id === l.locationId) {
              const m = new BusinessTemplateLocations();
              m.locationId = l.locationId;
              m.locationName = l.locationName;
              locationsModel.push(m);
              this.smsForm.get("locationName").patchValue(locationsModel);
            }
          }
        );
        this.bussinesstemplate.businessTemplateLocations = locationsModel;
      });
    this._route.params.subscribe(params => {
      if (params.smsTemplateId) {
        this.blockUI.start();
        this.addItem = false;
        this.itemid = params.smsTemplateId;
        this.settingsService.getallbusinesstemplatesById(this.itemid).subscribe(s => {
          console.log(s)
          this.templateName = s.name;
          this.templatelocation = s.locationName;
          this.templateDescription = s.description;
          this.templatestatus = s.isStatus;
          this.templatebody = s.body;
          this.smsForm.patchValue(s);
          let locationName = [];
          s.businessTemplateLocations.forEach(sl => {
            locationName.push(this.locationData.find(l => l.locationId == sl.locationId));
          });

          this.bussinesstemplate.businessTemplateLocations = locationName;
          this.blockUI.stop();
        });
      }
    });
  }
  cleartext(fieldname) {
    if (fieldname == 'templatename') {
      this.smsForm.controls.name.setValue('');
    }
    if (fieldname == 'bodydata') {
      this.smsForm.controls.body.setValue('');
    }
  }
  onLocationChange(locationData: any[]) {
    if (locationData.length === 0) {
      this.bussinesstemplate.businessTemplateLocations = [];
    } else {

      const locationsModel: BusinessTemplateLocations[] = [];

      locationData.forEach(
        (l: { locationId: string; locationName: string }) => {
          const m = new BusinessTemplateLocations();
          m.locationId = l.locationId;
          m.locationName = l.locationName;
          locationsModel.push(m);
        }
      );
      this.bussinesstemplate.businessTemplateLocations = locationsModel;
    }
  }
  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: 'startsWith'
  };
  expand() {
    if (this.textdata == 'expand') {
      this.textdata = 'collapse';
      this.data.forEach((i, idx) => {
        this.expandedKeys.push(idx.toString());
      })
    }
    else {
      this.textdata = 'expand';
      this.expandedKeys = [];
    }
  }
  onTextChange(event: any) {
    let str = event.replace(/(\r\n|\n|\r)/gm, "  ");
    if (str.length > this.maxChars) {
      event = event.slice(0, this.maxChars);
      this.characterlength = event.length;
    }
    else {
      this.characterlength = str.length;
    }
    if (this.characterlength >= 592) {
      this.errormessage = "To avoid SMS failures, it is recommended to leave atleast 20 characters remaining to replace the tags with actual values while sending SMS.";
    }
    else {
      this.errormessage = ""
    }
    if (this.characterlength <= 160) {
      this.smscount = 1;
    }
    else if (this.characterlength >= 160 && this.characterlength <= 306) {
      this.smscount = 2;
    }
    else if (this.characterlength >= 306 && this.characterlength <= 459) {
      this.smscount = 3;
    }
    else {
      this.smscount = 4;
    }
  }
  submitform() {
    console.log(this.smsForm)
    let bussinesstemplate: BusinessTemplateModel = this.smsForm.value;
    if (!this.smsForm.invalid) {
      this.blockUI.start();
      this.submitting = true;
      bussinesstemplate.businessTemplateLocations = this.bussinesstemplate.businessTemplateLocations;
      bussinesstemplate.subject = '';
      bussinesstemplate.type = 3;
      if (this.addItem) {
        this.settingsService.createbusinesstemplate(bussinesstemplate).subscribe(d => {
          this.submitting = false;
          if (this.duplicateform == 'Duplicate') {
            this.settingsService.sharedData = 'Your template has been duplicated successfully';
          }
          else {
            this.settingsService.sharedData = 'Your template has been created successfully';
          }
          this.router.navigate(['/settings/smstemplate']);
          this.blockUI.stop();
          this.itemid = d;
          this.addItem = false;
        }, error => {
          if (this.duplicateform == 'Duplicate') {
            this.displayErrorMessage("The name entered already exists, please try a different name!");
          }
          else {

            this.displayErrorMessage("Failed to save Template, please try again later.");

          }
          this.submitting = false;
          this.blockUI.stop();
        });
      }
      else {
        bussinesstemplate.id = this.itemid;
        this.settingsService.updatebusinesstemplate(bussinesstemplate).subscribe(d => {
          this.submitting = false;
          this.settingsService.sharedData = 'Your template has been updated successfully';
          this.router.navigate(['/settings/smstemplate']);
          this.blockUI.stop();
        }, error => {
          this.displayErrorMessage("Failed to edit, please try again later.");
          this.submitting = false;
          this.blockUI.stop();
        });
      }
    }
  }
  delete() {
    this.opened = true;
  }
  confirdelete(status, name) {
    if (name == 'delete') {
      if (status == "yes") {
        this.settingsService
          .deletebusinesstemplatesById(this.itemid)
          .subscribe(
            () => {
              this.settingsService.sharedData = 'Your template has been deleted successfully';
              this.router.navigate(['/settings/smstemplate/']);
            },
            () => {
              this.displayErrorMessage(
                "Failed to delete, please try again later."
              );
            }
          );
      }
    }
    else if (name == 'canceldata') {
      let bussinesstemplate: BusinessTemplateModel = this.smsForm.value;
      bussinesstemplate.businessTemplateLocations = this.bussinesstemplate.businessTemplateLocations;
      bussinesstemplate.subject = '';
      bussinesstemplate.type = 3;
      bussinesstemplate.id = this.itemid;
      this.settingsService.updatebusinesstemplate(bussinesstemplate).subscribe(d => {
        let locations: BusinessTemplateLocations[] = [];
        let locationSelections: LocationGridModel[] = this.smsForm.get("locationName").value;
        if (locationSelections) {
          locationSelections.forEach(l => {
            let m = new BusinessTemplateLocations();
            m.locationId = l.id;
            m.locationName = l.locationName;
            locations.push(m);
          });
        }
        bussinesstemplate.businessTemplateLocations = locations;
        bussinesstemplate.type = 3;
        this.openconfirmScreen = false;
        this.blockUI.start();
        this.router.navigate(['/settings/smstemplate']);
        this.blockUI.stop();
      }, error => {
        this.blockUI.stop();
      });
    }
    else {
      if (this.templateName != this.smsForm.get("name").value || this.templateDescription != this.smsForm.get("description").value || this.templatebody != this.smsForm.get("body").value || this.bussinesstemplate.businessTemplateLocations != this.bussinesstemplate.businessTemplateLocations) {
        let bussinesstemplate: BusinessTemplateModel = this.smsForm.value;
        bussinesstemplate.businessTemplateLocations = this.bussinesstemplate.businessTemplateLocations;
        bussinesstemplate.subject = '';
        bussinesstemplate.type = 3;
        bussinesstemplate.id = this.itemid;
        this.settingsService.updatebusinesstemplate(bussinesstemplate).subscribe(d => {
          this.submitting = false;
          this.addItem = true;
          this.smsForm.controls.name.setValue('');
          this.smsForm.controls.description.setValue(bussinesstemplate.description);
          this.smsForm.controls.body.setValue(bussinesstemplate.body);

          let locations: BusinessTemplateLocations[] = [];
          let locationSelections: LocationGridModel[] = this.smsForm.get("locationName").value;
          if (locationSelections) {
            locationSelections.forEach(l => {
              let m = new BusinessTemplateLocations();
              m.locationId = l.id;
              m.locationName = l.locationName;
              locations.push(m);
            });
          }
          bussinesstemplate.businessTemplateLocations = locations;
          bussinesstemplate.type = 3;
          this.duplicateform = "Duplicate"
          this.openedduplicate = false;
          this.blockUI.stop();
        }, error => {
          this.blockUI.stop();
        });
      }
      else {
        let bussinesstemplate: BusinessTemplateModel = this.smsForm.value;
        this.smsForm.controls.name.setValue('');
        this.smsForm.controls.description.setValue(bussinesstemplate.description);
        this.smsForm.controls.body.setValue(bussinesstemplate.body);
        let locations: BusinessTemplateLocations[] = [];
        let locationSelections: LocationGridModel[] = this.smsForm.get("locationName").value;
        if (locationSelections) {
          locationSelections.forEach(l => {
            let m = new BusinessTemplateLocations();
            m.locationId = l.id;
            m.locationName = l.locationName;
            locations.push(m);
          });
        }
        bussinesstemplate.businessTemplateLocations = locations;
        bussinesstemplate.type = 2;
        this.addItem = true;
        this.duplicateform = "Duplicate"
        this.openedduplicate = false;
      }
    }
  }
  duplicate() {
    this.openedduplicate = true;
  }
  public close(status: string) {
    this.opened = false;
    this.openedduplicate = false;
    this.openconfirmScreen = false;
  }
  cancelForm() {
    this.openconfirmScreen = true;
  }
  confirmDelete(data) {
    if (data == 'yes') {
      this.blockUI.start();
      this.router.navigate(['/settings/smstemplate']);
      this.blockUI.stop();
      this.openconfirmScreen = false;
    }
    else {
      this.openconfirmScreen = false;
    }
  }
}
