import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import {
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDividerModule,
    MatMenuModule,
    MatListModule,
    MatTooltipModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSliderModule,
    MatCardModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IntlModule } from '@progress/kendo-angular-intl';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { BlockUIModule } from 'ng-block-ui';
import { OrderModule } from 'ngx-order-pipe';
import { TimePickerModule, DateInputsModule, CalendarModule } from '@progress/kendo-angular-dateinputs';
import { SettingsRoutingModule } from './settings-routing.module';
import { PersonalizeComponent } from './personalize/personalize.component';
import { PatientPersonalizeComponent } from './personalize/patient-personalize/patient-personalize.component';
import { TreatmentNotesTemplateComponent } from './treatment-notes-template/treatment-notes-template.component';
import { AddTreatmentNotesTemplateComponent } from './treatment-notes-template/add-treatment-notes-template/add-treatment-notes-template.component';
import { TemplateLibraryComponent } from './treatment-notes-template/template-library/template-library.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ImageFileUploadComponent } from './treatment-notes-template/image-file-upload/image-file-upload.component';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { NgxImageCompressService } from 'ngx-image-compress';
import { TreatmentNotesService } from '../../../services/app.treatmentnotes.services';
import { TemplateLibraryBySpecialityComponent } from './treatment-notes-template/template-library-byspeciality/template-library-byspeciality.component';
import { TreatmentNotesPreviewComponent } from './treatment-notes-template/treatment-notes-preview/treatment-notes-preview.component';
import { EmailtemplateComponent } from './emailtemplate/emailtemplate.component';
import { LettertemplateComponent } from './lettertemplate/lettertemplate.component';
import { AddlettertemplateComponent } from './lettertemplate/addlettertemplate/addlettertemplate.component';
import { AddemailtemplateComponent } from './emailtemplate/addemailtemplate/addemailtemplate.component';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { CKEditorModule } from 'ckeditor4-angular';
import { YesNoPipe } from '../../../pipe/yes-no.pipe';
import { PopupModule } from '@progress/kendo-angular-popup';
import { EditorModule } from '@progress/kendo-angular-editor';
import { SmstemplateComponent } from './smstemplate/smstemplate.component';
import { AddsmstemplateComponent } from './smstemplate/addsmstemplate/addsmstemplate.component';
import '@vaadin/vaadin-time-picker/vaadin-time-picker.js';
import {ApplicationDataComponent} from './application-data/application-data.component';
import {AppCommonModule} from '../common/app-common.module';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {MultiSelectModule} from '@progress/kendo-angular-dropdowns';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {ManagingModule} from '../../module/managing.module';
import {RolesComponent} from './roles/roles.component';
import {AddRolesComponent} from './roles/add-roles/add-roles.component';
import {CharacterLimitModule} from '../../../directive/character-limit/character-limit.module';
import {AppKendoMultiSelectModule} from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import {LetterTagListModule} from '../../../shared/letter-tag-list/letter-tag-list.module';
import {TextOnlyModule} from '../../../directive/text-only/text-only.module';
import {AccessDirectiveModule} from '../../../shared/directives/access-directive/access-directive.module';
import {AppTimePickerModule} from '../common/time-picker/app-time-picker.module';
import { SmsSettingsComponent } from './sms-settings/sms-settings.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import {OnlyNumberModule} from '../../../directive/only-number/only-number.module';
import { ReminderSettingsComponent } from './reminder-settings/reminder-settings.component';
import {SharedModule} from '@progress/kendo-angular-grid';

@NgModule({
  declarations: [
    SettingsComponent,
    PersonalizeComponent,
    PatientPersonalizeComponent,
    TreatmentNotesTemplateComponent,
    AddTreatmentNotesTemplateComponent,
    TemplateLibraryComponent,
    ImageFileUploadComponent,
    TemplateLibraryBySpecialityComponent,
    TreatmentNotesPreviewComponent,
    EmailtemplateComponent,
    LettertemplateComponent,
    AddlettertemplateComponent,
    AddemailtemplateComponent,
    YesNoPipe,
    SmstemplateComponent,
    AddsmstemplateComponent,
    ApplicationDataComponent,
    RolesComponent,
    AddRolesComponent,
    SmsSettingsComponent,
    EmailSettingsComponent,
    ReminderSettingsComponent
  ],
    imports: [
        CommonModule,
        AppCommonModule,
        SharedContentFullLayoutModule,
        ManagingModule,
        SettingsRoutingModule,
        InputsModule,
        MultiSelectModule,
        DialogModule,
        MatListModule,
        MatSelectModule,
        MatSliderModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        MatRadioModule,
        MatTableModule,
        MatProgressSpinnerModule,
        DateInputsModule,
        CalendarModule,
        TimePickerModule,
        BlockUIModule.forRoot(),
        MatDialogModule,
        LayoutModule,
        DragDropModule,
        UploadsModule,
        TreeViewModule,
        CKEditorModule,
        PopupModule,
        EditorModule,
        CharacterLimitModule,
        TextOnlyModule,
        AppKendoMultiSelectModule,
        LetterTagListModule,
        AccessDirectiveModule,
        AppTimePickerModule,
        MatCardModule,
        OnlyNumberModule,
        SharedModule
    ],
  bootstrap: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  providers: [NgxImageCompressService, TreatmentNotesService]
})
export class SettingsModule { }
