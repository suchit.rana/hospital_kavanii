export interface EmailSettings {
    id: string,
    parentBusinessId: string,
    locationId: string,
    location: {
        id?: string,
        emailSettingsId?: string,
        locationId: string
    }[],
    email: string,
    displayName: string,
    status: boolean
}
