export interface SmsSettings {
    id: string,
    parentBusinessId: string,
    clientId: number,
    apiKey: string,
    apiSecret: string,
    autoRecharge: boolean,
    rechargeSmsCount: number,
    rechargeBelowSmsCount: number,
    emailAlert: boolean,
    emailAlertBelowSmsCount: number,
    balanceSms: number,
    status: boolean,
}
