import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppState} from 'src/app/app.state';
import {BaseGridComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {TreatmentNotesService} from 'src/app/services/app.treatmentnotes.services';
import {MatButtonToggleChange} from '@angular/material';
import {MessageType} from 'src/app/models/app.misc';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-treatment-notes-template',
  templateUrl: './treatment-notes-template.component.html',
  styleUrls: ['./treatment-notes-template.component.css'],
})
export class TreatmentNotesTemplateComponent
  extends BaseGridComponent
  implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;

  apperance: string = 'outline';
  @BlockUI() blockUI: NgBlockUI;

  treatmentNotesTitle = '';
  treatmentNotesRouteLink = '';
  treatmentNotesRouteName = '';
  treatmentNotesDescription = '';
  isLoading: boolean;
  treatmentNotesShow: boolean;
  status: string = 'active';
  displaySuccessMessage: any;
  messageType = MessageType;

  constructor(
    protected router: Router,
    public appState: AppState,
    private treatmentNotesService: TreatmentNotesService
  ) {
    super();
  }

  ngOnInit() {

    this.appState.selectedUserLocationIdState.subscribe((locationId) => {
      this.populateLanding();
      this.router.navigate(['/settings/treatmentnotestemplate']);
    });

    this.populateLanding();
    this.displayMessage();
  }

  populateLanding() {
    this.blockUI.start();
    this.treatmentNotesService
      .getAllTreatmentNotesTemplateByLocationId(
        this.appState.selectedUserLocation.id
      )
      .subscribe((data) => {
        if (data.length === 0) {
          this.treatmentNotesTitle = 'You haven\'t added a Template';
          this.treatmentNotesRouteLink = '/settings/treatmentnotestemplate/add';
          this.treatmentNotesRouteName = 'Add Template';
          this.treatmentNotesDescription = 'Template is';
          this.treatmentNotesShow = false;
        } else {
          // data['isStatus'] = data.status;
          this.treatmentNotesShow = true;
          this.treatmentNotesTitle = '';

          this.gridData = data;
          this.loadItems('templateName');
        }
        this.blockUI.stop();
      });
  }

  /*
    treatmentNotesActiveChanged(event: MatButtonToggleChange) {
      if (event.value === "active") {
        this.treatmentNotesTemplates = this.treatmentNotesTemplateAll.filter(
          (x) => x.isStatus === true
        );
      } else {
        this.treatmentNotesTemplates = this.treatmentNotesTemplateAll.filter(
          (x) => x.isStatus === false
        );
      }
    }*/

  treatmentNotesActiveChanged(event: MatButtonToggleChange) {
    if (event.value == 'active') {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }
    this.loadItems();
  }

  setActive() {
    this.setActiveFilter();
  }

  setInactive() {
    this.setInactiveFilter();
  }

  displayMessage() {
    if (
      this.treatmentNotesService.messageConfirmation !== undefined &&
      this.treatmentNotesService.messageConfirmation !== ''
    ) {

      this.displaySuccessMessage = this.treatmentNotesService.messageConfirmation;
      // this.treatmentNotesService.messageConfirmation = "";

      setInterval(
        (a) => {
          this.displaySuccessMessage = '';
          this.treatmentNotesService.messageConfirmation = '';
        },
        10000,
        []
      );
    }
  }
}
