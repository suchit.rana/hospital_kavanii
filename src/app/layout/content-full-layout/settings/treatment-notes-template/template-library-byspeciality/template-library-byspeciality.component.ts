import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Location } from "@angular/common";
import { TreatmentNotesService } from "src/app/services/app.treatmentnotes.services";
import { BaseGridComponent } from "src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component";
import {
  AddTreatmentNotesTemplateModel,
  TotalTreatmentNotesTemplateLibraryModel,
  TreatmentNotesModel,
  TreatmentNotesSpecialityModel,
  TreatmentNotesTemplateLibraryModel,
} from "src/app/models/app.treatmentnotes.model";
import { ActivatedRoute, Router } from "@angular/router";
import { FormControl, FormGroup } from "@angular/forms";
import { SettingsService } from "src/app/services/app.settings.service";
import { AppState } from "src/app/app.state";
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: "app-template-library-byspeciality",
  templateUrl: "./template-library-byspeciality.component.html",
  styleUrls: [
    "../add-treatment-notes-template/add-treatment-notes-template.component.css",
  ],
})
export class TemplateLibraryBySpecialityComponent
  extends BaseGridComponent
  implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;

  apperance = "outline";
  @BlockUI() blockUI: NgBlockUI;
  totalTreatmentNotesTemplateLibrary: TotalTreatmentNotesTemplateLibraryModel[];
  displaySuccessMessage: any;
  opened: boolean;
  treatmentNotesTemplateLibraryId: string;
  heading: string;

  treatmentNotesLibraryForm: FormGroup = new FormGroup({
    templateName: new FormControl(""),
    specialityName: new FormControl(""),
  });

  public categoryItems: Array<string> = [
    "Most Popular",
    "Newest",
    "Oldest",
    "A-Z",
    "Z-A",
  ];
  selectedCategory: string;
  selectedSpeciality: any;
  treatmentNotesTemplateLibrary: TreatmentNotesTemplateLibraryModel[];
  treatmentNotesTemplateLibraryFilter: TreatmentNotesTemplateLibraryModel[];
  selectedTreatmentNotesTemplateLibrary: TreatmentNotesTemplateLibraryModel;
  treatmentNotes: TreatmentNotesModel;
  public specialityData: any[] = [];
  selectedOptions: any;

  constructor(
    protected router: Router,
    protected appState: AppState,
    public location: Location,
    private _route: ActivatedRoute,
    private settingsService: SettingsService,
    private treatmentNotesService: TreatmentNotesService
  ) {
    super();
  }

  ngOnInit() {
    this.appState.selectedUserLocationIdState.subscribe((locationId) => {
      this.router.navigate(["/settings/templatelibrary"]);
    });
    this.selectedCategory = "Most Popular";
    this.treatmentNotes = new TreatmentNotesModel();
    this.treatmentNotes.isLibrary = false;
    this.opened = false;
    this.treatmentNotesTemplateLibraryId = "";
    this.heading = "";
    this.settingsService.getAllSpecialties().subscribe((d) => {
      this.specialityData = d;
      console.log(d);
    });
    this.treatmentNotesService
      .getTotalTemplatesBySpeciality()
      .subscribe((d) => {
        this.totalTreatmentNotesTemplateLibrary = d.filter(
          (x) => x.specialityName !== "All specialities"
        );
        this._route.params.subscribe((params) => {
          if (params.specialityId) {
            this.totalTreatmentNotesTemplateLibrary.map((x) => {
              if (String(x.specialityId) === String(params.specialityId)) {
                this.selectedSpeciality = x;
                this.heading = x.specialityName;
              }
            });

            this.treatmentNotesService
              .getAllTreatmentNotesTemplateLibraryBySpecialityId(
                params.specialityId
              )
              .subscribe((data) => {
                this.treatmentNotesTemplateLibrary = data;

                this.treatmentNotesTemplateLibrary.map((x) => {
                  x.cssName = "no-active";
                });
                this.treatmentNotesTemplateLibraryFilter = this.treatmentNotesTemplateLibrary;
                this.handleCategory("Most Popular");
              });
          }
        });
      });
  }

  selectTemplate(id: string) {
    this.treatmentNotes.specialityPreview = "";
    this.treatmentNotesService
      .getTreatmentNotesTemplateLibraryById(id)
      .subscribe((data) => {
        this.treatmentNotes.templateName = data.templateName;

        data.treatmentNotesTemplateLibrarySpeciality.map((x) => {
          this.treatmentNotes.specialityPreview =
            this.treatmentNotes.specialityPreview + x.specialityName + ", ";
        });

        this.treatmentNotes.specialityPreview = this.treatmentNotes.specialityPreview.substring(
          0,
          this.treatmentNotes.specialityPreview.length - 2
        );

        this.treatmentNotesTemplateLibraryId = data.id;
        this.treatmentNotes.itemTypeModel = JSON.parse(data.itemType);

        this.selectedTreatmentNotesTemplateLibrary = this.treatmentNotesTemplateLibrary.find(
          (x) => x.id === id
        );
      });

    this.treatmentNotesTemplateLibrary.map((x) => {
      if (x.id === id) {
        x.cssName = "active";
      } else {
        x.cssName = "no-active";
      }
    });
    this.treatmentNotesTemplateLibraryFilter = this.treatmentNotesTemplateLibrary;
  }

  handleSpeciality($event: { specialityName: string; specialityId: string }) {
    this.treatmentNotes = new TreatmentNotesModel();
    this.heading = $event.specialityName;
    this.treatmentNotesService
      .getAllTreatmentNotesTemplateLibraryBySpecialityId($event.specialityId)
      .subscribe((data) => {
        this.treatmentNotesTemplateLibrary = data;
        this.treatmentNotesTemplateLibrary.map((x) => {
          x.cssName = "no-active";
        });
        this.treatmentNotesTemplateLibraryFilter = this.treatmentNotesTemplateLibrary;
      });
  }

  handleCategory($event) {
    this.treatmentNotesTemplateLibraryFilter.sort((a, b) => {
      if ($event === "A-Z") {
        return a.templateName.localeCompare(b.templateName);
      } else if ($event === "Z-A") {
        return b.templateName.localeCompare(a.templateName);
      } else if ($event === "Most Popular") {
        return b.usedCount < a.usedCount ? -1 : 1;
      } else if ($event === "Newest") {
        return b.createdDate > a.createdDate ? 1 : -1;
      } else if ($event === "Oldest") {
        return b.createdDate < a.createdDate ? 1 : -1;
      }
    });
  }

  onClickAddToTemplateList() {
    this.opened = false;

    const el = document.getElementById("heading");
    const templateLibraryModel: AddTreatmentNotesTemplateModel = this
      .treatmentNotesLibraryForm.value;

    const treatmentNotesSpecialityModel: TreatmentNotesSpecialityModel[] = [];

    templateLibraryModel.specialityName.forEach(
      (s: { id: string; specialityName: string }) => {
        const m = new TreatmentNotesSpecialityModel();
        m.specialityId = s.id;
        m.locationId = this.appState.selectedUserLocation.id;
        m.specialityName = s.specialityName  + " (" + this.appState.selectedUserLocation.locationName + ")";;
        treatmentNotesSpecialityModel.push(m);
      }
    );

    templateLibraryModel.locationId = this.appState.selectedUserLocation.id;
    templateLibraryModel.treatmentNotesTemplateSpeciality = treatmentNotesSpecialityModel;
    templateLibraryModel.treatmentNotesTemplateLibraryId = this.treatmentNotesTemplateLibraryId;

    if (!this.treatmentNotesLibraryForm.invalid) {
      this.blockUI.start();

      this.treatmentNotesService
        .addToTreatmentNotesTemplate(templateLibraryModel)
        .subscribe(() => {
          this.treatmentNotesService.messageConfirmation =
            "Your treatment notes template is added successfully.";
          this.router.navigate(["/settings/treatmentnotestemplate"]);
          el.scrollIntoView();
          this.blockUI.stop();
        });
    }
  }

  openPopup() {
    this.opened = true;
    this.treatmentNotesLibraryForm.reset();
    this.treatmentNotesLibraryForm
      .get("templateName")
      .patchValue(this.treatmentNotes.templateName);
  }

  public onDialogClose() {
    this.opened = false;
  }

  templateNameChange(value: string) {
    this.treatmentNotesTemplateLibraryFilter = this.treatmentNotesTemplateLibrary.filter(
      (x) => x.templateName.toLowerCase().indexOf(value.toLowerCase()) > -1
    );
  }
}
