import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Location } from "@angular/common";
import { TreatmentNotesService } from "src/app/services/app.treatmentnotes.services";
import { BaseGridComponent } from "src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component";
import { TotalTreatmentNotesTemplateLibraryModel } from "src/app/models/app.treatmentnotes.model";
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: "app-template-library",
  templateUrl: "./template-library.component.html",
  styleUrls: ["./template-library.component.css"],
})
export class TemplateLibraryComponent
  extends BaseGridComponent
  implements OnInit {
  moduleName = RP_MODULE_MAP.settings_templates;

  apperance = "outline";
  @BlockUI() blockUI: NgBlockUI;
  totalTreatmentNotesTemplateLibraryModel: TotalTreatmentNotesTemplateLibraryModel[];
  displaySuccessMessage: any;

  constructor(
    public location: Location,
    private treatmentNotesService: TreatmentNotesService
  ) {
    super();
  }

  ngOnInit() {
    this.displayMessage();
    this.treatmentNotesService
      .getTotalTemplatesBySpeciality()
      .subscribe((d) => {
        this.totalTreatmentNotesTemplateLibraryModel = d;
      });
  }

  displayMessage() {
    if (
      this.treatmentNotesService.messageConfirmation !== undefined &&
      this.treatmentNotesService.messageConfirmation !== ""
    ) {
      this.displaySuccessMessage(this.treatmentNotesService.messageConfirmation);
      this.treatmentNotesService.messageConfirmation = "";
    }
  }
}
