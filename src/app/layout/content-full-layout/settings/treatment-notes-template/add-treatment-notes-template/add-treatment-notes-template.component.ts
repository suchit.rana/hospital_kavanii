import {ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {Location} from '@angular/common';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {
    ChartModel,
    CheckBoxModel,
    DateTimeModel,
    DropdownModel,
    ItemTypeModel,
    KeySubjectModel,
    KeyValueModel,
    LocationModel,
    NumberModel,
    RadioButtonModel,
    TableModel,
    TotalTreatmentNotesTemplateLibraryModel,
    TreatmentNotesLocationModel,
    TreatmentNotesModel,
    TreatmentNotesSpecialityModel,
    TreatmentNotesTemplateLibraryModel,
    TreatmentNotesTemplateLibrarySpecialityModel,
    VitalsModel,
} from 'src/app/models/app.treatmentnotes.model';
import {SettingsService} from 'src/app/services/app.settings.service';
import {BusinessService} from 'src/app/services/app.business.service';
import {AppState} from 'src/app/app.state';
import {FileRestrictions} from '@progress/kendo-angular-upload';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {NgxImageCompressService} from 'ngx-image-compress';
import {TreatmentNotesService} from 'src/app/services/app.treatmentnotes.services';
import {ActivatedRoute, Router} from '@angular/router';
import {groupBy, GroupResult} from '@progress/kendo-data-query';
import {TreatmentNotesPreviewComponent} from '../treatment-notes-preview/treatment-notes-preview.component';
import {FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

// import { Console } from "console";

@Component({
    selector: 'app-add-treatment-notes-template',
    templateUrl: './add-treatment-notes-template.component.html',
    styleUrls: ['./add-treatment-notes-template.component.css'],

})
export class AddTreatmentNotesTemplateComponent
    extends BaseItemComponent
    implements OnInit {
    moduleName = RP_MODULE_MAP.settings_templates;

    @BlockUI() blockUI: NgBlockUI;
    @ViewChild(ImageUploadComponent, {static: true})
    image: ImageUploadComponent;
    @ViewChild(TreatmentNotesPreviewComponent, {static: false})
    childTreatmentNotesPreview: TreatmentNotesPreviewComponent;
    apperance = 'outline';
    showChild: boolean = true;
    public opened = false;
    panelOpenState = true;
    opendeleteform: boolean;
    public openedDuplicate = false;
    public value = false;
    maxvalidation: boolean = false;
    defautmessage: boolean = false;
    maxDuplicatemessage: boolean = false;
    defautDuplicatemessage: boolean = false;
    exportImageData: any;
    defaultItem: any = 'placeholder';
    keyValue: any;
    selectedUnit: any = '';
    isLable: boolean = false;
    exportDuplicateImageData: any;
    items: FormArray;
    itemTitle: any;
    selectedId: any;
    defValue: any = 1;
    controlId: any;
    isDelete: boolean = false;
    public placeHolder: any = {id: '', title: 'Item Type'};
    matcher = new MyErrorStateMatcher();
    labelValue: any;
    itemTypes = [
        {
            id: 1,
            title: 'Heading',
            description: 'Section Heading',
            icon: 'local_offer',
            json: '',
        },
        {
            id: 2,
            title: 'Date Time',
            description: '(Field to enter date Time)',
            icon: 'date_range',
            json: '',
        },
        {
            id: 3,
            title: 'Text',
            description: 'A plain text area to type notes',
            icon: 'text_format',
            json: '',
        },
        {
            id: 4,
            title: 'Number',
            description: 'Field to enter a single number',
            icon: 'looks_one',
            json: '',
        },
        {
            id: 5,
            title: 'Dropdown',
            description:
                'Select one option from a list of options in a dropdown menu',
            icon: 'arrow_circle_down',
            json: '',
        },
        {
            id: 6,
            title: 'Radio Buttons',
            description: 'Select one option from a list of options in a radio button',
            icon: 'radio_button_checked',
            json: '',
        },
        {
            id: 7,
            title: 'Check Boxes',
            description:
                'Select one or more checkboxes and optionally add a note to each',
            icon: 'check_box',
            json: '',
        },
        {
            id: 8,
            title: 'Range or Scale',
            description:
                'Customizable range / scale / clider allows you to choose from a range of values',
            icon: 'linear_scale',
            json: '',
        },
        {
            id: 9,
            title: 'Option List',
            description: 'Select a choice from list of subjects',
            icon: 'reorder',
            json: '',
        },
        {
            id: 10,
            title: 'Table',
            description:
                'Set of facts or figures systematically displayed in rows and column',
            icon: 'view_module',
            json: '',
        },
        {
            id: 11,
            title: 'Chart',
            description:
                'Draw or type notes on the provided Body chart or any image of your choice',
            icon: 'accessibility',
            json: '',
        },
        {
            id: 12,
            title: 'File Attachment',
            description:
                'Upload any type of file,with a preview of most common file types',
            icon: 'attach_file',
            json: '',
        },
        {
            id: 13,
            title: 'Instruction',
            description:
                'A plain text area to type instructions.It will not appear in printed or exported Treatment Note',
            icon: 'toc',
            json: '',
        },
        {
            id: 14,
            title: 'Chief Complaint',
            description: 'Record the chief complaint or diagnosis',
            icon: 'content_paste',
            json: '',
        },
        {
            id: 15,
            title: 'Vitals',
            description:
                'Record weight, height, blood pressure, respiratory rate, and calculate BMI.',
            icon: 'receipt_long',
            json: '',
        },
        {
            id: 16,
            title: 'Spine',
            description: 'Checkboxes for each joint, sketch on the spine diagram',
            icon: 'more_vert',
            json: '',
        },
        {
            id: 17,
            title: 'Optical Measurements',
            description: 'Optical Measurements',
            icon: 'visibility',
            json: '',
        },
    ];
    selectedValue: any;
    optionListStyles = [
        {
            id: 1,
            type: 'Button',
        },
        {
            id: 2,
            type: 'Checkbox',
        },
        {
            id: 3,
            type: 'Dropdown',
        },
        {
            id: 4,
            type: 'Radio button',
        },
    ];

    metric = [
        {
            id: 1,
            name: 'Body Temp (C)',
            isChecked: true
        },
        {
            id: 2,
            name: 'Pulse (BPM)',
            isChecked: true
        },
        {
            id: 3,
            name: 'Resp. Rate (bpm)',
            isChecked: true
        },
        {
            id: 4,
            name: 'BP (mm/ Hg)',
            isChecked: true
        },
        {
            id: 5,
            name: 'Weight (Kg)',
            isChecked: true
        },
        {
            id: 6,
            name: 'Height (cm)',
            isChecked: true
        },
        {
            id: 7,
            name: 'BMI (kg/m2)',
            isChecked: true
        },
    ];
    imperial = [
        {
            id: 1,
            name: 'Body Temp (F)',
            isChecked: true
        },
        {
            id: 2,
            name: 'Pulse (BPM)',
            isChecked: true
        },
        {
            id: 3,
            name: 'Resp. Rate (bpm)',
            isChecked: true
        },
        {
            id: 4,
            name: 'BP (mm/ Hg)',
            isChecked: true
        },
        {
            id: 5,
            name: 'Weight (lbs)',
            isChecked: true
        },
        {
            id: 6,
            name: 'Height (in)',
            isChecked: true
        },
        {
            id: 7,
            name: 'BMI (lb/in2)',
            isChecked: true
        },
    ];

    constructor(
        protected appState: AppState,
        public location: Location,
        protected router: Router,
        private settingsService: SettingsService,
        private _route: ActivatedRoute,
        private businessService: BusinessService,
        private imageCompress: NgxImageCompressService,
        private treatmentNotesService: TreatmentNotesService,
        private fb: FormBuilder,
        private cdRef: ChangeDetectorRef
    ) {
        super(location);
    }

    treatmentNotesForm: FormGroup = new FormGroup({
        templateName: new FormControl('', Validators.required),
        allowLocation: new FormControl(''),
        templateLocation: new FormControl('', Validators.required),
        userName: new FormControl('', Validators.required),
        businessName: new FormControl('', Validators.required),
        specialityName: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
    });

    treatmentNotesLibraryForm: FormGroup = new FormGroup({
        // templateName:new FormControl("",Validators.required),
        // allowLocation:new FormControl(""),
        // templateLocation:new FormControl("",Validators.required),
        // userName: new FormControl("",Validators.required),
        // businessName: new FormControl("",Validators.required),
        // specialityName: new FormControl("",Validators.required),
        // description: new FormControl("",Validators.required),
        itemDropdown: new FormControl('', Validators.required),
        items: this.fb.array([])

    });


    uploadSaveUrl = 'saveUrl'; // should represent an actual API endpoint
    uploadRemoveUrl = 'removeUrl'; // should represent an actual API endpoint
    itemTypesData = [];
    rangeOrScale = [];
    rangeOrScaleDuplicate = [];
    myRestrictions: FileRestrictions = {
        allowedExtensions: ['.jpg', '.png', '.pdf'],
    };

    maxTableRowsColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    treatmentNotes: TreatmentNotesModel;
    treatmentNotesTemplateLibrary: TreatmentNotesTemplateLibraryModel;
    treatmentNotesTemplateLibrarySpeciality: TreatmentNotesTemplateLibrarySpecialityModel;
    tempItemTemplates: ItemTypeModel[] = [];
    templateName: string;
    locationName: any;
    specialityName: any;
    public specialityData: any[] = [];
    public groupedSpecialityData: GroupResult[];
    locationData: LocationModel[] = [];
    public dateValue: Date;
    public dateDuplicateValue: Date;
    // locationAllList: LocationModel[];
    specialityAllList: any;
    selectedSpeciality: any;
    unitOfMeasurement: { id: number; name: string; isChecked: boolean }[];
    @Input() headerDescription = 'Default Image';
    tempTreatmentNotesTemplateSpeciality: TreatmentNotesSpecialityModel[];
    chartDuplicateSize: string;
    chartSize: string;
    isSave: boolean;
    isItemAdded: boolean;
    isMandatoryFilled: boolean;
    public openedDelete = true;
    totalTreatmentNotesTemplateLibraryModel: TotalTreatmentNotesTemplateLibraryModel[];
    isLoading: boolean = true;
    rangeOrScaleDuplicateMax: number;
    rangeOrScaleMax: number;
    deleteButton: boolean;
    isSubmitted: boolean;
    locationNameData: any;

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(
            this.treatmentNotes.itemTypeModel,
            event.previousIndex,
            event.currentIndex
        );
    }

    ngOnInit() {
        console.log('#################', this.unitOfMeasurement);

        this.appState.selectedUserLocationIdState.subscribe((locationId) => {
            this.router.navigate(['/settings/treatmentnotestemplate']);
        });

        this.isSave = false;
        this.isItemAdded = false;
        this.openedDelete = false;
        this.openedDuplicate = false;
        this.deleteButton = false;
        this.treatmentNotes = new TreatmentNotesModel();
        this.treatmentNotes.specialityPreview = '';
        this.treatmentNotesTemplateLibrary = new TreatmentNotesTemplateLibraryModel();
        this.treatmentNotes.isStatus = true;
        this.treatmentNotes.isLibrary = true;
        this.treatmentNotes.isAllowAllLocation = false;
        this.treatmentNotes.templateName = '';
        this.chartSize = this.chartDuplicateSize = 'Large';
        this.businessService
            .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
            .subscribe((data) => {
                const locations: any = [];
                data.map((d) => {
                    const m = new LocationModel();
                    m.locationId = d.id;
                    m.locationName = d.locationName;
                    locations.push(m);
                });
                this.locationData = [...this.locationData, ...locations];
                this.locationPopulate();
            });

        this.settingsService.getAllSpecialties().subscribe((specialties) => {
            this.specialityData = this.specialityAllList = specialties;
            this.specialityPopulate();
        });

        this._route.params.subscribe((params) => {
            if (params.treatmentNotesTemplateId) {
                this.blockUI.start();
                this.addItem = false;
                this.itemid = params.treatmentNotesTemplateId;
                this.deleteButton = true;

                this.treatmentNotesService
                    .getTreatmentNotesTemplateById(params.treatmentNotesTemplateId)
                    .subscribe((data) => {
                        this.treatmentNotes = data;

                        this.treatmentNotes.itemTypeModel = JSON.parse(
                            this.treatmentNotes.itemType
                        );
                        this.isItemAdded = true;
                        this.itemMandatoryCheck();
                        this.treatmentNotes.treatmentNotesTemplateLocation.forEach(element => {
                            this.locationNameData = element.locationName;
                        });
                        this.treatmentNotes.itemTypeModel.map((x) => {
                            this.onAddItem();
                            this.items = this.treatmentNotesLibraryForm.get('items') as FormArray;

                            if (x.chartImage && x.chartImage.image != '../../../assets/image 1.png') {
                                x.chartImage.tempImage = x.chartImage.image;
                                this.chartChange(x.id, this.chartSize);
                            } else {
                                x.chartImage.tempImage = '../../../assets/image 1.png';
                                this.chartChange(x.id, this.chartSize);
                            }

                            if (x.chartDuplicateImage.image != '../../../assets/dropfileimage.png') {
                                x.chartDuplicateImage.tempImage = x.chartDuplicateImage.image;
                                this.chartDuplicateChange(x.id, this.chartDuplicateSize);
                            } else {
                                x.chartDuplicateImage.tempImage = '../../../assets/image 1.png';
                                this.chartDuplicateChange(x.id, this.chartDuplicateSize);
                            }

                            if (x.dateTime.date !== undefined) {
                                const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
                                    (y) => y.id === x.id
                                );
                                this.treatmentNotes.itemTypeModel[
                                    itemTypeIndex
                                    ].dateTime.date = new Date(x.dateTime.date);
                                this.treatmentNotes.itemTypeModel[
                                    itemTypeIndex
                                    ].dateTime.time = new Date(x.dateTime.time);
                            }
                            this.treatmentNotes.isLibrary = true;

                            if (x.dateTimeDuplicate.date !== undefined) {
                                const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
                                    (y) => y.id === x.id
                                );
                                this.treatmentNotes.itemTypeModel[
                                    itemTypeIndex
                                    ].dateTimeDuplicate.date = new Date(x.dateTimeDuplicate.date);
                                this.treatmentNotes.itemTypeModel[
                                    itemTypeIndex
                                    ].dateTimeDuplicate.time = new Date(x.dateTimeDuplicate.time);
                            }
                        });
                        this.locationPopulate();
                        this.specialityPopulate();

                        // this.validate();
                        this.blockUI.stop();
                    });
            }
        });
        // this.onAddItem();
        this.itemTypesData = this.itemTypes;
    }

    //   ngAfterViewChecked()
    // {

    //   this.cdRef.detectChanges();
    // }


    specialityPopulate() {
        if (
            this.specialityAllList !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateLocation !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateLocation.length > 0
        ) {
            this.onSpecialityChange(
                this.treatmentNotes.treatmentNotesTemplateSpeciality
            );
            this.populateSpeciality(
                this.treatmentNotes.treatmentNotesTemplateLocation
            );
            //console.log(this.groupedSpecialityData);
            if (this.treatmentNotes.treatmentNotesTemplateSpeciality.length > 0) {
                this.treatmentNotes.specialityPreview = '';
                this.treatmentNotes.treatmentNotesTemplateSpeciality.map((x) => {
                    this.treatmentNotes.specialityPreview =
                        this.treatmentNotes.specialityPreview + x.specialityName + ', ';
                });

                this.treatmentNotes.specialityPreview = this.treatmentNotes.specialityPreview.substring(
                    0,
                    this.treatmentNotes.specialityPreview.length - 2
                );
            }
        }
    }

    locationPopulate() {
        const locationsModel: TreatmentNotesLocationModel[] = [];
        if (
            this.locationData !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateLocation !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateLocation.length > 0
        ) {
            this.treatmentNotes.treatmentNotesTemplateLocation.forEach((tl) => {
                const m = new TreatmentNotesLocationModel();
                m.locationId = tl.locationId;
                m.locationName = tl.locationName;
                locationsModel.push(m);
            });
        } else {
            this.locationData.forEach(
                (l: { locationId: string; locationName: string }) => {
                    if (this.appState.selectedUserLocation.id === l.locationId) {
                        const m = new TreatmentNotesLocationModel();
                        m.locationId = l.locationId;
                        m.locationName = l.locationName;
                        locationsModel.push(m);
                    }
                }
            );
        }
        locationsModel.forEach(location => {
            const l = this.treatmentNotes.treatmentNotesTemplateLocation.find(x => x.locationId === location.locationId);
            if (!l) {
                this.treatmentNotes.treatmentNotesTemplateLocation.push(location);
            }
        });
    }

    /*validate() {
    if (
      this.treatmentNotes.templateName !== "" &&
      this.treatmentNotes.treatmentNotesTemplateLocation.length > 0 &&
      this.treatmentNotes.treatmentNotesTemplateSpeciality.length > 0
    ) {
      this.isSave = true;
    } else {
      this.isSave = false;
    }
  }*/

    checkValidation() {
        for (var i = 0; i < this.items.length; i++) {
            if (this.items.controls[i].dirty == true) {
                console.log(this.items.controls[i]);

                this.items.controls[i].clearValidators();
                this.items.controls[i].updateValueAndValidity();
            }
        }
    }


    touch(event, i) {
        console.log(event, i);

        console.log(this.items.controls[i].get('lable').touched);
        // this.items.controls[i].get('lable').markAsTouched()
        // if(this.items.controls[i].get('lable').touched == true){
        //     console.log(this.items.controls[i].get('lable'))
        //     this.items.controls[i].get('lable').setValidators([Validators.required])
        //     this.items.controls[i].get('lable').updateValueAndValidity()
        //     // this.items.controls[i].get('lable').setErrors(Validators.required)
        // }

        // if(this.items.controls[i].get('lable').touched == false && this.labelValue){
        //   // this.items.controls[i].get('lable').markAsTouched()
        //   this.items.controls[i].get('lable').setErrors(Validators.required)
        // }


    }

    onAddItem() {
        console.log(this.itemTypes);
        this.isSubmitted = false;
        console.log(this.treatmentNotesLibraryForm);
        console.log(this.treatmentNotes.itemTypeModel);
        // this.checkValidation()
        console.log(this.items);

        // if(this.controlId != undefined){
        //   if(this.items.controls[this.controlId].get('lable').dirty == true ||
        //     this.items.controls[this.controlId].get('itemDropdown').dirty == true){
        //     console.log("*********************")
        //     this.items.controls[this.controlId].get('lable').clearValidators();
        //     this.items.controls[this.controlId].get('lable').updateValueAndValidity();

        //     this.items.controls[this.controlId].get('itemDropdown').clearValidators();
        //     this.items.controls[this.controlId].get('itemDropdown').updateValueAndValidity();

        //     // this.treatmentNotesLibraryForm.markAsUntouched();
        //     console.log(this.treatmentNotesLibraryForm)
        //   }

        // }


        this.items = this.treatmentNotesLibraryForm.get('items') as FormArray;

        //  this.treatmentNotesLibraryForm.controls.items.status = 'VALID';
        this.items.push(this.fb.group({
            itemDropdown: ['', [Validators.required]],
            lable: ['', [Validators.required]],
            duplicateLabel: [],
            date: ['', [Validators.required]],
            time: ['', [Validators.required]],
            duplicatedate: [],
            duplicatetime: [],
            minNumber: ['', [Validators.required]],
            duplicateminNumber: [],
            maxNumber: ['', [Validators.required]],
            duplicatemaxNumber: [],
            defaultNumber: ['', [Validators.required]],
            duplicatedefaultNumber: [],
            dropdown: ['', [Validators.required]],
            duplicatedropdown: [],
            radiobutton: ['', [Validators.required]],
            duplicateradiobutton: [],
            checkbox: ['', [Validators.required]],
            duplicatecheckbox: [],
            minRange: ['', [Validators.required]],
            duplicateminRange: [],
            maxRange: ['', [Validators.required]],
            duplicatemaxRange: [],
            defaultRange: ['', [Validators.required]],
            duplicatedefaultRange: [],
            subjectinput: ['', [Validators.required]],
            duplicatesubjectinput: [],
            choiceinput: ['', [Validators.required]],
            duplicatechoiceinput: [],
            row: ['', [Validators.required]],
            column: ['', [Validators.required]],
            rowheader: ['', [Validators.required]],
            columnheader: ['', [Validators.required]],
            chartsize: ['', [Validators.required]],
            vitals: ['', [Validators.required]],
        }));
        console.log('items', this.items);
        console.log(this.treatmentNotesLibraryForm.value);
        this.isItemAdded = true;

        // if (this.treatmentNotes.itemTypeModel.length === 0) {
        //   const it = this.defaultItemType(this.treatmentNotes.itemTypeModel);
        //   this.treatmentNotes.itemTypeModel.push(it);
        //   this.isMandatoryFilled = false;
        //   console.log(this.treatmentNotes.itemTypeModel)

        // } else {
        if (this.addItem) {
            console.log(this.addItem);
            this.resetOpenFlag();
            const it = this.defaultItemType(this.treatmentNotes.itemTypeModel);
            this.treatmentNotes.itemTypeModel.push(it);
            console.log(this.treatmentNotes.itemTypeModel);
        }

        // this.treatmentNotes.itemTypeModel.forEach(element => {
        //   this.selectedId = element.id;
        //   console.log(this.selectedId)
        // })

        // }
        console.log('4444444444444444444444444444444444', this.treatmentNotesLibraryForm);
    }

    get f() {
        return this.treatmentNotesLibraryForm.controls;
    }

    get item(): FormArray {
        return this.treatmentNotesLibraryForm.get('items') as FormArray;
    }


    createTreatmentNotes() {
        this.isSubmitted = true;
        const el = document.getElementById('heading');

        if (this.isSubmitted == true) {
            this.treatmentNotesLibraryForm.markAllAsTouched();
        }

        if (this.controlId != undefined && this.items.controls[this.controlId].get('vitals').value == '' && this.itemTitle == 'Vitals') {
            return;

        } else {
            if (this.controlId != undefined && this.items.controls[this.controlId].get('lable').value == '') {
                this.items.controls[this.controlId].get('lable').setErrors(Validators.required);
            }
            this.itemMandatoryCheck();
            this.items = this.treatmentNotesLibraryForm.get('items') as FormArray;
            const validCheck = this.treatmentNotes.itemTypeModel.filter(
                (x) => x.isValid === false
            );

            // if(validCheck.length == 0){
            //   console.log("jdfkfhj")
            //   this.treatmentNotesLibraryForm.markAllAsTouched()
            // }
            if (this.treatmentNotes.itemTypeModel.length == 0) {
                el.scrollIntoView();
                this.displayErrorMessage(
                    'Please add at least one item before saving the template'
                );
                return;
            } else {
                this.displayErrorMessage(
                    ''
                );
            }

            if (this.treatmentNotes.treatmentNotesTemplateLocation.length == 0 || this.treatmentNotes.treatmentNotesTemplateSpeciality.length == 0) {
                this.displayErrorMessage(
                    'Mandatory fields Required.'
                );
                return;
            } else {
                this.displayErrorMessage(
                    ''
                );
                if (this.treatmentNotes.itemTypeModel.length == 0) {
                    el.scrollIntoView();
                    this.displayErrorMessage(
                        'Please add at least one item before saving the template'
                    );
                    return;
                } else {
                    this.displayErrorMessage(
                        ''
                    );
                    if (validCheck !== undefined && validCheck.length > 0) {
                        el.scrollIntoView();
                        this.displayErrorMessage(
                            'Please provide input for all mandatory fields'
                        );
                        return;
                    }
                }
            }

            this.blockUI.start();
            this.treatmentNotes.itemType = JSON.stringify(
                this.treatmentNotes.itemTypeModel
            );


            if (this.addItem) {
                this.treatmentNotesService
                    .createTreatmentNotesTemplate(this.treatmentNotes)
                    .subscribe(
                        (data) => {
                            this.submitting = false;
                            this.treatmentNotesService.messageConfirmation =
                                'Treatment Notes Template added successfully.';
                            this.router.navigate(['/settings/treatmentnotestemplate']);
                            el.scrollIntoView();
                            this.blockUI.stop();
                        },
                        () => {
                            this.displayErrorMessage(
                                'Error occurred while adding Treatment Notes Template, please try again.'
                            );
                            this.submitting = false;
                            el.scrollIntoView();
                            this.blockUI.stop();
                        }
                    );
            } else {

                this.treatmentNotes.id = this.itemid;
                this.treatmentNotesService
                    .updateTreatmentNotesTemplate(this.treatmentNotes)
                    .subscribe(
                        () => {
                            this.submitting = false;
                            this.treatmentNotesService.messageConfirmation =
                                'Treatment Notes Template updated successfully.';
                            this.router.navigate(['/settings/treatmentnotestemplate']);
                            el.scrollIntoView();
                            this.blockUI.stop();
                        },
                        () => {
                            this.displayErrorMessage(
                                'Error occurred while updating Treatment Notes Template, please try again.'
                            );
                            this.submitting = false;
                            el.scrollIntoView();
                            this.blockUI.stop();
                        }
                    );
            }

        }

        this.treatmentNotesLibraryForm.markAsUntouched();

    }


    templateNameChange(value: string) {
        this.treatmentNotes.templateName = value;
    }

    openCopyTemplate() {
        this.openedDuplicate = true;
    }

    copyTemplate() {
        this.addItem = true;
        this.treatmentNotes.templateName = '';
        //this.treatmentNotes.id = '';
        this.openedDuplicate = false;
    }

    closeDuplicate() {
        this.openedDuplicate = false;
    }

    shareTemplate() {
        this.opened = true;
        this.businessService
            .getParentBusiness(this.appState.userProfile.parentBusinessId)
            .subscribe((data) => {
                const libraryModel = new TreatmentNotesTemplateLibraryModel();

                libraryModel.templateName = this.treatmentNotes.templateName;
                libraryModel.userName =
                    this.appState.UserProfile.firstName +
                    ' ' +
                    this.appState.UserProfile.lastName;
                libraryModel.businessName = data.businessName;
                this.treatmentNotesTemplateLibrary = libraryModel;
                this.treatmentNotesLibraryForm
                    .get('templateName')
                    .patchValue(this.treatmentNotes.templateName);
                this.treatmentNotesLibraryForm
                    .get('userName')
                    .patchValue(
                        this.appState.UserProfile.firstName +
                        ' ' +
                        this.appState.UserProfile.lastName
                    );
                this.treatmentNotesLibraryForm
                    .get('businessName')
                    .patchValue(data.businessName);
            });
    }

    public onDialogClose() {
        this.opened = false;
    }

    public onShareData() {
        const el = document.getElementById('heading');
        const templateLibraryModel: TreatmentNotesTemplateLibraryModel = this
            .treatmentNotesLibraryForm.value;
        templateLibraryModel.originalTreatmentNotesTemplateId = this.itemid;

        const treatmentNotesTemplateLibrarySpecialityModel: TreatmentNotesTemplateLibrarySpecialityModel[] = [];

        if (templateLibraryModel.specialityName !== '') {
            templateLibraryModel.specialityName.forEach((s) => {
                const m = new TreatmentNotesTemplateLibrarySpecialityModel();
                m.specialityId = s.specialityId;
                m.specialityName = s.specialityName;
                treatmentNotesTemplateLibrarySpecialityModel.push(m);
            });
        }

        templateLibraryModel.treatmentNotesTemplateLibrarySpeciality = treatmentNotesTemplateLibrarySpecialityModel;
        if (!this.treatmentNotesLibraryForm.invalid) {
            this.blockUI.start();
            this.submitting = true;

            this.treatmentNotesService
                .shareTreatmentNotesTemplate(templateLibraryModel)
                .subscribe(
                    () => {
                        this.submitting = false;
                        this.treatmentNotesService.messageConfirmation =
                            'Your treatment notes template is shared successfully.';
                        this.router.navigate(['/settings/templatelibrary']);
                        el.scrollIntoView();
                        this.blockUI.stop();
                    },
                    () => {
                        this.displayErrorMessage(
                            'Error occurred while adding Patient, please try again.'
                        );
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    }
                );
        }
    }

    allLocationChange($event) {
        this.treatmentNotes.isAllowAllLocation = $event;
        if ($event === true) {
            this.value = true;
            const locationsModel: TreatmentNotesLocationModel[] = [];
            this.locationData.forEach(
                (l: { locationId: string; locationName: string }) => {
                    const m = new TreatmentNotesLocationModel();
                    m.locationId = l.locationId;
                    m.locationName = l.locationName;
                    locationsModel.push(m);
                }
            );
            this.treatmentNotes.treatmentNotesTemplateLocation = locationsModel;
            this.populateSpeciality(locationsModel);
        } else {
            this.treatmentNotes.treatmentNotesTemplateLocation = [];
            this.treatmentNotes.treatmentNotesTemplateSpeciality = [];
            this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
        }
    }

    onLocationChange(locationData: any[]) {
        this.value = this.locationData && this.locationData.length == locationData.length;
        if (locationData.length === 0) {
            this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
            this.specialityData = [];
            this.treatmentNotes.treatmentNotesTemplateLocation = [];
        } else {
            this.value = false;

            const locationsModel: TreatmentNotesLocationModel[] = [];

            locationData.forEach(
                (l: { locationId: string; locationName: string }) => {
                    const m = new TreatmentNotesLocationModel();
                    m.locationId = l.locationId;
                    m.locationName = l.locationName;
                    locationsModel.push(m);
                }
            );
            this.treatmentNotes.treatmentNotesTemplateLocation = locationsModel;
        }
        this.populateSpeciality(locationData);
    }

    onSpecialityChange(specialityData: any[]) {
        if (specialityData.length > 0) {
            const specialityModel: TreatmentNotesSpecialityModel[] = [];
            this.treatmentNotes.specialityPreview = '';

            specialityData.forEach(
                (s: {
                    locationId: string;
                    specialityId: string;
                    specialityName: string;
                }) => {
                    this.treatmentNotes.specialityPreview =
                        this.treatmentNotes.specialityPreview + s.specialityName + ', ';
                    const m = new TreatmentNotesSpecialityModel();
                    m.locationId = s.locationId;
                    m.treatmentNotesId = this.itemid;
                    m.specialityId = s.specialityId;
                    m.idAndLocationId = s.specialityId + s.locationId;
                    m.specialityName =
                        this.specialityAllList.find((s) => s.id === m.specialityId)
                            .specialityName +
                        ' (' +
                        this.locationData.find((l) => l.locationId === m.locationId)
                            .locationName +
                        ')';
                    specialityModel.push(m);
                }
            );

            this.treatmentNotes.specialityPreview = this.treatmentNotes.specialityPreview.substring(
                0,
                this.treatmentNotes.specialityPreview.length - 2
            );
            this.treatmentNotes.treatmentNotesTemplateSpeciality = specialityModel;
            console.log(this.treatmentNotes.treatmentNotesTemplateSpeciality);
        } else {
            this.treatmentNotes.treatmentNotesTemplateSpeciality = [];
        }
        console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', this.treatmentNotesLibraryForm);
    }

    populateSpeciality(locationData: any[]) {
        this.specialityData = [];
        locationData.forEach((e) => {
            if (this.specialityAllList !== undefined) {
                this.specialityAllList.forEach((s: { specialityLocation: any[]; id: string; specialityName: string; isAllowAllLocation: boolean; }) => {
                        let sLocation;
                        if (s.isAllowAllLocation) {
                            sLocation = s;
                        } else {
                            sLocation = s.specialityLocation.find((x: { locationId: any }) => x.locationId === e.locationId);
                            ;
                        }

                        if (sLocation !== undefined) {
                            const specialityDataCheck = this.specialityData.find(
                                (x) => x.locationId === e.locationId && x.id === s.id
                            );

                            const sData = {
                                specialityId: '',
                                idAndLocationId: '',
                                specialityName: '',
                                locationId: '',
                                locationName: '',
                            };
                            sData.specialityId = s.id;
                            sData.idAndLocationId = s.id + e.locationId;
                            sData.specialityName =
                                s.specialityName + ' (' + e.locationName + ')';
                            sData.locationId = e.locationId;
                            sData.locationName = e.locationName;
                            if (specialityDataCheck === undefined) {
                                this.specialityData.push(sData);
                                this.groupedSpecialityData = groupBy(this.specialityData, [
                                    {field: 'locationName'},
                                ]);
                            }
                        }
                    }
                );
            }
        });
        let obj = [];
        this.treatmentNotes.treatmentNotesTemplateSpeciality.forEach((element, index) => {
            const valuefilterData = this.treatmentNotes.treatmentNotesTemplateLocation.filter((x) => x.locationId === element.locationId);
            if (valuefilterData.length > 0) {
                obj.push(element);
            }
        });

        this.treatmentNotes.treatmentNotesTemplateSpeciality = obj;
        this.treatmentNotesService
            .getTotalTemplatesBySpeciality()
            .subscribe((d) => {
                this.totalTreatmentNotesTemplateLibraryModel = d.filter(
                    (x) => x.specialityName !== 'All specialities'
                );
            });
    }

    closeDelete() {
        this.openedDelete = false;
    }

    openDelete() {
        this.openedDelete = true;
    }

    deleteTreatmentNotesTemplate() {
        this.treatmentNotesService
            .deleteTreatmentNotesTemplate(this.itemid)
            .subscribe(
                () => {
                    this.treatmentNotesService.messageConfirmation =
                        'Treatment Notes Template deleted successfully.';
                    this.router.navigate(['/settings/treatmentnotestemplate']);
                },
                () => {
                    this.displayErrorMessage(
                        'Error occurred while adding Treatment Notes Template, please try again.'
                    );
                }
            );
        this.openedDelete = true;
    }

    resetOpenFlag() {
        this.tempItemTemplates = [];
        const itModel = this.treatmentNotes.itemTypeModel;
        console.log(itModel);
        itModel.forEach((x) => {
            let model = new ItemTypeModel();
            model = x;
            model.isOpen = false;
            this.tempItemTemplates.push(model);
        });
        this.treatmentNotes.itemTypeModel = this.tempItemTemplates;
    }

    defaultItemType(itemTypeModel: ItemTypeModel[]): ItemTypeModel {
        const model = new ItemTypeModel();
        let max = 0;
        if (itemTypeModel !== undefined) {
            itemTypeModel.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.itemLabel = '';
        model.itemTypeText = 'Please select an item type';
        model.itemLabelDuplicate = '';
        // model.itemTypeId = ;
        model.helpText = '';
        model.isValid = false;
        model.helpTextDuplicate = '';
        model.showMirrored = false;
        model.isMandatory = false;
        model.isOpen = true;
        return model;
    }


    itemMandatoryCheck() {
        if (
            this.treatmentNotes.templateName !== '' &&
            this.treatmentNotes.treatmentNotesTemplateLocation !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateLocation.length > 0 &&
            this.treatmentNotes.treatmentNotesTemplateSpeciality !== undefined &&
            this.treatmentNotes.treatmentNotesTemplateSpeciality.length > 0 &&
            this.treatmentNotes.itemTypeModel !== undefined &&
            this.treatmentNotes.itemTypeModel.length > 0
        ) {
            this.isMandatoryFilled = true;
        } else {
            this.isMandatoryFilled = false;
        }
        if (
            this.treatmentNotes.itemTypeModel.length > 0 &&
            this.isMandatoryFilled === true
        ) {
            this.treatmentNotes.itemTypeModel.map((x) => {
                x.isValid = true;
                console.log('___________________________________', x);

                if (x.itemLabel === '') {
                    x.isValid = false;
                }

                if (x.showMirrored && x.itemLabelDuplicate === '') {
                    x.isValid = false;
                }

                if (x.itemTypeText === 'Number') {
                    if (
                        this.isNumber(x.number.minimum) === false &&
                        this.isNumber(x.number.maximum) === false
                    ) {
                        x.isValid = false;
                    }

                    if (
                        x.showMirrored === true &&
                        this.isNumber(x.numberDuplicate.minimum) === false &&
                        this.isNumber(x.numberDuplicate.maximum) === false
                    ) {
                        x.isValid = false;
                    }

                    if (
                        this.isNumber(x.number.maximum) === true &&
                        this.isNumber(x.number.minimum) === true
                    ) {
                        if (
                            this.isNumber(x.number.default) === true &&
                            x.number.default > x.number.maximum
                        ) {
                            x.isValid = false;
                        }

                        if (x.number.maximum < x.number.minimum) {
                            x.isValid = false;
                        }
                    }

                    if (
                        x.showMirrored === true &&
                        this.isNumber(x.numberDuplicate.maximum) === true &&
                        this.isNumber(x.numberDuplicate.minimum) === true
                    ) {
                        if (
                            this.isNumber(x.numberDuplicate.default) === true &&
                            x.numberDuplicate.default > x.numberDuplicate.maximum
                        ) {
                            x.isValid = false;
                        }

                        if (x.numberDuplicate.maximum < x.numberDuplicate.minimum) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Dropdown') {
                    const d = x.dropdowns.filter((d) => d.value === '');
                    if ((d !== undefined && d.length > 0) || x.dropdowns.length === 0) {
                        x.isValid = false;
                    }
                    if (x.showMirrored === true) {
                        const d = x.duplicateDropdowns.filter((d) => d.value === '');
                        if (
                            (d !== undefined && d.length > 0) ||
                            x.duplicateDropdowns.length === 0
                        ) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Radio Buttons') {
                    const d = x.radioButtons.filter((d) => d.value === '');
                    if (
                        (d !== undefined && d.length > 0) ||
                        x.radioButtons.length === 0
                    ) {
                        x.isValid = false;
                    }
                    if (x.showMirrored === true) {
                        const d = x.duplicateRadioButtons.filter((d) => d.value === '');
                        if (
                            (d !== undefined && d.length > 0) ||
                            x.duplicateRadioButtons.length === 0
                        ) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Check Boxes') {
                    const d = x.checkBoxes.filter((d) => d.value === '');
                    if ((d !== undefined && d.length > 0) || x.checkBoxes.length === 0) {
                        x.isValid = false;
                    }
                    if (x.showMirrored === true) {
                        const d = x.checkBoxesDuplicate.filter((d) => d.value === '');
                        if (
                            (d !== undefined && d.length > 0) ||
                            x.checkBoxesDuplicate.length === 0
                        ) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Option List') {
                    if (x.optionListStyleId === undefined && x.optionListStyleId === 0) {
                        x.isValid = false;
                    }
                    console.log(x);
                    const ols = x.optionListSubjects.filter((d) => d.value === '');
                    if (
                        (ols !== undefined && ols.length > 0) ||
                        x.optionListSubjects.length === 0
                    ) {
                        x.isValid = false;
                    }
                    const olc = x.optionListChoices.filter((d) => d.value === '');
                    if (
                        (olc !== undefined && olc.length > 0) ||
                        x.optionListChoices.length === 0
                    ) {
                        x.isValid = false;
                    }
                    if (x.showMirrored === true) {
                        if (
                            x.optionListStyleDuplicateId === undefined &&
                            x.optionListStyleDuplicateId === 0
                        ) {
                            x.isValid = false;
                        }
                        const olsd = x.optionListSubjectsDuplicate.filter(
                            (d) => d.value === ''
                        );
                        if (
                            (olsd !== undefined && olsd.length > 0) ||
                            x.optionListSubjectsDuplicate.length === 0
                        ) {
                            x.isValid = false;
                        }
                        const olcd = x.optionListChoicesDuplicate.filter(
                            (d) => d.value === ''
                        );
                        if (
                            (olcd !== undefined && olcd.length > 0) ||
                            x.optionListSubjectsDuplicate.length === 0
                        ) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Range or Scale') {
                    if (
                        this.isNumber(x.rangeOrScale.maximum) === false &&
                        this.isNumber(x.rangeOrScale.maximum) === false
                    ) {
                        x.isValid = false;
                    }

                    if (
                        x.showMirrored === true &&
                        this.isNumber(x.rangeOrScaleDuplicate.maximum) === false &&
                        this.isNumber(x.rangeOrScaleDuplicate.maximum) === false
                    ) {
                        x.isValid = false;
                    }

                    if (
                        this.isNumber(x.rangeOrScale.maximum) === true &&
                        this.isNumber(x.rangeOrScale.minimum) === true &&
                        (x.rangeOrScale.maximum < x.rangeOrScale.minimum ||
                            x.rangeOrScale.minimum > 10 ||
                            x.rangeOrScale.maximum > 10 ||
                            x.rangeOrScale.default > 10)
                    ) {
                        x.isValid = false;
                    }

                    if (
                        x.showMirrored === true &&
                        this.isNumber(x.rangeOrScaleDuplicate.maximum) === true &&
                        this.isNumber(x.rangeOrScaleDuplicate.minimum) === true &&
                        (x.rangeOrScaleDuplicate.maximum <
                            x.rangeOrScaleDuplicate.minimum ||
                            x.rangeOrScaleDuplicate.minimum > 10 ||
                            x.rangeOrScaleDuplicate.maximum > 10 ||
                            x.rangeOrScaleDuplicate.default > 10)
                    ) {
                        x.isValid = false;
                    }
                } else if (x.itemTypeText === 'Table') {
                    if (x.table.columns === undefined || x.table.rows === undefined) {
                        x.isValid = false;
                    } else {
                        const col = x.table.columnHeader.filter((t) => t.value === '');
                        const row = x.table.rowHeader.filter((t) => t.value === '');
                        if (
                            (col !== undefined && col.length > 0) ||
                            (row !== undefined && row.length > 0)
                        ) {
                            x.isValid = false;
                        }
                    }
                } else if (x.itemTypeText === 'Chart') {
                    if (x.chartImage.imageSize === '') {
                        x.isValid = false;
                    }
                } else if (x.itemTypeText === 'Vitals') {
                    console.log('#########################################');
                    if (x.vitals.vitalType == '') {
                        console.log(x.vitals.vitalType);
                        x.isValid = false;
                    }
                } else if (x.itemTypeText == 'Text') {
                    if (x.itemLabel == '') {
                        x.isValid = false;
                    } else {

                    }
                }
            });
        }
    }

    cancel() {
        this.router.navigate(['/settings/treatmentnotestemplate']);
    }

    isNumber(value: string | number): boolean {
        return value != null && value !== '' && !isNaN(Number(value.toString()));
    }

    // Item Type Dropdown
    handleItemTypeChange(id: number, event, i) {
        console.log(i);
        this.controlId = i;
        console.log('dddiiii', this.items.controls[i].get('lable').dirty);
        console.log('tttttttttt', this.items.controls[i].get('lable').touched);
        this.treatmentNotesLibraryForm.reset();
        // if(this.items.controls[i].get('lable').dirty == true || this.items.controls[i].get('lable').touched == true){
        //   console.log("ddddddddddd",this.items.controls[i].get('lable').dirty)
        //   this.items.controls[i].get('lable').clearValidators();
        //   this.items.controls[i].get('lable').updateValueAndValidity();
        //   this.items.controls[i].get('lable').markAsUntouched()
        //   console.log(this.treatmentNotesLibraryForm)
        // }
        // if(this.items.controls[i].get('lable').touched == false && this.items.controls[i].get('lable').dirty == false){
        //   console.log("OOOOOOOOOOOOOOOOOOOOO")
        //   this.items.controls[i].get('lable').setValidators([Validators.required])
        //   this.items.controls[i].get('lable').updateValueAndValidity();
        //   // this.treatmentNotesLibraryForm.markAsUntouched();
        // }
        // this.treatmentNotesLibraryForm.markAllAsTouched()
        console.log(this.items.controls[i].get('lable').value);

        this.isSubmitted = false;
        this.selectedId = i;
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.clearItemType(itemTypeIndex);
        if (event !== undefined) {
            const itemType = this.itemTypes.find((x) => x.id === event);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].itemTypeId = event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].itemTypeText =
                itemType.title;
            this.itemTitle = itemType.title;
            this.populateInitialValue(itemType.title, id);
        }
        this.selectedValue = id;

        // this.itemMandatoryCheck(this.treatmentNotes);
    }

    handleItemTypeFilter(value: string) {
        this.itemTypesData = this.itemTypes.filter(
            (s) => s.title.toLowerCase().indexOf(value.toLowerCase()) !== -1
        );
    }

    private clearItemType(itemTypeIndex: number) {
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemLabel = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemLabelDuplicate = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpText = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpTextDuplicate = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemTypeId = 0;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemTypeText = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].text = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].textDuplicate = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].chiefComplaint = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].instruction = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].instructionDuplicate = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].spineNotes = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListStyleId = 0;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListSubjects = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoices = [];
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListStyleDuplicateId = 0;
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate = [];
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate = [];
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].dateTime = new DateTimeModel();
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].dateTimeDuplicate = new DateTimeModel();
        this.treatmentNotes.itemTypeModel[itemTypeIndex].number = new NumberModel();
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].numberDuplicate = new NumberModel();
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].rangeOrScale = new NumberModel();
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].rangeOrScaleDuplicate = new NumberModel();
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table = new TableModel();
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate = new TableModel();
        this.treatmentNotes.itemTypeModel[itemTypeIndex].selectedDropdown = 0;
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].selectedDuplicateDropdown = 0;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].setAsDefault = false;
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].setAsDuplicateDefault = false;
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartImage = new ChartModel('../../../assets/image 1.png');
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartDuplicateImage = new ChartModel('../../../assets/dropfileimage.png');
        this.treatmentNotes.itemTypeModel[itemTypeIndex].vitals = new VitalsModel();
        this.treatmentNotes.itemTypeModel[itemTypeIndex].isOpen = true;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].showMirrored = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].isMandatory = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].includeNotes = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].isOpen = true;
    }

    populateInitialValue(title: string, id: number) {
        console.log('############', id);
        const itemType = this.treatmentNotes.itemTypeModel.find((x) => x.id === id);

        if (title === 'Dropdown') {
            if (itemType.dropdowns.length === 0) {
                this.addDropDownOption(id);
            }
        } else if (title === 'Radio Buttons') {
            if (itemType.radioButtons.length === 0) {
                this.addRadioButtonOption(id);
            }
        } else if (title === 'Check Boxes') {
            if (itemType.checkBoxes.length === 0) {
                this.addCheckBox(id);
            }
        } else if (title === 'Option List') {
            console.log(itemType.optionListSubjects);
            itemType.optionListStyleId = 1;
            if (itemType.optionListSubjects.length === 0) {
                this.addOptionListSubject(id);
            }

            if (itemType.optionListChoices.length === 0) {
                this.addOptionListChoice(id);
            }
        } else if (title === 'Text') {
            itemType.text = 'Please enter appropriate notes';
        } else if (title == 'Chart') {
            itemType.chartImage.tempImage = '../../../assets/exportimage.png';
            this.chartChange(itemType.itemTypeId, this.chartSize);
            console.log(itemType);
        } else if (title === 'Spine') {
            itemType.itemLabel = 'Spine';
        } else if (title === 'Vitals') {
            console.log('******************', this.items);
            console.log('DDDDDDDDDDDDDD', this.treatmentNotes.itemTypeModel);
            this.unitOfMeasurement = [];
            itemType.itemLabel = 'Vitals';
            this.items.controls[this.controlId].get('vitals').setErrors(Validators.required);
            console.log('!!!!!!!!!!!!!!!!!!!!!', this.items.controls[this.controlId].get('vitals'));
        } else if (title === 'Chief Complaint') {
            itemType.itemLabel = 'Chief Complaint';
            itemType.chiefComplaint = 'Please type the chief complaint of the patient here';
        } else if (title === 'Instruction') {
            itemType.instruction = '';
        } else if (title === 'Range or Scale') {
            itemType.rangeOrScale = new NumberModel();
            itemType.rangeOrScaleDuplicate = new NumberModel();
        } else if (title === 'Number') {
            itemType.numberDuplicate = new NumberModel();
        }
    }

    populateInitialValueOnSelectOfShowMirror(title: string, id: number) {
        const itemType = this.treatmentNotes.itemTypeModel.find((x) => x.id === id);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );

        if (title === 'Dropdown') {
            if (itemType.dropdowns.length === 0) {
                this.addDropDownOption(id);
            }
            if (
                itemType.showMirrored === true &&
                itemType.duplicateDropdowns.length === 0
            ) {
                this.addDuplicateDropDownOption(id);
            } else {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].duplicateDropdowns = [];
            }
        } else if (title === 'Radio Buttons') {
            if (itemType.radioButtons.length === 0) {
                this.addRadioButtonOption(id);
            }
            if (
                itemType.showMirrored === true &&
                itemType.duplicateRadioButtons.length === 0
            ) {
                this.addDuplicateRadioButtonOption(id);
            } else {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].duplicateRadioButtons = [];
            }
        } else if (title === 'Check Boxes') {
            if (itemType.checkBoxes.length === 0) {
                this.addCheckBox(id);
            }
            if (
                itemType.showMirrored === true &&
                itemType.checkBoxesDuplicate.length === 0
            ) {
                this.addDuplicateCheckBox(id);
            } else {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].checkBoxesDuplicate = [];
            }
        } else if (title === 'Option List') {
            if (itemType.optionListSubjects.length === 0) {
                this.addOptionListSubject(id);
            }
            if (
                itemType.showMirrored === true &&
                itemType.optionListSubjectsDuplicate.length === 0
            ) {
                itemType.optionListStyleDuplicateId = 1;
                this.addOptionListSubjectDuplicate(id);
            } else {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].optionListSubjectsDuplicate = [];
            }

            if (itemType.optionListChoices.length === 0) {
                this.addOptionListChoice(id);
            }
            if (
                itemType.showMirrored === true &&
                itemType.optionListChoicesDuplicate.length === 0
            ) {
                this.addOptionListChoiceDuplicate(id);
            } else {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].optionListChoicesDuplicate = [];
                this.treatmentNotes.itemTypeModel[itemTypeIndex].helpTextDuplicate = '';
            }
        } else if (title === 'Text') {
            itemType.text = 'Please enter appropriate notes';
            if (itemType.showMirrored === true) {
                itemType.textDuplicate = 'Please enter appropriate notes';
            }
        } else if (title === 'Spine') {
            itemType.itemLabel = 'Spine';
        } else if (title === 'Vitals') {
            itemType.itemLabel = 'Vitals';
        } else if (title === 'Chief Complaint') {
            itemType.itemLabel = 'Chief Complaint';
        } else if (title === 'Instruction') {
            itemType.instruction = 'Type the instruction the user has to follow';
        } else if (title === 'Range or Scale') {
            // itemType.rangeOrScale = new NumberModel();
            itemType.rangeOrScaleDuplicate = new NumberModel();
        } else if (title === 'Number') {
            itemType.numberDuplicate = new NumberModel();
        }
    }

    removeDropdownSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.map((x) => {
            x.default = false;
        });
        this.treatmentNotes.itemTypeModel[itemTypeIndex].selectedDropdown = 0;
    }

    removeDuplicateDropdownSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns.map(
            (x) => {
                x.default = false;
            }
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].selectedDuplicateDropdown = 0;
    }

    removeRadiobuttonSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.map((x) => {
            x.default = false;
        });
    }

    removeDuplicateRadiobuttonSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons.map(
            (x) => {
                x.default = false;
            }
        );
    }

    removedCheckBoxSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.map((x) => {
            x.default = false;
        });
    }

    removedDuplicateCheckBoxSetAsDefault(id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate.map(
            (x) => {
                x.default = false;
            }
        );
    }

    labelChange(id: number, value: string, i) {
        console.log(value);
        this.labelValue = value;
        console.log(i);
        this.items.controls[i].get('lable').markAsTouched();
        // this.items.controls[i].get('lable').setValidators([Validators.required]);

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemLabel = value;
    }

    duplicateLabelChange(id: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].itemLabelDuplicate = value;
    }

    helpTextChange(id: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpText = value;
    }

    duplicateHelpTextChange(id: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpTextDuplicate = value;
    }

    deleteItem(id: number) {

        console.log(this.selectedUnit);
        this.isSubmitted = false;
        this.itemTitle = '';
        this.selectedUnit = '';
        // this.items.controls[this.controlId].get('lable').setValue("")
        console.log('@@@@@@@@@@@@@@@@@', this.itemTitle);

        this.treatmentNotesLibraryForm.reset();
        console.log(this.treatmentNotesLibraryForm);
        console.log(this.isDelete);

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        if (itemTypeIndex !== -1) {
            this.treatmentNotes.itemTypeModel.splice(itemTypeIndex, 1);
        }

        this.isItemAdded = this.treatmentNotes.itemTypeModel.length != 0;
        this.opendeleteform = false;

    }

    mirroredValueChange(itemTypeId: number, $event: { checked: boolean }, i) {
        this.items = this.treatmentNotesLibraryForm.get('items') as FormArray;
        console.log(this.items);
        console.log(i);
        this.items.controls[i].get('duplicateLabel').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatedate').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatetime').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatemaxNumber').setValidators([Validators.required]);
        this.items.controls[i].get('duplicateminNumber').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatedefaultNumber').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatedropdown').setValidators([Validators.required]);
        this.items.controls[i].get('duplicateradiobutton').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatecheckbox').setValidators([Validators.required]);
        this.items.controls[i].get('duplicateminRange').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatemaxRange').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatedefaultRange').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatesubjectinput').setValidators([Validators.required]);
        this.items.controls[i].get('duplicatechoiceinput').setValidators([Validators.required]);


        const itemType = this.treatmentNotes.itemTypeModel.find(
            (x) => x.id === itemTypeId
        );
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].showMirrored =
            $event.checked;
        this.populateInitialValueOnSelectOfShowMirror(
            itemType.itemTypeText,
            itemTypeId
        );
    }

    includeNotesValueChange(itemTypeId: number, $event: { checked: boolean }) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].includeNotes =
            $event.checked;
    }

    mandatoryValueChange(itemTypeId: number, $event: { checked: boolean }) {

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );


        this.treatmentNotes.itemTypeModel[itemTypeIndex].isMandatory =
            $event.checked;
    }

    defaultNoteChange(id: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpText = value;
    }

    duplicateDefaultNoteChange(id: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === id
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].helpTextDuplicate = value;
    }

    // Dropdown
    addDropDownOption(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new DropdownModel();
        let max = 0;
        if (
            itemTypeIndex >= 0 &&
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.forEach(
                (x) => {
                    if (x.id > max) {
                        max = x.id;
                    }
                }
            );
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.push(model);
    }

    removeDropDownOption(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].dropdowns.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.splice(
                ddIndex,
                1
            );
        }
    }

    selectedDropDownDefault(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.length > 0) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns.forEach(
                (c) => {
                    const dIndex = this.treatmentNotes.itemTypeModel[
                        itemTypeIndex
                        ].dropdowns.findIndex((x) => x.id === c.id);
                    this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns[
                        dIndex
                        ].default = false;
                }
            );

            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].dropdowns.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns[
                ddIndex
                ].default = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].setAsDefault = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].selectedDropdown = id;
            console.log(this.treatmentNotes);
        }
    }

    inputDropDownValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].dropdowns.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].dropdowns[ddIndex].value =
            event.target.value;
    }

    // Duplicate Dropdown
    addDuplicateDropDownOption(itemTypeId: number) {
        console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new DropdownModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateDropdowns.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns.push(
            model
        );
    }

    removeDuplicateDropDownOption(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].duplicateDropdowns.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateDropdowns.splice(ddIndex, 1);
        }
    }

    selectedDropDownDuplicateDefault(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateDropdowns.forEach((c) => {
                const dIndex = this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].duplicateDropdowns.findIndex((x) => x.id === c.id);
                this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns[
                    dIndex
                    ].default = false;
            });

            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateDropdowns.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns[
                ddIndex
                ].default = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].setAsDuplicateDefault = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].selectedDuplicateDropdown = id;

            console.log(this.treatmentNotes);
        }
    }

    inputDropDownDuplicateValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].duplicateDropdowns.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateDropdowns[
            ddIndex
            ].value = event.target.value;
    }

    // Radio Button
    addRadioButtonOption(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new RadioButtonModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.forEach(
                (x) => {
                    if (x.id > max) {
                        max = x.id;
                    }
                }
            );
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.push(model);
    }

    removeRadioButtonOption(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].radioButtons.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.splice(
                ddIndex,
                1
            );
        }
    }

    selectedRadioButtonDefault(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons.forEach(
                (c) => {
                    const dIndex = this.treatmentNotes.itemTypeModel[
                        itemTypeIndex
                        ].radioButtons.findIndex((x) => x.id === c.id);
                    this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons[
                        dIndex
                        ].default = false;
                }
            );

            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].radioButtons.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons[
                ddIndex
                ].default = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].setAsDefault = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].selectedDropdown = id;
            console.log(this.treatmentNotes);
        }
    }

    inputRadioButtonValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].radioButtons.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].radioButtons[
            ddIndex
            ].value = event.target.value;
    }

    // Duplicate Radio Button
    addDuplicateRadioButtonOption(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new RadioButtonModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateRadioButtons.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons.push(
            model
        );
    }

    removeDuplicateRadioButtonOption(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].duplicateRadioButtons.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateRadioButtons.splice(ddIndex, 1);
        }
    }

    selectedRadioButtonDuplicateDefault(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateRadioButtons.forEach((c) => {
                const dIndex = this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].duplicateRadioButtons.findIndex((x) => x.id === c.id);
                this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons[
                    dIndex
                    ].default = false;
            });

            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].duplicateRadioButtons.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons[
                ddIndex
                ].default = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].setAsDuplicateDefault = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].selectedDuplicateDropdown = id;

            console.log(this.treatmentNotes);
        }
    }

    inputRadioButtonDuplicateValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].duplicateRadioButtons.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].duplicateRadioButtons[
            ddIndex
            ].value = event.target.value;
    }

    // Check Box
    addCheckBox(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new CheckBoxModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.forEach(
                (x) => {
                    if (x.id > max) {
                        max = x.id;
                    }
                }
            );
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.push(model);
    }

    removeCheckBox(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].checkBoxes.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.splice(
                ddIndex,
                1
            );
        }
    }

    selectedCheckBoxDefault(itemTypeId: number, id: number, $event) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes.length > 0
        ) {
            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].checkBoxes.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes[
                ddIndex
                ].default = $event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].setAsDefault = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].selectedDropdown = id;
            console.log(this.treatmentNotes);
        }
    }

    inputCheckBoxValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].checkBoxes.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxes[ddIndex].value =
            event.target.value;
    }

    // Duplicate Check Box
    addDuplicateCheckBox(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new CheckBoxModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].checkBoxesDuplicate.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        model.default = false;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate.push(
            model
        );
    }

    removeDuplicateCheckBox(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].checkBoxesDuplicate.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].checkBoxesDuplicate.splice(ddIndex, 1);
        }
    }

    selectedCheckBoxDuplicateDefault(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate
                .length > 0
        ) {
            const ddIndex = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].checkBoxesDuplicate.findIndex((x) => x.id === id);
            this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate[
                ddIndex
                ].default = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].setAsDuplicateDefault = true;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].selectedDuplicateDropdown = id;

            console.log(this.treatmentNotes);
        }
    }

    inputCheckBoxDuplicateValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].checkBoxesDuplicate.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].checkBoxesDuplicate[
            ddIndex
            ].value = event.target.value;
    }

    // Option List
    addOptionListSubject(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new KeySubjectModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListSubjects
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListSubjects.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListSubjects.push(
            model
        );
    }

    optionListStylesChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListStyleId = $event;
    }

    removeOptionListSubject(itemTypeId: number, id: number) {
        console.log('iiiiiiiiiii', id);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        console.log(itemTypeId);
        console.log(this.treatmentNotes);
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjects.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListSubjects.splice(ddIndex, 1);
        }
    }

    inputOptionListSubjectValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjects.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListSubjects[
            ddIndex
            ].value = event.target.value;
    }

    addOptionListChoice(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new KeyValueModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoices
                .length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListChoices.forEach((x) => {
                console.log(x);
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoices.push(
            model
        );
    }

    removeOptionListChoice(itemTypeId: number, id: number, event: any) {
        console.log(event);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListChoices.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoices.splice(
                ddIndex,
                1
            );
        }
    }

    inputOptionListChoiceValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        console.log(id);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListChoices.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoices[
            ddIndex
            ].value = event.target.value;
    }

    // Duplicate Option List
    addOptionListSubjectDuplicate(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new KeySubjectModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex]
                .optionListSubjectsDuplicate.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListSubjectsDuplicate.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate.push(model);
    }

    optionListStylesDuplicateChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListStyleDuplicateId = $event;
    }

    removeOptionListSubjectDuplicate(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListSubjectsDuplicate.splice(ddIndex, 1);
        }
    }

    inputOptionListSubjectDuplicateValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListSubjectsDuplicate[ddIndex].value = event.target.value;
    }

    addOptionListChoiceDuplicate(itemTypeId: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const model = new KeyValueModel();
        let max = 0;
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex]
                .optionListChoicesDuplicate.length > 0
        ) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListChoicesDuplicate.forEach((x) => {
                if (x.id > max) {
                    max = x.id;
                }
            });
            model.id = max + 1;
        } else {
            model.id = 1;
        }
        model.value = '';
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListChoicesDuplicate.push(model);
    }

    removeOptionListChoiceDuplicate(itemTypeId: number, id: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListChoicesDuplicate.findIndex((x) => x.id === id);
        if (ddIndex !== -1) {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].optionListChoicesDuplicate.splice(ddIndex, 1);
        }
    }

    inputOptionListChoiceDuplicateValue(
        itemTypeId: number,
        id: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const ddIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].optionListChoicesDuplicate.findIndex((x) => x.id === id);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].optionListChoicesDuplicate[
            ddIndex
            ].value = event.target.value;
    }

    // Table
    tableRowChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.rowHeader = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.rows = $event;
        for (let i = 1; i <= $event; i++) {
            const m = new KeyValueModel();
            m.id = i;
            m.value = '';
            this.treatmentNotes.itemTypeModel[itemTypeIndex].table.rowHeader.push(m);
        }
    }

    tableColumnChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.columnHeader = [];
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.columns = $event;
        for (let i = 1; i <= $event; i++) {
            const m = new KeyValueModel();
            m.id = i;
            m.value = '';
            this.treatmentNotes.itemTypeModel[itemTypeIndex].table.columnHeader.push(
                m
            );
        }
    }

    tableRowHeaderInput(
        itemTypeId: number,
        rowId: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const rowIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].table.rowHeader.findIndex((x) => x.id === rowId);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.rowHeader[
            rowIndex
            ].id = rowId;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.rowHeader[
            rowIndex
            ].value = event.target.value;
    }

    tableColumnHeaderInput(
        itemTypeId: number,
        columnId: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const columnIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].table.columnHeader.findIndex((x) => x.id === columnId);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.columnHeader[
            columnIndex
            ].id = columnId;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].table.columnHeader[
            columnIndex
            ].value = event.target.value;
    }

    // Duplicate Table
    tableRowDuplicateChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.rowHeader = [];
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.rows = $event;
        for (let i = 1; i <= $event; i++) {
            const m = new KeyValueModel();
            m.id = i;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].tableDuplicate.rowHeader.push(m);
        }
    }

    tableColumnDuplicateChange(itemTypeId: number, $event: number) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.columnHeader = [];
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.columns = $event;
        for (let i = 1; i <= $event; i++) {
            const m = new KeyValueModel();
            m.id = i;
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].tableDuplicate.columnHeader.push(m);
        }
    }

    tableRowHeaderDuplicateInput(
        itemTypeId: number,
        rowId: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const rowIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.rowHeader.findIndex((x) => x.id === rowId);
        this.treatmentNotes.itemTypeModel[itemTypeIndex].tableDuplicate.rowHeader[
            rowIndex
            ].id = rowId;
        this.treatmentNotes.itemTypeModel[itemTypeIndex].tableDuplicate.rowHeader[
            rowIndex
            ].value = event.target.value;
    }

    tableColumnHeaderDuplicateInput(
        itemTypeId: number,
        columnId: number,
        event: { target: { value: string } }
    ) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        const columnIndex = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.columnHeader.findIndex((x) => x.id === columnId);
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.columnHeader[columnIndex].id = columnId;
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].tableDuplicate.columnHeader[columnIndex].value = event.target.value;
    }

    // Date Time
    onDateChange(itemTypeId: number, $event: Date) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime === undefined
        ) {
            const m = new DateTimeModel();
            m.date = $event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime = m;
        } else {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime.date = $event;
        }
        console.log('&&&&&&&&&&////////////&&&&');
    }

    // Date Time
    onTimeChange(itemTypeId: number, $event: Date) {
        console.log('&&&&&&&&&&&&&&');
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime === undefined
        ) {
            const m = new DateTimeModel();
            m.time = $event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime = m;
        } else {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime.time = $event;
        }
        console.log(this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTime.time);
    }

    // Duplicate Date Time
    onDuplicateDateChange(itemTypeId: number, $event: Date) {

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTimeDuplicate ===
            undefined
        ) {
            const m = new DateTimeModel();
            m.date = $event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTimeDuplicate = m;
        } else {

            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].dateTimeDuplicate.date = $event;
        }
    }

    // Duplicate Date Time
    onDuplicateTimeChange(itemTypeId: number, $event: Date) {

        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTimeDuplicate ===
            undefined
        ) {
            const m = new DateTimeModel();
            m.time = $event;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].dateTimeDuplicate = m;
        } else {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].dateTimeDuplicate.time = $event;
        }
    }

    // Text
    textChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].text = value;
    }

    // Spine
    spineChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].spineNotes = value;
    }

    // Duplicate Text
    textDuplicateChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].textDuplicate = value;
    }


    numberOnly(event): boolean {
        const charCode = event.which ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    // Number
    numberChange(itemTypeId: number, value: number, type: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (type === 'Minimum') {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].number.minimum = value;
        } else if (type === 'Maximum') {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].number.maximum = value;
        } else if (type === 'Default') {
            this.treatmentNotes.itemTypeModel[itemTypeIndex].number.default = value;
        } else if (type === 'Minimum Duplicate') {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].numberDuplicate.minimum = value;
        } else if (type === 'Maximum Duplicate') {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].numberDuplicate.maximum = value;
        } else if (type === 'Default Duplicate') {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].numberDuplicate.default = value;
        }
    }

    formatLabel(value: number | null) {
        if (!value) {
            return 0;
        }
        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }
        return value;
    }

    // Range or Scale
    rangeOrScaleChange(itemTypeId: number, value: number, type: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        if (type === 'Minimum') {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScale.minimum = value;
        } else if (type === 'Maximum') {
            if (value <= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScale.minimum) {
                this.maxvalidation = true;
            } else {
                this.maxvalidation = false;
            }
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScale.maximum = value;
        } else if (type === 'Default') {
            if (value <= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScale.minimum || value >= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScale.maximum) {
                this.defautmessage = true;
            } else {
                this.defautmessage = false;
            }
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScale.default = value;
        } else if (type === 'Minimum Duplicate') {
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScaleDuplicate.minimum = value;
        } else if (type === 'Maximum Duplicate') {
            if (value <= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScaleDuplicate.minimum) {
                this.maxDuplicatemessage = true;
            } else {
                this.maxDuplicatemessage = false;
            }
            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScaleDuplicate.maximum = value;
        } else if (type === 'Default Duplicate') {

            if (value <= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScaleDuplicate.minimum || value >= this.treatmentNotes.itemTypeModel[itemTypeIndex].rangeOrScaleDuplicate.maximum) {
                this.defautDuplicatemessage = true;
            } else {
                this.defautDuplicatemessage = false;
            }

            this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].rangeOrScaleDuplicate.default = value;
        }

        this.childTreatmentNotesPreview.updateRange();
    }

    // Chart
    parentChartCallBack(itemTypeId: number, $event: { src: string }) {
        console.log($event);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartImage.image = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartImage.tempImage =
            $event === null ? '../../../assets/image 1.png' : $event.src;
        this.chartChange(itemTypeId, this.chartSize);
    }

    chartChange(itemTypeId: number, sizeType: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );

        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartImage.imageSize = this.chartSize = sizeType;
        let ratio = 0;
        let quality = 0;
        if (sizeType === 'Small') {
            ratio = 50;
            quality = 100;
        } else if (sizeType === 'Medium') {
            ratio = 70;
            quality = 100;
        } else {
            ratio = 100;
            quality = 100;
        }
        const image = this.treatmentNotes.itemTypeModel[itemTypeIndex].chartImage
            .image;
        console.log('Change Image', image);
        if (image == '../../../assets/dropfileimage.png') {
            this.exportImageData = '../../../assets/image 1.png';
        } else {
            this.exportImageData = image;
        }

        this.imageCompress
            .compressFile(this.exportImageData, -1, ratio, quality)
            .then((result) => {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].chartImage.tempImage = result;
            });
    }

    // Chart Duplicate
    chartDuplicateChange(itemTypeId: number, sizeType: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartDuplicateImage.imageSize = this.chartDuplicateSize = sizeType;

        let ratio = 0;
        let quality = 0;
        if (sizeType === 'Small') {
            ratio = 50;
            quality = 100;
        } else if (sizeType === 'Medium') {
            ratio = 70;
            quality = 100;
        } else {
            ratio = 100;
            quality = 100;
        }
        const image = this.treatmentNotes.itemTypeModel[itemTypeIndex]
            .chartDuplicateImage.image;
        if (image == '../../../assets/dropfileimage.png') {
            this.exportDuplicateImageData = '../../../assets/exportimage.png';
        } else {
            this.exportDuplicateImageData = image;
        }

        this.imageCompress
            .compressFile(this.exportDuplicateImageData, -1, ratio, quality)
            .then((result) => {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].chartDuplicateImage.tempImage = result;
            });
    }

    parentChartDuplicateCallBack(itemTypeId: number, $event: { src: string }) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartDuplicateImage.image = this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].chartDuplicateImage.tempImage =
            $event === null ? '../../../assets/dropfileimage.png' : $event.src;
        this.chartDuplicateChange(itemTypeId, this.chartDuplicateSize);
    }

    // File Attachment
    fileDescriptionChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].fileDescription = value;
    }

    // File Attachment Duplicate
    fileDescriptionDuplicateChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].fileDuplicateDescription = value;
    }

    // Instruction
    instructionChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].instruction = value;
    }

    // Instruction Duplicate
    instructionDuplicateChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[
            itemTypeIndex
            ].instructionDuplicate = value;
    }

    // Chief Complaint
    chiefComplaintChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].chiefComplaint = value;
    }

    // Vitals
    vitalsChange(itemTypeId: number, value: string) {
        console.log(value);
        this.selectedUnit = value;
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].vitals = new VitalsModel();
        console.log(this.selectedUnit);
        if (this.selectedUnit) {
            if (value === 'Metric') {

                this.unitOfMeasurement = this.metric;
            } else {
                this.unitOfMeasurement = this.imperial;
            }
        }

        this.treatmentNotes.itemTypeModel[itemTypeIndex].vitals.vitalType = value;
    }


    // Optical
    opticalChange(itemTypeId: number, value: string) {
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );
        this.treatmentNotes.itemTypeModel[itemTypeIndex].OpticalNotes = value;
    }

    getChecked() {
        var key = [];
        console.log(this.unitOfMeasurement);
        setTimeout(() => {
            this.unitOfMeasurement.forEach(element => {
                console.log(element.name);
                if (element.name == 'Height (cm)' || element.name == 'Height (in)') {
                    console.log(element.isChecked);
                    if (element.isChecked == true) {
                        key.push(element.name);
                        console.log('!!!!!!!!!!!');
                    }
                }
                if (element.name == 'Weight (Kg)' || element.name == 'Weight (lbs)') {
                    console.log(element.isChecked);
                    if (element.isChecked == true) {
                        key.push(element.name);
                        console.log('$$$$$$$$$$$$$$$');
                    }
                }


            });

            console.log(key.length);

            if (key.length == 2) {
                console.log(key);

                this.keyValue = true;
            } else {
                this.keyValue = false;
            }
        }, 500);
    }

    unitOfMeasurementChange(
        itemTypeId: number,
        value: { checked: boolean },
        vitalId: any
    ) {
        console.log(vitalId);
        const itemTypeIndex = this.treatmentNotes.itemTypeModel.findIndex(
            (x) => x.id === itemTypeId
        );

        this.getChecked();
        setTimeout(() => {
            console.log(this.keyValue);

            console.log('###################################', this.keyValue);

            this.unitOfMeasurement.forEach(element => {
                if (this.keyValue == true) {
                    if (element.name == 'BMI (kg/m2)' || element.name == 'BMI (lb/in2)') {
                        console.log(element);

                        element.isChecked = true;
                    }
                } else if (element.name == 'BMI (kg/m2)' || element.name == 'BMI (lb/in2)') {
                    element.isChecked = false;

                }
            });

        }, 500);

        if (value.checked === true) {
            vitalId.isChecked = true;
            this.treatmentNotes.itemTypeModel[itemTypeIndex].vitals.measurements.push(
                vitalId.name
            );
        } else {
            vitalId.isChecked = false;
            const index = this.treatmentNotes.itemTypeModel[
                itemTypeIndex
                ].vitals.measurements.indexOf(vitalId.name);
            if (index !== -1) {
                this.treatmentNotes.itemTypeModel[
                    itemTypeIndex
                    ].vitals.measurements.splice(index, 1);
            }
        }
    }


    public close() {

        this.opendeleteform = false;
    }

    deleteData() {
        this.opendeleteform = true;
    }

    // deleteitem(itemTypeId: number) {
    //   console.log(this.treatmentNotes.treatmentData.length)
    //   if (this.treatmentNotes.treatmentData.length >= 2) {
    //     const id = itemTypeId === 0 ? this.currentItemTypeId : itemTypeId;
    //     //this.itemType = new ItemTypeModel();
    //     this.deletedItemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
    //       (x) => x.id === id
    //     );
    //     this.treatmentNotes.treatmentData.splice(this.deletedItemTypeIndex, 1);
    //     this.openheadingeditform = false;
    //     if (this.currentItemTypeId !== 0) {
    //       this.opendeleteform = false;
    //     }
    //     this.currentItemTypeId = 0;
    //     console.log(this.treatmentNotes.treatmentData.length)
    //   }
    //   else {
    //     this.opendeleteform = false;
    //     this.displayMessage('Atleast one item need to save record.');
    //   }
    // }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {

        // console.log(control ,"&& ",control.invalid," &&", control.dirty ,"||" ,control.touched)
        return !!(control && control.invalid && (control.dirty || control.touched));
    }
}

function isEmptyInputValue(value: any) {
    throw new Error('Function not implemented.');
}

