import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { TreatmentNotesModel } from "src/app/models/app.treatmentnotes.model";

@Component({
  selector: "app-treatment-notes-preview",
  templateUrl: "./treatment-notes-preview.component.html",
  styleUrls: [
    "../add-treatment-notes-template/add-treatment-notes-template.component.css",
  ],
})
export class TreatmentNotesPreviewComponent implements OnChanges {
  @Input() treatmentNotes: TreatmentNotesModel;
  @Input() unitOfMeasurement;
  apperance = "outline";

  spine = [
    {
      id: 1,
      name: "C0",
    },
    {
      id: 2,
      name: "C1",
    },
    {
      id: 3,
      name: "C2",
    },
    {
      id: 4,
      name: "C3",
    },
    {
      id: 5,
      name: "C4",
    },
    {
      id: 6,
      name: "C5",
    },
    {
      id: 7,
      name: "C6",
    },
    {
      id: 8,
      name: "C7",
    },
    {
      id: 9,
      name: "T1",
    },
    {
      id: 10,
      name: "T2",
    },
    {
      id: 11,
      name: "T3",
    },
    {
      id: 12,
      name: "T4",
    },
    {
      id: 13,
      name: "T5",
    },
    {
      id: 14,
      name: "T6",
    },
    {
      id: 15,
      name: "T7",
    },
    {
      id: 16,
      name: "T8",
    },
    {
      id: 17,
      name: "T9",
    },
    {
      id: 18,
      name: "T10",
    },
    {
      id: 19,
      name: "T11",
    },
    {
      id: 20,
      name: "T12",
    },
    {
      id: 21,
      name: "L1",
    },
    {
      id: 22,
      name: "L2",
    },
    {
      id: 23,
      name: "L3",
    },
    {
      id: 24,
      name: "L4",
    },
    {
      id: 25,
      name: "L5",
    },
    {
      id: 26,
      name: "Sacrum",
    },
    {
      id: 27,
      name: "SI AS",
    },
    {
      id: 28,
      name: "SI PI",
    },
    {
      id: 29,
      name: "Coccyx",
    },
  ];
  public step = 1;
  private numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  public value = 0;
  public min = 0;
  public max = 0;
  public valueDuplicate = 0;
  public minDuplicate = 0;
  public maxDuplicate = 0;
  ticks = Date.now().valueOf();

  constructor() { }

  ngOnChanges() {
    console.log(this.unitOfMeasurement)
    this.updateRange();
  }

  updateRange(): void {
    this.treatmentNotes.itemTypeModel.map((x) => {
      if (
        x.rangeOrScale !== null &&
        x.rangeOrScale.minimum !== undefined &&
        x.rangeOrScale.maximum !== undefined &&
        x.rangeOrScale.default !== undefined
      ) {
        this.min = Number(x.rangeOrScale.minimum);
        this.max = Number(x.rangeOrScale.maximum);
        this.value = Number(x.rangeOrScale.default);
        this.range(this.value);
      }

      if (
        x.rangeOrScaleDuplicate !== null &&
        x.rangeOrScaleDuplicate.minimum !== undefined &&
        x.rangeOrScaleDuplicate.maximum !== undefined &&
        x.rangeOrScaleDuplicate.default !== undefined
      ) {
        this.minDuplicate = Number(x.rangeOrScaleDuplicate.minimum);
        this.maxDuplicate = Number(x.rangeOrScaleDuplicate.maximum);
        this.valueDuplicate = Number(x.rangeOrScaleDuplicate.default);
        this.rangeDuplicate(this.valueDuplicate);
      }
    });
  }

  public range = (value: number) => {
    return this.numbers[value];
  };

  public rangeDuplicate = (value: number) => {
    return this.numbers[value];
  };
}
