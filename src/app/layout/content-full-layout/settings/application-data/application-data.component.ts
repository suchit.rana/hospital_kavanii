import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ApplicationDataService} from 'src/app/services/app.applicationdata.service';
import {AppState} from 'src/app/app.state';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {BaseItemComponent} from '../../../../shared/base-item/base-item.component';
import {ApplicationDataLocation, ApplicationDataModel,} from 'src/app/models/app.settings.model';
import {MatButtonToggleChange} from '@angular/material';
import {BusinessService} from 'src/app/services/app.business.service';
import {LocationModel} from 'src/app/models/app.treatmentnotes.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {BaseGridComponent} from '../../shared-content-full-layout/base-grid/base-grid.component';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-application-data',
  templateUrl: './application-data.component.html',
  styleUrls: ['./application-data.component.css'],
})
export class ApplicationDataComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.settings_import_export;

  @BlockUI() blockUI: NgBlockUI;
  submitting = false;
  apperance = 'outline';

  noDataMessage = [
    {
      id: ApplicationDataEnum.Title,
      name: 'Title',
      title: 'You haven’t added a Title.',
      description:
        'A title may signify either veneration, an official position, or a professional or academic qualification of a Staff, Contacts, and Patient in Kavanii',
      isActive: true,
    },
    {
      id: ApplicationDataEnum.StaffPosition,
      name: 'Staff Position',
      title: 'You haven’t added a Staff Position.',
      description:
        'Position defines the designation of the Staff in your Business Location. It may be director, Owner, Intern, Admin Staff, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.StaffOtherIdName,
      name: 'Staff Other Id Name',
      title: 'You haven’t added a Staff Other Id Name.',
      description: '',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.PatientStatus,
      name: 'Patient Status',
      title: 'You haven’t added a Patient Status.',
      description: 'Status of the patient can be Deceased, Archived, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.PatientPosition,
      name: 'Patient Position',
      title: 'You haven’t added a Patient Position.',
      description:
        'Position defines the marrital status of the patient. It may be Single, Married, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.PatientRelationship,
      name: 'Relationship',
      title: 'You haven’t added a Relationship.',
      description:
        'A relationship defines the nature of the relation between the Patient and the Emergency contact person.',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.PatientClassification,
      name: 'Patient Classification',
      title: 'You haven’t added a Patient Classification.',
      description:
        'You can classify patients by the special needs they require. It can a general condition or a particular problem such as Injury, Weight Loss, Depression, Fitness, Trauma, etc.',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.PatientOccupation,
      name: 'Occupation',
      title: 'You haven’t added an Occupation.',
      description: 'Job or Profession of the Patient.',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.CardType,
      name: 'Credit Card Type',
      title: 'You haven’t added a Credit Card Type.',
      description:
        'You can list all the bank issued credit cards used here. It may be VISA, MASTER, etc.',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.HealthFundName,
      name: 'Health Fund Name',
      title: 'You haven’t added a Health Fund Name.',
      description: 'You can create a Public or Private Health Fund',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.ServiceCategory,
      name: 'Service Category',
      title: 'You haven’t added a Service Category.',
      description:
        'You can create a category to group related services provided in your Business Location. It may be Consultation, Reoprt Writing, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.ClassCategory,
      name: 'Class Category',
      title: 'You haven’t added a Class Category.',
      description:
        'You can create a category to group related classes conducted in your Business Location. It may be Pilates, Yoga, etc.',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.ProductCategory,
      name: 'Product Category',
      title: 'You haven’t added a Product Category.',
      description:
        'You can create a category to group related products sold in your Business Location. It may be Shoes, Neck Support, Scrubs, Masks, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.UnitOfMeasurement,
      name: 'Unit Of Measurement',
      title: 'You haven’t added a Unit Of Measurement.',
      description:
        'A unit of measurement is a definite magnitude of a quantity. It may be Packet, Piece, Pair, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.ContactCategory,
      name: 'General Contact Category',
      title: 'You haven’t added a General Contact Category.',
      description:
        'You can create a category to group related contacts. It may be Laboratory, Pharmacy, Legal Advisors, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.BreakName,
      name: 'Break',
      title: 'You haven’t added a Break.',
      description:
        'Breaks are used to block off time in Practitioner’s diary for Lunch, Tea Break, Meeting, etc',
      isActive: false,
    },
    {
      id: ApplicationDataEnum.Case,
      name: 'Case',
      title: 'You haven’t added a common Case.',
      description:
        'A case is a set of standard criteria for classifying whether a person has a particular disease, syndrome, or other health condition. You can create commonly handled cases in your business location here. It may be General, COVID etcPre-defined values: General',
      isActive: false,
    },
  ];

  name: string;
  title: string;
  description: string;
  tabId: number;
  tabName: string;
  public opened = true;
  locationData: LocationModel[] = [];
  activeState: boolean;
  // public gridData: any[] = [];
  public masterData: ApplicationDataModel[] = [];
  public masterAllData: ApplicationDataModel[] = [];
  public categoryId: number;
  public applicationId: number;
  nodata: boolean;
  public actionsLayout = 'normal';
  active: string;

  applicationDataForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    categoryName: new FormControl('', [Validators.required]),
    categoryId: new FormControl(0),
    isAllLocation: new FormControl(false),
    status: new FormControl(true),
    locationName: new FormControl('', Validators.required),
  });
  fromState: any;
  stateValues: any;

  constructor(
    protected router: Router,
    public appState: AppState,
    public location: Location,
    private businessService: BusinessService,
    public _route: ActivatedRoute,
    public applicationDataService: ApplicationDataService,
    private alertService: AppAlertService
  ) {
    super();
    if (this.router.getCurrentNavigation() != null) {
      this.fromState = this.router.getCurrentNavigation().extras.state.fromState;
      this.stateValues = this.fromState ? this.router.getCurrentNavigation().extras.state.stateValues : null;
    } else {
      this.fromState = history.state.fromState;
      this.stateValues = this.fromState ? history.state.stateValues : null;
    }
  }

  public ngOnInit() {
    this.opened = false;
    this.categoryId = ApplicationDataEnum.Title;
    this.active = 'active';
    this.nodata = false;
    this.activeState = true;
    // if (this.applicationDataForm.get('isAllLocation').value == '') {
    //   this.applicationDataForm.get('isAllLocation').patchValue(false);
    // }
    this.getApplicationdata(ApplicationDataEnum.Title, 'Title');
    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((data) => {
        data.map((d) => {
          const m = new LocationModel();
          m.locationId = d.id;
          m.locationName = d.locationName;
          this.locationData.push(m);
        });
      });
  }

  ngAfterViewInit(): void {
    if(this.fromState){
      this.opened = true;
      this.name = 'case';
      this.applicationId = 0;
      this.getApplicationdata(ApplicationDataEnum.Case, 'Case');
    }
  }

  getApplicationdata(id: number, name: string) {
    this.applicationDataService
      .getAllApplicationDataByLocation(this.appState.selectedUserLocationId)
      .subscribe((data) => {
        data.map(value => {
          value['isStatus'] = value.status;
          if (value.isAllLocation) {
            value.allLocationName = 'All Locations';
          } else {
            value.allLocationName = value.applicationDataLocation.map(value1 => value1.locationName).join(', ');
          }
        });

        this.masterAllData = data;

        // console.log(data);
        this.clickTabs(id, name, 1);
      });
  }

  titleActiveChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.active = 'active';
      this.activeState = true;
      this.gridData = this.masterData = this.masterAllData.filter(
        (x) => x.categoryId === this.categoryId && x.status === true
      );
      this.setActiveFilter();
    } else {
      this.active = 'inactive';
      this.activeState = false;
      this.gridData = this.masterData = this.masterAllData.filter(
        (x) => x.categoryId === this.categoryId && x.status === false
      );
      this.setInactiveFilter();
    }
    this.loadItems('categoryName');

    if (this.masterData.length <= 0) {
      this.nodata = false;
    } else {
      this.nodata = true;
    }
  }

  clickTabs(id: number, name: string, check: number) {
    if (name == 'Case') {
      name = 'Case (Patient)';
    }
    this.tabId = id;
    this.tabName = name;
    if (check === 0) {
      this.activeState = true;
      this.active = 'active';
    }
    this.noDataMessage.map((x) => {
      if (x.id === id) {
        x.isActive = true;
      } else {
        x.isActive = false;
      }
    });
    // console.log(name, this.activeState);
    // const appData = this.masterAllData.find((x) => x.categoryName === name);
    // // console.log(appData);
    // if (appData !== undefined) {
    this.gridData = this.masterData = this.masterAllData.filter((x) => x.categoryId === id && x.status === this.activeState);
    // console.log(this.masterData);
    if (this.masterData.length <= 0) {
      this.nodata = false;
    } else {
      this.nodata = true;
    }

    this.categoryId = id;
    // }
    const msg = this.noDataMessage.find((x) => x.id == id);
    this.name = msg.name;
    this.title = msg.title;
    this.description = msg.description;
    this.loadItems('categoryName');
  }

  isDefaultData: boolean = false;

  openPopUp(id: number) {
    this.isDefaultData = false;
    this.applicationDataForm.reset();
    this.applicationId = id;
    const locationName = [];
    if (id === 0) {
      this.applicationDataForm.get('status').patchValue(true);
      const loc = this.locationData.filter((x) => x.locationId === this.appState.selectedUserLocationId);

      locationName.push(loc);
      this.applicationDataForm.get('locationName').patchValue(loc);
    } else {
      const d = this.masterData.find((x) => x.id === id);
      this.isDefaultData = !d['parentBusinessId'];
      if (this.isDefaultData) {
        this.applicationDataForm.get('categoryName').setValidators(Validators.maxLength(30));
        this.applicationDataForm.get('locationName').setValidators(null);
      } else {
        this.applicationDataForm.get('categoryName').setValidators([Validators.required, Validators.maxLength(30)]);
        this.applicationDataForm.get('locationName').setValidators(Validators.required);
      }
      this.applicationDataForm.get('categoryName').updateValueAndValidity();
      this.applicationDataForm.get('locationName').updateValueAndValidity();

      this.applicationDataForm.patchValue(d);
      this.onApplyLocationChanged(d.isAllLocation);

      d.applicationDataLocation.forEach((l) => {
        const loc = this.locationData.find(
          (x) => x.locationId === l.locationId
        );
        if (loc !== undefined) {
          locationName.push(loc);
        }
      });
      this.applicationDataForm.get('locationName').patchValue(locationName);
    }
    this.opened = true;
  }

  public close() {
    this.opened = false;
    this.applicationDataForm.reset();
  }

  public onDialogClose() {
    this.opened = false;
    this.applicationDataForm.reset();
  }

  onApplyLocationChanged($event: boolean) {
    if ($event === false) {
      this.applicationDataForm.get('locationName').patchValue('');
      this.applicationDataForm.get('locationName').setValidators(Validators.required);
      this.applicationDataForm.get('locationName').updateValueAndValidity();
    } else {
      this.applicationDataForm.get('locationName').setValidators(null);
      this.applicationDataForm.get('locationName').updateValueAndValidity();
    }
  }

  public onSaveData() {
    const isAllowAllLocation = this.applicationDataForm.get('isAllLocation').value;
    if (isAllowAllLocation) {
      this.applicationDataForm.get('locationName').setValidators(null);
      this.applicationDataForm.get('locationName').updateValueAndValidity();
    }
    if (this.applicationDataForm.invalid) {
      this.applicationDataForm.markAllAsTouched();
      this.applicationDataForm.updateValueAndValidity();
      return;
    }
    // this.blockUI.start();
    this.submitting = true;

    const applicationDataModel: ApplicationDataModel = this.applicationDataForm.value;

    const locations: ApplicationDataLocation[] = [];
    if (!isAllowAllLocation && applicationDataModel.locationName !== null && applicationDataModel.locationName !== undefined) {
      applicationDataModel.locationName.forEach(
        (element: { locationId: string; locationName: string }) => {
          const m = new ApplicationDataLocation();
          m.locationId = element.locationId;
          m.locationName = element.locationName;
          locations.push(m);
        }
      );
      applicationDataModel.applicationDataLocation = locations;
    }

    // if (!applicationDataModel.isAllLocation) {
    //   applicationDataModel.isAllLocation = false;
    // } else {
    //   applicationDataModel.isAllLocation = true;
    //   if (this.locationData !== undefined && this.locationData.length > 0) {
    //     this.locationData.forEach(
    //       (element: { locationId: string; locationName: string }) => {
    //         const m = new ApplicationDataLocation();
    //         m.locationId = element.locationId;
    //         m.locationName = element.locationName;
    //         locations.push(m);
    //       }
    //     );
    //     applicationDataModel.applicationDataLocation = locations;
    //   }
    // }

    applicationDataModel.categoryId = this.categoryId;
    applicationDataModel.isEditable = !this.isDefaultData;
    applicationDataModel.isAllLocation = !!isAllowAllLocation;
    if (isAllowAllLocation) {
      delete applicationDataModel.locationName;
    }
    if (this.applicationId === 0) {
      applicationDataModel.id = 0;
      this.applicationDataService
        .createApplicationData(applicationDataModel)
        .subscribe(
          (res) => {
            if (this.fromState) {
              const data = JSON.parse(this.stateValues);
              data.addEditScheduleData['caseId'] = res;
              this.stateValues = JSON.stringify(data);
              let fromState: NavigationExtras = {
                state: {
                  AppointmentTypeAdded: true,
                  stateValues: this.stateValues
                }
              };
              this.router.navigate(['/appointment'], fromState)
            }
            else {
              this.submitting = false;

              this.alertService.displaySuccessMessage(
                'Your new ' + applicationDataModel.categoryName + ' is created successfully.'
              );

              this.applicationDataForm.reset();
              this.getApplicationdata(this.tabId, this.tabName);
              this.blockUI.stop();
            }
          },
          (error) => {
            if (error.code == 'E0006') {
              this.alertService.displayErrorMessage('The same name already exists, please use a different name.');
            }else {
              this.alertService.displayErrorMessage('Failed to add Application Data, please try again later.');
            }
            this.submitting = false;
            this.blockUI.stop();
          }
        );
    } else {
      if (applicationDataModel.applicationDataLocation !== undefined) {
        applicationDataModel.applicationDataLocation.map((x) => {
          x.applicationDataId = this.applicationId;
        });
      }

      this.applicationDataService
        .updateApplicationData(applicationDataModel)
        .subscribe(
          () => {
            this.submitting = false;
            this.alertService.displaySuccessMessage('Your ' + applicationDataModel.categoryName + ' is updated successfully.');
            this.applicationId = 0;
            this.applicationDataForm.reset();
            this.getApplicationdata(this.tabId, this.tabName);
            this.blockUI.stop();
          },
          (error) => {
            if (error.code == 'E0006') {
              this.alertService.displayErrorMessage('The same name already exists, please use a different name.');
            }else {
              this.alertService.displayErrorMessage('Failed to update Application Data, please try again later.');
            }

            this.submitting = false;
            this.blockUI.stop();
          }
        );
    }
    this.opened = false;
  }

  restricateSpecCharNum(ev) {
    if (!/^[a-zA-Z\s'-]*$/.test(ev.key)) {
      ev.preventDefault();
    }
  }

  get getMaxLength() {
    switch (this.categoryId) {
      case ApplicationDataEnum.Title:
        return 15;
    }
    return 50;
  }
}
