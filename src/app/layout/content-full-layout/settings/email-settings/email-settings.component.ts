import {Component, OnInit} from '@angular/core';
import {AppState} from '../../../../app.state';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseItemComponent} from '../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {EmailSettings} from '../interface/email-settings';
import {EmailSettingsService} from '../service/email-settings.service';
import {AppAlertService} from '../../../../shared/services/app-alert.service';

@Component({
    selector: 'app-email-settings',
    templateUrl: './email-settings.component.html',
    styleUrls: ['./email-settings.component.scss']
})
export class EmailSettingsComponent extends BaseItemComponent implements OnInit {

    constructor(
        public location: Location,
        protected appState: AppState,
        protected alertService: AppAlertService,
        private emailSettingsService: EmailSettingsService
    ) {
        super(location);
    }

    currentLocation: string;
    formGroup: FormGroup = new FormGroup({
        id: new FormControl(),
        status: new FormControl(true),
        displayName: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.required, Validators.email])
    });

    ngOnInit(): void {
        this.currentLocation = this.appState.selectedUserLocation.locationName;
        this.getEmailSettings();
    }

    save() {
        const data: EmailSettings = this.formGroup.value;

        data.parentBusinessId = this.appState.UserProfile.parentBusinessId;
        data.locationId = this.appState.selectedUserLocationId;

        if (!data.id) {
            this.emailSettingsService.createEmailSettings(data).subscribe(response => {
                if (response) {
                    this.alertService.displaySuccessMessage('Email Settings Saved Successfully.');
                }
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        } else {
            this.emailSettingsService.updateEmailSettings(data).subscribe(response => {
                if (response) {
                    this.alertService.displaySuccessMessage('Email Settings Updated Successfully.');
                }
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    private getEmailSettings() {
        this.emailSettingsService.getEmailSettingsByLocationId(this.appState.selectedUserLocationId).subscribe(data => {
            if (data) {
                this.formGroup.patchValue(data);
            }
        }, error => {
            this.alertService.displayErrorMessage(error);
        });
    }

}
