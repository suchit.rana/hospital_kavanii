import {Component, OnInit} from '@angular/core';
import {onSideNavChange, animateText} from '../../../animations/animations';
import {Location} from '@angular/common';
import {Router, NavigationEnd} from '@angular/router';
import {BusinessService} from '../../../services/app.business.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {SortDescriptor} from '@progress/kendo-data-query';
import {MenuModel} from '../../../models/app.menu.model';
import {BaseItemComponent} from '../../../shared/base-item/base-item.component';
import {PatientService} from '../../../services/app.patient.service';
import {RP_MODULE_MAP} from '../../../shared/model/RolesAndPermissionModuleMap';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css'],
    animations: [onSideNavChange, animateText]
})
export class SettingsComponent extends BaseItemComponent implements OnInit {
    readonly PERSONALISATION_MD = RP_MODULE_MAP.settings_personalisation;
    readonly COMMU_MD = RP_MODULE_MAP.settings_communications;
    readonly TEMP_MD = RP_MODULE_MAP.settings_templates;
    readonly DATA_MD = RP_MODULE_MAP.settings_import_export;
    readonly REMINDERS_MD = RP_MODULE_MAP.settings_reminder;
    readonly INT_MD = RP_MODULE_MAP.settings_integrations;

    @BlockUI() blockUI: NgBlockUI;

    isRoot: boolean = false;
    isError: boolean = false;
    isLoading: boolean = true;

    public multiple = false;
    public allowUnsort = true;
    public sort: SortDescriptor[] = [{
        field: 'id',
        dir: 'asc'
    }];


    public menus: MenuModel[] = [
        {
            name: 'Personalisation', moduleName: this.PERSONALISATION_MD, link: '', icon: '', hover: '', child: [
                {
                    name: 'Patient ID',
                    link: '/settings/personalize/patient',
                    icon: '',
                    hover: 'Patient ID'
                },
                {
                    name: 'Invoice ID',
                    link: '/settings/personalize/invoice',
                    icon: '',
                    hover: 'Invoice ID'
                },
                {
                    name: 'Payment ID',
                    link: '/settings/personalize/payment',
                    icon: '',
                    hover: 'Payment ID'
                }
            ]
        },
        {
            name: 'Communications', moduleName: this.COMMU_MD, link: '', icon: '', hover: '', child: [
                {name: 'SMS Account set up', link: '', icon: '', hover: 'SMS Account set up'},
                {name: 'SMS History', link: '', icon: '', hover: 'SMS History'},
                {name: 'SMS Settings', link: '/settings/smsSettings/', icon: '', hover: 'SMS Settings'},
                {name: 'Email Settings', link: '/settings/emailSettings/', icon: '', hover: 'Email Settings'}
            ]
        },
        {
            name: 'Templates', moduleName: this.TEMP_MD, link: '', icon: '', hover: '', child: [
                {name: 'SMS Templates', link: '/settings/smstemplate/', icon: 'add', hover: 'SMS'},
                {name: 'Email Templates', link: '/settings/emailtemplate/', icon: '', hover: 'Email'},
                {name: 'Letter Templates', link: '/settings/lettertemplate/', icon: '', hover: 'Letters'},
                {
                    name: 'Treatment Notes Template',
                    link: '/settings/treatmentnotestemplate',
                    icon: '',
                    hover: 'Treatment Notes Template'
                },
                {name: 'Invoice Template', link: '', icon: '', hover: 'Invoice Template'},
                {name: 'Intake Forms Template', link: '', icon: '', hover: 'Intake Forms Template'}
            ]
        },
        {
            name: 'Reminder', link: '', icon: '', hover: '', child: [
                {name: 'Reminder Settings', link: '/settings/reminder-settings', icon: '', hover: 'Reminder Settings'},
            ]
        },
        {
            name: 'Data ', moduleName: this.DATA_MD, link: '', icon: '', hover: '', child: [
                {name: 'Application Data', link: '/settings/applicationdata/', icon: '', hover: 'Application Data'},
                {name: 'Audit trial', link: '', icon: '', hover: 'Audit trial'},
                {name: 'Import', link: '', icon: '', hover: 'Import'},
                {name: 'Export', link: '', icon: '', hover: 'Export'},
                {name: 'Data Storage', link: '', icon: '', hover: 'Data Storage'},
            ]
        },
        {
            name: 'Integrations  ', moduleName: this.INT_MD, link: '', icon: '', hover: '', child: [
                {name: 'Online Booking', link: '', icon: '', hover: 'Online Booking'},
                {name: 'Medicare', link: '', icon: '', hover: 'Medicare'},
                {name: 'DVA', link: '', icon: '', hover: 'DVA'},
                {name: 'Xero', link: '', icon: '', hover: 'Xero'},
            ]
        }
    ];

    constructor(
        protected router: Router,
        public location: Location,
        private businessService: BusinessService,
        private patientService: PatientService
    ) {
        super(location);
    }

    ngOnInit() {
        this.isRoot = this.router.url == '/settings';
        if (this.isRoot) {
            this.populateLanding();
        }
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.isRoot = event.url == '/settings';
                if (this.isRoot) {
                    this.populateLanding();
                }
            }
        });

    }

    populateLanding() {
        this.displayMessage();
    }

    displayMessage() {
        if (this.patientService.sharedData !== undefined && this.patientService.sharedData !== '') {
            this.displaySuccessMessage(this.patientService.sharedData);
            this.patientService.sharedData = '';
        }

        setInterval(a => {
            this.displaySuccessMessage('');
        }, 5000, []);
    }
}
