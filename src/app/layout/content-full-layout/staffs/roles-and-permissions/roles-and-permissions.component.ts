import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AppState} from 'src/app/app.state';
import {RoleService} from 'src/app/services/app.role.service';
import {ModuleAccess, ModuleRoles, RoleModule, Roles} from '../../../../models/app.role.model';
import {MatButtonToggleChange} from '@angular/material';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {CustomeRoleComponent} from './custome-role/custome-role.component';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {DELETE, SAVE} from '../../../../shared/popup-component/popup-component.component';
import {SwitchComponent} from '@progress/kendo-angular-inputs';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {hasPermission} from '../../../../app.component';

@Component({
    selector: 'app-roles-and-permissions',
    templateUrl: './roles-and-permissions.component.html',
    styleUrls: ['./roles-and-permissions.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RolesAndPermissionsComponent implements OnInit {
    readonly ROLE_FIELD_NAME = 'customizeRole';
    readonly NONE = ModuleAccess.None;
    readonly PARTIAL = ModuleAccess.Partial;
    readonly FULL = ModuleAccess.Full;

    moduleName = RP_MODULE_MAP.staff_roles_permission;

    @ViewChild('resetConfirmation', {static: false}) resetConfirmation;

    roles: Roles[] = [];
    data: RoleModule[];
    isLoading: boolean = false;

    constructor(
        private appAlertService: AppAlertService,
        private dialogService: AppDialogService,
        private appState: AppState,
        private roleService: RoleService
    ) {
    }

    ngOnInit() {
        this.loadAccessAndRoles();
    }

    changeAccess($event: MatButtonToggleChange, obj: ModuleRoles, parentModuleId: string, role: Roles) {
        const selectedValue = $event.value as string[];
        obj.modulePartialAccess = selectedValue.join('');
        const parentModule = this.data.find(x => x.moduleId == parentModuleId);
        const hasDeleteAccess = obj.modulePartialAccess.includes('D');
        const hasViewAccess = obj.modulePartialAccess.includes('V');
        const hasModifyAccess = obj.modulePartialAccess.includes('C') && obj.modulePartialAccess.includes('C');
        if (hasDeleteAccess && hasViewAccess && hasModifyAccess) {
            obj.moduleRoleAccess = this.FULL;
        } else if (!hasViewAccess && !hasModifyAccess && !hasDeleteAccess) {
            obj.moduleRoleAccess = this.NONE;
        } else {
            obj.moduleRoleAccess = this.PARTIAL;
        }
        const childModules = this.data.filter(x => x.parentModuleId == parentModule.moduleId);
        const parentModuleAccess = this.getChildModuleAccess(childModules, role);
        const fields = role.field.split('.');
        const hasFullAccess = parentModuleAccess.filter(x => x === this.FULL);
        const hasNoneAccess = parentModuleAccess.filter(x => x === this.NONE);
        let partialAccess = '';
        if (hasFullAccess.length === childModules.length) {
            parentModule[fields[0]][fields[1]].moduleRoleAccess = this.FULL;
            partialAccess = 'CVED';
        } else if (hasNoneAccess.length === childModules.length) {
            parentModule[fields[0]][fields[1]].moduleRoleAccess = this.NONE;
            partialAccess = '';
        } else {
            parentModule[fields[0]][fields[1]].moduleRoleAccess = this.PARTIAL;
            partialAccess = 'CVE';
        }

        const payload = [
            {
                id: obj.businessRoleModulesAccessId,
                moduleRoleAccess: obj.moduleRoleAccess,
                modulePartialAccess: obj.modulePartialAccess
            },
            {
                id: parentModule[fields[0]][fields[1]].businessRoleModulesAccessId,
                moduleRoleAccess: parentModule[fields[0]][fields[1]].moduleRoleAccess,
                modulePartialAccess: partialAccess
            }
        ];

        this.saveRoles(payload);
    }

    parentModuleValueChange(dataItem: ModuleRoles, $event: boolean, moduleId: string, role: Roles, ctrl: SwitchComponent) {
        let partialAccess = '';
        if ($event) {
            if (dataItem.moduleRoleAccess === this.PARTIAL) {
                dataItem.moduleRoleAccess = this.FULL;
                partialAccess = 'CVED';
            } else {
                dataItem.moduleRoleAccess = this.PARTIAL;
                ctrl.checked = false;
                partialAccess = 'CVE';
            }
        } else {
            dataItem.moduleRoleAccess = this.NONE;
            partialAccess = '';
        }
        const childModules = this.data.filter(x => x.parentModuleId == moduleId);
        const payload = [
            {
                id: dataItem.businessRoleModulesAccessId,
                moduleRoleAccess: dataItem.moduleRoleAccess,
                modulePartialAccess: partialAccess
            }
        ];
        const fields = role.field.split('.');
        if (childModules && childModules.length > 0) {
            childModules.forEach(childModule => {
                const roleModule = childModule[fields[0]][fields[1]] as ModuleRoles;
                roleModule.moduleRoleAccess = dataItem.moduleRoleAccess;
                roleModule.modulePartialAccess = partialAccess;
                payload.push({
                    id: roleModule.businessRoleModulesAccessId,
                    moduleRoleAccess: dataItem.moduleRoleAccess,
                    modulePartialAccess: partialAccess
                });
            });
        }
        this.saveRoles(payload);
    }

    resetAccess() {
        this.dialogService.open(this.resetConfirmation, {
            title: 'DO YOU WISH TO RESET?',
            width: '778px',
            height: '300px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
            bodyPadding: true
        }).clickSave.subscribe(() => {
            this.roleService.resetBusinessRoleModuleAccess().subscribe(response => {
                if (response) {
                    this.appState.fetchRolesAndPermission();
                    this.appAlertService.displaySuccessMessage('Permissions for all default roles has been reverted back to original settings successfully.');
                    this.loadAccessAndRoles();
                }
            }, error => {
                this.appAlertService.displaySuccessMessage(error || 'Failed to reset. Please try again later.');
            });
        });
    }

    addEditCustomRole(role?: Roles) {
        CustomeRoleComponent.open(this.dialogService, role).close.subscribe(value => {
            if (value == SAVE || value == DELETE) {
                this.loadAccessAndRoles();
            }
        });
    }

    hasPermission(moduleName: string, permission: string | string[]) {
        return hasPermission(this.appState, moduleName, permission);
    }

    private saveRoles(payload: { id: string, moduleRoleAccess: number, modulePartialAccess: string }[]) {
        this.roleService.updateBusinessRoleModulesAccess(payload).subscribe(response => {
            if (response) {
                this.appState.fetchRolesAndPermission();
            }
        }, error => {
            this.appAlertService.displayErrorMessage(error || 'Failed to update access.');
        });
    }

    private getChildModuleAccess(childModules: RoleModule[], role: Roles): ModuleAccess[] {
        const parentModuleAccess = [];
        const fields = role.field.split('.');
        childModules.forEach((childModule) => {
            const childModuleRole = childModule[fields[0]][fields[1]];
            const hasDeleteAccess = childModuleRole.modulePartialAccess.includes('D');
            const hasViewAccess = childModuleRole.modulePartialAccess.includes('V');
            const hasModifyAccess = childModuleRole.modulePartialAccess.includes('C') && childModuleRole.modulePartialAccess.includes('C');
            if (hasDeleteAccess && hasViewAccess && hasModifyAccess) {
                parentModuleAccess.push(this.FULL);
            } else if (!hasViewAccess && !hasModifyAccess && !hasDeleteAccess) {
                parentModuleAccess.push(this.NONE);
            } else {
                parentModuleAccess.push(this.PARTIAL);
            }
        });
        return parentModuleAccess;
    }

    private getModuleAccess(obj: ModuleRoles, parentModuleId: string) {
        const hasDeleteAccess = obj.modulePartialAccess.includes('D');
        const hasViewAccess = obj.modulePartialAccess.includes('V');
        const hasModifyAccess = obj.modulePartialAccess.includes('C') && obj.modulePartialAccess.includes('C');
        const parentModule = this.data.find(x => x.moduleId == parentModuleId);
        if (hasDeleteAccess && hasViewAccess && hasModifyAccess) {
            return obj.moduleRoleAccess = this.FULL;
        } else if (!hasViewAccess && !hasModifyAccess && !hasDeleteAccess) {
            return obj.moduleRoleAccess = this.NONE;
        } else {
            return obj.moduleRoleAccess = this.PARTIAL;
        }
    }

    private loadAccessAndRoles() {
        this.isLoading = true;
        this.roleService.getAllRoles().subscribe(roles => {
            this.roles = [...roles as Roles[]];
            this.roles.forEach(value => {
                value.field = this.ROLE_FIELD_NAME + '.' + value.roleName.toSlug();
            });
        });

        this.roleService.getAllBusinessAccessByModule().subscribe(response => {
            if (response && response.length > 0) {
                response.forEach(data => {
                    const newRoleObj = {};
                    data.roles.forEach(role => {
                        newRoleObj[role.roleName.toSlug()] = role;
                    });
                    data['customizeRole'] = newRoleObj;
                });
                this.data = [...response];
                this.isLoading = false;
            }
        });
    }
}


