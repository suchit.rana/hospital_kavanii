import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {CANCEL, DELETE, SAVE} from '../../../../../shared/popup-component/popup-component.component';
import {Roles} from '../../../../../models/app.role.model';
import {FormControl, Validators} from '@angular/forms';
import {RoleService} from '../../../../../services/app.role.service';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-custome-role',
  templateUrl: './custome-role.component.html',
  styleUrls: ['./custome-role.component.css']
})
export class CustomeRoleComponent implements OnInit {
  moduleName = RP_MODULE_MAP.staff_roles_permission;

  private static ref: IPopupAction;

  @Input()
  role: Roles;

  @ViewChild('deleteConfirmation', {static: false}) deleteConfirmation;
  @ViewChild('deleteError', {static: false}) deleteError;

  roleNameFC = new FormControl('', Validators.required);
  isLoading: boolean = false;
  deleteErrorMsg: string = '';

  constructor(
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
    private roleService: RoleService
  ) {
  }

  ngOnInit() {
    if (this.role) {
      this.roleNameFC.setValue(this.role.roleName);
    }
  }

  delete() {
    this.appDialogService.open(this.deleteConfirmation, {
      title: 'DO YOU WISH TO DELETE?',
      width: '778px',
      height: '300px',
      cancelBtnTitle: 'No',
      saveBtnTitle: 'Yes',
      bodyPadding: true
    }).clickSave.subscribe(() => {
      this.roleService.deleteCustomRole(this.role.id).subscribe(response => {
        this.alertService.displaySuccessMessage('Custom role deleted successfully.');
        this.isLoading = false;
        this.ref.closeDialog(DELETE);
      }, error => {
        this.deleteErrorMsg = error;
        this.appDialogService.open(this.deleteConfirmation, {
          title: 'CUSTOM ROLE CAN NOT BE DELETED?',
          width: '778px',
          height: '300px',
          hideCancel: true,
          saveBtnTitle: 'Ok',
          bodyPadding: true
        });
        this.isLoading = false;
      });
    });
  }

  cancel() {
    this.ref.closeDialog(CANCEL);
  }

  save() {
    if (this.roleNameFC.invalid) {
      this.roleNameFC.markAsTouched();
      return;
    }
    this.isLoading = true;
    if (!this.role) {
      this.roleService.addCustomRole(this.roleNameFC.value).subscribe(response => {
        this.alertService.displaySuccessMessage('Custom role created successfully.');
        this.isLoading = false;
        this.ref.closeDialog(SAVE);
      }, error => {
        this.alertService.displayErrorMessage(error || 'Failed to create custom role. Please try again later.');
        this.isLoading = false;
      });
    } else {
      this.roleService.editCustomRole(this.role.id, this.roleNameFC.value).subscribe(response => {
        this.alertService.displaySuccessMessage('Custom role updated successfully.');
        this.isLoading = false;
        this.ref.closeDialog(SAVE);
      }, error => {
        this.alertService.displayErrorMessage(error || 'Failed to update custom role. Please try again later.');
        this.isLoading = false;
      });
    }
  }

  get ref() {
    return CustomeRoleComponent.ref;
  }

  static open(
    dialogService: AppDialogService,
    role: Roles
  ): IPopupAction {
    this.ref = dialogService.open(CustomeRoleComponent, {
      title: role ? 'Edit Custom Role' : 'Add Custom Role',
      width: '500px',
      hideFooter: true,
      bodyPadding: true,
      data: {'role': role}
    });
    return this.ref;
  }
}
