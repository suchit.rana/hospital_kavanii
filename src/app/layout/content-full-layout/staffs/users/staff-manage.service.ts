import {Injectable} from '@angular/core';
import {StaffModel} from '../../../../models/app.staff.model';

@Injectable()
export class StaffManageService {

  private _recentlySavedStaff: StaffModel;
  private _isAddingStaff: boolean = true;

  constructor() {
  }

  get recentlySavedStaff(): StaffModel {
    return this._recentlySavedStaff;
  }

  set recentlySavedStaff(value: StaffModel) {
    this._recentlySavedStaff = value;
  }

  get isAddingStaff(): boolean {
    return this._isAddingStaff;
  }

  set isAddingStaff(value: boolean) {
    this._isAddingStaff = value;
  }
}
