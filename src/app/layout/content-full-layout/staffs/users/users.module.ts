import {DialogModule} from '@progress/kendo-angular-dialog';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UsersRoutingModule} from './users-routing.module';
import {MatAutocompleteModule, MatDatepickerModule, MatDialogModule, MatPaginatorModule, MatSelectModule} from '@angular/material';
import {AppointmentOptionsComponent} from './appointment-options/appointment-options.component';
import {UsersComponent} from './users.component';
import {AddUserComponent} from './addUser/addUser.component';
import {AppCommonModule} from '../../common/app-common.module';
import {RolesAndPermissionsComponent} from '../roles-and-permissions/roles-and-permissions.component';
import {BlockUIModule} from 'ng-block-ui';
import {SharedContentFullLayoutModule} from '../../shared-content-full-layout/shared-content-full-layout.module';
import {OrderModule} from 'ngx-order-pipe';
import {ManagingModule} from '../../../module/managing.module';
import {PractitionerDetailsOptionsComponent} from './practitioner-details-options/practitioner-details-options.component';
import {RoleAndLocationComponent} from './role-and-location/role-and-location.component';
import {DxCalendarModule} from 'devextreme-angular';
import {AppCalenderPopupModule} from '../../common/calender-popup/app-calender-popup.module';
import {AddUserPersonalInformationComponent} from './add-edit-staff/add-user-personal-information/add-user-personal-information.component';
import {AddEditUserRoleAndLocationComponent} from './addUser/add-edit-user-role-and-location/add-edit-user-role-and-location.component';
import {AddEditStaffComponent} from './add-edit-staff/add-edit-staff.component';
import {AddEditStaffRightSidePanelComponent} from './add-edit-staff/add-edit-staff-right-side-panel/add-edit-staff-right-side-panel.component';
import {AddEditStaffRoleAndLocationsComponent} from './add-edit-staff/add-edit-staff-role-and-locations/add-edit-staff-role-and-locations.component';
import {AppKendoMultiSelectModule} from '../../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import {AddEditPractitionerConfigComponent} from './add-edit-staff/add-edit-practitioner-config/add-edit-practitioner-config.component';
import {AddEditStaffOthetInfoComponent} from './add-edit-staff/add-edit-staff-othet-info/add-edit-staff-othet-info.component';
import {StaffManageService} from './staff-manage.service';
import {CharacterLimitModule} from '../../../../directive/character-limit/character-limit.module';
import {AppDatePickerModule} from '../../common/app-date-picker/app-date-picker.module';
import {TrimValueAccessorModule} from 'ng-trim-value-accessor';
import {AddressFieldModule} from '../../shared-content-full-layout/address-field/address-field.module';
import {BodyModule, HeaderModule, SharedModule, TreeListModule} from '@progress/kendo-angular-treelist';
import { CustomeRoleComponent } from '../roles-and-permissions/custome-role/custome-role.component';
import {AccessDirectiveModule} from '../../../../shared/directives/access-directive/access-directive.module';


@NgModule({
  declarations: [
    UsersComponent,
    AddUserComponent,
    AppointmentOptionsComponent,
    RolesAndPermissionsComponent,
    PractitionerDetailsOptionsComponent,
    RoleAndLocationComponent,
    AddUserPersonalInformationComponent,
    AddEditUserRoleAndLocationComponent,

    AddEditStaffComponent,
    AddEditStaffRightSidePanelComponent,
    AddEditStaffRoleAndLocationsComponent,
    AddEditPractitionerConfigComponent,
    AddEditStaffOthetInfoComponent,
    CustomeRoleComponent,
  ],
    imports: [
        CommonModule,
        AppCommonModule,
        SharedContentFullLayoutModule,
        ManagingModule,
        UsersRoutingModule,
        // MatFormFieldModule,
        // MatIconModule,
        MatSelectModule,
        MatPaginatorModule,
        BlockUIModule.forRoot(),
        // GooglePlaceModule,
        DialogModule,
        // MatCheckboxModule,
        // MatSidenavModule,
        // MatToolbarModule,
        // MatDividerModule,
        // MatMenuModule,
        // MatListModule,
        // MatTooltipModule,
        MatDatepickerModule,
        // MatSlideToggleModule,
        // MatTabsModule,
        // MatRadioModule,
        // ReactiveFormsModule,
        // ColorPickerModule,
        // FormsModule,
        // MatInputModule
        OrderModule,
        MatDialogModule,
        MatAutocompleteModule,
        DxCalendarModule,
        AppCalenderPopupModule,
        AppKendoMultiSelectModule,
        CharacterLimitModule,
        AppDatePickerModule,
        TrimValueAccessorModule,
        AddressFieldModule,
        SharedModule,
        BodyModule,
        TreeListModule,
        BodyModule,
        SharedModule,
        HeaderModule,
        AccessDirectiveModule,
    ],
  exports:[
    AddEditStaffComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [PractitionerDetailsOptionsComponent, CustomeRoleComponent],
  providers: [StaffManageService]
})
export class UsersModule {
}
