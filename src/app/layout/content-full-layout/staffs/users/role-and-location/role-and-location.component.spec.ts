import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleAndLocationComponent } from './role-and-location.component';

describe('RoleAndLocationComponent', () => {
  let component: RoleAndLocationComponent;
  let fixture: ComponentFixture<RoleAndLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAndLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAndLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
