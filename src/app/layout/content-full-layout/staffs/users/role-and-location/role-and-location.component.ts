import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { LocationGridModel } from '../../../../../models/app.location.model';
import { StaffService } from '../../../../../services/app.staff.service';
import { map } from 'rxjs/operators';
import { SettingsService } from '../../../../../services/app.settings.service';
import { RoleService } from '../../../../../services/app.role.service';
import { RoleModel } from '../../../../../models/app.role.model';
import { FormGroup } from '@angular/forms';
import { BusinessService } from '../../../../../services/app.business.service';
import { AppState } from '../../../../../app.state';
import { RoleLocation } from '../addUser/addUser.component';
import { Component, Input, OnInit } from '@angular/core';
import * as _ from "lodash";

@Component({
  selector: 'app-role-and-location',
  templateUrl: './role-and-location.component.html',
  styleUrls: ['./role-and-location.component.css']
})
export class RoleAndLocationComponent implements OnInit {
  selectedSpeciality: any;
  roleLocations: RoleLocation[] = [];
  locationInterval: any;
  locationList: any = [];
  apperance: string = "outline";
  roles: Array<RoleModel> = [];
  specialities: any = [];
  serviceType: any;
  practitioners: any[];
  @Input() roleAndLoactionForm: FormGroup;
  @Input() otherInfoRolePermissionForm: FormGroup;
  @Input() backtoPage: boolean;
  hasPractitionerRole: boolean = false;
  rolesSearchList: any = [];
  showPopup: boolean = false;
  whichPopup: string = '';
  popupHeaderTitle: string = '';
  popupBodyContent: string = '';
  popupBodyContent1: string = '';
  popupBodyContent2: string = '';
  popupBodyContent3: string = '';
  popupBodyContent4: string = '';
  roleLocationArray: any;
  otherIdsArray: any;
  otherNameList: any = [];
  otherSpecialityArray: any;
  selectedArrayIndex = null;
  selectedIndex: any;
  constructor(
    public appState: AppState,
    public businessService: BusinessService,
    public roleService: RoleService,
    public settingService: SettingsService,
    private staffService: StaffService,
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit() {
    this.getUserLoactions();
    this.populateLocationList();
    this.getAllRoles();
    this.getAllSpecialities();
    this.getAllPractitioners();
    this.getServiceType()
    console.log(this.backtoPage)
    this.roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
    if (this.backtoPage) {

    } else {
      // this.roleLocationArray.clear();
      this.roleLocationArray.push(this.formBuilder.group({
        location: ['', Validators.required],
        role: ['', Validators.required],
        serviceType: [''],
        specialities: [''],
        practitioner: [''],
        otherSpecialityArray: new FormArray([])
      }));
    }
  }

  getRoleandLocationArray(form) {
    return form.controls.roleLocationArray.controls as FormArray;
  }

  getotherSpecialityArrayControl(form, index) {
    return <FormArray>this.roleAndLoactionForm.get('roleLocationArray')['controls'][index].get('otherSpecialityArray')['controls'];
  }

  getotherSpecialityArray(index) {
    return <FormArray>this.roleAndLoactionForm.get('roleLocationArray')['controls'][index].get('otherSpecialityArray');
  }

  getServiceType() {
    this.serviceType = [{
      id: 0,
      serviceName: 'General'
    },
    {
      id: 1,
      serviceName: 'Specialist'
    },
    {
      id: 2,
      serviceName: 'Pathology'
    },
    {
      id: 4,
      serviceName: 'Optical Service Provider'
    },
    {
      id: 9,
      serviceName: 'Radiology'
    },
    {
      id: 10,
      serviceName: 'Diagonstics'
    }]
  }

  getUserLoactions() {
    if (this.appState.selectedUserLocation) {
      let rl: RoleLocation = new RoleLocation();
      rl.location = this.appState.selectedUserLocation;
      rl.role = null;
      rl.options = [];
      rl.practitioners = [];
      this.roleLocations.push(rl);
    }
  }

  populateLocationList() {
    this.locationList = [];
    this.businessService.getLocationsByBusiness(this.appState.userProfile.parentBusinessId).subscribe(locations => {
      this.locationList = locations;
      console.log(this.locationList)
      this.roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
      console.log(this.roleLocationArray)
      for (let index = 0; index < this.locationList.length; index++) {
        this.locationList[index].disabled = false;
        if (this.locationList[index].id == this.appState.selectedUserLocation.id) {
          this.roleLocationArray.controls[0].controls['location'].setValue(this.locationList[index].id)
          this.locationList[index].disabled = true;
        }
      }
    }, (err => {

    }));
  }

  selectedLocation(location, index) {
    console.log(location, index)
    this.locationList[index].disabled = true;
  }

  getAllRoles() {
    this.roles = [];
    this.rolesSearchList = [];
    this.roleService.getAllRoles().subscribe(roles => {
      roles.splice(roles.findIndex(data => data.roleName == "Owner"), 1)
      this.roles = roles;
      this.rolesSearchList = roles
    }, (err => {

    }));
  }

  getAllSpecialities() {
    this.specialities = [];
    this.settingService.getAllSpecialties()
      .pipe(map(data => data.filter(d => d.isStatus === true)))
      .subscribe((res: any) => {
        for (let index = 0; index < res.length; index++) {
          res[index].provider_no = null;
          res[index].disabled = false;
          res[index].index = index;
        }
        this.specialities = res;
      }, (err => {

      }));
  }

  getAllPractitioners() {
    this.practitioners = [];
    this.staffService.getPractitioners().subscribe(p => {
      this.practitioners = p;
    }, (err => {

    }));
  }

  //add and remove the another location
  addRoleLocation() {
    this.roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
    this.roleLocationArray.push(this.formBuilder.group({
      location: ['', Validators.required],
      role: ['', Validators.required],
      specialities: [''],
      providerNumber: [''],
      practitioner: [''],
      otherSpecialityArray: new FormArray([])
    }));
  }

  getProceededLocationList(): LocationGridModel[] {
    let clonedLocations: LocationGridModel[] = this.appState.clone(this.locationList);
    this.roleLocations.forEach(rl => {
      if (rl && rl.location && rl.location.id) {
        let idx = clonedLocations.findIndex(l => l.id == rl.location.id);
        if (idx > -1) {
          clonedLocations.splice(idx, 1);
        }
      }
    });
    return clonedLocations;
  }

  public roleChanged(value: any[], fromTo: any): void {
    this.hasPractitionerRole = false;
    let data = _.find(value, { roleName: 'Practitioner' })
    if (data != undefined && data != null && data != '') {
      this.hasPractitionerRole = true;
    }
  }

  //filter role
  handleFilterRole(event) {
    if (event.target.value != '') {
      this.rolesSearchList = this.roles.filter(
        (s) => s.roleName.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.rolesSearchList = this.roles
    }
  }

  //add other id pop-up
  openPopup(whichPopup, index) {
    this.showPopup = true;
    this.selectedArrayIndex = index;
    this.whichPopup = whichPopup;
    if (whichPopup == 'addOtherIds') {
      this.popupHeaderTitle = "practitioner other ID's";
      this.popupBodyContent = "You can add Practitioner related Id's here. ";
      this.otherIdsArray = this.otherInfoRolePermissionForm.get('otherIdsArray') as FormArray;
      this.roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
      this.otherIdsArray.clear()
      if (this.roleLocationArray.controls[0].get('specialities').value[0] && this.roleLocationArray.controls[0].get('specialities').value[0].id) {
        for (let index = 0; index < this.roleLocationArray.controls[0].get('specialities').value.length; index++) {
          this.otherIdsArray.push(
            this.formBuilder.group({
              speciality: [this.roleLocationArray.controls[0].get('specialities').value[index]],
              name: [''],
              id_no: ['']
            })
          );
        }
      } else {
        let responseHandle = {
          message: "Treatment Note is duplicated successfully.",
          type: "success"
        }
        this.staffService.publishSomeData(JSON.stringify(responseHandle))
      }

    }
    else if (whichPopup == 'deleteOtherLocation') {
      this.popupHeaderTitle = "DO YOU WISH TO REMOVE THIS LOCATION?";
      this.popupBodyContent = "This staff will no longer be available for this location.";
    }
    else if (whichPopup == 'deleteOtherLocationError') {
      this.popupHeaderTitle = "THIS LOCATION CANNOT BE REMOVED";
      this.popupBodyContent = "This Practitioner has already created the below records for this location, hence you cannot remove this location for this Practitioner.";
      this.popupBodyContent1 = "Appointment(s)"
      this.popupBodyContent2 = "Treatment note(s)"
      this.popupBodyContent3 = "Letter(s)"
      this.popupBodyContent4 = "Invoice(s)"
    }
  }

  closePopups(whichPopup) {
    if (whichPopup == 'addOtherIds') {
    }
    else if (whichPopup == 'deleteOtherLocation') {
    }
    else if (whichPopup == 'deleteOtherLocationError') {
    }
    this.showPopup = false;
  }

  performAction(whichPopup) {
    this.showPopup = false;
    if (whichPopup == 'addOtherIds') {
      this.otherIdsArray = this.otherInfoRolePermissionForm.get('otherIdsArray') as FormArray;
      this.otherSpecialityArray = this.getotherSpecialityArray(this.selectedArrayIndex);
      this.otherSpecialityArray.clear()
      for (let index = 0; index < this.otherIdsArray.value.length; index++) {
        let data = _.find(this.otherNameList, { name: this.otherIdsArray.value[index].name })
        if (!data) {
          this.otherNameList.push({ name: this.otherIdsArray.value[index].name })
        }
        this.otherSpecialityArray.push(
          this.formBuilder.group({
            speciality: [this.otherIdsArray.value[index].speciality.id],
            name: [this.otherIdsArray.value[index].speciality.specialityName],
            provider_no: ['']
          })
        )
      }
    }
    else if (whichPopup == 'deleteOtherLocation') {
      this.roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
      if (this.roleLocationArray.length != 0) {
        this.locationList[this.selectedArrayIndex].disabled = false;
        for (let i = 0; i < this.locationList.length; i++) {
          if (this.locationList[i].id == this.appState.selectedUserLocation.id) {
            this.locationList[i].disabled = true;
          }
        }
        this.roleLocationArray.removeAt(this.selectedArrayIndex)
      } else {
        this.openPopup('deleteOtherLocationError', this.selectedArrayIndex)
      }
    }
  }

  addOtherIds() {
    this.otherIdsArray = this.otherInfoRolePermissionForm.get('otherIdsArray') as FormArray;
    this.otherIdsArray.push(
      this.formBuilder.group({
        speciality: [''],
        name: [''],
        id_no: ['']
      })
    );
  }

  removeOtherIds(index) {
    this.otherIdsArray = this.otherInfoRolePermissionForm.get('otherIdsArray') as FormArray;
    for (let i = 0; i < this.specialities.length; i++) {
      if (this.specialities[i].id == this.otherIdsArray.value[index].speciality.id) {
        this.specialities[i].disabled = false;
      }
    }
    this.otherIdsArray.removeAt(index)
  }

  changeServiceType(event, fromTo) {
  }

  changeSpecialityType(event, index) {
    this.otherSpecialityArray = this.getotherSpecialityArray(index);
    if (this.otherSpecialityArray.length != 0) {
      this.otherSpecialityArray.clear()
    }
    for (let index = 0; index < event.length; index++) {
      this.otherSpecialityArray.push(
        this.formBuilder.group({
          speciality: [event[index].id],
          name: [event[index].specialityName],
          provider_no: [event[index].provider_no]
        })
      )
    }
    for (let i = 0; i < this.specialities.length; i++) {
      this.specialities[i].disabled = false;
      for (let index = 0; index < event.length; index++) {
        if (this.specialities[i].id == event[index].id) {
          this.specialities[i].disabled = true;
        }
      }
    }
  }
  public itemDisabled(itemArgs: { dataItem: any; index: number }): boolean {
    return itemArgs.dataItem.disabled;
  }

  handleFilterName(event) {
  }

  selectedSpecialityObj(speciality) {
    for (let i = 0; i < this.specialities.length; i++) {
      if (this.specialities[i].id == speciality.id) {
        this.specialities[i].disabled = true;
      }
    }
  }
}
