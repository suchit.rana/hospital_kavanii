import {Component, OnInit} from '@angular/core';
import {BaseGridComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {StaffService} from 'src/app/services/app.staff.service';
import {MatButtonToggleChange} from '@angular/material';
import {Location} from '@angular/common';
import {AppState} from '../../../../app.state';
import {ApplicationUsersLocationModel} from '../../../../models/app.staff.model';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {MessageType} from '../../../../models/app.misc';
import {StaffManageService} from './staff-manage.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {hasPermission} from '../../../../app.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent extends BaseGridComponent implements OnInit {

  readonly STAFF_MEMBER = RP_MODULE_MAP.staff_members;
  readonly STAFF_ROLE_N_PERMISSION = RP_MODULE_MAP.staff_roles_permission;

  apperance: string = 'outline';

  status: string = 'active';
  displaySelection: string = 'list';
  staffSelection: string = 'All';
  isOpenSchedulerGuidePopup: boolean = false;
  recentlyAddedData;

  constructor(public location: Location, protected appState: AppState,
              protected router: Router,
              private staffService: StaffService, private staffManageService: StaffManageService) {
    super();
  }

  ngOnInit() {
    this.removeFilters();
    this.recentlyAddedData = _.cloneDeep(this.staffManageService.recentlySavedStaff);
    const isAddingStaff = this.staffManageService.isAddingStaff;
    const locList = this.appState.locationList || [];
    this.staffManageService.recentlySavedStaff = null;
    if (this.recentlyAddedData) {
      this.recentlyAddedData.applicationUsersLocationModel.forEach(value => {
        const location = locList.find(value1 => value1.id === value.locationId);
        if (location) {
          value['locationName'] = location.locationName;
        }
      });
      const message = isAddingStaff ? 'Staff created successfully.' : 'Staff updated successfully.';
      this.displaySuccessMessage(message);
      if(this.recentlyAddedData.hasPractitionerRole){
        this.isOpenSchedulerGuidePopup = isAddingStaff;
      }
    }
    this.staffService.getAllStaffsList().subscribe((data: any[]) => {
      console.log(data);
      data.forEach(value => {
        value['licence'] = 'No';
        value.location.forEach(location => {
          if (location.roles) {
            let hasPttrRole = false;
            location.roles.forEach(role => {
              if (role.roleName.toLowerCase() == 'practitioner') {
                hasPttrRole = true;
              }
            });
            if (hasPttrRole) {
              value['licence'] = 'Yes';
            } else {
              value['licence'] = 'No';
            }
          }
        });
      });
      this.gridData = data;
      this.setActiveFilter();
      this.loadItems();
    });
  }

  staffActiveChanged(event: MatButtonToggleChange) {
    if (event.value == 'active') {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }
    this.loadItems();
  }

  setActive() {
    this.setActiveFilter();
  }

  setInactive() {
    this.setInactiveFilter();
  }

  setupSchedule(data: ApplicationUsersLocationModel) {
    this.appState.selectedUserLocationIdState.next(data.locationId);
    this.router.navigate(['appointment']);
    this.closeScheduleGuideDialog();
  }

  closeScheduleGuideDialog() {
    this.isOpenSchedulerGuidePopup = false;
  }

  message;
  type;

  displaySuccessMessage(message: string) {
    this.message = message;
    this.type = MessageType.success;
    // setInterval((a) => {
    //   this.message = '';
    //   this.type = MessageType.success;
    // }, 5000, [])
    setTimeout(() => {
      this.message = '';
      this.type = MessageType.success;
    }, 5000);
  }
}
