import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './addUser/addUser.component';
import {UsersComponent} from './users.component';
import {AddEditStaffComponent} from './add-edit-staff/add-edit-staff.component';


const routes: Routes = [
  {
    path: '', component: UsersComponent
  },
  {
    'path': 'add', component: AddEditStaffComponent
  },
  {
    'path': ':staffId', component: AddEditStaffComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
