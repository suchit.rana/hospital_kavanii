import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ImageSnippet} from '../../../../../../models/app.misc';
import {ImageUploadComponent} from '../../../../shared-content-full-layout/image-upload/image-upload.component';
import {AppUtils} from '../../../../../../shared/AppUtils';

@Component({
  selector: 'app-add-edit-staff-othet-info',
  templateUrl: './add-edit-staff-othet-info.component.html',
  styleUrls: ['./add-edit-staff-othet-info.component.css']
})
export class AddEditStaffOthetInfoComponent implements OnInit, AfterViewInit {

  @Input()
  formGroup: FormGroup;

  appearance = 'outline';

  @ViewChild('signature', {static: true}) signature: ImageUploadComponent;

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    const photo = this.formGroup.get('signature').value;
    if (this.signature && photo) {
      this.signature.selectedFile = new ImageSnippet(AppUtils.prepareBase64String(photo), null);
    }
  }

  fileSelectionChange(data: ImageSnippet) {
    this.formGroup.get('signature').setValue(data ? data.plainBase64 : null);
  }
}
