import {Injectable} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppState} from '../../../../../../app.state';
import {SpecialityModel} from '../../../../../../models/app.settings.model';
import {BehaviorSubject} from 'rxjs';
import {LocationSelectionModel} from '../../../../../../models/app.location.model';
import {map} from 'rxjs/operators';
import {RoleModel} from '../../../../../../models/app.role.model';
import {PractitionerModel} from '../../../../../../models/app.staff.model';
import {SettingsService} from '../../../../../../services/app.settings.service';
import {BusinessService} from '../../../../../../services/app.business.service';
import {RoleService} from '../../../../../../services/app.role.service';
import {StaffService} from '../../../../../../services/app.staff.service';
import {validateProviderNo} from '../../../../../../shared/validation';

@Injectable()
export class StaffAddEditService {

  private _specialitiesByLocationIdGroup: { locationId: string, data: SpecialityModel[] }[] = [];
  private _specialitiesByLocationSubject = new BehaviorSubject<SpecialityModel[]>(null);
  private _locationSubject: BehaviorSubject<LocationSelectionModel[]> = new BehaviorSubject<LocationSelectionModel[]>(null);
  private _rolesSubject: BehaviorSubject<RoleModel[]> = new BehaviorSubject<RoleModel[]>(null);
  private _practitionerSubject: BehaviorSubject<PractitionerModel[]> = new BehaviorSubject<PractitionerModel[]>(null);
  private _practitionerInactiveSubject: BehaviorSubject<PractitionerModel[]> = new BehaviorSubject<PractitionerModel[]>(null);

  constructor(private fb: FormBuilder,
              private appState: AppState,
              private settingsService: SettingsService,
              private businessService: BusinessService,
              private roleService: RoleService,
              private staffService: StaffService) {

  }

  get applicationUsersLocationModelFromGroup(): FormGroup {
    return this.fb.group({
      id: new FormControl(),
      applicationUsersId: new FormControl(this.appState.userProfile.id),
      locationId: new FormControl('', Validators.required),
      appointmentColour: new FormControl('#ffffff'),
      includeInScheduler: new FormControl(true),
      treatmentNotesPrivacy: new FormControl(false),
      doubleBooking: new FormControl(false),
      onlineBooking: new FormControl(false),
      isOnlineBookingConfirmation: new FormControl(true),
      isOnlineBookingCancellation: new FormControl(true),
      applicationUsersLocationRolesModel: this.fb.array([]),
      applicationUsersPractApptAccessModel: this.fb.array([]),
      // Extra Key
      hasSchedulerAccess: new FormControl({value: false, disabled: true}),
      hasPractitionerRole: new FormControl({value: false, disabled: true}),
      selectedRoles: new FormControl(null, Validators.required),
      selectedPractitioner: new FormControl(null),
      selectedSpecialities: new FormControl(null)
    });
  }

  get applicationUsersLocationRolesModelFromGroup(): FormGroup {
    return this.fb.group({
      applicationUsersPractSpltyandOthsModel: this.fb.array([]),
      id: new FormControl(''),
      applicationUsersLocationId: new FormControl(this.appState.selectedUserLocationId),
      roleId: new FormControl()
    });
  }

  get applicationUsersPractApptAccessModelFromGroup(): FormGroup {
    return this.fb.group({
      id: new FormControl(),
      applicationUsersLocationId: new FormControl(this.appState.selectedUserLocationId),
      practitonerId: new FormControl()
    });
  }

  getApplicationUsersPractSpltyandOthsModelFromGroup(isForOtherId?: boolean): FormGroup {
    return this.fb.group({
      id: new FormControl(),
      applicationUsersLocationRolesId: new FormControl(),
      type: new FormControl(0),
      serviceTypeId: new FormControl(),
      specialityId: new FormControl('', Validators.required),
      providerNumber: new FormControl('', validateProviderNo),
      otherIdName: new FormControl('', (isForOtherId ? Validators.required : null)),
      otherID: new FormControl('', (isForOtherId ? Validators.required : null)),
      // Extra Keys
      specialityName: new FormControl({value: '', disabled: true})
    });
  }

  getSpecialitiesByLocationId(locationId: string) {
    if (!locationId) {
      return [];
    }
    const obj = this.specialitiesByLocationIdGroup.find(value => value.locationId == locationId);
    return obj ? obj.data || [] : [];
  }

  prepareSpecialitiesDataByLocation(locationId: string) {
    this.settingsService.getAllSpecialtiesByLocationId(locationId).subscribe(response => {
      if (response && response.length > 0) {
        this.specialitiesByLocationIdGroup.push({locationId: locationId, data: response});
      }
    });
  }

  getLocationForCurrentUser(isForced?: boolean) {
    const value = this.locationSubject.getValue();
    if (!isForced && value && value.length > 0) {
      this.locationSubject.next(value);
      return;
    }
    this.businessService.getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .pipe(
        map(items => items.filter(l => l.status))
      ).subscribe(l => {
      this.locationSubject.next(l);
      if (l) {
        l.forEach(value => {
          this.prepareSpecialitiesDataByLocation(value.id);
        });
      }
    });
  }

  getRoles(isForced?: boolean) {
    const value = this.rolesSubject.getValue();
    if (!isForced && value && value.length > 0) {
      this.rolesSubject.next(value);
      return;
    }
    this.roleService.getAllRoles().subscribe(response => {
      this.rolesSubject.next(response);
    }, error => {

    });
  }

  getPrctitioner(isForced?: boolean) {
    const value = this.practitionerSubject.getValue();
    const inactiveVal = this.practitionerInactiveSubject.getValue();
    if (!isForced) {
      if (value && value.length > 0) {
        this.practitionerSubject.next(value);
      }
      if (inactiveVal && inactiveVal.length > 0) {
        this.practitionerSubject.next(inactiveVal);
      }
      return;
    }
    this.staffService.getPractitioners().subscribe(p => {
      p.forEach(value1 => {
        value1.fullName = value1.firstName + ' ' + value1.lastName;
      });
      const active = p.filter(value1 => value1.isStatus);
      const inActive = p.filter(value2 => !value2.isStatus);
      this.practitionerSubject.next(active);
      this.practitionerInactiveSubject.next(inActive);
    }, (err => {

    }));
  }

  markPersonalInfoFormFieldTouched(staffFormGroup: FormGroup) {
    staffFormGroup.get('emailId').markAsTouched();
    staffFormGroup.get('firstName').markAsTouched();
    staffFormGroup.get('lastName').markAsTouched();
    staffFormGroup.get('nickName').markAsTouched();
    staffFormGroup.get('position').markAsTouched();

    staffFormGroup.get('position').markAsDirty();
    staffFormGroup.get('position').updateValueAndValidity();
    staffFormGroup.updateValueAndValidity();
  }

  private get specialitiesByLocationIdGroup(): { locationId: string; data: SpecialityModel[] }[] {
    return this._specialitiesByLocationIdGroup;
  }

  get specialitiesByLocationSubject(): BehaviorSubject<SpecialityModel[]> {
    return this._specialitiesByLocationSubject;
  }

  get locationSubject(): BehaviorSubject<LocationSelectionModel[]> {
    return this._locationSubject;
  }

  get rolesSubject(): BehaviorSubject<RoleModel[]> {
    return this._rolesSubject;
  }

  get practitionerSubject(): BehaviorSubject<PractitionerModel[]> {
    return this._practitionerSubject;
  }

  get practitionerInactiveSubject(): BehaviorSubject<PractitionerModel[]> {
    return this._practitionerInactiveSubject;
  }
}
