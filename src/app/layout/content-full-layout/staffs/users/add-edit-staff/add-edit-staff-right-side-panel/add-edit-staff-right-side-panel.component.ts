import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AppState} from '../../../../../../app.state';
import {FormArray, FormGroup} from '@angular/forms';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {StaffAddEditService} from '../service/staff-add-edit.service';

@Component({
  selector: 'app-add-edit-staff-right-side-panel',
  templateUrl: './add-edit-staff-right-side-panel.component.html',
  styleUrls: ['./add-edit-staff-right-side-panel.component.css']
})
export class AddEditStaffRightSidePanelComponent implements OnInit, OnChanges {
  static readonly PERSONAL_INFO = 'PERSONAL_INFO';
  static readonly ROLE_LOCATION = 'ROLE_LOCATION';
  static readonly PTTR_CONFIG = 'PTTR_CONFIG';
  static readonly OTHER_INFO = 'OTHER_INFO';

  @Input()
  staffFormGroup: FormGroup;
  @Input()
  selectedTabName: string;
  @Input()
  isDisplayPttrConfigTab: boolean = false;
  @Input()
  isAddingStaff: boolean = false;
  @Input()
  isOwner: boolean = false;
  @Input()
  isMyProfileDialog: boolean = false;

  @Output()
  tabChange: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  setupSchedule: EventEmitter<boolean> = new EventEmitter<boolean>();

  staffTabs = [
    {
      active: true,
      title: 'Personal Information',
      validate: 0,
      show: true,
      visited: true,
      value: AddEditStaffRightSidePanelComponent.PERSONAL_INFO
    },
    {
      active: false,
      title: 'Roles & Locations',
      validate: 0,
      show: true,
      visited: false,
      value: AddEditStaffRightSidePanelComponent.ROLE_LOCATION
    },
    {
      active: false,
      title: 'Practitioner Configuration ',
      validate: 0,
      show: false,
      visited: false,
      value: AddEditStaffRightSidePanelComponent.PTTR_CONFIG
    },
    {
      active: false,
      title: 'Other Information',
      validate: 0,
      show: true,
      visited: false,
      value: AddEditStaffRightSidePanelComponent.OTHER_INFO
    }
  ];
  private static selectedTab;

  constructor(public appState: AppState, private staffAddEditService: StaffAddEditService) {
    AddEditStaffRightSidePanelComponent.selectedTab = this.staffTabs[0];
  }

  ngOnInit() {
    if(this.isMyProfileDialog){
      //My profile need to show only two tabs
      this.staffTabs = [
        {
          active: true,
          title: 'Personal Information',
          validate: 0,
          show: true,
          visited: true,
          value: AddEditStaffRightSidePanelComponent.PERSONAL_INFO
        },
        {
          active: false,
          title: 'Other Information',
          validate: 0,
          show: true,
          visited: false,
          value: AddEditStaffRightSidePanelComponent.OTHER_INFO
        }
      ];
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isRoleAndLocationFormFieldEmpty();
    if (changes.hasOwnProperty('selectedTabName') && !changes.selectedTabName.firstChange) {
      this.tabChange.emit(changes.selectedTabName.currentValue);
      this.deselectExceptOneTab(changes.selectedTabName.currentValue);
    }
    if (changes.hasOwnProperty('isDisplayPttrConfigTab')) {
      this.isDisplayPractitionerConfigTab();
    }
  }

  clickStaffTabs(tab: { show: boolean; active: boolean; title: string; validate: number; value: string }) {
    if (!this.checkCurrentTabIsValid()) {
      this.setTabValidateOrNot(AddEditStaffRightSidePanelComponent.selectedTab);
      this.tabChange.emit(AddEditStaffRightSidePanelComponent.selectedTab);
      return;
    }
    this.deselectExceptOneTab(tab.value);
    this.tabChange.emit(tab.value);
  }

  static isPractitionerConfigTabVisible(): boolean {
    return AddEditStaffRightSidePanelComponent.selectedTab && AddEditStaffRightSidePanelComponent.selectedTab.value == AddEditStaffRightSidePanelComponent.PTTR_CONFIG && AddEditStaffRightSidePanelComponent.selectedTab.active;
  }

  private setTabValidateOrNot(tab: { show: boolean; visited: boolean; active: boolean; title: string; validate: number; value: string }) {
    switch (tab.value) {
      case AddEditStaffRightSidePanelComponent.PERSONAL_INFO:
        tab.validate = tab.visited && this.isPersonalInfoFormValid() && !this.isPersonalInfoFormFieldEmpty() ? 1 : 2;
        break;
      case AddEditStaffRightSidePanelComponent.ROLE_LOCATION:
        tab.validate = tab.visited && this.staffFormGroup.get('applicationUsersLocationModel').valid ? 1 : 2;
        break;
      case AddEditStaffRightSidePanelComponent.PTTR_CONFIG:
        tab.validate = tab.visited ? 1 : 2;
        break;
      case AddEditStaffRightSidePanelComponent.OTHER_INFO:
        tab.validate = tab.visited ? 1 : 2;
        break;
    }
  }

  private deselectExceptOneTab(value: string) {
    this.staffTabs.forEach(tab => {
      tab.active = tab.value == value;
      if (!tab.visited) {
        tab.visited = tab.active;
      }
      if (tab.active && AddEditStaffRightSidePanelComponent.selectedTab.value != tab.value) {
        this.setTabValidateOrNot(AddEditStaffRightSidePanelComponent.selectedTab);
        AddEditStaffRightSidePanelComponent.selectedTab = tab;
      }
    });
  }

  private isDisplayPractitionerConfigTab() {
    this.staffTabs.forEach(tab => {
      if (tab.value == AddEditStaffRightSidePanelComponent.PTTR_CONFIG) {
        tab.show = this.isDisplayPttrConfigTab;
      }
    });
  }

  private checkCurrentTabIsValid(): boolean {
    switch (AddEditStaffRightSidePanelComponent.selectedTab.value) {
      case AddEditStaffRightSidePanelComponent.PERSONAL_INFO:
        const isValid = this.isPersonalInfoFormValid();
        if (!isValid) {
          this.staffAddEditService.markPersonalInfoFormFieldTouched(this.staffFormGroup);
        }
        return isValid;
      case AddEditStaffRightSidePanelComponent.ROLE_LOCATION:
        const isValidForm = this.isRoleAndLocationFormValid();
        if (!isValidForm) {
          this.staffFormGroup.markAllAsTouched();
          this.staffFormGroup.updateValueAndValidity();
        }
        return isValidForm;
      case AddEditStaffRightSidePanelComponent.PTTR_CONFIG :
      case AddEditStaffRightSidePanelComponent.OTHER_INFO :
        return true;
    }
    return true;
  }

  private isPersonalInfoFormFieldEmpty(): boolean {
    return this.checkControlIsEmpty('title', 'firstName', 'lastName', 'middleName', 'nickName', 'dob', 'qualification', 'address', 'country', 'state', 'city', 'postCode');
  }

  private isRoleAndLocationFormFieldEmpty(): boolean {
    const roleAndLocationArray = this.getApplicationUsersLocationModel;
    if (roleAndLocationArray) {
      for (let i = 0; i < roleAndLocationArray.length; i++) {
        const roleAndLocFG = roleAndLocationArray.at(i);
        const userRoleLocFA = roleAndLocFG.get('applicationUsersLocationRolesModel') as FormArray;
        const schedulerAccessFA = roleAndLocFG.get('applicationUsersPractApptAccessModel') as FormArray;
        const spltAndOthFA = roleAndLocFG.get('applicationUsersPractSpltyandOthsModel') as FormArray;

        if (roleAndLocFG) {
          const locationValue = roleAndLocFG.get('locationId').value;
          const hasPttrRole = roleAndLocFG.get('hasPractitionerRole').value;
          const hasSchedulerAccess = roleAndLocFG.get('hasSchedulerAccess').value;
          let selectedPttr;

          if (hasSchedulerAccess) {
            selectedPttr = roleAndLocFG.get('selectedPractitioner').value;
          }
          if (hasPttrRole) {

          }
        }

        console.log(roleAndLocFG, userRoleLocFA,
        schedulerAccessFA,
        spltAndOthFA)

        // const
      }
    }
    return false;
  }

  private isPersonalInfoFormValid(): boolean {
    return AppUtils.isControlValid(this.staffFormGroup, 'firstName', 'lastName', 'middleName', 'nickName', 'position', 'emailId');
  }

  private isRoleAndLocationFormValid(): boolean {
    return AppUtils.isControlValid(this.staffFormGroup, 'applicationUsersLocationModel');
  }

  private checkControlIsEmpty(...controls: string[]) {
    if (controls) {
      for (let control of controls) {
        const value = this.staffFormGroup.get(control).value;
        if (!value || value == '' || value.length == 0) {
          return true;
        }
      }
    }
    return false;
  }

  get getApplicationUsersLocationModel(): FormArray {
    return this.staffFormGroup.get('applicationUsersLocationModel') as FormArray;
  }

  private getApplicationUsersLocationRolesModel(index: number): FormArray {
    return this.getApplicationUsersLocationModel.at(index).get('applicationUsersLocationRolesModel') as FormArray;
  }

  private getApplicationUsersPractSpltyandOthsModel(index: number): FormArray {
    return this.getApplicationUsersLocationRolesModel(index).at(index).get('applicationUsersPractSpltyandOthsModel') as FormArray;
  }

  private getApplicationUsersPractApptAccessModel(index: number): FormArray {
    return this.getApplicationUsersLocationModel.at(index).get('applicationUsersPractApptAccessModel') as FormArray;
  }

  setSchedule() {
    this.setupSchedule.emit(true);
  }
}
