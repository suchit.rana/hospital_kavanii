import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ApplicationDataEnum} from '../../../../../../enum/application-data-enum';
import {ApplicationDataService} from '../../../../../../services/app.applicationdata.service';
import {AppState} from '../../../../../../app.state';
import {PatientService} from '../../../../../../services/app.patient.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {BusinessService} from '../../../../../../services/app.business.service';
import {MatFormFieldAppearance} from '@angular/material/form-field/typings/form-field';
import {MiscService} from '../../../../../../services/app.misc.service';
import {AppUtils} from '../../../../../../shared/AppUtils';

@Component({
  selector: 'app-add-user-personal-information',
  templateUrl: './add-user-personal-information.component.html',
  styleUrls: ['./add-user-personal-information.component.css']
})
export class AddUserPersonalInformationComponent implements OnInit {

  readonly APP_DATA_TITLE = ApplicationDataEnum.Title;

  @Input()
  personalInfoForm: FormGroup;

  locationList: any[];

  @ViewChild('dobInput', {static: false}) dobInputElem: ElementRef;

  @BlockUI() blockUI: NgBlockUI;
  appearance: MatFormFieldAppearance = 'outline';

  allTitles: any = [];
  genderList: any = [];
  staffPositionList: any = [];
  staffPositionListSearch: any = [];

  // STAF ADDRESS
  addressManual: any;
  countries: any[];
  mainCountryList: any = [];
  // states: any[];
  // cities: any[];
  country: string;
  selectedAddress: string;
  // private showgoogleaddressField: boolean = false;
  max= new Date();

  constructor(private applicationDataService: ApplicationDataService,
              private appState: AppState,
              private patientService: PatientService,
              private businessService: BusinessService,
              private miscService: MiscService) {
  }

  ngOnInit() {
    this.selectedAddress = this.personalInfoForm.get('address').value;
    this.getAllTitle();
    this.genderList = [
      {id: 1, value: 'Male'},
      {id: 2, value: 'Female'},
      {id: 3, value: 'Other'}
    ];
    this.getgoogleLocations();
    console.log(this.genderList);
  }

  //get all title
  getAllTitle() {
    this.allTitles = [];
    this.applicationDataService.GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.Title, this.appState.selectedUserLocationId).subscribe((data) => {
      this.allTitles = data;
    }, (err) => {
      this.blockUI.stop();
      let responseHandle = {
        message: 'Something Wrong!',
        type: 'error'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    });

    this.applicationDataService.GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.StaffPosition, this.appState.selectedUserLocationId)
      .subscribe((data) => {
        this.staffPositionList = data;
        this.staffPositionListSearch = data;
      });
  }

  //location logic start
  getgoogleLocations() {
    this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
      this.locationList = locations ? locations : [];
      this.miscService.getCountries().subscribe((countries) => {
        this.countries = countries;
        this.mainCountryList = countries;
        const countryName = this.locationList.find((l) => l.locationName === this.appState.selectedUserLocation.locationName).country;
        if (countryName) {
          this.country = countries.find((x) => x.country_Name === countryName).country_Code;
          this.personalInfoForm.get('country').patchValue(countryName);
        }
      });
    });
  }

  onDateSelect($event) {
    this.personalInfoForm.get('dob').setValue($event);
  }

  restrictMaxLength($event, maxLength: number) {
    const length = AppUtils.getStringDefault($event.target.value, '').length + 1;
    const inputVal = $event.keyCode;
    console.log(inputVal,"inputVal")
    const excludedKeys = [39,45];
    if (length > maxLength) {
      $event.preventDefault();
    }
    if (!((inputVal >= 65 && inputVal <= 90) || (inputVal >= 97 && inputVal <= 122) || (excludedKeys.includes(inputVal)))) {
      $event.preventDefault();
    }
  }

  // Country, State and City Validation
  locationValidation($event){
    const inputVal = $event.keyCode;
    if (!((inputVal >= 65 && inputVal <= 90) || (inputVal >= 97 && inputVal <= 122))) {
      $event.preventDefault();
    }
  }

  // qualifications Field Validation
  qualificationsValidation($event, maxLength: number){
    const length = AppUtils.getStringDefault($event.target.value, '').length + 1;
    if (length > maxLength) {
      $event.preventDefault();
    }
  }

  onPaste($event: any, maxLength: number) {
    const pastedText = $event.target.value;

    if (pastedText.length > maxLength) {
      this.personalInfoForm.get('qualification').setValue(pastedText.substring(0,100));
    }
  }

  // Post Code Field Validation
  validatePostCode(event) {
    const keyCode = event.keyCode;
    const excludedKeys = [8, 37, 39, 46];
    if (!((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (excludedKeys.includes(keyCode)))) {
      event.preventDefault();
    }
  }

  restricateSpecCharNum(ev) {
    if (!/^[a-zA-Z\s'-]*$/.test(ev.key)) {
      ev.preventDefault();
    }
  }
}
