import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {StaffAddEditService} from '../service/staff-add-edit.service';
import {LocationSelectionModel} from '../../../../../../models/app.location.model';

@Component({
  selector: 'app-add-edit-practitioner-config',
  templateUrl: './add-edit-practitioner-config.component.html',
  styleUrls: ['./add-edit-practitioner-config.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditPractitionerConfigComponent implements OnInit {

  @Input()
  formGroup: FormGroup;

  private locations: LocationSelectionModel[];

  constructor(private staffAddEditService: StaffAddEditService) {
  }

  ngOnInit() {
    this.getLocations();
    this.staffAddEditService.getLocationForCurrentUser();
  }

  getLocations() {
    this.staffAddEditService.locationSubject.subscribe(data => {
      this.locations = data || [];
    });
  }

  get getSelectedLocation() {
    for (let location of this.locations) {
      if (this.formGroup.get('locationId').value == location.id){
        return location.locationName;
      }
    }
    return "";
  }

}
