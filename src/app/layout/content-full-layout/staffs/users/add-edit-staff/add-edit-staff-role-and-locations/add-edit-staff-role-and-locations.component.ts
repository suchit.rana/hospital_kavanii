import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoleModel} from '../../../../../../models/app.role.model';
import {PractitionerModel, UserLocationRolesPractitonerModel} from '../../../../../../models/app.staff.model';
import {StaffService} from '../../../../../../services/app.staff.service';
import {RoleService} from '../../../../../../services/app.role.service';
import {MatFormFieldAppearance, MatSelectChange} from '@angular/material';
import {StaffAddEditService} from '../service/staff-add-edit.service';
import {LocationSelectionModel} from '../../../../../../models/app.location.model';
import {BusinessService} from '../../../../../../services/app.business.service';
import {AppState} from '../../../../../../app.state';
import {AddEditStaffRightSidePanelComponent} from '../add-edit-staff-right-side-panel/add-edit-staff-right-side-panel.component';
import * as _ from 'lodash';
import {ApplicationDataEnum} from '../../../../../../enum/application-data-enum';
import {RemoveTagEvent} from '@progress/kendo-angular-dropdowns/dist/es2015/common/models/remove-tag-event';

@Component({
  selector: 'app-add-edit-staff-role-and-locations',
  templateUrl: './add-edit-staff-role-and-locations.component.html',
  styleUrls: ['./add-edit-staff-role-and-locations.component.css']
})
export class AddEditStaffRoleAndLocationsComponent implements OnInit {
  readonly STAFF_OTHER_ID_NAME = ApplicationDataEnum.StaffOtherIdName;
  readonly LOCATION_REMOVE_CONFIRM = 'LOCATION_REMOVE_CONFIRM';
  readonly LOCATION_REMOVE_ERROR = 'LOCATION_REMOVE_ERROR';
  readonly SPECIALITY_REMOVE_ALERT = 'SPECIALITY_REMOVE_ALERT';

  @Input()
  isAddingStaff: boolean = true;
  @Input()
  currentStaffId: string = '';
  @Input()
  country: string = '';
  @Input()
  roleAndLocationFormArray: FormArray;
  @Input()
  isOwner: boolean = false;

  @Output()
  isPractitionerConfig: EventEmitter<boolean> = new EventEmitter<boolean>();

  roles: Array<RoleModel> = [];
  practitioners: PractitionerModel[];
  inactivePractitioners: PractitionerModel[];
  locations: LocationSelectionModel[];
  serviceTypes: { id: number, serviceName: string }[] = [];
  appearance: MatFormFieldAppearance = 'outline';
  private practitionerRoleId: string;
  private ownerRoleId: string;
  private roleIdsWhoHasSchedulerAccess: string[] = [];
  private roleAndLocationFormArrayCopy: FormArray;
  private otherIdsDialogIndex: number = -1;
  private isRoleLoaded: boolean = false;
  private isLocationLoaded: boolean = false;
  private isPractitionerLoaded: boolean = false;
  private isPractitionerInactiveLoaded: boolean = false;
  isOtherIdDialogOpen: boolean = false;
  isOpenConfirmAlertDialog: boolean = false;
  isCancelOpenConfirmAlertDialog: boolean = false;
  otherIDsSpecialities: any[] = [];
  otherIdsFormArray: FormArray;

  confirmAlertDialogTitle: string = '';
  confirmAlertDialogDesc: string = '';
  confirmAlertDialogOkText: string = 'Yes';
  confirmAlertDialogCancelText: string = 'No';
  confirmAlertDialogType: string;
  confirmAlertDialogFormIndex: number = null;

  constructor(public appState: AppState,
              public staffAddEditService: StaffAddEditService,
              private staffService: StaffService,
              private roleService: RoleService,
              private businessService: BusinessService,
              private fb: FormBuilder) {
    this.otherIdsFormArray = this.fb.array([]);
  }

  ngOnInit() {
    // TODO Call to get All Inactive practitioner
    this.getAllRoles();
    this.getServiceType();
    this.getAllPractitioners();
    this.getCurrentBusinessLocations();
  }

  addUserLocationAndRoleForm() {
    this.roleAndLocationFormArray.push(this.staffAddEditService.applicationUsersLocationModelFromGroup);
  }

  removeUserLocationAndRoleForm(index: number) {
    const fg = this.roleAndLocationFormArray.at(index) as FormGroup;
    const schedulerAccess = fg.get('selectedPractitioner').value;
    if (schedulerAccess && schedulerAccess.length > 0) {
      this.openConfirmAlertDialog(this.LOCATION_REMOVE_CONFIRM, index);
      return;
    }
    this.roleAndLocationFormArray.removeAt(index);
  }

  private prepareFormData() {
    if (this.isRoleLoaded && this.isLocationLoaded && this.isPractitionerLoaded && this.isPractitionerInactiveLoaded && this.roleAndLocationFormArray) {
      for (let i = 0; i < this.roleAndLocationFormArray.length; i++) {
        const roleAndLocFG = this.getRoleAnLocationFormByIndex(i);
        const userRoleLocFA = this.getFormForRole(i);
        const schedulerAccessFA = this.getFormForSchedulerAccess(i);
        const spltAndOthFA = this.getFormForSpecialitiesAndOtherId(i);
        if (userRoleLocFA) {
          const roles = [];
          for (let j = 0; j < userRoleLocFA.length; j++) {
            const id = userRoleLocFA.at(j).get('roleId').value;
            const name = this.getRoleNameById(id);
            roles.push({id: id, roleName: name});
          }
          if (roles.length > 0) {
            roleAndLocFG.get('selectedRoles').setValue(roles);
          }
          const hasPttrRole = this.isValidForPractitionerDetails(i);
          roleAndLocFG.get('hasSchedulerAccess').setValue(this.isValidForSchedulerAccess(i));
          roleAndLocFG.get('hasPractitionerRole').setValue(hasPttrRole);
          if (hasPttrRole) {
            this.isPractitionerConfig.emit(true);
          }
        }

        if (schedulerAccessFA) {
          const pttrs = [];
          for (let j = 0; j < schedulerAccessFA.length; j++) {
            const id = schedulerAccessFA.at(j).get('practitonerId').value;
            pttrs.push({id: id, fullName: this.getPractitionerNameById(id)});
          }
          if (pttrs.length > 0) {
            roleAndLocFG.get('selectedPractitioner').setValue(pttrs);
          }
        }

        if (spltAndOthFA) {
          const data = [];
          for (let j = 0; j < spltAndOthFA.length; j++) {
            const spltAndOthFG = spltAndOthFA.at(j);
            if (spltAndOthFG.get('type').value == 0) {
              const id = spltAndOthFG.get('specialityId').value;
              const spltName = this.getSpecialityNameById(roleAndLocFG.get('locationId').value, id);
              spltAndOthFA.at(j).get('specialityName').setValue(spltName);
              data.push({id: id, specialityName: spltName});
            }
          }
          if (data.length > 0) {
            roleAndLocFG.get('selectedSpecialities').setValue(data);
          }
        }
      }
      this.roleAndLocationFormArrayCopy = _.cloneDeep(this.roleAndLocationFormArray);
    }
  }

  private getInactivePractitioner() {

  }

  // USED TO GET ALL PRACTITIONER
  private getAllPractitioners() {
    this.practitioners = [];
    this.staffAddEditService.practitionerSubject.subscribe(p => {
      this.practitioners = p;
      // if (this.isAddingStaff) {
      // } else {
      //   this.practitioners = p.filter(value => value.id != this.currentStaffId);
      // }
      this.isPractitionerLoaded = true;
      this.prepareFormData();
    });

    this.staffAddEditService.practitionerInactiveSubject.subscribe(p => {
      this.inactivePractitioners = p;
      this.isPractitionerInactiveLoaded = true;
      this.prepareFormData();
    });
  }

  // USED TO GET ALL ROLE
  private getAllRoles() {
    this.roles = [];
    this.staffAddEditService.rolesSubject.subscribe(response => {
      if (response && response.length > 0) {

        // this.roles = response;

        // sort by order
        var order = ['Receptionist', 'Practitioner', 'Accountant', 'Manager', 'Owner'];
        this.roles = _.sortBy(response, function(obj) {
          return _.indexOf(order, obj.roleName);
        });

        if (!this.isOwner) {
          this.roles = this.roles.filter(value => value.roleName != 'Owner');
        }
        const otherRoles = this.roles.filter(value => value.roleName != 'Accountant');
        otherRoles.forEach(value => {
          if (value.roleName.toLowerCase() == 'practitioner') {
            this.practitionerRoleId = value.id;
          }
          if (value.roleName.toLowerCase() == 'owner') {
            this.ownerRoleId = value.id;
          }
          this.roleIdsWhoHasSchedulerAccess.push(value.id);
        });
        if (this.isOwner) {
          // this.roles.push({id: this.ownerRoleId, roleName: 'Owner'});
          this.roleIdsWhoHasSchedulerAccess.push(this.ownerRoleId);
        }
        this.isRoleLoaded = true;
        this.prepareFormData();
      }
    });
  }

  // GET ALL LOCATION RELATED TO CURRENT BUSINESS
  private getCurrentBusinessLocations() {
    this.staffAddEditService.locationSubject.subscribe(locations => {
      this.locations = locations || [];
      this.isLocationLoaded = true;
      this.prepareFormData();
      if (this.isAddingStaff) {
        const userLocationForm = this.getRoleAnLocationFormByIndex(0);
        userLocationForm.get('locationId').setValue(this.appState.selectedUserLocationId);
      }
    });
  }

  // Get Service Type For Practitioner
  private getServiceType() {
    this.serviceTypes = [
      {id: 0, serviceName: 'General'},
      {id: 1, serviceName: 'Specialist'},
      {id: 2, serviceName: 'Pathology'},
      {id: 4, serviceName: 'Optical Service Provider'},
      {id: 9, serviceName: 'Radiology or Diagnostic'},
    ];
  }

  // Generate Form Data Based on selected Roles
  onSelectRole(selectedRoles: any[], index: number) {
    const userLocationFormId = this.getUserLocationFormIdIdByIndex(index);
    const formArray = this.getFormForRole(index);
    const formArrayC = _.cloneDeep(this.getFormForRole(index));
    let isDisplayPttrConfigTab = false;
    formArray.clear();
    if (selectedRoles && selectedRoles.length > 0) {
      selectedRoles.forEach((value, index) => {
        if (this.practitionerRoleId == value.id && !AddEditStaffRightSidePanelComponent.isPractitionerConfigTabVisible()) {
          isDisplayPttrConfigTab = true;
        }
        let id;
        if (!this.isAddingStaff) {
          id = this.getRoleIdFromEdit(value.id, index);
        }

        if (value.id === this.practitionerRoleId) {
          const isSelected = AddEditStaffRoleAndLocationsComponent.isRoleSelectedInForm(formArrayC, value.id);
          if (isSelected.has) {
            if (id) {
              isSelected.data['id'] = id;
            }
            formArray.push(isSelected.data);
          } else {
            this.setDataToFormOnRoleSelectionChange(formArray, index, value, userLocationFormId, id);
          }
        } else {
          this.setDataToFormOnRoleSelectionChange(formArray, index, value, userLocationFormId, id);
        }
      });
    }

    this.resetValuesOnRoleSelect(index);
    this.isPractitionerConfig.emit(isDisplayPttrConfigTab);
  }

  getRoleIdFromEdit(id: any, index: number) {
    const userLocationFG = this.roleAndLocationFormArrayCopy.at(index);
    if (userLocationFG) {
      const fm = userLocationFG.get('applicationUsersLocationRolesModel') as FormArray;
      if (fm) {
        for (let i = 0; i < fm.length; i++) {
          const fg = fm.at(i);
          if (fg && fg.get('roleId').value == id) {
            return fg.get('id').value;
          }
        }
      }
    }
    return null;
  }

  // Generate Scheduler Access FormData As per Selected Practitioner
  onSelectPttrForSchedulerAccess(selectedPttr: any[], index: number) {
    const userLocationId = this.getUserLocationFormIdIdByIndex(index);
    const formArray = this.getFormForSchedulerAccess(index);
    formArray.clear();
    if (selectedPttr && selectedPttr.length > 0) {
      selectedPttr.forEach((value, index) => {
        formArray.push(this.staffAddEditService.applicationUsersPractApptAccessModelFromGroup);
        const formGroup = formArray.at(index);
        if (formGroup) {
          formGroup.get('practitonerId').setValue(value.id);
          if (!this.isAddingStaff) {
            formGroup.get('applicationUsersLocationId').setValue(userLocationId);
          }
        }
      });
    }
  }

  // Generate Scheduler Access FormData As per Selected Practitioner
  onSelectSpecialities(selectedSpecialities: any[], serviceTypeId: string, index: number) {
    const formArray = this.getFormForSpecialitiesAndOtherId(index);
    formArray.clear();
    if (selectedSpecialities && selectedSpecialities.length > 0) {
      selectedSpecialities.forEach((value, index) => {
        formArray.push(this.staffAddEditService.getApplicationUsersPractSpltyandOthsModelFromGroup());
        const formGroup = formArray.at(index);
        if (formGroup) {
          formGroup.get('applicationUsersLocationRolesId').setValue('DO LATER');
          formGroup.get('type').setValue(0);
          formGroup.get('serviceTypeId').setValue(serviceTypeId);
          formGroup.get('specialityId').setValue(value.id);
          formGroup.get('providerNumber').setValue('');
          formGroup.get('otherIdName').setValue('');
          formGroup.get('otherID').setValue('');
          // Extra Keys
          formGroup.get('specialityName').setValue(value.specialityName);

        }
      });
    }
    console.log(this.getFormForSpecialitiesAndOtherId(index));
  }

  onRemoveSpecialities($event: RemoveTagEvent, index) {
    const removedTag = $event.dataItem;
    const formArray = this.getFormForSpecialitiesAndOtherId(index);
    const datas = formArray.value;
    const isHaveOtherIds = (datas.filter(value => value.specialityId == removedTag.id && value.type == 1) || []).length > 0;
    if (isHaveOtherIds) {
      this.openConfirmAlertDialog(this.SPECIALITY_REMOVE_ALERT, null);
      $event.preventDefault();
    }
  }

  // Change Service Type for specific location
  serviceTypeSelectionChange($event: MatSelectChange, index: number) {
    const value = $event.value;
    const formArray = this.getFormForSpecialitiesAndOtherId(index);

    for (let i = 0; i < formArray.length; i++) {
      const formGroup = formArray.at(i);
      if (formGroup) {
        formGroup.get('serviceTypeId').setValue(value);
      }
    }
  }

  // GET SPECIALITIES BASED ON LOCATION
  getSpecialitiesByLocationId(index: number) {
    const locationId = this.getUserLocationIdBtIndex(index);
    return this.staffAddEditService.getSpecialitiesByLocationId(locationId);
  }

  // DISPLAY SCHEDULER ACCeSS FIELD OR NOT
  isValidForSchedulerAccess(index: number): boolean {
    const roleForm = this.getFormForRole(index);
    for (let i = 0; i < roleForm.length; i++) {
      const formGroup = roleForm.at(i);
      if (this.roleIdsWhoHasSchedulerAccess.indexOf(formGroup.get('roleId').value) > -1) {
        return true;
      }
    }
    return false;
  }

  // DISPLAY PRACTITIONER DETAILS FIELDS OR NOT
  isValidForPractitionerDetails(index: number): boolean {
    const roleForm = this.getFormForRole(index);
    for (let i = 0; i < roleForm.length; i++) {
      const formGroup = roleForm.at(i);
      if (this.practitionerRoleId == formGroup.get('roleId').value) {
        return true;
      }
    }
    return false;
  }

  // Used to hide previously selected location
  isDisplayThisLocation(locationId: string, index: number) {
    for (let i = 0; i < this.roleAndLocationFormArray.length; i++) {
      const fg = this.roleAndLocationFormArray.at(i);
      if (fg.get('locationId').value == locationId && index != i) {
        return false;
      }
    }
    return true;
  }

  // OPEN OTHER ID's DIALOG
  openOtherIdDialog(index: number) {
    this.otherIdsDialogIndex = index;
    const oldOtherIds = this.getFormForSpecialitiesAndOtherId(index);
    this.otherIdsFormArray = this.fb.array([]);

    console.log(oldOtherIds);

    for (let i = 0; i < oldOtherIds.length; i++) {
      const fg = oldOtherIds.at(i);
      if (fg && fg.get('type').value == 1) {
        this.otherIdsFormArray.push(fg);
      }
    }

    if (this.otherIdsFormArray && this.otherIdsFormArray.length <= 0) {
      this.addOtherIdsPopupFormGroup();
    }
    this.otherIDsSpecialities = oldOtherIds.getRawValue().filter(value => value.type == 0);

    this.isOtherIdDialogOpen = true;
  }

  // Add new For Other Ids
  addOtherIdsPopupFormGroup() {
    const fg = this.staffAddEditService.getApplicationUsersPractSpltyandOthsModelFromGroup(true);
    fg.get('type').setValue(1);
    this.otherIdsFormArray.push(fg);
  }

  // Remove Row From Other Ids
  removeOtherIdsPopupFormGroup(index: number) {
    this.otherIdsFormArray.removeAt(index);
  }

  // CLOSE OTHER ID's DIALOG
  closeOtherIdDialog() {
    this.otherIdsFormArray.clear();
    this.otherIDsSpecialities = [];
    this.isOtherIdDialogOpen = false;
  }

  // APPLY OTHER IDS BY LOCATION
  applyOtherId() {
    if (this.otherIdsFormArray.invalid) {
      this.otherIdsFormArray.markAllAsTouched();
      this.otherIdsFormArray.updateValueAndValidity();
      return;
    }
    const otherIdsFA = this.getFormForSpecialitiesAndOtherId(this.otherIdsDialogIndex);
    const indexes = [];
    for (let i = 0; i < otherIdsFA.length; i++) {
      const fg = otherIdsFA.at(i);
      if (fg.get('type').value == 1) {
        indexes.push(i);
      }
    }

    indexes.forEach(value => {
      otherIdsFA.removeAt(value);
    });

    if (this.otherIdsFormArray.length > 0) {
      for (var i = 0; i < this.otherIdsFormArray.length; i++) {
        const fg = this.otherIdsFormArray.at(i);
        if (!this.isDuplicateOtherId(otherIdsFA.value, fg.value)) {
          otherIdsFA.push(fg);
        }
      }
    } else {
      const indexes = [];
      for (let i = 0; i < otherIdsFA.length; i++) {
        const fg = otherIdsFA.at(i);
        if (fg.get('type').value == 1) {
          indexes.push(i);
        }
      }
      indexes.forEach(value => {
        otherIdsFA.removeAt(value);
      });
    }
    this.closeOtherIdDialog();
  }

  isDuplicateOtherId(data: UserLocationRolesPractitonerModel[], value: UserLocationRolesPractitonerModel) {
    return data.filter(v => v.specialityId == value.specialityId && v.otherID == value.otherID && v.providerNumber == value.providerNumber).length > 0;
  }

  openConfirmAlertDialog(type: string, index: number) {
    this.confirmAlertDialogType = type;
    this.confirmAlertDialogFormIndex = index;
    this.confirmAlertDialogOkText = 'Ok';
    switch (this.confirmAlertDialogType) {
      case this.LOCATION_REMOVE_CONFIRM:
        this.confirmAlertDialogTitle = 'DO YOU WISH TO REMOVE THIS LOCATION?';
        this.confirmAlertDialogDesc = 'This staff will no longer be available for this location.';
        this.isCancelOpenConfirmAlertDialog = true;
        this.confirmAlertDialogCancelText = 'No';
        this.confirmAlertDialogOkText = 'Yes';
        break;
      case this.LOCATION_REMOVE_ERROR:
        this.confirmAlertDialogTitle = 'THIS LOCATION CAN NOT BE REMOVED';
        this.confirmAlertDialogDesc = `This Practitioner has already created the below records for this location, hence you cannot remove this location for this Practitioner. <br>
                                        -  Appointment(s)<br>
                                        -  Treatment note(s) <br>
                                        -  Letter(s) <br>
                                        -  Invoice(s) `;
        break;
      case this.SPECIALITY_REMOVE_ALERT:
        this.confirmAlertDialogTitle = 'THIS SPECIALITY CANNOT BE REMOVED';
        this.confirmAlertDialogDesc = 'This speciality is already used for creating “Other ID’s” hence cannot be deleted';
        break;
    }
    this.isOpenConfirmAlertDialog = true;
  }

  closeConfirmAlertDialog() {
    this.isOpenConfirmAlertDialog = false;
  }

  clickConfirmAlertOk() {
    switch (this.confirmAlertDialogType) {
      case this.LOCATION_REMOVE_CONFIRM:
        if (this.confirmAlertDialogFormIndex != null) {
          this.roleAndLocationFormArray.removeAt(this.confirmAlertDialogFormIndex);
        }
        break;
    }
    this.closeConfirmAlertDialog();
  }

  private getRoleNameById(id: string) {
    if (id === this.ownerRoleId) {
      return 'Owner';
    }
    const role = this.roles.find(value => value.id == id);
    return role ? role.roleName : '';
  }

  private getPractitionerNameById(id: string) {
    let pttr = this.practitioners.find(value => value.id == id);
    if (!pttr) {
      pttr = this.inactivePractitioners.find(value => value.id == id);
    }
    return pttr ? pttr.fullName : '';
  }

  private getSpecialityNameById(locationId: string, id: string) {
    const spltArr = this.staffAddEditService.getSpecialitiesByLocationId(locationId);
    const splt = spltArr.find(value => value.id == id);
    return splt ? splt.specialityName : '';
  }

  // Get Role And Location Form Array
  private getRoleAnLocationFormByIndex(index: number): FormGroup {
    return this.roleAndLocationFormArray.at(index) as FormGroup;
  }

  // Get Form For Role For Specific Location
  private getFormForRole(index: number): FormArray {
    return this.getRoleAnLocationFormByIndex(index).get('applicationUsersLocationRolesModel') as FormArray;
  }

  // Get Form For Scheduler Access For Specific Location
  private getFormForSchedulerAccess(index: number): FormArray {
    return this.getRoleAnLocationFormByIndex(index).get('applicationUsersPractApptAccessModel') as FormArray;
  }

  // Get Form For Specialities & Other ID's For Specific Location Role
  getFormForSpecialitiesAndOtherId(index: number): FormArray {
    const userRoleLocationFG = this.getFormForRole(index);
    if (userRoleLocationFG) {
      let fg;
      for (let i = 0; i < userRoleLocationFG.length; i++) {
        const formGroup = userRoleLocationFG.at(i);
        if (formGroup && formGroup.get('roleId').value === this.practitionerRoleId) {
          fg = formGroup;
          break;
        }
      }
      return fg ? fg.get('applicationUsersPractSpltyandOthsModel') as FormArray : this.fb.array([]);
    }
    return this.fb.array([]);
  }

  /** GET OTHER IDS BY INDEX */
  private getFormForOtherId(index: number): FormArray {
    const userRoleLocationFG = this.getFormForRole(index);
    if (userRoleLocationFG && !this.isAddingStaff) {
      let fg;
      for (let i = 0; i < userRoleLocationFG.length; i++) {
        const formGroup = userRoleLocationFG.at(i);
        if (formGroup && formGroup.get('roleId').value === this.practitionerRoleId) {
          fg = formGroup;
          break;
        }
      }
      const fa = fg ? fg.get('applicationUsersPractSpltyandOthsModel') as FormArray : this.fb.array([]);
      const otherIdFrmArray = this.fb.array([]);
      for (let i = 0; i < fa.length; i++) {
        const frmGroup = fa.at(i);
        if (frmGroup.get('type').value == 1) {
          otherIdFrmArray.push(frmGroup);
        }
      }
      return otherIdFrmArray;
    }
    return this.fb.array([]);
  }

  // Get User Location Id For Specific Location getUserLocationIdBtIndex
  private getUserLocationIdBtIndex(index: number): string {
    const userLocationForm = this.getRoleAnLocationFormByIndex(index);
    return userLocationForm.get('locationId').value;
  }

  // Get User Location Id For Specific Location
  private getUserLocationFormIdIdByIndex(index: number): string {
    const userLocationForm = this.getRoleAnLocationFormByIndex(index);
    return userLocationForm.get('id').value;
  }

  // Set Role Data to Form
  private setDataToFormOnRoleSelectionChange(formArray: FormArray, index: number, value: RoleModel, userLocationFormId: string, id: string) {
    formArray.push(this.staffAddEditService.applicationUsersLocationRolesModelFromGroup);
    const formGroup = formArray.at(index);
    if (formGroup) {
      formGroup.get('roleId').setValue(value.id);
      if (!this.isAddingStaff) {
        formGroup.get('applicationUsersLocationId').setValue(userLocationFormId);
      }
      if (id) {
        formGroup.get('id').setValue(id);
      }
    }
  }

  // Reset some form value based on selected role
  private resetValuesOnRoleSelect(index: number) {
    const roleAndLocationFG = this.getRoleAnLocationFormByIndex(index);
    if (roleAndLocationFG) {
      const hasPttrRole = this.isValidForPractitionerDetails(index);
      const hasSchedulerAccess = this.isValidForSchedulerAccess(index);
      const roleAndLocFG = this.getRoleAnLocationFormByIndex(index);
      roleAndLocationFG.get('hasSchedulerAccess').setValue(hasSchedulerAccess);
      roleAndLocationFG.get('hasPractitionerRole').setValue(hasPttrRole);

      if (!hasSchedulerAccess) {
        roleAndLocationFG.get('selectedPractitioner').setValue(null);
        roleAndLocationFG.get('selectedSpecialities').setValue(null);
        const schedulerAccessFA = this.getFormForSchedulerAccess(index) || this.fb.array([]);
        const spltAndOthFA = this.getFormForSpecialitiesAndOtherId(index) || this.fb.array([]);
        AddEditStaffRoleAndLocationsComponent.emptyFormArrayByIndex(schedulerAccessFA);
        AddEditStaffRoleAndLocationsComponent.emptyFormArrayByIndex(spltAndOthFA);
      }

      if (!hasPttrRole) {
        roleAndLocationFG.get('selectedSpecialities').setValue(null);
        const spltAndOthFA = this.getFormForSpecialitiesAndOtherId(index) || this.fb.array([]);
        AddEditStaffRoleAndLocationsComponent.emptyFormArrayByIndex(spltAndOthFA);
        roleAndLocFG.get('selectedSpecialities').setValidators(null);
      } else {
        roleAndLocFG.get('selectedSpecialities').setValidators(Validators.required);
      }
      roleAndLocFG.get('selectedSpecialities').updateValueAndValidity();
    }
  }

  // Check for prctitioner Role is previously selected
  private static isRoleSelectedInForm(formArray: FormArray, roleId: string): { has: boolean, data?: FormGroup, index?: number } {
    for (let i = 0; i < formArray.length; i++) {
      const fg = formArray.at(i) as FormGroup;
      if (fg && fg.get('roleId').value === roleId) {
        return {has: true, data: fg, index: i};
      }
    }
    return {has: false};
  }


  // Removes all data inside form Array By index
  private static emptyFormArrayByIndex(formArray: FormArray) {
    if (formArray) {
      formArray.clear();
    }
  }

  getPractitionerById(locationId: string) {
    return this.practitioners.filter(value => value.locations.indexOf(locationId) > -1);
  }

  getInactivePractitionerById(locationId: string) {
    return this.inactivePractitioners.filter(value => value.locations.indexOf(locationId) > -1);
  }
}
