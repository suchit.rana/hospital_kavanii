import {AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {BaseItemComponent} from '../../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {MatFormFieldAppearance} from '@angular/material/form-field/typings/form-field';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AddEditStaffRightSidePanelComponent} from './add-edit-staff-right-side-panel/add-edit-staff-right-side-panel.component';
import {AppUtils} from '../../../../../shared/AppUtils';
import {StaffAddEditService} from './service/staff-add-edit.service';
import {ImageUploadComponent} from '../../../shared-content-full-layout/image-upload/image-upload.component';
import {StaffService} from '../../../../../services/app.staff.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppState} from '../../../../../app.state';
import {ImageSnippet} from '../../../../../models/app.misc';
import {StaffManageService} from '../staff-manage.service';
import {ApplicationUsersLocationModel} from '../../../../../models/app.staff.model';
import {Subscription} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import {EMAIL_REGEX, hasPermission} from '../../../../../app.component';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
    selector: 'app-add-edit-staff',
    templateUrl: './add-edit-staff.component.html',
    styleUrls: ['./add-edit-staff.component.css'],
    providers: [StaffAddEditService]
})
export class AddEditStaffComponent extends BaseItemComponent implements OnInit, AfterViewInit, OnDestroy {
    moduleName = RP_MODULE_MAP.staff_members;

    @Input() public isMyProfileDialog: boolean;
    appearance: MatFormFieldAppearance = 'outline';
    staffFormGroup: FormGroup;
    displayPersonalInformationSection: boolean = true; // Hide/Show Personal Information Content
    displayRoleSection: boolean = false;
    displayPttrConfig: boolean = false;
    displayOtherSection: boolean = false;
    saveBtnText: string = 'Next'; // Change value according to middle content
    selectedTabName: string = AddEditStaffRightSidePanelComponent.PERSONAL_INFO;
    isChangeTabOnNext: boolean = true;
    isDisplayPttrConfigTab: boolean = false;
    practitionerConfigFormGroup: FormGroup;
    applicationUsersLocationModel: FormArray;
    isAddingStaff: boolean = true;
    redirectURL: string;
    id: string;
    isOpenSchedulerGuidePopup: boolean = false;
    isOpenEmailConfirmDialog: boolean = false;
    oldEmailId: string;
    isOwner: boolean = false;
    isPractitioner: boolean = false;
    private totalPageCount = 3;
    private currentPage = 1;
    demo: string = 'bhavesh';
    @ViewChild('photo', {static: true}) photo: ImageUploadComponent;
    staffName: any = '';
    allRoleId: string[] = [];
    private subscripton: Subscription[] = [];

    @Output() closeMyprofile = new EventEmitter();

    constructor(public location: Location,
                private staffAddEditService: StaffAddEditService,
                private staffService: StaffService,
                private fb: FormBuilder,
                private _route: ActivatedRoute,
                protected router: Router,
                public appState: AppState,
                private sanitizer: DomSanitizer,
                private staffManageService: StaffManageService) {
        super(location);
        let qPrams = this._route.snapshot.queryParams;
        let params = this._route.snapshot.params;
        if (qPrams && qPrams['relative'] && qPrams['url']) {
            this.redirectURL = qPrams['url'];
        }

        if (params && params.hasOwnProperty('staffId')) {
            this.id = params['staffId'];
            this.isAddingStaff = false;
        }


        this.getRoleAndCheckForPractitioner();
        staffAddEditService.getRoles();
    }

    ngOnInit() {
        if (this.isMyProfileDialog) {
            this.id = this.appState.UserProfile.id;
            this.isAddingStaff = false;
        }
        this.createStaffForm();
        if (!this.isAddingStaff || this.isMyProfileDialog) {
            this.getStaffById(this.id);
        }
        this.staffAddEditService.getRoles(true);
        this.staffAddEditService.getPrctitioner(true);
        this.staffAddEditService.getLocationForCurrentUser(true);
        this.staffFormGroup.get('applicationUsersLocationModel').valueChanges.subscribe(value => {
            this.totalPageCount = 3 + this.getApplicationUsersLocationModel.value.length;
        });
    }

    ngAfterViewInit(): void {
        const photo = this.staffFormGroup.get('photo').value;
        if (this.photo && photo) {
            this.photo.selectedFile = new ImageSnippet(AppUtils.prepareBase64String(photo), null);
        }
    }

    ngOnDestroy(): void {
        if (this.subscripton) {
            this.subscripton.forEach(value => {
                if (value) {
                    value.unsubscribe();
                }
            });
        }
    }

    private createStaffForm() {
        this.createApplicationUSerLocationModelFormArray();
        this.staffFormGroup = this.fb.group({
            id: new FormControl(''),
            title: new FormControl(''),
            firstName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
            lastName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
            middleName: new FormControl('', [Validators.maxLength(30)]),
            nickName: new FormControl('', [Validators.required, Validators.maxLength(15)]),
            position: new FormControl('', [Validators.required, Validators.min(1)]),
            emailId: new FormControl('', [Validators.required, Validators.pattern(EMAIL_REGEX)]),
            qualification: new FormControl(),
            phone: new FormControl(),
            mobile: new FormControl(),
            address: new FormControl(),
            address2: new FormControl(),
            country: new FormControl(),
            state: new FormControl(),
            city: new FormControl(),
            photo: new FormControl(),
            postCode: new FormControl(),
            dob: new FormControl(),
            gender: new FormControl(),
            signature: new FormControl(),
            emailSignature: new FormControl(),
            biography: new FormControl(),
            notes: new FormControl(),
            isStatus: new FormControl(true),
            isTermOfServicee: new FormControl(false),
            isAccess: new FormControl(true),
            applicationUsersLocationModel: this.applicationUsersLocationModel
        });

        if (this.isMyProfileDialog) {
            this.staffFormGroup.get('emailId').disable();
            this.staffFormGroup.get('position').disable();
        }

        if (!hasPermission(this.appState, this.moduleName, 'D')) {
            this.staffFormGroup.get('isStatus').disable();
        }
    }

    // CREATE User Location Model - 0
    private createApplicationUSerLocationModelFormArray() {
        this.applicationUsersLocationModel = this.fb.array([
            this.staffAddEditService.applicationUsersLocationModelFromGroup
        ]);
    }

    // Show appropriate content based on selected tab
    onTabChange(tab: string) {
        if (!this.isChangeTabOnNext) {
            this.isChangeTabOnNext = false;
            return;
        }
        this.displayPersonalInformationSection = false;
        this.displayRoleSection = false;
        this.displayOtherSection = false;
        this.displayPttrConfig = false;
        switch (tab) {
            case AddEditStaffRightSidePanelComponent.PERSONAL_INFO:
                this.currentPage = 1;
                break;
            case AddEditStaffRightSidePanelComponent.ROLE_LOCATION:
                this.currentPage = 2;
                break;
            case AddEditStaffRightSidePanelComponent.PTTR_CONFIG:
                this.currentPage = 3;
                break;
            case AddEditStaffRightSidePanelComponent.OTHER_INFO:
                this.currentPage = this.totalPageCount;
                break;
        }
        this.goToPage(this.currentPage);
    }

    nextBackPage(num: number) {
        this.currentPage += num;

        if (this.isMyProfileDialog) {
            if (num == -1) {
                this.currentPage = 1;
            }
            if (this.staffFormGroup.invalid || this.staffFormGroup.get('position').invalid) {
                if (!this.isPersonalInfoFormValid()) {
                    this.staffAddEditService.markPersonalInfoFormFieldTouched(this.staffFormGroup);
                    this.currentPage = 1;
                    this.goToPage(1);
                    this.selectedTabName = AddEditStaffRightSidePanelComponent.PERSONAL_INFO;
                } else if (this.currentPage == 2) {
                    this.currentPage = 3;
                    this.goToPage(1);
                }

            }
        } else {
            // check form validation
            if (this.staffFormGroup.invalid || this.staffFormGroup.get('position').invalid) {
                if (!this.isPersonalInfoFormValid()) {
                    // this.staffFormGroup.markAllAsTouched();

                    //new change
                    this.staffAddEditService.markPersonalInfoFormFieldTouched(this.staffFormGroup);
                    this.currentPage = 1;
                    this.goToPage(1);
                    this.selectedTabName = AddEditStaffRightSidePanelComponent.PERSONAL_INFO;
                } else if (!this.isRoleAndLocationFormValid()) {
                    if (this.currentPage == 3) {
                        this.staffFormGroup.markAllAsTouched();
                        this.currentPage = 2;
                        this.goToPage(1);
                    }
                    if (this.currentPage == 2 && this.isPersonalInfoFormValid()) {
                        this.currentPage = 2;
                        this.goToPage(1);
                    } else {
                        this.currentPage = 1;
                        this.goToPage(1);
                    }
                }
                return;
            }
        }

        if (this.currentPage > this.totalPageCount) {
            this.submitStaff();
            return;
        }

        this.goToPage(num);

    }

    private goToPage(num: number) {
        this.isChangeTabOnNext = true;
        this.saveBtnText = 'Next';
        this.displayPersonalInformationSection = false;
        this.displayRoleSection = false;
        this.displayPttrConfig = false;
        this.displayOtherSection = false;
        if (this.currentPage == 1) {
            this.displayPersonalInformationSection = true;
            this.selectedTabName = AddEditStaffRightSidePanelComponent.PERSONAL_INFO;
        } else if (this.currentPage == 2) {
            this.displayRoleSection = true;
            this.selectedTabName = AddEditStaffRightSidePanelComponent.ROLE_LOCATION;
        } else if (this.currentPage >= 3 && this.currentPage <= (2 + this.getApplicationUsersLocationModel.value.length)) {
            this.practitionerConfigFormGroup = this.getApplicationUsersLocationModel.at(this.currentPage - 3) as FormGroup;
            if (this.practitionerConfigFormGroup && !AppUtils.getBoolean(this.practitionerConfigFormGroup.get('hasPractitionerRole').value)) {
                this.nextBackPage(num);
                return;
            }
            this.displayPttrConfig = true;
            this.selectedTabName = AddEditStaffRightSidePanelComponent.PTTR_CONFIG;
        } else if (this.currentPage == this.totalPageCount) {
            this.displayOtherSection = true;
            this.selectedTabName = AddEditStaffRightSidePanelComponent.OTHER_INFO;
            this.saveBtnText = 'Save';
        }
    }

    // Submit Form Data
    submitStaff() {
        if (this.isAddingStaff) {
            this.addStaff();
        } else {
            if (this.oldEmailId != this.staffFormGroup.get('emailId').value) {
                this.isOpenEmailConfirmDialog = true;
                return;
            }
            this.updateStaff();
        }
    }

    private addStaff() {
        this.saveBtnText = 'Saving...';
        this.submitting = true;
        this.staffManageService.isAddingStaff = true;
        this.staffService.addStaff(this.prepareDataForSubmit()).subscribe(response => {
            this.saveBtnText = 'Save';
            this.submitting = false;
            if (response) {
                if (this.redirectURL && this.redirectURL.length > 0) {
                    this.router.navigate([this.redirectURL], {queryParams: {addedNewPtt: 1}});
                    return;
                }
                this.cancel();
                let profile = this.appState.userProfile;
                this.staffService.getStaffById(this.appState.userProfile.id).subscribe((data: any) => {
                    if (data.photo) {
                        profile.photo = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
                            + data.photo);
                    } else {
                        profile.photo = '../../assets/user.jpg';
                    }
                    this.appState.setUserProfileSubject(profile);
                });
            }
        }, (error) => {
            this.saveBtnText = 'Save';
            this.submitting = false;
            if (error) {
                this.displayErrorMessage(error);
            } else {
                this.displayErrorMessage('Failed to add Staff, please try again later.');
            }
        });
    }

    private updateStaff() {
        this.submitting = true;
        this.saveBtnText = 'Saving...';
        this.staffManageService.isAddingStaff = false;
        this.staffService.updateStaff(this.prepareDataForSubmit()).subscribe(response => {
            this.submitting = false;
            this.saveBtnText = 'Save';
            if (response) {
                if (this.isMyProfileDialog) {
                    this.closeMyprofile.emit();
                } else {
                    this.cancel();
                }
                let profile = this.appState.userProfile;
                this.staffService.getStaffById(this.appState.userProfile.id).subscribe((data: any) => {
                    if (data.photo) {
                        profile.photo = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
                            + data.photo);
                    } else {
                        profile.photo = '../../assets/user.jpg';
                    }
                    profile.firstName = data.firstName;

                    this.appState.setUserProfileSubject(profile);
                });
            }
        }, (error) => {
            this.submitting = false;
            this.saveBtnText = 'Save';
            if (error) {
                this.displayErrorMessage(error);
            } else {
                this.displayErrorMessage('Failed to update Staff, please try again later');
            }
        });
    }

    public cancelStaff() {
        if (this.isMyProfileDialog) {
            this.closeMyprofile.emit();
        } else {
            this.cancel();
        }
    }

    private getStaffById(id: string) {
        this.staffService.getStaffById(id).subscribe(data => {
            if (data) {
                // selected Staff Name in header
                if (data.title != '') {
                    if (data.title && data.title != '') {
                        this.staffName = data.title + '. ' + data.firstName + ' ' + data.lastName;
                    }
                } else {
                    this.staffName = data.firstName + ' ' + data.lastName;
                }

                this.oldEmailId = data.emailId;
                this.isOwner = data.isOwnerForAnyLocation;
                this.staffFormGroup.patchValue(data);
                if (this.isOwner) {
                    this.staffFormGroup.get('isAccess').setValue(true);
                    this.staffFormGroup.get('isAccess').disable();
                }
                const photo = this.staffFormGroup.get('photo').value;
                if (this.photo && photo) {
                    this.photo.selectedFile = new ImageSnippet(AppUtils.prepareBase64String(photo), null);
                }
                this.staffFormGroup.get('address').patchValue(data.address);
                this.staffFormGroup.get('address2');
                // this.staffFormGroup.get('position').patchValue(parseInt(data.position));
                this.staffFormGroup.get('position').patchValue(data.position);
                this.applicationUsersLocationModel.clear();
                data.applicationUsersLocationModel.forEach((applicationUsersLocationModel, index) => {

                    this.applicationUsersLocationModel.push(this.staffAddEditService.applicationUsersLocationModelFromGroup);
                    const roleAndLocationFG = this.applicationUsersLocationModel.at(index);
                    roleAndLocationFG.patchValue(applicationUsersLocationModel);
                    const applicationUsersLocationRolesModelFA = roleAndLocationFG.get('applicationUsersLocationRolesModel') as FormArray;

                    const roles = this.staffAddEditService.rolesSubject.getValue();
                    const otherRoles = roles.filter(value => value.roleName != 'Accountant');
                    let hasPttrRole = false;

                    applicationUsersLocationModel.applicationUsersLocationRolesModel.forEach((applicationUsersLocationRolesModel, index) => {
                        if (this.allRoleId.indexOf(applicationUsersLocationRolesModel.roleId) == -1) {
                            this.allRoleId.push(applicationUsersLocationRolesModel.roleId);
                        }

                        applicationUsersLocationRolesModelFA.push(this.staffAddEditService.applicationUsersLocationRolesModelFromGroup);
                        const applicationUsersLocationRolesModelFG = applicationUsersLocationRolesModelFA.at(index);
                        applicationUsersLocationRolesModelFG.patchValue(applicationUsersLocationRolesModel);
                        const applicationUsersPractSpltyandOthsModelFA = applicationUsersLocationRolesModelFG.get('applicationUsersPractSpltyandOthsModel') as FormArray;

                        applicationUsersLocationRolesModel.applicationUsersPractSpltyandOthsModel.forEach((applicationUsersPractSpltyandOthsModel, index) => {
                            applicationUsersPractSpltyandOthsModelFA.push(this.staffAddEditService.getApplicationUsersPractSpltyandOthsModelFromGroup());
                            const applicationUsersPractSpltyandOthsModelFG = applicationUsersPractSpltyandOthsModelFA.at(index);
                            applicationUsersPractSpltyandOthsModelFG.patchValue(applicationUsersPractSpltyandOthsModel);
                        });

                        otherRoles.forEach(value => {
                            if (value.roleName.toLowerCase() == 'practitioner') {
                                hasPttrRole = applicationUsersLocationRolesModel.roleId == value.id;
                            }
                        });
                    });
                    roleAndLocationFG.get('hasPractitionerRole').setValue(hasPttrRole);

                    const applicationUsersPractApptAccessModelFA = roleAndLocationFG.get('applicationUsersPractApptAccessModel') as FormArray;
                    applicationUsersLocationModel.applicationUsersPractApptAccessModel.forEach((applicationUsersPractApptAccessModel, index) => {
                        applicationUsersPractApptAccessModelFA.push(this.staffAddEditService.applicationUsersPractApptAccessModelFromGroup);
                        const applicationUsersPractApptAccessModelFG = applicationUsersPractApptAccessModelFA.at(index);
                        applicationUsersPractApptAccessModelFG.patchValue(applicationUsersPractApptAccessModel);
                    });
                });
                this.staffAddEditService.getRoles();


            }
        }, error => {
            if (error) {
                this.displayErrorMessage(error);
            } else {
                this.displayErrorMessage('Something went wrong. Try Again.');
            }
        });
    }

    prepareDataForSubmit() {
        const data = this.staffFormGroup.getRawValue();
        this.staffManageService.recentlySavedStaff = data;
        this.staffManageService.recentlySavedStaff['hasPractitionerRole'] = false;
        let hasPractitionerRole = false;

        data.photo = this.photo.selectedFile ? this.photo.selectedFile.plainBase64 : null;
        if (this.isAddingStaff) {
            delete data.id;
        }
        data.applicationUsersLocationModel.forEach(applicationUsersLocationModel => {
            if (this.isAddingStaff || !applicationUsersLocationModel.id || applicationUsersLocationModel.id.length == 0) {
                delete applicationUsersLocationModel.id;
            }
            if (applicationUsersLocationModel.hasPractitionerRole) {
                hasPractitionerRole = true;
            }
            delete applicationUsersLocationModel.selectedRoles;
            delete applicationUsersLocationModel.selectedPractitioner;
            delete applicationUsersLocationModel.selectedSpecialities;
            delete applicationUsersLocationModel.hasSchedulerAccess;
            delete applicationUsersLocationModel.hasPractitionerRole;
            applicationUsersLocationModel.applicationUsersLocationRolesModel.forEach(applicationUsersLocationRolesModel => {
                if (this.isAddingStaff || !applicationUsersLocationRolesModel.id || applicationUsersLocationRolesModel.id.length == 0) {
                    delete applicationUsersLocationRolesModel.id;
                    delete applicationUsersLocationRolesModel.applicationUsersLocationId;
                } else {
                    applicationUsersLocationRolesModel.applicationUsersLocationId = applicationUsersLocationModel.id;
                }
                applicationUsersLocationRolesModel.applicationUsersPractSpltyandOthsModel.forEach(applicationUsersPractSpltyandOthsModel => {
                    delete applicationUsersPractSpltyandOthsModel.specialityName;
                    if (this.isAddingStaff || !applicationUsersPractSpltyandOthsModel.id || applicationUsersPractSpltyandOthsModel.id.length == 0) {
                        delete applicationUsersPractSpltyandOthsModel.id;
                        delete applicationUsersPractSpltyandOthsModel.applicationUsersLocationRolesId;
                    } else {
                        applicationUsersPractSpltyandOthsModel.applicationUsersLocationRolesId = applicationUsersLocationRolesModel.id;
                    }
                });
            });

            applicationUsersLocationModel.applicationUsersPractApptAccessModel.forEach(applicationUsersPractApptAccessModel => {
                if (this.isAddingStaff || !applicationUsersPractApptAccessModel.id || applicationUsersPractApptAccessModel.id.length == 0) {
                    delete applicationUsersPractApptAccessModel.id;
                }
            });
        });
        this.staffManageService.recentlySavedStaff['hasPractitionerRole'] = hasPractitionerRole;

        return data;
    }

    // Enable/Disable Next Button
    disableNextButton() {
        // if (this.displayPersonalInformationSection) {
        //   return !this.isPersonalInfoFormValid();
        // } else if (this.displayRoleSection) {
        //   return !this.isRoleAndLocationFormValid();
        // }
        return false;
    }

    setupSchedule(data: ApplicationUsersLocationModel) {
        this.appState.selectedUserLocationIdState.next(data.locationId);
        this.router.navigate(['appointment']);
        this.closeScheduleGuideDialog();
    }

    openSchedulerGuidePopup() {
        this.isOpenSchedulerGuidePopup = true;
    }

    closeScheduleGuideDialog() {
        this.isOpenSchedulerGuidePopup = false;
    }

    confirmEmailChange() {
        this.isOpenEmailConfirmDialog = false;
        if (!this.isAddingStaff) {
            this.updateStaff();
        }
    }

    showPractitionerConfigTab(isDisplay: boolean) {
        this.isDisplayPttrConfigTab = isDisplay;
    }

    getApplicationUsersLocationModelValue(): ApplicationUsersLocationModel[] {
        const data = this.getApplicationUsersLocationModel.value;
        if (data) {
            const locList = this.appState.locationList || [];
            data.forEach(value => {
                const location = locList.find(value1 => value1.id === value.locationId);
                if (location) {
                    value['locationName'] = location.locationName;
                }
            });
        }
        return data || [];
    }

    restricateE($event: KeyboardEvent) {
        if ($event.which == 101 || $event.which == 69) {
            $event.preventDefault();
        }
    }

    get getApplicationUsersLocationModel(): FormArray {
        return this.applicationUsersLocationModel;
    }

    private getRoleAndCheckForPractitioner() {
        const sub = this.staffAddEditService.rolesSubject.subscribe(value => {
            if (value) {
                const practitionerRole = value.find(value1 => value1.roleName.toLowerCase() == 'practitioner');
                if (practitionerRole) {
                    const pId = practitionerRole.id;
                    if (this.allRoleId && this.allRoleId.length > 0 && this.allRoleId.indexOf(pId) > -1) {
                        this.isDisplayPttrConfigTab = true;
                    }
                }
            }
        });
        this.subscripton.push(sub);
    }

    private getApplicationUsersLocationRolesModel(index: number): FormArray {
        return this.getApplicationUsersLocationModel.at(index).get('applicationUsersLocationRolesModel') as FormArray;
    }

    private getApplicationUsersPractSpltyandOthsModel(index: number): FormArray {
        return this.getApplicationUsersLocationRolesModel(index).at(index).get('applicationUsersPractSpltyandOthsModel') as FormArray;
    }

    private getApplicationUsersPractApptAccessModel(index: number): FormArray {
        return this.getApplicationUsersLocationModel.at(index).get('applicationUsersPractApptAccessModel') as FormArray;
    }

    private isPersonalInfoFormValid(): boolean {
        return AppUtils.isControlValid(this.staffFormGroup, 'firstName', 'lastName', 'middleName', 'nickName', 'position', 'emailId');
    }

    private isRoleAndLocationFormValid(): boolean {
        return !this.getApplicationUsersLocationModel.invalid;
    }
}
