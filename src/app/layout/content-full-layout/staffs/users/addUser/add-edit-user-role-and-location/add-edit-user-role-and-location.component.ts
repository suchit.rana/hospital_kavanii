import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-add-edit-user-role-and-location',
  templateUrl: './add-edit-user-role-and-location.component.html',
  styleUrls: ['./add-edit-user-role-and-location.component.css']
})
export class AddEditUserRoleAndLocationComponent implements OnInit {

  @Input()
  roleAndLocationForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
