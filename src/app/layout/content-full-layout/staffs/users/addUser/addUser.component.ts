import {GoogleAddressComponent} from './../../../shared-content-full-layout/google-address/google-address.component';
import {PatientService} from './../../../../../services/app.patient.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ApplicationDataEnum} from './../../../../../enum/application-data-enum';
import {ApplicationDataService} from './../../../../../services/app.applicationdata.service';
import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Location} from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS, MatDialog} from '@angular/material';
import {APP_DATE_FORMATS, AppDateAdapter} from 'src/app/shared/dt-format';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {BusinessService} from 'src/app/services/app.business.service';
import {AppState} from 'src/app/app.state';
import {ActivatedRoute, Router} from '@angular/router';
import {SettingsService} from 'src/app/services/app.settings.service';
import {LocationGridModel} from 'src/app/models/app.location.model';
import {PractitionerDetailsOptionsComponent} from '../practitioner-details-options/practitioner-details-options.component';
import {RoleService} from 'src/app/services/app.role.service';
import {map} from 'rxjs/operators';
import {RoleModel} from 'src/app/models/app.role.model';
import {
  PractitionerLocationModel,
  PractitionerModel,
  StaffModel,
  UserLocationRoleModel,
  UserLocationRolesPractitonerModel
} from 'src/app/models/app.staff.model';
import {SpecialityModel} from 'src/app/models/app.settings.model';
import {MiscService} from 'src/app/services/app.misc.service';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {StaffService} from 'src/app/services/app.staff.service';
import {ImageSnippet} from 'src/app/models/app.misc';
import {GeoLocationService} from 'src/app/services/app.geolocation';
import {BehaviorSubject} from 'rxjs';
import {DateUtils} from 'src/app/shared/DateUtils';

@Component({
  selector: 'app-add-user',
  templateUrl: './addUser.component.html',
  styleUrls: ['./addUser.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})

export class AddUserComponent extends BaseItemComponent implements OnInit {
  public filter: string;

  //calender
  isStartCalenderOpen: boolean = false;
  dob: Date = null;
  currHvrDateStart: Date;
  isCalenderStartFocus: boolean = false;
  currentView = 'day';
  public calendarPopUpMargin = {horizontal: -60, vertical: 10};
  firstDay: number = 1;
  currHvrDateend: Date;
  hoverDate: Date = new Date();
  dateFormat: string = 'ddd, D MMM YYYY';

  staffPositionList: any = [];
  staffPositionListSearch: any = [];

  showPopup: boolean = false;
  whichPopup: string = '';
  popupHeaderTitle: string = '';
  popupBodyContent: string = '';
  popupBodyContent1: string = '';
  popupBodyContent2: string = '';
  popupBodyContent3: string = '';
  popupBodyContent4: string = '';
  popupBodyContentLink: string = '';
  popupBodyContentLink1: string = '';

  @ViewChild('photo', {static: true}) photo: ImageUploadComponent;
  // @ViewChild('dobInput', {static: false}) dobInputElem: ElementRef;
  @Input() indexVerticalTabs: number = 0;
  backtoPage: boolean = false;
  signatureImage: BehaviorSubject<ImageSnippet> = new BehaviorSubject<ImageSnippet>(null);
  signatureImageVal: ImageSnippet;
  appearance: string = 'outline';
  color: any;
  addUser: boolean = true;
  // countries: any[];
  // mainCountryList: any = [];
  // states: any[];
  // cities: any[];
  // country: string;
  selectedAddress: string;
  clickedPractitionerOptions: boolean = false;
  hasPractitionerRole: boolean = false;
  displayRoleSection: boolean = false;
  displayPersonalInformationSection: boolean = true;
  displayOtherSection: boolean = false;
  doNotProcessSignature: boolean = false;
  roleLocations: RoleLocation[] = [];
  specialities: SpecialityModel[];
  practitioners: any[];
  verticalTabList: string[];
  saveBtnText: string = 'Next';
  copyRoleLocationArray: any = [];

  allTitles: any = [];
  @BlockUI() blockUI: NgBlockUI;
  // genderList: any = [];

  staffPositionId: number;
  staffPositionParentId: number;

  personalInfoForm: FormGroup = new FormGroup({
    title: new FormControl(''),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    middleName: new FormControl(''),
    nickName: new FormControl(''),
    qualifications: new FormControl(''),
    address: new FormControl(''),
    address2: new FormControl(''),
    country: new FormControl(''),
    state: new FormControl(''),
    city: new FormControl(''),
    postCode: new FormControl(''),
    dob: new FormControl(null),
    gender: new FormControl(''),
    // isTermOfServicee: new FormControl(false),
    // isTreatmentPrivateNote: new FormControl(false),
    // isTreatmentSharedNote: new FormControl(false),
  });


  bioForm: FormGroup = new FormGroup({
    emailId: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')]),
    phone: new FormControl('', Validators.pattern('^[0-9]*$')),
    mobile: new FormControl('', [Validators.pattern('^[0-9]*$')]),
    position: new FormControl(null, Validators.required),
    photo: new FormControl(''),

  });

  otherInfoForm: FormGroup = new FormGroup({

    signature: new FormControl(ImageUploadComponent),
    emailSignature: new FormControl(''),
    biography: new FormControl(''),
    notes: new FormControl(''),

  });

  locationList: any[];
  roleAndLoactionForm: FormGroup;
  otherInfoRolePermissionForm: FormGroup;
  statusForm: FormGroup;
  // addressManual: any;
  showgoogleaddressField: boolean = false;
  public googleAddress: GoogleAddressComponent;
  statusArray: any;

  // USED TO REDIRECT TO APPOINTMENT
  private redirectURL = '';

  constructor(public businessService: BusinessService,
              public appState: AppState,
              public location: Location,
              protected router: Router,
              public _route: ActivatedRoute,
              public dialog: MatDialog,
              public settingService: SettingsService,
              public roleService: RoleService,
              private miscService: MiscService,
              private staffService: StaffService,
              public applicationDataService: ApplicationDataService,
              private patientService: PatientService,
              private geoLocationService: GeoLocationService,
              private formBuilder: FormBuilder,
  ) {
    super(location);
    let params = this._route.snapshot.queryParams;
    if (params && params['relative'] && params['url']) {
      this.redirectURL = params['url'];
    }
    // this.genderList = [{
    //   id: 1,
    //   value: 'Male'
    // },
    //   {
    //     id: 2,
    //     value: 'Female'
    //   },
    //   {
    //     id: 3,
    //     value: 'Other'
    //   }];
    this.staffPositionParentId = ApplicationDataEnum.StaffPosition;

    this.roleAndLoactionForm = this.formBuilder.group({
      roleLocationArray: new FormArray([]),
    });
    this.otherInfoRolePermissionForm = this.formBuilder.group({
      otherIdsArray: this.formBuilder.array([])
    });

    this.statusForm = this.formBuilder.group({
      statusArray: this.formBuilder.array([])
    });
  }

  // disabledInput: boolean = false;

  staffTabs: Array<any> = [];
  roles: Array<RoleModel> = [];
  locationInterval: any;


  statusAccessForm: FormGroup = new FormGroup({
    isStatus: new FormControl(true),
    isAccess: new FormControl(true),
    // isPractitionerCalendar: new FormControl(true),
    // isDoubleBooking: new FormControl(false),
    // isPatientOnlineBooking: new FormControl(false),
    // practitionerCalendarColor: new FormControl("#ffffff"),
    // isCancelledAppointment: new FormControl(true),
    // isOnlineBooking: new FormControl(true),
    // treatmentNotesPrivacy: new FormControl(false),
  });

  ngOnInit() {

    this.verticalTabList = ['Personal Information', 'Roles & Locations', 'Other Information'];
    // this.getAllTitle();
    this.populateOptions();
    this.signatureImage.subscribe((data) => {

      if (this.doNotProcessSignature === true) {

        this.doNotProcessSignature = false;
      } else {
        // console.log("Signature Subscription");
        // console.log(data);
        this.signatureImageVal = data;
      }

    });

    // this.geoLocationService.getAllCountries().subscribe(countries => {
    // //   console.log(countries)

    //   if (countries !== undefined && countries !== null) {
    //     this.countries = countries;
    //   }

    // });
    this._route.params.subscribe(params => {
      if (params.staffId) {
        this.addItem = false;
        this.itemid = params.staffId;
        this.staffService.getStaffById(this.itemid).subscribe((data: any) => {
          // console.log(data)
          if (data.photo) {
            this.photo.selectedFile = new ImageSnippet(`data:image/jpeg;base64,${data.photo}`, null);
          }
          if (data.signature) {
            this.signatureImage.next(new ImageSnippet(`data:image/jpeg;base64,${data.signature}`, null));
          }

          //personal Info form set

          // this.personalInfoForm.patchValue(data);
          this.personalInfoForm.controls['title'].setValue(data.title);
          this.personalInfoForm.controls['firstName'].setValue(data.firstName);
          this.personalInfoForm.controls['lastName'].setValue(data.lastName);
          this.personalInfoForm.controls['middleName'].setValue(data.middleName);
          this.personalInfoForm.controls['nickName'].setValue(data.nickName);
          this.personalInfoForm.controls['qualifications'].setValue(data.qualification);
          this.personalInfoForm.controls['address'].setValue(data.address);
          this.personalInfoForm.controls['country'].setValue(data.country);
          this.personalInfoForm.controls['state'].setValue(data.state);
          this.personalInfoForm.controls['city'].setValue(data.city);
          this.personalInfoForm.controls['postCode'].setValue(data.postCode);
          this.personalInfoForm.controls['postCode'].setValue(data.postCode);
          this.dob = data.dob;
          this.personalInfoForm.controls['dob'].setValue(DateUtils.format(this.dob, 'DD/MM/YYYY'));
          this.personalInfoForm.controls['gender'].setValue(data.gender);

          //bio form set
          this.bioForm.controls['emailId'].setValue(data.emailId);
          this.bioForm.controls['phone'].setValue(data.phone);
          this.bioForm.controls['mobile'].setValue(data.mobile);
          this.bioForm.controls['position'].setValue(parseInt(data.position));
          this.bioForm.controls['photo'].setValue(data.photo);

          //status access form set
          this.statusAccessForm.controls['isStatus'].setValue(data.isStatus);
          this.statusAccessForm.controls['isAccess'].setValue(data.isAccess);


          //role&location set
          let roleLocationArray: any = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
          if (data.applicationUsersLocationModel.length != 0) {
            for (let index = 0; index < data.applicationUsersLocationModel.length; index++) {
              roleLocationArray.push(this.formBuilder.group({
                location: [data.applicationUsersLocationModel[index].locationId, Validators.required],
                role: ['', Validators.required],
                serviceType: [''],
                specialities: [''],
                practitioner: [''],
                otherSpecialityArray: new FormArray([])
              }));
              for (let i = 0; i < data.applicationUsersLocationModel[index].applicationUsersLocationRolesModel.length; i++) {
                ((this.roleAndLoactionForm.get('roleLocationArray') as FormArray).at(index) as FormGroup).get('role').patchValue(data.applicationUsersLocationModel[index].applicationUsersLocationRolesModel[i].roleId);
              }
              for (let i = 0; i < data.applicationUsersLocationModel[index].applicationUsersPractSpltyModel.length; i++) {
                ((this.roleAndLoactionForm.get('roleLocationArray') as FormArray).at(index) as FormGroup).get('serviceType').patchValue(data.applicationUsersLocationModel[index].applicationUsersPractSpltyModel[i].roleId);
              }
            }
          }
          // console.log(roleLocationArray, this.roleAndLoactionForm)

          let treatmentNotesPrivacy: string = data.isTreatmentPrivateNote ? 'Private Notes' : 'Shared Notes';
          // this.personalInfoForm.get('treatmentNotesPrivacy').setValue(treatmentNotesPrivacy);

          this.locationInterval = setInterval(() => {
            if (this.locationList) {
              this.loadRoleLocations(data);
              clearInterval(this.locationInterval);
            }
          }, 200);
        });
      } else {
        this.locationInterval = setInterval(() => {
          if (this.appState.selectedUserLocation) {
            let rl: RoleLocation = new RoleLocation();
            rl.location = this.appState.selectedUserLocation;
            rl.role = null;
            rl.options = [];
            rl.practitioners = [];
            this.roleLocations.push(rl);
            // console.log('1111', this.roleLocations)
            clearInterval(this.locationInterval);
          }
        }, 200);
      }
    });

    this.staffTabs = [
      {
        active: true,
        title: 'Personal Information',
        validate: 0,
        show: true
      },
      {
        active: false,
        title: 'Roles & Locations',
        validate: 0,
        show: true
      },
      {
        active: false,
        title: 'Practitioner Configuration ',
        validate: 0,
        show: false
      },
      {
        active: false,
        title: 'Other Information',
        validate: 0,
        show: true
      }];
    // this.getgoogleLocations();
  }

  //location logic start
  // getgoogleLocations() {
  //   this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
  //     this.locationList = locations ? locations : [];
  //     this.miscService.getCountries().subscribe((countries) => {
  //       this.countries = countries;
  //       this.mainCountryList = countries;
  //       const countryName = this.locationList.find((l) => l.locationName === this.appState.selectedUserLocation.locationName).country;
  //       if (countryName) {
  //         const country_Code = countries.find(
  //           (x) => x.country_Name === countryName
  //         ).country_Code;
  //         this.country = country_Code;
  //         if (this.country) {
  //           this.showgoogleaddressField = true;
  //         }
  //         // console.log(countryName)
  //         this.personalInfoForm.get('country').patchValue(countryName);
  //       }
  //     });
  //     // if (callback) callback();
  //   });
  // }

  // addressHandler($event) {
  //   this.addressManual = $event.name;
  //   if ($event !== undefined) {
  //     let country = _.find(this.countries, {country_Name: $event.countryName});
  //     this.personalInfoForm.get('country').patchValue(country.country_Code);
  //     this.personalInfoForm.get('state').patchValue($event.stateCode);
  //     this.personalInfoForm.get('city').patchValue($event.cityName);
  //     this.disabledInput = true;
  //   }
  // }
  //
  // addressManualHandler($event) {
  //   this.addressManual = $event;
  //   if (!$event) {
  //     this.addressManual = null;
  //     this.getgoogleLocations();
  //     this.personalInfoForm.get('state').patchValue('');
  //     this.personalInfoForm.get('city').patchValue('');
  //     this.disabledInput = false;
  //   }
  // }
  //
  // searchCountry(event) {
  //   // console.log(event, this.mainCountryList)
  //   this.mainCountryList;
  //   if (event.target.value != '') {
  //     this.countries = this.mainCountryList.filter(
  //       (s: any) =>
  //         s.country_Name
  //           .toLowerCase()
  //           .indexOf(event.target.value.toLowerCase()) !== -1
  //     );
  //   } else {
  //     this.countries = this.mainCountryList;
  //   }
  // }

  selectedCountry(country) {
  }

  //location logic end

  loadRoleLocations(data: StaffModel) {
    if (data.userLocationRoles && data.userLocationRoles.length > 0) {
      data.userLocationRoles.forEach(ulr => {
        let rm: RoleModel = new RoleModel();
        rm.id = ulr.roleId;
        rm.roleName = ulr.roleName;
        let existing = this.roleLocations.find(rl => rl.location && rl.location.id == ulr.locationId);
        if (existing) {
          existing.role.push(rm);
        } else {
          let rl: RoleLocation = new RoleLocation();
          // let locationList = this.getProceededLocationList();
          rl.options = this.locationList;
          rl.location = this.locationList.find(l => l.id == ulr.locationId);
          rl.role = [];
          rl.practitioners = [];
          let rm: RoleModel = new RoleModel();
          rm.id = ulr.roleId;
          rm.roleName = ulr.roleName;
          rl.role.push(rm);
          this.roleLocations.push(rl);
          // console.log('2222', this.roleLocations)
        }
      });
    }

    if (data.userLocationRolesPractitoner && data.userLocationRolesPractitoner.length > 0) {
      this.hasPractitionerRole = true;
      this.clickedPractitionerOptions = true;
      data.userLocationRolesPractitoner.forEach(ulr => {

        let roleName = this.roles.find(r => r.id == ulr.roleId).roleName;
        let rm: RoleModel = new RoleModel();
        rm.id = ulr.roleId;
        rm.roleName = roleName;
        let existing = this.roleLocations.find(rl => rl.location && rl.location.id == ulr.locationId);
        if (existing) {
          existing.hasPractitionerRole = true;
          existing.otherID = ulr.otherID;
          existing.providerNumber = ulr.providerNumber;
          if (!existing.role.find(r => r.id == rm.id)) {
            existing.role.push(rm);
          }

          if (!existing.specialities) {
            existing.specialities = [];
          }
          let sp: SpecialityModel = new SpecialityModel();
          sp.id = ulr.specialityId;
          sp.specialityName = ulr.specialityName;
          existing.specialities.push(sp);
        } else {
          let rl: RoleLocation = new RoleLocation();
          let locationList = this.getProceededLocationList();
          rl.options = this.locationList;
          rl.location = this.locationList.find(l => l.id == ulr.locationId);
          rl.otherID = ulr.otherID;
          rl.providerNumber = ulr.providerNumber;
          rl.specialities = [];
          rl.practitioners = [];

          let sp: SpecialityModel = new SpecialityModel();
          sp.id = ulr.specialityId;
          sp.specialityName = ulr.specialityName;
          rl.specialities.push(sp);
          rl.role = [];
          let rm: RoleModel = new RoleModel();
          rm.id = ulr.roleId;
          rm.roleName = roleName;
          rl.hasPractitionerRole = true;
          rl.role.push(rm);
          this.roleLocations.push(rl);
          // console.log('3333', this.roleLocations)
        }
      });
    }

    if (data.userPractitonerAccess && data.userPractitonerAccess.length > 0) {
      data.userPractitonerAccess.forEach(upa => {
        let existing = this.roleLocations.find(rl => rl.location && rl.location.id == upa.locationId);
        if (existing) {
          let p: PractitionerModel = new PractitionerModel();
          p.id = upa.practitonerId;
          p.firstName = upa.name;
          existing.practitioners.push(p);
        }
      });
    }

  }

  getProceededLocationList(): LocationGridModel[] {
    let clonedLocations: LocationGridModel[] = this.appState.clone(this.locationList);
    this.roleLocations.forEach(rl => {
      if (rl && rl.location && rl.location.id) {
        let idx = clonedLocations.findIndex(l => l.id == rl.location.id);
        if (idx > -1) {
          clonedLocations.splice(idx, 1);
        }
      }
    });
    return clonedLocations;
  }

  populateOptions() {
    this.populateLocationList();

    this.roleService.getAllRoles().subscribe(roles => {
      roles.splice(roles.findIndex(data => data.roleName == 'Owner'), 1);
      this.roles = roles;
    });

    this.staffService.getPractitioners().subscribe(p => {
      this.practitioners = p;
    });

    this.settingService.getAllSpecialties()
      .pipe(map(data => data.filter(d => d.isStatus === true)))
      .subscribe(s => {
        this.specialities = s;
      });

    // this.miscService.getCountries().subscribe(countries => {
    //   this.countries = countries;
    // });
  }

  populateLocationList() {
    this.businessService.getLocationsByBusiness(this.appState.userProfile.parentBusinessId).subscribe(locations => {
      this.locationList = locations;
    });
  }

  // countryChanged(event: MatSelectChange) {
  //
  //   this.geoLocationService.getAllStates().pipe(
  //     map((states: any[]) =>
  //       states.filter(item => item.country_id === event.value)
  //     )
  //   ).subscribe(states => {
  //
  //     if (states !== undefined && states !== null) {
  //
  //       this.states = states;
  //
  //     }
  //   });
  //
  // }
  //
  // stateChanged(event: MatSelectChange) {
  //
  //   this.geoLocationService.getAllCities().pipe(
  //     map((cities: any[]) =>
  //       cities.filter(item => item.state_id === event.value)
  //     )
  //   ).subscribe((cities: any[]) => {
  //
  //     if (cities !== undefined && cities !== null) {
  //
  //       this.cities = cities;
  //
  //     }
  //
  //   });
  //
  // }

  // addRoleLocation(event: Event) {
  //   event.preventDefault();
  //   let rl: RoleLocation = new RoleLocation();
  //   rl.location = null;
  //   rl.role = null;
  //   let locationList = this.getProceededLocationList();
  //   rl.options = locationList;
  //   rl.practitioners = [];
  //   this.roleLocations.push(rl);
  // //   console.log('3333', this.roleLocations)
  // }

  // removeRoleLocation(event: Event, idx: number) {
  //   event.preventDefault();
  //   this.roleLocations.splice(idx, 1);
  // }

  openPractitionerDialog(event: Event) {
    event.preventDefault();
    const dialogRef = this.dialog.open(PractitionerDetailsOptionsComponent, {
      width: '1200px',
    });

    let instance = dialogRef.componentInstance;
    instance.formGroup = this.personalInfoForm;

    dialogRef.afterClosed().subscribe(result => {
      this.clickedPractitionerOptions = true;
    });
  }

  handleLocationSelection(selected) {

  }

  public roleChanged(value: any[], roleLocation: RoleLocation): void {
    this.hasPractitionerRole = false;
    let result = roleLocation.role.find(r => r.roleName == 'Practitioner') != null;
    roleLocation.hasPractitionerRole = result;
    this.hasPractitionerRole = result;
  }

  appIdStaffPositionHandler($event) {

    this.staffPositionId = $event;
  }

  clickStaffTabs(selectedTab: any) {

    let index = 0;
    this.backtoPage = false;
    this.staffTabs.forEach((tab: any) => {

      if (tab.title === selectedTab.title) {

        this.staffTabs[index].active = true;

        if (selectedTab.title === 'Personal Information') {
          this.staffTabs[index].validate = (this.personalInfoForm.valid ? 1 : 2);
          this.displayPersonalInformationSection = true;
          this.displayRoleSection = false;
          this.displayOtherSection = false;
        } else if (selectedTab.title === 'Roles & Locations') {
          this.staffTabs[index].validate = (this.roleAndLoactionForm.valid ? 1 : 2);
          this.displayPersonalInformationSection = false;
          this.displayRoleSection = true;
          this.displayOtherSection = false;
          let roleLocationArray = this.roleAndLoactionForm.get('roleLocationArray') as FormArray;
          if (roleLocationArray.length != 0) {
            this.backtoPage = true;
          } else {
            this.backtoPage = false;
          }
        } else if (selectedTab.title === 'Other Information') {
          this.staffTabs[index].validate = (this.otherInfoForm.valid ? 1 : 2);
          this.displayPersonalInformationSection = false;
          this.displayRoleSection = false;
          this.displayOtherSection = true;
        }

      } else {

        this.staffTabs[index].active = false;

      }

      index++;
    });

  }

  backToOtherpage: boolean = false;
  hasSchedulerAccess: boolean = false;
  back() {
    this.backtoPage = false;
    this.statusArray = this.statusForm.get('statusArray') as FormArray;
    for (let index = 0; index < this.staffTabs.length; index++) {
      this.staffTabs[index].active = false;
    }
    if (this.displayPersonalInformationSection) {
      this.displayPersonalInformationSection = true;
      this.displayRoleSection = false;
      this.displayOtherSection = false;
      this.router.navigate(['/staffs']);
    } else if (this.displayRoleSection) {
      this.displayPersonalInformationSection = true;
      this.displayRoleSection = false;
      this.displayOtherSection = false;
      this.staffTabs[0].active = true;
      this.backToOtherpage = false;
    } else if (this.hasSchedulerAccess) {
      this.displayPersonalInformationSection = false;
      this.displayOtherSection = false;
      this.clickedPractitionerOptions = false;
      this.staffTabs[1].active = true;
      this.backtoPage = true;
      if (this.copyRoleLocationArray.length != 0) {
        for (let index = 0; index < this.statusArray.controls.length; index++) {
          this.statusArray.controls[index].controls['isSelected'].setValue(false);
          if (this.copyRoleLocationArray[this.copyRoleLocationArray.length - 1].location.locationName == this.statusArray.controls[index].value.locationName) {
            this.statusArray.controls[index].controls['isSelected'].setValue(true);
            this.copyRoleLocationArray.splice(this.copyRoleLocationArray.length - 1, 1);
            if (this.copyRoleLocationArray.length == 1) {
              if (!this.backToOtherpage) {
                this.statusArray.controls[index].controls['isSelected'].setValue(false);
                this.statusArray.controls[0].controls['isSelected'].setValue(true);
                this.backToOtherpage = false;
              }
            }
            if (this.copyRoleLocationArray.length == 0) {
              if (!this.backToOtherpage) {
                this.displayRoleSection = true;
                this.hasPractitionerRole = false;
                this.backToOtherpage = false;
              }
            }
          }
        }
      } else {
        this.displayRoleSection = true;
        this.hasPractitionerRole = false;
      }
    } else if (this.displayOtherSection) {
      this.displayPersonalInformationSection = false;
      this.hasPractitionerRole = true;
      this.clickedPractitionerOptions = true;
      this.backToOtherpage = true;
      // this.copyRoleLocationArray = Object.assign(this.roleAndLoactionForm.value.roleLocationArray)
      // console.log(this.statusArray, this.copyRoleLocationArray)
      this.statusArray.controls[this.statusArray.controls.length - 1].controls['isSelected'].setValue(true);
      this.copyRoleLocationArray.splice(this.copyRoleLocationArray.length - 1, 1);
      this.displayRoleSection = false;
      this.displayOtherSection = false;
      this.staffTabs[2].active = true;
    }
    this.saveBtnText = 'Next';
  }

  disableNextButton() {
    //return false;
    if (this.displayPersonalInformationSection) {
      if (this.personalInfoForm.invalid) {
        return true;
      } else if (this.bioForm.invalid) {
        return true;
      } else {
        return false;
      }
    } else if (this.displayRoleSection) {
      if (this.roleAndLoactionForm.invalid) {
        return true;
      } else if (this.otherInfoRolePermissionForm.invalid) {
        return true;
      } else {
        return false;
      }
    } else if (this.displayOtherSection) {
      if (this.otherInfoForm.invalid && this.bioForm.invalid) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  submitStaff() {
    // console.log(this.addItem)
    this.backtoPage = false;
    this.statusArray = this.statusForm.get('statusArray') as FormArray;
    for (let index = 0; index < this.staffTabs.length; index++) {
      this.staffTabs[index].active = false;
    }
    // console.log(this.staffTabs)
    if (this.displayPersonalInformationSection && this.personalInfoForm.valid) {
      this.displayRoleSection = true;
      this.displayPersonalInformationSection = false;
      this.displayOtherSection = false;
      this.hasPractitionerRole = false;
      this.saveBtnText = 'Next';
      this.staffTabs[1].active = true;
      this.staffTabs[0].validate = (this.personalInfoForm.valid ? 1 : 2);
      if (this.roleAndLoactionForm.value.roleLocationArray.length != 0) {
        this.backtoPage = true;
      }
    } else if (this.displayRoleSection) {
      this.statusArray.clear();
      this.copyRoleLocationArray = [];
      if (this.statusArray.length == 0 || this.statusArray.length != this.roleAndLoactionForm.value.roleLocationArray.length) {
        this.statusArray.push(this.formBuilder.group({
            locationId: new FormControl(this.roleAndLoactionForm.value.roleLocationArray[0].location.id),
            locationName: new FormControl(this.roleAndLoactionForm.value.roleLocationArray[0].location.locationName),
            isPractitionerCalendar: new FormControl(true),
            isDoubleBooking: new FormControl(false),
            isPatientOnlineBooking: new FormControl(false),
            practitionerCalendarColor: new FormControl('#ffffff'),
            isCancelledAppointment: new FormControl(true),
            isOnlineBooking: new FormControl(true),
            treatmentNotesPrivacy: new FormControl(false),
            isSelected: new FormControl(true),
          })
        );
      }
      // this.copyRoleLocationArray.push(this.roleAndLoactionForm.value.roleLocationArray[0])
      this.copyRoleLocationArray.push(this.roleAndLoactionForm.value.roleLocationArray[0]);
      this.displayRoleSection = false;
      this.displayPersonalInformationSection = false;
      this.displayOtherSection = false;
      this.hasPractitionerRole = true;
      this.clickedPractitionerOptions = true;
      this.saveBtnText = 'Next';
      this.doNotProcessSignature = true;
      this.signatureImage.next(this.signatureImageVal);
      this.staffTabs[2].show = true;
      this.staffTabs[2].active = true;
      this.staffTabs[1].validate = (this.roleAndLoactionForm.valid ? 1 : 2);
    } else if (this.hasSchedulerAccess) {
      this.saveBtnText = 'Next';
      if (this.roleAndLoactionForm.value.roleLocationArray.length == this.copyRoleLocationArray.length) {
        this.hasPractitionerRole = false;
        this.clickedPractitionerOptions = false;
        this.displayOtherSection = true;
        this.saveBtnText = 'Save';
      }
      this.staffTabs[3].active = true;
      this.staffTabs[2].validate = (this.statusForm.valid ? 1 : 2);
      if (this.roleAndLoactionForm.value.roleLocationArray.length) {
        for (let index = 0; index < this.statusArray.controls.length; index++) {
          this.statusArray.controls[index].controls['isSelected'].setValue(false);
        }
        this.statusArray.push(this.formBuilder.group({
          locationId: new FormControl(this.roleAndLoactionForm.value.roleLocationArray[this.copyRoleLocationArray.length].location.id),
          locationName: new FormControl(this.roleAndLoactionForm.value.roleLocationArray[this.copyRoleLocationArray.length].location.locationName),
          isPractitionerCalendar: new FormControl(true),
          isDoubleBooking: new FormControl(false),
          isPatientOnlineBooking: new FormControl(false),
          practitionerCalendarColor: new FormControl('#ffffff'),
          isCancelledAppointment: new FormControl(true),
          isOnlineBooking: new FormControl(true),
          treatmentNotesPrivacy: new FormControl(false),
          isSelected: new FormControl(true),
        }));
        this.copyRoleLocationArray.push(this.roleAndLoactionForm.value.roleLocationArray[this.copyRoleLocationArray.length]);
      }
    } else if (this.displayOtherSection) {
      this.displayRoleSection = false;
      this.displayPersonalInformationSection = false;
      this.hasPractitionerRole = false;
      this.staffTabs[3].validate = (this.otherInfoForm.valid ? 1 : 2);
      this.saveStaff();
    }
  }

  private prepareStaffModel() {

    let staffModel: any = {
      id: '0',
      title: this.personalInfoForm.value.title,
      firstName: this.personalInfoForm.value.firstName,
      lastName: this.personalInfoForm.value.lastName,
      middleName: this.personalInfoForm.value.middleName,
      nickName: this.personalInfoForm.value.nickName,
      position: this.bioForm.value.position,
      emailId: this.bioForm.value.emailId,
      qualification: this.personalInfoForm.value.qualifications,
      phone: this.bioForm.value.phone,
      mobile: this.bioForm.value.mobile,
      address: this.personalInfoForm.value.address,
      country: this.personalInfoForm.value.country,
      state: this.personalInfoForm.value.state,
      city: this.personalInfoForm.value.city,
      photo: '',
      postCode: this.personalInfoForm.value.postCode,
      timeZone: '',
      dob: this.personalInfoForm.value.dob,
      gender: this.personalInfoForm.value.gender,
      signature: '',
      emailSignature: this.otherInfoForm.value.emailSignature,
      biography: this.otherInfoForm.value.biography,
      notes: this.otherInfoForm.value.notes,
      isStatus: this.statusForm.value.isStatus,
      isTermOfServicee: false,
      isTreatmentPrivateNote: this.statusForm.value.treatmentNotesPrivacy,
      isTreatmentSharedNote: this.statusForm.value.treatmentNotesPrivacy,
      isPractitionerCalendar: this.statusForm.value.isPractitionerCalendar,
      isDoubleBooking: this.statusForm.value.isDoubleBooking,
      isPatientOnlineBooking: this.statusForm.value.isPatientOnlineBooking,
      isCancelledAppointment: this.statusForm.value.isCancelledAppointment,
      isOnlineBooking: this.statusForm.value.isOnlineBooking,
      practitionerCalendarColor: this.statusForm.value.practitionerCalendarColor,
      userLocationRoles: this.getAllRoleLocations(),
      userLocationRolesPractitoner: this.getPractitionerLocationModel(),
      userPractitonerAccess: this.getAllPractitionerAccess(),
      isAccess: this.statusForm.value.isAccess

    };

    return staffModel;

  }

  saveStaff() {

    this.roleAndLoactionForm.value.otherIdArray = this.otherInfoRolePermissionForm.value;
    let userLocationRoles = [];
    if (this.photo !== undefined && this.photo.selectedFile) {
      this.personalInfoForm.value.photo = this.photo.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
      ;
    }

    if (this.signatureImageVal !== undefined && this.signatureImageVal) {
      this.otherInfoForm.value.signature = this.signatureImageVal.src.replace('data:', '').replace(/^.+,/, '');
    }

    this.personalInfoForm.value.position = this.bioForm.value.position;
    this.personalInfoForm.value.emailId = this.bioForm.value.emailId;
    this.personalInfoForm.value.phone = this.bioForm.value.phone;
    this.personalInfoForm.value.mobile = this.bioForm.value.mobile;

    this.statusArray = this.statusForm.get('statusArray') as FormArray;
    // console.log(this.statusArray)

    // console.log(this.roleAndLoactionForm.value, this.otherInfoRolePermissionForm.value, this.personalInfoForm.value, this.bioForm.value, this.statusForm.value, this.otherInfoForm.value, this.copyRoleLocationArray)

    let roleLocationObj: any = null;
    let applicationUsersPractSpltyModel = [];
    let applicationUsersPractSpltyOthsModel = [];
    let applicationUsersLocationRolesModel = [];
    let applicationUsersPractApptAccessModel = [];
    let applicationUsersLocationModel = [];

    for (let index = 0; index < this.roleAndLoactionForm.value.roleLocationArray.length; index++) {
      for (let index = 0; index < this.otherInfoRolePermissionForm.value.otherIdsArray.length; index++) {
        let obj = {
          // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          // "applicationUsersLocationRolesId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          'specialityId': this.otherInfoRolePermissionForm.value.otherIdsArray[index].speciality.id,
          'otherIdName': this.otherInfoRolePermissionForm.value.otherIdsArray[index].name,
          'otherID': this.otherInfoRolePermissionForm.value.otherIdsArray[index].id_no
        };
        applicationUsersPractSpltyOthsModel.push(obj);
      }
      for (let i = 0; i < this.roleAndLoactionForm.value.roleLocationArray[index].otherSpecialityArray.length; i++) {
        let obj = {
          // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          // "applicationUsersLocationRolesId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          'serviceTypeId': this.roleAndLoactionForm.value.roleLocationArray[index].otherSpecialityArray.serviceType || 0,
          'specialityId': this.roleAndLoactionForm.value.roleLocationArray[index].otherSpecialityArray[i].speciality,
          'providerNumber': this.roleAndLoactionForm.value.roleLocationArray[index].otherSpecialityArray[i].provider_no
        };
        applicationUsersPractSpltyModel.push(obj);
      }
      for (let i = 0; i < this.roleAndLoactionForm.value.roleLocationArray[index].role.length; i++) {
        roleLocationObj =
          {
            applicationUsersPractSpltyModel,
            applicationUsersPractSpltyOthsModel,
            // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            // "applicationUsersLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            'roleId': this.roleAndLoactionForm.value.roleLocationArray[index].role.id,
            'createdDate': new Date(),
            'modifiedDate': new Date(),
            // "lastModifiedBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
          };
        // roleLocationObj.applicationUsersLocationId = "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        roleLocationObj.roleId = this.roleAndLoactionForm.value.roleLocationArray[index].role[i].id,
          roleLocationObj.createdDate = new Date(),
          roleLocationObj.modifiedDate = new Date(),
          // roleLocationObj.lastModifiedBy = "3fa85f64-5717-4562-b3fc-2c963f66afa6";

          applicationUsersLocationRolesModel.push(roleLocationObj);
      }
      for (let j = 0; j < this.roleAndLoactionForm.value.roleLocationArray[index].practitioner.length; j++) {
        let obj = {
          // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          // "applicationUsersLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          'practitonerId': this.roleAndLoactionForm.value.roleLocationArray[index].practitioner[j].id,
          // "roleId": this.roleAndLoactionForm.value.roleLocationArray[index].role[i].id,
        };
        applicationUsersPractApptAccessModel.push(obj);
      }
    }

    // console.log(applicationUsersLocationRolesModel, applicationUsersPractApptAccessModel)
    for (let index = 0; index < this.statusForm.value.statusArray.length; index++) {
      this.statusForm.value.statusArray[index];
      let obj = {
        // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        // "applicationUserId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        'locationId': this.statusForm.value.statusArray[index].locationId,
        'appointmentColour': this.statusForm.value.statusArray[index].practitionerCalendarColor,
        'includeInScheduler': true,
        'treatmentNotesPrivacy': this.statusForm.value.statusArray[index].treatmentNotesPrivacy,
        'doubleBooking': this.statusForm.value.statusArray[index].isDoubleBooking,
        'onlineBooking': this.statusForm.value.statusArray[index].isOnlineBooking,
        'createdDate': new Date(),
        'modifiedDate': new Date(),
        // "lastModifiedBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        'isDisabled': true,
        applicationUsersLocationRolesModel,
        applicationUsersPractApptAccessModel
      };
      applicationUsersLocationModel.push(obj);
    }
    let objParam1: any = {
      // "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      'title': this.personalInfoForm.value.title,
      'firstName': this.personalInfoForm.value.firstName,
      'lastName': this.personalInfoForm.value.lastName,
      'middleName': this.personalInfoForm.value.middleName,
      'nickName': this.personalInfoForm.value.nickName,
      'qualification': this.personalInfoForm.value.qualifications,
      'position': this.bioForm.value.position,
      'emailId': this.bioForm.value.emailId,
      'phone': this.bioForm.value.phone,
      'mobile': this.bioForm.value.mobile,
      'address': this.personalInfoForm.value.address,
      'country': this.personalInfoForm.value.country,
      'state': this.personalInfoForm.value.state,
      'city': this.personalInfoForm.value.city,
      'photo': this.personalInfoForm.value.photo,
      'postCode': this.personalInfoForm.value.postCode,
      'dob': this.personalInfoForm.value.dob,
      'gender': this.personalInfoForm.value.gender,
      'signature': this.otherInfoForm.value.signature,
      'emailSignature': this.otherInfoForm.value.emailSignature,
      'biography': this.otherInfoForm.value.biography,
      'notes': this.otherInfoForm.value.notes,
      'isStatus': this.statusAccessForm.value.isStatus,
      'isTermOfServicee': true,
      'isAccess': this.statusAccessForm.value.isAccess,
      'applicationUsersLocationModel': applicationUsersLocationModel
    };
    // console.log(objParam1)
    if (this.addItem) {
      this.blockUI.start();
      this.staffService.addStaff(objParam1).subscribe(data => {
        this.itemid = data;
        this.addItem = false;
        this.submitting = false;
        if (this.redirectURL && this.redirectURL.length > 0) {
          this.router.navigate([this.redirectURL], {queryParams: {addedNewPtt: 1}});
          return;
        }
        this.displaySuccessMessage('Staff created successfully.');
        this.router.navigate(['/staffs']);
        el.scrollIntoView();
        this.blockUI.stop();
      }, error => {
        // console.log(error);
        this.submitting = false;
        this.displayErrorMessage('Failed to add staff, please try again later.');
        this.blockUI.stop();
      });
    } else {
      this.blockUI.start();
      objParam1.id = this.itemid;
      this.staffService.updateStaff(objParam1).subscribe(data => {
        this.submitting = false;
        this.displaySuccessMessage('Staff updated successfully.');
        el.scrollIntoView();
        this.blockUI.stop();
      }, error => {
        // console.log(error);
        this.displayErrorMessage('Failed to update staff, please try again later.');
        this.submitting = false;
        this.blockUI.stop();
      });
    }
    return;

    // old code start
    for (let index = 0; index < this.roleAndLoactionForm.value.roleLocationArray.length; index++) {
      for (let i = 0; i < this.roleAndLoactionForm.value.roleLocationArray[index].role.length; i++) {
        let obj = {
          'locationId': this.roleAndLoactionForm.value.roleLocationArray[index].location.id,
          'roleId': this.roleAndLoactionForm.value.roleLocationArray[index].role[i].id,
          'locationName': this.roleAndLoactionForm.value.roleLocationArray[index].location.locationName,
          'roleName': this.roleAndLoactionForm.value.roleLocationArray[index].role[i].roleName
        };

        userLocationRoles.push(obj);
        for (let j = 0; j < this.roleAndLoactionForm.value.roleLocationArray[index].specialities.length; j++) {
          let newObj = {
            'locationId': this.roleAndLoactionForm.value.roleLocationArray[index].location.id,
            'specialityId': this.roleAndLoactionForm.value.roleLocationArray[index].specialities[j].id,
            'roleId': this.roleAndLoactionForm.value.roleLocationArray[index].role[i].id,
            'specialityName': this.roleAndLoactionForm.value.roleLocationArray[index].specialities[j].specialityName,
            'providerNumber': this.roleAndLoactionForm.value.roleLocationArray[index].specialities[j].provider_no,
            'otherID': 'string',
            'otherIdName': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
            'providerIdType': 0,
            'serviceTypeId': '3fa85f64-5717-4562-b3fc-2c963f66afa6'
          };
        }
      }
    }
    let objParam = {
      'title': this.personalInfoForm.value.title,
      'firstName': this.personalInfoForm.value.firstName,
      'lastName': this.personalInfoForm.value.lastName,
      'middleName': this.personalInfoForm.value.middleName,
      'nickName': this.personalInfoForm.value.nickName,
      'qualification': this.personalInfoForm.value.qualifications,
      'position': this.bioForm.value.position,
      'emailId': this.bioForm.value.emailId,
      'phone': this.bioForm.value.phone,
      'mobile': this.bioForm.value.mobile,
      'address': this.personalInfoForm.value.address,
      'country': this.personalInfoForm.value.country,
      'state': this.personalInfoForm.value.state,
      'city': this.personalInfoForm.value.city,
      'photo': 'string',
      'postCode': this.personalInfoForm.value.postCode,
      'dob': this.personalInfoForm.value.dob,
      'gender': this.personalInfoForm.value.gender,
      'signature': 'string',
      'emailSignature': this.otherInfoForm.value.emailSignature,
      'biography': this.otherInfoForm.value.biography,
      'notes': this.otherInfoForm.value.notes,
      // "isStatus": this.statusForm.value.isStatus,
      // // "isTermOfServicee":this.statusForm.value.,
      // // "isTreatmentPrivateNote":this.statusForm.value.,
      // // "isTreatmentSharedNote":this.statusForm.value.,
      // "isPractitionerCalendar": this.statusForm.value.isPractitionerCalendar,
      // "isDoubleBooking": this.statusForm.value.isDoubleBooking,
      // "isPatientOnlineBooking": this.statusForm.value.isPatientOnlineBooking,
      // "isCancelledAppointment": this.statusForm.value.isCancelledAppointment,
      // "isOnlineBooking": this.statusForm.value.isOnlineBooking,
      // "isAccess": this.statusForm.value.isAccess,
      // "practitionerCalendarColor": this.statusForm.value.practitionerCalendarColor,
      'userLocationRoles': userLocationRoles,
      'userLocationRolesPractitoner': [
        {
          'locationId': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'specialityId': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'roleId': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'specialityName': 'string',
          'providerNumber': 'string',
          'otherID': 'string',
          'otherIdName': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'providerIdType': 0,
          'serviceTypeId': '3fa85f64-5717-4562-b3fc-2c963f66afa6'
        }
      ],
      'userPractitonerAccess': [
        {
          'locationId': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'practitonerId': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          'name': 'string'
        }
      ]
    };
    // console.log(objParam)
    let el = document.getElementById('heading');

    //this.submitting = true;
    let staffSubmissionModel: StaffModel = this.prepareStaffModel();

    if (this.personalInfoForm.get('treatmentNotesPrivacy') !== null) {

      staffSubmissionModel.isTreatmentPrivateNote = this.personalInfoForm.get('treatmentNotesPrivacy').value == 'Private Notes';
      staffSubmissionModel.isTreatmentSharedNote = this.personalInfoForm.get('treatmentNotesPrivacy').value == 'Shared Notes';

    }

    if (this.photo !== undefined && this.photo.selectedFile) {
      staffSubmissionModel.photo = this.photo.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
      ;
    }

    if (this.signatureImageVal !== undefined && this.signatureImageVal) {
      staffSubmissionModel.signature = this.signatureImageVal.src.replace('data:', '').replace(/^.+,/, '');
    }

    // console.log("***** staffSubmissionModel *****")
    // console.log(staffSubmissionModel);
    // let userLocationRoles: UserLocationRoleModel[] = this.getAllRoleLocations();
    // let practitionerLocationRoles: UserLocationRolesPractitonerModel[] = this.getPractitionerLocationModel();
    // let userPractitonerAccess: PractitionerLocationModel[] = this.getAllPractitionerAccess();

    // staffSubmissionModel.userLocationRoles = userLocationRoles;
    // staffSubmissionModel.userLocationRolesPractitoner = practitionerLocationRoles;
    // staffSubmissionModel.userPractitonerAccess = userPractitonerAccess;

    // staffSubmissionModel.isTreatmentPrivateNote = this.personalInfoForm.get("treatmentNotesPrivacy").value == "Private Notes";
    // staffSubmissionModel.isTreatmentSharedNote = this.personalInfoForm.get("treatmentNotesPrivacy").value == "Shared Notes";

    if (this.photo.selectedFile) {
      staffSubmissionModel.photo = this.photo.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
      ;
    }

    if (this.signatureImage) {
      //staffSubmissionModel.signature = this.signatureImage.src.replace('data:', '').replace(/^.+,/, '');;
    }

    if (this.addItem) {
      this.staffService.addStaff(staffSubmissionModel).subscribe(data => {
        this.itemid = data;
        this.addItem = false;
        this.submitting = false;
        this.displaySuccessMessage('Staff created successfully.');
        el.scrollIntoView();
      }, error => {
        // console.log(error);
        this.submitting = false;
        this.displayErrorMessage('Error occurred while adding staff, please try again.');
      });
    } else {
      staffSubmissionModel.id = this.itemid;
      this.staffService.updateStaff(staffSubmissionModel).subscribe(data => {
        this.submitting = false;
        this.displaySuccessMessage('Staff updated successfully.');
        el.scrollIntoView();
      }, error => {
        // console.log(error);
        this.displayErrorMessage('Error occurred while updating staff, please try again.');
        this.submitting = false;
      });
    }
    // old code end
  }


  private getAllRoleLocations(): UserLocationRoleModel[] {
    let userLocationRoles: UserLocationRoleModel[] = [];
    // let containerPractitionerLocations: RoleLocation[] = this.appState.clone(this.roleLocations.filter(rl => rl.hasSchedulerAccess === true));
    // //Other Role Locations
    // let otherLocations: RoleLocation[] = this.appState.clone(containerPractitionerLocations);
    // otherLocations.forEach(sl => {
    //   sl.role = sl.role.filter(r => r.roleName != 'Practitioner');
    // });
    // otherLocations = otherLocations.filter(rl => rl.role.length > 0);
    // let standardLocations: RoleLocation[] = this.appState.clone(this.roleLocations.filter(rl => rl.hasSchedulerAccess === false || !rl.hasSchedulerAccess));
    // standardLocations.forEach(sl => {
    //   if (sl.role !== null) {
    //     sl.role = sl.role.filter(r => r.roleName != 'Practitioner');
    //   }
    // });
    // standardLocations = standardLocations.filter(rl => rl.role !== null && rl.role.length > 0);
    // standardLocations = standardLocations.concat(otherLocations);
    //
    // standardLocations.forEach(sl => {
    //   sl.role.forEach(rl => {
    //     let m: UserLocationRoleModel = new UserLocationRoleModel();
    //     m.locationId = sl.location.id;
    //     m.roleId = rl.id;
    //     userLocationRoles.push(m);
    //   });
    // });
    return userLocationRoles;
  }

  private getPractitionerLocationModel(): UserLocationRolesPractitonerModel[] {
    let practitionerLocationRoles: UserLocationRolesPractitonerModel[] = [];
    //Practitioner Locations
    // let containerPractitionerLocations: RoleLocation[] = this.appState.clone(this.roleLocations.filter(rl => rl.hasSchedulerAccess === true));
    // let practitionerLocation: RoleLocation[] = this.appState.clone(containerPractitionerLocations);
    // practitionerLocation.forEach(sl => {
    //   sl.role = sl.role.filter(r => r.roleName == 'Practitioner');
    // });
    // practitionerLocation = practitionerLocation.filter(rl => rl.role.length > 0);
    // let practitionerId = this.roles.find(r => r.roleName == 'Practitioner').id;
    // practitionerLocation.forEach(pl => {
    //   if (pl.specialities && pl.specialities.length > 0) {
    //     pl.specialities.forEach(sl => {
    //       let p: UserLocationRolesPractitonerModel = new UserLocationRolesPractitonerModel();
    //       p.locationId = pl.location.id;
    //       p.roleId = practitionerId;
    //       p.otherID = pl.otherID;
    //       p.providerNumber = pl.providerNumber;
    //       p.specialityId = sl.id;
    //       practitionerLocationRoles.push(p);
    //     });
    //   }
    // });
    return practitionerLocationRoles;
  }

  private getAllPractitionerAccess(): PractitionerLocationModel[] {
    let result: PractitionerLocationModel[] = [];
    this.roleLocations.forEach(rl => {
      rl.practitioners.forEach(p => {
        let model: PractitionerLocationModel = new PractitionerLocationModel();
        model.locationId = rl.location.id;
        model.practitonerId = p.id;
        result.push(model);
      });
    });

    return result;
  }

  // //get all title
  // getAllTitle() {
  //   this.allTitles = [];
  //   this.applicationDataService
  //     .GetApplicationDataByCategoryIdAndLocationId(
  //       ApplicationDataEnum.Title,
  //       this.appState.selectedUserLocationId
  //     )
  //     .subscribe((data) => {
  //       this.allTitles = data;
  //     }, (err) => {
  //       this.blockUI.stop();
  //       let responseHandle = {
  //         message: 'Something Wrong!',
  //         type: 'error'
  //       };
  //       this.patientService.publishSomeData(JSON.stringify(responseHandle));
  //     });
  //
  //   this.applicationDataService
  //     .GetApplicationDataByCategoryIdAndLocationId(
  //       ApplicationDataEnum.StaffPosition,
  //       this.appState.selectedUserLocationId
  //     )
  //     .subscribe((data) => {
  //       this.staffPositionList = data;
  //       this.staffPositionListSearch = data;
  //       // console.log(this.staffPositionList)
  //     });
  // }

  // onDateSelect($event) {
  //   this.personalInfoForm.controls['dob'].setValue(DateUtils.format($event, 'DD/MM/YYYY'));
  //   // this.toggleCalendarPopup(false, from);
  // }

  // handleFilterPosition(event) {
  //   if (event != '') {
  //     this.staffPositionListSearch = this.staffPositionList.filter(
  //       (s) => s.categoryName.toLowerCase().indexOf(event.toLowerCase()) !== -1
  //     );
  //   } else {
  //     this.staffPositionListSearch = this.staffPositionList;
  //   }
  //   this.filter = event;
  // }
  //
  // addNewPatientContactPosition() {
  //
  // }

  //set practitioner schedule
  openPopup(whichPopup) {
    this.showPopup = true;
    this.whichPopup = whichPopup;
    if (whichPopup == 'setScheduleforPractitioner') {
      this.popupHeaderTitle = 'setup practitioner schedule',
        this.popupBodyContent = 'You can create schedule for this practitioner by using the links below.',
        this.popupBodyContentLink = 'Set Rhodes Schedules >',
        this.popupBodyContentLink1 = 'Set Mascott Schedules >';
    }
  }

  closePopups(whichPopup) {
    if (whichPopup == 'setScheduleforPractitioner') {

    }
    this.showPopup = false;

  }

  performAction(whichPopup) {
    if (whichPopup == 'setScheduleforPractitioner') {

    }
    this.showPopup = false;
  }

  goToRhodesSchedule() {

  }

  goToMascottSchedule() {

  }
}

export class RoleLocation {
  location: LocationGridModel;
  role: RoleModel[];
  options: LocationGridModel[];
  hasPractitionerRole: boolean;
  otherID: string;
  providerNumber: string;
  specialities: SpecialityModel[];
  practitioners: PractitionerModel[];
}
