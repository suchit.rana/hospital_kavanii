import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import {CalenderPopupComponent} from './calender-popup.component';
import {DxCalendarModule} from 'devextreme-angular/ui/calendar';
import {PopupModule} from '@progress/kendo-angular-popup';


@NgModule({
  declarations: [
    CalenderPopupComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    DxCalendarModule,
    PopupModule
  ],
  exports: [
    CalenderPopupComponent
  ]
})
export class AppCalenderPopupModule { }
