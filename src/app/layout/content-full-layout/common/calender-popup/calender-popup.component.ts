import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-calender-popup',
  templateUrl: './calender-popup.component.html',
  styleUrls: ['./calender-popup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CalenderPopupComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('popup', {static: false, read: ElementRef}) public popup: ElementRef;

  @Input()
  anchor: ElementRef;
  @Input()
  isTodayButton: boolean = true;
  @Input()
  format: string = '';
  @Input()
  isPicker: boolean = false;
  @Input()
  value: Date;
  @Input()
  min: Date | number | string;
  @Input()
  max: Date | number | string;
  @Input()
  minZoomLevel: string = 'decade';
  @Input()
  maxZoomLevel: string = 'month';

  @Output()
  valueChange: EventEmitter<Date | number | string> = new EventEmitter<Date | number | string>();
  collision = {vertical: 'flip', horizontal: 'flip'};
  isCalendarOpen: boolean = false;
  isCalenderFocus: boolean = false;
  _value: Date;

  constructor() {
  }

  // POPUP CALENDAR METHODS
  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.toggleCalendarPopup(false);
    }
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
    event.stopPropagation();
    if (!this.isCalenderFocus && !this.contains(event.target)) {
      this.toggleCalendarPopup(false);
    }
  }

  private contains(target: any): boolean {
    if (!this.isCalendarOpen) {
      return false;
    }
    return (
      this.anchor.nativeElement.contains(target) ||
      (this.popup ? this.popup.nativeElement.contains(target) : false)
    );
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['anchor'] && changes['anchor']['currentValue']) {
      this.setValue(this.value);
      this.addEventOnAnchor();
    }
  }

  ngAfterViewInit(): void {
    this.onDateSelect(this.value, false);
  }

  private toggleCalendarPopup(val: boolean) {
    this.isCalendarOpen = val;
  }

  calenderPopupFocus(b: boolean) {
    this.isCalenderFocus = b;
  }

  optionChanged() {

  }

  onDateSelect($event: Date | number | string, emitValue: boolean = true) {
    this.setValue($event);
    this.toggleCalendarPopup(false);
    emitValue && this.valueChange.emit(moment($event).format());
  }

  private setValue(date) {
    if (!!date) {
      this._value = date;
    }
    if (this.isPicker) {
      this.anchor.nativeElement.innerText = moment(date).format(this.format);
      this.anchor.nativeElement.value = moment(date).format(this.format);
    }
  }

  setTodayDate() {
    this.value = new Date();
  }

   openCalendarPopup() {
    this.isCalenderFocus = false;
    let leftArrContainer = document.querySelectorAll('.app-calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
    let rightArrContainer = document.querySelectorAll('.app-calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
    if(leftArrContainer[0]) {
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
    }
    if(rightArrContainer[0]) {
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }
  }

  private addEventOnAnchor() {
    if (this.anchor instanceof HTMLElement) {
      this.anchor = new ElementRef(this.anchor);
    }
    if (this.anchor) {
      this.anchor.nativeElement.onclick = () => {
        if (!this.isCalendarOpen) {
          this.toggleCalendarPopup(!this.isCalendarOpen);
        }
      };
    }
  }

  openCalender() {
    this.toggleCalendarPopup(true);
  }
}
