import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output, ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {AppoinmentDateUtils} from '../../appointment/AppoinmentDateUtils';
import {DxPopoverComponent} from 'devextreme-angular';

@Component({
    selector: 'app-date-range-duration',
    templateUrl: './app-date-range-duration.component.html',
    styleUrls: ['./app-date-range-duration.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppDateRangeDurationComponent implements OnInit {
    @Input()
    value: { startDate?: Date, endDate?: Date } = {};

    @Output()
    valueChange: EventEmitter<{}> = new EventEmitter<{}>();

    @ViewChild('durationPopover', {static: true}) durationPopover: DxPopoverComponent;

    dateActions = [
        {id: 1, name: '1 Month', value: '1-M'},
        {id: 2, name: '2 Month', value: '2-M'},
        {id: 3, name: '3 Month', value: '3-M'},
        {id: 4, name: '4 Month', value: '4-M'},
        {id: 4, name: '5 Month', value: '5-M'},
        {id: 5, name: '6 Month', value: '6-M'},
        {id: 5, name: '7 Month', value: '7-M'},
        {id: 6, name: '8 Month', value: '8-M'},
        {id: 6, name: '9 Month', value: '9-M'},
        {id: 7, name: '10 Month', value: '10-M'},
        {id: 8, name: '11 Month', value: '11-M'},
        {id: 8, name: '12 Month', value: '12-M'},
        {id: 8, name: '18 Month', value: '18-M'},
        {id: 9, name: '24 Months', value: '24-M'},
    ];

    ngOnInit(): void {

    }

    performDateAction(data: any) {
        setTimeout(() => {
            if (this.durationPopover) {
                this.durationPopover.instance.hide();
            }
            if (data) {
                let valArr = data.value.split('-');
                if (valArr) {
                    let num = valArr[0];
                    let type = valArr[1];
                    this.value['endDate'] = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(new Date(), num, type));
                    this.value['startDate'] = new Date();
                    this.valueChange.emit(this.value);
                }
            }
        });
    }
}
