import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {AppDateRangeDurationComponent} from './app-date-range-duration.component';
import {PopupModule} from '@progress/kendo-angular-popup';
import {ReactiveFormsModule} from '@angular/forms';
import {DxPopoverModule} from 'devextreme-angular/ui/popover';


@NgModule({
  declarations: [
    AppDateRangeDurationComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    PopupModule,
    MatIconModule,
    ReactiveFormsModule,
    DxPopoverModule
  ],
  exports: [
    AppDateRangeDurationComponent
  ]
})
export class AppDateRangeDurationModule { }
