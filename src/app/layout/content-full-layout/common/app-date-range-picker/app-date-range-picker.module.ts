import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {AppDateRangePickerComponent} from './app-date-range-picker.component';
import {DxCalendarModule} from 'devextreme-angular/ui/calendar';
import {PopupModule} from '@progress/kendo-angular-popup';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppDateRangePickerComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    DxCalendarModule,
    PopupModule,
    MatIconModule,
    ReactiveFormsModule
  ],
  exports: [
    AppDateRangePickerComponent
  ]
})
export class AppDateRangePickerModule { }
