import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnInit, Optional,
    Output, Self,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import * as moment from 'moment';
import {AbstractControl, ControlValueAccessor, FormControl, FormGroup, NgControl, ValidationErrors} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material';
import {FocusMonitor} from '@angular/cdk/a11y';
import {Subject} from 'rxjs';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {DateUtils} from '../../../../shared/DateUtils';
import {AppDateRangeDurationComponent} from '../app-date-range-duration/app-date-range-duration.component';

@Component({
    selector: 'app-date-range',
    templateUrl: './app-date-range-picker.component.html',
    styleUrls: ['./app-date-range-picker.component.scss'],
    providers: [{provide: MatFormFieldControl, useExisting: AppDateRangePickerComponent}],
    encapsulation: ViewEncapsulation.None
})
export class AppDateRangePickerComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, AfterViewInit, OnChanges {

    readonly START = 'START';
    readonly END = 'END';

    @ViewChild('popup', {static: false, read: ElementRef}) public popup: ElementRef;
    @ViewChild('anchor', {static: false, read: ElementRef}) public anchor: ElementRef;
    @ViewChild('startField', {static: false, read: ElementRef}) public startField: ElementRef;
    @ViewChild('endField', {static: false, read: ElementRef}) public endField: ElementRef;

    @Input()
    startDateCtrlName: string = 'startDate';
    @Input()
    endDateCtrlName: string = 'endDateCtrl';
    @Input()
    format: string = '';
    @Input()
    isPicker: boolean = false;
    @Input()
    min: Date | number | string;
    @Input()
    max: Date | number | string;
    @Input()
    minZoomLevel: string = 'decade';
    @Input()
    maxZoomLevel: string = 'month';
    @Input()
    formGroup: FormGroup;
    @Input()
    showDuration: boolean = false;
    @Input()
    dateRangeDurationComponent: AppDateRangeDurationComponent;

    @Output()
    valueChange: EventEmitter<{}> = new EventEmitter<{}>();

    startDate: Date;
    endDate: Date;
    collision = {vertical: 'flip', horizontal: 'flip'};
    isCalendarOpen: boolean = false;
    isCalenderFocus: boolean = false;
    isTodayButton: boolean = false;
    currentSelectedField = this.START;
    _value: any;
    isAutoSelected = true;

    static nextId = 0;
    focused = false;
    controlType = 'app-time-picker';
    id = `app-time-picker${AppDateRangePickerComponent.nextId++}`;
    _class = `app-time-picker${AppDateRangePickerComponent.nextId++}`;
    describedBy = '';
    stateChanges: Subject<void> = new Subject<void>();

    onChange = (value) => {
    };
    onTouched = () => {
    };

    constructor(
        private _focusMonitor: FocusMonitor,
        private _elementRef: ElementRef<HTMLElement>,
        @Optional() @Self() public ngControl: NgControl
    ) {
        _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
            if (this.focused && !origin) {
                this.onTouched();
            }
            this.focused = !!origin;
            this.stateChanges.next();
        });

        if (this.ngControl != null) {
            this.ngControl.valueAccessor = this;
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(value: any): void {
        this.value = value ? value : new Date();
    }

    markAsTouched() {
        if (!this.touched) {
            this.onTouched();
            // this.touched = true;
        }
    }

    setDisabledState(disabled: boolean) {
        this.disabled = disabled;
    }

    changeVal(val: Date | number | string) {
        this.markAsTouched();
        if (!this.disabled) {
            this.setValue(val);
            this.value = {startDate: this.startDate, endDate: this.endDate};
            this.onChange(this.value);
            this.valueChange.emit(this.value);
        }
        this.isAutoSelected = false;
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return control.errors;
    }

    get touched(): boolean {
        return this.ngControl && this.ngControl.touched;
    }

    get errorState(): boolean {
        return this.ngControl && this.ngControl.invalid && this.touched;
    }

    get empty() {
        return !this.value;
    }

    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    @Input()
    get placeholder(): string {
        return this._placeholder;
    }

    set placeholder(value: string) {
        this._placeholder = value;
        this.stateChanges.next();
    }

    private _placeholder: string;

    @Input()
    get required(): boolean {
        return this._required;
    }

    set required(value: boolean) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _required = false;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _disabled = false;

    get fieldValue(): Date {
        return this.currentSelectedField === this.START ? this.startDate : this.endDate;
    }

    @Input()
    get value(): { [x: string]: Date } | null {
        return this._value;
    }

    set value(obj: { [x: string]: Date } | null) {
        this._value = obj;
        this.stateChanges.next();
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this._focusMonitor.stopMonitoring(this._elementRef);
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }

    // POPUP CALENDAR METHODS
    @HostListener('keydown', ['$event'])
    public keydown(event: any): void {
        if (event.keyCode === 27) {
            this.toggleCalendarPopup(false);
        }
    }

    @HostListener('document:click', ['$event'])
    public documentClick(event: any): void {
        event.stopPropagation();
        if (!this.isCalenderFocus && !this.contains(event.target)) {
            this.toggleCalendarPopup(false);
        }
    }

    private contains(target: any): boolean {
        if (!this.isCalendarOpen) {
            return false;
        }
        return (
            this.anchor.nativeElement.contains(target) ||
            (this.popup ? this.popup.nativeElement.contains(target) : false)
        );
    }

    ngOnInit(): void {
        if (this.dateRangeDurationComponent) {
            this.dateRangeDurationComponent.valueChange.subscribe((value) => {
                this.currentSelectedField = this.START;
                this.setValue(value.startDate);
                this.currentSelectedField = this.END;
                this.setValue(value.endDate);
                this.value = {startDate: this.startDate, endDate: this.endDate};
                this.onChange(this.value);
                this.valueChange.emit(this.value);
            })
        }
        if (!this.formGroup) {
            this.formGroup = new FormGroup({});
        }
        this.formGroup.addControl(this.startDateCtrlName, new FormControl());
        this.formGroup.addControl(this.endDateCtrlName, new FormControl());
        this.addEventOnAnchor();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['anchor'] && changes['anchor']['currentValue']) {
            this.setValue(this.value);
            this.addEventOnAnchor();
        }
    }

    ngAfterViewInit(): void {
        this.addEventOnAnchor();
    }

    private toggleCalendarPopup(val: boolean) {
        this.isCalendarOpen = val;
    }

    calenderPopupFocus(b: boolean) {
        this.isCalenderFocus = b;
    }

    optionChanged() {

    }

    private setValue(date) {
        if (this.currentSelectedField === this.START) {
            this.startDate = date;
            this.formGroup.get(this.startDateCtrlName).setValue(this.startDate);

            if (DateUtils.isGreater(this.startDate, this.endDate)) {
                this.formGroup.get(this.endDateCtrlName).setValue(null);
                this.endDate = null;
                this.currentSelectedField = this.END;
                this.focusEndField();
            }
        } else {
            this.endDate = date;
            this.formGroup.get(this.endDateCtrlName).setValue(this.endDate);

            if (DateUtils.isLess(this.endDate, this.startDate)) {
                this.formGroup.get(this.startDateCtrlName).setValue(date);
                this.startDate = date;
                this.currentSelectedField = this.START;
                this.focusStartField();
            }
        }

        if (this.isAutoSelected) {
            this.currentSelectedField = this.END;
            this.focusEndField();
        }
    }

    setTodayDate() {
    }

    openCalendarPopup() {
        this.focusStartField();
        this.isCalenderFocus = false;
        let leftArrContainer = document.querySelectorAll('.app-calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
        let rightArrContainer = document.querySelectorAll('.app-calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
        leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
        rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }

    private addEventOnAnchor() {
        if (this.anchor instanceof HTMLElement) {
            this.anchor = new ElementRef(this.anchor);
        }
        if (this.anchor) {
            this.anchor.nativeElement.onclick = () => {
                if (!this.isCalendarOpen) {
                    this.toggleCalendarPopup(!this.isCalendarOpen);
                }
            };
        }
    }

    onContainerClick(event: MouseEvent): void {
    }

    clickField(field: string) {
        this.currentSelectedField = field;
    }

    isSelected(date: Date) {
        return this.startDate && this.endDate && DateUtils.isBetween(this.startDate, this.endDate, date);
    }

    isStart(date: Date) {
        return this.startDate && DateUtils.isEqual(this.startDate, date);
    }

    isEnd(date: Date) {
        return this.endDate && DateUtils.isEqual(this.endDate, date);
    }

    private focusStartField() {
        if (this.startField) {
            this.startField.nativeElement.focus();
        }
    }

    private focusEndField() {
        if (this.endField) {
            this.endField.nativeElement.focus();
        }
    }

    get isValueSelected(): boolean {
        return !!this.startDate && !!this.endDate;
    }
}
