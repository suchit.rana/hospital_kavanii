import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import {TimePickerComponent} from './time-picker.component';
import {DxDateBoxModule} from 'devextreme-angular/ui/date-box';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    TimePickerComponent
  ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        DxDateBoxModule,
        ReactiveFormsModule
    ],
  exports: [
    TimePickerComponent
  ]
})
export class AppTimePickerModule { }
