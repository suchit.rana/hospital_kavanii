import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {DxDateBoxComponent} from 'devextreme-angular/ui/date-box';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {MatFormFieldControl} from '@angular/material';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.css'],
  // providers: [
  //   {
  //     provide: NG_VALUE_ACCESSOR,
  //     multi:true,
  //     useExisting: TimePickerComponent
  //   },
  //   {
  //     provide: NG_VALIDATORS,
  //     multi: true,
  //     useExisting: TimePickerComponent
  //   }
  // ],
  providers: [{provide: MatFormFieldControl, useExisting: TimePickerComponent}],
  encapsulation: ViewEncapsulation.None
})
export class TimePickerComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnDestroy {

  @ViewChild(DxDateBoxComponent, {static: false}) dateBox: DxDateBoxComponent;

  @Input()
  label: string = 'Start Time';
  @Input()
  icon: string = './assets/ic-clock.png';
  @Input()
  error: string = null;
  @Input()
  interval: number = 30;
  @Input()
  min: Date | number | string = 0;
  @Input()
  max: Date | number | string = 23;
  // @Input()
  // value: any = new Date();
  @Input()
  isDisplayIcon: boolean = true;
  @Input()
  editable: boolean = true;

  @Output()
  valueChange: EventEmitter<Date | number | string> = new EventEmitter<Date | number | string>();

  private opened: boolean = false;
  private _value = new Date();

  // touched = false;
  static nextId = 0;
  focused = false;
  controlType = 'app-time-picker';
  id = `app-time-picker${TimePickerComponent.nextId++}`;
  _class = `app-time-picker${TimePickerComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();

  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl) {

    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value ? value : new Date();
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  onOpened() {
    this.markAsTouched();
  }

  focusInOut(val: boolean) {
    this.opened = !val;
  }

  changeVal(val: Date | number | string) {
    this.markAsTouched();
    if (!this.disabled) {
      this.value = val;
      this.onChange(this.value);
      this.valueChange.emit(this.value);
    }
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.invalid && this.touched;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(date: any | null) {
    this._value = date || new Date();
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    this.dateBox.instance.open();
    // if ((event.target as Element).tagName.toLowerCase() != 'input') {
    //   this._elementRef.nativeElement.querySelector('input')!.focus();
    // }
  }

  contentReady(e) {
    if (this.editable) {
      return;
    }
    const dateBoxEditor = e.element.querySelector('.' + this._class + ' .dx-texteditor-input');
    if(dateBoxEditor){
      dateBoxEditor.setAttribute("readonly", '');
    }
  }
}
