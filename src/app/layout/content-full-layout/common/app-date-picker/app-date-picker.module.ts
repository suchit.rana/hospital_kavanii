import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import {DxDateBoxModule} from 'devextreme-angular/ui/date-box';
import {ReactiveFormsModule} from '@angular/forms';
import {AppDatePickerComponent} from './app-date-picker.component';
import {AppCalenderPopupModule} from '../calender-popup/app-calender-popup.module';


@NgModule({
  declarations: [
    AppDatePickerComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    DxDateBoxModule,
    ReactiveFormsModule,
    AppCalenderPopupModule
  ],
  exports: [
    AppDatePickerComponent
  ]
})
export class AppDatePickerModule {
}
