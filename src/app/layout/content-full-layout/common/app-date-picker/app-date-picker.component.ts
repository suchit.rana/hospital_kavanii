import {Component, ElementRef, EventEmitter, Input, OnDestroy, Optional, Output, Self, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {CalenderPopupComponent} from '../calender-popup/calender-popup.component';
import * as moment from 'moment';

@Component({
  selector: 'app-date-picker',
  templateUrl: './app-date-picker.component.html',
  styleUrls: ['./app-date-picker.component.css'],
  providers: [{provide: MatFormFieldControl, useExisting: AppDatePickerComponent}],
  encapsulation: ViewEncapsulation.None
})
export class AppDatePickerComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnDestroy {

  @ViewChild('targetAnchor', {static: false}) anchor: ElementRef;
  @ViewChild('calenderPopup', {static: false}) calenderPopup: CalenderPopupComponent;

  @Input()
  isDisplayIcon: boolean = true;
  @Input()
  icon: string = './assets/calendar%202.png';
  @Input()
  isTodayButton: boolean = true;
  @Input()
  format: string = 'DD/MM/YYYY';
  @Input()
  isPicker: boolean = false;
  @Input()
  min: Date | number | string;
  @Input()
  max: Date | number | string;
  @Output()
  valueChange: EventEmitter<Date | number | string> = new EventEmitter<Date | number | string>();

  private _value;

  // touched = false;
  static nextId = 0;
  focused = false;
  controlType = 'app-date-picker';
  id = `app-date-picker${AppDatePickerComponent.nextId++}`;
  _class = `app-date-picker${AppDatePickerComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();
  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl) {

    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  changeVal(val: Date | number | string) {
    this.markAsTouched();
    if (!this.disabled) {
      this.value = val;
      this.onChange(this.value);
      this.valueChange.emit(this.value);
    }
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.invalid && this.touched;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(date: any | null) {
    this._value = date;
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    // if (!this.calenderPopup.isCalendarOpen) {
    //   this.calenderPopup.openCalender();
    // }
  }

  get formattedValue() {
    return this.value ? moment(this.value).format(this.format) : null;
  }
}

