import {NgModule} from '@angular/core';
import {TabContentComponent} from './tab-content/tab-content.component';
import {ContentContainerDirective} from './tab-content/content-container.directive';
import {GridModule} from '@progress/kendo-angular-grid';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    TabContentComponent,
    ContentContainerDirective,
  ],
  imports: [
    GridModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    TabContentComponent,
    ContentContainerDirective,
    GridModule,
  ]
})
export class AppCommonModule {
}
