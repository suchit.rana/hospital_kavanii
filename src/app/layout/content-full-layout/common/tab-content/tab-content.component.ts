import { Component, Input, ComponentFactoryResolver, ViewChild, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, ChangeDetectionStrategy } from "@angular/core";
import { ContentContainerDirective } from "./content-container.directive";
import { Tab } from 'src/app/models/app.tab.model';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {display} from 'html2canvas/dist/types/css/property-descriptors/display';

@Component({
  selector: 'app-tab-content',
  template: "<ng-template content-container></ng-template>"
})

export class TabContentComponent implements OnInit, OnChanges {
  @Input() tab : Tab;
  @Input() active : boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild(ContentContainerDirective, { static: true}) contentContainer: ContentContainerDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  componentRef: any;

  ngOnInit() {
    this.loadComponent();
  }

  loadComponent() {
    const viewContainerRef = this.contentContainer.viewContainerRef;
    viewContainerRef.clear();

    const tab: Tab = this.tab;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      tab.component
    );

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    (this.componentRef.instance as SkeletonComponent).data = this.active;
    (this.componentRef.instance as SkeletonComponent).applyToggle.subscribe(val => {
      this.applyToggle.emit(val);
    });
  }

  ngOnChanges(changes: SimpleChanges)  {
    if(changes && changes.active && this.componentRef) {
      this.active = changes.active.currentValue;
      this.loadComponent();
    }
  }
}

export interface SkeletonComponent {
  data: boolean;
  applyToggle: EventEmitter<boolean>;
  clickEvent: any;
  queryEvent: any;
}
