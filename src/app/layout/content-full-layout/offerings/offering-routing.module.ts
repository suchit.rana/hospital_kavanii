import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddServiceComponent} from './service/add-service.component';
import {AddClassComponent} from './classes/add-class.component';
import {AddProductsComponent} from './products/add-products.component';
import {OfferingsComponent} from './offerings.component';


const routes: Routes = [
  { path: '', component: OfferingsComponent},
  { path: 'products/add', component: AddProductsComponent},
  { path: 'products/edit/:productId', component: AddProductsComponent},
  { path: 'classes/add', component: AddClassComponent},
  { path: 'classes/edit/:classId', component: AddClassComponent},
  { path: 'service/add', component: AddServiceComponent},
  { path: 'service/edit/:serviceId', component: AddServiceComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferingRoutingModule { }
