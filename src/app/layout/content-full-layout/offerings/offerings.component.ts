import {Component, OnInit} from '@angular/core';
import {AppState} from '../../../app.state';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {BaseGridComponent} from '../shared-content-full-layout/base-grid/base-grid.component';
import {OfferingsService} from '../../../services/app.offerings.service';
import {MatButtonToggleChange, MatTabChangeEvent} from '@angular/material';
import {ProductModel} from '../../../models/app.product.model';
import {ClassModel} from '../../../models/app.class.model';
import {ServiceModel} from '../../../models/app.service.model';
import {process, SortDescriptor} from '@progress/kendo-data-query';
import {GridDataResult} from '@progress/kendo-angular-grid';
import {RP_MODULE_MAP} from '../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-offerings',
  templateUrl: './offerings.component.html',
  styleUrls: ['./offerings.component.css'],
})
export class OfferingsComponent extends BaseGridComponent implements OnInit {

  readonly OFFERINGS_SERVICE = RP_MODULE_MAP.offerings_service;
  readonly OFFERINGS_CLASS = RP_MODULE_MAP.offerings_class;
  readonly OFFERINGS_PRODUCT = RP_MODULE_MAP.offerings_products;

  @BlockUI() blockUI: NgBlockUI;

  products: ProductModel[];
  sortP: SortDescriptor[] = [{dir: 'asc', field: 'productCode'}];
  productAll: any[] = [];
  classes: ClassModel[];
  sortC: SortDescriptor[] = [{dir: 'asc', field: 'classCode'}];
  classesAll: any[] = [];
  service: ServiceModel[];
  sortS: SortDescriptor[] = [{dir: 'asc', field: 'serviceCode'}];
  serviceAll: any[] = [];
  isRoot = false;
  isError = false;
  isLoading = true;
  detailViewProductShow = false;
  listViewProductShow = false;
  detailViewClassShow = false;
  listViewClassShow = false;
  detailViewServiceShow = false;
  listViewServiceShow = false;

  serviceTitle = '';
  serviceRouteLink = '';
  serviceRouteName = '';
  serviceDescription = '';
  classTitle = '';
  classRouteLink = '';
  classRouteName = '';
  classDescription = '';
  productTitle = '';
  productRouteLink = '';
  productRouteName = '';
  productDescription = '';

  constructor(
    public offeringsService: OfferingsService,
    public appState: AppState
  ) {
    super();
  }

  ngOnInit() {
    this.removeFilters();
    this.populateLanding();
  }

  populateLanding() {
    this.blockUI.start();

    this.offeringsService.getAllProducts().subscribe((products) => {
      this.productAll = [];
      if (products.length === 0) {
        this.productTitle = 'You haven\'t added a product';
        this.productRouteLink = '/offerings/products/add';
        this.productRouteName = 'Add Products';
        this.productDescription =
          'Health products are important for addressing health problems and improve quality of lives. They form an indispensable component of health systems in the prevention, diagnosis and treatment of disease and in alleviating disability and functional deficiency.';
        this.detailViewProductShow = false;
        this.listViewProductShow = false;
      } else {
        this.productTitle = '';
        this.detailViewProductShow = true;
        this.listViewProductShow = false;
      }
      products.forEach((d) => {
        if (d.productLogo) {
          d.productLogo = `data:image/jpg;base64,${d.productLogo}`;
        }
        d['isStatus'] = d['status'];
        this.productAll.push(d);
      });
      this.products = products.filter((x) => x.status === true);
      this.sort = [{dir: 'asc', field: 'productCode'}];
    });

    this.offeringsService.getAllClasses().subscribe((classes) => {
      this.classesAll = [];
      if (classes.length === 0) {
        this.classTitle = 'You haven\'t added a class';
        this.classRouteLink = '/offerings/classes/add';
        this.classRouteName = 'Add Classes';
        this.classDescription =
          'classes are the assistance that your business offer and provide to your customers';
        this.detailViewClassShow = false;
        this.listViewClassShow = false;
      } else {
        this.classTitle = '';
        this.detailViewClassShow = true;
        this.listViewClassShow = false;
      }
      classes.forEach((d) => {
        if (d.classLogo) {
          d.classLogo = `data:image/jpg;base64,${d.classLogo}`;
        }
        d['isStatus'] = d['status'];
        this.classesAll.push(d);
      });
      this.classes = classes.filter((x) => x.status === true);
      this.sort = [{dir: 'asc', field: 'classCode'}];
    });

    this.offeringsService.getAllService().subscribe((services) => {
      this.serviceAll = [];
      if (services.length === 0) {
        this.serviceTitle = 'You haven\'t added a service';
        this.serviceRouteLink = '/offerings/service/add';
        this.serviceRouteName = 'Add Service';
        this.serviceDescription =
          'Services are the assistance that your business offer and provide to your customers';
        this.detailViewServiceShow = false;
        this.listViewServiceShow = false;
      } else {
        this.serviceTitle = '';
        this.detailViewServiceShow = true;
        this.listViewServiceShow = false;
      }
      services.forEach((d) => {
        d['taxOption'] = d.taxOptionId == 0 ? 'No Tax' : (d.taxOptionId == 1 ? 'Inclusive' : 'Exclusive');
        if (d.serviceLogo) {
          d.serviceLogo = `data:image/jpg;base64,${d.serviceLogo}`;
        }
        d['isStatus'] = d['status'];
        this.serviceAll.push(d);
      });
      this.service = services.filter((x) => x.status === true);
      this.sort = [{dir: 'asc', field: 'serviceCode'}];
    });

    this.isLoading = false;
    this.blockUI.stop();
  }

  productActiveChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.products = this.productAll.filter((x) => x.status === true);
    } else {
      this.products = this.productAll.filter((x) => x.status === false);
    }
    this.products = this.products.sort();
  }

  productViewChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.detailViewProductShow = true;
      this.listViewProductShow = false;
    } else {
      this.detailViewProductShow = false;
      this.listViewProductShow = true;
    }
  }

  classActiveChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.classes = this.classesAll.filter((x) => x.status === true);
    } else {
      this.classes = this.classesAll.filter((x) => x.status === false);
    }
    this.classes = this.classes.sort();
  }

  classViewChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.detailViewClassShow = true;
      this.listViewClassShow = false;
    } else {
      this.detailViewClassShow = false;
      this.listViewClassShow = true;
    }
  }

  serviceActiveChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.service = this.serviceAll.filter((x) => x.status === true);
    } else {
      this.service = this.serviceAll.filter((x) => x.status === false);
    }
    this.service = this.service.sort();
  }

  serviceViewChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.detailViewServiceShow = true;
      this.listViewServiceShow = false;
    } else {
      this.detailViewServiceShow = false;
      this.listViewServiceShow = true;
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent) {
    this.appState.selectedTabState.next(tabChangeEvent.index);
    switch(tabChangeEvent.index) {
      case 0:
        this.sortP = [{dir: 'asc', field: 'productCode'}];
        break;
      case 1:
        this.sortC = [{dir: 'asc', field: 'classCode'}];
        break;
      case 2:
        this.sortS = [{dir: 'asc', field: 'serviceCode'}];
        break;
    }
  }

  sortBy = (field, a,  b) => {
    console.log(a[field])
    return a[field].localeCompare(b[field]);
  }

  // private resetGridState(fieldName: string, isActive = true) {
  //   this.skip = 0;
  //   this.pageSize = 10;
  //   this.sort = [{dir: 'asc', field: fieldName}];
  //   this.activeFilters = {
  //     logic: 'and',
  //     filters: [{field: 'isStatus', operator: 'eq', value: isActive}]
  //   };
  // }
}
