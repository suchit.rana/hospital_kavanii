import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {OfferingsService} from 'src/app/services/app.offerings.service';
import {SettingsService} from 'src/app/services/app.settings.service';
import {StaffService} from 'src/app/services/app.staff.service';

import {AppState} from 'src/app/app.state';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute} from '@angular/router';
import {ImageSnippet} from '../../../../models/app.misc';
import {
  ClassConcessionModel,
  ClassLocationModel,
  ClassModel,
  ClassPractitionerModel,
  ClassRelatedProductModel,
  ClassSpecialityModel,
} from '../../../../models/app.class.model';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {groupBy, GroupResult} from '@progress/kendo-data-query';
import { DashboardModel } from 'src/app/models/app.dashboard.model';
import { DashboardService } from 'src/app/services/app.dashboard.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css'],
})
export class AddClassComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.offerings_class;
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(ImageUploadComponent, {static: true})
  image: ImageUploadComponent;

  submitting = false;
  classCategoryId: number;
  classCategoryParentId: number;
  private dashboard:DashboardModel;
  classForm: FormGroup = new FormGroup({
    classLogo: new FormControl(''),
    colour: new FormControl(''),
    locationName: new FormControl('', Validators.required),
    specialityName: new FormControl('', Validators.required),
    practitionerName: new FormControl('', Validators.required),
    classCode: new FormControl('', Validators.required),
    medicareCode: new FormControl(''),
    dvaCode: new FormControl(''),
    className: new FormControl('', Validators.required),
    description: new FormControl('',Validators.required),
    categoryId: new FormControl('',Validators.required),
    productName: new FormControl(''),
    duration: new FormControl('',Validators.required),
    maximumParticipants: new FormControl('',Validators.required),
    standardPrice: new FormControl('', Validators.required),
    taxTypeId: new FormControl('',Validators.required),
    taxOptionId: new FormControl(0,Validators.required),
    onlineBooking: new FormControl(true),
    status: new FormControl(true),
    isAllowAllLocation: new FormControl(false),
  });

  public selectedCategory: any;
  locationList: any[] = [];
  categories: any[] = [];
  concessionList: any[] = [];
  concessions: any[] = [];
  concessionAllList: any[] = [];
  specialityAllList: any[] = [];
  practitionerAllList: any[] = [];
  taxTypes: any[];
  taxOptions: any[];
  apperance = 'outline';
  addClass = true;
  catData: any[];
  public selectedCategoryName: any[];

  specialityEnable = true;
  practitionerEnable = true;
  locationEnable = false;
  concessionsEnable = false;

  addItem: boolean;
  itemid: any;
  public categorySelect: string;
  public taxTypeSelect: string;
  public taxOptionSelect: string;
  public selectionTaxOption: string;
  public filter: string;
  selectedColour: string;
  products: any[];
  categoryId: number;
  public specialityData: any[] = [];
  public groupedSpecialityData: GroupResult[];
  public practitionerData: any[] = [];
  public groupedPractitionerData: GroupResult[];
  specialityDataTemp: any[] = [];
  practitionerDataTemp: any[] = [];
  taxOptionHide = true;
  taxTypeId: string;
  classes: ClassModel;
  selectedLocation: any[];
  selectedSpeciality: any[];
  taxOptionSelected: number;

  constructor(
    protected appState: AppState,
    public location: Location,
    private _route: ActivatedRoute,
    private offeringsService: OfferingsService,
    private settingsService: SettingsService,
    private businessService: BusinessService,
    private staffService: StaffService,
    private dashboardService:DashboardService
  ) {
    super(location);
    this.fillTaxTypes();
  }

  ngOnInit() {
    this.classCategoryId = 0;
    this.classes = new ClassModel();

    this.classForm.get('dvaCode').valueChanges.subscribe(value => {
      if (!parseInt(value) && parseInt(value) == 0) {
        this.classForm.get('dvaCode').setErrors({notZero: true});
      }else{
        this.classForm.get('dvaCode').setErrors(null);
      }
    });

    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((locations) => {
        this.locationList = locations;
        this.locationPopulate();
      });

    this.settingsService.getAllConcessions().subscribe((concessionData) => {
      this.concessionList = this.concessionAllList = concessionData;
      this.concessionPopulate();
    });

    this.classCategoryParentId = ApplicationDataEnum.ClassCategory;
    this.settingsService.getAllSpecialties().subscribe((specialityData) => {
      this.specialityAllList = specialityData;
      this.specialityPopulate();
    });

    this.staffService.getPractitioners().subscribe((practitionersData) => {
      this.practitionerAllList = practitionersData;
      this.practitionersPopulate();
    });
    this.dashboardService.getDashboardCheckListView().subscribe(d => {
      this.dashboard = d;
    });

    this.offeringsService.getAllProducts().subscribe((productsData) => {
      this.products = productsData.filter((x) => x.status === true);
      this.productsPopulate();

    });
    this._route.params.subscribe((params) => {
      if (params.classId) {
        this.blockUI.start();
        this.addItem = false;
        this.itemid = params.classId;

        this.offeringsService
          .getClassById(params.classId)
          .subscribe((data: ClassModel) => {
            this.classForm.patchValue(data);
            this.classes = data;
            if (data.classLogo) {
              this.image.selectedFile = new ImageSnippet(
                `data:image/jpeg;base64,${data.classLogo}`,
                null
              );
            }
            this.selectedColour = data.colour;
            this.taxOptionSelected = data.taxOptionId;
            this.taxTypeId = data.taxTypeId;
            if (this.taxTypes !== undefined) {
              this.handleTaxTypeChange(this.taxTypeId);
            }

            this.locationPopulate();
            this.specialityPopulate();
            this.practitionersPopulate();
            this.productsPopulate();
            this.classCategoryId = data.categoryId;
            this.blockUI.stop();
          });
      }
    });
  }

  productsPopulate() {
    if (
      this.products !== undefined &&
      this.classes.classRelatedProduct !== undefined
    ) {
      const productName = [];
      this.classes.classRelatedProduct.forEach((rp) => {
        productName.push(this.products.find((p) => p.id === rp.productId));
      });
      this.classForm.get('productName').patchValue(productName);
    }
  }

  practitionersPopulate() {
    if (this.practitionerAllList !== undefined && this.classes.classPractitioner !== undefined) {
      this.populatePractitioner(this.selectedSpeciality);
      this.classes.classPractitioner.forEach((p) => {
        const location = this.locationList.find((x) => x.id === p.locationId);
        const speciality = this.specialityAllList.find((x) => x.id === p.specialityId);
        const practitioner = this.practitionerAllList.find((x) => x.id === p.practitionerId);

        if (location && speciality && practitioner) {
          const sData = {
            id: p.practitionerId,
            idAndSpecialityIdAndLocationId: p.practitionerId + p.specialityId + p.locationId,
            firstName: practitioner.firstName + ' (' + location.locationName + ')',
            specialityId: p.specialityId,
            specialityName: speciality.specialityName,
            locationId: p.locationId,
            locationName: location.locationName,
          };

          this.practitionerDataTemp.push(sData);
        }
      });

      if (this.practitionerDataTemp.length > 0) {
        this.practitionerEnable = false;
      }

      this.classForm
        .get('practitionerName')
        .patchValue(this.practitionerDataTemp);
    }
  }

  specialityPopulate() {
    if (this.locationList.length === 0) {
      return;
    }
    this.specialityEnable = false;
    if (
      this.specialityAllList !== undefined &&
      this.classes.classSpeciality !== undefined
    ) {
      this.populateSpeciality(this.selectedLocation);
      const specialityName = [];
      this.specialityDataTemp = [];
      this.classes.classSpeciality.forEach((s) => {
        const location = this.locationList.find((x) => x.id === s.locationId);
        const speciality = this.specialityAllList.find(
          (x) => x.id === s.specialityId
        );

        const sData = {
          id: '',
          idAndLocationId: '',
          specialityName: '',
          locationId: '',
          locationName: '',
        };
        sData.id = s.specialityId;
        sData.idAndLocationId = s.specialityId + location.id;
        sData.specialityName =
          speciality.specialityName + ' (' + location.locationName + ')';
        sData.locationId = location.id;
        sData.locationName = location.locationName;

        this.specialityDataTemp.push(sData);

        specialityName.push(
          this.specialityAllList.find((sl) => sl.id === s.specialityId)
        );
      });
      this.selectedSpeciality = this.specialityDataTemp;
      this.classForm
        .get('specialityName')
        .patchValue(this.specialityDataTemp);
    }
  }

  concessionPopulate() {

    if (
      this.concessionAllList !== undefined &&
      this.classes.classConcession !== undefined
    ) {
      this.concessions = [];
      if (this.classes.classConcession.length > 0) {
        this.concessionsEnable = true;
        this.classes.classConcession.forEach((c) => {
          const model = new ClassConcessionModel();
          model.concessionId = c.concessionId;
          model.concessionAmount = c.concessionAmount;
          model.concessionType = this.concessionAllList.find(
            (x) => x.id === c.concessionId
          ).concessionType;
          model.locationName = this.locationList.find(
            (x) => x.id === c.locationId
          ).locationName;
          model.locationId = c.locationId;
          model.classId = c.classId;
          model.isStatus = c.isStatus;
          this.concessions.push(model);
        });
      }
      this.populateConcession(this.selectedLocation);
    }
  }

  allLocationChange($event) {
    if ($event === true) {
      const locationName = [];
      if (this.locationList !== undefined) {
        this.locationList.forEach((l) => {
          locationName.push(l);
        });
        this.selectedLocation = locationName;
        this.classForm.get('locationName').patchValue(locationName);
        this.populateSpeciality(locationName);
        this.populateConcession(this.selectedLocation);
        // this.serviceForm.get("specialityName").patchValue(locationName);
      }
    } else {
      this.classForm.get('locationName').patchValue('');
      this.classForm.get('specialityName').patchValue('');
      this.classForm.get('practitionerName').patchValue('');
      this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
    }
  }

  locationPopulate() {
    const locationName = [];
    if (this.locationList !== undefined &&
      this.classes.classLocation !== undefined) {
      this.classes.classLocation.forEach((pl) => {
        const data = this.locationList.find((l) => l.id === pl.locationId);
        if (data) {
          locationName.push();
        }
      });
      this.selectedLocation = locationName;
      this.concessionPopulate();
      this.populateSpeciality(locationName);
      this.classForm.get('locationName').patchValue(locationName);
      this.locationEnable = false;
    }
  }

  public onColourChange(color: string): void {
    this.selectedColour = color;
  }

  fillTaxTypes() {
    this.settingsService.getAllTaxes().subscribe((data) => {
      this.taxTypes = data;
      const noTax = this.taxTypes.find(value => value.taxType.toLowerCase() === 'no tax');
      if (noTax) {
        this.classForm.get('taxTypeId').setValue(noTax.id);
        this.classes.taxTypeId = noTax.id;
        this.taxTypeId = noTax.id;
      }
      if (this.classes.taxTypeId !== undefined) {
        this.handleTaxTypeChange(this.taxTypeId);
      }
    });
  }

  createClass() {
    const el = document.getElementById('heading');
    const classModel: ClassModel = this.classForm.value;
    const locations: ClassLocationModel[] = [];
    const speciality: ClassSpecialityModel[] = [];
    const practitioner: ClassPractitionerModel[] = [];
    const relatedProduct: ClassRelatedProductModel[] = [];
    const concession: ClassConcessionModel[] = [];

    if (!this.classForm.invalid) {
      this.blockUI.start();
      this.submitting = true;

      if (this.image.selectedFile) {
        classModel.classLogo = this.image.selectedFile.src
          .replace('data:', '')
          .replace(/^.+,/, '');
      }

      if (classModel.locationName !== '') {
        classModel.locationName.forEach((l) => {
          const model = new ClassLocationModel();
          model.locationId = l.id;
          model.classId = this.itemid;
          locations.push(model);
        });
      }

      if (classModel.specialityName !== '') {
        classModel.specialityName.forEach((s) => {
          const model = new ClassSpecialityModel();
          model.locationId = s.locationId;
          model.specialityId = s.id;
          model.classId = this.itemid;
          speciality.push(model);
        });
      }

      if (classModel.practitionerName !== '') {
        classModel.practitionerName.forEach((p) => {
          const model = new ClassPractitionerModel();
          model.locationId = p.locationId;
          model.specialityId = p.specialityId;
          model.practitionerId = p.id;
          model.classId = this.itemid;
          practitioner.push(model);
        });
      }

      if (classModel.productName !== '') {
        classModel.productName.forEach((rp) => {
          const model = new ClassRelatedProductModel();
          model.productId = rp.id;
          model.classId = this.itemid;
          relatedProduct.push(model);
        });
      }

      if (this.concessionList.length > 0) {
        this.concessionList.forEach((c) => {
          const model = new ClassConcessionModel();
          model.concessionId = c.id;
          model.locationId = c.locationId;
          model.concessionAmount = Number(
            (document.getElementById(
              c.locationId + c.concessionType
            ) as HTMLInputElement).value
          );
          model.classId = this.itemid;
          concession.push(model);
        });
      }

      classModel.classLocation = locations;
      classModel.classSpeciality = speciality;
      classModel.classPractitioner = practitioner;
      classModel.classRelatedProduct = relatedProduct;
      classModel.classConcession = concession;
      classModel.colour = this.selectedColour;
      classModel.categoryId = this.classCategoryId;
      classModel.taxTypeId = classModel.taxTypeId;

      if (this.taxOptionHide === true && classModel.taxOptionId === 0) {
        this.displayErrorMessage(
          'Error occurred while adding class, please try again.'
        );
        this.blockUI.stop();
        el.scrollIntoView();
        this.submitting = false;
        return;
      }

      if (this.addItem) {
        this.offeringsService.createClass(classModel).subscribe(
          () => {
            this.dashboard.isClassAdded = true;
            this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u=>{
              this.submitting = false;
              this.displaySuccessMessage('Class added successfully.');
              this.cancel();
              el.scrollIntoView();
              this.blockUI.stop();
            },error=>{
              this.displayErrorMessage(
                'Error occurred while adding Class, please try again.'
              );
              this.submitting = false;
              el.scrollIntoView();
              this.blockUI.stop();
            });

          },
          () => {
            this.displayErrorMessage(
              'Error occurred while adding Class, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      } else {
        classModel.id = this.itemid;
        this.offeringsService.updateClass(classModel).subscribe(
          () => {
            this.submitting = false;
            this.displaySuccessMessage('Class updated successfully.');
            this.cancel();
            el.scrollIntoView();
            this.blockUI.stop();
          },
          () => {
            this.displayErrorMessage(
              'Error occurred while updating Class, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      }
    }
  }

  onLocationChange(locationData) {
    if (locationData.length === 0) {
      this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
      this.specialityEnable = true;
      this.practitionerEnable = true;
      this.specialityData = [];
      this.practitionerData = [];
      this.classForm.get('specialityName').patchValue('');
      this.classForm.get('practitionerName').patchValue('');
    } else {
      this.removeSpeciality(locationData);
      this.populateSpeciality(locationData);
      this.populateConcession(locationData);
    }
  }

  removeSpeciality(locationData) {
    this.specialityDataTemp = [];
    const specialityNameData = this.classForm.get('specialityName').value;
    if (specialityNameData.length > 0) {
      locationData.forEach((e) => {
        const speciality = specialityNameData.filter(
          (x) => x.locationName === e.locationName
        );

        speciality.forEach((element) => {
          const sData = {
            id: element.id,
            idAndLocationId: element.idAndLocationId,
            specialityName: element.specialityName,
            locationId: element.locationId,
            locationName: element.locationName,
          };

          this.specialityDataTemp.push(sData);
        });
      });
      this.classForm.get('specialityName').patchValue(this.specialityDataTemp);
    }
  }

  removePractitioner(specialityData) {
    this.practitionerDataTemp = [];
    const practitionerNameData = this.classForm.get('practitionerName').value;
    if (practitionerNameData.length > 0) {
      specialityData.forEach((e) => {
        const practitioner = practitionerNameData.filter(
          (x) => x.locationId === e.locationId && x.specialityId === e.id
        );

        practitioner.forEach((element) => {
          const sData = {
            id: element.id,
            idAndSpecialityIdAndLocationId:
            element.idAndSpecialityIdAndLocationId,
            firstName: element.firstName,
            specialityId: element.specialityId,
            specialityName: element.specialityName,
            locationId: element.locationId,
            locationName: element.locationName,
          };

          this.practitionerDataTemp.push(sData);
        });
      });
      this.classForm
        .get('practitionerName')
        .patchValue(this.practitionerDataTemp);

      if (this.practitionerDataTemp.length > 0) {
        this.practitionerEnable = false;
      } else {
        this.practitionerEnable = true;
        this.practitionerData = [];
        this.groupedPractitionerData = groupBy(
          [],
          [{field: 'specialityName'}]
        );
        this.classForm.get('practitionerName').patchValue('');
      }
    }
  }

  populateSpeciality(locationData) {
    if (!locationData) {
      return;
    }
    this.specialityData = [];
    locationData.forEach((e) => {
      this.specialityEnable = false;
      this.specialityAllList.forEach((s) => {
        let sLocation;
        if (s.isAllowAllLocation) {
          sLocation = s;
        } else {
          sLocation = s.specialityLocation.find((x) => x.locationId === e.id || x.isAllowAllLocation);
        }
        if (sLocation) {
          const specialityDataCheck = this.specialityData.find((x) => x.locationId === e.id && x.id === s.id);
          const sData = {
            id: '',
            idAndLocationId: '',
            specialityName: '',
            locationId: '',
            locationName: '',
          };
          sData.id = s.id;
          sData.idAndLocationId = s.id + e.id;
          sData.specialityName =
            s.specialityName + ' (' + (sLocation.isAllowAllLocation ? 'All Location' : sLocation.locationName) + ')';
          sData.locationId = e.id;
          sData.locationName = e.locationName;
          if (specialityDataCheck === undefined) {
            this.specialityData.push(sData);
            this.groupedSpecialityData = groupBy(this.specialityData, [
              {field: 'locationName'},
            ]);
          }
        }
      });

      //this.removePractitioner(this.specialityData);
    });

    if (this.specialityData.length > 0) {
      this.specialityEnable = false;
    } else {
      this.specialityEnable = true;
    }
  }

  populateConcession(locationData) {
    if (!locationData) {
      return;
    }
    this.concessionList = [];
    locationData.forEach((e) => {
      this.concessionAllList.forEach((s) => {
        let cLocation;
        if (s.isAllowAllLocation) {
          cLocation = s;
        } else {
          cLocation = s.concessionLocation ? s.concessionLocation.filter((x) => x.locationId === e.id || x.isAllowAllLocation) : [];
        }

        if (cLocation && cLocation.length > 0) {
          cLocation.forEach((cl) => {
            let cAmount = 0;
            if (this.concessions !== undefined && this.concessions.length > 0) {
              const cList = this.concessions.find(
                (x) => x.locationId === cl.locationId && x.id === s.concessionId
              );
              if (cList !== undefined) {
                cAmount = cList.concessionAmount;
              }
            }
            const sData = {
              id: s.id,
              locationId: cl.locationId,
              concessionAmount: cAmount,
              concessionType: s.concessionType,
              serviceId: '',
              locationName: cl.locationName,
            };

            this.concessionList.push(sData);
          });
          this.concessionsEnable = true;
        }
      });
    });
    if (this.concessionList === undefined) {
      this.concessionsEnable = false;
    }
  }

  onSpecialityChange(specialityData) {
    if (specialityData.length === 0) {
      this.practitionerEnable = true;
      this.practitionerData = [];
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
      this.classForm.get('practitionerName').patchValue('');
    } else {
      this.removePractitioner(specialityData);
      this.populatePractitioner(specialityData);
    }
  }

  populatePractitioner(specialityData) {
    if (!specialityData) {
      return;
    }
    specialityData.forEach((e) => {
      const pList = this.practitionerAllList.filter((x) => x.specilities.indexOf(e.id) > -1 && x.locations.indexOf(e.locationId) > -1);
      if (pList !== undefined && pList.length > 0) {
        pList.forEach((p) => {
          const lName = this.locationList.find((x) => p.locations.indexOf(x.id) > -1);
          const sName = this.specialityAllList.find(
            (x) => p.specilities.indexOf(x.id) > -1
          );

          const d = this.practitionerData.filter((x) => x.id + x.specialityId + x.locationId === p.id + p.specialityId + p.locationId);

          if (d.length === 0) {
            const pData = {
              id: p.id,
              idAndSpecialityIdAndLocationId:
                p.id + p.specialityId + p.locationId,
              firstName: p.firstName + ' (' + lName.locationName + ')',
              specialityId: p.specialityId,
              specialityName: sName.specialityName,
              locationId: p.locationId,
              locationName: lName.locationName,
            };

            this.practitionerData.push(pData);
          }
        });
      }
    });
    if (this.practitionerData.length > 0) {
      this.groupedPractitionerData = groupBy(this.practitionerData, [
        {field: 'specialityName'},
      ]);
      this.practitionerEnable = false;
    } else {
      this.practitionerEnable = true;
    }
  }

  handleTaxTypeChange(e) {
    const tt = this.taxTypes.filter((x) => x.id === e);
    if (tt[0].taxType.toLowerCase() === 'no tax') {
      this.taxOptionHide = false;
      this.classForm.get('taxOptionId').patchValue(0);
    } else {
      this.taxOptionHide = true;
    }
  }


  appIdClassCategoryHandler($event) {
    this.classCategoryId = $event;
    this.classForm.get('categoryId').setValue(this.classCategoryId);
  }
}
