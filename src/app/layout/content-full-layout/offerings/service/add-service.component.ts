import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {OfferingsService} from 'src/app/services/app.offerings.service';
import {SettingsService} from 'src/app/services/app.settings.service';
import {StaffService} from 'src/app/services/app.staff.service';

import {AppState} from 'src/app/app.state';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute} from '@angular/router';
import {ImageSnippet} from '../../../../models/app.misc';
import {
  ServiceConcessionModel,
  ServiceLocationModel,
  ServiceModel,
  ServicePractitionerModel,
  ServiceRelatedProductModel,
  ServiceSpecialityModel,
} from '../../../../models/app.service.model';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {groupBy, GroupResult} from '@progress/kendo-data-query';
import {TreatmentNotesService} from '../../../../services/app.treatmentnotes.services';
import { DashboardModel } from 'src/app/models/app.dashboard.model';
import { DashboardService } from 'src/app/services/app.dashboard.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
const uniqueBy = require('lodash/uniqBy');

@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css'],
})
export class AddServiceComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.offerings_service;

  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(ImageUploadComponent, {static: true})
  image: ImageUploadComponent;

  submitting = false;
  serviceCategoryId: number;
  serviceCategoryParentId: number;

  serviceForm: FormGroup = new FormGroup({
    serviceLogo: new FormControl(''),
    colour: new FormControl(''),
    locationName: new FormControl('', Validators.required),
    specialityName: new FormControl('', Validators.required),
    practitionerName: new FormControl('', Validators.required),
    serviceCode: new FormControl('', Validators.required),
    medicareCode: new FormControl(''),
    dvaCode: new FormControl(''),
    serviceName: new FormControl('', Validators.required),
    description: new FormControl(''),
    categoryId: new FormControl('', Validators.required),
    productName: new FormControl(''),
    defaultTreatmentNotesTemplateId: new FormControl(''),
    duration: new FormControl(''),
    standardPrice: new FormControl('', Validators.required),
    taxTypeId: new FormControl(''),
    taxOptionId: new FormControl(0),
    onlineBooking: new FormControl(true),
    status: new FormControl(true),
    isAllowAllLocation: new FormControl(false),
  });

  public selectedCategory: any;
  taxOptionSelected: number;
  locationList: any[];
  concessionList: any[];
  concessions: any[];
  concessionAllList: any[];
  specialityAllList: any[];
  practitionerAllList: any[];
  taxTypes: any[];
  taxOptions: any[];
  apperance = 'outline';
  addService = true;
  catData: any[];
  public selectedCategoryName: any[];

  specialityEnable = true;
  practitionerEnable = true;
  locationEnable = false;
  concessionsEnable = false;
  private dashboard: DashboardModel;

  addItem: boolean;
  itemid: any;
  public categorySelect: string;
  public taxTypeSelect: string;
  public taxOptionSelect: string;
  public selectionTaxOption: string;
  public filter: string;
  selectedColour: string;
  products: any[];
  treatmentNotesTemplate: any[];
  categoryId: number;
  public specialityData: any[] = [];
  public groupedSpecialityData: GroupResult[];
  public practitionerData: any[] = [];
  public groupedPractitionerData: GroupResult[];
  specialityDataTemp: any[] = [];
  practitionerDataTemp: any[] = [];
  taxOptionHide = true;
  taxTypeId: string;
  service: ServiceModel;
  selectedLocation: any[];
  selectedSpeciality: any[];

  constructor(
    protected appState: AppState,
    public location: Location,
    private _route: ActivatedRoute,
    private offeringsService: OfferingsService,
    private settingsService: SettingsService,
    private businessService: BusinessService,
    private staffService: StaffService,
    private treatmentNotesService: TreatmentNotesService,
    private dashboardService:DashboardService
  ) {
    super(location);
    this.fillTaxTypes();
  }

  ngOnInit() {
    this.serviceCategoryId = 0;
    this.service = new ServiceModel();

    this.serviceForm.get('medicareCode').valueChanges.subscribe(value => {
      if (!parseInt(value) && parseInt(value) == 0) {
        this.serviceForm.get('medicareCode').setErrors({notZero: true});
      }else{
        this.serviceForm.get('medicareCode').setErrors(null);
      }
    });

    this.serviceForm.get('dvaCode').valueChanges.subscribe(value => {
      if (!parseInt(value) && parseInt(value) == 0) {
        this.serviceForm.get('dvaCode').setErrors({notZero: true});
      }else{
        this.serviceForm.get('dvaCode').setErrors(null);
      }
    });

    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((locations) => {
        this.locationList = locations;
        this.locationPopulate();
      });

    this.settingsService.getAllConcessions().subscribe((concessionData) => {
      this.concessionList = this.concessionAllList = concessionData;
      this.concessionPopulate();
    });

    this.dashboardService.getDashboardCheckListView().subscribe(d => {
      this.dashboard = d;
    });


    this.serviceCategoryParentId = ApplicationDataEnum.ServiceCategory;
    this.settingsService.getAllSpecialties().subscribe((specialityData) => {
      this.specialityAllList = specialityData;
      this.specialityPopulate();
    });

    this.staffService.getPractitioners().subscribe((practitionersData) => {
      this.practitionerAllList = practitionersData;
      this.practitionersPopulate();
    });

    this.offeringsService.getAllProducts().subscribe((productsData) => {
      this.products = productsData.filter((x) => x.status === true);
      this.productsPopulate();
    });

    this.treatmentNotesService.getAllTreatmentNotesTemplateByLocationId(this.appState.selectedUserLocationId).subscribe(response => {
      this.treatmentNotesTemplate = response.filter(value => value.isStatus);
    });

    this._route.params.subscribe((params) => {
      if (params.serviceId) {
        this.blockUI.start();
        this.addItem = false;
        this.itemid = params.serviceId;

        this.offeringsService
          .getServiceById(params.serviceId)
          .subscribe((data: ServiceModel) => {
            this.service = data;
            this.serviceForm.patchValue(data);
            if (data.serviceLogo) {
              this.image.selectedFile = new ImageSnippet(
                `data:image/jpeg;base64,${data.serviceLogo}`,
                null
              );
            }
            this.selectedColour = data.colour;
            this.taxOptionSelected = data.taxOptionId;
            this.taxTypeId = data.taxTypeId;
            if (this.taxTypes !== undefined) {
              this.handleTaxTypeChange(this.taxTypeId);
            }

            this.locationPopulate();
            this.specialityPopulate();
            this.practitionersPopulate();
            this.productsPopulate();

            this.serviceCategoryId = data.categoryId;
            this.blockUI.stop();
          });
      }
    });
  }

  concessionPopulate() {
    if (this.concessionAllList !== undefined && this.service.serviceConcession !== undefined) {
      this.concessions = [];
      if (this.service.serviceConcession.length > 0) {
        this.concessionsEnable = true;
        this.service.serviceConcession.forEach((c) => {
          const model = new ServiceConcessionModel();
          model.concessionId = c.concessionId;
          model.concessionAmount = c.concessionAmount;
          model.concessionType = this.concessionAllList.find(
            (x) => x.id === c.concessionId
          ).concessionType;
          const location = this.locationList.find((x) => x.id === c.locationId || c.isAllowAllLocation);
          model.locationName = location ? location.locationName : '';
          model.locationId = c.locationId;
          model.isAllowAllLocation = c.isAllowAllLocation;
          model.serviceId = c.serviceId;
          model.isStatus = c.isStatus;
          this.concessions.push(model);
        });
      }
      this.populateConcession(this.selectedLocation);
    }
  }

  productsPopulate() {
    if (
      this.products !== undefined &&
      this.service.serviceRelatedProduct !== undefined
    ) {
      const productName = [];
      this.service.serviceRelatedProduct.forEach((rp) => {
        productName.push(this.products.find((p) => p.id === rp.productId));
      });
      this.serviceForm.get('productName').patchValue(productName);
    }
  }

  practitionersPopulate() {
    if (
      this.practitionerAllList !== undefined &&
      this.service.servicePractitioner !== undefined
    ) {
      this.populatePractitioner(this.selectedSpeciality);
      this.service.servicePractitioner.forEach((p) => {
        const location = this.locationList.find((x) => x.id === p.locationId);
        const speciality = this.specialityAllList.find(
          (x) => x.id === p.specialityId
        );
        const practitioner = this.practitionerAllList.find(
          (x) => x.id === p.practitionerId
        );

        if (practitioner && location) {
          const sData = {
            id: p.practitionerId,
            idAndSpecialityIdAndLocationId: p.practitionerId + p.specialityId + p.locationId,
            firstName: practitioner.firstName + ' (' + location.locationName + ')',
            specialityId: p.specialityId,
            specialityName: speciality.specialityName,
            locationId: p.locationId,
            locationName: location.locationName,
          };
          this.practitionerDataTemp.push(sData);
        }
      });

      if (this.practitionerDataTemp.length > 0) {
        this.practitionerEnable = false;
      }

      this.serviceForm
        .get('practitionerName')
        .patchValue(this.practitionerDataTemp);
    }
  }

  locationPopulate() {
    const locationName = [];
    if (this.locationList !== undefined && this.service.serviceLocation !== undefined) {
      this.service.serviceLocation.forEach((pl) => {
        const data = this.locationList.find((l) => l.id === pl.locationId);
        if (data) {
          locationName.push(data);
        }
      });
      this.selectedLocation = locationName;
      this.concessionPopulate();
      //`this.populateSpeciality(locationName);`
      this.serviceForm.get('locationName').patchValue(locationName);
      this.locationEnable = false;
    }
  }

  public onColourChange(color: string): void {
    this.selectedColour = color;
  }

  specialityPopulate() {
    this.specialityEnable = false;
    if (this.specialityAllList && this.service.serviceSpeciality && this.locationList && this.specialityAllList) {
      this.populateSpeciality(this.selectedLocation);
      const specialityName = [];
      this.specialityDataTemp = [];
      this.service.serviceSpeciality.forEach((s) => {
        const location = this.locationList.find((x) => x.id === s.locationId);
        const speciality = this.specialityAllList.find((x) => x.id === s.specialityId);

        if (location && speciality) {
          const sData = {
            id: '',
            idAndLocationId: '',
            specialityName: '',
            locationId: '',
            locationName: '',
          };
          sData.id = s.specialityId;
          sData.idAndLocationId = s.specialityId + location.id;
          sData.specialityName =
            speciality.specialityName + ' (' + location.locationName + ')';
          sData.locationId = location.id;
          sData.locationName = location.locationName;

          this.specialityDataTemp.push(sData);

        }
        specialityName.push(this.specialityAllList.find((sl) => sl.id === s.specialityId));
      });
      this.selectedSpeciality = this.specialityDataTemp;
      this.serviceForm.get('specialityName').patchValue(this.specialityDataTemp);
    }
  }

  allLocationChange($event) {
    if ($event === true) {
      const locationName = [];
      if (this.locationList !== undefined) {
        this.locationList.forEach((l) => {
          locationName.push(l);
        });
        this.selectedLocation = locationName;
        this.serviceForm.get('locationName').patchValue(locationName);
        this.populateSpeciality(locationName);
        this.populateConcession(this.selectedLocation);
        // this.serviceForm.get("specialityName").patchValue(locationName);
      }
    } else {
      this.serviceForm.get('locationName').patchValue('');
      this.serviceForm.get('specialityName').patchValue('');
      this.serviceForm.get('practitionerName').patchValue('');
      this.concessionList = [];
      this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
    }
  }

  fillTaxTypes() {
    this.settingsService.getAllTaxes().subscribe((data) => {
      this.taxTypes = data;
      const noTax = this.taxTypes.find(value => value.taxType.toLowerCase() === 'no tax');
      if (noTax) {
        this.serviceForm.get('taxTypeId').setValue(noTax.id);
        this.service.taxTypeId = noTax.id;
        this.taxTypeId = noTax.id;
      }
      if (this.service.taxTypeId !== undefined) {
        this.handleTaxTypeChange(this.taxTypeId);
      }
    });
  }

  createService() {
    const el = document.getElementById('heading');
    const serviceModel: ServiceModel = this.serviceForm.value;
    const locations: ServiceLocationModel[] = [];
    const speciality: ServiceSpecialityModel[] = [];
    const practitioner: ServicePractitionerModel[] = [];
    const relatedProduct: ServiceRelatedProductModel[] = [];
    // const treatmentNotesTemplate: ServiceTreatmentNotesTemplateModel[] = [];
    const concession: ServiceConcessionModel[] = [];

    if (!this.serviceForm.invalid) {
      this.blockUI.start();
      this.submitting = true;

      if (this.image.selectedFile) {
        serviceModel.serviceLogo = this.image.selectedFile.src
          .replace('data:', '')
          .replace(/^.+,/, '');
      }

      if (serviceModel.locationName !== '') {
        serviceModel.locationName.forEach((l) => {
          const model = new ServiceLocationModel();
          model.locationId = l.id;
          model.serviceId = this.itemid;
          locations.push(model);
        });
      }

      if (serviceModel.specialityName !== '') {
        serviceModel.specialityName.forEach((s) => {
          const model = new ServiceSpecialityModel();
          model.locationId = s.locationId;
          model.specialityId = s.id;
          model.serviceId = this.itemid;
          speciality.push(model);
        });
      }

      if (serviceModel.practitionerName !== '') {
        serviceModel.practitionerName.forEach((p) => {
          const model = new ServicePractitionerModel();
          model.locationId = p.locationId;
          model.specialityId = p.specialityId;
          model.practitionerId = p.id;
          model.serviceId = this.itemid;
          practitioner.push(model);
        });
      }

      if (serviceModel.productName !== '') {
        serviceModel.productName.forEach((rp) => {
          const model = new ServiceRelatedProductModel();
          model.productId = rp.id;
          model.serviceId = this.itemid;
          relatedProduct.push(model);
        });
      }

      if (this.concessionList.length > 0) {
        this.concessionList.forEach((c) => {
          const model = new ServiceConcessionModel();
          model.concessionId = c.id;
          model.locationId = c.locationId;
          model.concessionAmount = Number(
            (document.getElementById(
              (c.locationId ? c.locationId : '') + c.concessionType
            ) as HTMLInputElement).value
          );
          model.serviceId = this.itemid;
          concession.push(model);
        });
      }

      serviceModel.serviceLocation = locations;
      serviceModel.serviceSpeciality = speciality;
      serviceModel.servicePractitioner = practitioner;
      serviceModel.serviceRelatedProduct = relatedProduct;
      serviceModel.serviceConcession = concession;
      serviceModel.colour = this.selectedColour;
      serviceModel.categoryId = this.serviceCategoryId;
      serviceModel.taxTypeId = serviceModel.taxTypeId;

      if (this.taxOptionHide === true && serviceModel.taxOptionId === 0) {
        this.displayErrorMessage(
          'Error occurred while adding Service, please try again.'
        );
        this.blockUI.stop();
        el.scrollIntoView();
        this.submitting = false;
        return;
      }

      if (this.addItem) {
        this.offeringsService.createService(serviceModel).subscribe(
          () => {

            this.dashboard.isServiceAdded = true;

            this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u=>{
              this.submitting = false;
              this.displaySuccessMessage('Service added successfully.');
              this.cancel();
              el.scrollIntoView();
              this.blockUI.stop();
            },
            error=>{
              this.displayErrorMessage(
                'Error occurred while adding Service, please try again.'
              );
              this.submitting = false;
              el.scrollIntoView();
              this.blockUI.stop();
            });
          },
          () => {
            this.displayErrorMessage(
              'Error occurred while adding Service, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      } else {
        serviceModel.id = this.itemid;
        this.offeringsService.updateService(serviceModel).subscribe(
          () => {
            this.submitting = false;
            this.displaySuccessMessage('Service updated successfully.');
            this.cancel();
            el.scrollIntoView();
            this.blockUI.stop();
          },
          () => {
            this.displayErrorMessage(
              'Error occurred while updating Service, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      }
    }
  }

  onLocationChange(locationData) {
    if (locationData.length === 0) {
      this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
      this.specialityEnable = true;
      this.practitionerEnable = true;
      this.specialityData = [];
      this.practitionerData = [];
      this.concessionList = [];
      this.serviceForm.get('specialityName').patchValue('');
      this.serviceForm.get('practitionerName').patchValue('');
    } else {
      this.removeSpeciality(locationData);
      this.populateSpeciality(locationData);
      this.populateConcession(locationData);
    }
  }

  removeSpeciality(locationData) {
    this.specialityDataTemp = [];
    const specialityNameData = this.serviceForm.get('specialityName').value;
    if (specialityNameData.length > 0) {
      locationData.forEach((e) => {
        const speciality = specialityNameData.filter(
          (x) => x.locationName === e.locationName
        );

        speciality.forEach((element) => {
          const sData = {
            id: element.id,
            idAndLocationId: element.idAndLocationId,
            specialityName: element.specialityName,
            locationId: element.locationId,
            locationName: element.locationName,
          };

          this.specialityDataTemp.push(sData);
        });
      });
      this.serviceForm
        .get('specialityName')
        .patchValue(this.specialityDataTemp);
    }
  }

  removePractitioner(specialityData) {
    this.practitionerDataTemp = [];
    const practitionerNameData = this.serviceForm.get('practitionerName').value;
    if (practitionerNameData.length > 0) {
      specialityData.forEach((e) => {
        const practitioner = practitionerNameData.filter(
          (x) => x.locationId === e.locationId && x.specialityId === e.id
        );

        practitioner.forEach((element) => {
          const sData = {
            id: element.id,
            idAndSpecialityIdAndLocationId:
            element.idAndSpecialityIdAndLocationId,
            firstName: element.firstName,
            specialityId: element.specialityId,
            specialityName: element.specialityName,
            locationId: element.locationId,
            locationName: element.locationName,
          };

          this.practitionerDataTemp.push(sData);
        });
      });
      this.serviceForm
        .get('practitionerName')
        .patchValue(this.practitionerDataTemp);

      if (this.practitionerDataTemp.length > 0) {
        this.practitionerEnable = false;
      } else {
        this.practitionerEnable = true;
        this.practitionerData = [];
        this.groupedPractitionerData = groupBy(
          [],
          [{field: 'specialityName'}]
        );
        this.serviceForm.get('practitionerName').patchValue('');
      }
    }
  }

  populateSpeciality(locationData) {
    if (!locationData) {
      return;
    }
    this.specialityData = [];
    locationData.forEach((e) => {
      console.log(e);
      this.specialityEnable = false;
      this.specialityAllList.forEach((s) => {
        let sLocation;
        if (s.isAllowAllLocation) {
          sLocation = s;
        } else {
          sLocation = s.specialityLocation.find((x) => x.locationId === e.id || x.isAllowAllLocation);
        }
        if (sLocation) {
          const specialityDataCheck = this.specialityData.find((x) => x.locationId === e.id && x.id === s.id);
          const sData = {
            id: '',
            idAndLocationId: '',
            specialityName: '',
            locationId: '',
            locationName: '',
          };
          sData.id = s.id;
          sData.idAndLocationId = s.id + e.id;
          sData.specialityName =
            s.specialityName + ' (' + (sLocation.isAllowAllLocation ? 'All Location' : sLocation.locationName) + ')';
          sData.locationId = e.id;
          sData.locationName = e.locationName;
          if (specialityDataCheck === undefined) {
            this.specialityData.push(sData);
            this.groupedSpecialityData = groupBy(this.specialityData, [
              {field: 'locationName'},
            ]);
          }
        }
      });

      //this.removePractitioner(this.specialityData);
    });

    console.log(this.groupedSpecialityData);
    if (this.specialityData.length > 0) {
      this.specialityEnable = false;
    } else {
      this.specialityEnable = true;
    }
  }

  populateConcession(locationData) {
    if (!locationData) {
      return;
    }
    this.concessionList = [];
    locationData.forEach((e) => {
      this.concessionAllList.forEach((s) => {
        let cLocation;
        if (s.isAllowAllLocation) {
          cLocation = [s];
        } else {
          cLocation = s.concessionLocation ? s.concessionLocation.filter((x) => x.locationId === e.id || x.isAllowAllLocation) : [];
        }

        if (cLocation && cLocation.length > 0) {
          cLocation.forEach((cl) => {
            let cAmount = 0;
            if (this.concessions !== undefined && this.concessions.length > 0) {
              const cList = this.concessions.find(
                (x) => x.locationId === cl.locationId && x.id === s.concessionId
              );
              if (cList !== undefined) {
                cAmount = cList.concessionAmount;
              }
            }
            const sData = {
              id: s.id,
              locationId: cl.locationId,
              concessionAmount: cAmount,
              concessionType: s.concessionType,
              serviceId: '',
              locationName: cl.isAllowAllLocation ? "All" : cl.locationName,
            };

            this.concessionList.push(sData);
          });
          this.concessionsEnable = true;
        }
      });
    });
    this.concessionList = uniqueBy(this.concessionList, 'id');
    if (this.concessionList === undefined) {
      this.concessionsEnable = false;
    }
  }

  onSpecialityChange(specialityData) {
    if (specialityData.length === 0) {
      this.practitionerEnable = true;
      this.practitionerData = [];
      this.groupedPractitionerData = groupBy([], [{field: 'specialityName'}]);
      this.serviceForm.get('practitionerName').patchValue('');
    } else {
      this.removePractitioner(specialityData);
      this.populatePractitioner(specialityData);
    }
  }

  populatePractitioner(specialityData) {
    specialityData.forEach((e) => {
      const pList = this.practitionerAllList.filter((x) => x.specilities.indexOf(e.id) > -1 && x.locations.indexOf(e.locationId) > -1);
      if (pList !== undefined && pList.length > 0) {
        pList.forEach((p) => {
          const lName = this.locationList.find((x) => p.locations.indexOf(x.id) > -1);
          const sName = this.specialityAllList.find(
            (x) => p.specilities.indexOf(x.id) > -1
          );

          const d = this.practitionerData.filter((x) => x.id + x.specialityId + x.locationId === p.id + p.specialityId + p.locationId);

          if (d.length === 0) {
            const pData = {
              id: p.id,
              idAndSpecialityIdAndLocationId:
                p.id + p.specialityId + p.locationId,
              firstName: p.firstName + ' (' + lName.locationName + ')',
              specialityId: p.specialityId,
              specialityName: sName.specialityName,
              locationId: p.locationId,
              locationName: lName.locationName,
            };

            this.practitionerData.push(pData);
          }
        });
      }
    });
    if (this.practitionerData.length > 0) {
      this.groupedPractitionerData = groupBy(this.practitionerData, [
        {field: 'specialityName'},
      ]);
      this.practitionerEnable = false;
    } else {
      this.practitionerEnable = true;
    }
  }

  handleTaxTypeChange(e) {
    const tt = this.taxTypes.find((x) => x.id === e);
    if (tt.taxType.toLowerCase() === 'no tax') {
      this.taxOptionHide = false;
      this.serviceForm.get('taxOptionId').patchValue(0);
    } else {
      this.taxOptionHide = true;
    }
  }

  appIdServiceCategoryHandler($event) {
    this.serviceCategoryId = $event;
    this.serviceForm.get('categoryId').setValue($event);
  }
}
