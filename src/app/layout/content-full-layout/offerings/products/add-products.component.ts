import {Component, EventEmitter, OnInit, Output, ViewChild,} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {OfferingsService} from '../../../../services/app.offerings.service';
import {SettingsService} from 'src/app/services/app.settings.service';
import {ApplicationDataService} from 'src/app/services/app.applicationdata.service';
import {AppState} from 'src/app/app.state';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute} from '@angular/router';
import {ImageSnippet} from '../../../../models/app.misc';
import {ProductLocationModel, ProductModel,} from '../../../../models/app.product.model';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {ContactService} from '../../../../services/app.contact.service';
import {ContactModel} from 'src/app/models/app.contact.model';
import { DashboardModel } from 'src/app/models/app.dashboard.model';
import { DashboardService } from 'src/app/services/app.dashboard.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.css'],
})
export class AddProductsComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.offerings_products
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(ImageUploadComponent, {static: true})
  image: ImageUploadComponent;
  @Output() appId: EventEmitter<number> = new EventEmitter<number>();

  submitting = false;
  public contact: EventEmitter<string> = new EventEmitter();
private dashboard:DashboardModel;
  productForm: FormGroup = new FormGroup({
    productLogo: new FormControl(''),
    productCode: new FormControl('', Validators.required),
    medicareCode: new FormControl(''),
    dvaCode: new FormControl(''),
    productName: new FormControl('', Validators.required),
    description: new FormControl(''),
    supplierId: new FormControl('00000000-0000-0000-0000-000000000000'),
    serialNumber: new FormControl(''),
    unitOfMeasurement: new FormControl(''),
    currentStock: new FormControl('', Validators.required),
    minimumStock: new FormControl('', Validators.required),
    costPrice: new FormControl('', Validators.required),
    salePrice: new FormControl('', Validators.required),
    taxTypeId: new FormControl(0),
    taxOptionId: new FormControl(0),
    locationName: new FormControl('', Validators.required),
    isAllowAllLocation: new FormControl(false),
    status: new FormControl(true),
    productCategory: new FormControl('', Validators.required),
  });

  locationList: any[];
  taxTypes: any[];
  apperance = 'outline';
  addProduct = true;
  taxOptionHide = true;
  taxOptionSelected: number;
  addItem: boolean;
  itemid: any;
  unitOfMeasurement: number;
  unitOfMeasurementParentId: number;
  suppliers: ContactModel[];
  product: ProductModel;
  taxTypeId: string;
  productCategoryParentId = ApplicationDataEnum.ProductCategory;

  constructor(
    private businessService: BusinessService,
    protected appState: AppState,
    public location: Location,
    private _route: ActivatedRoute,
    public offeringsService: OfferingsService,
    public contactService: ContactService,
    public settingsService: SettingsService,
    public applicationDataService: ApplicationDataService,
    public dashboardService:DashboardService
  ) {
    super(location);
    this.fillTaxTypes();
    this.unitOfMeasurementParentId = ApplicationDataEnum.UnitOfMeasurement;
  }

  ngOnInit() {
    this.product = new ProductModel();

    this.productForm.get('medicareCode').valueChanges.subscribe(value => {
      if (!parseInt(value) && parseInt(value) == 0) {
        this.productForm.get('medicareCode').setErrors({notZero: true});
      }else{
        this.productForm.get('medicareCode').setErrors(null);
      }
    });

    this.productForm.get('dvaCode').valueChanges.subscribe(value => {
      if (!parseInt(value) && parseInt(value) == 0) {
        this.productForm.get('dvaCode').setErrors({notZero: true});
      }else{
        this.productForm.get('dvaCode').setErrors(null);
      }
    });

    this.businessService
      .getLocationsByBusiness(this.appState.userProfile.parentBusinessId)
      .subscribe((locations) => {
        this.locationList = locations;
        this.locationPopulate();
      });

    this.getSupplierContacts('');
    this.dashboardService.getDashboardCheckListView().subscribe(d => {
      this.dashboard = d;
    });


    this._route.params.subscribe((params) => {
      if (params.productId) {
        this.blockUI.start();
        this.addItem = false;
        this.itemid = params.productId;

        this.offeringsService
          .getProductById(params.productId)
          .subscribe((data: ProductModel) => {
            this.product = data;
            this.productForm.patchValue(data);
            if (data.productLogo) {
              this.image.selectedFile = new ImageSnippet(
                `data:image/jpeg;base64,${data.productLogo}`,
                null
              );
            }
            this.taxOptionSelected = data.taxOptionId;
            this.taxTypeId = data.taxTypeId;
            if (this.taxTypes !== undefined) {
              this.handleTaxTypeChange(this.taxTypeId);
            }

            if (this.locationList !== undefined) {
              const locationName = [];
              data.productLocation.forEach((pl) => {
                locationName.push(
                  this.locationList.find((l) => l.id === pl.locationId)
                );
              });
              this.productForm.get('locationName').patchValue(locationName);
            }
            this.unitOfMeasurement = data.unitOfMeasurement;
            this.blockUI.stop();
          });
      }
    });
  }

  locationPopulate() {
    const locationName = [];
    if (this.product.productLocation !== undefined) {
      this.product.productLocation.forEach((pl) => {
        locationName.push(
          this.locationList.find((l) => l.id === pl.locationId)
        );
      });
      this.productForm.get('locationName').patchValue(locationName);
    }
  }

  private getSupplierContacts(supplierId) {
    this.applicationDataService
      .GetApplicationDataByCategoryId(
        ApplicationDataEnum.ContactCategory,
        this.appState.selectedUserLocationId
      )
      .subscribe((data) => {
        const catId = data.find((x) => x.categoryName === 'Supplier');
        this.contactService.getAllContact().subscribe((contactData) => {
          this.suppliers = contactData.filter(
            (x) => x.contactType === 3 &&
              x.categoryId === 'Supplier'
            // x.categoryId === catId.id
          );

          if (supplierId !== '') {
            this.productForm.get('supplierId').patchValue(supplierId);
          }
        });
      });
  }

  appIdUnitOfMeasurementChangedHandler($event) {
    this.unitOfMeasurement = $event;
  }

  fillTaxTypes() {
    this.settingsService.getAllTaxes().subscribe((data) => {
      this.taxTypes = data;
      const noTax = this.taxTypes.find(value => value.taxType.toLowerCase() === 'no tax');
      if (noTax) {
        this.productForm.get('taxTypeId').setValue(noTax.id);
        this.product.taxTypeId = noTax.id;
        this.taxTypeId = noTax.id;
      }
      if (this.product && this.product.taxTypeId !== undefined) {
        this.handleTaxTypeChange(this.taxTypeId);
      }
    });
  }

  allLocationChange($event) {
    if ($event === true) {
      const locationName = [];
      if (this.locationList !== undefined) {
        this.locationList.forEach((l) => {
          locationName.push(l);
        });
        this.productForm.get('locationName').patchValue(locationName);
      }
    } else {
      this.productForm.get('locationName').patchValue('');
    }
  }

  createProduct() {
    const el = document.getElementById('heading');
    const productUpdate: ProductModel = this.productForm.value;

    if (!this.productForm.invalid) {
      this.blockUI.start();
      this.submitting = true;

      if (this.image.selectedFile) {
        productUpdate.productLogo = this.image.selectedFile.src
          .replace('data:', '')
          .replace(/^.+,/, '');
      }

      const locations: ProductLocationModel[] = [];
      productUpdate.locationName.forEach((l) => {
        const model = new ProductLocationModel();
        model.locationId = l.id;
        model.productId = this.itemid;
        locations.push(model);
      });

      productUpdate.productLocation = locations;
      productUpdate.supplierId = productUpdate.supplierId;
      productUpdate.taxTypeId = productUpdate.taxTypeId;
      productUpdate.costPrice = Number(productUpdate.costPrice);
      productUpdate.currentStock = Number(productUpdate.currentStock);
      productUpdate.minimumStock = Number(productUpdate.minimumStock);
      productUpdate.salePrice = Number(productUpdate.salePrice);
      productUpdate.unitOfMeasurement = this.unitOfMeasurement;

      if (this.taxOptionHide === true && productUpdate.taxOptionId === 0) {
        this.displayErrorMessage(
          'Error occurred while adding product, please try again.'
        );
        this.blockUI.stop();
        el.scrollIntoView();
        this.submitting = false;
        return;
      }

      if (this.addItem) {
        this.offeringsService.createProduct(productUpdate).subscribe(
          (data) => {
          this.dashboard.isProductAdded = true;
          this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u=>{
            this.submitting = false;
            this.displaySuccessMessage('Product added successfully.');
            this.cancel();
            el.scrollIntoView();
            this.blockUI.stop();
          },error=>{
            this.displayErrorMessage(
              'Error occurred while adding Product, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          });


          },
          (error) => {
            this.displayErrorMessage(
              'Error occurred while adding Product, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      } else {
        productUpdate.id = this.itemid;
        this.offeringsService.updateProduct(productUpdate).subscribe(
          (data) => {
            this.submitting = false;
            this.displaySuccessMessage('Product updated successfully.');
            this.cancel();
            el.scrollIntoView();
            this.blockUI.stop();
          },
          (error) => {
            this.displayErrorMessage(
              'Error occurred while updating Product, please try again.'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      }
    }
  }

  openSlider() {
    this.contact.emit('col-xl-12');
    document.getElementById('slider').style.width = '30%';
    document.getElementById('sliderShadow').style.display = 'block';
  }

  closeSlider() {
    document.getElementById('slider').style.width = '0px';
    document.getElementById('sliderShadow').style.display = 'none';
  }

  handleTaxTypeChange(e) {
    const tt = this.taxTypes.find((x) => x.id === e);
    if (tt.taxType.toLowerCase() === 'no tax') {
      this.taxOptionHide = false;
      this.productForm.get('taxOptionId').patchValue(0);
    } else {
      this.taxOptionHide = true;
    }
  }

  parentProductCallBack(e) {
    this.getSupplierContacts(e);
  }
}
