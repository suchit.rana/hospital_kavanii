import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OfferingRoutingModule} from './offering-routing.module';
import {OfferingsComponent} from './offerings.component';
import {AddProductsComponent} from './products/add-products.component';
import {AddClassComponent} from './classes/add-class.component';
import {AddServiceComponent} from './service/add-service.component';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {AppCommonModule} from '../common/app-common.module';
import {MatRadioModule} from '@angular/material';
import {BlockUIModule} from 'ng-block-ui';
import {MultiSelectModule} from '@progress/kendo-angular-dropdowns';
import {ManagingModule} from '../../module/managing.module';
import {AddGeneralModule} from '../contacts/add-general/add-general.module';
import {CharacterLimitModule} from '../../../directive/character-limit/character-limit.module';
import {OnlyNumberModule} from '../../../directive/only-number/only-number.module';
import {TreatmentNotesService} from '../../../services/app.treatmentnotes.services';
import {TextOnlyModule} from '../../../directive/text-only/text-only.module';
import {NoSpecCharModule} from '../../../directive/no-spec-char/no-spec-char.module';
import {AccessDirectiveModule} from '../../../shared/directives/access-directive/access-directive.module';


@NgModule({
  declarations: [
    OfferingsComponent,
    AddProductsComponent,
    AddClassComponent,
    AddServiceComponent
  ],
    imports: [
        CommonModule,
        SharedContentFullLayoutModule,
        ManagingModule,
        AppCommonModule,
        OfferingRoutingModule,
        BlockUIModule.forRoot(),
        MultiSelectModule,
        MatRadioModule,
        AddGeneralModule,
        CharacterLimitModule,
        OnlyNumberModule,
        TextOnlyModule,
        NoSpecCharModule,
        AccessDirectiveModule,
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [TreatmentNotesService]
})
export class OfferingModule {
}
