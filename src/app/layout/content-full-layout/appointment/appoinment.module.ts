import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AppoinmentRoutingModule} from './appoinment-routing.module';
import {AppointmentComponent} from './appointment.component';
import {SchedularComponent} from './schedular-main/schedular/schedular.component';
import {SchedularHeaderComponent} from './schedular-main/schedular/schedular-header/schedular-header.component';
import {PractitionerListComponent} from './schedular-main/schedular/practitioner-list/practitioner-list.component';
import {AppoinmentHeaderComponent} from './appoinment-header/appoinment-header.component';
import {PopupModule} from '@progress/kendo-angular-popup';
import {DxCalendarModule} from 'devextreme-angular/ui/calendar';
import {DxSchedulerModule} from 'devextreme-angular/ui/scheduler';
import {DxPopoverModule} from 'devextreme-angular/ui/popover';
import {SharedModuleModule} from '../../../shared/shared-module.module';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {EmptyPractitionerComponent} from './schedular-main/schedular/empty-practitioner/empty-practitioner.component';
import {AddEditScheduleComponent} from './schedular-main/schedular/add-edit-schedule/add-edit-schedule.component';
import {ReplaceStringPipe} from '../../../pipe/replace-string.pipe';
import {IndicatorsModule} from '@progress/kendo-angular-indicators';
import {BlockUIModule} from 'ng-block-ui';
import {AutoCompleteModule, DropDownListModule} from '@progress/kendo-angular-dropdowns';
import {MatSelectModule} from '@angular/material/select';
import {MatOptionModule} from '@angular/material/core';
import {DatePickerModule, TimePickerModule} from '@progress/kendo-angular-dateinputs';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {MatDividerModule} from '@angular/material/divider';
import {SchedularMainComponent} from './schedular-main/schedular-main.component';
import {DxDateBoxModule} from 'devextreme-angular/ui/date-box';
import {NgxRruleModule} from '../../../../ngx-rrule/src/lib/ngx-rrule.module';
import {AppCommonModule} from '../common/app-common.module';
import {TooltipModule} from '@progress/kendo-angular-tooltip';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {AppTimePickerModule} from '../common/time-picker/app-time-picker.module';
import {AppTimePipeModule} from '../../../pipe/module/app-time-pipe.module';
import {SchedulerManageService} from './scheduler-manage.service';
import {AppSafeHtmlPipeModule} from '../../../pipe/module/app-safe-html-pipe.module';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {SchedulerSettingsPopupComponent} from './schedular-main/schedular/popup-component/scheduler-settings-popup/scheduler-settings-popup.component';
import {MatDialogModule} from '@angular/material/dialog';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {AddEditAppointmentComponent} from './schedular-main/schedular/add-edit-appointment/add-edit-appointment.component';
import {PatientSearchComponent} from './schedular-main/schedular/shared/patient-search/patient-search.component';
import {AppCalenderPopupModule} from '../common/calender-popup/app-calender-popup.module';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {HeadernavComponent} from './schedular-main/schedular/tooltip-Templat/headernav/headernav.component';
import {FooterNavComponent} from './schedular-main/schedular/tooltip-Templat/footer-nav/footer-nav.component';
import {ListdataComponent} from './schedular-main/schedular/tooltip-Templat/listdata/listdata.component';
import {AppointmentdetailsComponent} from './schedular-main/schedular/tooltip-Templat/appointmentdetails/appointmentdetails.component';
import {VideosessionComponent} from './schedular-main/schedular/tooltip-Templat/videosession/videosession.component';
import {ActionAppointmentComponent} from './schedular-main/schedular/tooltip-Templat/action-appointment/action-appointment.component';
import {MoreDetailsComponent} from './schedular-main/schedular/tooltip-Templat/more-details/more-details.component';
import {PatientHistoryComponent} from './schedular-main/schedular/tooltip-Templat/patient-history/patient-history.component';
import {AppointmentTooltipComponent} from './schedular-main/schedular/appointment-tooltip/appointment-tooltip.component';
import {AppointmentHandlerService} from './schedular-main/schedular/service/appointment-handler.service';
import {ApptDropDownAddNewModule} from './schedular-main/schedular/shared/appt-drop-down-add-new/appt-drop-down-add-new.module';
import {AppointmentPopupModule} from '../patients/patient-appointments/appointment-popup/appointment-popup.module';
import {AddEditAppointmentUtils} from './schedular-main/schedular/shared/utils/AddEditAppointmentUtils';
import { PatientAlertDisplayComponent } from './schedular-main/schedular/popup-component/patient-alert-display/patient-alert-display.component';
import { WaitingRoomComponent } from './schedular-main/schedular/waiting-room/waiting-room.component';
import {BodyModule, SharedModule} from '@progress/kendo-angular-grid';
import { MatBottomSheetModule } from '@angular/material';
import {AppDateDiffPipeModule} from '../../../pipe/module/app-date-diff-pipe.module';
import {PreviewLetterModule} from '../patients/patient-letters/preview-letter/preview-letter.module';
import { WaitListComponent } from './schedular-main/schedular/component/wait-list/wait-list.component';
import { AddEditWaitListComponent } from './schedular-main/schedular/component/wait-list/add-edit-wait-list/add-edit-wait-list.component';
import {AppKendoMultiSelectModule} from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import {NgbButtonsModule} from '@ng-bootstrap/ng-bootstrap';
import {ExportWaitListComponent} from './schedular-main/schedular/component/wait-list/export-waitlist/export-wait-list.component';
import { FilterPractitionerComponent } from './schedular-main/schedular/component/wait-list/filter-practitioner/filter-practitioner.component';
import {WaitlistManageService} from './schedular-main/schedular/service/waitlist-manage.service';
import {RoomListComponent} from './schedular-main/schedular/room-list/room-list.component';
import { PractitionerAvailabilityComponent } from './schedular-main/schedular/popup-component/practitioner-availability/practitioner-availability.component';
import {CharacterLimitModule} from '../../../directive/character-limit/character-limit.module';
import {AccessDirectiveModule} from '../../../shared/directives/access-directive/access-directive.module';
import { RecurrenceComponent } from './schedular-main/schedular/add-edit-appointment/recurrence/recurrence.component';
import {AppDatePickerModule} from '../common/app-date-picker/app-date-picker.module';


@NgModule({
  declarations: [
    AppointmentComponent,
    AppoinmentHeaderComponent,
    SchedularMainComponent,
    SchedularComponent,
    SchedularHeaderComponent,
    PractitionerListComponent,
    EmptyPractitionerComponent,
    AddEditScheduleComponent,
    ReplaceStringPipe,
    SchedulerSettingsPopupComponent,
    AddEditAppointmentComponent,
    PatientSearchComponent,
    HeadernavComponent,
    FooterNavComponent,
    ListdataComponent,
    AppointmentdetailsComponent,
    VideosessionComponent,
    ActionAppointmentComponent,
    MoreDetailsComponent,
    PatientHistoryComponent,
    AppointmentTooltipComponent,
    PatientAlertDisplayComponent,
    WaitingRoomComponent,
    WaitListComponent,
    AddEditWaitListComponent,
    ExportWaitListComponent,
    FilterPractitionerComponent,
    RoomListComponent,
    PractitionerAvailabilityComponent,
    RecurrenceComponent
  ],
  exports: [],
    imports: [
        CommonModule,
        SharedModuleModule,
        SharedContentFullLayoutModule,
        AppTimePickerModule,
        AppCommonModule,
        AppoinmentRoutingModule,
        InputsModule,
        LayoutModule,
        PopupModule,
        IndicatorsModule,
        DxSchedulerModule,
        DxPopoverModule,
        DxCalendarModule,
        DropDownListModule,
        MatSelectModule,
        MatOptionModule,
        MatDividerModule,
        MatDialogModule,
        DatePickerModule,
        TimePickerModule,
        BlockUIModule.forRoot(),
        DxDateBoxModule,
        NgxRruleModule,
        TooltipModule,
        DialogModule,
        AppTimePipeModule,
        AppSafeHtmlPipeModule,
        DragDropModule,
        AutoCompleteModule,
        AppCalenderPopupModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatBottomSheetModule,
        ApptDropDownAddNewModule,
        AppointmentPopupModule,
        AppDateDiffPipeModule,
        PreviewLetterModule,
        AppKendoMultiSelectModule,
        NgbButtonsModule,
        CharacterLimitModule,
        AccessDirectiveModule,
        AppDatePickerModule,
        DxDateBoxModule
    ],
  providers: [SchedulerManageService, AddEditAppointmentUtils, WaitlistManageService],
  entryComponents: [
      SchedulerSettingsPopupComponent,
      PatientAlertDisplayComponent,
      AddEditWaitListComponent,
      FilterPractitionerComponent,
      ExportWaitListComponent,
      PractitionerAvailabilityComponent,
      RecurrenceComponent
  ]
})
export class AppoinmentModule {
}
