import { Component, OnInit, ViewChild } from "@angular/core";
import { AppState } from "../../../app.state";
import "@progress/kendo-date-math/tz/all";
import { AppointmentEditService } from "src/app/services/app.appointment.editservice";
import { MenuModel } from "../../../models/app.menu.model";
import { ScheduleEditService } from "../../../services/app.schedule.editservice";
import {SchedulerManageService} from './scheduler-manage.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-appointment",
  templateUrl: "./appointment.component.html",
  styleUrls: ["./appointment.component.css"],
  providers: [SchedulerManageService]
})
export class AppointmentComponent implements OnInit {
  // @ViewChild("appointments", { static: false })
  // appointments: CreateAppointmentComponent;
  // appointments: CreateAppointmentComponent;
  //
  fromDashboard:boolean;
  constructor(
    public appState: AppState,
    private route:ActivatedRoute
    // public editService: AppointmentEditService,
    // public scheduleEditService: ScheduleEditService
  ) {}

  // showAppointment: boolean = true;
  // showSchedule: boolean = false;
  // showRoom: boolean = false;
  // showPractitionerToday: boolean = false;
  //
  // public showWaitList: boolean = false;

  ngOnInit() {
    this.appState.sideNavState.next(false);
    this.route.queryParams.subscribe(params=>{
      //checking whether  request came from dashboard page
      this.fromDashboard = params['dashboard'];
    });
  }

  // createClicked(event: boolean) {
  //   //this.showSchedule = false;
  //   //this.showSchedule = event;
  // }
  //
  // menuChanged(event: MenuModel) {
  //   switch (event.name) {
  //     case "Schedule":
  //       this.showSchedule = event.show;
  //       this.showAppointment = !this.showSchedule;
  //       this.showRoom = false;
  //       this.showWaitList = false;
  //       this.showPractitionerToday = false;
  //       break;
  //     case "Rooms":
  //       this.showRoom = event.show;
  //       this.showAppointment = !this.showRoom;
  //       this.showSchedule = false;
  //       this.showWaitList = false;
  //       this.showPractitionerToday = false;
  //       break;
  //     case "Practitioner Today":
  //       this.showPractitionerToday = event.show;
  //       this.showAppointment = true;
  //       this.showRoom = false;
  //       this.showWaitList = false;
  //       this.showPractitionerToday = false;
  //       break;
  //     case "Wait List":
  //       this.showWaitList = event.show;
  //       this.showAppointment = true;
  //       this.showSchedule = false;
  //       this.showRoom = false;
  //       this.showPractitionerToday = false;
  //       break;
  //   }
  // }
}
