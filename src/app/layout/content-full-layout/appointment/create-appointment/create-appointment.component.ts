// import {
//   Component,
//   OnInit,
//   ChangeDetectorRef,
//   Input,
//   OnChanges,
//   SimpleChanges,
//   ViewChild,
//   OnDestroy,
//   ViewChildren,
//   QueryList,
// } from "@angular/core";
// import { AppState } from "../../../../app.state";
// import {
//   DateChangeEvent,
//   SlotClickEvent,
//   EventClickEvent,
//   CrudOperation,
//   EditMode,
//   RemoveEvent,
//   SlotClassArgs,
// } from "@progress/kendo-angular-scheduler";
// import "@progress/kendo-date-math/tz/all";
// import { StaffService } from "../../../../services/app.staff.service";
// import {
//   PractitionerSpecialityModel,
//   GroupSpecialityPractitonerModel,
// } from "../../../../models/app.staff.model";
// import {
//   ScheduleEvent,
//   WaitListEvent,
// } from "src/app/models/app.appointment.model";
// import { BusinessService } from "src/app/services/app.business.service";
// import { AppointmentEditService } from "src/app/services/app.appointment.editservice";
// import { filter, map } from "rxjs/operators";
// import { createPopper } from "@popperjs/core";
// import { RoomEditService } from "src/app/services/app.room.editservice";
// import { TreatmentRoomModel } from "src/app/models/app.settings.model";
// import { SettingsService } from "src/app/services/app.settings.service";
// import { AddRoomComponent } from "../room/add-room/add-room.component";
// import { ScheduleEditService } from "src/app/services/app.schedule.editservice";
// import { AddScheduleComponent } from "../schedule/add-schedule/add-schedule.component";
// import { Subscription } from "rxjs";
// import {
//   SchedulerManageService,
//   Data,
// } from "src/app/services/app.appointment.service";
// import { WaitListComponent } from "../wait-list/wait-list.component";
// import { DatePipe } from "@angular/common";
// import DataSource from "devextreme/data/data_source";
// import { DxSchedulerComponent } from "devextreme-angular";
// import { MenuModel } from "src/app/models/app.menu.model";
//
// @Component({
//   selector: "app-create-appointment",
//   templateUrl: "./create-appointment.component.html",
//   styleUrls: [
//     "./create-appointment.component.css",
//     "../appointment.component.css",
//   ],
// })
// export class CreateAppointmentComponent
//   implements OnInit, OnChanges, OnDestroy {
//   @ViewChildren(WaitListComponent) waitlistItems: QueryList<WaitListComponent>;
//   @ViewChild(AddRoomComponent, { static: false }) addRoomComp: AddRoomComponent;
//   @ViewChild(DxSchedulerComponent, { static: false })
//   scheduler: DxSchedulerComponent;
//   @ViewChild(AddScheduleComponent, { static: false })
//   addScheduleComp: AddScheduleComponent;
//
//   @Input() showWaitList: boolean;
//   @Input() showAppointment: boolean;
//   @Input() showRoom: boolean;
//   @Input() showSchedule: boolean;
//
//   dataSource: any;
//   currentDate: Date = new Date(2021, 7, 2, 11, 30);
//   startDate: Date = new Date(2021, 7, 2, 11, 30);
//   endDate: Date = new Date(2021, 7, 2, 11, 30);
//   groupByDate: boolean = false;
//   resourcesDataSource: Array<PractitionerSpecialityModel> = [];
//   currentView: string = "day";
//   classObject: any;
//   tempData: Data[] = [];
//   tempEmployees: Array<PractitionerSpecialityModel> = [];
//   timeZone: string;
//   practitionerId: string;
//
//   appointmentLinks: MenuModel[] = [
//     {
//       name: "Practitioner Today",
//       link: "/appointment/today",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Schedule",
//       link: "/appointment/schedule",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Wait List",
//       link: "/appointment/waitlist",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Rooms",
//       link: "/appointment/room",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//   ];
//   IsShow: boolean;
//
//   constructor(
//     public appState: AppState,
//     private staffService: StaffService,
//     private businessService: BusinessService,
//     public appointmentEditService: AppointmentEditService,
//     public roomEditService: RoomEditService,
//     public scheduleEditService: ScheduleEditService,
//     private settingsService: SettingsService,
//     public appointmentService: SchedulerManageService,
//     private datepipe: DatePipe,
//     private cd: ChangeDetectorRef
//   ) {}
//
//   public editService:
//     | AppointmentEditService
//     | ScheduleEditService
//     | RoomEditService;
//
//   public numberOfPractitioners: number = 0;
//   public alreadyAdded: boolean = false;
//
//   resourceData: any[] = [];
//   public allPractitioners: Array<PractitionerSpecialityModel>;
//   public practitioners: Array<PractitionerSpecialityModel>;
//   public selectedPractitioners: Array<PractitionerSpecialityModel>;
//   public specialityPractitioners: GroupSpecialityPractitonerModel[];
//   public rooms: Array<TreatmentRoomModel>;
//
//   panelExpandSize: string = "300px";
//   panelCollaspeSize: string = "80px";
//   panelSize: string = this.panelExpandSize;
//   panelExpand: boolean = true;
//
//   resource: boolean = false;
//
//   sliderTitle: string = "Add Break";
//
//   isAddBreak: boolean = false;
//   isAddClass: boolean = false;
//   isAddAppointment: boolean = false;
//   isAddWaitList: boolean = false;
//
//   isViewWaitList: boolean = false;
//
//   slotDuration: number = 60;
//   startTime: Date;
//   endTime: Date;
//
//   _startTime: number = 7;
//   _endTime: number = 19;
//
//   defaultVisible: boolean;
//
//   locationStart: Date;
//   locationEnd: Date;
//
//   public editedEvent: any;
//   public waitListEvent: WaitListEvent;
//   public editMode: EditMode;
//   public isNew: boolean;
//   public selectedDate: Date;
//   public events: ScheduleEvent[];
//   public group: any = {
//     resources: ["Specialities"],
//     orientation: "horizontal",
//   };
//
//   //specialityPractitionerId
//   //practitionerId
//
//   public resources: any[] = [
//     {
//       name: "Specialities",
//       data: [],
//       field: "specialityPractitionerId",
//       valueField: "specialityPractitionerId",
//       textField: "title",
//       colorField: "color",
//       multiple: false,
//     },
//     {
//       name: "Rooms",
//       data: [],
//       field: "roomId",
//       valueField: "id",
//       textField: "text",
//       colorField: "color",
//       multiple: false,
//     },
//   ];
//
//   locationSubscribe: Subscription;
//   scheduleSubscribe: Subscription;
//   editServiceSubscribe: Subscription;
//
//   currentEvents: any[];
//
//   ngOnInit() {
//     this.appState.sideNavState.next(false);
//
//     this.editService = this.appointmentEditService;
//     this.loadLocationTime();
//     this.loadEvents();
//     this.registerSubscribes();
//     this.timeZone = "Australia/Sydney";
//     this.groupByDate = false;
//   }
//
//   onAppointmentDbl(e) {
//     e.cancel = true;
//     this.openAppointmentSlider();
//     console.log("onAppointmentDblClick", e);
//   }
//
//   onAppointment(e) {
//     e.cancel = true;
//     console.log("onAppointmentClick", e);
//   }
//
//   onCell(e) {
//     e.cancel = true;
//     console.log("onCellClick", e);
//   }
//
//   desktopFocusRowChanged($event) {
//     console.log($event);
//   }
//
//
//   openfilter() {
//     if (this.IsShow == false) {
//       this.IsShow = true;
//     } else {
//       this.IsShow = false;
//     }
//   }
//
//   registerSubscribes() {
//     this.locationSubscribe = this.appState.selectedUserLocationIdState.subscribe(
//       (l) => {
//         this.loadEvents();
//         this.loadLocationTime();
//         this.refreshWaitLists();
//         this.loadStaffs(true);
//       }
//     );
//
//     this.scheduleSubscribe = this.scheduleEditService.events.subscribe(
//       (data) => {
//         this.scheduleEvents = data;
//         this.onOrientationChange("horizontal");
//         this.cd.detectChanges();
//       }
//     );
//
//     this.editServiceSubscribe = this.editService.events.subscribe((e) => {
//       this.alreadyAdded = e && e.length > 0;
//       this.currentEvents = e;
//     });
//   }
//
//   ngOnDestroy() {
//     this.locationSubscribe.unsubscribe();
//     this.scheduleSubscribe.unsubscribe();
//     this.editServiceSubscribe.unsubscribe();
//   }
//
//   ngOnChanges(changes: SimpleChanges) {
//     //this.showAppointment = true;
//     this.hideAllPopover();
//     if (changes.showAppointment || changes.showRoom || changes.showSchedule) {
//       if (changes.showRoom && changes.showRoom.currentValue === true) {
//         this.editService = this.roomEditService;
//         this.loadTreatmentRooms();
//         this.group.resources = ["Rooms"];
//         this.updateRoomResources();
//         this.onOrientationChange("horizontal");
//       }
//
//       if (
//         changes.showAppointment &&
//         changes.showAppointment.currentValue === true
//       ) {
//         this.editService = this.appointmentEditService;
//         // this.resourceData = this.practitioners;
//         this.refreshResourceData();
//         this.group.resources = ["Specialities"];
//         this.onOrientationChange("horizontal");
//         this.updateResources();
//       }
//
//       if (changes.showSchedule && changes.showSchedule.currentValue === true) {
//         this.editService = this.scheduleEditService;
//         // this.resourceData = this.practitioners;
//         this.group.resources = ["Specialities"];
//         //this.openAppointmentSlider();
//         //this.refreshResourceData();
//         this.onOrientationChange("horizontal");
//         //this.updateResources();
//       }
//
//       this.editService.read();
//     }
//   }
//
//   loadTreatmentRooms() {
//     this.settingsService
//       .getAllTreatmentRooms()
//       .pipe(
//         map((items) =>
//           items.filter(
//             (r) =>
//               r.isAllowAllLocation ||
//               r.treatmentLocation.find(
//                 (t) => t.locationId == this.appState.selectedUserLocationId
//               ) != null
//           )
//         )
//       )
//       .subscribe((data) => {
//         this.rooms = data;
//         this.resourceData = data;
//         if (data.length > 0) {
//           this.onRoomClick(this.rooms[0]);
//         }
//       });
//   }
//
//   public onRoomClick(c) {
//     c.selected = !c.selected;
//     this.updateRoomResources();
//   }
//
//   onCreateAppointmentClick(event: MouseEvent) {
//     event.preventDefault();
//     this.sliderTitle = "Add Appointment";
//     this.openAppointmentSlider();
//     //this.onCreateClicked.emit(true);
//   }
//
//   onMenuClick(event: MouseEvent, menu: MenuModel) {
//     if (event) {
//       event.preventDefault();
//     }
//     menu.show = !menu.show;
//
//     if (menu.name == "Schedule") {
//       // let r = this.appointmentLinks.find(a => a.name == "Rooms");
//       // r.show = false;
//       // let wl = this.appointmentLinks.find(a => a.name == "Wait List");
//       // wl.show = false;
//       // this.hideAllMenuExcept("Schedule");
//       this.hideMenu("Rooms");
//       this.hideMenu("Wait List");
//     }
//
//     if (menu.name == "Rooms") {
//       this.hideAllMenuExcept(menu.name);
//     }
//
//     if (menu.name == "Wait List") {
//       this.hideAllMenuExcept(menu.name);
//     }
//
//     //this.onActiveChanged.emit(menu);
//   }
//
//   hideMenu(name: string) {
//     let m = this.appointmentLinks.find((a) => a.name == name);
//     m.show = false;
//   }
//
//   showMenu(name: string) {
//     let m = this.appointmentLinks.find((a) => a.name == name);
//     m.show = true;
//   }
//
//   hideAllMenuExcept(name: string) {
//     this.appointmentLinks
//       .filter((a) => a.name != name)
//       .forEach((a) => {
//         a.show = false;
//       });
//   }
//
//   telerikSchedulerChange($event) {
//     //this.onSwitchClicked.emit($event);
//   }
//
//   onlyEnableFor(name: string) {
//     this.appointmentLinks.forEach((a) => (a.enabled = false));
//     let a = this.appointmentLinks.find((a) => a.name == name);
//     a.enabled = true;
//   }
//
//   loadEvents() {
//     this.editService.read();
//     this.loadStaffs();
//   }
//
//   handlePropertyChange(e: any) {
//     if (this.currentView === "day") {
//     }
//     this.currentView = e.component._currentView.type;
//   }
//
//   loadStaffs(populateDefault: boolean = false) {
//     this.staffService.getAllSpecialityPractitoners().subscribe((sp) => {
//       sp.forEach((s) => {
//         s.photo = s.photo ? `data:image/jpeg;base64,${s.photo}` : null;
//         s.selected = false;
//         s.color = "#a18bd6";
//         s.practitionerspecialitiesNamesString = s.practitionerspecialitiesNames
//           .map((sn) => sn.specialityName)
//           .join(",");
//       });
//
//       this.practitioners = this.allPractitioners = sp;
//       this.selectedPractitioners = this.allPractitioners.filter(
//         (x) => x.selected === true
//       );
//
//       this.dataSource = new DataSource({
//         store: this.getData(this.startDate, this.endDate),
//       });
//
//       this.resourcesDataSource = this.getEmployees(
//         this.startDate,
//         this.endDate
//       );
//
//       this.refreshResourceData();
//
//       if (populateDefault) {
//         this.onPractitionerClick(this.practitioners[0]);
//       }
//     });
//
//     // TODO: wait for api update
//     // this.staffService.getAllSpecialityPractitonersByLocId(this.appState.selectedUserLocationId)
//     // .subscribe(sp => {
//     //   sp.forEach(s => {
//     //     s.photo = s.photo ? `data:image/jpeg;base64,${s.photo}` : null;
//     //     s.selected = false;
//     //     s.color = "#a18bd6";
//     //   });
//     //   this.resourceData = sp;
//     //   this.practitioners = sp;
//     //   let selectedPractitioner = this.practitioners.find(p => p.selected == true);
//     //   if(this.alreadyAdded && !selectedPractitioner) {
//     //     this.onPractitionerClick(this.practitioners[0]);
//     //   }
//
//     //   if(populateDefault) {
//     //     this.onPractitionerClick(this.practitioners[0]);
//     //   }
//     // });
//
//     this.staffService
//       .getAllGroupSpecialityPractitonersByLocId(
//         this.appState.selectedUserLocationId
//       )
//       .subscribe((data) => {
//         data.forEach((s) => {
//           s.practitioners.forEach((p) => {
//             p.photo = p.photo ? `data:image/jpeg;base64,${p.photo}` : null;
//           });
//         });
//
//         this.specialityPractitioners = data;
//       });
//   }
//
//   searchPractitionerSpeciality(searchKey: string) {
//     this.practitioners = this.allPractitioners.filter(
//       (p) =>
//         p.practitionerName.toLowerCase().indexOf(searchKey.toLowerCase()) !== -1
//     );
//
//     const dataBasedOnSpecialityName = this.allPractitioners.filter(
//       (p) =>
//         p.practitionerspecialitiesNamesString
//           .toLowerCase()
//           .indexOf(searchKey.toLowerCase()) !== -1
//     );
//
//     dataBasedOnSpecialityName.map((s) => {
//       const pCheck = this.practitioners.find(
//         (x) => x.practitionerId === s.practitionerId
//       );
//       if (pCheck === undefined) {
//         this.practitioners.push(s);
//       }
//     });
//   }
//
//   refreshResourceData() {
//     this.resourceData = [];
//     if (this.practitioners) {
//       this.practitioners.forEach((s) => {
//         s.practitionerspecialitiesNames.forEach((pn) => {
//           this.resourceData.push({
//             ...s,
//             specialityName: pn.specialityName,
//             specialityPractitionerId: pn.specialityPractitionerId,
//           });
//         });
//       });
//     }
//   }
//
//   loadLocationTime() {
//     if (this.appState.selectedUserLocationId) {
//       this.businessService
//         .getLocation(this.appState.selectedUserLocationId)
//         .subscribe((l) => {
//           this.slotClass = l.timeSlotSize;
//           if (l.startTime && l.endTime) {
//             let s: Date = new Date(l.startTime);
//             let e: Date = new Date(l.endTime);
//             this.locationStart = s;
//             this.locationEnd = e;
//
//             let startTime = `${s.getHours()}:${s.getMinutes()}`;
//             let endTime = `${e.getHours()}:${e.getMinutes()}`;
//             //this._startTime = startTime;
//             //this._endTime = endTime;
//             if (l.timeSlot) {
//               this.slotDuration = l.timeSlot;
//             }
//             this._startTime = 8.5;
//             this._endTime = e.getHours();
//             this.slotDuration = l.timeSlot;
//             // console.log(this._startTime);
//             //console.log(this._endTime);
//             //console.log(this.slotDuration);
//           }
//         });
//     }
//   }
//
//   slotClass: string;
//
//   public getSlotClass = (args: SlotClassArgs) => {
//     let schedulebg = false;
//     let resource = args.resources ? args.resources[0] : null;
//
//     if (this.showAppointment) {
//       if (resource && this.scheduleEvents && this.scheduleEvents.length > 0) {
//         let slotTarget = this.scheduleEvents.find(
//           (e) =>
//             e.practitionerId == resource.practitionerId &&
//             args.start >= e.start &&
//             args.end <= e.end
//         );
//         schedulebg = slotTarget != null;
//       }
//     }
//
//     let d = {
//       small: this.slotClass == "Small",
//       medium: this.slotClass == "Medium",
//       default: this.slotClass == "Default",
//       large: this.slotClass == "Large",
//       big: this.slotClass == "Big",
//       "schedule-bg": schedulebg,
//     };
//
//     return d;
//   };
//
//   public onPractitionerClick(c) {
//     c.selected = !c.selected;
//     const practitionerIndex = this.allPractitioners.findIndex(
//       (p) => p.practitionerId === c.practitionerId
//     );
//
//     this.allPractitioners.map((x) => {
//       if (x.practitionerId === c.practitionerId) {
//         if (c.selected === true) {
//           this.selectedPractitioners.push(c);
//         } else {
//           this.selectedPractitioners.splice(practitionerIndex, 1);
//         }
//       }
//     });
//     //this.scheduler.
//     this.dataSource = new DataSource({
//       store: this.getData(this.startDate, this.endDate),
//     });
//     this.resourcesDataSource = this.selectedPractitioners;
//     this.scheduler.instance.repaint();
//
//     this.refreshCalender();
//
//     this.timeZone = "Australia/Sydney";
//     this.updateResources();
//   }
//
//   refreshCalender() {
//     setTimeout(() => {
//       this.scheduler.instance.repaint();
//     }, 100);
//   }
//
//   practitionerClick(practitionerId) {
//     const practitioner = this.allPractitioners.find(
//       (p) => p.practitionerId === practitionerId
//     );
//
//     this.practitioners.map((x) => {
//       if (x.practitionerId === practitioner.practitionerId) {
//         x.selected = true;
//       } else {
//         x.selected = false;
//       }
//     });
//
//     this.selectedPractitioners = [];
//     this.selectedPractitioners.push(practitioner);
//
//     this.resourcesDataSource = this.selectedPractitioners;
//
//     this.refreshCalender();
//   }
//
//   toggleDefault(practitionerId, status) {
//     this.practitioners.map((x) => {
//       if (x.practitionerId === practitionerId) {
//         x.mouseOver = status;
//       } else {
//         x.mouseOver = false;
//       }
//     });
//   }
//
//   onContentReady(e) {
//     var currentHour = new Date().getHours() - 1;
//
//     e.component.scrollToTime(currentHour, 30, new Date());
//   }
//
//   public onSpecailityClick(s) {
//     s.expand = !s.expand;
//   }
//
//   public togglePanel(e) {
//     this.panelExpand = !this.panelExpand;
//     this.panelSize = this.panelExpand
//       ? this.panelExpandSize
//       : this.panelCollaspeSize;
//   }
//
//   public updateResources() {
//     if (this.practitioners) {
//       let selected = this.practitioners.filter((c) => c.selected);
//       if (selected) {
//         let selectedIds = selected.map((s) => s.practitionerId);
//         let filtered = this.resourceData.filter(
//           (r) => selectedIds.indexOf(r.practitionerId) >= 0
//         );
//         this.updateResourceData(filtered);
//         this.numberOfPractitioners = filtered.length;
//       }
//     }
//   }
//
//   private updateResourceData(data) {
//     let resource = this.resources.find((r) => r.name == "Specialities");
//     resource.data = data;
//     this.onOrientationChange("horizontal");
//   }
//
//   public updateRoomResources() {
//     if (this.rooms) {
//       let selected = this.rooms.filter((c) => c.selected);
//       if (selected) {
//         let selectedIds = selected.map((s) => s.id);
//         let filtered = this.resourceData.filter(
//           (r) => selectedIds.indexOf(r.id) >= 0
//         );
//         this.updateRoomResourceData(filtered);
//         this.numberOfPractitioners = filtered.length;
//       }
//     }
//   }
//
//   private updateRoomResourceData(data) {
//     let resource = this.resources.find((r) => r.name == "Rooms");
//     resource.data = data;
//     this.onOrientationChange("horizontal");
//   }
//
//   public onOrientationChange(value: any): void {
//     this.group = { ...this.group, orientation: value };
//   }
//
//   scheduleEvents: ScheduleEvent[];
//
//   //startDate: Date;
//   //endDate: Date;
//
//   public onDateChange(args: DateChangeEvent): void {
//     this.startDate = args.dateRange.start;
//     this.endDate = args.dateRange.end;
//     this.editService.setDateRange(args.dateRange.start, args.dateRange.end);
//     this.editService.read();
//     this.scheduleEditService.setDateRange(
//       args.dateRange.start,
//       args.dateRange.end
//     );
//     this.scheduleEditService.read();
//     this.refreshWaitLists(args.dateRange.start, args.dateRange.end);
//   }
//
//   checkFirstSchedule(r: any[], d: any) {
//     let result = false;
//     let date = new Date(d);
//     if (
//       r &&
//       r.length > 0 &&
//       this.scheduleEvents &&
//       this.scheduleEvents.length > 0
//     ) {
//       let resource = r[0];
//       if (resource && resource.practitionerId) {
//         let targetDate = this.scheduleEvents.find(
//           (e) => e.practitionerId == resource.practitionerId && e.start == date
//         );
//         if (targetDate) {
//         }
//       }
//     }
//
//     return result;
//   }
//
//   routeOpenSlider() {
//     if (this.showAppointment) {
//       this.openAppointmentSlider();
//     }
//     if (this.showSchedule) {
//       this.openScheduleSlider();
//     }
//     if (this.showRoom) {
//       this.openRoomSlider();
//     }
//   }
//
//   openAppointmentSlider() {
//     document.getElementById("slider").style.width = "700px";
//     document.getElementById("sliderShadow").style.display = "block";
//   }
//
//   closeAppointmentSlider() {
//     document.getElementById("slider").style.width = "0px";
//     document.getElementById("sliderShadow").style.display = "none";
//     this.isAddBreak = false;
//     this.clearPreviousEventSelection();
//   }
//
//   openRoomSlider() {
//     document.getElementById("room_slider").style.width = "700px";
//     document.getElementById("room_sliderShadow").style.display = "block";
//     this.addRoomComp.loadLocationSettings();
//   }
//
//   closeRoomSlider() {
//     document.getElementById("room_slider").style.width = "0px";
//     document.getElementById("room_sliderShadow").style.display = "none";
//   }
//
//   openWaitList(e: MouseEvent, practitionerId: string) {
//     e.preventDefault();
//     this.isNew = true;
//     this.isAddWaitList = true;
//     this.isViewWaitList = false;
//     this.toggleTitle();
//
//     let until: Date = new Date(this.startDate);
//     until.setMonth(until.getMonth() + 1);
//
//     this.waitListEvent = {
//       firstAvailableDate: this.startDate,
//       keepInWaitListUntil: until,
//       availableFrom: this.locationStart,
//       availableUntil: this.locationEnd,
//       practitionerId: practitionerId,
//       isHighPriority: false,
//     };
//
//     this.openWaitListSlider();
//   }
//
//   openWaitListSlider() {
//     document.getElementById("waitlist_slider").style.width = "700px";
//     document.getElementById("waitlist_sliderShadow").style.display = "block";
//     this.clearPreviousEventSelection();
//   }
//
//   closeWaitListSlider() {
//     this.isNew = false;
//     this.isAddWaitList = false;
//     document.getElementById("waitlist_slider").style.width = "0px";
//     document.getElementById("waitlist_sliderShadow").style.display = "none";
//     this.clearPreviousEventSelection();
//   }
//
//   openScheduleSlider() {
//     document.getElementById("schedule_slider").style.width = "700px";
//     document.getElementById("schedule_sliderShadow").style.display = "block";
//     this.addScheduleComp.loadLocationSettings();
//     this.clearPreviousEventSelection();
//   }
//
//   closeScheduleSlider() {
//     document.getElementById("schedule_slider").style.width = "0px";
//     document.getElementById("schedule_sliderShadow").style.display = "none";
//     this.clearPreviousEventSelection();
//   }
//
//   clearPreviousEventSelection() {
//     if (this.lastEventSelectedEvent) {
//       this.lastEventSelectedEvent.classList.remove("selected-event");
//     }
//   }
//
//   toggleTitle() {
//     let t: string;
//
//     if (this.showSchedule) {
//       t = "Schedule";
//     }
//
//     if (this.showAppointment) {
//       t = "Appointment";
//     }
//
//     if (this.showRoom) {
//       t = "Room";
//     }
//
//     if (this.isAddBreak) {
//       t = "Break";
//     }
//
//     if (this.isAddClass) {
//       t = "Class";
//     }
//
//     if (this.isAddAppointment) {
//       t = "Appointment";
//     }
//
//     if (this.isAddWaitList) {
//       t = "Patient to Wait";
//     }
//
//     this.sliderTitle = this.isNew ? `Add ${t}` : `Edit ${t}`;
//
//     if (this.isViewWaitList) {
//       this.sliderTitle = "View Wait List";
//     }
//   }
//
//   private clickedDataItem: any = null;
//   private clickSender: any = null;
//
//   public slotDblClickHandler({
//     start,
//     end,
//     isAllDay,
//     originalEvent,
//     sender,
//   }: SlotClickEvent): void {
//     if (sender) {
//       let e: MouseEvent = originalEvent;
//       let slot = sender.slotByPosition(e.pageX, e.pageY);
//       let p_id =
//         slot && slot.resources && slot.resources.length > 0
//           ? slot.resources[0].practitionerId
//           : "";
//       let sp_id =
//         slot && slot.resources && slot.resources.length > 0
//           ? slot.resources[0].specialityPractitionerId
//           : "";
//
//       this.isNew = true;
//       this.toggleTitle();
//       this.clickedDataItem = null;
//       this.clickSender = sender;
//
//       this.editMode = EditMode.Series;
//
//       this.editedEvent = {
//         start: start,
//         end: end,
//         isAllDay: isAllDay,
//         practitioner: this.practitioners.find((p) => p.practitionerId == p_id),
//         practitionerId: p_id,
//         specialityPractitionerId: sp_id,
//         locationId: this.appState.selectedUserLocationId,
//       };
//
//       this.routeOpenSlider();
//     }
//   }
//
//   routeDblClickHandler({ sender, event }: EventClickEvent): void {}
//
//   lastEventSelectedEvent: HTMLElement;
//
//   eventDblClickHandler({
//     sender,
//     event,
//     originalEvent,
//   }: EventClickEvent): void {
//     this.isNew = false;
//     this.lastEventSelectedEvent = null;
//
//     if (event.dataItem.type && event.dataItem.type == "Break") {
//       this.isAddBreak = true;
//       this.lastEventSelectedEvent = originalEvent.srcElement.closest(
//         "div.break-event"
//       );
//       this.lastEventSelectedEvent.classList.add("selected-event");
//     }
//
//     this.toggleTitle();
//
//     let dataItem = event.dataItem;
//     this.clickedDataItem = dataItem;
//     this.clickSender = sender;
//
//     dataItem.practitioner = this.practitioners.find(
//       (p) => p.practitionerId == event.dataItem.practitionerId
//     );
//     dataItem.practitionerId = event.dataItem.practitionerId;
//
//     if (this.editService.isRecurring(dataItem)) {
//       sender
//         .openRecurringConfirmationDialog(CrudOperation.Edit)
//         .subscribe((editMode: EditMode) => {
//           if (editMode) {
//             if (editMode === EditMode.Series) {
//               dataItem = this.editService.findRecurrenceMaster(dataItem);
//               dataItem.practitioner = this.practitioners.find(
//                 (p) => p.practitionerId == event.dataItem.practitionerId
//               );
//               dataItem.practitionerId = event.dataItem.practitionerId;
//             }
//             this.editMode = editMode;
//             this.editedEvent = dataItem;
//             this.routeOpenSlider();
//           } else {
//             this.clearPreviousEventSelection();
//           }
//         });
//     } else {
//       this.editMode = EditMode.Series;
//       this.editedEvent = dataItem;
//       this.routeOpenSlider();
//     }
//   }
//
//   onRoomSave(formValue: any) {
//     if (this.isNew) {
//       this.editService.create(formValue);
//     } else {
//       this.handleUpdate(this.editedEvent, formValue, this.editMode);
//     }
//
//     this.editService.read();
//     this.closeRoomSlider();
//     this.cd.detectChanges();
//     this.onOrientationChange("horizontal");
//   }
//
//   onScheduleSave(formValue: any) {
//     if (this.isNew) {
//       this.editService.create(formValue);
//     } else {
//       this.handleUpdate(this.editedEvent, formValue, this.editMode);
//     }
//
//     this.editService.read();
//     this.closeScheduleSlider();
//     this.cd.detectChanges();
//     this.onOrientationChange("horizontal");
//   }
//
//   onWaitListSave(formValue: any) {
//     if (this.isNew) {
//       this.appointmentService
//         .createPractitionerWaitList(formValue)
//         .subscribe((r) => {
//           this.refreshWaitLists();
//         });
//     } else {
//       this.appointmentService
//         .updatePractitionerWaitList(formValue)
//         .subscribe((r) => {
//           this.refreshWaitLists();
//         });
//     }
//     this.closeWaitListSlider();
//   }
//
//   refreshWaitLists(s = null, e = null) {
//     if (this.waitlistItems) {
//       this.waitlistItems.forEach((wl) => {
//         if (s == null && e == null) {
//           wl.refreshWaitlist(this.startDate, this.endDate);
//         } else {
//           wl.refreshWaitlist(s, e);
//         }
//       });
//     }
//   }
//
//   onBreakSave(formValue: any) {
//     formValue.type = "Break";
//     formValue.breakTypes = "Break";
//     if (this.isNew) {
//       this.editService.create(formValue);
//     } else {
//       this.handleUpdate(this.editedEvent, formValue, this.editMode);
//     }
//
//     this.closeAppointmentSlider();
//     this.cd.detectChanges();
//     this.onOrientationChange("horizontal");
//   }
//
//   private handleUpdate(item: any, value: any, mode: EditMode): void {
//     const service = this.editService;
//     if (mode === EditMode.Occurrence) {
//       if (service.isException(item)) {
//         service.update(item, value);
//       } else {
//         service.createException(item, value);
//       }
//     } else {
//       // Item is not recurring or we're editing the entire series
//       service.update(item, value);
//     }
//   }
//
//   removeItem() {
//     if (this.clickedDataItem && this.clickSender) {
//       let r: RemoveEvent = new RemoveEvent(this.clickSender, {
//         dataItem: this.clickedDataItem,
//       });
//       this.removeHandler(r);
//       this.closeAllSliders();
//     }
//   }
//
//   public opened: boolean = false;
//
//   public close(status: string) {
//     this.opened = false;
//     this.isAddWaitList = false;
//
//     if (status === "yes") {
//       if (this.waitListEvent) {
//         this.appointmentService
//           .removePractitionerWaitList(this.waitListEvent.id)
//           .subscribe((r) => {
//             this.refreshWaitLists();
//             this.closeWaitListSlider();
//           });
//       }
//     }
//   }
//
//   removeWaitlist() {
//     this.opened = true;
//   }
//
//   editWaitlist() {
//     this.isAddWaitList = true;
//     this.isNew = false;
//     this.isViewWaitList = false;
//     this.toggleTitle();
//   }
//
//   removeHandler({ sender, dataItem }: RemoveEvent) {
//     if (this.editService.isRecurring(dataItem)) {
//       sender
//         .openRecurringConfirmationDialog(CrudOperation.Remove)
//         // result will be undefined if the Dialog was closed
//         .pipe(filter((editMode) => editMode !== undefined))
//         .subscribe((editMode) => {
//           this.handleRemove(dataItem, editMode);
//         });
//     } else {
//       sender.openRemoveConfirmationDialog().subscribe((shouldRemove) => {
//         if (shouldRemove) {
//           this.editService.remove(dataItem);
//         }
//       });
//     }
//   }
//
//   private handleRemove(item: any, mode: EditMode): void {
//     const service = this.editService;
//     if (mode === EditMode.Series) {
//       service.removeSeries(item);
//     } else if (mode === EditMode.Occurrence) {
//       if (service.isException(item)) {
//         service.remove(item);
//       } else {
//         service.removeOccurrence(item);
//       }
//     } else {
//       service.remove(item);
//     }
//   }
//
//   onCancel(event) {
//     this.showSchedule = false;
//     this.closeAllSliders();
//   }
//
//   closeAllSliders() {
//     this.closeAppointmentSlider();
//     this.closeRoomSlider();
//     this.closeScheduleSlider();
//     this.closeWaitListSlider();
//   }
//
//   onNavigate(event) {
//     this.hideAllPopover();
//   }
//
//   toggleEventType(t: string) {
//     this.toggleTitle();
//   }
//
//   onBreakClick(event: MouseEvent, id: string) {
//     event.preventDefault();
//     this.isNew = true;
//     this.isAddBreak = true;
//     this.editMode = EditMode.Series;
//     this.hidePopover(id);
//     this.openAppointmentSlider();
//     this.toggleTitle();
//   }
//
//   onClassClick(event: MouseEvent, id) {
//     event.preventDefault();
//     this.isNew = true;
//     this.isAddClass = true;
//     this.hidePopover(id);
//     this.toggleTitle();
//   }
//
//   onAppointmentClick(event: MouseEvent, id) {
//     event.preventDefault();
//     this.isNew = true;
//     this.isAddAppointment = true;
//     this.hidePopover(id);
//     this.toggleTitle();
//   }
//
//   onWaitlistClick(event: MouseEvent) {}
//
//   routeClickEvent({
//     start,
//     end,
//     isAllDay,
//     originalEvent,
//     sender,
//   }: SlotClickEvent) {
//     if (sender) {
//       if (this.showRoom) {
//         return;
//       }
//
//       if (this.showAppointment) {
//         this.onShowPopoverClick({
//           start,
//           end,
//           isAllDay,
//           originalEvent,
//           sender,
//         } as SlotClickEvent);
//       }
//       if (this.showRoom) {
//         this.roomClickHandler({
//           start,
//           end,
//           isAllDay,
//           originalEvent,
//           sender,
//         } as SlotClickEvent);
//       }
//       if (this.showSchedule) {
//         this.scheduleClickHandler({
//           start,
//           end,
//           isAllDay,
//           originalEvent,
//           sender,
//         } as SlotClickEvent);
//       }
//     }
//   }
//
//   public roomClickHandler({
//     start,
//     end,
//     isAllDay,
//     originalEvent,
//     sender,
//   }: SlotClickEvent): void {
//     this.isNew = true;
//     this.toggleTitle();
//
//     this.editMode = EditMode.Series;
//
//     this.editedEvent = {
//       start: start,
//       end: end,
//       isAllDay: isAllDay,
//     };
//
//     this.openRoomSlider();
//   }
//
//   public scheduleClickHandler({
//     start,
//     end,
//     isAllDay,
//     originalEvent,
//     sender,
//   }: SlotClickEvent): void {
//     let e: MouseEvent = originalEvent;
//     let slot = sender.slotByPosition(e.pageX, e.pageY);
//     let p_id =
//       slot && slot.resources && slot.resources.length > 0
//         ? slot.resources[0].practitionerId
//         : "";
//     let sp_id =
//       slot && slot.resources && slot.resources.length > 0
//         ? slot.resources[0].specialityPractitionerId
//         : "";
//
//     let eventExist: boolean = false;
//     let exist = this.scheduleEvents.find(
//       (s) => start >= s.start && end <= s.end && s.practitionerId == p_id
//     );
//     eventExist = exist != null;
//
//     if (!eventExist) {
//       this.isNew = true;
//       this.toggleTitle();
//
//       this.editMode = EditMode.Series;
//
//       this.editedEvent = {
//         start: start,
//         end: end,
//         isAllDay: isAllDay,
//         practitioner: this.practitioners.find((p) => p.practitionerId == p_id),
//         practitionerId: p_id,
//         specialityPractitionerId: sp_id,
//         locationId: this.appState.selectedUserLocationId,
//       };
//
//       this.openScheduleSlider();
//     }
//   }
//
//   onShowPopoverClick({
//     start,
//     end,
//     isAllDay,
//     originalEvent,
//     sender,
//   }: SlotClickEvent) {
//     let e: MouseEvent = originalEvent;
//     let slot = sender.slotByPosition(e.pageX, e.pageY);
//     let p_id =
//       slot && slot.resources && slot.resources.length > 0
//         ? slot.resources[0].practitionerId
//         : "";
//     let sp_id =
//       slot && slot.resources && slot.resources.length > 0
//         ? slot.resources[0].specialityPractitionerId
//         : "";
//
//     let eventExist: boolean = false;
//     let exist = this.currentEvents.find(
//       (s) => start >= s.start && end <= s.end && s.practitionerId == p_id
//     );
//     eventExist = exist != null;
//
//     if (!eventExist) {
//       this.startTime = start;
//       this.endTime = end;
//
//       this.editedEvent = {
//         start: start,
//         end: end,
//         isAllDay: isAllDay,
//         practitioner: this.practitioners.find((p) => p.practitionerId == p_id),
//         practitionerId: p_id,
//         specialityPractitionerId: sp_id,
//         locationId: this.appState.selectedUserLocationId,
//       };
//
//       this.showPopover(p_id, e);
//     } else {
//       this.hideAllPopover();
//     }
//   }
//
//   onWaitlistClicked(e: WaitListEvent) {
//     this.isViewWaitList = true;
//     this.toggleTitle();
//     e.availableFrom = new Date(e.availableFrom);
//     e.availableUntil = new Date(e.availableUntil);
//
//     e.firstAvailableDate = new Date(e.firstAvailableDate);
//     e.keepInWaitListUntil = new Date(e.keepInWaitListUntil);
//     e.availableFrom.setSeconds(0);
//     e.availableFrom.setMilliseconds(0);
//     e.availableUntil.setSeconds(0);
//     e.availableUntil.setMilliseconds(0);
//
//     this.waitListEvent = e;
//     this.isNew = false;
//     this.openWaitListSlider();
//   }
//
//   onClosePopoverClick(event: MouseEvent, id: string) {
//     this.hidePopover(id);
//   }
//
//   hideAllPopover() {
//     if (this.resourceData && this.resourceData.length > 0) {
//       this.resourceData.forEach((r) => {
//         this.hidePopover(r.practitionerId);
//       });
//     }
//   }
//
//   showPopover(id: string, e: MouseEvent) {
//     this.hideAllPopover();
//     // let p = document.querySelector("#pop_" + id);
//     let p: Element = e.target as Element;
//     let t: HTMLElement = document.querySelector("#tp_" + id);
//     t.setAttribute("data-show", "");
//     createPopper(p, t, {
//       placement: "bottom",
//     });
//   }
//
//   hidePopover(id: string) {
//     let t: HTMLElement = document.querySelector("#tp_" + id);
//     if (t) {
//       t.removeAttribute("data-show");
//     }
//   }
//
//   getEmployees(startDate: Date, endDate: Date) {
//     return this.selectedPractitioners;
//   }
//
//   getData(startDate: Date, endDate: Date) {
//     return data;
//   }
// }
//
// let data: Data[] = [];
