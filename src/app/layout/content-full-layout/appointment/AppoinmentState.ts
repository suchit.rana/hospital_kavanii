import {AppoinmentDateUtils} from './AppoinmentDateUtils';

export class AppoinmentState {

  static timeZone = "Australia/Sydney";
  static currentDate = AppoinmentDateUtils.getCurrentDate();

}
