import {Component, EventEmitter, Input, OnInit, Output, AfterViewInit} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {AppState} from '../../../../app.state';
import {Constant} from '../../../../shared/Constant';
import {SchedulerManageService} from '../scheduler-manage.service';
import {Observable} from 'rxjs';
import {For, FromPage} from '../../../../shared/interface';
import {BaseItemComponent} from '../../../../shared/base-item/base-item.component';
import {Router} from '@angular/router';
import { Location } from '@angular/common';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-appoinment-header',
  templateUrl: './appoinment-header.component.html',
  styleUrls: ['./appoinment-header.component.css']
})
export class AppoinmentHeaderComponent extends BaseItemComponent implements OnInit, AfterViewInit {
  readonly APPT_SCHEDULE = RP_MODULE_MAP.appointment_schedule;
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly CREATE_APPT = Constant.CREATE_APPT;
  readonly SCHEDULE = Constant.SCHEDULE;
  readonly WAIT_LIST = Constant.WAIT_LIST;
  readonly ROOMS = Constant.ROOMS;
  readonly ROOMS_ACTIVE = Constant.ROOMS_ACTIVE;
  @Input()
  fromDashboard:boolean=false;
  @Input()
  waitListCount:number = 0;

  @Output()
  changeGroup: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  buttonClick: EventEmitter<string> = new EventEmitter<string>();

  isSmallScreen: boolean = true;
  selectedButton: string = null;

  constructor(
      public location: Location,
      protected router: Router,
    private breakPoint: BreakpointObserver,
    protected appState: AppState,
    public schedulerManageService: SchedulerManageService
  ) {
    super(location, router);
    this.isSmallScreen = appState.isTablet;
    this.breakPoint.observe(['(max-width: 992px)']).subscribe(state => {
      this.isSmallScreen = state.matches;
    });
  }

  ngOnInit() {
    if (this.fromDashboard) {
      this.selectedButton ='SCHEDULE';
    }
  }

  ngAfterViewInit() {
    if (this.isFromState()) {
       if (this.routerState.for === For.TreatmentRoom && this.routerState.fromPage === FromPage.TreatmentRoom) {
         this.selectedButton = this.ROOMS;
      }
    }
  }

  buttonClickGrp() {
    this.changeGroup.emit(null);
  }

  onButtonClick(action) {
    if (this.schedulerManageService.displayRoomVal
        && (
            this.selectedButton === this.CREATE_APPT
            || this.selectedButton === this.SCHEDULE
        )) {
      return;
    }
    if (this.selectedButton == action && this.selectedButton != this.CREATE_APPT && this.selectedButton != this.WAIT_LIST){
      this.selectedButton = null;
    }else{
      this.selectedButton = action;
    }
    this.buttonClick.emit(this.selectedButton);
  }

  isDisplayRoom$(): Observable<boolean> {
    return this.schedulerManageService.displayRoomSubject.asObservable();
  }

}
