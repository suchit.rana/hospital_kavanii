// import {
//   Component,
//   OnInit,
//   ViewChild,
//   ChangeDetectorRef,
//   Input,
// } from "@angular/core";
// import { AppState } from "../../../../app.state";
// import {
//   DateChangeEvent,
//   SlotClickEvent,
//   EventClickEvent,
//   CrudOperation,
//   EditMode,
//   RemoveEvent,
//   SlotClassArgs,
// } from "@progress/kendo-angular-scheduler";
// import "@progress/kendo-date-math/tz/all";
// import { StaffService } from "../../../../services/app.staff.service";
// import {
//   PractitionerSpecialityModel,
//   GroupSpecialityPractitonerModel,
// } from "../../../../models/app.staff.model";
// import { AddScheduleComponent } from "./add-schedule/add-schedule.component";
// import { ScheduleEvent, Scheduler } from "src/app/models/app.appointment.model";
// import { BusinessService } from "src/app/services/app.business.service";
// import { ScheduleEditService } from "src/app/services/app.schedule.editservice";
// import { filter } from "rxjs/operators";
// import { createPopper } from "@popperjs/core";
// import { Data } from "src/app/services/app.appointment.service";
// import { DxSchedulerComponent } from "devextreme-angular";
//
// @Component({
//   selector: "app-schedule",
//   templateUrl: "./schedule.component.html",
//   styleUrls: [
//     "./schedule.component.css",
//     "../appointment.component.css",
//   ],
// })
// export class ScheduleComponent implements OnInit {
//   @ViewChild(AddScheduleComponent, { static: false })
//   addSchedule: AddScheduleComponent;
//   @ViewChild(DxSchedulerComponent, { static: false })
//   scheduler: DxSchedulerComponent;
//
//   //dataSource: any;
//   schedulerData: Scheduler[] = [];
//   currentDate: Date = new Date(2021, 7, 2, 11, 30);
//   startDate: Date = new Date(2021, 7, 2, 11, 30);
//   endDate: Date = new Date(2021, 7, 2, 11, 30);
//   groupByDate: boolean = false;
//   resourcesDataSource: Array<PractitionerSpecialityModel> = [];
//   currentView: string = "day";
//   classObject: any;
//   tempData: Data[] = [];
//   tempEmployees: Array<PractitionerSpecialityModel> = [];
//   timeZone: string;
//   practitionerId: string;
//   public allPractitioners: Array<PractitionerSpecialityModel>;
//   public selectedPractitioners: Array<PractitionerSpecialityModel>;
//
//   @Input() showWaitList: boolean;
//   @Input() showAppointment: boolean;
//   @Input() showRoom: boolean;
//   @Input() showSchedule: boolean;
//   IsShow: boolean;
//
//   constructor(
//     public appState: AppState,
//     private staffService: StaffService,
//     private businessService: BusinessService,
//     public editService: ScheduleEditService,
//     private cd: ChangeDetectorRef
//   ) {}
//
//   public numberOfPractitioners: number = 0;
//   public alreadyAdded: boolean = false;
//
//   resourceData: PractitionerSpecialityModel[] = [];
//   public practitioners: Array<PractitionerSpecialityModel>;
//   public specialityPractitioners: GroupSpecialityPractitonerModel[];
//
//   panelExpandSize: string = "300px";
//   panelCollaspeSize: string = "200px";
//   panelSize: string = this.panelExpandSize;
//   panelExpand: boolean = true;
//
//   resource: boolean = false;
//
//   sliderTitle: string = "Add Schedule";
//
//   slotDuration: number = 60;
//
//   _startTime: number = 7;
//   _endTime: number = 19;
//
//   locationStart: Date;
//   locationEnd: Date;
//
//   public editedEvent: any;
//   public editMode: EditMode;
//   public isNew: boolean;
//   public selectedDate: Date;
//   public events: ScheduleEvent[];
//   public group: any = {
//     resources: ["Specialities"],
//     orientation: "horizontal",
//   };
//
//   public resources: any[] = [
//     {
//       name: "Specialities",
//       data: [],
//       field: "practitionerId",
//       valueField: "practitionerId",
//       textField: "text",
//       colorField: "color",
//       multiple: false,
//     },
//   ];
//
//   ngOnInit() {
//     this.appState.sideNavState.next(false);
//     this.loadLocationTime();
//     this.loadEvents();
//     this.timeZone = "Australia/Sydney";
//     this.groupByDate = false;
//     //this.openSlider();
//   }
//
//   loadEvents() {
//     this.appState.selectedUserLocationIdState.subscribe((l) => {
//       this.editService.read();
//       this.loadLocationTime();
//     });
//
//     this.editService.read();
//
//     this.loadStaffs();
//   }
//
//   loadStaffs(populateDefault: boolean = false) {
//     this.staffService.getAllSpecialityPractitoners().subscribe((sp) => {
//       sp.forEach((s) => {
//         s.photo = s.photo ? `data:image/jpeg;base64,${s.photo}` : null;
//         s.selected = true;
//         s.color = "#a18bd6";
//         s.practitionerspecialitiesNamesString = s.practitionerspecialitiesNames
//           .map((sn) => sn.specialityName)
//           .join(",");
//       });
//
//       this.practitioners = this.allPractitioners = sp;
//       this.selectedPractitioners = this.allPractitioners.filter(
//         (x) => x.selected === true
//       );
//
//       console.log(this.selectedPractitioners);
//
//       this.numberOfPractitioners = this.selectedPractitioners.length;
//       //this.schedulerData =  this.getData();
//
//       console.log(this.schedulerData);
//
//       this.selectedPractitioners.map((x) => {
//         x.id = x.practitionerId;
//       });
//
//       this.resourcesDataSource = this.selectedPractitioners;
//
//       //this.refreshResourceData();
//
//       if (populateDefault) {
//         this.onPractitionerClick(this.practitioners[0]);
//       }
//     });
//
//     // TODO: wait for api update
//     // this.staffService.getAllSpecialityPractitonersByLocId(this.appState.selectedUserLocationId)
//     // .subscribe(sp => {
//     //   sp.forEach(s => {
//     //     s.photo = s.photo ? `data:image/jpeg;base64,${s.photo}` : null;
//     //     s.selected = false;
//     //     s.color = "#a18bd6";
//     //   });
//     //   this.resourceData = sp;
//     //   this.practitioners = sp;
//     //   let selectedPractitioner = this.practitioners.find(p => p.selected == true);
//     //   if(this.alreadyAdded && !selectedPractitioner) {
//     //     this.onPractitionerClick(this.practitioners[0]);
//     //   }
//
//     //   if(populateDefault) {
//     //     this.onPractitionerClick(this.practitioners[0]);
//     //   }
//     // });
//
//     this.staffService
//       .getAllGroupSpecialityPractitonersByLocId(
//         this.appState.selectedUserLocationId
//       )
//       .subscribe((data) => {
//         data.forEach((s) => {
//           s.practitioners.forEach((p) => {
//             p.photo = p.photo ? `data:image/jpeg;base64,${p.photo}` : null;
//           });
//         });
//
//         this.specialityPractitioners = data;
//       });
//   }
//
//   getEmployees() {
//     return this.selectedPractitioners;
//   }
//
//   loadLocationTime() {
//     if (this.appState.selectedUserLocationId) {
//       this.businessService
//         .getLocation(this.appState.selectedUserLocationId)
//         .subscribe((l) => {
//           this.slotClass = l.timeSlotSize;
//           if (l.startTime && l.endTime) {
//             let s: Date = new Date(l.startTime);
//             let e: Date = new Date(l.endTime);
//             this.locationStart = s;
//             this.locationEnd = e;
//
//             let startTime = `${s.getHours()}:${s.getMinutes()}`;
//             let endTime = `${e.getHours()}:${e.getMinutes()}`;
//             //this._startTime = startTime;
//             //this._endTime = endTime;
//             if (l.timeSlot) {
//               this.slotDuration = l.timeSlot;
//             }
//             this._startTime = s.getHours();
//             this._endTime = 23; // e.getHours();
//             this.slotDuration = l.timeSlot;
//             // console.log(this._startTime);
//             //console.log(this._endTime);
//             //console.log(this.slotDuration);
//           }
//         });
//     }
//   }
//
//   slotClass: string;
//
//   public getSlotClass = (args: SlotClassArgs) => {
//     return {
//       small: this.slotClass == "Small",
//       medium: this.slotClass == "Medium",
//       default: this.slotClass == "Default",
//       large: this.slotClass == "Large",
//       big: this.slotClass == "Big",
//     };
//   };
//
//   public onPractitionerClick(c) {
//     c.selected = !c.selected;
//
//     const practitionerIndex = this.allPractitioners.findIndex(
//       (p) => p.practitionerId === c.practitionerId
//     );
//
//     this.allPractitioners.map((x) => {
//       if (x.practitionerId === c.practitionerId) {
//         if (c.selected === true) {
//           this.selectedPractitioners.push(c);
//         } else {
//           this.selectedPractitioners.splice(practitionerIndex, 1);
//         }
//       }
//     });
//     //this.scheduler.
//     //this.schedulerData =  this.getData();
//
//     this.selectedPractitioners.map((x) => {
//       x.id = x.practitionerId;
//     });
//
//     this.resourcesDataSource = this.selectedPractitioners;
//
//     //this.scheduler.instance.repaint();
//
//     this.refreshCalender();
//
//     this.timeZone = "Australia/Sydney";
//
//     this.updateResources();
//   }
//
//   public onSpecailityClick(s) {
//     s.expand = !s.expand;
//   }
//
//   refreshCalender() {
//     setTimeout(() => {
//       this.scheduler.instance.repaint();
//     }, 100);
//   }
//
//   handlePropertyChange(e: any) {
//     console.log("handlePropertyChange", e);
//     if (this.currentView === "day") {
//     }
//     this.currentView = e.component._currentView.type;
//   }
//
//   onContentReady(e) {
//     console.log("onContentReady", e);
//   }
//
//   onAppointmentDbl(e) {
//     e.cancel = true;
//     //this.openAppointmentSlider();
//     console.log("onAppointmentDblClick", e);
//   }
//   practitionerClick(practitionerId) {
//     const practitioner = this.allPractitioners.find(
//       (p) => p.practitionerId === practitionerId
//     );
//
//     this.practitioners.map((x) => {
//       if (x.practitionerId === practitioner.practitionerId) {
//         x.selected = true;
//       } else {
//         x.selected = false;
//       }
//     });
//
//     this.selectedPractitioners = [];
//     this.selectedPractitioners.push(practitioner);
//
//     this.selectedPractitioners.map((x) => {
//       x.id = x.practitionerId;
//     });
//
//     this.resourcesDataSource = this.selectedPractitioners;
//
//     this.refreshCalender();
//   }
//
//   toggleDefault(practitionerId, status) {
//     this.practitioners.map((x) => {
//       if (x.practitionerId === practitionerId) {
//         x.mouseOver = status;
//       } else {
//         x.mouseOver = false;
//       }
//     });
//   }
//
//   searchPractitionerSpeciality(searchKey: string) {
//     this.practitioners = this.allPractitioners.filter(
//       (p) =>
//         p.practitionerName.toLowerCase().indexOf(searchKey.toLowerCase()) !== -1
//     );
//
//     const dataBasedOnSpecialityName = this.allPractitioners.filter(
//       (p) =>
//         p.practitionerspecialitiesNamesString
//           .toLowerCase()
//           .indexOf(searchKey.toLowerCase()) !== -1
//     );
//
//     dataBasedOnSpecialityName.map((s) => {
//       const pCheck = this.practitioners.find(
//         (x) => x.practitionerId === s.practitionerId
//       );
//       if (pCheck === undefined) {
//         this.practitioners.push(s);
//       }
//     });
//   }
//
//   onAppointment(e) {
//     //e.cancel = true;
//     console.log("onAppointmentClick", e);
//   }
//
//   onCell(e) {
//     //e.cancel = true;
//     //event.preventDefault();
//     //this.hidePopover(id);
//     this.isNew = true;
//     this.toggleTitle();
//     this.editMode = EditMode.Series;
//     this.editedEvent = {
//       start: e.cellData.startDate,
//       end: e.cellData.endDate,
//       isAllDay: e.cellData.allDay,
//       practitionerId: e.cellData.groups,
//     };
//     this.openSlider();
//   }
//
//   public togglePanel(e) {
//     this.panelExpand = !this.panelExpand;
//     this.panelSize = this.panelExpand
//       ? this.panelExpandSize
//       : this.panelCollaspeSize;
//   }
//
//   public updateResources() {
//     let selected = this.practitioners.filter((c) => c.selected);
//     if (selected) {
//       let selectedIds = selected.map((s) => s.practitionerId);
//       let filtered = this.resourceData.filter(
//         (r) => selectedIds.indexOf(r.practitionerId) >= 0
//       );
//       this.updateResourceData(filtered);
//       this.numberOfPractitioners = this.selectedPractitioners.length;
//     }
//   }
//
//   private updateResourceData(data) {
//     let resource = this.resources.find((r) => r.name == "Specialities");
//     resource.data = data;
//     this.onOrientationChange("horizontal");
//   }
//
//   public onOrientationChange(value: any): void {
//     this.group = { ...this.group, orientation: value };
//   }
//
//   public onDateChange(args: DateChangeEvent): void {
//     this.editService.setDateRange(args.dateRange.start, args.dateRange.end);
//     this.editService.read();
//     this.cd.detectChanges();
//   }
//
//   openSlider() {
//     document.getElementById("schedule_slider").style.width = "700px";
//     document.getElementById("schedule_sliderShadow").style.display = "block";
//     //this.addSchedule.loadLocationSettings();
//   }
//
//   closeSlider() {
//     document.getElementById("schedule_slider").style.width = "0px";
//     document.getElementById("schedule_sliderShadow").style.display = "none";
//   }
//
//   toggleTitle() {
//     let t: string;
//     t = "Schedule";
//     this.sliderTitle = this.isNew ? `Add ${t}` : `Update ${t}`;
//   }
//
//   private clickedDataItem: any = null;
//   private clickSender: any = null;
//
//   eventDblClickHandler({ sender, event }: EventClickEvent): void {
//     this.isNew = false;
//     this.toggleTitle();
//     let dataItem = event.dataItem;
//     this.clickedDataItem = dataItem;
//     this.clickSender = sender;
//
//     dataItem.practitioner = this.practitioners.find(
//       (p) => p.practitionerId == event.dataItem.practitionerId
//     );
//     dataItem.practitionerId = event.dataItem.practitionerId;
//
//     if (this.editService.isRecurring(dataItem)) {
//       sender
//         .openRecurringConfirmationDialog(CrudOperation.Edit)
//         // The dialog will emit `undefined` on cancel
//         .pipe(filter((editMode) => editMode !== undefined))
//         .subscribe((editMode: EditMode) => {
//           if (editMode === EditMode.Series) {
//             dataItem = this.editService.findRecurrenceMaster(dataItem);
//             dataItem.practitioner = this.practitioners.find(
//               (p) => p.practitionerId == event.dataItem.practitionerId
//             );
//             dataItem.practitionerId = event.dataItem.practitionerId;
//           }
//           this.editMode = editMode;
//           this.editedEvent = dataItem;
//         });
//     } else {
//       this.editMode = EditMode.Series;
//       this.editedEvent = dataItem;
//     }
//
//     this.openSlider();
//   }
//
//   onScheduleSave(formValue: any) {
//     console.log(formValue);
//     if (this.addSchedule.isNew) {
//       this.editService.create(formValue);
//     } else {
//       this.handleUpdate(this.editedEvent, formValue, this.editMode);
//     }
//
//     this.editService.read();
//     this.closeSlider();
//     this.cd.detectChanges();
//     this.onOrientationChange("horizontal");
//   }
//
//   private handleUpdate(item: any, value: any, mode: EditMode): void {
//     const service = this.editService;
//     if (mode === EditMode.Occurrence) {
//       if (service.isException(item)) {
//         service.update(item, value);
//       } else {
//         service.createException(item, value);
//       }
//     } else {
//       // Item is not recurring or we're editing the entire series
//       service.update(item, value);
//     }
//   }
//
//   removeItem() {
//     if (this.clickedDataItem && this.clickSender) {
//       let r: RemoveEvent = new RemoveEvent(this.clickSender, {
//         dataItem: this.clickedDataItem,
//       });
//       this.removeHandler(r);
//       this.closeSlider();
//     }
//   }
//
//   removeHandler({ sender, dataItem }: RemoveEvent) {
//     if (this.editService.isRecurring(dataItem)) {
//       sender
//         .openRecurringConfirmationDialog(CrudOperation.Remove)
//         // result will be undefined if the Dialog was closed
//         .pipe(filter((editMode) => editMode !== undefined))
//         .subscribe((editMode) => {
//           this.handleRemove(dataItem, editMode);
//         });
//     } else {
//       sender.openRemoveConfirmationDialog().subscribe((shouldRemove) => {
//         if (shouldRemove) {
//           this.editService.remove(dataItem);
//         }
//       });
//     }
//   }
//
//   private handleRemove(item: any, mode: EditMode): void {
//     const service = this.editService;
//     if (mode === EditMode.Series) {
//       service.removeSeries(item);
//     } else if (mode === EditMode.Occurrence) {
//       if (service.isException(item)) {
//         service.remove(item);
//       } else {
//         service.removeOccurrence(item);
//       }
//     } else {
//       service.remove(item);
//     }
//   }
//
//   onCancel(event) {
//     this.closeSlider();
//   }
//
//   onNavigate(event) {
//     this.hideAllPopover();
//   }
//
//   onBreakClick(event: MouseEvent, id) {
//     //event.preventDefault();
//     this.hidePopover(id);
//     this.openSlider();
//   }
//
//   onClassClick(event: MouseEvent, id) {
//     // event.preventDefault();
//     this.hidePopover(id);
//   }
//
//   onAppointmentClick(event: MouseEvent, id) {
//     // event.preventDefault();
//     this.hidePopover(id);
//   }
//
//   onShowPopoverClick(event: SlotClickEvent) {
//     if (event.resources && event.resources.length > 0) {
//       let id = event.resources[0].practitionerId;
//       let e = event.originalEvent;
//       this.showPopover(id, e);
//     }
//   }
//
//   onClosePopoverClick(event: MouseEvent, id: string) {
//     this.hidePopover(id);
//   }
//
//   hideAllPopover() {
//     this.resourceData.forEach((r) => {
//       this.hidePopover(r.practitionerId);
//     });
//   }
//
//   openfilter() {
//     if (this.IsShow == false) {
//       this.IsShow = true;
//     } else {
//       this.IsShow = false;
//     }
//   }
//
//   showPopover(id: string, e: MouseEvent) {
//     this.hideAllPopover();
//     // let p = document.querySelector("#pop_" + id);
//     let p: Element = e.target as Element;
//     let t: HTMLElement = document.querySelector("#tp_" + id);
//     t.setAttribute("data-show", "");
//     createPopper(p, t, {
//       placement: "bottom",
//     });
//   }
//
//   hidePopover(id: string) {
//     let t: HTMLElement = document.querySelector("#tp_" + id);
//     t.removeAttribute("data-show");
//   }
// }
// /*
// let data: Data[] = [
//   {
//     text: "Helen 1",
//     practitionerId: "3f9fbefe-b98b-4aa7-8309-feaddeefcc95",
//     startDate: new Date("2021-08-02T16:30:00.000Z"),
//     endDate: new Date("2021-06-02T18:30:00.000Z"),
//     // recurrenceRule: "FREQ=WEEKLY;BYDAY=MO,TH;COUNT=10"
//   },
//   {
//     text: "Helen 2",
//     practitionerId: "618d1b89-b512-4c1f-b9fa-e81676f2d396",
//     startDate: new Date("2021-08-03T16:30:00.000Z"),
//     endDate: new Date("2021-08-03T18:30:00.000Z"),
//   },
//   {
//     text: "Alex 3",
//     practitionerId: "991a3d9e-ebbb-4274-aed9-52c850fe91bc",
//     startDate: new Date("2021-08-15T16:30:00.000Z"),
//     endDate: new Date("2021-08-15T18:30:00.000Z"),
//   },
//   {
//     text: "Alex 4",
//     practitionerId: "991a3d9e-ebbb-4274-aed9-52c850fe91bc",
//     startDate: new Date("2021-08-02T16:30:00.000Z"),
//     endDate: new Date("2021-08-02T18:30:00.000Z"),
//   },
//   {
//     text: "Alex 5",
//     practitionerId: "618d1b89-b512-4c1f-b9fa-e81676f2d396",
//     startDate: new Date("2021-08-05T16:30:00.000Z"),
//     endDate: new Date("2021-08-05T18:30:00.000Z"),
//   },
//   {
//     text: "Stan 6",
//     practitionerId: "991a3d9e-ebbb-4274-aed9-52c850fe91bc",
//     startDate: new Date("2021-08-14T16:30:00.000Z"),
//     endDate: new Date("2021-08-14T18:30:00.000Z"),
//   },
//   {
//     text: "Stan 7",
//     practitionerId: "3f9fbefe-b98b-4aa7-8309-feaddeefcc95",
//     startDate: new Date("2021-08-02T16:30:00.000Z"),
//     endDate: new Date("2021-08-02T18:30:00.000Z"),
//   },
//   {
//     text: "Stan 8",
//     practitionerId: "3f9fbefe-b98b-4aa7-8309-feaddeefcc95",
//     startDate: new Date("2021-08-06T16:30:00.000Z"),
//     endDate: new Date("2021-08-06T18:30:00.000Z"),
//   },
//   {
//     text: "Rachel 9",
//     practitionerId: "991a3d9e-ebbb-4274-aed9-52c850fe91bc",
//     startDate: new Date("2021-08-02T16:30:00.000Z"),
//     endDate: new Date("2021-08-02T18:30:00.000Z"),
//   },
//   {
//     text: "Rachel 10",
//     practitionerId: "991a3d9e-ebbb-4274-aed9-52c850fe91bc",
//     startDate: new Date("2021-08-03T16:30:00.000Z"),
//     endDate: new Date("2021-08-03T18:30:00.000Z"),
//   },
//   {
//     text: "Rachel 11",
//     practitionerId: "3f9fbefe-b98b-4aa7-8309-feaddeefcc95",
//     startDate: new Date("2021-08-20T16:30:00.000Z"),
//     endDate: new Date("2021-08-20T18:30:00.000Z"),
//   },
//   {
//     text: "Kelly 12",
//     practitionerId: "618d1b89-b512-4c1f-b9fa-e81676f2d396",
//     startDate: new Date("2021-08-08T16:30:00.000Z"),
//     endDate: new Date("2021-08-08T18:30:00.000Z"),
//   },
//   {
//     text: "Kelly 13",
//     practitionerId: "618d1b89-b512-4c1f-b9fa-e81676f2d396",
//     startDate: new Date("2021-08-09T16:30:00.000Z"),
//     endDate: new Date("2021-08-09T18:30:00.000Z"),
//   },
// ];
// */
