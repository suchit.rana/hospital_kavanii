import {Injectable} from '@angular/core';
import {AppoinmentDateUtils} from './AppoinmentDateUtils';
import {BehaviorSubject} from 'rxjs';
import {DateUtils} from '../../../shared/DateUtils';
import {ApplicationDataModel} from '../../../models/app.settings.model';
import {ApplicationDataService} from '../../../services/app.applicationdata.service';
import {ApplicationDataEnum} from '../../../enum/application-data-enum';
import {AppState} from '../../../app.state';
import {PractitionerSpecialityModel} from '../../../models/app.staff.model';

@Injectable({
  providedIn: 'root'
})
export class SchedulerManageService {

  private _currentDate: BehaviorSubject<any> = new BehaviorSubject<any>(AppoinmentDateUtils.getCurrentDate());
  private _currentView = 'day';
  private _startDate: any = this.getCurrentDate();
  private _scheduleManage = new BehaviorSubject(null);
  private _breaksSubject = new BehaviorSubject<ApplicationDataModel[]>([]);
  private _selectedPractitioner: PractitionerSpecialityModel[] = [];
  private _displayRoom: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  selectedDateArray: string[] = [DateUtils.format(this.getCurrentDate(), 'YYYY-MM-DD')];

  breaks$ = this._breaksSubject.asObservable();

  constructor(private applicationDataService: ApplicationDataService,
              private appState: AppState) {
  }

  get scheduleManage(): BehaviorSubject<null> {
    return this._scheduleManage;
  }

  notify(data) {
    this.scheduleManage.next(data);
  }

  getCurrentDate() {
    return this._currentDate.getValue();
  }

  getCurrentView() {
    return this._currentView;
  }

  getDaysToAdd(plusMinusNum: number = 0) {
    return this.getCurrentView() == 'day' ? 1 : this.getCurrentView() == 'week' ? (7 + plusMinusNum) : (5 + plusMinusNum);
  }

  get breaksSubject(): BehaviorSubject<ApplicationDataModel[]> {
    return this._breaksSubject;
  }

  get currentDate(): BehaviorSubject<any> {
    return this._currentDate;
  }

  get startDate(): string {
    if (this.getCurrentView() != 'day') {
      // console.log(DateUtils.toUtcWithNoTime(DateUtils.getStartDateOfWeek(this.getCurrentDate())))
      return DateUtils.toUtcWithNoTime(DateUtils.getStartDateOfWeek(this.getCurrentDate()));
    }
    return DateUtils.toUtcWithNoTime(this.getCurrentDate());
  }

  get selectedPractitioner(): PractitionerSpecialityModel[] {
    return this._selectedPractitioner;
  }

  set selectedPractitioner(value: PractitionerSpecialityModel[]) {
    this._selectedPractitioner = value;
  }

  set startDate(value) {
    this._startDate = value;
  }

  set currentView(value: string) {
    this._currentView = value;
    this.createSelectedDateArray();
  }

  getBreaks(isForece?: boolean) {
    if (this._breaksSubject.getValue().length <= 0 || isForece) {
      this.applicationDataService.GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.BreakName, this.appState.selectedUserLocationId).subscribe(response => {
        this._breaksSubject.next(response);
      });
    } else {
      this._breaksSubject.next(this._breaksSubject.getValue());
    }
  }

  setCurrentDate(date, from) {
    if (AppoinmentDateUtils.isEqual(date, this.currentDate.getValue())) {
      return;
    }
    this.currentDate.next(date);
    this.createSelectedDateArray();
  }

  displayRoom(value: boolean) {
    this.displayRoomSubject.next(value);
  }

  get displayRoomVal(): boolean{
    return this._displayRoom.getValue();
  }

  get displayRoomSubject(): BehaviorSubject<boolean>{
    return this._displayRoom;
  }

  private createSelectedDateArray() {
    const daysToadd = this.getDaysToAdd();
    this.selectedDateArray = [];
    if (daysToadd == 1) {
      this.selectedDateArray.push(DateUtils.format(this.currentDate.getValue(), 'YYYY-MM-DD'));
      return;
    }
    const startDate = DateUtils.getStartDateOfWeek(this.getCurrentDate());
    for (let i = 0; i < daysToadd; i++) {
      this.selectedDateArray.push(DateUtils.format(AppoinmentDateUtils.addAndGetNewValueDefault(startDate, i, 'd'), 'YYYY-MM-DD'));
    }
  }
}
