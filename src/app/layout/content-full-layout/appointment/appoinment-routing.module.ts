import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppointmentComponent} from './appointment.component';


const routes: Routes = [
  { path: '', component: AppointmentComponent },
  // { path: '/schedule', component: ScheduleComponent, canActivate: [SecurityGuard] },
  // { path: '/create', component: CreateAppointmentComponent, canActivate: [SecurityGuard] },
  // { path: '/today', component: PractitionerTodayComponent, canActivate: [SecurityGuard] },
  // { path: '/room', component: RoomComponent, canActivate: [SecurityGuard] },
  // { path: '/waitlist', component: WaitListComponent, canActivate: [SecurityGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppoinmentRoutingModule { }
