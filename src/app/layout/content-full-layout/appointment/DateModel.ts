import {AppoinmentDateUtils} from './AppoinmentDateUtils';

export class DateModel {
  date: Date;
  text: string;
  id: string = '0';
  isDateTemplate: boolean = true;


  constructor(date) {
    this.date = date;
    this.id = AppoinmentDateUtils.format(date, "MMDDYYYY");
    // this.text = date.toDateString();
  }
}
