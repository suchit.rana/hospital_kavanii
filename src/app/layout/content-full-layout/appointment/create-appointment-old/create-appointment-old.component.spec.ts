import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAppointmentOldComponent } from './create-appointment-old.component';

describe('CreateAppointmentOldComponent', () => {
  let component: CreateAppointmentOldComponent;
  let fixture: ComponentFixture<CreateAppointmentOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAppointmentOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAppointmentOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
