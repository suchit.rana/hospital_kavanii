// import { Component, EventEmitter, OnInit, Output } from "@angular/core";
// import { AppState } from "src/app/app.state";
// import { MenuModel } from "src/app/models/app.menu.model";
// import { Router } from "@angular/router";
//
// @Component({
//   selector: "app-appointment-header-old",
//   templateUrl: "./appointment-header-old.component.html",
//   styleUrls: ["./appointment-header-old.component.css"],
// })
// export class AppointmentHeaderOldComponent implements OnInit {
//   @Output()
//   onActiveChanged: EventEmitter<MenuModel> = new EventEmitter<MenuModel>();
//   @Output()
//   onCreateClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
//   @Output()
//   onSwitchClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
//
//   constructor(public appState: AppState, private router: Router) {}
//
//   // {name: 'Create Appt', link:'/appointment', icon: '', hover: '', show: false, enabled: true },
//
//   appointmentLinks: MenuModel[] = [
//     {
//       name: "Practitioner Today",
//       link: "/appointment/today",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Schedule",
//       link: "/appointment/schedule",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Wait List",
//       link: "/appointment/waitlist",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//     {
//       name: "Rooms",
//       link: "/appointment/room",
//       icon: "",
//       hover: "",
//       show: false,
//       enabled: true,
//     },
//   ];
//
//   downloads = [
//     { name: "Print Schedule", icon: "print" },
//     { name: "Appointment List View", icon: "menu" },
//     { name: "Practitioner Avaiability", icon: "check" },
//     { name: "Transfer Appointment", icon: "pulldown" },
//     { name: "Settings", icon: "preferences" },
//   ];
//
//   todayList = [
//     { id: 1, name: "1 Week" },
//     { id: 2, name: "2 Week" },
//     { id: 3, name: "3 Week" },
//     { id: 4, name: "4 Week" },
//     { id: 5, name: "6 Week" },
//     { id: 6, name: "8 Week" },
//     { id: 7, name: "10 Week" },
//     { id: 8, name: "1 Month" },
//     { id: 9, name: "2 Months" },
//     { id: 10, name: "3 Months" },
//     { id: 11, name: "6 Months" },
//     { id: 12, name: "12 Months" },
//   ];
//
//   public defaultItem: { name: string; id: number } = {
//     name: "Today",
//     id: null,
//   };
//   ngOnInit() {}
//
//   onCreateAppointmentClick(event: MouseEvent) {
//     event.preventDefault();
//     this.onCreateClicked.emit(true);
//   }
//
//   onMenuClick(event: MouseEvent, menu: MenuModel) {
//     if (event) {
//       event.preventDefault();
//     }
//     menu.show = !menu.show;
//
//     if (menu.name == "Schedule") {
//       // let r = this.appointmentLinks.find(a => a.name == "Rooms");
//       // r.show = false;
//       // let wl = this.appointmentLinks.find(a => a.name == "Wait List");
//       // wl.show = false;
//       //this.hideAllMenuExcept("Schedule");
//       this.hideMenu("Rooms");
//       this.hideMenu("Wait List");
//
//       this.showMenu("Schedule");
//     }
//
//     if (menu.name == "Rooms") {
//       this.hideAllMenuExcept(menu.name);
//     }
//
//     if (menu.name == "Wait List") {
//       this.hideAllMenuExcept(menu.name);
//     }
//
//     this.onActiveChanged.emit(menu);
//   }
//
//   hideMenu(name: string) {
//     let m = this.appointmentLinks.find((a) => a.name == name);
//     m.show = false;
//   }
//
//   showMenu(name: string) {
//     let m = this.appointmentLinks.find((a) => a.name == name);
//     m.show = true;
//   }
//
//   hideAllMenuExcept(name: string) {
//     this.appointmentLinks
//       .filter((a) => a.name != name)
//       .forEach((a) => {
//         a.show = false;
//       });
//   }
//
//   telerikSchedulerChange($event) {
//     this.onSwitchClicked.emit($event);
//   }
//
//   onlyEnableFor(name: string) {
//     this.appointmentLinks.forEach((a) => (a.enabled = false));
//     let a = this.appointmentLinks.find((a) => a.name == name);
//     a.enabled = true;
//   }
// }
