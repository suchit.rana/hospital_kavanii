import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentHeaderOldComponent } from './appointment-header-old.component';

describe('AppointmentHeaderComponent', () => {
  let component: AppointmentHeaderOldComponent;
  let fixture: ComponentFixture<AppointmentHeaderOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentHeaderOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentHeaderOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
