import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Constant} from '../../../../../../shared/Constant';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {map, startWith} from 'rxjs/operators';
import {StaffService} from '../../../../../../services/app.staff.service';
import {AppState} from '../../../../../../app.state';
import {BreakpointObserver, MediaMatcher} from '@angular/cdk/layout';
import {AppointmentService} from '../../../../../../services/app.appointment.service';
import {SchedulerManageService} from '../../../scheduler-manage.service';
import {Appointment, ScheduleCheck} from '../../../../../../models/app.appointment.model';
import {BehaviorSubject} from 'rxjs';
import {moveItemInArray} from '@angular/cdk/drag-drop';
import {AppointmentHandlerService} from '../service/appointment-handler.service';
import {SettingsService} from "../../../../../../services/app.settings.service";
import {ApptTreatmentRoomModel} from '../../../../../../models/app.settings.model';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RoomListComponent implements OnInit, OnDestroy {
  static readonly NO_PRACTITIONER: number = 0;
  static readonly NO_SELECTED_PRACTITIONER: number = 1;

  @Input()
  isCustomSortEnabled = false;
  @Input()
  dataSource: Appointment[] = [];
  @Input()
  initFromHistory = false;
  @Input()
  fromDashboard = false;
  @Output()
  resourceLoaded: EventEmitter<ApptTreatmentRoomModel[]> = new EventEmitter();
  @Output()
  togglePanel: EventEmitter<boolean> = new EventEmitter();
  @Output()
  isEmptyPractitioner: EventEmitter<number> = new EventEmitter();

  // CONSTANT VARIABLE
  readonly addToSdlr = Constant.ADD_TO_SDLR;

  resource: ApptTreatmentRoomModel[] = [];
  // POPUP DISPLAY CONTROL
  opened = false;

  isExpanded = true;
  isDiplayPanelUI = true;
  practionerSearchFormCtrl: FormControl = new FormControl('');
  selectedRoomsFilter = Constant.ACTIVE;

  roomsList: ApptTreatmentRoomModel[] = [];
  filteredRoomsList: ApptTreatmentRoomModel[] = [];
  filteredRoomsListDisplay: ApptTreatmentRoomModel[] = [];
  haveRoomAppointment: ScheduleCheck[] = [];

  // POPUP ACTION LISTS
  practitionerFilterList = [
    {text: 'All Rooms', value: Constant.ALL},
    {text: 'Active Rooms', value: Constant.ACTIVE},
    {text: 'Inactive Rooms', value: Constant.INACTIVE}
  ];

  private isAddedRoom = false;
  _subscribes: any[] = [];
  _scheduleAvailabilitySubscribes: BehaviorSubject<boolean>[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appState: AppState,
    private staffService: StaffService,
    private appointmentService: AppointmentService,
    private media: MediaMatcher,
    private breakPoint: BreakpointObserver,
    private schedulerManageService: SchedulerManageService,
    private apptHandler: AppointmentHandlerService,
    private settingsService: SettingsService
  ) {
    this.breakPoint.observe(['(min-width: 992px)']).subscribe(state => {
      this.togglePractitionerPanel(true, state.matches);
    });

    let params = this.route.snapshot.queryParams;
    if (params['addedNewPtt']) {
      this.isAddedRoom = true;
    }
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy(): void {
    this._subscribes.forEach((value) => {
      value.unsubscribe();
    });
  }

  togglePractitionerPanel(isForceFully?: boolean, value?: boolean) {
    this.isDiplayPanelUI = false;
    if (isForceFully) {
      this.isExpanded = value;
      this.togglePanel.emit(this.isExpanded);
      this.setPanelUIDisplay(this.isExpanded);
      return;
    }
    this.isExpanded = !this.isExpanded;
    this.togglePanel.emit(this.isExpanded);
    this.setPanelUIDisplay(this.isExpanded);
  }

  toggleFilter() {
    this.opened = !this.opened;
  }

  practitionerFilterChanged(value: string) {
    this.toggleFilter();
    this.selectedRoomsFilter = value;
    this._setDataToResource(this.filterPractitionerList(), false, 'ACTIVE/INACTIVE FILTER');
  }

  loadData() {
    this.listPractitioner();
    this.searchPractitioner();
  }

  listPractitioner() {
    this.getPractitioner();
    let sub = this.appState.selectedUserLocationIdState.subscribe(c => {
      if (this._scheduleAvailabilitySubscribes.length > 0) {
        this._scheduleAvailabilitySubscribes.forEach(value => {
          if (value) {
            value.unsubscribe();
          }
        });
        this._scheduleAvailabilitySubscribes = [];
      }
      this.resource = this.filteredRoomsList = this.filteredRoomsListDisplay = this.filteredRoomsListDisplay = this.roomsList = [];
      this.getPractitioner();
    });
    this._subscribes.push(sub);
  }

  addRemoveRoom(value: ApptTreatmentRoomModel) {
    setTimeout(() => {
      value.selected = !value.selected;
      let res = this.resource;
      if (!value.selected) {
        this.resource = res.filter(item => item.id !== value.id);
      } else if (value.selected) {
        this.resource = [...this.resource, value];
      }
      this.notifyResourceReadey(this.resource, 'FILTER ACTION');
    }, 200);
  }

  private checkSchedulesOnDateChange() {
    // let onComplete: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    // this._scheduleAvailabilitySubscribes.push(onComplete);
    // onComplete.subscribe(val => {
    //   if (val) {
    //     this.filteredRoomsList.forEach(resource => {
    //       // const isSelected = this.initFromHistory && this.apptHandler.schedulerState.practitioners && this.apptHandler.schedulerState.practitioners.findIndex(x => x.practitionerId == resource.id) > -1
    //       // resource.selected = this.checkScheduleAvailable(resource.id) || isSelected;
    //       resource.selected = this.checkScheduleAvailable(resource.id);
    //     });
    //     this._setDataToResource(this.filteredRoomsList, true, 'CHECK SCHEDULE');
    //   }
    // });
    this.schedulerManageService.currentDate.subscribe(value => {
      // console.log("PLIST DATE SUBSCRIBE")
      // this.checkPractitionerSchedulr(onComplete);
      this.filteredRoomsList.forEach(resource => {
        // const isSelected = this.initFromHistory && this.apptHandler.schedulerState.practitioners && this.apptHandler.schedulerState.practitioners.findIndex(x => x.practitionerId == resource.id) > -1
        // resource.selected = this.checkScheduleAvailable(resource.id) || isSelected;
        resource.selected = this.checkScheduleAvailable(resource.id);
      });
      this._setDataToResource(this.filteredRoomsList, true, 'CHECK SCHEDULE');
    });
  }

  private checkPractitionerSchedulr(onComplete?: BehaviorSubject<boolean>) {
    let  defaultPractitionerToSchedule:any;
    if (this.filteredRoomsListDisplay.length > 0) {
      defaultPractitionerToSchedule= this.filteredRoomsListDisplay[0].id
    }

    this.appointmentService.getPractitionerSchedules(this.appState.selectedUserLocationId, this.schedulerManageService.startDate, this.schedulerManageService.getDaysToAdd())
      .subscribe((response) => {
        if (response) {
          this.haveRoomAppointment = response;

          if (this.haveRoomAppointment.length > 0) {
           const practitionersWithOutSchedule =   this.haveRoomAppointment.filter(f=>f.isSchduled == false);
           if (practitionersWithOutSchedule.length > 0) {
             // Pick any one
            defaultPractitionerToSchedule = practitionersWithOutSchedule[0].practitionerId;
           }
          }
        }
        // console.log('GET SCHEDULE AVAILABILITY');
        if (onComplete) {
          onComplete.next(true);
        }

        if (this.fromDashboard) {
          let index=  this.filteredRoomsListDisplay.findIndex(obj=>obj.id == defaultPractitionerToSchedule);
          this.filteredRoomsListDisplay[index].selected = false;
          this.onPractitionerListAction(this.filteredRoomsListDisplay[index],this.addToSdlr);
        }

      }, () => {
        if (onComplete) {
          onComplete.unsubscribe();
        }
      });
  }

  private getPractitioner() {
    this._listPractitioner();
  }

  private _listPractitioner() {
    this.settingsService.getAllTreatmentRooms().subscribe(response => {
      const data = response.filter(treatmentRoom => treatmentRoom.isAllowAllLocation || treatmentRoom.treatmentLocation.filter(location => location.locationId === this.appState.selectedUserLocationId).length > 0) as unknown as ApptTreatmentRoomModel[];
      this.isAddedRoom = this.isAddedRoom && response.length == 1;
      data.forEach(value => {
        value.displayName = value.treatmentType;
        value.selected = this.checkScheduleAvailable(value.id) && value.isStatus;
        value.practitionerName = value.treatmentType;
      });
      this.roomsList = data;
      // this.filterPractitionerList();
      this._setDataToResource(this.filterPractitionerList(), false, 'FIRST API CALL');
      this.checkSchedulesOnDateChange();
    });
  }

  searchPractitioner() {
    this.practionerSearchFormCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterPractitioner(value))
    ).subscribe();
  }

  filterPractitionerList(): ApptTreatmentRoomModel[] {
    let filteredPractionerList: ApptTreatmentRoomModel[] = [];
    let index = 0;
    if (this.selectedRoomsFilter == Constant.ALL) {
      filteredPractionerList = this.roomsList;
      if (this.initFromHistory && this.apptHandler.schedulerState.practitioners) {
        this.apptHandler.schedulerState.practitioners.forEach(value => {
          const hasFound = filteredPractionerList.find(x => x.id == value.practitionerId);
          if (hasFound) {
            hasFound.selected = true;
          }
        });
      }
      return filteredPractionerList;
    }
    this.roomsList.forEach(value => {
      value.index = index;
      if (this.selectedRoomsFilter == Constant.ACTIVE && value.isStatus) {
        filteredPractionerList.push(value);
      } else if (this.selectedRoomsFilter == Constant.INACTIVE && !value.isStatus) {
        filteredPractionerList.push(value);
      }
      index++;
    });
    if (this.initFromHistory && this.apptHandler.schedulerState.practitioners) {
      this.apptHandler.schedulerState.practitioners.forEach(value => {
        const hasFound = filteredPractionerList.find(x => x.id == value.practitionerId);
        if (hasFound) {
          hasFound.selected = true;
        }
      });
    }
    if (filteredPractionerList.length != 0) {
      this.practionerSearchFormCtrl.enable();
    } else {
      this.practionerSearchFormCtrl.disable();
    }

    return filteredPractionerList;
  }

  onPractitionerListAction(value: ApptTreatmentRoomModel, action) {
    setTimeout(() => {
      value.selected = !value.selected;
      if (action == this.addToSdlr) {
        this.resource = [...this.resource, value];
      }
      this.notifyResourceReadey(this.resource, 'FILTER ACTION');
    }, 200);
  }

  resetResourceNormal() {
    this.notifyResourceReadey(this.resource, 'RESER NORMAL');
  }

  private notifyResourceReadey(data: ApptTreatmentRoomModel[], from) {
    // console.log('NOTIFY RESOURCE', from);
    this.resourceLoaded.emit(data);
    this.isEmptyPractitioner.emit((this.roomsList.length == 0 || this.filteredRoomsList.length == 0) ? RoomListComponent.NO_PRACTITIONER : (this.resource.length == 0 ? RoomListComponent.NO_SELECTED_PRACTITIONER : -1));
  }

  private _filterPractitioner(query: string) {
    this.filteredRoomsListDisplay = this.filteredRoomsList.filter(value => value.practitionerName.toLocaleLowerCase().includes(query.toLocaleLowerCase()));
  }

  private static getSelectedOnly(filteredList: ApptTreatmentRoomModel[]): ApptTreatmentRoomModel[] {
    return filteredList.filter(value => value.selected);
  }

  private _setDataToResource(filteredList: ApptTreatmentRoomModel[], isNotify: boolean = true, from) {
    this.resource = JSON.parse(JSON.stringify(RoomListComponent.getSelectedOnly(filteredList)));
    this.filteredRoomsList = filteredList;
    this.filteredRoomsListDisplay = JSON.parse(JSON.stringify(filteredList));
    if (isNotify) {
      // console.log('SET DATASOURCE', this.resource, this.filteredPractionerList, this.filteredPractionerListDisplay);
      this.notifyResourceReadey(this.resource, from);
    }
  }

  private setPanelUIDisplay(val: boolean) {
    setTimeout(() => {
      this.isDiplayPanelUI = val;
    });
  }

  private checkScheduleAvailable(treatmentRoomId: string): boolean {
    if (!this.dataSource) {
      return false;
    }
    let data: Appointment[] = this.dataSource.filter(value => value.treatmentRoomId == treatmentRoomId && AppoinmentDateUtils.isEqual(value.startDateUTC, this.schedulerManageService.getCurrentDate()));
console.log(data)
    return data && data.length > 0;
  }

}
