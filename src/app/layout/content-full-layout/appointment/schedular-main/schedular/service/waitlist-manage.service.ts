import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';
import {Appointment} from '../../../../../../models/app.appointment.model';
import {WaitList} from '../interface/wait-list';

@Injectable()
export class WaitlistManageService {

  private _waitListApptData: BehaviorSubject<Appointment> = new BehaviorSubject<Appointment>(null);
  private _bookAppointmentSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  private _waitList: WaitList;

  constructor() {
  }

  get waitListApptData(): BehaviorSubject<Appointment> {
    return this._waitListApptData;
  }

  get bookAppointmentSubject(): BehaviorSubject<boolean> {
    return this._bookAppointmentSubject;
  }

  get bookAppointment(): boolean {
    return this.bookAppointmentSubject.getValue();
  }

  get waitList(): WaitList {
    return this._waitList;
  }

  set waitList(value: WaitList) {
    this._waitList = value;
  }
}
