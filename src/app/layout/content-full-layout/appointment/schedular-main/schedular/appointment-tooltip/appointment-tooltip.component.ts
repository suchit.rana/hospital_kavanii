import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Appointment} from '../../../../../../models/app.appointment.model';
import {
  AppAppointmentTooltipService,
  BubbleAppointment,
  BubblePatient,
  BubbleStatics
} from '../rest-service/app.appointment-tooltip.service';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {IPatientAppointmentRecurringData} from '../../../../patients/rest-service/app.patient-appointment.service';
import {HeadernavComponent} from '../tooltip-Templat/headernav/headernav.component';
import {FooterNavComponent} from '../tooltip-Templat/footer-nav/footer-nav.component';
import {AppointmentHandlerService} from '../service/appointment-handler.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-appointment-tooltip',
  templateUrl: './appointment-tooltip.component.html',
  styleUrls: ['./appointment-tooltip.component.scss']
})
export class AppointmentTooltipComponent implements OnInit, OnChanges {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  appointmentClicked = '1';
  @Input()
  data: Appointment;
  @Input()
  mainAppt: Appointment;
  @Input()
  practitionerDetails: PractitionerSpecialityModel;

  patient: BubblePatient = {} as BubblePatient;
  apptDetails: BubbleAppointment = {} as BubbleAppointment;
  bubbleStatics: BubbleStatics = {} as BubbleStatics;
  recurringData: IPatientAppointmentRecurringData = {} as IPatientAppointmentRecurringData;
  patientOld = '';
  patientDobString = '';
  isSendEmailDialog = false;
  loading = false;

  @ViewChild(HeadernavComponent, {static: false}) headerNacComponent: HeadernavComponent;
  @ViewChild(FooterNavComponent, {static: false}) footerNacComponent: FooterNavComponent;

  constructor(
    private appointmentTooltipService: AppAppointmentTooltipService,
    private apptHandler: AppointmentHandlerService,
    private alertService: AppAlertService,
  ) {
  }

  ngOnInit() {
    this.prepareRecurringData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data') && this.data) {
      this.loading = true;
      this.appointmentTooltipService.getTooltipData(this.data.id, AppoinmentDateUtils.getDateWithTz(this.data.startDateUTC)).subscribe(response => {
        this.loading = false;
        response.bubblePatient.patientPhoto = AppUtils.prepareBase64String(response.bubblePatient.patientPhoto);
        this.patient = response.bubblePatient;
        this.apptDetails = response.bubbleAppointment;
        this.bubbleStatics = response.bubbleStatistics;

        if (this.patient.dob) {
          const diff = AppoinmentDateUtils.getDiff(this.patient.dob, new Date(), 'months');
          if (diff < 12) {
            this.patientOld = diff + ' Month';
          } else {
            this.patientOld = parseInt((diff / 12).toString()) + ' Yrs';
          }
          this.patientDobString = AppoinmentDateUtils.format(this.patient.dob, 'DD MMM YYYY');
        } else {
          this.patientOld = null;
          this.patientDobString = null;
        }
      }, error => {
        this.loading = false;
        this.apptHandler.hideTooltip();
        this.alertService.error(error || "Failed to fetch data. Try Again.");
      });
    }
  }

  popupBtnClick($event, from: string) {
    if (['1', '2', '3', '4', '6', '7'].indexOf($event) >= 0) {
      this.appointmentClicked = $event;
    }
    if (from == 'FOOTER') {
      this.headerNacComponent.resetSelect();
    } else if (from == 'HEADER') {
      this.footerNacComponent.resetSelect();
    }
  }

  toggleSendEmailDialog(value: boolean) {
    this.isSendEmailDialog = value;
  }

  private prepareRecurringData() {
    this.recurringData.appointmentsId = this.data.id;
    this.recurringData.recurrenceRule = this.mainAppt.recurrenceRule;
    this.recurringData.recurrenceException = this.mainAppt.recurrenceException;
    this.recurringData.startDateUTC = this.mainAppt.startDateUTC;
    this.recurringData.endDateUTC = this.mainAppt.endDateUTC;
    this.recurringData.status = this.data.status;
    this.recurringData.type = this.data.type;
  }
}
