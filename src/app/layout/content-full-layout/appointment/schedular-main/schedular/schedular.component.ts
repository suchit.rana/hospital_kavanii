import { Component, ComponentRef, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AppoinmentDateUtils } from '../../AppoinmentDateUtils';
import { DxSchedulerComponent } from 'devextreme-angular/ui/scheduler';
import { PractitionerSpecialityModel } from '../../../../../models/app.staff.model';
import { AppState } from '../../../../../app.state';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { BusinessService } from '../../../../../services/app.business.service';
import { DateModel } from '../../DateModel';
import { SchedularHeaderComponent } from './schedular-header/schedular-header.component';
import { PractitionerListComponent } from './practitioner-list/practitioner-list.component';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AppUtils } from '../../../../../shared/AppUtils';
import { Constant } from '../../../../../shared/Constant';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Appointment, AppointmentEntry, Break, Schedule, TreatmentType } from '../../../../../models/app.appointment.model';
import { AppointmentType, Frequency, ScheduleMode } from '../../../../../enum/application-data-enum';
import { SchedulerManageService } from '../../scheduler-manage.service';
import { AppointmentService } from '../../../../../services/app.appointment.service';
import { TooltipDirective } from '@progress/kendo-angular-tooltip';
import * as moment from 'moment';
import { BaseItemComponent } from '../../../../../shared/base-item/base-item.component';
import { Location } from '@angular/common';
import { DateUtils } from '../../../../../shared/DateUtils';
import { DxPopoverComponent } from 'devextreme-angular';
import { RRule, rrulestr } from 'rrule';
import { SchedulerSettingsEnum } from '../../../../../enum/scheduler-settings-enum';
import { SchedulerSettings } from '../../../../../models/scheduler-settings';
import { SchedulerSettingsService } from '../../../../../services/app.scheduler-settings.service';
import { SchedulerSettingsPopupComponent } from './popup-component/scheduler-settings-popup/scheduler-settings-popup.component';
import { MatDialog } from '@angular/material';
import { SchedulerGroupByenum } from '../../../../../enum/scheduler-group-byenum';
import { SchedulerViewEnum } from '../../../../../enum/scheduler-view-enum';
import { AppSchedulerHolidayListService } from '../../../../../services/app.scheduler-holiday-list.service';
import { PatientSearchComponent } from './shared/patient-search/patient-search.component';
import { MessageType } from '../../../../../models/app.misc';
import { PopupService } from '@progress/kendo-angular-popup';
import { CaseContactType, PatientModel } from '../../../../../models/app.patient.model';
import invert from 'invert-color';
import { positionConfig } from 'devextreme/animation/position';
import { AppointmentHandlerService, MOVE, NONE, REBOOK } from './service/appointment-handler.service';
import { AppointmentConfirmationStatus, AppointmentStatus, LetterTreatmentStatus } from './rest-service/app.appointment-tooltip.service';
import { AppAlertService } from '../../../../../shared/services/app-alert.service';
import { AlertComponent } from '../../../../../shared/alert/alert.component';
import { AddEditAppointmentUtils } from './shared/utils/AddEditAppointmentUtils';
import { AppDialogService } from '../../../../../shared/services/app-dialog.service';
import { formatREString } from '../../../../../app.component';
import { PatientAppointmentService } from '../../../patients/rest-service/app.patient-appointment.service';
import { PatientAlertDisplayComponent } from './popup-component/patient-alert-display/patient-alert-display.component';
import { For, FromPage, RouterState } from '../../../../../shared/interface';
import { WaitlistManageService } from './service/waitlist-manage.service';
import { PatientService } from '../../../../../services/app.patient.service';
import { WaitList } from './interface/wait-list';
import { WaitListService } from './rest-service/wait-list.service';
import { ApptTreatmentRoomModel } from '../../../../../models/app.settings.model';
import { PractitionerAvailabilityComponent } from './popup-component/practitioner-availability/practitioner-availability.component';
import { RP_MODULE_MAP } from '../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
    selector: 'app-schedular',
    templateUrl: './schedular.component.html',
    styleUrls: ['./schedular.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SchedularComponent extends BaseItemComponent implements OnInit, OnDestroy {
    readonly APPT_SCHEDULE = RP_MODULE_MAP.appointment_schedule;
    readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

    STATUS_PENDING = AppointmentStatus.Pending;
    STATUS_COMPLETED = AppointmentStatus.Completed;
    STATUS_MISSED = AppointmentStatus.Missed;
    STATUS_CANCELLED = AppointmentStatus.Cancelled;
    STATUS_DELETED = AppointmentStatus.Deleted;
    // Default value
    active = 1;
    appointmentClicked = '1';


    readonly STATUS_DRAFT = LetterTreatmentStatus.Draft;
    readonly STATUS_FINALISE = LetterTreatmentStatus.Finalise;

    readonly DEF_TIMEZONE: string = 'Australia/Sydney';
    readonly CELL_DURATION: number = 15;
    readonly TIME_SLOAT_SIZE: string = 'default';
    readonly CAL_COLOR: string = '#ffffff';
    readonly START_TIME: number = 9;
    readonly END_TIME: number = 17;

    readonly SCHEDULE_MODE_EDIT: ScheduleMode = ScheduleMode.EDIT;

    readonly SCHEDULE: string = Constant.SCHEDULE;

    readonly ACB_APPT: string = 'ACB_APPT';
    readonly ACB_CLASS: string = 'ACB_CLASS';
    readonly ACB_BREAK: string = 'ACB_BREAK';

    readonly TYPE_SCHEDULE = AppointmentType.SCHEDULE;
    readonly TYPE_BREAK = AppointmentType.BREAK;
    readonly TYPE_APPT = AppointmentType.APPOINMENT;
    readonly TYPE_HOLIDAY = AppointmentType.Holiday;

    @BlockUI() blockUI: NgBlockUI;

    guideTitle: string = '';
    guideMsg: string = '';
    guideUrl: string = '';
    guideUrlTitle: string = '';
    guideQP = { relative: true, url: 'appointment' };
    routerState: RouterState;

    schedulerSettings: SchedulerSettings;
    selectedHeaderButton = null;
    holidayList: any[] = [];

    isSdlrReady: boolean = false;
    isDaysHeaderAdded: boolean = false;

    // ADD/EDIT SCHEDULE, BREAK, APPOINTMENT POPUP
    isDisplayAddEditSchedule: boolean = false;
    isDisplayAddEditApptPopup: boolean = false;
    isDisplayWaitListPopup: boolean = false;
    isWaitListPopup: boolean = false;

    //SCHEDULER EDITING VARIABLES
    isAddEnabled: boolean = false;
    isEditEnabled: boolean = false;
    isDeleteEnabled: boolean = false;

    // VARIABLES FOR EMPTY PRACTITIONER
    isNoPractitioner: boolean = true;
    // VARIABLES FOR EMPTY TREATMENT ROOM
    isNoTreatmentRoom: boolean = false;

    isFirst: boolean = true;
    isSecondTypeGroup: boolean = true;
    repaintAfterResize: boolean = false;

    // CALENDER CONFIGURATION
    timeZone: string = this.DEF_TIMEZONE;
    startTime: number = this.START_TIME;
    endTime: number = this.END_TIME;
    cellDuration: number = this.CELL_DURATION;
    timeSlotSize: string = this.TIME_SLOAT_SIZE;
    calendarColor: string = this.CAL_COLOR;
    isDoubleBooking: boolean;
    isPatientOnlineBooking: boolean;
    isAllowBookingOutsideScheduleHours: boolean;
    isFromDummyCell: boolean = false;
    isShowWaitRoom: boolean;
    isErorrOnMoveRebookAppt = false;

    isOpenWaitingRoomTry: boolean = false;


    /* CALENDAER OPTIONS FOR POPUP */
    public calendarPopUpMargin = { horizontal: -60, vertical: 10 };

    // Variables for practitioner static popup
    totalScheduleHour: string = '0 hrs';

    @ViewChild('practitionerListComponent', { static: false }) public practitionerListComponent: PractitionerListComponent;
    @ViewChild('headerComponent', { static: false }) public headerComponent: SchedularHeaderComponent;
    @ViewChild('schedularWrapper', { static: true }) public schedularWrapper: ElementRef;
    @ViewChild('dateDayViewWrapper', { static: true }) public dateDayViewWrapper: ElementRef;
    @ViewChild(DxSchedulerComponent, { static: false }) scheduler: DxSchedulerComponent;
    @ViewChild('pttrStaticsPopOver', { static: false }) pttrStaticsPopOver: DxPopoverComponent;
    @ViewChild('appCellPopover', { static: false }) appCellPopover: ElementRef;
    @ViewChild('acbPopupPatientSearch', { static: false }) acbPopupPatientSearchCmp: PatientSearchComponent;
    @ViewChild('apptTooltipPopop', { static: false }) apptTooltipPopop: DxPopoverComponent;
    @ViewChild('appointmentConflict', { static: false }) appointmentConflict;
    @ViewChild('waitListField', { static: false }) waitListField: ElementRef;

    @Input() fromDashboard: boolean;

    appCellPopoverOpen: boolean = false;
    selectedDate: DateModel[] = [];
    data: any[] = [];
    practitioners: PractitionerSpecialityModel[] = [];
    resource: PractitionerSpecialityModel[] = [];
    resourceTr: ApptTreatmentRoomModel[] = [];
    practitionerResource: PractitionerSpecialityModel[] = [];
    currentView = 'day';
    currentViewT = this.currentView;
    currentDate = AppoinmentDateUtils.getCurrentDate();
    height: any = 500;
    isDisplayAllDayPanel: boolean = true;

    // VARIABLE FOR PRACTIONERS
    panelSize: string = '246px';

    opened: boolean = false;

    // ADD/EDIT SCHEDULE FORM
    addEditScheduleData: any;
    targetAddEditScheduleData: any = {};
    _targetAddEditScheduleData: any;
    selectedPatient = null;
    practitionerDetails = null;
    rebookMoveOldAppt: any;

    private sdlrColWidth = 0;
    private rowsPerHour: number = 60 / this.cellDuration;

    dataSource: Schedule[] = [];
    schedules: Schedule[] = [];
    breaks: Schedule[] = [];
    _subscribes: any[] = [];
    isACBPopoverFocused: boolean = true;
    PatientAdded: any;
    patientAddedData: PatientModel;
    stateValues: any;
    SearchTextPatient: any;
    AppointmentTypeAdded: any;
    appointments: Appointment[];
    routeApptId: string;
    routeDate: Date;
    rebookAlert: ComponentRef<AlertComponent>;
    waitListApptBookNowAlert: ComponentRef<AlertComponent>;
    appointment: Appointment;
    initFromHistory: boolean = false;
    waitingRoomAppts: Appointment[] = [];
    waitingRoomCount: number = 0;

    /** WAIT LIST */
    waitList: WaitList[] = [];
    waitListFiltered: WaitList[] = [];

    isDisplayRoom = false;

    constructor(
        public location: Location,
        private dialog: MatDialog,
        protected router: Router,
        private businessService: BusinessService,
        protected appState: AppState,
        private breakPoint: BreakpointObserver,
        public manageScheduleService: SchedulerManageService,
        private apptService: AppointmentService,
        private schedulerSettingsService: SchedulerSettingsService,
        private holidayListService: AppSchedulerHolidayListService,
        private _router: Router,
        private popupService: PopupService,
        private apptHandlar: AppointmentHandlerService,
        private activatedRoute: ActivatedRoute,
        private alertService: AppAlertService,
        private dialogService: AppDialogService,
        private appointmentService: AppointmentService,
        private patientAppointmentService: PatientAppointmentService,
        private waitListService: WaitListService,
        private waitlistManageService: WaitlistManageService,
        private patientService: PatientService
    ) {
        super(location, router);
        if (this.router.getCurrentNavigation().extras.state != null) {
            this.PatientAdded = this.router.getCurrentNavigation().extras.state.PatientAdded;
            this.patientAddedData = this.router.getCurrentNavigation().extras.state.data;
            this.AppointmentTypeAdded = this.router.getCurrentNavigation().extras.state.AppointmentTypeAdded;
            this.stateValues = (this.PatientAdded || this.AppointmentTypeAdded) ? JSON.parse(this.router.getCurrentNavigation().extras.state.stateValues) : null;
            const appointmentData = this.router.getCurrentNavigation().extras.state.appointmentData;
            const data = this.router.getCurrentNavigation().extras.state.data;
            this.initFromHistory = data ? data.initFromHistory : false;

            if (appointmentData) {
                this.routeApptId = appointmentData.apptId;
                this.routeDate = appointmentData.date;
            }
            if (this.initFromHistory) {
                this.routeDate = this.apptHandlar.schedulerState.currentDate;
            }
        } else {
            this.PatientAdded = history.state.PatientAdded;
            this.AppointmentTypeAdded = history.state.AppointmentTypeAdded;
            this.stateValues = (this.PatientAdded || this.AppointmentTypeAdded) ? JSON.parse(history.state.stateValues) : null;
            const appointmentData = history.state.appointmentData;
            const data = history.state.data;
            this.initFromHistory = data ? data.initFromHistory : false;

            if (appointmentData) {
                this.routeApptId = appointmentData.apptId;
                this.routeDate = appointmentData.date;
            }

            if (this.initFromHistory) {
                this.routeDate = this.apptHandlar.schedulerState.currentDate;
            }
        }

        this.blockUI.start();
        this.breakPoint.observe(['(max-width: 992px)']).subscribe(state => {
            this.togglePractitionerPanel(!state.matches);
            this.renderSchedular();
        });
    }

    @ViewChild(TooltipDirective, { static: false }) public tooltipDir: TooltipDirective;

    @HostListener('document:click', ['$event'])
    public documentClick(event: any): void {
        if (!this.isACBPopoverFocused) {
            this.closeACBPopover();
        }
    }

    public showTooltip(e: MouseEvent): void {
        if (!this.isShowTimeToolTip) {
            this.tooltipDir.hide();
            return;
        }
        const element = e.target as HTMLElement;
        if (element.nodeName === 'TD') {
            this.tooltipDir.toggle(element);
        } else {
            this.tooltipDir.hide();
        }
    }

    // Practitioner list method
    togglePractitionerPanel(isExpanded: boolean) {
        isExpanded ? this.panelSize = '246px' : this.panelSize = '72px';
        this.repaintAfterResize = true;
        setTimeout(() => {
            this.renderSchedular();
        }, 500);
    }

    ngOnInit() {
        this.getWaitList();
        if (this.fromDashboard) {
            // Default as Schedule , when user clicks on Add Schedule button from Dashboard page
            this.headerButtonClick(Constant.SCHEDULE);
        }
        this.getCalenderConfiguration();
        this.setSchedularHeight();

        let subscribable = this.manageScheduleService.scheduleManage.subscribe(value => {
            if (value && !this.isFirst) {
                this.saveSchedule(value);
            }
        });
        this._subscribes.push(subscribable);

        let apptAddSubs = this.apptHandlar.openApptDialogSubject.subscribe((data) => {
            if (data) {
                const practitioner = this.practitionerListComponent.filteredPractionerListDisplay.find(x => x.practitionerId == data.practitionerId);
                if (!practitioner.selected) {
                    this.practitionerListComponent.onPractitionerListAction(practitioner, Constant.ADD_TO_SDLR);
                }
                this.addEditScheduleData = data;
                this.targetAddEditScheduleData = null;
                this.apptTooltipPopop.instance.hide();
                this.isDisplayApptTooltip = false;
                this.openAddEditApptPopup();
            }
        });
        this._subscribes.push(apptAddSubs);

        let apptEditSubs = this.apptHandlar.editApptSubject.subscribe(value => {
            if (value) {
                if (this.addEditScheduleData && this.addEditScheduleData.type == this.TYPE_APPT) {
                    if (this._targetAddEditScheduleData) {
                        this._targetAddEditScheduleData.mode = this.SCHEDULE_MODE_EDIT;
                    }
                    this.apptTooltipPopop.instance.hide();
                    this.isDisplayApptTooltip = false;
                    this.openAddEditApptPopup();
                }
            }
        });
        this._subscribes.push(apptEditSubs);

        let hideTooltip = this.apptHandlar.hideApptTooltipPopup.subscribe(value => {
            if (value && this.apptTooltipPopop) {
                this.apptTooltipPopop.instance.hide();
            }
        });
        this._subscribes.push(hideTooltip);

        let openTooltip = this.apptHandlar.openApptTooltipPopup.subscribe(value => {
            if (value) {
                this.appoinmentlClick(this.apptHandlar.tooltipPopupData);
            }
        });
        this._subscribes.push(openTooltip);

        let reloAdAppt = this.apptHandlar.loadAppointment.subscribe(value => {
            if (value) {
                this.blockUI.start();
                this.apptService.getAppointmentsById(this.appState.selectedUserLocationId, value).subscribe(response => {
                    this.blockUI.stop();
                    if (response) {
                        const appt = this.getAppointmentById(value)[0];
                        this.scheduler.instance.deleteAppointment(appt);
                        this.scheduler.instance.addAppointment(new Appointment(response));
                        this.waitingRoomAppts = this.getAppointments();
                        this.calculateWaitingListCount();
                    }
                }, () => {
                    this.blockUI.stop();
                });
            }
        });
        this._subscribes.push(reloAdAppt);

        let rebookAppt = this.apptHandlar.rebookMoveAppt.subscribe(value => {
            if (value != NONE) {
                if (value == REBOOK) {
                    this.rebookAlert = this.alertService.error('To rebook this appointment, click on the available time slot. Press Esc to cancel.', false, false);
                } else if (value == MOVE) {
                    this.rebookAlert = this.alertService.error('To move this appointment, click on the available time slot. Press Esc to cancel.', false, false);
                }
            } else {
                if (this.rebookAlert) {
                    this.rebookAlert.instance.close();
                }
            }
        });
        this._subscribes.push(rebookAppt);

        let bookNowWaitList = this.waitlistManageService.bookAppointmentSubject.subscribe(value => {
            if (value) {
                this.hideWaitListPopup();
                this.waitListApptBookNowAlert = this.alertService.error('To book this wait list appointment, click on the available time slot. Press Esc to cancel.', false, false);
            } else {
                if (this.waitListApptBookNowAlert) {
                    this.waitListApptBookNowAlert.instance.close();
                }
            }
        });
        this._subscribes.push(bookNowWaitList);
    }

    ngAfterViewInit() {
        if (this.PatientAdded && this.stateValues) {
            this.isNoPractitioner = false;
            this.isFirst = false;
            this.addEditScheduleData = this.stateValues.addEditScheduleData;
            this.SearchTextPatient = this.stateValues.SearchTextPatient;
            this.selectedPatient = this.patientAddedData;
            this.selectedPatient['name'] = this.selectedPatient.firstName + ' ' + this.selectedPatient.lastName;
            this.bindResourceAfterRedirect(this.stateValues.resource);
            this.addEditScheduleData.type = AppointmentType.APPOINMENT;
            setTimeout(() => {
                this.openAddEditApptPopup();
            }, 1000);
        }
        if (this.AppointmentTypeAdded && this.stateValues) {
            // this.isNoPractitioner = false;
            // this.isFirst = false;
            this.addEditScheduleData = this.stateValues.addEditScheduleData;
            this.targetAddEditScheduleData = this.stateValues.targetAddEditScheduleData;
            this.cellDuration = this.stateValues.interval;
            this.startTime = this.stateValues.startTime;
            this.endTime = this.stateValues.endTime;
            //this.bindResourceAfterRedirect(this.stateValues.resource);
            this.openAddEditApptPopup();
        }
        // if (this.PatientAdded) {
        //   this.appCellPopover.nativeElement.style.left = this.stateValues.left;
        //   this.appCellPopover.nativeElement.style.top = this.stateValues.top;
        //   this.appCellPopover.nativeElement.classList.remove('invisible');
        // }

        if (this.isFromState()) {
            if (this.routerState.for === For.WaitList && this.routerState.fromPage === FromPage.Patient) {
                this.openWaitListPopup();
            } else if (this.routerState.for === For.TreatmentRoom && this.routerState.fromPage === FromPage.TreatmentRoom) {
                this.headerButtonClick(Constant.ROOMS);
            }
        }
    }

    ngOnDestroy(): void {
        this._subscribes.forEach((value) => {
            if (value) {
                value.unsubscribe();
            }
        });
        this.apptHandlar.notifyRebookMove(NONE);
    }

    private getCalenderConfiguration() {
        this.getConfiguration();
        this.getSchedulerSettings();
        let sub = this.appState.selectedUserLocationIdState.subscribe(c => {
            this.manageScheduleService.getBreaks(true);
            this.blockUI.start();
            this.hideAddEditPopup();
            // this.isDisplayAddEditSchedule = false;
            this.closeACBPopover();
            this.isNoPractitioner = true;
            this.isFirst = true;
            // this.headerComponent.changeView("day");
            // this.headerComponent.setCurrentDate(AppoinmentDateUtils.getCurrentDate());
            this.getConfiguration();
        });
        this._subscribes.push(sub);
    }

    private getConfiguration() {
        this.businessService.getTimeandIntervalByLocation(this.appState.selectedUserLocationId).subscribe(data => {
            if (data) {
                this.timeZone = data.ianaTimeZone;
                AppoinmentDateUtils.setTimeZone(this.timeZone);
                if (!this.routeDate) {
                    this.headerComponent.setCurrentDate(AppoinmentDateUtils.getCurrentDate());
                } else {
                    this.headerComponent.setCurrentDate(this.routeDate);
                }
                // this.dataSource = appointments;

                let appointments = [];
                this.apptService.getAllPractitionerSchedulesDetails<Schedule>(this.appState.selectedUserLocationId).subscribe((response) => {
                    this.schedules = response.map(value => new Schedule(value));
                    appointments = this.schedules;
                    // FETCH BREAKS
                    this.apptService.getAllPractitionerSchedulesDetails<Break>(this.appState.selectedUserLocationId, AppointmentType.BREAK).subscribe((response) => {
                        this.breaks = response.map(value => new Break(value));
                        appointments = appointments.concat(this.breaks);
                        this.dataSource = this.dataSource.concat(appointments);

                        // FETCH APPOINTMENTS
                        this.apptService.GetAllAppointmentsByLocation(this.appState.selectedUserLocationId).subscribe((response) => {
                            this.appointments = response.map(value => new Appointment(value));
                            appointments = appointments.concat(this.appointments);
                            this.dataSource = this.dataSource.concat(appointments);
                            if (this.routeApptId) {
                                this.appointment = appointments.find(x => x.id == this.routeApptId);
                                const practitioner = this.practitionerListComponent.filteredPractionerListDisplay.find(x => x.practitionerId == this.appointment.practitionerId);
                                if (!practitioner.selected) {
                                    this.practitionerListComponent.onPractitionerListAction(practitioner, Constant.ADD_TO_SDLR);
                                }
                                this.calculateWaitingListCount();
                            }
                        });
                    });

                });

                this.startTime = AppUtils.getIntegerDefault(data.startTime, this.START_TIME);
                this.endTime = AppUtils.getIntegerDefault(data.endTime, this.END_TIME);
                this.cellDuration = AppUtils.getIntegerDefault(data.timeSlot, this.CELL_DURATION);
                this.timeSlotSize = AppUtils.getStringDefault(data.timeSlotSize, this.TIME_SLOAT_SIZE);
                this.calendarColor = AppUtils.getStringDefault(data.calendarColor, this.CAL_COLOR);
                this.isDoubleBooking = AppUtils.getBoolean(data.isDoubleBooking);
                this.isPatientOnlineBooking = AppUtils.getBoolean(data.isPatientOnlineBooking);
                this.isAllowBookingOutsideScheduleHours = AppUtils.getBoolean(data.isAllowBookingOutsideScheduleHours);
                this.isShowWaitRoom = AppUtils.getBoolean(data.isShowWaitRoom);
                this.rowsPerHour = 60 / this.cellDuration;

                let timeDiff = this.endTime - this.startTime;
                let rows = 60 / this.cellDuration;

                this.getHolidayList();

                this.setSdlrHeight(this.schedularWrapper.nativeElement.offsetHeight - 68, rows * timeDiff * this.getCellSize);
                let rowSepratorStyle = '';
                for (let i = 1; i <= timeDiff; i++) {
                    let child = ((i * rows) + 1);
                    rowSepratorStyle += '.dx-scheduler-date-table-row:nth-child(' + child + ') .data-template,' +
                        '.dx-scheduler-time-panel .dx-scheduler-time-panel-row:nth-child(' + child + ') .time-cell-text' +
                        ' {\n' +
                        '  border-top: 1px solid #b3b3b3 !important;\n' +
                        '}';
                }

                let style = '.appoinment-sdlr .dx-scheduler-date-table-scrollable .dx-scrollable-container{\n' +
                    '      background: ' + this.calendarColor + ' !important;\n' +
                    '    }';

                this.findOrCreatStyleTag(rowSepratorStyle, 'schedularRowSeprator');
                this.findOrCreatStyleTag(style, 'schedularBackground');
            }
        });
    }

    @HostListener('window:resize')
    public detectResize(): void {
        this.setSchedularHeight();
        this.renderSchedular();
        this.scheduler.instance._refresh();
    }

    @HostListener('document:keydown.escape', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        if (this.apptHandlar.isRebookMoveAppointment) {
            this.undoLastUpdate();
            this.apptHandlar.isRebookMoveAppointment = false;
            this.rebookAlert.instance.close();
        }

        if (this.waitlistManageService.bookAppointmentSubject) {
            this.waitlistManageService.bookAppointmentSubject.next(false);
            this.waitListApptBookNowAlert.instance.close();
        }
    }

    private setSchedularHeight() {
        this.setSdlrHeight(this.schedularWrapper.nativeElement.offsetHeight - 68);
    }

    // SCHEDULAR METHOS
    onSdlrContentReady(e) {
        if (this.appointment) {
            let type = 'type_' + this.appointment.type;

            const id = DateUtils.toUtcRecurrenceException(this.routeDate) + '' + type;
            const elem = document.getElementById(id);
            if (elem) {
                this.onlyPrepareData = true;
                elem.click();
                setTimeout(() => {
                    this.onlyPrepareData = false;
                }, 200);
                this.apptHandlar.notifyEditAppt();
            }
        }
        if (this.currentView == 'day') {
            let thead = e.element.querySelector('.dx-scheduler-header-panel thead');

            if (thead) {
                let hasTr = document.getElementById('addedByDefault');
                if (hasTr) {
                    thead.removeChild(hasTr);
                }
                let tr = document.createElement('tr');
                tr.classList.add('dx-scheduler-group-row');
                tr.id = 'addedByDefault';
                for (let i = 0; i < this.getArray.length; i++) {
                    tr.innerHTML += '' +
                        '<th class="dx-scheduler-group-header ' + (i == 0 ? 'dx-scheduler-first-group-cell' : null) + '"  colspan="' + (this.isSecondTypeGroup ? (!this.isDisplayRoom ? this.resource.length : this.resourceTr.length) : 1) + '">' +
                        '<div class="dx-scheduler-group-header-content">' +
                        '<div class="ng-star-inserted dx-template-wrapper">' +
                        '<div class="date-template text-left d-flex align-items-center header-row-seperator date-template ' + (this.isSecondTypeGroup ? 'justify-content-start' : 'justify-content-center') + ' d-flex">' +
                        '<a class="hr-font" role="button">' + AppoinmentDateUtils.formatDateTimeDefault(this.manageScheduleService.getCurrentDate(), this.isSecondTypeGroup ? 'ddd, D MMM YYYY' : 'ddd, D/M', false) + '</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</th>';
                }
                if (this.isSecondTypeGroup) {
                    thead.insertBefore(tr, thead.firstChild);
                } else {
                    thead.appendChild(tr);
                }
                this.isDaysHeaderAdded = true;
            }
        }

        this.setMinutesLabel();

        //  This will solved problem in 20 min time slot not show hour label in sequence
        if (this.cellDuration == 20) {
            const timeRows: HTMLElement[] = e.element.querySelectorAll('.dx-scheduler-time-panel tbody tr');
            if (timeRows) {
                let currentTime = AppoinmentDateUtils.combineDateTime(this.currentDate, { 'hour': this.startTime, 'minute': 0, 'second': 0 });
                timeRows.forEach((value, index) => {
                    if (index % 3 == 0) {
                        let content = '<div class="time-cell-text ng-star-inserted">' +
                            '<span>' + AppoinmentDateUtils.formatDateTime(currentTime, 'h', false) + '</span>' +
                            '<sup> ' + AppoinmentDateUtils.formatDateTime(currentTime, 'A', false) + '</sup>' +
                            '</div>';
                        let div = value.getElementsByClassName('dx-template-wrapper');
                        if (div) {
                            div[0].innerHTML = content;
                        }
                        currentTime = AppoinmentDateUtils.addAndGetNewValue(currentTime, 1, 'h');
                    }
                });
            }

        }

        let wrapperWidth = this.schedularWrapper.nativeElement.offsetWidth - (parseInt(this.panelSize) + 48 + 18);
        let cols = (this.currentView == 'day' ? 1 : (this.getDaysToAdd() + 1)) * this.resource.length;
        let neededWidth = cols * ((!this.isSecondTypeGroup && this.currentView != 'day') ? 75 : 180);

        if (neededWidth <= wrapperWidth) {
            neededWidth = wrapperWidth;
        }

        this.sdlrColWidth = neededWidth / cols;
        let style = '.appoinment-sdlr .dx-scheduler-date-table, .appoinment-sdlr .dx-scheduler-header-panel, .top-date-scrollable-content{\n' +
            '      width: ' + (neededWidth + 1) + 'px !important;\n' +
            '    }';
        style += '\n.appoinment-sdlr .dx-scheduler-date-table, .appoinment-sdlr .dx-scheduler-date-table {\n' +
            '      width: ' + (neededWidth + 1) + 'px !important;\n' +
            '    }';
        style += '\n.appoinment-sdlr.all-day .dx-scheduler-date-table, .appoinment-sdlr .dx-scheduler-all-day-table {\n' +
            '      width: ' + (neededWidth) + 'px !important;\n' +
            '    }';

        this.findOrCreatStyleTag(style, 'schedularWidth');
        if (AppoinmentDateUtils.isEqual(this.currentDate, AppoinmentDateUtils.getCurrentDate())) {
            setTimeout(() => {
                let hour = parseInt(AppoinmentDateUtils.getCurrentDateFormat('HH'));
                let minute = parseInt(AppoinmentDateUtils.getCurrentDateFormat('mm'));
                if (hour > this.startTime && hour < this.endTime) {
                    this.scheduler.instance.scrollToTime(
                        (hour >= 1 ? hour - 0.5 : hour),
                        (minute > 20 ? minute - 20 : minute)
                    );
                }
            }, 100);
        }

        let elems = e.element.querySelectorAll('.dx-scheduler-header-panel .pttr-avatar-wrapper');
        if (elems && elems.length > 0) {
            let startDate = AppUtils.refrenceClone(this.manageScheduleService.startDate);
            elems.forEach((elem, index) => {
                elem.setAttribute('date', AppoinmentDateUtils.addAndGetNewValueDefault(startDate, (index + (this.manageScheduleService.getCurrentView() == 'day' ? 0 : 1)), 'd').toString());
            });
        }

        this.isSdlrReady = true;
        if (this.repaintAfterResize) {
            this.repaintAfterResize = false;
            this.renderSchedular();
        } else {
            setTimeout(() => {
                this.repaintAfterResize = true;
            }, 1000);
        }
        if (this.isFirst) {
            setTimeout(() => {
                this.isFirst = false;
            });
            this.renderSchedular();
        }

        setTimeout(() => {
            this.blockUI.stop();
        }, 500);
    }

    // END - SCHEDULAR METHOS
    changeView(view: string) {
        this.isSdlrReady = false;
        this.headerComponent.changeView(view);
        this.calendarPopUpMargin.horizontal = view != 'date' ? 0 : -60;
    }

    viewsChange(e) {
        this.renderSchedular();
    }

    isDisplayTimeCell(time: string): boolean {
        let min = parseInt(AppoinmentDateUtils.formatTime(time, 'm', true));
        return min == 0;
    }

    findOrCreatStyleTag(style: string, id: string) {
        let head = document.getElementsByTagName('head')[0];
        let element = document.getElementById(id);

        if (!element) {
            element = document.createElement('style');
            element.id = id;
            head.appendChild(element);
        }
        element.textContent = '';
        element.appendChild(document.createTextNode(style));
    }

    openInDayView(date: Date) {
        this.headerComponent.setCurrentDate(date);
        this.changeView('day');
    }

    private renderSchedular() {
        if (this.isNoPractitioner || !this.scheduler) {
            return;
        }
        this.isSdlrReady = false;
        setTimeout(() => {
            this.scheduler.instance.repaint();
        });
    }

    openInDayViewWithPrctioner(practitioner: PractitionerSpecialityModel | ApptTreatmentRoomModel) {
        if (!practitioner.isShowOnlyThis) {
            practitioner.isShowOnlyThis = true;
            if (!this.isDisplayRoom) {
                this.resource = [...[], practitioner as PractitionerSpecialityModel];
            } else {
                this.resourceTr = [...[], practitioner as ApptTreatmentRoomModel];
            }
            this.changeView('day');
            return;
        }
        practitioner.isShowOnlyThis = false;
        this.practitionerListComponent.resetResourceNormal();
    }

    // PRACTINER COMPONENT METHODS
    checkPractitioner(val: number) {
        if (val == -1) {
            this.isNoPractitioner = false;
            return;
        }
        if (val == PractitionerListComponent.NO_SELECTED_PRACTITIONER) {
            this.blockUI.stop();
            this.isNoPractitioner = !(this.PatientAdded || this.AppointmentTypeAdded);
            this.guideTitle = 'You haven’t added a Schedule for this day.';
            this.guideMsg = 'First, select a practitioner from the Practitioner List, click on ellipsis “<mat-icon class="m-0">more_vert</mat-icon>” and select “Add to \n scheduler” to add this practitioner into the scheduler. Then click on Schedule button and select a \n timeslot in scheduler to create schedule for this practitioner.';
            this.guideUrl = '';
            setTimeout(() => {
                this.isFirst = false;
            }, 500);
        } else if (val == PractitionerListComponent.NO_PRACTITIONER) {
            this.blockUI.stop();
            if (this.isNoPractitioner) {
                this.guideTitle = 'You haven\'t added a practitioner.';
                this.guideMsg = 'Please click on “Add Practitioner” to add a new Practitioner to this Location.';
                this.guideUrl = '/staffs/add';
                this.guideUrlTitle = 'Add Practitioner';
                this.guideQP = { relative: true, url: 'appointment' };
                this.isNoPractitioner = true;
                setTimeout(() => {
                    this.isFirst = false;
                }, 500);
            }
        }

        if (this.PatientAdded) {
            this.PatientAdded = false;
            setTimeout(() => {
                this.openAddEditApptPopup();
            }, 1000);
        }
    }

    checkTreatmenrRoom(val: number) {
        if (val == -1) {
            this.isNoTreatmentRoom = false;
            return;
        }
        if (val == PractitionerListComponent.NO_SELECTED_PRACTITIONER) {
            this.blockUI.stop();
            this.isNoTreatmentRoom = true;
            this.guideTitle = 'You haven’t added a Appointment or Class to this Room';
            this.guideMsg = 'Room column will be shown in scheduler, if and only if, the particular room has an appointment or class scheduled for the selected date.';
            this.guideUrl = '';
            setTimeout(() => {
                this.isFirst = false;
            }, 500);
        } else if (val == PractitionerListComponent.NO_PRACTITIONER) {
            this.blockUI.stop();
            this.guideTitle = 'You haven’t added a treatment room yet';
            this.guideMsg = 'Please click on “Add Treatment Room” to add a new treatment room for this Location.';
            this.guideUrl = '/tools/treatmentRoom/add';
            this.guideUrlTitle = 'Add Treatment Room';
            this.guideQP = { relative: true, url: 'appointment' };
            this.routerState = {
                fromState: true,
                fromPage: FromPage.Scheduler,
                for: For.Scheduler,
                data: {
                    isDisplayRoom: this.isDisplayRoom
                }
            };
            this.isNoTreatmentRoom = true;
            setTimeout(() => {
                this.isFirst = false;
            }, 500);
        }

        if (this.PatientAdded) {
            this.PatientAdded = false;
            setTimeout(() => {
                this.openAddEditApptPopup();
            }, 1000);
        }
    }

    resourceIds: string[] = [];
    resourceIdsForBlockPos: string[] = [];
    apptColorByStaff: any = {};

    resourceReady(data: PractitionerSpecialityModel[]) {
        // if (!this.PatientAdded && !this.AppointmentTypeAdded && !this.stateValues) {
        if ((this.PatientAdded && this.AppointmentTypeAdded) && data && data.length > 0) {
            data.forEach(value => {
                const hasValue = this.resource.find(res => res.practitionerId == value.practitionerId);
                if (!hasValue) {
                    this.resource.push(value);
                }
            });
        }
        this.practitioners = this.practitionerListComponent.practionersList;
        this.resourceIds = [];
        this.resource = [...data];
        this.resource.forEach(value => {
            this.resourceIds.push(value.id);
            this.apptColorByStaff[value.id] = value.color || 'rgba(0,138,255, 1)';
        });
        this.manageScheduleService.selectedPractitioner = this.resource;

        Object.keys(this.apptColorByStaff).forEach(value => {
            const css = `.appoinment-sdlr .dx-scheduler-appointment .type-2.pi-${value} {
                      padding: 0;
                      background: ${this.apptColorByStaff[value]} !important;
                    }
                    .appoinment-sdlr .dx-scheduler-appointment .type-2.pi-${value}.status-1 {
                      padding: 0;
                      background: ${this.completedAppointmentColor} !important;
                    }`;
            this.findOrCreatStyleTag(css, 'appoint-color-dynamic-' + value);
        });

        this.dataSource.forEach(value => {
            if (value.type == AppointmentType.Holiday) {
                value.practitionerId = this.resourceIds[0];
            }
        });
        this.renderSchedular();
        this.calculateWaitingListCount();
    }

    resourceIdsTr: string[] = [];
    resourceIdsForBlockPosTr: string[] = [];
    apptColorByStaffTr: any = {};

    resourceReadyTr(data: ApptTreatmentRoomModel[]) {
        console.log(data);
        // if (!this.PatientAdded && !this.AppointmentTypeAdded && !this.stateValues) {
        // if ((this.PatientAdded && this.AppointmentTypeAdded) && data && data.length > 0) {
        //   data.forEach(value => {
        //     const hasValue = this.resource.find(res => res.practitionerId == value.practitionerId);
        //     if (!hasValue) {
        //       this.resource.push(value);
        //     }
        //   });
        // }
        this.resourceIdsTr = [];
        this.resourceTr = [...data];
        this.resourceTr.forEach(value => {
            this.resourceIdsTr.push(value.id);
        });
        // this.manageScheduleService.selectedPractitioner = this.resource;

        Object.keys(this.apptColorByStaff).forEach(value => {
            const css = `.appoinment-sdlr .dx-scheduler-appointment .type-2.pi-${value} {
                      padding: 0;
                      background: ${this.apptColorByStaff[value]} !important;
                    }
                    .appoinment-sdlr .dx-scheduler-appointment .type-2.pi-${value}.status-1 {
                      padding: 0;
                      background: ${this.completedAppointmentColor} !important;
                    }`;
            this.findOrCreatStyleTag(css, 'appoint-color-dynamic');
        });

        this.dataSource.forEach(value => {
            if (value.type == AppointmentType.Holiday) {
                value.practitionerId = this.resourceIds[0];
            }
        });
        this.renderSchedular();
        this.calculateWaitingListCount();
    }

    clickSchedulerSetting(value: SchedulerSettingsEnum) {
        if (value == SchedulerSettingsEnum.SchedulerSettings) {
            const dialogRef = this.dialog.open(SchedulerSettingsPopupComponent, {
                width: '800px',
                panelClass: 'scheduler-settings-popup',
                data: this.schedulerSettings ? this.schedulerSettings : null
            });
            dialogRef.afterClosed().subscribe(response => {
                if (response.isClose) {
                    return;
                }
                if (response.hasError) {
                    if (response.isUpdate) {
                        this.displayErrorMessage('Failed to update scheduler settings. Please try again later.');
                    } else {
                        this.displayErrorMessage('Failed to save scheduler settings. Please try again later.');
                    }
                } else {
                    this.schedulerSettings = response.data;
                    this.setMinutesLabel();
                    this.changeGroup();
                    this.changeView(this.getSchedulerView);
                    if (response.isUpdate) {
                        this.displaySuccessMessage('Scheduler settings updated successfully');
                    } else {
                        this.displaySuccessMessage('Scheduler settings saved successfully');
                    }
                }
            });
        } else if (value === SchedulerSettingsEnum.PractitionerAvailability) {
            const ref = this.dialogService.open(PractitionerAvailabilityComponent, {
                title: 'Practitioner Availability Finder',
                width: '660px',
                height: '822px',
                hideSave: true,
                cancelBtnTitle: 'Close',
                data: { slotSize: this.cellDuration, isPatientOnlineBooking: this.isPatientOnlineBooking }
            });
        }
    }

    // CURRENT DATE CHANGE
    changeCurrentDate(date) {
        this.setCurrentDate(date);
        this.addHolidayToScheduler();
    }

    setCurrentDate(date: any) {
        // setTimeout(() => {
        this.currentDate = date;
        // });
    }

    setCurrentView(view: string) {
        // setTimeout(() => {
        this.isSdlrReady = false;
        this.currentView = view;
        this.addHolidayToScheduler();
        setTimeout(() => {
            this.currentViewT = JSON.parse(JSON.stringify(this.currentView));
        });
        // }, 1000)
    }

    togglePttrStatics(element, data: any, isLeave?: boolean) {
        if (!this.isShowPractitionerStatic || (!this.getSchedulerGroupByPractitioner && this.manageScheduleService.getCurrentView() != 'day')) {
            return;
        }
        if (isLeave) {
            this.pttrStaticsPopOver.target = null;
            this.pttrStaticsPopOver.instance.hide();
            this.totalScheduleHour = '0 hrs';
            return;
        }
        const id = data.id;
        const hoverDate = AppoinmentDateUtils.format(element.getAttribute('date'), 'YYYY-MM-DD');
        let totalScheduleHour = 0;

        this.schedules.forEach(value => {
            if (value.practitionerId == id) {
                if (value.recurrenceRule) {
                    let rStr = 'DTSTART:' + DateUtils.toUtcRecurrenceException(value.startDate) + ' RRULE:' + value.recurrenceRule;
                    let sRRule: RRule = rrulestr(rStr);
                    let possibleDates = sRRule.between(AppoinmentDateUtils._toDate(value.startDate), AppoinmentDateUtils._toDate(this.scheduler.instance.getEndViewDate()), true);
                    let dates = [];
                    possibleDates.forEach(date => {
                        dates.push(AppoinmentDateUtils.format(date, 'YYYY-MM-DD'));
                    });
                    if (dates.indexOf(hoverDate) > -1) {
                        const diff = AppoinmentDateUtils.getDiffInMin(value.startDate, value.endDate);
                        totalScheduleHour += diff + 1;
                    }
                } else {
                    if (AppoinmentDateUtils.isEqual(value.startDate, hoverDate)) {
                        const diff = AppoinmentDateUtils.getDiffInMin(value.startDate, value.endDate);
                        totalScheduleHour += diff + 1;
                    }
                }
            }
        });
        const hour = totalScheduleHour / 60;
        const min = (hour % 1) * 60;
        this.totalScheduleHour = parseInt(hour.toString()) + ' hrs ' + Math.round(min) + ' mins';
        this.pttrStaticsPopOver.target = element;
        this.pttrStaticsPopOver.instance.show();
    }

    changeGroup() {
        this.isSecondTypeGroup = this.getSchedulerGroupByPractitioner;
        this.renderSchedular();
    }

    openApptForm($event) {
        if ($event) {
            $event.cancel = true;
        }
    }

    private isPractitionerInActive(event, isDummy) {
        let pttrId = '';
        if (isDummy) {
            pttrId = event.practitionerId;
        } else {
            pttrId = event.cellData.groups.practitionerId;
        }
        if (pttrId) {
            for (let res of this.resource) {
                if (res.id == pttrId && !res.status) {
                    return true;
                }
            }
        }
        return false;
    }

    clickWaitList(waitList: WaitList) {
        this.closeACBPopover();
        const treatmentServiceProduct = [];

        if (waitList.serviceId) {
            const serviceName = waitList.serviceName;
            const price = parseInt(serviceName.split('(')[1].split(',')[0].replace('$', ''));
            const duration = parseInt(serviceName.split('(')[1].split(',')[1]);
            this.addEditScheduleData._endDate = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(this.addEditScheduleData._startDate, duration, 'm'));
            this.addEditScheduleData.endDateUTC = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(this.addEditScheduleData.startDateUTC, duration, 'm'));
            treatmentServiceProduct.push({
                treatmentDetailId: '',
                treatementType: TreatmentType.Service,
                productId: '00000000-0000-0000-0000-000000000000',
                serviceId: waitList.serviceId,
                duration: duration,
                price: price,
                isStatus: true
            });
        }

        this.selectedPatient = new PatientModel();
        this.selectedPatient.id = waitList.patientId;
        this.selectedPatient.name = waitList.patientName;

        this.addEditScheduleData.treatmentDetails = {
            treatmentServiceProduct: treatmentServiceProduct,
            practitionerId: this.addEditScheduleData.practitionerId,
        };
        this.addEditScheduleData.type = AppointmentType.APPOINMENT;
        this.openAddEditApptPopup();
    }

    refreshedData($event: WaitList[]) {
        this.waitList = $event;
        this.waitListFiltered = AppUtils.refrenceClone(this.waitList);
        if (!this.appCellPopover.nativeElement.classList.contains('invisible') && this.addEditScheduleData) {
            this.waitListFiltered = this.waitList.filter(x => x['practitionerIds'].indexOf(this.addEditScheduleData.practitionerId) > -1);
        }
    }

    cellClick(event, num?: number, dummyCell?: HTMLElement, isDummyCell: boolean = false, oEvent?) {
        this.isFromDummyCell = isDummyCell;
        this.hideAddEditPopup();
        this.isACBPopoverFocused = true;
        this.appCellPopoverOpen = false;
        const cellCLickedData = this.assignAppointmentDataCopy(event, num, isDummyCell);

        if (this.apptHandlar.isRebookMoveAppointment) {
            if (this.isPractitionerInActive(event, dummyCell)) {
                this.message = 'This is an inactive Practitioner; you cannot rebook Appointment';
                this.type = MessageType.error;
                return;
            }
            const data = this.apptHandlar.tooltipPopupData;
            let type = data.appointmentData.type;
            this.prepareDtataForEditForm(data, type);

            // const cellCLickedData = this.assignAppointmentDataCopy(event, num, isDummyCell);
            const oldAppt = AppUtils.refrenceClone(this.addEditScheduleData);

            this.addEditScheduleData._startDate = cellCLickedData._startDate;
            this.addEditScheduleData._endDate = cellCLickedData._endDate;
            this.addEditScheduleData.startDateUTC = cellCLickedData.startDateUTC;
            this.addEditScheduleData.endDateUTC = cellCLickedData.endDateUTC;
            const preparedData = AddEditAppointmentUtils.prepareData(this.addEditScheduleData, true, cellCLickedData.practitionerId, this.addEditScheduleData.treatmentDetails.treatmentServiceProduct, '');
            delete preparedData.treatmentDetails.createdBy;
            delete preparedData.treatmentDetails.createdDate;
            delete preparedData.treatmentDetails.isStatus;
            delete preparedData.treatmentDetails.modifiedBy;
            delete preparedData.treatmentDetails.modifiedDate;
            delete preparedData.appointmentsEntry;
            delete preparedData.appointmentsExtraDetails;
            delete preparedData.apptRecurringId;
            delete preparedData.isHoliday;
            delete preparedData.plannedService;
            delete preparedData.isNeedAttention;
            delete preparedData.status;
            delete preparedData['text'];

            if (this.apptHandlar.rebookMoveAppt.getValue() == REBOOK) {
                preparedData.treatmentDetails.treatmentServiceProduct.forEach(data => {
                    delete data.id;
                });
            }
            if (this.apptHandlar.rebookMoveAppt.getValue() == MOVE) {
                delete oldAppt.treatmentDetails.createdBy;
                delete oldAppt.treatmentDetails.createdDate;
                delete oldAppt.treatmentDetails.isStatus;
                delete oldAppt.treatmentDetails.modifiedBy;
                delete oldAppt.treatmentDetails.modifiedDate;
                delete oldAppt.appointmentsEntry;
                delete oldAppt.appointmentsExtraDetails;
                delete oldAppt.apptRecurringId;
                delete oldAppt.isHoliday;
                delete oldAppt.plannedService;
                delete oldAppt.isNeedAttention;
                delete oldAppt.status;
                delete oldAppt['text'];
                this.rebookMoveOldAppt = AppUtils.refrenceClone(oldAppt);
                if (oldAppt.recurrenceRule && oldAppt.recurrenceRule.length > 0) {
                    oldAppt.recurrenceException = formatREString(oldAppt.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(AppoinmentDateUtils.getDateWithTz(this.targetAddEditScheduleData.startDate)));
                } else {
                    oldAppt._startDate = cellCLickedData._startDate;
                    oldAppt._endDate = cellCLickedData._endDate;
                    oldAppt.startDateUTC = cellCLickedData.startDateUTC;
                    oldAppt.endDateUTC = cellCLickedData.endDateUTC;
                }
                const oldApptPreparedDats = AddEditAppointmentUtils.prepareData(oldAppt, false, cellCLickedData.practitionerId, oldAppt.treatmentDetails.treatmentServiceProduct, oldAppt.recurrenceRule, oldAppt.recurrenceException);
                this.updateAppointment(oldApptPreparedDats, preparedData, oldAppt.recurrenceRule && oldAppt.recurrenceRule.length > 0);
                return;
            }

            this.addAppointment(preparedData);
            return;
        }

        if (this.waitlistManageService.bookAppointment) {
            if (this.isPractitionerInActive(event, dummyCell)) {
                this.message = 'This is an inactive Practitioner; you cannot create Appointment';
                this.type = MessageType.error;
                return;
            }
            this.blockUI.start();
            this.hideWaitListPopup();
            // const cellCLickedData = this.assignAppointmentDataCopy(event, num, isDummyCell);
            const treatmentServiceProduct = [];
            let endDate = cellCLickedData._endDate;
            let endDateUTC = cellCLickedData.endDateUTC;
            if (this.waitlistManageService.waitList.serviceId) {
                const serviceName = this.waitlistManageService.waitList.serviceName;
                const price = parseInt(serviceName.split('(')[1].split(',')[0].replace('$', ''));
                const duration = parseInt(serviceName.split('(')[1].split(',')[1]);
                endDate = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(cellCLickedData._startDate, duration, 'm'));
                endDateUTC = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(cellCLickedData.startDateUTC, duration, 'm'));
                treatmentServiceProduct.push({
                    treatmentDetailId: '',
                    treatementType: TreatmentType.Service,
                    productId: '00000000-0000-0000-0000-000000000000',
                    serviceId: this.waitlistManageService.waitList.serviceId,
                    duration: duration,
                    price: price,
                    isStatus: true
                });
            }
            const formData = {
                locationId: this.appState.selectedUserLocationId,
                practitionerId: cellCLickedData.practitionerId,
                appointmentType: '',
                treatmentRoomId: '',
                patientId: this.waitlistManageService.waitList.patientId,
                referralId: '00000000-0000-0000-0000-000000000000',
                payerId: '00000000-0000-0000-0000-000000000000',
                caseId: '',
                telehealth: false,
                title: '',
                description: '',
                _startDate: cellCLickedData._startDate,
                _endDate: endDate,
                startDateUTC: cellCLickedData.startDateUTC,
                endDateUTC: endDateUTC,
                treatmentDetails: {
                    treatmentServiceProduct: treatmentServiceProduct,
                    practitionerId: cellCLickedData.practitionerId,
                    concessionId: ''
                },
                isAllDay: false,
                recurrenceRule: '',
                recurrenceException: '',
                bookOnline: false,
                note: this.waitlistManageService.waitList.note,
                frequencyType: Frequency.None,
                type: AppointmentType.APPOINMENT,
            };

            this.patientService.getPatientCases(this.appState.selectedUserLocationId, this.waitlistManageService.waitList.patientId).subscribe(response => {
                if (response) {
                    const defaultCase = response.find(x => x.isdefault);
                    if (defaultCase) {
                        formData.caseId = defaultCase.id;
                        this.patientService.getCaseDetailbyCaseId(defaultCase.id, this.waitlistManageService.waitList.patientId).subscribe(response => {
                            if (response) {
                                if (response.caseContacts && response.caseContacts.length > 0) {
                                    const referralDefault = response.caseContacts.find(x => x.isDefault && x.type === CaseContactType.Referral);
                                    const tpDefault = response.caseContacts.find(x => x.isDefault && x.type === CaseContactType.ThridParty);

                                    if (referralDefault) {
                                        formData.referralId = referralDefault.id;
                                    }

                                    if (tpDefault) {
                                        formData.payerId = tpDefault.id;
                                    }
                                }

                                const data = AddEditAppointmentUtils.prepareData(formData, true, cellCLickedData.practitionerId, formData.treatmentDetails.treatmentServiceProduct, '');
                                this.addAppointment(data);
                            }
                        }, error => {
                            this.alertService.displayErrorMessage(error);
                            this.blockUI.stop();
                        });
                    }
                }
            }, error => {
                this.alertService.displayErrorMessage(error);
                this.blockUI.stop();
            });
            return;
        }

        if (this.selectedHeaderButton == this.SCHEDULE) {
            if (this.isAddEnabled && this.isEditEnabled) {
                if (!this.hasPermission(this.APPT_SCHEDULE, 'C')) {
                    this.alertService.displayErrorMessage('You have no access to create Schedule.');
                    return;
                }
                if (this.isPractitionerInActive(event, dummyCell)) {
                    this.message = 'This is an inactive Practitioner; you cannot create Schedule';
                    this.type = MessageType.error;
                    return;
                }
                this.assignAppointmentData(event, num, isDummyCell);
                this.openAddEditBreakSchedulePopup();
                // this.isDisplayAddEditSchedule = true;
            }
        } else {
            if (!this.hasPermission(this.APPT_BASIC, 'C')) {
                this.alertService.displayErrorMessage(`You have no access to create Appointment/Class/Break.`);
                return;
            }
            let x: number, y: number;
            if (isDummyCell) {
                x = oEvent.x;
                y = oEvent.y;
            } else {
                x = event.event.x;
                y = event.event.y;
            }

            y -= 153;
            x -= 225.5; // (popupWidth/2) + 58

            this.appCellPopover.nativeElement.style.left = x + 'px';
            this.appCellPopover.nativeElement.style.top = y + 'px';

            this.assignAppointmentData(event, num, isDummyCell);

            this.waitListFiltered = this.waitList.filter(x => x['practitionerIds'].indexOf(this.addEditScheduleData.practitionerId) > -1);

            if (this.appCellPopover.nativeElement.classList.contains('invisible') > -1) {
                this.SearchTextPatient = null;
                this.appCellPopoverOpen = true;
                this.acbPopupPatientSearchCmp.focus();
                this.appCellPopover.nativeElement.classList.remove('invisible');
            }
        }
    }

    assignAppointmentData(event, num: number, isDummyCell: boolean = false) {
        if (isDummyCell) {
            event = this.prepeareDummyCellData(AppUtils.refrenceClone(event), num);
        }
        this.addEditScheduleData = this.prepareScheduleData(event.cellData);
    }

    assignAppointmentDataCopy(event, num: number, isDummyCell: boolean = false) {
        if (isDummyCell) {
            event = this.prepeareDummyCellData(AppUtils.refrenceClone(event), num);
        }
        return this.prepareScheduleData(event.cellData);
    }

    headerButtonClick(action) {
        this.toggleBreak(action == Constant.SCHEDULE);
        this.toggleAppt(action == Constant.SCHEDULE);
        this.selectedHeaderButton = action;
        this.enableDisableCRUD(action == Constant.SCHEDULE);
        if (action == Constant.CREATE_APPT) {
            if (!this.hasPermission(this.APPT_BASIC, 'C')) {
                this.alertService.displayErrorMessage(`You have no access to create Appointment.`);
                return;
            }
            this.addEditScheduleData = {
                'id': null,
                'startDateUTC': new Date(),
                'endDateUTC': new Date(),
                'frequencyType': undefined,
                'recurrenceRule': '',
                'recurrenceID': '',
                'bookOnline': this.isPatientOnlineBooking,
                'note': '',
                'locationId': this.appState.selectedUserLocationId,
                'practitionerId': undefined,
                '_startDate': new Date(),
                '_endDate': new Date(),
                'type': AppointmentType.APPOINMENT,
                'isManualSelection': true
            };
            this.openAddEditApptPopup();
        } else if (action == Constant.WAIT_LIST) {
            if (!this.hasPermission(this.APPT_BASIC, 'C')) {
                this.alertService.displayErrorMessage(`You have no access to use this feature.`);
                return;
            }
            this.openWaitListPopup();
        } else if (action == Constant.ROOMS || action == Constant.ROOMS_ACTIVE) {
            if (!this.hasPermission(this.APPT_BASIC, 'C')) {
                this.alertService.displayErrorMessage(`You have no access to use this feature.`);
                return;
            }
            this.blockUI.start();
            if (action == Constant.ROOMS_ACTIVE) {
                this.isNoTreatmentRoom = false;
                this.isSecondTypeGroup = this.getSchedulerGroupByPractitioner;
                this.changeView('day');
                this.togglePractitionerPanel(true);
            }
            if (action == Constant.ROOMS) {
                this.isSecondTypeGroup = true;
                this.togglePractitionerPanel(true);
            }
            this.manageScheduleService.displayRoom(action == Constant.ROOMS);
            this.isDisplayRoom = action == Constant.ROOMS;
            this.practitionerResource = AppUtils.refrenceClone(this.resource);
        }
    }

    getTimeForDummyCell(data: Schedule, num: number) {
        let startDate = AppoinmentDateUtils.addAndGetNewValue(data.startDate, num * this.cellDuration, 'm');

        return AppoinmentDateUtils.formatDateTime(startDate, 'hh:mm A', false);
    }

    getMinuteForDummyCell(data: Schedule, num: number) {
        let startDate = AppoinmentDateUtils.addAndGetNewValue(data.startDate, num * this.cellDuration, 'm');

        return AppoinmentDateUtils.formatDateTime(startDate, 'mm', false);
    }

    prepeareDummyCellData(data: Schedule, num: number) {
        let startDate = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(data.startDate, num * this.cellDuration, 'm'));
        let endDate = AppoinmentDateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(startDate, this.cellDuration, 'minutes'), true);

        return {
            'cellData': {
                'startDate': startDate,
                'endDate': endDate,
                'allDay': false,
                'groupIndex': 0,
                'groups': {
                    'practitionerId': data.practitionerId
                }
            }
        };
    }

    /** Appointment Functions */
    onSelectPatientForAppt($event) {
        let data = this.addEditScheduleData;
        if (!this.hasPermission(this.APPT_BASIC, 'C')) {
            this.alertService.displayErrorMessage(`You have no access to create Appointment.`);
            return;
        }
        if (!this.isFromDummyCell) {
            data = { cellData: { groups: { practitionerId: this.addEditScheduleData.practitionerId } } };
        }
        if (this.isPractitionerInActive(data, this.isFromDummyCell)) {
            this.message = 'This is an inactive Practitioner; you cannot create Appointment';
            this.type = MessageType.error;
            this.hideACBPopover();
            this.closeACBPopover();
            this.hideApptAddEditPopup();
            return;
        }
        if (!this.isFromDummyCell && !this.isAllowBookingOutsideScheduleHours) {
            this.message = 'You cannot create booking outside scheduled hours.';
            this.type = MessageType.error;
            this.isFromDummyCell = false;
            this.hideACBPopover();
            this.closeACBPopover();
            this.hideApptAddEditPopup();
            return;
        }
        if ($event) {
            this.patientAppointmentService.getPatientAlerts($event.id).subscribe(response => {
                if (response && response.length > 0 && response[0].message) {
                    this.dialogService.open(PatientAlertDisplayComponent, {
                        title: 'Patient Alert',
                        width: '719px',
                        hideCancel: true,
                        saveBtnTitle: 'Ok',
                        data: { data: response }
                    }).clickSave.subscribe(() => {
                        this.selectedPatient = $event;
                        this.addEditScheduleData.type = AppointmentType.APPOINMENT;
                        this.openAddEditApptPopup();
                    });
                } else {
                    this.selectedPatient = $event;
                    this.addEditScheduleData.type = AppointmentType.APPOINMENT;
                    this.openAddEditApptPopup();
                }
            });
        }
    }

    // PREPARE DATA FOR EDIT/ADD SCHEDULE
    private prepareScheduleData(data) {
        return {
            'id': data['id'],
            'startDateUTC': data.startDate,
            'endDateUTC': data.endDate,
            'frequencyType': data.frequencyType,
            'recurrenceRule': '',
            'recurrenceID': '',
            'bookOnline': this.isPatientOnlineBooking,
            'note': '',
            'locationId': this.appState.selectedUserLocationId,
            'practitionerId': data.groups.practitionerId,
            '_startDate': data.startDate,
            '_endDate': data.endDate,
            'type': this.getApptType()
        };
    }

    redirectPatient(data: any) {
        let obj = {
            addEditScheduleData: this.addEditScheduleData,
            resource: this.resource,
            SearchTextPatient: this.SearchTextPatient,
            left: this.appCellPopover.nativeElement.style.left,
            top: this.appCellPopover.nativeElement.style.top
        };
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                stateValues: JSON.stringify(obj)
            }
        };
        this._router.navigate(['patients/add'], fromState);
    }

    SearchText(data) {
        this.SearchTextPatient = data;
    }

    bindResourceAfterRedirect(data) {
        this.resourceIds = [];
        this.resource = [...data];
        this.resource.forEach(value => {
            this.resourceIds.push(value.id);
        });

        this.dataSource.forEach(value => {
            if (value.type == AppointmentType.Holiday) {
                value.practitionerId = this.resourceIds[0];
            }
        });
        this.renderSchedular();
        this.renderSchedular();
    }

    onAddClick(data) {
        let obj = {
            addEditScheduleData: this.addEditScheduleData,
            resource: this.resource,
            SearchTextPatient: this.SearchTextPatient,
            left: this.appCellPopover.nativeElement.style.left,
            top: this.appCellPopover.nativeElement.style.top,
            displayName: this.getPttrNameFromId(this.addEditScheduleData.practitionerId)
        };
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                stateValues: JSON.stringify(obj)
            }
        };
        if (data == '+ Add Appointment Type') {
            this._router.navigate(['tools/appointmentType/add'], fromState);
        } else if (data == '+ Add Treatment Rooms') {
            this._router.navigate(['tools/treatmentRoom/add'], fromState);
        } else if (data == '+ Add Consession') {
            this._router.navigate(['tools/concession/add'], fromState);
        } else if (data == '+ Attach Referral') {
            let fromState: NavigationExtras = {
                state: {
                    fromState: JSON.stringify({}),
                    fromStateReferral: true,
                    stateValues: JSON.stringify(obj)
                }
            };
            this._router.navigate(['contacts/referral/add'], fromState);
        } else if (data == '+ Attach Third Party') {
            let fromState: NavigationExtras = {
                state: {
                    fromState: JSON.stringify({}),
                    fromStateThirdParty: true,
                    stateValues: JSON.stringify(obj)
                }
            };
            this._router.navigate(['contacts/thirdparty/add'], fromState);
        }
    }

    private getApptType(): number {
        if (this.acbClickeHeaderBtn == this.ACB_BREAK) {
            return 1;
        }
        return 0;
    }

    // GET DAYS TO ADD
    private getDaysToAdd(): number {
        return this.manageScheduleService.getDaysToAdd(-1);
        // return this.currentView == 'day' ? 1 : this.currentView == 'week' ? 6 : 4;
    }

    get aePopupAnimation() {
        return {
            type: 'slide',
            direction: 'left',
            duration: 200,
        };
    }

    get getArray() {
        return new Array(this.isSecondTypeGroup ? 1 : this.resource.length);
    }

    setSdlrHeight(height, needHeight?) {
        height = AppUtils.getInteger(height);
        if (!needHeight || (needHeight + 94) >= height) {
            this.height = height;
        } else {
            this.height = 'unset';
        }
    }

    get getCellSize() {
        if (this.timeSlotSize == 'Small') {
            return 16;
        } else if (this.timeSlotSize == 'Medium') {
            return 20;
        } else if (this.timeSlotSize == 'Default') {
            return 25;
        } else if (this.timeSlotSize == 'Large') {
            return 30;
        } else if (this.timeSlotSize == 'Big') {
            return 50;
        }
    }

    cancelSchedule() {
        // this.isDisplayAddEditSchedule = false;
        // this.targetAddEditScheduleData = null;
        this.hideAddEditPopup();
        this.hideApptAddEditPopup();
        this.selectedPatient = null;
        if (this._targetAddEditScheduleData) {
            this._targetAddEditScheduleData.mode = ScheduleMode.NORMAL;
        }
    }

    saveSchedule(schedule: any) {
        if (this._targetAddEditScheduleData) {
            this._targetAddEditScheduleData.mode = ScheduleMode.NORMAL;
        }
        let isError = schedule['isError'];
        let type = schedule['type'];
        let isHidePopup = schedule['isHidePopup'];
        let isDisplayMessage = schedule['isDisplayMessage'];
        let secondaryType = schedule['secondaryType'];
        let data: Schedule = schedule['data'];

        if (isError && isHidePopup) {
            if (type == Constant.ADD) {
                this.displayErrorMessage(`Failed to add ${this.typeToString(data.type).toLowerCase()}, please try again later.`);
            } else if (type == Constant.EDIT) {
                this.displayErrorMessage(`Failed to update ${this.typeToString(data.type).toLowerCase()}, please try again later.`);
            } else if (type == Constant.DELETE) {
                this.displayErrorMessage(`Failed to delete ${this.typeToString(data.type).toLowerCase()}, please try again later.`);
            }
            return;
        }

        if (type == Constant.ADD) {
            this.scheduler.instance.addAppointment(data);
        } else if (type == Constant.EDIT) {
            this.scheduler.instance.updateAppointment(this.getAppointmentById(data.id)[0], data);
        } else if (type == Constant.DELETE) {
            this.scheduler.instance.deleteAppointment(this.getAppointmentById(data.id)[0]);
        }
        if (schedule['isNotify']) {
            this.displayMessage(secondaryType, data.type, isDisplayMessage);
        }
        // this.isDisplayAddEditSchedule = !isHidePopup;
        if (isHidePopup) {
            this.hideAddEditPopup();
            this.cancelSchedule();
            this.renderSchedular();
        }
    }

    displayMessage(type, appointmentType, isDisplayMessage) {
        let message = null;
        if (isDisplayMessage) {
            return;
        }
        if (type == Constant.ADD) {
            message = `${this.typeToString(appointmentType)} created successfully.`;
        } else if (type == Constant.EDIT) {
            message = `${this.typeToString(appointmentType)} updated successfully.`;
        } else if (type == Constant.DELETE) {
            message = `${this.typeToString(appointmentType)} deleted successfully.`;
        }
        this.displaySuccessMessage(message);
    }

    private getAppointmentById(id): Schedule[] {
        return this.dataSource.filter(value => value.id == id);
    }

    getAppointments(): Appointment[] {
        return this.dataSource.filter(value => value.type == AppointmentType.APPOINMENT) as Appointment[];
    }

    // APPOINTMENT HANDLING
    appointmentRender(e) {
        if (this.scheduler && this.scheduler.resources[0]) {
            this.resourceIdsForBlockPos = [];
            if (!this.isDisplayRoom) {
                this.scheduler.resources[0].dataSource.forEach(pttr => {
                    this.resourceIdsForBlockPos.push(pttr.practitionerId);
                });
            } else {
                this.scheduler.resources[0].dataSource.forEach(pttr => {
                    this.resourceIdsForBlockPos.push(pttr.id);
                });
            }
        }

        let data = e.appointmentData as Schedule;
        let tData = e.targetedAppointmentData as Schedule;
        let sTime = AppoinmentDateUtils.formatDateTime(data.startDate, 'H', false);
        let elem = e.appointmentElement as HTMLElement;
        let type = 'type_' + data.type;

        elem.classList.add(type);
        elem.classList.remove('d-none');
        elem.id = DateUtils.toUtcRecurrenceException(tData.startDate) + '' + type;
        elem.setAttribute('data-startDate', tData.startDate);
        elem.setAttribute('data-type', data.type.toString());
        let cell = e.element.querySelector('.dx-scheduler-date-table .dx-scheduler-date-table-row .dx-scheduler-date-table-cell');
        const width = (parseFloat(cell.offsetWidth) + (this.currentView != 'day' ? 1.47 : 0.0));

        let extraSpace = 0;
        let extraWidth = 0;


        const translateCurrrent = elem.style.transform.replace('translate(', '').replace(')', '').split(',');
        const sd = AppoinmentDateUtils.format(tData.startDate, 'YYYY-MM-DD');
        const dateIndex = this.manageScheduleService.selectedDateArray.indexOf(sd);
        const pttrIndex = this.resourceIdsForBlockPos.indexOf(!this.isDisplayRoom ? tData.practitionerId : tData['treatmentRoomId']);
        const colIndex = !this.isSecondTypeGroup ? (dateIndex + (pttrIndex * this.manageScheduleService.getDaysToAdd())) : (pttrIndex + (dateIndex * this.resourceIdsForBlockPos.length));
        let finalWidth = parseFloat(cell.offsetWidth);
        finalWidth = finalWidth < 0 ? 0 : finalWidth;
        if (this.currentView == 'day' && this.resourceIdsForBlockPos.length > 1 && colIndex > 0) {
            extraSpace = 1;
            extraWidth = 1;
        } else if (this.currentView == 'workWeek') {
            if (this.resourceIdsForBlockPos.length == 1) {
                extraWidth = -1;
                if (colIndex > 0) {
                    extraSpace = 1;
                    extraWidth = 0.01;
                }
            }
        } else if (this.currentView == 'week') {
            if (this.resourceIdsForBlockPos.length == 1) {
                if (colIndex == 4) {
                    extraWidth = 1;
                }
                if (colIndex > 4) {
                    extraSpace = -1;
                }
            }
        }
        if (cell) {
            elem.style.width = (width + extraWidth) + 'px';
        }
        const xFinal = ((finalWidth * colIndex) - extraSpace);
        let yFinal = translateCurrrent[1];

        if (data.type == this.TYPE_HOLIDAY) {
            const listElem = document.querySelectorAll(`[id='${elem.id}']`);
            yFinal = ((listElem.length - 1) * 21) + 'px';
        }

        elem.style.transform = 'translate(' + (xFinal < -1 ? 0 : xFinal) + 'px,' + yFinal + ')';

        if (data.type == this.TYPE_BREAK || data.type == this.TYPE_APPT) {
            elem.style.zIndex = '1';
            if (this.selectedHeaderButton == this.SCHEDULE) {
                elem.classList.add('d-none');
            }
        } else {
            elem.style.zIndex = '0';
        }

        if (sTime == this.startTime.toString()) {
            elem.style.borderTop = 'none';
            let dummyCellWrapper: any = elem.getElementsByClassName('hours-line-wrapper');
            if (dummyCellWrapper && dummyCellWrapper.length > 0) {
                dummyCellWrapper[0].style.top = 0;
            }
        }
    }

    isDisplayApptTooltip = true;
    onlyPrepareData = false;

    appoinmentlClick(data: any) {
        let type = data.appointmentData.type;
        this.isDisplayApptTooltip = true;
        data.cancel = true;
        if (this.selectedHeaderButton != this.SCHEDULE && type == this.TYPE_SCHEDULE) {
            return;
        }
        this.prepareDtataForEditForm(data, type);
        if (type == this.TYPE_APPT) {
            this.selectedPatient = new PatientModel();
            this.selectedPatient.id = data.appointmentData.patientId;
            this.selectedPatient.name = data.appointmentData.appointmentsExtraDetails.patientFullNameWithTitle;
            if (!this.onlyPrepareData) {
                setTimeout(() => {
                    if (this.isDisplayApptTooltip) {
                        const elem: HTMLDivElement = data.appointmentElement;
                        const elemX = elem.getBoundingClientRect().left;
                        const elemY = elem.getBoundingClientRect().top;
                        const eventX = data.event.clientX;
                        const eventY = data.event.clientY;
                        let x = eventX - elemX;
                        let y = eventY - elemY;
                        this.apptTooltipPopop.position = {
                            at: 'left',
                            my: eventX + 324 > window.innerWidth ? 'right' : 'left',
                            offset: { x: x, y: y },
                            collision: { x: 'fit', y: 'flipfit' }
                        } as positionConfig;
                        this.addEditScheduleData.mode = ScheduleMode.NORMAL;
                        this._targetAddEditScheduleData.mode = ScheduleMode.NORMAL;
                        this.apptHandlar.setTooltipData(data);
                        this.apptTooltipPopop.target = elem;
                        this.apptTooltipPopop.instance.show();
                    }
                }, 300);
            }
            return;
        } else {
            if (!this.hasPermission(this.APPT_BASIC, 'E')) {
                this.alertService.displayErrorMessage(`You have no access to modify ${type === this.TYPE_SCHEDULE ? 'Schedule' : 'Break'}.`);
                return;
            }
            if (this.isEditEnabled && this.isAddEnabled || type == this.TYPE_BREAK || type == this.TYPE_APPT) {
                data.appointmentData.mode = this.SCHEDULE_MODE_EDIT;
            }
            this.openAddEditBreakSchedulePopup();
        }
    }

    appoinmentlDblClick(data: any) {
        this.apptTooltipPopop.instance.hide();
        this.isDisplayApptTooltip = false;
        data.cancel = true;
        if (!this.hasPermission(this.APPT_BASIC, 'E')) {
            this.alertService.displayErrorMessage(`You have no access to modify Appointment.`);
            return;
        }
        let type = data.appointmentData.type;
        if (type == this.TYPE_APPT) {
            this.prepareDtataForEditForm(data, type);
            data.appointmentData.mode = this.SCHEDULE_MODE_EDIT;
            this.openAddEditApptPopup();
        }
    }

    prepareDatesForApppt(appointmentData: Schedule) {
        appointmentData.startDate = new Date(AppoinmentDateUtils.formatDateTime(appointmentData.startDate, 'YYYY-MM-DDTHH:mm:ss', false));
        appointmentData.endDate = new Date(AppoinmentDateUtils.formatDateTime(appointmentData.endDate, 'YYYY-MM-DDTHH:mm:ss', false));
        appointmentData.startDateUTC = appointmentData.startDate;
        appointmentData.endDateUTC = appointmentData.endDate;
        appointmentData._startDate = appointmentData.startDate;
        appointmentData._endDate = appointmentData.endDate;
    }

    range = (start, end, delta) => {
        return Array.from(
            { length: ((end - start) / delta) * this.rowsPerHour }, (v, k) => (k * delta) + start
        );
    };

    timeDiff = (start, end) => {
        let a = moment(start);
        let b = moment(end);
        return b.diff(a, 'hour');
    };

    leaveMouseFromTimeTooltipContainer($event) {
        this.tooltipDir.hide();
    }

    apptTooltipHide() {
        if (this._targetAddEditScheduleData) {
            this._targetAddEditScheduleData.mode = ScheduleMode.NORMAL;
        }
    }

    isDateIqual(targetedAppointmentData) {
        return this.targetAddEditScheduleData && AppoinmentDateUtils.isEqual(targetedAppointmentData.startDate, this.targetAddEditScheduleData.startDate);
    }

    private prepareDtataForEditForm(data: any, type: any) {
        if (this._targetAddEditScheduleData) {
            this._targetAddEditScheduleData.mode = ScheduleMode.NORMAL;
        }
        let appointmentData = AppUtils.refrenceClone(data.appointmentData);
        let targetedAppointmentData = AppUtils.refrenceClone(data.targetedAppointmentData);
        this._targetAddEditScheduleData = data.appointmentData;
        this.practitionerDetails = this.resource.find(x => x.practitionerId == appointmentData.practitionerId);
        if (this.selectedHeaderButton == this.SCHEDULE || type == this.TYPE_BREAK || type == this.TYPE_APPT) {
            this.prepareDatesForApppt(appointmentData);
            this.prepareDatesForApppt(targetedAppointmentData);
            this.addEditScheduleData = appointmentData;
            this.targetAddEditScheduleData = targetedAppointmentData;
        }
    }

    private enableDisableCRUD(isEnabled: boolean) {
        this.isAddEnabled = isEnabled;
        this.isEditEnabled = isEnabled;
        this.isDeleteEnabled = isEnabled;
        this.cancelSchedule();
    }

    private toggleBreak(isScheduleMode: boolean) {
        let breaks = document.querySelectorAll('.type_1');
        breaks.forEach(value => {
            if (isScheduleMode) {
                value.classList.add('d-none');
            } else {
                value.classList.remove('d-none');
            }
        });
    }

    private toggleAppt(isScheduleMode: boolean) {
        let appts = document.querySelectorAll('.type_2');
        appts.forEach(value => {
            if (isScheduleMode) {
                value.classList.add('d-none');
            } else {
                value.classList.remove('d-none');
            }
        });
    }

    private typeToString(type): string {
        if (type == this.TYPE_BREAK) {
            return 'Break';
        } else if (type == this.TYPE_APPT) {
            return 'Appointment';
        }
        return 'Schedule';
    }

    // APPOINTMENT, CLASS, BREAK POPOVER MEHODD
    acbClickeHeaderBtn: string = this.ACB_APPT;

    acbHeaderBtnClick(clicked: string, isOpenPopup = true) {
        this.enableDisableCRUD(false);
        this.acbClickeHeaderBtn = clicked;
        const isInActive = this.isPractitionerInActive(this.addEditScheduleData, true);
        if (this.acbClickeHeaderBtn == this.ACB_BREAK) {
            if (isInActive) {
                this.closeACBPopover();
                this.message = 'This is an inactive Practitioner; you cannot create Break';
                this.type = MessageType.error;
                return;
            }
            this.enableDisableCRUD(true);
            this.addEditScheduleData.type = AppointmentType.BREAK;
            this.openAddEditBreakSchedulePopup();
        } else if (this.acbClickeHeaderBtn == this.ACB_APPT) {
            if (isInActive) {
                this.closeACBPopover();
                this.message = 'This is an inactive Practitioner; you cannot create Appointment';
                this.type = MessageType.error;
                return;
            }
            this.addEditScheduleData.type = AppointmentType.APPOINMENT;
        }
        if (isOpenPopup) {
            this.closeACBPopover();
        }
    }

    closeACBPopover() {
        this.acbPopupPatientSearchCmp.clearText();
        this.appCellPopover.nativeElement.classList.add('invisible');
        this.appCellPopoverOpen = false;
        this.isWaitListPopup = false;
        this.hideACBPopover();
    }

    getPttrNameFromId(practitionerId: any) {
        let data = this.resource.filter(value => value.id == practitionerId);
        return data && data.length > 0 ? data[0].displayName : '';
    }

    hideACBPopover() {
        this.acbClickeHeaderBtn = this.ACB_APPT;
    }

    waitingRoomEditAPpt(date: Date) {
        let type = 'type_' + this.TYPE_APPT;

        const id = DateUtils.toUtcRecurrenceException(date) + '' + type;
        const elem = document.getElementById(id);
        if (elem) {
            this.onlyPrepareData = true;
            elem.click();
            setTimeout(() => {
                this.onlyPrepareData = false;
            }, 200);
            this.apptHandlar.notifyEditAppt();
        }
    }

    /** GET SCHEDULER SETTINGS */
    private getSchedulerSettings() {
        const userId = this.appState.UserProfile.id || null;
        this.schedulerSettingsService.getSchedulerSettings(userId).subscribe(response => {
            if (response) {
                this.schedulerSettings = response;
                if (!this.initFromHistory) {
                    this.changeView(this.getSchedulerView);
                } else {
                    this.changeView(this.apptHandlar.schedulerState.currentView);
                }
                this.changeGroup();
            }
        }, error => {

        });
    }

    public hideWaitListPopup() {
        this.isDisplayWaitListPopup = false;
    }

    private openWaitListPopup() {
        this.isDisplayWaitListPopup = true;
        this.closeACBPopover();
        this.hideApptAddEditPopup();
        this.hideAddEditPopup();
    }

    private hideApptAddEditPopup() {
        this.isDisplayAddEditApptPopup = false;
        this.targetAddEditScheduleData = null;
    }

    private openAddEditApptPopup() {
        this.isDisplayAddEditApptPopup = true;
        this.closeACBPopover();
    }

    private hideAddEditPopup() {
        this.isDisplayAddEditSchedule = false;
        this.targetAddEditScheduleData = null;
    }

    private openAddEditBreakSchedulePopup() {
        this.isDisplayAddEditSchedule = true;
        this.closeACBPopover();
    }

    private setMinutesLabel() {
        const timeRows = document.querySelectorAll('.dx-scheduler-time-panel tbody tr');
        if (this.isShowMinutesLineLabel) {
            if (timeRows) {
                const skipRow = (60 / this.cellDuration);
                let min = this.cellDuration;
                timeRows.forEach((value, index) => {
                    if (index % skipRow != 0 && index != 0) {
                        let content = '<div class="time-cell-text minute-text ng-star-inserted">' +
                            '<span>' + min + '</span>' +
                            '</div>';
                        let div = value.getElementsByClassName('dx-template-wrapper');
                        if (div) {
                            div[0].innerHTML = content;
                        }
                        min += this.cellDuration;
                    } else {
                        min = this.cellDuration;
                    }
                });
            }
        } else {
            if (timeRows) {
                const skipRow = (60 / this.cellDuration);
                timeRows.forEach((value, index) => {
                    if (index % skipRow != 0 && index != 0) {
                        let content = '<div class="time-cell-text minute-text ng-star-inserted">' +
                            '<span></span>' +
                            '</div>';
                        let div = value.getElementsByClassName('dx-template-wrapper');
                        if (div) {
                            div[0].innerHTML = content;
                        }
                    }
                });
            }
        }
    }

    getTimeDiff(start, end) {
        return AppoinmentDateUtils.getDiffInMin(start, end);
    }

    private getHolidayList() {
        this.holidayListService.getHolidayList().subscribe(response => {
            if (response) {
                const data = [];
                response.items.forEach(value => {
                    let endDate: any = new Date(value.start.date);
                    let startDate: any = new Date(value.start.date);

                    startDate.setHours(this.startTime);
                    startDate.setMinutes(0);
                    startDate.setSeconds(0);

                    endDate.setHours(this.endTime);
                    endDate.setMinutes(0);
                    endDate.setSeconds(0);

                    startDate = AppoinmentDateUtils.toUtc(startDate);
                    endDate = AppoinmentDateUtils.toUtc(endDate);

                    const obj = {
                        startDate: startDate,
                        endDate: endDate,
                        locationId: null,
                        practitionerId: '',
                        name: null,
                        title: value.summary,
                        note: '',
                        isAllDay: false,
                        recurrenceRule: '',
                        recurrenceException: null,
                        status: true,
                        bookOnline: false,
                        type: AppointmentType.Holiday,
                        breakTypeId: null,
                        frequencyType: 0,
                        allDay: true,
                        isHoliday: true
                    } as unknown as Schedule;
                    data.push(obj);
                });
                this.holidayList = data;
                this.dataSource = this.dataSource.concat(data);
            }
        });
    }

    get clickedPttrName() {
        return this.addEditScheduleData && this.getPttrNameFromId(this.addEditScheduleData.practitionerId);
    }

    get clickedCellTime() {
        return this.addEditScheduleData && AppoinmentDateUtils.formatDateTimeDefault(this.addEditScheduleData._startDate, 'hh:mm A', false);
    }

    get getSchedulerView() {
        let view = 'day';
        if (this.schedulerSettings) {
            if (this.schedulerSettings.schedulerView == SchedulerViewEnum.Day) {
                view = 'day';
            } else if (this.schedulerSettings.schedulerView == SchedulerViewEnum.MF) {
                view = 'workWeek';
            } else if (this.schedulerSettings.schedulerView == SchedulerViewEnum.Week) {
                view = 'week';
            }
        }
        return view;
    }

    get getSchedulerGroupByPractitioner() {
        return this.schedulerSettings && this.schedulerSettings.groupBy == SchedulerGroupByenum.Default;
    }

    get isCustomPractitionerSort() {
        return this.schedulerSettings && this.schedulerSettings.isCustomPractitionerSort;
    }

    get isShowPractitionerStatic() {
        return this.schedulerSettings && this.schedulerSettings.isShowPractitionerStatic;
    }

    get isShowPublicHoliday() {
        return this.schedulerSettings && this.schedulerSettings.isShowPublicHoliday;
    }

    get isShowHourLineLabel() {
        return this.schedulerSettings && this.schedulerSettings.isShowHourLineLabel;
    }

    get isShowMinutesLineLabel() {
        return this.schedulerSettings && this.schedulerSettings.isShowMinutesLineLabel;
    }

    get isShowTimeToolTip() {
        return this.schedulerSettings && this.schedulerSettings.isShowTimeToolTip;
    }

    get isShowPatientName() {
        return this.schedulerSettings && this.schedulerSettings.isShowPatientName;
    }

    get completedAppointmentColor() {
        return this.schedulerSettings && this.schedulerSettings.completedApoointmentColor ? this.schedulerSettings.completedApoointmentColor : 'gray';
    }

    get isShowAllDayPanel() {
        return this.isShowPublicHoliday && this.isDisplayAllDayPanel;
    }

    private addHolidayToScheduler() {
        if (!this.isShowPublicHoliday) {
            return;
        }
        if (this.dataSource) {
            const list = [];
            const dates = [];
            let count = 1;
            this.dataSource.forEach(value => {
                let isMatched = false;
                if (this.currentView != 'day') {
                    const startD = this.manageScheduleService.selectedDateArray[0];
                    const endD = this.manageScheduleService.selectedDateArray[this.manageScheduleService.selectedDateArray.length - 1];
                    if (DateUtils.isBetween(startD, endD, value.startDate) && value.type == this.TYPE_HOLIDAY) {
                        value.practitionerId = this.resourceIds[0];
                        list.push(value);
                        isMatched = true;
                    }
                } else {
                    if (DateUtils.isEqual(this.manageScheduleService.currentDate.getValue(), value.startDate) && value.type == this.TYPE_HOLIDAY) {
                        value.practitionerId = this.resourceIds[0];
                        list.push(value);
                        isMatched = true;
                    }
                }
                if (isMatched) {
                    if (dates.includes(value.startDate)) {
                        count++;
                    } else {
                        dates.push(value.startDate);
                    }
                }
            });

            const style = '.appoinment-sdlr.all-day .dx-scheduler-header-scrollable{\n' +
                '      height: ' + (94 + (count * 20) + count) + 'px !important;\n' +
                '    }';

            this.findOrCreatStyleTag(style, 'dynamicSchedulerHeader');
            this.isDisplayAllDayPanel = list.length > 0;
            this.renderSchedular();
        }
    }


    preventToolTip($event) {
        $event.stopPropagation();
    }

    popupBtnClick($event) {
        if (['1', '2', '3', '4', '6', '7'].indexOf($event) >= 0) {
            this.appointmentClicked = $event;
        }
    }

    apptVerticleLineColor(data: any) {
        if (data.type == 2) {
            const apptTypeColor = data['appointmentsExtraDetails']['appointmentTypesColor'];
            if (!apptTypeColor) {
                let color = this.apptColorByStaff[data['practitionerId']];
                if (color) {
                    color = color.replace(/[^\d,]/g, '').split(',');
                }
                return invert(color);
            }
            return apptTypeColor;
        }
        return '';
    }

    getApptStatus(data: Appointment): AppointmentStatus {
        const d = this.getAppointmentEntry(data);
        return d ? d.status : AppointmentStatus.Pending;
    }

    getApptConfStatus(data: Appointment): AppointmentConfirmationStatus {
        const d = this.getAppointmentEntry(data);
        return d ? d.confirmationStatus : AppointmentConfirmationStatus.Unconfirmed;
    }

    getConfStatusFlag(data: Appointment): string {
        const confStatus = this.getApptConfStatus(data);
        const status = this.getApptStatus(data);

        if (confStatus == AppointmentConfirmationStatus.Confirmed || status == AppointmentStatus.Completed) {
            return './assets/scheduler-legends/confirmed.png';
        } else if (confStatus == AppointmentConfirmationStatus.RepliedNo) {
            return './assets/scheduler-legends/replied_no.png';
        } else if (confStatus == AppointmentConfirmationStatus.WaitingRoomArrived) {
            return './assets/scheduler-legends/waiting_room_arrived.png';
        } else if (confStatus == AppointmentConfirmationStatus.WaitingRoomDismissed) {
            return './assets/scheduler-legends/waiting_room_dismissed.png';
        }
        return './assets/scheduler-legends/unconfirmed.png';
    }

    isNeedAttention(data: Appointment): boolean {
        const d = this.getAppointmentEntry(data);
        return d ? d.isNeedAttention : false;
    }

    getAppointmentEntry(data: Appointment): AppointmentEntry | null {
        if (data.appointmentsEntry && data.appointmentsEntry.length > 0) {
            for (let appointmentEntry of data.appointmentsEntry) {
                if (AppoinmentDateUtils.isEqual(data.startDate, appointmentEntry.startDateUTC)) {
                    return appointmentEntry;
                }
            }
        } else {
            return null;
        }
    }

    toggleWaitingRoomTray() {
        this.isOpenWaitingRoomTry = !this.isOpenWaitingRoomTry;
        if (this.isOpenWaitingRoomTry) {
            this.waitingRoomAppts = this.getAppointments();
        }
        this.calculateWaitingListCount();
    }

    /** CREATE NEW APPOINTMENT ON CLICK */
    private addAppointment(data, added?, undoIfError = false) {
        this.blockUI.start();
        this.appointmentService.createAppointment(data).subscribe(response => {
            if (response.response.toLocaleLowerCase() === 'success') {
                this.alertService.displayErrorMessage(`Appointment ${this.apptHandlar.rebookMoveAppt.getValue() == REBOOK ? 're-booked' : 'moved'} successfully.`);
                const _data = new Appointment(response.appointmentsModel);
                let nData = this.prepareEmitData(AppUtils.refrenceClone(_data), Constant.ADD, false, true, Constant.ADD);
                this.saveSchedule(nData);
                this.apptHandlar.notifyRebookMove(NONE);
                this.waitlistManageService.bookAppointmentSubject.next(false);
            } else if (response.response.toLocaleLowerCase() === 'error') {
                this.dialogService.open(this.appointmentConflict, {
                    title: 'Appointment Availability Conflicts',
                    width: '738px',
                    height: '250px',
                    hideCancel: true,
                    saveBtnTitle: 'Ok'
                });
            }
            this.blockUI.stop();
        }, error => {
            this.blockUI.stop();
            if (undoIfError) {
                this.undoLastUpdate();
            }
            this.alertService.displayErrorMessage(error || `Failed to ${this.apptHandlar.rebookMoveAppt.getValue() == REBOOK ? 're-booked' : 'moved'} appointment.`);
        });
    }

    private updateAppointment(data, preparedData, isAddNew = true) {
        this.blockUI.start();
        this.isErorrOnMoveRebookAppt = false;
        this.rebookMoveOldAppt = data;
        this.appointmentService.updateAppointment(data).subscribe(response => {
            if (response.response.toLocaleLowerCase() === 'success') {
                if (isAddNew) {
                    this.addAppointment(preparedData, data, true);
                }
                const _data = new Appointment(response.appointmentsModel);
                let nData = this.prepareEmitData(AppUtils.refrenceClone(_data), Constant.EDIT, false, true, Constant.EDIT, false);
                this.saveSchedule(nData);
                if (!isAddNew) {
                    this.apptHandlar.notifyRebookMove(NONE);
                }
            } else if (response.response.toLocaleLowerCase() === 'error') {
                this.dialogService.open(this.appointmentConflict, {
                    title: 'Appointment Availability Conflicts',
                    width: '738px',
                    height: '250px',
                    hideCancel: true,
                    saveBtnTitle: 'Ok'
                });
                this.isErorrOnMoveRebookAppt = true;
            }
            this.blockUI.stop();
        }, error => {
            this.blockUI.stop();
            this.isErorrOnMoveRebookAppt = true;
            this.alertService.displayErrorMessage(error || 'Failed to move appointment.');
        });
    }

    private undoLastUpdate() {
        if (this.isErorrOnMoveRebookAppt && this.rebookMoveOldAppt) {
            this.blockUI.start();
            this.isErorrOnMoveRebookAppt = false;
            this.appointmentService.updateAppointment(this.rebookMoveOldAppt).subscribe(response => {
                this.rebookMoveOldAppt = null;
                if (response.response.toLocaleLowerCase() === 'success') {
                    const _data = new Appointment(response.appointmentsModel);
                    let nData = this.prepareEmitData(AppUtils.refrenceClone(_data), Constant.EDIT, false, true, Constant.EDIT, false);
                    this.saveSchedule(nData);
                    this.rebookMoveOldAppt = null;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
    }

    private calculateWaitingListCount() {
        this.waitingRoomCount = 0;
        const pIds = this.resource.map(p => p.id);
        this.getAppointments().forEach(appointment => {
            appointment.appointmentsEntry.forEach(d => {
                if (d.confirmationStatus == AppointmentConfirmationStatus.WaitingRoomArrived && pIds.indexOf(d.practitionerId) > -1) {
                    this.waitingRoomCount++;
                }
            });
        });
    }

    private getWaitList() {
        this.waitListService.getAllPatientWaitListByLocation(this.appState.selectedUserLocationId, null, null).subscribe(response => {
            if (response) {
                response = response.map(data => {
                    data.preferredDaysData = JSON.parse(data.preferredDaysData);
                    data['practitionerIds'] = data.practitionerForPatientWaitList.map(x => x.practitionerId);
                    data.practitionerName = data.practitionerForPatientWaitList.map(x => x.practitionerName).join(', ');
                    return data;
                });
                this.waitList = response;
                this.waitListFiltered = AppUtils.refrenceClone(this.waitList);
            }
        }, error => {
            this.alertService.displayErrorMessage(error);
        });
    }

    prepareEmitData(data, type, isError: boolean, isHidePopup: boolean, secondaryType: string, isNotify = true) {
        let emitData = {};
        emitData['data'] = data;
        emitData['type'] = type;
        emitData['isError'] = isError;
        emitData['isHidePopup'] = isHidePopup;
        emitData['secondaryType'] = secondaryType;
        emitData['isNotify'] = isNotify;
        return emitData;
    }

    getPractitionerImage(practitionerId: string, initial = false): string {
        const practitioner = this.practitioners.find(x => x.id = practitionerId);
        if (practitioner && !initial) {
            return practitioner.photo;
        }
        if (initial) {
            return practitioner.initial;
        }
        return '';
    }
}

