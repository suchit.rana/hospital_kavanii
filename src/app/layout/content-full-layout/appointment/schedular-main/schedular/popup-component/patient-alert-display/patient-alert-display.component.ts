import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {BaseGridComponent} from '../../../../../shared-content-full-layout/base-grid/base-grid.component';
import {AppDialogService} from '../../../../../../../shared/services/app-dialog.service';
import {IPatientAlert, PatientAppointmentService} from '../../../../../patients/rest-service/app.patient-appointment.service';
import {AppointmentAlertComponent} from '../../../../../patients/patient-appointments/appointment-popup/appointment-alert/appointment-alert.component';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {SAVE} from '../../../../../../../shared/popup-component/popup-component.component';

@Component({
  selector: 'app-patient-alert-display',
  templateUrl: './patient-alert-display.component.html',
  styleUrls: ['./patient-alert-display.component.css']
})
export class PatientAlertDisplayComponent extends BaseGridComponent implements OnInit, OnChanges {

  @Input()
  data: any[] = [];

  @ViewChild('deleteConfirm', {static: false}) deleteConfirm;

  constructor(
    private dialogRef: DialogRef,
    private patientAppointmentService: PatientAppointmentService,
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
  ) {
    super();
  }

  ngOnInit() {
    this.gridData = this.data;
    this.state.filter = null;
    this.loadItems();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data') && this.data) {
      this.gridData = this.data;
      this.state.filter = null;
      this.loadItems();
    }
  }

  updatePlannedService(dataItem: IPatientAlert, isDelete = false) {
    if (isDelete) {
      this.appDialogService.open(this.deleteConfirm, {
        title: 'DO YOU WISH TO DELETE THIS ALERT?',
        width: '660px',
        cancelBtnTitle: 'No',
        saveBtnTitle: 'Yes',
      }).close.subscribe((value) => {
        if (value == SAVE) {
          dataItem.message = '';
          this.patientAppointmentService.updatePatientAlert(dataItem).subscribe(response => {
            this.dialogRef.close();
            this.loadItem(dataItem.patientId);
          }, error => {
            this.alertService.displayErrorMessage(error || 'Something went wrong. Try Again.');
          });
        }
      });
    } else {
      this.addEditPlannedService(dataItem);
    }
  }

  addEditPlannedService(dataItem: IPatientAlert) {
    this.appDialogService.open(AppointmentAlertComponent, {
      title: 'Appointment Alert',
      width: '660px',
      hideOnSave: false,
      hideOnCancel: false,
      hideFooter: true,
      data: {patientId: dataItem.patientId}
    }).close.subscribe(() => {
      this.loadItem(dataItem.patientId);
    });
  }

  loadItem(patientId: string) {
    this.patientAppointmentService.getPatientAlerts(patientId).subscribe(response => {
      if (response && response.length > 0 && response[0].message) {
        this.data = response;
        this.gridData = this.data;
        this.state.filter = null;
        this.loadItems();
      }
    });
  }
}
