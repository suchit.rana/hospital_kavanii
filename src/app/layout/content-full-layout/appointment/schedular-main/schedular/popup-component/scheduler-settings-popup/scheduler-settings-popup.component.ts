import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SchedulerViewEnum} from '../../../../../../../enum/scheduler-view-enum';
import {SchedulerGroupByenum} from '../../../../../../../enum/scheduler-group-byenum';
import {SchedulerSettingsService} from '../../../../../../../services/app.scheduler-settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BaseItemComponent} from '../../../../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {SchedulerSettings} from '../../../../../../../models/scheduler-settings';
import {AppState} from '../../../../../../../app.state';
import {RP_MODULE_MAP} from '../../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-scheduler-settings-popup',
  templateUrl: './scheduler-settings-popup.component.html',
  styleUrls: ['./scheduler-settings-popup.component.css']
})
export class SchedulerSettingsPopupComponent extends BaseItemComponent implements OnInit {
  readonly APPT_SCHEDULE_SETTINGS = RP_MODULE_MAP.appointment_schedule_settings;
  moduleName = this.APPT_SCHEDULE_SETTINGS;

  formGroup: FormGroup;
  apperance = 'outline';
  schedulerGroups: { name: string, value: SchedulerGroupByenum }[] = [];
  schedulerViews: { name: string, value: SchedulerViewEnum }[] = [];
  isOpenResetConfirm: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: SchedulerSettings,
    public dialogRef: MatDialogRef<SchedulerSettingsPopupComponent>,
    private fb: FormBuilder,
    private schedulerSettingsService: SchedulerSettingsService,
    public location: Location,
    protected appState: AppState
  ) {
    super(location);
    this.schedulerGroups.push({name: 'Date & Practitioner', value: SchedulerGroupByenum.Default});
    this.schedulerGroups.push({name: 'Practitioner & Date', value: SchedulerGroupByenum.Practitioner});

    this.schedulerViews.push({name: 'Day View', value: SchedulerViewEnum.Day});
    this.schedulerViews.push({name: 'M-F View', value: SchedulerViewEnum.MF});
    this.schedulerViews.push({name: 'Week View', value: SchedulerViewEnum.Week});
  }

  ngOnInit() {
    this.createFormGroup();
    this.getSchedulerSettings();
    if (this.data && this.data.id) {
      this.formGroup.patchValue(this.data);
    }
  }

  private createFormGroup() {
    this.formGroup = this.fb.group({
      id: new FormControl(''),
      schedulerView: new FormControl(SchedulerViewEnum.Day),
      groupBy: new FormControl(SchedulerGroupByenum.Default),
      isCustomPractitionerSort: new FormControl(false),
      isShowPractitionerStatic: new FormControl(true),
      isShowPublicHoliday: new FormControl(true),
      isShowHourLineLabel: new FormControl(true),
      isShowMinutesLineLabel: new FormControl(false),
      isShowTimeToolTip: new FormControl(true),
      isShowPatientName: new FormControl(true),
      completedApoointmentColor: new FormControl('rgba(173,173,173,1)')
    });
  }

  upsertSchedulerSettings() {
    if (this.id) {
      this.updateSchedulerSettings();
    } else {
      this.saveSchedulerSettings();
    }
  }

  resetForm() {
    this.formGroup.reset({
      id: this.formGroup.get('id').value,
      schedulerView: SchedulerViewEnum.Day,
      groupBy: SchedulerGroupByenum.Default,
      isCustomPractitionerSort: false,
      isShowPractitionerStatic: true,
      isShowPublicHoliday: true,
      isShowHourLineLabel: true,
      isShowMinutesLineLabel: false,
      isShowTimeToolTip: true,
      isShowPatientName: true,
      completedApoointmentColor: 'rgba(173,173,173,1)'
    });
    this.isOpenResetConfirm = false;
  }

  confirmReset() {
    this.isOpenResetConfirm = true;
  }

  private getSchedulerSettings() {
    const userId = this.appState.UserProfile.id || null;
    this.schedulerSettingsService.getSchedulerSettings(userId).subscribe(response => {
      if (response) {
        this.formGroup.patchValue(response);
      }
    }, error => {

    });
  }

  private saveSchedulerSettings() {
    const data = this.formGroup.value;
    delete data.id;
    this.schedulerSettingsService.saveSchedulerSettings(data).subscribe(response => {
      if (response) {
        this.dialogRef.close(SchedulerSettingsPopupComponent.prepareDialogCloseData(false, this.formGroup.value));
      }
    }, error => {
      this.dialogRef.close(SchedulerSettingsPopupComponent.prepareDialogCloseData(false, null, true));
    });
  }

  private updateSchedulerSettings() {
    this.schedulerSettingsService.updateSchedulerSettings(this.formGroup.value).subscribe(response => {
      if (response) {
        this.dialogRef.close(SchedulerSettingsPopupComponent.prepareDialogCloseData(true, this.formGroup.value));
      }
    }, error => {
      this.dialogRef.close(SchedulerSettingsPopupComponent.prepareDialogCloseData(true, null, true));
    });
  }

  closeDialog() {
    this.dialogRef.close(SchedulerSettingsPopupComponent.prepareDialogCloseData(false, null, false, true));
  }

  get id() {
    return this.data && this.data.id ? this.data.id : null;
  }

  private static prepareDialogCloseData(isUpdate: boolean, data: any, hasError: boolean = false, isClose: boolean = false) {
    return {isUpdate: isUpdate, data: data, hasError: hasError, isClose: isClose};
  }

}
