import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {PractitionerSpecialityModel} from '../../../../../../../models/app.staff.model';
import {SpecialityModel} from '../../../../../../../models/app.settings.model';
import {StaffService} from '../../../../../../../services/app.staff.service';
import {SettingsService} from '../../../../../../../services/app.settings.service';
import {AppState} from '../../../../../../../app.state';
import {AppoinmentDateUtils} from '../../../../AppoinmentDateUtils';
import {AvailableTime, IPaDates} from '../../interface/pratitioner-availability';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DxPopoverComponent} from 'devextreme-angular';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {PractitionerAvailabilityService} from '../../rest-service/practitioner-availability.service';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {AppointmentType} from '../../../../../../../enum/application-data-enum';
import {AppointmentHandlerService} from '../../service/appointment-handler.service';
import {Schedule} from '../../../../../../../models/app.appointment.model';
import {DateUtils} from '../../../../../../../shared/DateUtils';

@Component({
    selector: 'app-practitioner-availability',
    templateUrl: './practitioner-availability.component.html',
    styleUrls: ['./practitioner-availability.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PractitionerAvailabilityComponent implements OnInit {
    readonly CTRL_SPECIALITY = 'CTRL_SPECIALITY';


    @Input()
    slotSize: number;
    @Input()
    isPatientOnlineBooking: boolean;

    constructor(
        private appState: AppState,
        private alertService: AppAlertService,
        private settingsService: SettingsService,
        private staffService: StaffService,
        private practitionerAvailabilityService: PractitionerAvailabilityService,
        private apptHandlar: AppointmentHandlerService,
        private dialogRef: DialogRef
    ) {
    }

    @ViewChild('practitionerSelectPopup', {static: true}) practitionerSelectPopup: DxPopoverComponent;

    readonly days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    appearance = 'outline';

    specialities: SpecialityModel[] = [];
    practitioners: PractitionerSpecialityModel[] = [];
    practitionersFiltered: PractitionerSpecialityModel[] = [];
    availablePractitioners: { practitionerId: string; practitionerName: string }[] = [];
    slots: { text: string, value: number }[] = [];

    dates: IPaDates[] = [];
    thisMonth = new Date().getMonth() + 1;
    currentMonth = new Date().getMonth() + 1;
    currentYear = new Date().getFullYear();
    currentDate = new Date(this.currentYear, (this.currentMonth-1));

    clickedCellIndex = -1;
    clickedCell: IPaDates = null;
    isOpenPractitionerSelect = false;
    clickedElem: HTMLElement;
    clickedTime: AvailableTime;
    interval: any;
    loading: boolean = false;

    formGroup = new FormGroup({
        specialityId: new FormControl('', Validators.required),
        practitionerId: new FormControl('', Validators.required),
        slot: new FormControl('', Validators.required),
    });

    ngOnInit(): void {
        this.init();
    }

    nextPrevDate(i: number) {
        if (this.interval) {
            clearTimeout(this.interval);
        }
        const date = AppoinmentDateUtils.addAndGetNewValue(new Date(this.currentYear, this.currentMonth - 1), i, 'months');
        this.currentYear = new Date(date).getFullYear();
        this.currentMonth = new Date(date).getMonth() + 1;
        this.currentDate = new Date(this.currentYear, (this.currentMonth - 1));

        if (!this.errorMsg) {
            this.interval = setTimeout(() => {
                this.checkAvailability();
            }, 500);
        }
    }

    clickDateCell(i: number, date: IPaDates) {
        if (i === this.clickedCellIndex || (date && !date.morningTimes && !date.eveningTimes && !date.afterNoonTimes)) {
            this.clickedCellIndex = -1;
            this.clickedCell = null;
            return;
        }
        this.clickedCellIndex = i;
        this.clickedCell = date;
    }

    bookAppointment(time: AvailableTime, $event: MouseEvent) {
        this.clickedTime = time;
        if (this.formGroup.get('practitionerId').value === '00000000-0000-0000-0000-000000000000') {
            this.clickedElem = $event.target as HTMLElement;
            this.clickedElem.style.width = '200px';
            this.practitionerSelectPopup.target = this.clickedElem;
            this.practitionerSelectPopup.instance.show();
            this.isOpenPractitionerSelect = true;
            this.availablePractitioners = time.practitioners;
            return;
        }
        this.availablePractitioners = [];
        this.apptHandlar.openApptDialogSubject.next(this.prepareApptData(this.formGroup.get('practitionerId').value));
        this.dialogRef.close();
    }

    autoAdjust() {
        if (this.clickedElem) {
            this.clickedElem.style.width = '66px';
        }
        this.isOpenPractitionerSelect = false;
    }

    onSelectPractitioner($event: string) {
        this.apptHandlar.openApptDialogSubject.next(this.prepareApptData($event));
        this.dialogRef.close();
    }

    onSelectionChange(ctrl?: string) {
        if (ctrl === this.CTRL_SPECIALITY) {
            this.formGroup.get('practitionerId').setValue('');
            const specialityId = this.formGroup.get('specialityId').value;
            this.practitionersFiltered = this.practitioners.filter(p => (p.practitionerRoleSpecialityModel.filter(ps => ps.specialityId === specialityId).length > 0 && p.status) || p['isAll']);
        }

        if (!this.errorMsg) {
            this.checkAvailability();
        }
    }

    dateChange($event: Date) {
        if ($event) {
            const date = new Date($event);
            if (this.interval) {
                clearTimeout(this.interval);
            }
            this.currentMonth = date.getMonth() + 1;
            this.currentYear = date.getFullYear();

            if (!this.errorMsg) {
                this.interval = setTimeout(() => {
                    this.checkAvailability();
                }, 500);
            }
        }
    }

    private init() {
        this.prepareSlots();
        this.getSpeciality();
        this.getPractitioners();
    }

    private prepareDates() {
        const startDay = AppoinmentDateUtils.getInstance(new Date(this.currentYear, this.currentMonth - 1)).startOf('month').format('ddd');
        const daysToadd = this.days.indexOf(startDay) + (new Date(this.dates[0].date).getDate() - 1);
        for (let i = 0; i < daysToadd; i++) {
            this.dates.insert(i, null);
        }
        const needRows = (Math.floor(this.dates.length / 7)) + 1;
        const extraCell = (needRows * 7) - this.dates.length;
        for (let i = 1; i <= extraCell; i++) {
            this.dates.insert(this.dates.length + i, null);
        }
    }

    private prepareSlots() {
        let minutes = 0;
        while (minutes < 180) {
            minutes += this.slotSize;
            const hour = Math.floor(minutes / 60);
            const minute = minutes - (hour * 60);
            this.slots.push({
                text: String(hour.toFixed(0)).padStart(2, '0') + ':' + String(minute.toFixed(0)).padStart(2, '0'),
                value: minutes
            });
        }
    }

    private getSpeciality() {
        this.settingsService.getAllSpecialtiesByLocationId(this.appState.selectedUserLocationId).subscribe(response => {
            if (response) {
                this.specialities = response;
            }
        });
    }

    private getPractitioners() {
        this.staffService.getAllPractitionerSpecialityByLocationIdForCalendar(this.appState.selectedUserLocationId).subscribe(response => {
            if (response) {
                this.practitioners = response;
                this.practitioners.insert(0, {
                    practitionerId: '00000000-0000-0000-0000-000000000000',
                    nickName: 'Any Practitioner',
                    practitionerRoleSpecialityModel: [],
                    isAll: true
                });
            }
        });
    }

    private checkAvailability() {
        this.loading = true;
        const date = AppoinmentDateUtils.toUtc(new Date());
        const data = {...this.formGroup.value, locationId: this.appState.selectedUserLocationId, date};
        data.practitionerId = data.practitionerId === '00000000-0000-0000-0000-000000000000' ? '' : data.practitionerId;
        this.practitionerAvailabilityService.get(data).subscribe((response) => {
            this.dates = response;
            this.prepareDates();
            this.loading = false;
        }, error => {
            this.loading = false;
            this.alertService.displayErrorMessage(error);
        });
    }

    private prepareApptData(practitionerId: string): Schedule {
        const startDate = new Date(this.clickedCell.date);

        const selectedStartTime = DateUtils.getInstance(this.clickedTime.time, 'HH:mm A');
        startDate.setHours(parseInt(selectedStartTime.format('HH')), parseInt(selectedStartTime.format('mm')), parseInt(selectedStartTime.format('ss')));

        const endDate = DateUtils._toDate(AppoinmentDateUtils.addAndGetNewValue(startDate, this.formGroup.get('slot').value, 'minutes'));

        return {
            'id': null,
            'startDateUTC': startDate,
            'endDateUTC': endDate,
            'frequencyType': undefined,
            'recurrenceRule': '',
            'recurrenceID': '',
            'bookOnline': this.isPatientOnlineBooking,
            'note': '',
            'locationId': this.appState.selectedUserLocationId,
            'practitionerId': practitionerId,
            '_startDate': startDate,
            '_endDate': endDate,
            'type': AppointmentType.APPOINMENT,
            'isManualSelection': false
        } as unknown as Schedule;
    }

    get errorMsg(): string {
        let msg = null;
        const isSpecialityEmpty = !this.formGroup.get('specialityId').value;
        const isPttrEmpty = !this.formGroup.get('practitionerId').value;
        const isDurationEmpty = !this.formGroup.get('slot').value;

        if (isSpecialityEmpty || isPttrEmpty || isDurationEmpty) {
            msg = 'Please select a ';
        }

        if (isSpecialityEmpty) {
            msg += 'Speciality';
        }

        if (isPttrEmpty) {
            if (isSpecialityEmpty) {
                msg += ', ';
            }
            msg += 'Practitioner';
        }

        if (isDurationEmpty) {
            if (isSpecialityEmpty || isPttrEmpty) {
                msg += '& ';
            }
            msg += 'Duration';
        }

        return msg;
    }

    get month(): string {
        return [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ][this.currentMonth - 1];
    }
}
