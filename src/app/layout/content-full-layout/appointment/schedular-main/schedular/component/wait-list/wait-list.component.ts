import {Component, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {AddEditWaitListComponent} from './add-edit-wait-list/add-edit-wait-list.component';
import {AppDialogService} from '../../../../../../../shared/services/app-dialog.service';
import {PractitionerSpecialityModel} from '../../../../../../../models/app.staff.model';
import {WaitListService} from '../../rest-service/wait-list.service';
import {AppState} from '../../../../../../../app.state';
import {WaitList, WaitListStatus} from '../../interface/wait-list';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {FormControl} from '@angular/forms';
import {FilterPractitionerComponent} from './filter-practitioner/filter-practitioner.component';
import {AppUtils} from '../../../../../../../shared/AppUtils';
import {DateUtils} from '../../../../../../../shared/DateUtils';
import {WaitlistManageService} from '../../service/waitlist-manage.service';
import {SAVE} from '../../../../../../../shared/popup-component/popup-component.component';
import {PatientModel} from '../../../../../../../models/app.patient.model';
import {ExportWaitListComponent} from './export-waitlist/export-wait-list.component';
import {StaffService} from '../../../../../../../services/app.staff.service';
import {RP_MODULE_MAP} from '../../../../../../../shared/model/RolesAndPermissionModuleMap';
import {hasPermission} from '../../../../../../../app.component';

@Component({
  selector: 'app-wait-list',
  templateUrl: './wait-list.component.html',
  styleUrls: ['./wait-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WaitListComponent implements OnInit {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly FROM_DATE = 'FROM_DATE';
  readonly TO_DATE = 'TO_DATE';
  readonly DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  readonly PTTR = 'PTTR';
  readonly ED = 'ED';
  readonly PD = 'PD';


  @Output()
  cancel: EventEmitter<any> = new EventEmitter();

  @Output()
  refresh: EventEmitter<WaitList[]> = new EventEmitter();

  @ViewChild('dateFilter', {static: false}) dateFilter;
  @ViewChild('pDaysTemplate', {static: false}) pDaysTemplate;
  @ViewChild('deleteConfirm', {static: false}) deleteConfirm;

  apperance = 'outline';
  isDisplayClear: boolean = false;
  isLoading: boolean = false;
  waitList: WaitList[] = [];
  waitListFiltered: WaitList[] = [];
  selectedPDays: { Mon: boolean, Tue: boolean, Wed: boolean, Thu: boolean, Fri: boolean, Sat: boolean, Sun: boolean } = {} as any;
  isWaitListPopupOpen: boolean = false;
  practitioner: PractitionerSpecialityModel[];

  /** Date filter control */
  fromDateCtrl = new FormControl();
  toDateCtrl = new FormControl();

  constructor(
    private appState: AppState,
    private alertService: AppAlertService,
    private dialogService: AppDialogService,
    private waitListService: WaitListService,
    private waitlistManageService: WaitlistManageService,
    private staffService: StaffService
  ) {
    this.DAYS.forEach(day => {
      this.selectedPDays[day] = true;
    });
  }

  ngOnInit() {
    this.getWaitList();
    this._listPractitioner();
  }

  filter(action: string) {
    this.isWaitListPopupOpen = false;
    switch (action) {
      case this.PTTR:
        this.openPractitionerPopup();
        break;
      case this.ED:
        this.openExpiryDatePopup();
        break;
      case this.PD:
        this.openPreferredDaysPopup();
        break;
    }
  }

  bookNow(waitList: WaitList) {
    waitList.status = WaitListStatus.Booked;
    this.waitlistManageService.waitList = waitList;
    this.waitlistManageService.bookAppointmentSubject.next(true);
  }

  clearFilter() {
    this.isDisplayClear = false;
    this.fromDateCtrl.setValue(null);
    this.toDateCtrl.setValue(null);
    Object.keys(this.selectedPDays).forEach(key => {
      this.selectedPDays[key] = false;
    });
    this.waitListFiltered = AppUtils.refrenceClone(this.waitList);
  }

  exportWaitList() {
    this.dialogService.open(ExportWaitListComponent, {
      title: 'Export Wait List',
      width: '400px',
      height: '555px',
      hideFooter: true,
      data: {'apptList': this.waitListFiltered}
    });
  }

  /** Close Popup */
  onCancel() {
    this.cancel.emit();
  }

  addEditWaitList(data?: any) {
    if (!hasPermission(this.appState, this.APPT_BASIC, ['C', 'E'])) {
      return;
    }
    AddEditWaitListComponent.open(this.dialogService, data, this.practitioner).close.subscribe(clickedBtn => {
      if (clickedBtn === SAVE) {
        this.getWaitList();
      }
    });
  }

  delete(id: string) {
    if (!hasPermission(this.appState, this.APPT_BASIC, 'D')) {
      return;
    }
    this.dialogService.open(this.deleteConfirm, {
      title: 'Do you wish to remove this patient from Wait List?',
      width: '778px',
      height: '299px',
      cancelBtnTitle: 'No',
      saveBtnTitle: 'Yes',
    }).close.subscribe((value) => {
      if (value == SAVE) {
        this.isLoading = true;
        this.waitListService.deleteWaitList(id).subscribe(response => {
          if (response) {
            const index = this.waitListFiltered.findIndex(x => x.id === id);
            this.waitListFiltered.splice(index, 1);
            const wlIndex = this.waitList.findIndex(x => x.id === id);
            this.waitList.splice(wlIndex, 1);
            this.alertService.displaySuccessMessage('Patient removed from Wait List successfully');
          } else {
            this.alertService.displayErrorMessage('Failed to remove patient from Wait List. Try Again.');
          }
          this.isLoading = false;
        }, error => {
          this.alertService.displayErrorMessage(error || 'Failed to remove patient from Wait List. Try Again.');
          this.isLoading = false;
        });
      }
    });
  }

  onDateSelect($event: Date | number | string, from: string) {
    if (from == this.FROM_DATE) {
      this.fromDateCtrl.setValue(new Date($event));
    } else {
      this.toDateCtrl.setValue(new Date($event));
    }
  }

  onSelectPatient($event: PatientModel) {
    if ($event) {
      this.isDisplayClear = true;
      this.waitListFiltered = this.waitList.filter(x => x.patientId === $event.id);
    } else {
      this.clearFilter();
    }
  }

  openPractitionerPopup() {
    FilterPractitionerComponent.open(this.dialogService, this.practitioner).close.subscribe((result) => {
      if (result['action'] === SAVE) {
        this.filterByPractitioner(result['data']);
      }
    });
  }

  openExpiryDatePopup() {
    this.dialogService.open(this.dateFilter, {
      title: 'Filter BY DATE RANGE',
      width: '420px',
      saveBtnTitle: 'Apply'
    }).clickSave.subscribe(() => {
      this.filterByExpiryDate();
    });
  }

  openPreferredDaysPopup() {
    this.dialogService.open(this.pDaysTemplate, {
      title: 'Filter BY Preferred Days',
      width: '420px',
      saveBtnTitle: 'Apply'
    }).clickSave.subscribe(() => {
      this.filterByPfDays();
    });
  }

  togglePDays($event, day: string) {
    this.selectedPDays[day] = $event.target.checked;
  }

  get fromDateCtrlVal() {
    return this.fromDateCtrl.value || new Date();
  }

  get toDateCtrlVal() {
    return this.toDateCtrl.value || new Date();
  }

  private getWaitList() {
    this.isLoading = true;
    this.waitListService.getAllPatientWaitListByLocation(this.appState.selectedUserLocationId, this.fromDateCtrl.value, this.toDateCtrl.value).subscribe(response => {
      if (response) {
        response = response.map(data => {
          data.preferredDaysData = JSON.parse(data.preferredDaysData);
          data['practitionerIds'] = data.practitionerForPatientWaitList.map(x => x.practitionerId);
          data.practitionerName = data.practitionerForPatientWaitList.map(x => x.practitionerName).join(', ');
          return data;
        });
        this.waitList = response;
        this.waitListFiltered = AppUtils.refrenceClone(this.waitList);
        this.refresh.emit(this.waitList);
      }
      this.isLoading = false;
    }, error => {
      this.alertService.displayErrorMessage(error);
      this.isLoading = false;
    });
  }

  private filterByPractitioner(practitionerIds: string[]) {
    this.isDisplayClear = true;
    this.waitListFiltered = this.waitList.filter(x => x['practitionerIds'].filter(y => practitionerIds.indexOf(y) > -1).length > 0);
  }

  private filterByExpiryDate() {
    this.isDisplayClear = true;
    this.waitListFiltered = this.waitList.filter(x => DateUtils.isBetween(this.fromDateCtrl.value, this.toDateCtrl.value, x.expiryDate));
  }

  private filterByPfDays() {
    this.isDisplayClear = true;
    this.waitListFiltered = this.waitList.filter(wl => {
      const found = Object.keys(wl.preferredDaysData).filter(pDays => {
        if (this.selectedPDays[pDays] && (wl.preferredDaysData[pDays].morning || wl.preferredDaysData[pDays].afternoon || wl.preferredDaysData[pDays].evening)) {
          return wl.preferredDaysData[pDays];
        }
      });
      if (found.length > 0) {
        return wl;
      }
    });
  }

  private _listPractitioner() {
    this.staffService.getAllPractitionerSpecialityByLocationIdForCalendar(this.appState.selectedUserLocationId).subscribe(data => {
      data.forEach(value => {
        value.displayName = value.firstName + ' ' + value.lastName;
        value.id = value.practitionerId;
        value.photo = value.photo ? `data:image/jpeg;base64,${value.photo}` : null;
        value.isPractioner = true;
        value.practitionerName = value.nickName;
      });
      this.practitioner = data.filter(p => p.status);

    });
  }

  sortDays = (a, b) => {
    return this.DAYS.indexOf(a.key) > this.DAYS.indexOf(b.key) ? 1 : -1;
  };

}
