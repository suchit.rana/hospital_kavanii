import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../../../../shared/services/app-dialog.service';
import {PractitionerSpecialityModel} from '../../../../../../../../models/app.staff.model';
import {MatCheckboxChange} from '@angular/material';
import {prefixRadialGradient} from 'html2canvas/dist/types/css/types/functions/-prefix-radial-gradient';
import {SAVE} from '../../../../../../../../shared/popup-component/popup-component.component';

@Component({
  selector: 'app-filter-practitioner',
  templateUrl: './filter-practitioner.component.html',
  styleUrls: ['./filter-practitioner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterPractitionerComponent implements OnInit, OnDestroy {

  static ref: IPopupAction;

  @Input()
  practitioners: PractitionerSpecialityModel[] = [];

  selectedPractitionerId: string[] = [];

  constructor() {
  }

  ngOnInit() {
    this.ref.clickSave.subscribe(() => {
      this.ref.closeDialog({action: SAVE, data: this.selectedPractitionerId});
    });
  }

  ngOnDestroy(): void {
    FilterPractitionerComponent.ref = null;
  }

  togglePractitioner($event: MatCheckboxChange) {
    const value = $event.source.value;
    if ($event.checked && this.selectedPractitionerId.indexOf(value) < 0) {
      this.selectedPractitionerId.push(value);
    } else {
      this.selectedPractitionerId.splice(this.selectedPractitionerId.indexOf(value), 1);
    }
  }

  toggleAllPractitioner($event: MatCheckboxChange) {
    if ($event.checked) {
      this.practitioners.forEach(practitioner => {
        if (this.selectedPractitionerId.indexOf(practitioner.practitionerId) < 0) {
          this.selectedPractitionerId.push(practitioner.practitionerId);
        }
      });
    } else {
      this.selectedPractitionerId = [];
    }
  }

  get ref(): IPopupAction {
    return FilterPractitionerComponent.ref;
  }

  static open(dialogService: AppDialogService, practitioner: PractitionerSpecialityModel[]): IPopupAction {
    this.ref = dialogService.open(FilterPractitionerComponent, {
      title: 'Filter By Practitioner',
      width: '400px',
      height: '500px',
      bodyPadding: true,
      saveBtnTitle: 'Apply',
      hideOnSave: false,
      data: {
        practitioners: practitioner
      }
    });
    return this.ref;
  }

}
