import {Component, Input, OnInit} from '@angular/core';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {WaitList} from '../../../interface/wait-list';
import {AppUtils} from '../../../../../../../../shared/AppUtils';

@Component({
    selector: 'app-export-appointments',
    templateUrl: './export-wait-list.component.html',
    styleUrls: ['./export-wait-list.component.css']
})
export class ExportWaitListComponent implements OnInit {

    @Input()
    apptList: WaitList[] = [];

    apptListFiltered: WaitList[] = [];

    selectedItem: WaitList[] = [];

    constructor(
        private dialogRef: DialogRef
    ) {
    }

    ngOnInit() {
        this.apptListFiltered = AppUtils.refrenceClone(this.apptList);
        this.apptListFiltered.forEach((item) => {
            item['isSelected'] = true;
            this.selectedItem.push(item);
        });
    }

    selectItem($event: MouseEvent, index: number) {
        if ($event.ctrlKey || $event.metaKey) {
            const item = this.apptListFiltered[index];
            const hasItem = this.selectedItem.find(value => value.id == item.id);
            if (hasItem) {
                item['isSelected'] = false;
                const selItemIndex = this.selectedItem.findIndex(value => value.id == hasItem.id);
                this.selectedItem.splice(selItemIndex, 1);
            } else {
                item['isSelected'] = true;
                this.selectedItem.push(item);
            }
        }
        if ($event.shiftKey) {
            const lastSelectedItem = this.selectedItem[this.selectedItem.length - 1];
            const lastSelectedItemIndex = this.apptListFiltered.findIndex(value => value.id == lastSelectedItem.id);
            for (let i = lastSelectedItemIndex; i < index; i++) {
                const item = this.apptListFiltered[i];
                const hasItem = this.selectedItem.find(value => value.id == item.id);
                if (!hasItem) {
                    item['isSelected'] = true;
                    this.selectedItem.push(item);
                }
            }
        }
    }

    preview() {
        this.dialogRef.close();
    }
}


