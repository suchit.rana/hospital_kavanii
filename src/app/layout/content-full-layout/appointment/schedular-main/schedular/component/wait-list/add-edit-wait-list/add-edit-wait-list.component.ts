import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../../../../shared/services/app-dialog.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PatientModel} from '../../../../../../../../models/app.patient.model';
import {NavigationExtras, Router} from '@angular/router';
import {PatientService} from '../../../../../../../../services/app.patient.service';
import {For, FromPage} from '../../../../../../../../shared/interface';
import {TreatmentType} from '../../../../../../../../models/app.appointment.model';
import {AppointmentService} from '../../../../../../../../services/app.appointment.service';
import {AppState} from '../../../../../../../../app.state';
import {AppoinmentDateUtils} from '../../../../../AppoinmentDateUtils';
import {PractitionerSpecialityModel} from '../../../../../../../../models/app.staff.model';
import {WaitListService} from '../../../rest-service/wait-list.service';
import {DateUtils} from '../../../../../../../../shared/DateUtils';
import {WaitList, WaitListStatus} from '../../../interface/wait-list';
import {BaseItemComponent} from '../../../../../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {AppAlertService} from '../../../../../../../../shared/services/app-alert.service';
import {SAVE} from '../../../../../../../../shared/popup-component/popup-component.component';

@Component({
  selector: 'app-add-edit-wait-list',
  templateUrl: './add-edit-wait-list.component.html',
  styleUrls: ['./add-edit-wait-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditWaitListComponent extends BaseItemComponent implements OnInit, OnDestroy {

  static ref: IPopupAction;

  @Input()
  data: WaitList;
  @Input()
  selectedPatient: PatientModel;

  practitioner: PractitionerSpecialityModel[] = [];
  days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  selectedPDays: { Mon: boolean, Tue: boolean, Wed: boolean, Thu: boolean, Fri: boolean, Sat: boolean, Sun: boolean } = {
    Mon: false,
    Tue: false,
    Wed: false,
    Thu: false,
    Fri: false,
    Sat: false,
    Sun: false,
  };
  apperance = 'outline';
  isOpenPatientDetailsPopup: boolean = false;
  isLoading: boolean = false;
  waitUntilDate: Date;

  serviceProductsList: any = [];
  serviceProductsOriginal: any = [];
  serviceProducts: any = [];
  practitionerIds;

  /** Patient Full Details */
  selectedPatientDetails: PatientModel;

  formGroup: FormGroup = new FormGroup({
    id: new FormControl(''),
    parentBusinessId: new FormControl(''),
    locationId: new FormControl(this.appState.selectedUserLocationId),
    serviceId: new FormControl(''),
    patientId: new FormControl('', Validators.required),
    expiryDate: new FormControl('', Validators.required),
    practitionerForPatientWaitList: new FormControl('', Validators.required),
    note: new FormControl(''),
    preferredDaysData: new FormControl(''),
    highPriority: new FormControl(false),
  });

  pDayFormGroup: FormGroup = new FormGroup({
    Mon: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Tue: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Wed: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Thu: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Fri: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Sat: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    }),
    Sun: new FormGroup({
      morning: new FormControl(false),
      afternoon: new FormControl(false),
      evening: new FormControl(false)
    })
  });

  constructor(
    public location: Location,
    protected _router: Router,
    protected appState: AppState,
    private alertService: AppAlertService,
    private patientService: PatientService,
    private appointmentService: AppointmentService,
    private waitListService: WaitListService,
  ) {
    super(location, _router);
  }

  ngOnInit() {
    this.prepareForEdit();
    this.checkForRouterState();
    if (this.ref) {
      this.ref.clickSave.subscribe(value => {
        if (this.formGroup.invalid) {
          this.formGroup.markAllAsTouched();
          return;
        }
        this.isLoading = true;
        this.formGroup.get('preferredDaysData').setValue(JSON.stringify(this.pDayFormGroup.value));
        const data = this.formGroup.value;
        if (!this.data) {
          delete data.id;
          data.status = WaitListStatus.Created;
          this.waitListService.createWaitList(data).subscribe(response => {
            if (response) {
              this.alertService.displaySuccessMessage('Patient is added to the Wait List successfully.');
              this.ref.closeDialog(SAVE);
            }
            this.isLoading = false;
          }, error => {
            this.alertService.displayErrorMessage(error || 'Failed to add patient to the Wait List.');
            this.isLoading = false;
          });
        } else {
          this.waitListService.updateWaitList(data).subscribe(response => {
            if (response) {
              this.alertService.displaySuccessMessage('Wait List details updated successfully.');
              this.ref.closeDialog(SAVE);
            }
            this.isLoading = false;
          }, error => {
            this.alertService.displayErrorMessage(error || 'Failed to update patient Wait List.');
            this.isLoading = false;
          });
        }
      });
    }
    this.getProductAndServiceSearch();
  }

  ngOnDestroy(): void {
    AddEditWaitListComponent.ref = null;
    this.data = null;
  }

  selectPractitioner($event: PractitionerSpecialityModel[]) {
    if (!$event) {
      return;
    }
    const data = [];
    const preDefined = this.data ? (this.data.practitionerForPatientWaitList || []) : [];
    $event.forEach(practitioner => {
      const has = preDefined.find(x => x.practitionerId === practitioner.id);
      if (has) {
        data.push(has);
      } else {
        data.push({practitionerId: practitioner.id});
      }
    });
    this.formGroup.get('practitionerForPatientWaitList').setValue(data);
  }

  clickDayButton($event: any, day: string) {
    if ($event.currentTarget.checked) {
      this.pDayFormGroup.get(day).patchValue({
        morning: true,
        afternoon: true,
        evening: true,
      });
    } else {
      this.pDayFormGroup.get(day).patchValue({
        morning: false,
        afternoon: false,
        evening: false,
      });
    }
  }

  clearOrRemoveSP() {
    this.formGroup.get('serviceId').setValue('');
  }

  dateSelectionChange($event) {
    this.waitUntilDate = AppoinmentDateUtils._toDate($event, true);
    this.formGroup.get('expiryDate').setValue(AppoinmentDateUtils.toUtc(this.waitUntilDate));
  }

  /** Toggle Patient Details Popup */
  togglePatientDetailsPopup(value: boolean) {
    this.isOpenPatientDetailsPopup = value;
  }

  /** Get Value On Patient Changed */
  onSelectPatientForAppt($event) {
    this.selectedPatient = $event;
    this.togglePatientDetailsPopup(false);
    this.formGroup.get('patientId').setValue(this.patientId);
    if (this.selectedPatient) {
      this.getPatientDetails();
    }
  }

  redirectPatient(data: any) {
    let fromState: NavigationExtras = {
      state: {
        fromState: true,
        fromPage: FromPage.WaitList,
        for: For.AddPatient,
        data: {formData: this.formGroup.value, pDays: this.pDayFormGroup.value, selectedPatient: this.selectedPatient}
      }
    };
    this._router.navigate(['patients/add'], fromState);
  }

  /** Get Patient Details */
  get patientNumber() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.patientId : '';
  }

  get patientName() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.firstName + ' ' + this.selectedPatientDetails.lastName : '';
  }

  get patientDOB() {
    const dob = this.selectedPatientDetails.dob;
    if (dob) {
      const yearsOld = AppoinmentDateUtils.getDiff(dob, new Date(), 'years');
      const formattedDob = DateUtils.format(dob, 'DD MMM YYYY');
      return yearsOld + ' Yrs, ' + formattedDob;
    }
    return '';
  }

  get patientGender() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.gender : '';
  }

  get patientHome() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.homePhone : '';
  }

  get patientMobile() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.mobile : '';
  }

  get patientEmail() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.email : '';
  }

  get patientOutstanding() {
    return 'NILL';
  }

  get patientMedicalCondition() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.medicalCondition : '';
  }

  get patientAllergy() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.allergy : '';
  }

  get patientMedication() {
    return this.selectedPatientDetails ? this.selectedPatientDetails.medication : '';
  }

  get ref(): IPopupAction {
    return AddEditWaitListComponent.ref;
  }

  get patientId() {
    return this.selectedPatient ? this.selectedPatient.id : null;
  }

  private getProductAndServiceSearch() {
    this.appointmentService.getProductAndServiceSearch(this.appState.selectedUserLocationId).subscribe((data) => {
      this.serviceProductsOriginal = data;
      this.prepareServiceNProducts();
    });
  }

  private prepareServiceNProducts() {
    let idInc = 1;
    this.serviceProductsOriginal.service.speciality.forEach(e => {
      const dd = {
        id: idInc,
        name: e.specialityName,
        _class: 'main-header',
      };
      idInc++;
      this.addToServiceNProductList(dd);
      e.category.forEach(d => {
        const dd = {
          id: idInc,
          name: d.categoryName,
          _class: 'sub-header-1'
        };
        idInc++;
        this.addToServiceNProductList(dd);
        d.serviceItems.forEach(f => {
          let price = f.standardPrice;
          const dd = {
            id: f.id,
            name: (f.productCode ? f.productCode + ' - ' : '') + f.name + '($' + price + ', ' + f.duration + ' min)',
            actualName: (f.productCode ? f.productCode + ' - ' : '') + f.name,
            actualDuration: f.duration,
            duration: f.duration,
            price: price,
            type: 0,
            _class: 'item-under-2'
          };
          this.addToServiceNProductList(dd);
          this.serviceProductsList.push(this.preparePSObj(f, TreatmentType.Service));
        });
      });
      this.serviceProducts = [...this.serviceProducts];
    });

  }

  private preparePSObj(d: any, type: TreatmentType) {
    return {
      treatementType: type,
      serviceId: d['id'],
      duration: d['duration'],
      price: d['salePrice'],
      tax: d.taxRate,
      isStatus: true
    };
  }

  private addToServiceNProductList(data: any) {
    this.serviceProducts.push(data);
  }

  private getPatientDetails() {
    if (!this.patientId) {
      return;
    }
    this.patientService.getPatientById(this.patientId).subscribe(data => {
      this.selectedPatientDetails = data;
    });
  }

  private prepareForEdit() {
    if (this.data && !this.isFromState()) {
      this.pDayFormGroup.patchValue(this.data.preferredDaysData);
      Object.keys(this.data.preferredDaysData).forEach(key => {
        const day = this.data.preferredDaysData[key];
        this.selectedPDays[key] = day.morning || day.afternoon || day.evening;
      });
      this.formGroup.patchValue(this.data);
      this.waitUntilDate = AppoinmentDateUtils._toDate(this.data.expiryDate, true);
      this.selectedPatient = new PatientModel();
      this.selectedPatient['name'] = this.data.patientName;
      this.selectedPatient.id = this.data.patientId;
      this.practitionerIds = [];
      this.data.practitionerForPatientWaitList.forEach(data => {
        const practitioner = this.practitioner.find(x => x.practitionerId === data.practitionerId);
        const displayName = practitioner ? practitioner.displayName : '';
        this.practitionerIds.push({id: data.practitionerId, displayName: displayName});
      });
      this.getPatientDetails();
    }
  }

  private checkForRouterState() {
    if (this.isFromState()) {
      if (this.routerState.for === For.WaitList && this.routerState.fromPage === FromPage.Patient) {
        this.selectedPatient = this.routerState.data['patient'] ? this.routerState.data['patient'] : this.routerState.data['selectedPatient'];
        this.formGroup.patchValue(this.routerState.data['formData']);
        this.pDayFormGroup.patchValue(this.routerState.data['pDays']);
      }
    }
  }

  static open(dialogService: AppDialogService, data: any, practitioner: PractitionerSpecialityModel[]): IPopupAction {
    this.ref = dialogService.open(AddEditWaitListComponent, {
      title: (data ? 'Edit' : 'Add To') + ' wait list',
      width: '620px',
      height: '676px',
      hideOnSave: false,
      data: {
        data: data,
        practitioner: practitioner
      }
    });
    return this.ref;
  }

}
