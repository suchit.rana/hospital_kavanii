import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';
import {Constant} from '../../../../../../shared/Constant';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {map, startWith} from 'rxjs/operators';
import {StaffService} from '../../../../../../services/app.staff.service';
import {AppState} from '../../../../../../app.state';
import {BreakpointObserver, MediaMatcher} from '@angular/cdk/layout';
import {AppointmentService} from '../../../../../../services/app.appointment.service';
import {SchedulerManageService} from '../../../scheduler-manage.service';
import {ScheduleCheck} from '../../../../../../models/app.appointment.model';
import {BehaviorSubject} from 'rxjs';
import {moveItemInArray} from '@angular/cdk/drag-drop';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {AppointmentHandlerService} from '../service/appointment-handler.service';
import {init} from 'protractor/built/launcher';

@Component({
  selector: 'app-practitioner-list',
  templateUrl: './practitioner-list.component.html',
  styleUrls: ['./practitioner-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PractitionerListComponent implements OnInit, OnDestroy {
  static readonly NO_PRACTITIONER: number = 0;
  static readonly NO_SELECTED_PRACTITIONER: number = 1;

  @Input()
  isCustomSortEnabled: boolean = false;
  @Input()
  initFromHistory: boolean = false;
  @Input()
  fromDashboard:boolean = false;
  @Output()
  resourceLoaded: EventEmitter<PractitionerSpecialityModel[]> = new EventEmitter();
  @Output()
  togglePanel: EventEmitter<boolean> = new EventEmitter();
  @Output()
  isEmptyPractitioner: EventEmitter<number> = new EventEmitter();

  // CONSTANT VARIABLE
  readonly addToSdlr = Constant.ADD_TO_SDLR;
  readonly removeFrmSdlr = Constant.REMOVE_FRM_SDLR;
  readonly viewProfile = Constant.VIEW_PROFILE;

  resource: PractitionerSpecialityModel[] = [];
  // POPUP DISPLAY CONTROL
  opened: boolean = false;

  isExpanded: boolean = true;
  isDiplayPanelUI: boolean = true;
  practionerSearchFormCtrl: FormControl = new FormControl('');
  selectedPractitionerFilter = Constant.ACTIVE;

  practionersList: PractitionerSpecialityModel[] = [];
  filteredPractionerList: PractitionerSpecialityModel[] = [];
  filteredPractionerListDisplay: PractitionerSpecialityModel[] = [];
  havePractitionerSchedule: ScheduleCheck[] = [];

  // POPUP ACTION LISTS
  practitionerFilterList = [
    {text: 'All Practitioner', value: Constant.ALL},
    {text: 'Active Practitioner', value: Constant.ACTIVE},
    {text: 'Inactive Practitioner', value: Constant.INACTIVE}
  ];

  practitionerListAction = [
    {text: '{0} scheduler', value: null, icon: 'fa fa-{0}'},
    {text: 'View profile', value: this.viewProfile, icon: 'fa fa-user'}
  ];

  private isAddedPractitioner: boolean = false;
  _subscribes: any[] = [];
  _scheduleAvailabilitySubscribes: BehaviorSubject<boolean>[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appState: AppState,
    private staffService: StaffService,
    private appointmentService: AppointmentService,
    private media: MediaMatcher,
    private breakPoint: BreakpointObserver,
    private schedulerManageService: SchedulerManageService,
    private apptHandler: AppointmentHandlerService
  ) {
    this.breakPoint.observe(['(min-width: 992px)']).subscribe(state => {
      this.togglePractitionerPanel(true, state.matches);
    });

    let params = this.route.snapshot.queryParams;
    if (params['addedNewPtt']) {
      this.isAddedPractitioner = true;
    }
  }

  ngOnInit() {
    // this.checkPractitionerSchedulr();
    this.loadData();
  }

  ngOnDestroy(): void {
    this._subscribes.forEach((value) => {
      value.unsubscribe();
    });
  }

  togglePractitionerPanel(isForceFully?: boolean, value?: boolean) {
    this.isDiplayPanelUI = false;
    if (isForceFully) {
      this.isExpanded = value;
      this.togglePanel.emit(this.isExpanded);
      this.setPanelUIDisplay(this.isExpanded);
      return;
    }
    this.isExpanded = !this.isExpanded;
    this.togglePanel.emit(this.isExpanded);
    this.setPanelUIDisplay(this.isExpanded);
  }

  toggleFilter() {
    this.opened = !this.opened;
  }

  practitionerFilterChanged(value: string) {
    this.toggleFilter();
    this.selectedPractitionerFilter = value;
    this._setDataToResource(this.filterPractitionerList(), false, 'ACTIVE/INACTIVE FILTER');
  }

  loadData() {
    this.listPractitioner();
    this.searchPractitioner();
  }

  listPractitioner() {
    this.getPractitioner();
    let sub = this.appState.selectedUserLocationIdState.subscribe(c => {
      if (this._scheduleAvailabilitySubscribes.length > 0) {
        this._scheduleAvailabilitySubscribes.forEach(value => {
          if (value) {
            value.unsubscribe();
          }
        });
        this._scheduleAvailabilitySubscribes = [];
      }
      this.resource = this.filteredPractionerList = this.filteredPractionerListDisplay = this.filteredPractionerListDisplay = this.practionersList = [];
      this.getPractitioner();
    });
    this._subscribes.push(sub);
  }

  private checkSchedulesOnDateChange() {
    let onComplete: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    this._scheduleAvailabilitySubscribes.push(onComplete);
    onComplete.subscribe(val => {
      if (val) {
        // console.log('CHANGE CURRENT DATE', this.schedulerManageService.currentDate.value);
        this.filteredPractionerList.forEach(resource => {
          const isSelected = this.initFromHistory && this.apptHandler.schedulerState.practitioners && this.apptHandler.schedulerState.practitioners.findIndex(x => x.practitionerId == resource.practitionerId) > -1
          resource.selected = this.checkScheduleAvailable(resource.practitionerId) || isSelected;
        });
        // console.log(this.filteredPractionerList);
        this._setDataToResource(this.filteredPractionerList, true, 'CHECK SCHEDULE');
      }
    });
    this.schedulerManageService.currentDate.subscribe(value => {
      // console.log("PLIST DATE SUBSCRIBE")
      this.checkPractitionerSchedulr(onComplete);
    });
  }

  private checkPractitionerSchedulr(onComplete?: BehaviorSubject<boolean>) {
    let  defaultPractitionerToSchedule:any;
    if (this.filteredPractionerListDisplay.length > 0) {
      defaultPractitionerToSchedule= this.filteredPractionerListDisplay[0].id
    }

    this.appointmentService.getPractitionerSchedules(this.appState.selectedUserLocationId, this.schedulerManageService.startDate, this.schedulerManageService.getDaysToAdd())
      .subscribe((response) => {
        if (response) {
          this.havePractitionerSchedule = response;

          if (this.havePractitionerSchedule.length > 0) {
           const practitionersWithOutSchedule =   this.havePractitionerSchedule.filter(f=>f.isSchduled == false);
           if (practitionersWithOutSchedule.length > 0) {
             // Pick any one
            defaultPractitionerToSchedule = practitionersWithOutSchedule[0].practitionerId;
           }
          }
        }
        // console.log('GET SCHEDULE AVAILABILITY');
        if (onComplete) {
          onComplete.next(true);
        }

        if (this.fromDashboard) {
          let index=  this.filteredPractionerListDisplay.findIndex(obj=>obj.id == defaultPractitionerToSchedule);
          this.filteredPractionerListDisplay[index].selected = false;
          this.onPractitionerListAction(this.filteredPractionerListDisplay[index],'ADD_TO_SDLR');
        }

      }, error => {
        if (onComplete) {
          onComplete.unsubscribe();
        }
      });
  }

  private getPractitioner() {
    this._listPractitioner();
  }

  private _listPractitioner() {
    this.staffService.getAllPractitionerSpecialityByLocationIdForCalendar(this.appState.selectedUserLocationId).subscribe(data => {
      this.isAddedPractitioner = this.isAddedPractitioner && data.length == 1;
      const listSize = data.length + 1;
      const d = data.sort((a, b) => {
          return AppUtils.getIntegerDefault(a.displayOrder, listSize) > AppUtils.getIntegerDefault(b.displayOrder, listSize) ? 1 : AppUtils.getIntegerDefault(a.displayOrder, listSize) < AppUtils.getIntegerDefault(b.displayOrder, listSize) ? -1 : 0;
        }
      );
      data.forEach(value => {
        value.displayName = PractitionerListComponent.getPractitionerName(value);
        value.id = value.practitionerId;
        value.photo = value.photo ? `data:image/jpeg;base64,${value.photo}` : null;
        value.selected = this.checkScheduleAvailable(value.practitionerId) && value.status;
        value.isPractioner = true;
        value.practitionerName = value.nickName;
        if (value.practitionerRoleSpecialityModel && value.practitionerRoleSpecialityModel.length > 0) {
          value.practitionerspecialitiesNamesString = value.practitionerRoleSpecialityModel
            .map((sn) => sn.specialityName)
            .join(', ');
        } else {
          value.practitionerspecialitiesNamesString = '';
        }
      });
      this.practionersList = data;
      // this.filterPractitionerList();
      this._setDataToResource(this.filterPractitionerList(), false, 'FIRST API CALL');
      this.checkSchedulesOnDateChange();

    });
  }

  searchPractitioner() {
    this.practionerSearchFormCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterPractitioner(value))
    ).subscribe();
  }

  filterPractitionerList(): PractitionerSpecialityModel[] {
    let filteredPractionerList: PractitionerSpecialityModel[] = [];
    let index = 0;
    if (this.selectedPractitionerFilter == Constant.ALL) {
      filteredPractionerList = this.practionersList;
      if (this.initFromHistory && this.apptHandler.schedulerState.practitioners) {
        this.apptHandler.schedulerState.practitioners.forEach(value => {
          const hasFound = filteredPractionerList.find(x => x.practitionerId == value.practitionerId);
          if (hasFound) {
            hasFound.selected = true;
          }
        });
      }
      return filteredPractionerList;
    }
    this.practionersList.forEach(value => {
      value.index = index;
      if (this.selectedPractitionerFilter == Constant.ACTIVE && value.status) {
        filteredPractionerList.push(value);
      } else if (this.selectedPractitionerFilter == Constant.INACTIVE && !value.status) {
        filteredPractionerList.push(value);
      }
      index++;
    });
    if (this.initFromHistory && this.apptHandler.schedulerState.practitioners) {
      this.apptHandler.schedulerState.practitioners.forEach(value => {
        const hasFound = filteredPractionerList.find(x => x.practitionerId == value.practitionerId);
        if (hasFound) {
          hasFound.selected = true;
        }
      });
    }
    if (filteredPractionerList.length != 0) {
      this.practionerSearchFormCtrl.enable();
    } else {
      this.practionerSearchFormCtrl.disable();
    }
    // this.practitionerFiltered.emit(filteredPractionerList);
    return filteredPractionerList;
  }

  onPractitionerListAction(value: PractitionerSpecialityModel, action) {
    setTimeout(() => {
      value.selected = !value.selected;
      if (action == this.viewProfile) {
        this.router.navigate(['/staffs/', value.practitionerId]);
        return;
      }
      let res = this.resource;
      if (action == this.removeFrmSdlr) {
        this.resource = res.filter(item => item.practitionerId !== value.practitionerId);
      } else if (action == this.addToSdlr) {

        this.resource = [...this.resource, value];
      }
      this.notifyResourceReadey(this.resource, 'FILTER ACTION');
    }, 200);
  }

  // resetResource() {
  //   this.resource = JSON.parse(JSON.stringify(this.filteredPractionerList));
  //   this.notifyResourceReadey(this.resource);
  // }

  resetResourceNormal() {
    this.notifyResourceReadey(this.resource, 'RESER NORMAL');
  }

  private notifyResourceReadey(data: PractitionerSpecialityModel[], from) {
    // console.log('NOTIFY RESOURCE', from);
    this.resourceLoaded.emit(data);
    this.isEmptyPractitioner.emit((this.practionersList.length == 0 || this.filteredPractionerList.length == 0) ? PractitionerListComponent.NO_PRACTITIONER : (this.resource.length == 0 ? PractitionerListComponent.NO_SELECTED_PRACTITIONER : -1));
  }

  private _filterPractitioner(query: string) {
    this.filteredPractionerListDisplay = this.filteredPractionerList.filter(value =>
      value.practitionerName.toLocaleLowerCase().includes(query.toLocaleLowerCase())
      || value.practitionerspecialitiesNamesString.toLocaleLowerCase().includes(query.toLocaleLowerCase())
    );
  }

  private static getSelectedOnly(filteredList: PractitionerSpecialityModel[]): PractitionerSpecialityModel[] {
    return filteredList.filter(value => value.selected);
  }

  private _setDataToResource(filteredList: PractitionerSpecialityModel[], isNotify: boolean = true, from) {
    this.resource = JSON.parse(JSON.stringify(PractitionerListComponent.getSelectedOnly(filteredList)));
    this.filteredPractionerList = filteredList;
    this.filteredPractionerListDisplay = JSON.parse(JSON.stringify(filteredList));
    if (isNotify) {
      // console.log('SET DATASOURCE', this.resource, this.filteredPractionerList, this.filteredPractionerListDisplay);
      this.notifyResourceReadey(this.resource, from);
    }


  }

  private static getPractitionerName(value: PractitionerSpecialityModel) {
    return value.firstName + ' ' + value.lastName;
  }

  private setPanelUIDisplay(val: boolean) {
    setTimeout(() => {
      this.isDiplayPanelUI = val;
    });
  }

  private checkScheduleAvailable(practitionerId: string): boolean {
    if (!this.havePractitionerSchedule) {
      return false;
    }
    let data: ScheduleCheck[] = this.havePractitionerSchedule.filter(value => value.practitionerId == practitionerId && value.isSchduled);

    return data && data.length > 0;
  }

  private preparePractitionerSortDataAndSave() {
    const list = [];
    this.filteredPractionerListDisplay.forEach((value, index) => {
      list.push({practitionerId: value.practitionerId, order: (index + 1)});
    });

    this.savePractitionerCustomSort({locationId: this.appState.selectedUserLocationId, practitioners: list});
  }

  private savePractitionerCustomSort(data) {
    this.appointmentService.updatePractitionerSort(data).subscribe(response => {

    }, error => {

    });
  }


  onChangePractitionerOrder($event: any) {
    moveItemInArray(this.filteredPractionerListDisplay, $event.previousIndex, $event.currentIndex);
    this.preparePractitionerSortDataAndSave();
  }
}
