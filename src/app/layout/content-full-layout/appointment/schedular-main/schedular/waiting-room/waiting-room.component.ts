import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild, ViewEncapsulation} from '@angular/core';
import {BaseGridComponent} from '../../../../shared-content-full-layout/base-grid/base-grid.component';
import {Appointment, AppointmentEntry} from '../../../../../../models/app.appointment.model';
import {AppointmentConfirmationStatus} from '../rest-service/app.appointment-tooltip.service';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';
import {DxPopoverComponent} from 'devextreme-angular';
import {PatientAppointmentService} from '../../../../patients/rest-service/app.patient-appointment.service';
import {AppDialogService} from '../../../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {AppointmentHandlerService} from '../service/appointment-handler.service';
import {process} from '@progress/kendo-data-query';
import * as moment from 'moment';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['./waiting-room.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WaitingRoomComponent extends BaseGridComponent implements OnInit, OnChanges {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly DISMISSED = 'DISMISSED';
  readonly SEND_SMS = 'SEND_SMS';

  @Input()
  data: Appointment[] = [];
  @Input()
  practitioners: PractitionerSpecialityModel[] = [];
  @Output()
  editAppt: EventEmitter<Date> = new EventEmitter<Date>();

  @ViewChild('actionPopup', {static: true}) actionPopup: DxPopoverComponent;

  today = new Date();
  waitingRoomAppt: AppointmentEntry[] = [];
  clickedItem: AppointmentEntry;

  sortable = {
    allowUnsort: this.allowUnsort,
    mode: this.multiple ? 'multiple' : 'single'
  };

  constructor(
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
    private patientAppointmentService: PatientAppointmentService,
    private apptHandlar: AppointmentHandlerService
  ) {
    super();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data') || changes.hasOwnProperty('practitioners')) {
      this.loadWaitingRoomAppt();
    }
  }

  openApptActionPopup(dataItem: AppointmentEntry, event: any) {
    this.clickedItem = dataItem;
    this.actionPopup.target = event.target;
    this.actionPopup.instance.show();
  }

  doAction(action: string) {
    this.actionPopup.instance.hide();
    switch (action) {
      case this.SEND_SMS:
        break;
      case this.DISMISSED:
        this.patientAppointmentService.update(this.clickedItem['patientId'], this.clickedItem.id, AppointmentConfirmationStatus.WaitingRoomDismissed, this.clickedItem.status, null, '', false, false).subscribe(response => {
          this.apptHandlar.reloadAppt(this.clickedItem['apptId']);
          this.alertService.displaySuccessMessage('Waiting Room Dismissed Successfully');
          const index = this.waitingRoomAppt.findIndex(x => x.id == this.clickedItem.id);
          this.waitingRoomAppt.splice(index, 1);
        }, error => {
          this.alertService.displaySuccessMessage(error || 'Failed to Dismissed. Please try again later.');
        });
        break;
    }
  }

  onEditAppt(date: Date) {
    this.editAppt.emit(date);
  }

  onFilter(inputValue: string): void {
    this.gridView = process(this.gridData, {
      filter: {
        logic: 'or',
        filters: [
          {
            field: 'patientName',
            operator: 'contains',
            value: inputValue,
          },
          {
            field: 'waitingTimeStartUTCFormatted',
            operator: 'contains',
            value: inputValue,
          },
          {
            field: 'patientMobileNumber',
            operator: 'contains',
            value: inputValue,
          },
          {
            field: 'phone',
            operator: 'contains',
            value: inputValue,
          },
          {
            field: 'practitionerName',
            operator: 'contains',
            value: inputValue,
          },
          {
            field: 'startDateUTCFormatted',
            operator: 'contains',
            value: inputValue,
          },
        ],
      },
    });
  }

  getWaitingTime(dataItem: any) {
    const duration = moment.duration(AppoinmentDateUtils.diff(dataItem.waitingTimeStartUTC, new Date()));
    dataItem['waitingTimeStartUTCFormatted'] = duration.hours() + ':' + duration.minutes() + ':' + duration.seconds();
    return dataItem['waitingTimeStartUTCFormatted'];
  }

  private loadWaitingRoomAppt() {
    const pIds = this.practitioners.map(p => p.id);
    const data = this.data.filter(d => pIds.indexOf(d.practitionerId) > -1);
    this.waitingRoomAppt = [];
    data.forEach(appointment => {
      const patientName = appointment.appointmentsExtraDetails.patientFullNameWithTitle;
      const patientId = appointment.patientId;
      const patientNumber = appointment.appointmentsExtraDetails.mobile;
      const offerings = appointment.treatmentDetails.treatmentServiceProduct;
      const findPractitioner = this.practitioners.find(p => p.practitionerId === appointment.practitionerId);
      const practitionerName = findPractitioner ? findPractitioner.displayName : '';
      appointment.appointmentsEntry.forEach((appointmentEntry) => {
        if (appointmentEntry.confirmationStatus === AppointmentConfirmationStatus.WaitingRoomArrived && AppoinmentDateUtils.isEqual(appointmentEntry.startDateUTC, new Date())) {
          appointmentEntry['patientId'] = patientId;
          appointmentEntry['patientName'] = patientName;
          appointmentEntry['patientMobileNumber'] = patientNumber;
          appointmentEntry['offerings'] = offerings;
          appointmentEntry['practitionerName'] = practitionerName;
          appointmentEntry['apptId'] = appointment.id;
          appointmentEntry['startDateUTCFormatted'] = AppoinmentDateUtils.formatDateTime(appointmentEntry['startDateUTC'], 'ddd, DD MMM @ hh:mm A', false);
          appointmentEntry['waitingTimeStartUTCFormatted'] = AppoinmentDateUtils.formatDateTime(appointmentEntry['waitingTimeStartUTC'], 'hh:mm:ss', false);
          this.waitingRoomAppt.push(appointmentEntry);
        }
      });
    });
    this.gridData = this.waitingRoomAppt;
    this.state.filter = null;
    this.loadItems('waitingTimeStartUTCFormatted');
  }

}
