import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {DateModel} from '../../../DateModel';
import {SchedulerManageService} from '../../../scheduler-manage.service';
import {DateUtils} from '../../../../../../shared/DateUtils';
import {SchedulerSettingsEnum} from '../../../../../../enum/scheduler-settings-enum';
import {MatDialog} from '@angular/material';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-schedular-header',
  templateUrl: './schedular-header.component.html',
  styleUrls: ['./schedular-header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SchedularHeaderComponent implements OnInit {
  readonly APPT_SCHEDULE_SETTINGS = RP_MODULE_MAP.appointment_schedule_settings;
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  @Output()
  dateChange: EventEmitter<Date> = new EventEmitter<Date>();
  @Output()
  viewChange: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  clickSchedulerSetting: EventEmitter<SchedulerSettingsEnum> = new EventEmitter<SchedulerSettingsEnum>();


  // REFERENCE TO HTML ELEM
  @ViewChild('dropdownlist', {static: true}) public dropdownlist: any;
  @ViewChild('calendarOpenBtn', {static: false}) public anchor: ElementRef;
  @ViewChild('popup', {static: false, read: ElementRef}) public popup: ElementRef;

  currentDate;
  currentView: string = 'day';
  selectedDate: DateModel[] = [];
  SchedulerSettingsEnum = SchedulerSettingsEnum;

  // SCHEDULAR SETTINGS
  @Input()
  isSecondTypeGroup: boolean = false;

  // CALENDAER OPTIONS FOR POPUP
  public calendarPopUpMargin = {horizontal: -60, vertical: 10};
  isClickedToday: boolean = false;
  isCalendarOpen: boolean = false;
  firstDay: number = 1;
  hoverDate: Date = new Date();
  currHvrDateStart;
  currHvrDateend;
  isSchedulerLegendDialogOpen: boolean = false;

  // DATE ACTIONS POPUP
  isOpenDropDownList = false;
  dateActions = [
    {id: 1, name: '1 Week', value: '1-w'},
    {id: 2, name: '2 Weeks', value: '2-w'},
    {id: 3, name: '3 Weeks', value: '3-w'},
    {id: 4, name: '4 Weeks', value: '4-w'},
    {id: 5, name: '6 Weeks', value: '6-w'},
    {id: 6, name: '8 Weeks', value: '8-w'},
    {id: 7, name: '10 Weeks', value: '10-w'},
    {id: 8, name: '1 Month', value: '1-M'},
    {id: 9, name: '2 Months', value: '2-M'},
    {id: 10, name: '3 Months', value: '3-M'},
    {id: 11, name: '6 Months', value: '6-M'},
    {id: 12, name: '12 Months', value: '12-M'},
  ];

  // SCHEDULAR ACTIONS
  isEODropDownListOpen = false;

  constructor(public service: SchedulerManageService, public dialog: MatDialog,public schedulerManageService: SchedulerManageService) {
  }

  ngOnInit(): void {

  }

  // POPUP CALENDAR METHODS
  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.toggleCalendarPopup(false);
    }
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
    if (!this.isCalenderFocus && !this.contains(event.target)) {
      this.toggleCalendarPopup(false);
    }
  }

  private contains(target: any): boolean {
    return (
      this.anchor.nativeElement.contains(target) ||
      (this.popup ? this.popup.nativeElement.contains(target) : false)
    );
  }

  // ON CLICK CALENDER PREV/NEXT
  nextPrevDate(num: number) {
    let date = this.service.getCurrentDate();
    // let addAlpha = 'd';
    if (this.currentView != 'day') {
      num *= 7;
    }

    this.setCurrentDate(AppoinmentDateUtils.addAndGetNewValue(date, num, 'd'));
  }

  // CALENDER METODS
  openCalendarPopup() {
    this.isCalenderFocus = false;
    let leftArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
    let rightArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
    leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
    rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';

    this.changeDayLabelName();
  }

  isCalenderFocus: boolean = false;

  optionChanged() {
    this.changeDayLabelName();
  }

  onDateSelect(e) {
    // if (this.isClickedToday){
    //   return;
    // }
    this.toggleCalendarPopup(false);
    // this.prepareDateArrayForSdlr();
    this.setCurrentDate(e);
  }

  setupStartEndDayOfWeek() {
    this.currHvrDateStart = AppoinmentDateUtils.getStartDateOfWeek(this.hoverDate);
    this.currHvrDateend = AppoinmentDateUtils.getEndDateOfWeek(this.currHvrDateStart, this.getDaysToAdd());
    this.isInWeek(this.hoverDate);
  }

  isApplyWeekOrMF(date: Date) {
    return this.currentView != 'day' && (this.isInWeek(date) || this.isCurrDateInWeek(date));
  }

  isInWeek(date: Date) {
    return AppoinmentDateUtils.isBetween(this.currHvrDateStart, this.currHvrDateend, date);
  }

  isCurrDateInWeek(date: Date) {
    let currHvrDateStart = AppoinmentDateUtils.getStartDateOfWeek(this.currentDate);
    let currHvrDateend = AppoinmentDateUtils.getEndDateOfWeek(currHvrDateStart, this.getDaysToAdd());
    return AppoinmentDateUtils.isBetween(currHvrDateStart, currHvrDateend, date);
  }

  mouseHover(cell: any) {
    this.hoverDate = cell.date;
    this.setupStartEndDayOfWeek();
  }

  performDateAction(data: any) {
    this.closeDropDown();
    setTimeout(() => {
      if (data) {
        let valArr = data.value.split('-');
        if (valArr) {
          let num = valArr[0];
          let type = valArr[1];
          this.setCurrentDate(AppoinmentDateUtils.addAndGetNewValue(this.currentDate, num, type));
          // this.prepareDateArrayForSdlr();
        }
      }
    });
  }

  // HEADER BUTTON CLICK ACTIONS
  //OPEN DATE PICKER
  toggleCalendarPopup(val: boolean) {
    this.hoverDate = this.currentDate;
    this.setupStartEndDayOfWeek();
    this.isCalendarOpen = val;
  }

  // SET CURRENT DATE TO TODAY DATE
  setTodayDate(isClickedToday?: boolean) {
    this.isClickedToday = isClickedToday;
    this.setCurrentDate(AppoinmentDateUtils.getCurrentDate());
  }

  // OPEN DATE ACTIONS POPUP
  openDateActionDeropDown() {
    this.isOpenDropDownList = !this.isOpenDropDownList;
    // this.dropdownlist.toggle(this.isOpenDropDownList);
  }

  // CLOSE DATE ACTIONS POPUP
  closeDropDown() {
    this.isOpenDropDownList = false;
  }

  // CHANGE VIEWS
  changeView(view: string) {
    if (this.schedulerManageService.displayRoomVal && (view === 'workWeek' || view === 'week')) {
      return;
    }
    this.service.currentView = view;
    this.currentView = view;
    this.calendarPopUpMargin.horizontal = view != 'date' ? 0 : -60;
    this.viewChange.emit(view);
  }

  // Notify parent of clicked option
  clickSchedulerSettings($event: SchedulerSettingsEnum) {
    if ($event == SchedulerSettingsEnum.SchedulerLegend) {
      this.isSchedulerLegendDialogOpen = true;
    }
    this.clickSchedulerSetting.emit($event);
    this.isEODropDownListOpen = false;
  }

  closeSchedulerLegendDialog() {
    this.isSchedulerLegendDialogOpen = false;
  }


  // CHANGE CALENDER DAY NAMES
  private changeDayLabelName() {
    setTimeout(() => {
      let tables = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-body table');
      if (tables) {
        Array.from(tables).forEach(table => {
          let nameLabes = table.querySelectorAll('thead tr th');
          Array.from(nameLabes).forEach(th => {
            let name = th.getAttribute('abbr');
            th.innerHTML = name.substr(0, 2);
          });
        });
      }
    });
  }

  // GET DAYS TO ADD
  private getDaysToAdd(): number {
    return this.service.getDaysToAdd(-1);
    // return this.currentView == 'day' ? 1 : this.currentView == 'week' ? 6 : 4;
  }

  // SET CURRENT DATE
  setCurrentDate(date) {
    this.service.setCurrentDate(date, 'FROM SCHEDULEr HEADER');
    setTimeout(() => {
      this.currentDate = date;
      this.notifyDateChange();
      // this.prepareDateArrayForSdlr();
    });
  }

  get getSelectedFormattedDate() {
    if (this.currentView != 'day') {
      return DateUtils.getFormatedDateForWeek(DateUtils.getStartDateOfWeek(this.service.getCurrentDate()), this.getDaysToAdd());
    }
    return DateUtils.getFormatedDate(this.service.getCurrentDate());
  }

  // NOTIFY CHANGES
  notifyDateChange() {
    this.dateChange.emit(this.currentDate);
  }
}
