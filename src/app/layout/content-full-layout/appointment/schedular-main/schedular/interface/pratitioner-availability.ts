export interface IPaDates {
  date: Date;
  morningTimes: AvailableTime[];
  afterNoonTimes: AvailableTime[];
  eveningTimes: AvailableTime[];
}

export interface AvailableTime {
  time: string;
  practitioners: {practitionerId: string; practitionerName: string}[];
}
