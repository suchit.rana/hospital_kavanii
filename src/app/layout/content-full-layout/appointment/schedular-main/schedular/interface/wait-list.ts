export interface WaitList {
  id: string,
  parentBusinessId: string,
  locationId: string,
  patientId: string,
  expiryDate: string,
  serviceId: string,
  note: string,
  patientName: string,
  patientCode: string,
  gender: string,
  status: WaitListStatus,
  practitionerName: string,
  serviceName: string,
  mobileNo: string,
  preferredDaysData: string | any,
  highPriority: boolean,
  practitionerForPatientWaitList: PractitionerForPatientWaitList[]
}

export interface PractitionerForPatientWaitList {
  id: string,
  patientWaitListId: string,
  practitionerId: string,
  practitionerName: string,
}

export enum WaitListStatus
{
  Created,
  Booked,
  Deleted,
}
