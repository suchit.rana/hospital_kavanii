import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Output,
    Self,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {BaseSettingModel} from '../../../../../../../models/app.settings.model';
import {Subject} from 'rxjs';
import {AppState} from '../../../../../../../app.state';
import {ApplicationDataService} from '../../../../../../../services/app.applicationdata.service';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {DropDownListComponent} from '@progress/kendo-angular-dropdowns';

@Component({
    selector: 'app-appt-drop-down-add-new',
    templateUrl: './appt-drop-down-add-new.component.html',
    styleUrls: ['./appt-drop-down-add-new.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [{provide: MatFormFieldControl, useExisting: ApptDropDownAddNewComponent}],
})
export class ApptDropDownAddNewComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, OnDestroy {

    @Input() data: BaseSettingModel[];
    @Input() valueField: string = '';
    @Input() textField: string = '';
    @Input() isDisabled: boolean = false;
    @Input() isDisplayFooter: boolean = true;
    @Input() isServiceProduct: boolean = false;
    @Input() isApptType: boolean = false;
    @Input() footerText: string = '+ Add New';
    @Output()
    valueChange: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    clickAddNew: EventEmitter<string> = new EventEmitter<string>();
    @Output()
    clearOrRemove: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(DropDownListComponent, {static: false}) component: DropDownListComponent;

    _value: any = null;
    private _valueDummy: any;
    filterdData: BaseSettingModel[] = [];
    isOpen: boolean = false;
    popupClass = 'app-appt-mat-dropdown-popup';

    valuePrimitive = false;
    static nextId = 0;
    focused = false;
    controlType = 'app-appt-dropdown-with-new';
    id = `app-appt-dropdown-with-new${ApptDropDownAddNewComponent.nextId++}`;
    _class = `app-appt-dropdown-with-new${ApptDropDownAddNewComponent.nextId++}`;
    describedBy = '';
    stateChanges: Subject<void> = new Subject<void>();
    onChange = (value) => {
    };
    onTouched = () => {
    };

    constructor(
        private appState: AppState,
        private applicationDataService: ApplicationDataService,
        private _focusMonitor: FocusMonitor,
        private _elementRef: ElementRef<HTMLElement>,
        @Optional() @Self() public ngControl: NgControl
    ) {
        _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
            if (this.focused && !origin) {
                this.onTouched();
            }
            this.focused = !!origin;
            this.stateChanges.next();
        });

        if (this.ngControl != null) {
            this.ngControl.valueAccessor = this;
        }
        const obj = new BaseSettingModel();
        obj['id'] = -1;
        obj[this.textField] = this.value;
        this.filterdData.push(obj);
        this.data = this.filterdData;
    }

    ngOnInit() {
        if (this.isServiceProduct) {
            this.popupClass += ' service-product';
        }
        this.addDummyIfNotPresent(this.data);
        this.filterdData = this.data;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('data')) {
            this.addDummyIfNotPresent(this.data);
            this.filterdData = this.data;
            this.valuePrimitive = this.filterdData && this.filterdData.length > 0;
            // this.value = this._valueDummy;
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(value: any): void {
        this.value = value ? value : null;
    }

    markAsTouched() {
        if (!this.touched) {
            this.onTouched();
            // this.touched = true;
        }
    }

    setDisabledState(disabled: boolean) {
        this.disabled = disabled;
    }

    changeVal(val: any) {
        this.markAsTouched();
        if (!this.disabled) {
            this.value = val;
            this.onChange(this.value);
            this.handleChange(val);
            if (val) {
                this.addClearButton();
            } else {
                this.removeClearButton();
            }
        }
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return control.errors;
    }

    get touched(): boolean {
        return this.ngControl && this.ngControl.touched;
    }

    get errorState(): boolean {
        return this.ngControl && this.ngControl.touched && this.ngControl.invalid;
    }

    get empty() {
        return !this.value;
    }

    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    @Input()
    get placeholder(): string {
        return this._placeholder;
    }

    set placeholder(value: string) {
        this._placeholder = value;
        this.stateChanges.next();
    }

    private _placeholder: string;

    @Input()
    get required(): boolean {
        return this._required;
    }

    set required(value: boolean) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _required = false;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _disabled = false;

    @Input()
    get value(): any | null {
        return this._value;
    }

    set value(data: any | null) {
        if (this.data && this.data.length > 0) {
            this._value = data ? data : null;
            this.stateChanges.next();
        } else {
            this._valueDummy = data ? data : null;
        }
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this._focusMonitor.stopMonitoring(this._elementRef);
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }

    onContainerClick(event: MouseEvent) {
        // this.component.togglePopup(!this.component.isOpen);
    }

    handleFilter(value: string) {
        if (!value) {
            this.filterdData = this.data;
            return;
        }
        this.filterdData = this.data.filter((s) => value && s[this.valueField].toLowerCase().trim().includes(value.toLowerCase().trim()));
    }

    handleChange(data: any) {
        this.valueChange.emit(data);
    }

    addNew() {
        this.clickAddNew.emit(this.textField);
    }

    toggle(isOpen: boolean) {
        this.isOpen = isOpen;
        this.markAsTouched();
    }

    private addDummyIfNotPresent(data: any[]) {
        if (!this.value || !data || data.length == 0) {
            return;
        }
        let isValFound = false;
        data.forEach(value1 => {
            if (value1[this.valueField] == this.value) {
                isValFound = true;
            }
        });
        if (!isValFound) {
            const obj = {id: -1};
            obj[this.textField] = this.value;
            data.push(obj);
        }
    }

    private addClearButton() {
        if (!this.isServiceProduct) {
            return;
        }
        this.removeClearButton();
        const list = document.getElementById(this.component.focusableId);
        if (list) {
            (list.lastElementChild as HTMLElement).style.order = '1';
            const matIcon = document.createElement('mat-icon');
            matIcon.onclick = () => {
                this.clearOrRemove.emit(this.value);
                this.changeVal(null);
            };
            matIcon.innerText = 'close';
            matIcon.className = 'material-icons ic-clear';
            matIcon.style.order = '1';
            list.prepend(matIcon);
        }
    }

    private removeClearButton() {
        if (!this.isServiceProduct) {
            return;
        }
        const list = document.getElementById(this.component.focusableId);
        if (list) {
            if (list.firstElementChild.classList.contains('ic-clear')) {
                list.removeChild(list.firstElementChild);
            }
        }
    }

    getItemClass(dataItem: any): string {
        return dataItem['_class'];
    }

    itemDisabled = (itemArgs: { dataItem: string; index: number }) => {
        return this.isServiceProduct && itemArgs.dataItem['_class'] == 'main-header' || itemArgs.dataItem['_class'] == 'sub-header-1';
        // return this.isDisabledTag && this.disabledTagsVal.indexOf(itemArgs.dataItem[this.valueField]) > -1;
    };
}
