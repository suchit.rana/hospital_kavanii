import {Appointment} from '../../../../../../../models/app.appointment.model';
import {AppoinmentDateUtils} from '../../../../AppoinmentDateUtils';
import {Frequency} from '../../../../../../../enum/application-data-enum';
import * as moment from 'moment';
import {AppointmentStatus} from '../../rest-service/app.appointment-tooltip.service';
import {Injectable} from '@angular/core';

@Injectable()
export class AddEditAppointmentUtils {

  static prepareData(value, isNew, practitionerId, selectedserviceProductsList, recurrenceRule, recurrenceException = null) {
    let data: Appointment = JSON.parse(JSON.stringify(value));
    delete data._startDate;
    delete data._endDate;
    if (isNew) {
      delete data.id;
      delete data.treatmentDetails.id;
      delete data.treatmentDetails.appointmentsId;
      data.status = AppointmentStatus.Pending;
    }
    data.treatmentDetails.practitionerId = practitionerId || '';
    data.treatmentDetails.treatmentServiceProduct = selectedserviceProductsList;
    data.recurrenceRule = recurrenceRule;
    data.recurrenceException = recurrenceException;

    value._startDate = AppoinmentDateUtils.getDateWithTz(value._startDate);
    data.startDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._startDate, {
      'hour': this.getHour(data.startDateUTC),
      'minute': this.roundOffMinute(data.startDateUTC),
      'second': 1
    }));
    value._endDate = AppoinmentDateUtils.getDateWithTz(value._endDate);

    data.endDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._endDate, {
      'hour': this.getHour(data.endDateUTC),
      'minute': this.roundOffMinute(data.endDateUTC)
    }));
    return data;
  }

  static prepareTime(value: any) {
    value._startDate = AppoinmentDateUtils.getDateWithTz(value._startDate);
    value.startDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._startDate, {
      'hour': this.getHour(value.startDateUTC),
      'minute': this.roundOffMinute(value.startDateUTC),
      'second': 1
    }));
    value._endDate = AppoinmentDateUtils.getDateWithTz(value._endDate);

    value.endDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._endDate, {
      'hour': this.getHour(value.endDateUTC),
      'minute': this.roundOffMinute(value.endDateUTC)
    }));
  }

  static formatRRule(rrule: string) {
    return (rrule.includes('UNTIL') && rrule.lastIndexOf('Z') < 0) ? rrule + 'Z' : rrule;
  }

  private static roundOffMinute(date) {
    let min = this.getMinute(date);
    let divideBy = min;

    if (min > 0 && min < 5) {
      divideBy = 5;
    } else if (min > 5 && min < 10) {
      divideBy = 10;
    } else if (min > 15 && min < 20) {
      divideBy = 20;
    } else if (min > 20 && min < 25) {
      divideBy = 25;
    } else if (min > 25 && min < 30) {
      divideBy = 30;
    } else if (min > 30 && min < 35) {
      divideBy = 35;
    } else if (min > 35 && min < 40) {
      divideBy = 40;
    } else if (min > 40 && min < 45) {
      divideBy = 45;
    } else if (min > 45 && min < 50) {
      divideBy = 50;
    } else if (min > 50 && min < 55) {
      divideBy = 55;
    } else if (min > 55 && min < 60) {
      divideBy = 60;
    }

    if (divideBy == min) {
      return min;
    }

    let decimalPoint = Math.round((min / 5) % 1);

    if (decimalPoint <= 0) {
      return 0;
    } else {
      return divideBy;
    }
  }

  private static getHour(date: Date) {
    return parseInt(moment(date).format('HH'));
  }

  private static getMinute(date: Date) {
    return parseInt(moment(date).format('mm'));
  }

  private static getFrequencyEnum(frequency): Frequency {
    switch (frequency.toUpperCase()) {
      case 'DAILY':
        return Frequency.Day;
      case 'ROSD':
        return Frequency.SameAsSchedule;
      case 'WEEKLY':
        return Frequency.Weekly;
      case 'MONTHLY':
        return Frequency.Monthly;
      case 'YEARLY':
        return Frequency.Yearly;
      default:
        return Frequency.None;
    }
  }

}
