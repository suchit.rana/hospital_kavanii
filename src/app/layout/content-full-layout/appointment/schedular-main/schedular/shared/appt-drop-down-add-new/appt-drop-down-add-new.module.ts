import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApptDropDownAddNewComponent} from './appt-drop-down-add-new.component';
import {DropDownListModule} from '@progress/kendo-angular-dropdowns';


@NgModule({
  declarations: [
    ApptDropDownAddNewComponent
  ],
  exports: [
    ApptDropDownAddNewComponent
  ],
  imports: [
    CommonModule,
    DropDownListModule,
  ],
})
export class ApptDropDownAddNewModule {
}
