import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AutoCompleteComponent} from '@progress/kendo-angular-dropdowns';
import {SchedulerSettingsService} from '../../../../../../../services/app.scheduler-settings.service';
import {PatientModel} from '../../../../../../../models/app.patient.model';
import {catchError, debounceTime, distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PatientSearchComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() valueField: string = 'id';
  @Input() textField: string = 'name';
  @Input()
  placeHolder: string = 'Search Patient ( ID, Name or Mobile Number)';
  @Input()
  isFocus: boolean = false;
  @Input()
  isOpen: boolean = false;
  @Input()
  isHideAdd: boolean = false;
  @Input()
  focusFirst: boolean = true;
  @Output()
  valueChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  redirectPatient: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  PatientSearchText: EventEmitter<string> = new EventEmitter<string>();

  patientList: PatientModel[] = [];
  selectedPatient: PatientModel = null;
  private isFirstTimeOpen: boolean = false;
  private isInitInAfetViewInit: boolean = false;

  // patientListSource = [
  // {
  //   ID: '0123',
  //   name: 'Test Patient',
  //   birthDate: '7th MAR 2000',
  //   email: 'test@gmail.com',
  //   phone: '+91 7854589745',
  // }
  // ];
  @Input()
  value: any;
  @Input() searchValue: string;

  _value;
  @ViewChild('patientSearch', {static: false}) patientSearch: AutoCompleteComponent;

  constructor(public schedulerSettingsService: SchedulerSettingsService) {
  }

  ngOnInit() {
    if (this.value) {
      this._value = this.value[this.valueField];
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isFirstTimeOpen && changes.hasOwnProperty('isOpen') && this.isOpen) {
      if (this.patientList) {
        const component = document.getElementById('patient-search-autocomplete');
        const searchIcon = document.getElementById('patient-search-field-ic');
        component.prepend(searchIcon);
        if (this.value) {
          this.patientSearch.searchBarChange(this.value ? this.value[this.textField] : '');
          this.patientSearch.togglePopup(false);
        }
      }
    }
  }

  ngAfterViewInit(): void {
    this.filterPatientSearchData();
    if (this.focusFirst) {
      this.patientSearch.searchbar.focus();
    }
    if (this.patientList) {
      this.isInitInAfetViewInit = true;
      const component = document.getElementById('patient-search-autocomplete');
      const searchIcon = document.getElementById('patient-search-field-ic');
      component.prepend(searchIcon);
      if (this.value) {
        setTimeout(() => {
          this.patientSearch.searchBarChange(this.value ? this.value[this.textField] : '');
          this.patientSearch.togglePopup(false);
        });
      }
    }
    this.isFirstTimeOpen = false;
  }

  handleChange(value: any, fromList = true) {
    if (fromList || !value) {
      this.patientSearch.searchBarChange(value ? value[this.textField] : '');
      this.patientSearch.toggle(false);
      this.valueChanged.emit(value);
    }
  }

  redirectHandle(data: any) {
    this.redirectPatient.emit(data);
  }

  focus() {
    setTimeout(() => {
      this.patientSearch.searchbar.focus();
    });
  }

  private filterPatientSearchData() {
    this.patientSearch.filterChange
      .asObservable()
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((term) => {
          if (!term) {
            this.patientSearch.loading = false;
            return of([]);
          }
          this.patientSearch.loading = true;
          return this.schedulerSettingsService.getPatientsBySearch(term).pipe(
            map(data => {
              data.forEach(value => {
                value['id'] = value.id;
                value['ID'] = value.patientId;
                value['name'] = value.firstName + ' ' + value.lastName;
                value['birthDate'] = value.dob;
                value['email'] = value.email;
                value['phone'] = value.mobile;
              });
              return data;
            }),
            tap(() => (this.patientSearch.loading = false)),
            catchError(() => {
              this.patientSearch.loading = false;
              return of([]);
            })
          );
        }),
        tap(() => (this.patientSearch.loading = false)),
      )
      .subscribe((data) => {
        this.patientList = data;
        this.patientSearch.loading = false;
      });
  }

  clearText() {
    if (this.patientSearch) {
      this.patientSearch.searchBarChange('');
    }
  }

  get getPlaceHolder() {
    return this.selectedPatient ? '' : this.placeHolder;
  }
}
