import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {
  IPatientAppointmentRecurringData,
  PatientAppointmentService
} from '../../../../../patients/rest-service/app.patient-appointment.service';
import {AppDialogService, IPopupAction} from '../../../../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {AppointmentHandlerService, MOVE, REBOOK} from '../../service/appointment-handler.service';
import {AppointmentConfirmationStatus, AppointmentStatus, BubbleAppointment} from '../../rest-service/app.appointment-tooltip.service';
import {formatREString} from '../../../../../../../app.component';
import {AppoinmentDateUtils} from '../../../../AppoinmentDateUtils';
import {Appointment} from '../../../../../../../models/app.appointment.model';
import {FormControl, Validators} from '@angular/forms';
import {RP_MODULE_MAP} from '../../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-action-appointment',
  templateUrl: './action-appointment.component.html',
  styleUrls: ['./action-appointment.component.scss']
})
export class ActionAppointmentComponent implements OnInit, OnChanges {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly CONFIRM_APPT = 'CONFIRM_APPT';
  readonly UNCONFIRM_APPT = 'UNCONFIRM_APPT';
  readonly MOVE_APPT = 'MOVE_APPT';
  readonly REBOOK_APPT = 'REBOOK_APPT';
  readonly WAITING_ROOM_ARRIVED = 'WAITING_ROOM_ARRIVED';
  readonly WAITING_ROOM_DISMISSED = 'WAITING_ROOM_DISMISSED';
  readonly NEEDS_APPT = 'NEEDS_APPT';
  readonly REMOVE_NEEDS_APPT = 'REMOVE_NEEDS_APPT';
  readonly DELETE_APPT = 'DELETE_APPT';
  readonly RESET_APPT = 'RESET_APPT';

  readonly CNF_STATUS_UNCONFIRMED = AppointmentConfirmationStatus.Unconfirmed;
  readonly CNF_STATUS_CONFIRMED = AppointmentConfirmationStatus.Confirmed;
  readonly CNF_STATUS_REPLIEDNO = AppointmentConfirmationStatus.RepliedNo;
  readonly CNF_STATUS_WAITINGROOMARRIVED = AppointmentConfirmationStatus.WaitingRoomArrived;
  readonly CNF_STATUS_WAITINGROOMDISMISSED = AppointmentConfirmationStatus.WaitingRoomDismissed;

  readonly STATUS_PENDING = AppointmentStatus.Pending;
  readonly STATUS_COMPLETED = AppointmentStatus.Completed;
  readonly STATUS_MISSED = AppointmentStatus.Missed;
  readonly STATUS_CANCELLED = AppointmentStatus.Cancelled;
  readonly STATUS_DELETED = AppointmentStatus.Deleted;

  @Input()
  recurringData: IPatientAppointmentRecurringData;
  @Input()
  patientId: string;
  @Input()
  appt: BubbleAppointment;
  @Input()
  clickedItem: Appointment;

  confirmationStatus: AppointmentConfirmationStatus = this.CNF_STATUS_UNCONFIRMED;
  status: AppointmentStatus = AppointmentStatus.Pending;
  needAttentionAlert = new FormControl('', Validators.required);
  isTodayAppt = false;

  @ViewChild('needAttentionTemplate', {static: false}) needAttentionTemplate;
  @ViewChild('deleteConfirmation', {static: false}) deleteConfirmation;

  constructor(
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
    private patientAppointmentService: PatientAppointmentService,
    private apptHandlar: AppointmentHandlerService,
  ) {
  }

  ngOnInit() {
    this.confirmationStatus = this.appt.confirmationStatus;
    this.status = this.appt.status;
    this.isTodayAppt = AppoinmentDateUtils.getDiff(new Date(), this.appt.startDateTime) == 0;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('appt') && this.appt) {
      this.confirmationStatus = this.appt.confirmationStatus;
      this.status = this.appt.status;
    }
  }

  clickAction(clickedItem: string) {
    this.apptHandlar.hideTooltip();
    switch (clickedItem) {
      case this.CONFIRM_APPT:
        this.update(
          this.appt.status,
          AppointmentConfirmationStatus.Confirmed,
          this.recurringData,
          '',
          false,
          'Appointment confirmed Successfully'
        );
        break;
      case this.UNCONFIRM_APPT:
        this.update(
          this.appt.status,
          AppointmentConfirmationStatus.Unconfirmed,
          this.recurringData,
          '',
          false,
          'Appointment confirmed Successfully'
        );
        break;
      case this.WAITING_ROOM_ARRIVED:
        this.update(
          this.appt.status,
          AppointmentConfirmationStatus.WaitingRoomArrived,
          this.recurringData,
          '',
          false,
          'Waiting Room Arrived Successfully'
        );
        break;
      case this.WAITING_ROOM_DISMISSED:
        this.update(
          this.appt.status,
          AppointmentConfirmationStatus.WaitingRoomDismissed,
          this.recurringData,
          '',
          false,
          'Waiting Room Dismissed Successfully'
        );
        break;
      case this.DELETE_APPT:
        if (this.recurringData) {
          this.recurringData.recurrenceException = formatREString(this.recurringData.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(this.clickedItem.startDate));
          delete this.recurringData.status;
          delete this.recurringData.type;
        }
        const ref = this.appDialogService.open(this.deleteConfirmation, {
          title: 'Do you wish to delete this Appointment?',
          width: '778px',
          height: '229px',
          hideOnSave: false,
          saveBtnTitle: 'Yes',
          cancelBtnTitle: 'No'
        });
        ref.clickSave.subscribe((value) => {
          this.update(
            AppointmentStatus.Deleted,
            this.appt.confirmationStatus,
            this.recurringData,
            '',
            true,
            'Appointment Deleted Successfully'
          );
        });
        break;
      case this.RESET_APPT:
        this.update(
          AppointmentStatus.Pending,
          this.appt.confirmationStatus,
          this.recurringData,
          '',
          true,
          'Appointment Reset Successfully'
        );
        break;
      case this.NEEDS_APPT:
        const dialogRef = this.appDialogService.open(this.needAttentionTemplate, {
          title: 'NEED ATTENTION',
          hideOnSave: false,
          width: '778px',
        });
        dialogRef.clickSave.subscribe(value => {
          if (this.needAttentionAlert.invalid) {
            return;
          }
          this.update(
            AppointmentStatus.Pending,
            this.appt.confirmationStatus,
            this.recurringData,
            this.needAttentionAlert.value,
            true,
            'Need attention set Successfully',
            true,
              dialogRef
          );
        });
        break;
      case this.REMOVE_NEEDS_APPT:
        this.update(
          AppointmentStatus.Pending,
          this.appt.confirmationStatus,
          this.recurringData,
          '',
          true,
          'Need attention removed Successfully'
        );
        break;
      case this.MOVE_APPT:
        this.apptHandlar.notifyRebookMove(MOVE);
        break;
      case this.REBOOK_APPT:
        this.apptHandlar.notifyRebookMove(REBOOK);
        break;
    }
  }

  update(status: AppointmentStatus, confirmationStatus: AppointmentConfirmationStatus, recurringData: IPatientAppointmentRecurringData, statusReasonMessage: string, isRecurringChange = false, msg: string = '', isNeedAttention = false, dialogRef?: IPopupAction) {
    if (recurringData) {
      delete recurringData.type;
      delete recurringData.status;
    }
    this.patientAppointmentService.update(this.patientId, this.appt.appointmentEntryId, confirmationStatus, status, recurringData, statusReasonMessage, isRecurringChange, isNeedAttention).subscribe(response => {
      this.apptHandlar.reloadAppt(this.appt.appointmentId);
      this.alertService.displaySuccessMessage(msg);
      if (dialogRef) {
        dialogRef.closeDialog();
      }
    }, error => {
      this.alertService.displaySuccessMessage(error || 'Failed to update status');
    });
  }
}
