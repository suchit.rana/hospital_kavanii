import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BubbleStatics} from '../../rest-service/app.appointment-tooltip.service';
import {Appointment} from '../../../../../../../models/app.appointment.model';

@Component({
  selector: 'app-appointmentdetails',
  templateUrl: './appointmentdetails.component.html',
  styleUrls: ['./appointmentdetails.component.scss']
})
export class AppointmentdetailsComponent implements OnInit, OnChanges {

  @Input()
  data: BubbleStatics;

  toggleFutureApptSection: boolean = false;

  totalAppts: number;
  thisYearAppts: number;
  prevAppt: Appointment = {} as Appointment;
  createdDate: Date;
  modifiedDate: Date;
  createdBy: string;
  modifiedBy: string;
  futureAppts: Appointment[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data') && this.data) {
      this.totalAppts = this.data.totalAppts;
      this.thisYearAppts = this.data.thisYearAppts;
      this.prevAppt = this.data.prevAppt;
      this.createdDate = this.data.createdDate;
      this.modifiedDate = this.data.modifiedDate;
      this.createdBy = this.data.createdBy;
      this.modifiedBy = this.data.modifiedBy;
      this.futureAppts = this.data.futureAppts;
    }
  }

}
