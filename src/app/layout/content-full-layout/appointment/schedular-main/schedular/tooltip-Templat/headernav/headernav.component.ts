import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BubbleAppointment, BubblePatient} from '../../rest-service/app.appointment-tooltip.service';
import {Appointment} from '../../../../../../../models/app.appointment.model';

@Component({
  selector: 'app-headernav',
  templateUrl: './headernav.component.html',
  styleUrls: ['./headernav.component.css']
})
export class HeadernavComponent implements OnInit {
  @Input()
  patient: BubblePatient;
  @Input()
  appt: Appointment;
  @Output()
  valueChange: EventEmitter<string> = new EventEmitter<string>();

  selected = '1';

  constructor() { }

  ngOnInit() {
  }

  btnClick($event) {
    this.selected = $event;
    this.valueChange.emit($event);
  }

  hasPatientHistory(): boolean {
    return this.patient && (!!this.patient.medicalCondition || !!this.patient.medication || !!this.patient.allergy);
  }

  resetSelect() {
    this.selected = '0';
  }

  get icCalender() {
    return this.selected == '1' ? 'selected' : 'unselected';
  }

  get icApptHistory() {
    return this.selected == '3' ? 'selected' : 'unselected';
  }

  get icVideo() {
    return this.selected == '4' ? 'selected' : 'unselected';
  }

}
