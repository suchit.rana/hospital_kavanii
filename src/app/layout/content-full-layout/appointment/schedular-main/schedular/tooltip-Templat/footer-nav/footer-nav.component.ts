import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {
  IPatientAppointmentRecurringData,
  PatientAppointmentService
} from '../../../../../patients/rest-service/app.patient-appointment.service';
import {AppointmentConfirmationStatus, AppointmentStatus, BubbleAppointment} from '../../rest-service/app.appointment-tooltip.service';
import {
  AskedCancelledMissedReasonComponent,
  CANCEL_REBOOK
} from '../../../../../patients/patient-appointments/appointment-popup/asked-cancelled-missed-reason/asked-cancelled-missed-reason.component';
import {CANCEL, SAVE} from '../../../../../../../shared/popup-component/popup-component.component';
import {AppDialogService} from '../../../../../../../shared/services/app-dialog.service';
import {formatREString} from '../../../../../../../app.component';
import {AppoinmentDateUtils} from '../../../../AppoinmentDateUtils';
import {AppointmentHandlerService, REBOOK} from '../../service/appointment-handler.service';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-footer-nav',
  templateUrl: './footer-nav.component.html',
  styleUrls: ['./footer-nav.component.css']
})
export class FooterNavComponent implements OnInit {
  readonly COMPLETED = '10';
  readonly CANCELLED = '9';
  readonly MISSED = '8';
  readonly CHANG_STATUS = '6';
  readonly MORE = '7';

  readonly CNF_STATUS_UNCONFIRMED = AppointmentConfirmationStatus.Unconfirmed;
  readonly CNF_STATUS_CONFIRMED = AppointmentConfirmationStatus.Confirmed;
  readonly CNF_STATUS_REPLIEDNO = AppointmentConfirmationStatus.RepliedNo;
  readonly CNF_STATUS_WAITINGROOMARRIVED = AppointmentConfirmationStatus.WaitingRoomArrived;
  readonly CNF_STATUS_WAITINGROOMDISMISSED = AppointmentConfirmationStatus.WaitingRoomDismissed;

  readonly STATUS_PENDING = AppointmentStatus.Pending;
  readonly STATUS_COMPLETED = AppointmentStatus.Completed;
  readonly STATUS_MISSED = AppointmentStatus.Missed;
  readonly STATUS_CANCELLED = AppointmentStatus.Cancelled;
  readonly STATUS_DELETED = AppointmentStatus.Deleted;

  @Input()
  recurringData: IPatientAppointmentRecurringData;
  @Input()
  patientId: string;
  @Input()
  appt: BubbleAppointment;
  @Output()
  valueChange: EventEmitter<string> = new EventEmitter<string>();

  confirmationStatus: AppointmentConfirmationStatus = this.CNF_STATUS_UNCONFIRMED;
  status: AppointmentStatus = AppointmentStatus.Pending;

  @ViewChild('cancelledPastApptWarning', {static: false}) cancelledPastApptWarning;
  @ViewChild('missedFutureApptWarning', {static: false}) missedFutureApptWarning;
  selected = '1';

  constructor(
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
    private patientAppointmentService: PatientAppointmentService,
    private apptHandlar: AppointmentHandlerService,
  ) {
  }

  ngOnInit() {
    this.confirmationStatus = this.appt.confirmationStatus;
    this.status = this.appt.status;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('appt') && this.appt) {
      this.confirmationStatus = this.appt.confirmationStatus;
      this.status = this.appt.status;
    }
  }

  popupFooterBtnClick($event) {
    this.selected = $event;
    this.valueChange.emit($event);
    this.update($event);
  }

  update(clicked: string) {
    let status;
    let isRecurringChange = false;
    delete this.recurringData.status;
    delete this.recurringData.type;
    switch (clicked) {
      case this.COMPLETED:
        status = AppointmentStatus.Completed;
        this.patientAppointmentService.update(this.patientId, this.appt.appointmentEntryId, this.appt.confirmationStatus, status, this.recurringData, '', isRecurringChange).subscribe(response => {
          this.alertService.displaySuccessMessage('Appointment completed successfully.');
          this.apptHandlar.reloadAppt(this.appt.appointmentId);
          this.apptHandlar.hideTooltip();
        }, error => {
          this.alertService.displaySuccessMessage(error || 'Failed to complete appointment.');
        });
        this.apptHandlar.hideTooltip();
        break;
      case this.CANCELLED:
        if (AppoinmentDateUtils.getDiff(this.recurringData.startDateUTC, new Date()) > 0) {
          const warningDialog = this.appDialogService.open(this.cancelledPastApptWarning, {
            title: 'Warning',
            height: '200px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
          });
          warningDialog.close.subscribe((value) => {
            if (value == SAVE) {
              this.cancelledAppt();
            } else if (value == CANCEL) {
              warningDialog.closeDialog();
              this.apptHandlar.openTooltip();
            }
          });
          return;
        } else {
          this.cancelledAppt();
        }
        this.apptHandlar.hideTooltip();
        break;
      case this.MISSED:
        this.apptHandlar.hideTooltip();
        if (AppoinmentDateUtils.getDiff(this.recurringData.startDateUTC, new Date()) < 0) {
          const warningDialog = this.appDialogService.open(this.missedFutureApptWarning, {
            title: 'Warning',
            height: '200px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
          });
          warningDialog.close.subscribe((value) => {
            if (value == SAVE) {
              this.missedAppt();
            } else if (value == CANCEL) {
              warningDialog.closeDialog();
              this.apptHandlar.openTooltip();
            }
          });
          return;
        } else {
          this.missedAppt();
        }
        this.apptHandlar.hideTooltip();
        break;
    }
  }

  resetSelect() {
    this.selected = '0';
  }

  get icApptStatusChange() {
    return this.selected == '6' ? 'selected' : 'unselected';
  }

  get icMore() {
    return this.selected == '7' ? 'selected' : 'unselected';
  }

  private cancelledAppt() {
    let isRecurringChange = false;
    if (this.recurringData) {
      this.recurringData.recurrenceException = formatREString(this.recurringData.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(this.recurringData.startDateUTC));
      delete this.recurringData.status;
      delete this.recurringData.type;
      isRecurringChange = true;
    }
    const ref = AskedCancelledMissedReasonComponent.open(this.appDialogService, 'CANCELLED', this.patientId, this.appt.appointmentEntryId, this.appt.confirmationStatus, AppointmentStatus.Cancelled, this.recurringData, isRecurringChange, true);
    ref.close.subscribe((value) => {
      if (value == SAVE) {
        this.apptHandlar.reloadAppt(this.appt.appointmentId);
      }else if (value == CANCEL_REBOOK) {
        this.apptHandlar.notifyRebookMove(REBOOK);
        this.apptHandlar.reloadAppt(this.appt.appointmentId);
      } else if (value == CANCEL) {
        this.apptHandlar.openTooltip();
      }
    });
  }

  private missedAppt() {
    const ref = AskedCancelledMissedReasonComponent.open(this.appDialogService, 'MISSED', this.patientId, this.appt.appointmentEntryId, this.appt.confirmationStatus, AppointmentStatus.Missed, this.recurringData, false, false);
    ref.close.subscribe((value) => {
      if (value == SAVE) {
        this.apptHandlar.reloadAppt(this.appt.appointmentId);
      } else if (value == CANCEL) {
        this.apptHandlar.openTooltip();
      }
    });
  }

}
