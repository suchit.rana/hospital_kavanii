import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BubbleAppointment, BubblePatient} from '../../rest-service/app.appointment-tooltip.service';

@Component({
  selector: 'app-patient-history',
  templateUrl: './patient-history.component.html',
  styleUrls: ['./patient-history.component.scss']
})
export class PatientHistoryComponent implements OnInit, OnChanges {

  @Input()
  data: BubblePatient;

  medicalCondition: string;
  allergy: string;
  medication: string;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data') && this.data) {
      this.medicalCondition = this.data.medicalCondition;
      this.allergy = this.data.allergy;
      this.medication = this.data.medication;
    }
  }

}
