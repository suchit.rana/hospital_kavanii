import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BubbleAppointment} from '../../rest-service/app.appointment-tooltip.service';
import {AppoinmentDateUtils} from '../../../../AppoinmentDateUtils';
import {AppointmentHandlerService} from '../../service/appointment-handler.service';
import {TreatmentType} from '../../../../../../../models/app.appointment.model';
import {RP_MODULE_MAP} from '../../../../../../../shared/model/RolesAndPermissionModuleMap';
import {AppState} from '../../../../../../../app.state';
import {BaseItemComponent} from '../../../../../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';

@Component({
    selector: 'app-listdata',
    templateUrl: './listdata.component.html',
    styleUrls: ['./listdata.component.scss']
})
export class ListdataComponent extends BaseItemComponent implements OnInit, OnChanges {
    readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

    readonly PRODUCT = TreatmentType.Product;
    readonly SERVICE = TreatmentType.Service;

    @Input()
    data: BubbleAppointment;

    apptDateStr = '';

    constructor(
        public location: Location,
        private apptHandle: AppointmentHandlerService,
        protected appState: AppState,
    ) {
        super(location, null, appState);
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('data') && this.data) {
            const totalMinute = AppoinmentDateUtils.getDiff(this.data.startDateTime, this.data.endDateTime, 'minutes').toString();
            this.apptDateStr = AppoinmentDateUtils.format(this.data.startDateTime, 'ddd, DD MMM YYYY, HH:mm A') + ` (${parseInt(totalMinute) + 1} mins)`;
        }
    }

    editAppt() {
        if (!this.hasPermission(this.APPT_BASIC, 'E')) {
            return;
        }
        this.apptHandle.notifyEditAppt();
    }

    hasOffering(): boolean {
        return this.data && this.data.offerings && this.data.offerings.length > 0;
    }

}
