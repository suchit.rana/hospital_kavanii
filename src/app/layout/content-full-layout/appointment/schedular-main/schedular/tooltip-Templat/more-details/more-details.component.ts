import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {For, FromPage} from '../../../../../../../shared/interface';
import {PATIENT_TAB_LETTERS, PATIENT_TAB_TREATMENT_NOTES} from '../../../../../../../shared/Constant';
import {
  AppointmentStatus,
  BubbleAppointment,
  BubblePatient,
  LetterTreatmentStatus,
  Offerings
} from '../../rest-service/app.appointment-tooltip.service';
import {AppointmentHandlerService} from '../../service/appointment-handler.service';
import {SchedulerManageService} from '../../../../scheduler-manage.service';
import {TreatmentType} from '../../../../../../../models/app.appointment.model';
import {AppDialogService} from '../../../../../../../shared/services/app-dialog.service';
import {FormControl, Validators} from '@angular/forms';
import {PreviewLetterComponent} from '../../../../../patients/patient-letters/preview-letter/preview-letter.component';
import {RP_MODULE_MAP} from '../../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-more-details',
  templateUrl: './more-details.component.html',
  styleUrls: ['./more-details.component.scss']
})
export class MoreDetailsComponent implements OnInit {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly STATUS_PENDING = AppointmentStatus.Pending;
  readonly STATUS_COMPLETED = AppointmentStatus.Completed;

  @Input()
  data: BubbleAppointment;
  @Input()
  patient: BubblePatient;

  @ViewChild('serviceSelectionTemplate', {static: false}) serviceSelectionTemplate;

  isServiceAdded: boolean = false;
  selectedService = new FormControl('', Validators.required);
  serviceList: Offerings[] = [];

  constructor(
    private router: Router,
    private apptHandlar: AppointmentHandlerService,
    private schedulerManageService: SchedulerManageService,
    private appDialogService: AppDialogService
  ) {
  }

  ngOnInit() {
    if (this.data.offerings) {
      this.serviceList = this.data.offerings.filter(x => x.treatementType == TreatmentType.Service);
      this.isServiceAdded = this.serviceList.length > 1;
      const hasTreatMentNotes = this.data.treatmentNotesStatus && this.data.treatmentNotesStatus.length >= 0;
      this.serviceList.map(data => {
        let status = '';
        if (hasTreatMentNotes) {
          const treatmentNote = this.data.treatmentNotesStatus.find(x => x.serviceId === data['serviceId']);
          if (treatmentNote) {
            data['treatmentNotesId'] = treatmentNote.id;
            status = treatmentNote.status === LetterTreatmentStatus.Draft ? '- Pending' : '- Finalise';
          }
        }
        data['id'] = data['serviceId'];
        data['title'] = `${data['code']} - ${data['name']} ($${data['actualPrice'] || 0}, ${data['treatementType'] == TreatmentType.Service ? data['duration'] : data['units']}) ${status}`;
      });
    }
  }

  addLetter(isEdit: boolean = false) {
    let letterId = '';
    if (this.data.patientLetterStatus && this.data.patientLetterStatus.length > 0 ) {
      letterId = this.data.patientLetterStatus[0].id;
      if (this.data.patientLetterStatus[0].status === LetterTreatmentStatus.Finalise) {
        PreviewLetterComponent.open(this.appDialogService, letterId, this.patient.id);
        return;
      }
    }
    let fromState: NavigationExtras = {
      state: {
        fromState: true,
        fromPage: FromPage.Appointment,
        for: For.Letter,
        data: {
          tabIndex: PATIENT_TAB_LETTERS,
          forAdd: !isEdit,
          appointment: this.data,
          caseId: this.data.caseId,
          letterId: letterId
        }
      }
    };
    this.apptHandlar.setSchedulerState(this.schedulerManageService.currentDate.getValue(), this.schedulerManageService.getCurrentView(), this.schedulerManageService.selectedPractitioner, false);
    this.router.navigate(['patients/edit/' + this.patient.id], fromState);
  }

  addTreatmentNote(isEdit: boolean = false) {
    if (this.serviceList && this.serviceList.length > 1) {
      const dialogRef = this.appDialogService.open(this.serviceSelectionTemplate, {
        title: 'SELECT SERVICE',
        width: '778px',
        saveBtnTitle: 'Yes',
        cancelBtnTitle: 'No',
        hideOnSave: false,
      });
      dialogRef.clickSave.subscribe(() => {
        if (this.selectedService.valid) {
          dialogRef.closeDialog();
        }
        const selectedService = this.serviceList.find(x => x.serviceId === this.selectedService.value);
        this.gotoTreatmentNote(selectedService, !!selectedService.treatmentNotesId);
      });
      return;
    }
    this.gotoTreatmentNote(this.serviceList[0], isEdit);
  }

  private gotoTreatmentNote(service: Offerings, isEdit: boolean = false) {
    let fromState: NavigationExtras = {
      state: {
        fromState: true,
        fromPage: FromPage.Appointment,
        for: For.TreatmentNotes,
        data: {
          tabIndex: PATIENT_TAB_TREATMENT_NOTES,
          forAdd: !isEdit,
          appointment: this.data,
          service: service,
          caseId: this.data.caseId,
          treatmentNoteId: service.treatmentNotesId,
        }
      }
    };
    this.apptHandlar.setSchedulerState(this.schedulerManageService.currentDate.getValue(), this.schedulerManageService.getCurrentView(), this.schedulerManageService.selectedPractitioner, false);
    this.router.navigate(['patients/edit/' + this.patient.id], fromState);
  }

}
