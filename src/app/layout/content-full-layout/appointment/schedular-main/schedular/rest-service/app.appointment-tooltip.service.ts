import {Injectable} from '@angular/core';
import {BaseService} from '../../../../../../services/app.base.service';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../../../app.state';
import {Appointment, TreatmentType} from '../../../../../../models/app.appointment.model';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';

@Injectable({
  providedIn: 'root'
})
export class AppAppointmentTooltipService extends BaseService {

  constructor(
    public http: HttpClient,
    private appState: AppState
  ) {
    super(http);
  }

  getTooltipData(appointmentId: string, appointmentDate: string) {
    if (!appointmentDate || !appointmentId) {
      return;
    }
    return this.http.get<BubblePopupDetails>(
      this.environmentSettings.apiBaseUrl +
      '/GetBubblePopUpDetails',
      {
        params: {
          'appointmentId': appointmentId,
          'locationId': this.appState.selectedUserLocationId,
          'appointmentDate': AppoinmentDateUtils.toUtc(appointmentDate)
        }
      }
    );
  }

}

export interface BubblePopupDetails {
  bubblePatient: BubblePatient,
  bubbleAppointment: BubbleAppointment,
  bubbleStatistics: BubbleStatics
}

export interface BubbleAppointment {
  appointmentId?: string,
  appointmentEntryId?: string,
  startDateTime?: Date,
  endDateTime?: Date,
  practitionerId?: string,
  practitionerName?: string,
  appointmentType?: string,
  offerings?: Offerings[],
  case?: string,
  caseId?: string,
  payer?: IBubbleApptReferralPayer,
  referral?: IBubbleApptReferralPayer,
  notes?: string,
  reminderCommunication?: string,
  statusValue?: string,
  status: AppointmentStatus,
  confirmationStatus: AppointmentConfirmationStatus,
  totalFees: number;
  isNeedAttention: boolean;
  statusReasonMessage: string;
  patientLetterStatus: {id: string, status: LetterTreatmentStatus}[];
  treatmentNotesStatus: {id: string, serviceId: string, status: LetterTreatmentStatus}[];
}

export interface BubbleStatics {
  createdBy: string,
  createdDate: Date,
  futureAppts: Appointment[],
  modifiedBy: string,
  modifiedDate: Date,
  prevAppt: Appointment;
  thisYearAppts: number,
  totalAppts: number
}

export interface Offerings {
  treatmentNotesId: string;
  productId: string,
  serviceId: string,
  code: string,
  name: string,
  units: string,
  defaultTreatmentNotesTemplateId: string,
  duration: number,
  price: number,
  actualPrice: number,
  tax: number,
  treatementType: TreatmentType,
}

export interface BubblePatient {
  id?: string,
  patientId?: string,
  getFullNameWithTitle?: string,
  patientStatus?: string,
  patientClassification?: string,
  dob?: string,
  homePhone?: string,
  mobile?: string,
  email?: string,
  medicalCondition?: string,
  allergy?: string,
  medication?: string,
  initial?: string,
  patientPhoto?: string,
  gender?: string,
}

export enum AppointmentStatus {
  Pending,
  Completed,
  Missed,
  Cancelled,
  Deleted
}


export interface IBubbleApptReferralPayer {
  type: number,
  contactName: string,
  count: number,
  ordered: number,
  endDate: Date
}

export enum AppointmentConfirmationStatus {
  Unconfirmed,
  Confirmed,
  RepliedNo,
  WaitingRoomArrived,
  WaitingRoomDismissed
}

export enum LetterTreatmentStatus {
  Draft,
  Finalise
}
