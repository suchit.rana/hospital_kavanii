import {Injectable} from '@angular/core';
import {BaseService} from '../../../../../../services/app.base.service';
import {IPaDates} from '../interface/pratitioner-availability';

@Injectable({
    providedIn: 'root'
})
export class PractitionerAvailabilityService extends BaseService {

    get(params: { [x: string]: string }) {
        return this.http.get<IPaDates[]>(this.environmentSettings.apiBaseUrl + '/GetAppointmentsAvailability', {
            params: params
        });
    }

}
