import {Injectable} from '@angular/core';
import {BaseService} from '../../../../../../services/app.base.service';
import {WaitList} from '../interface/wait-list';

export const REBOOK = 0;
export const MOVE = 1;
export const NONE = -1;

@Injectable({
  providedIn: 'root'
})
export class WaitListService extends BaseService {

  createWaitList(data: any) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/CreatePatientWaitList', data);
  }

  updateWaitList(data: any) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/UpdatePatientWaitList', data);
  }

  deleteWaitList(id: string) {
    return this.http.delete(this.environmentSettings.apiBaseUrl + '/RemovePatientWaitList', {
      params: {id}
    });
  }

  getAllPatientWaitListByLocation(locationId: string, startDateUTC: Date, endDateUTC: Date) {
    const params = {locationId};
    if (startDateUTC) {
      params['startDateUTC'] = startDateUTC.toJSON();
    }
    if (endDateUTC) {
      params['endDateUTC'] = endDateUTC.toJSON();
    }
    return this.http.get<WaitList[]>(this.environmentSettings.apiBaseUrl + '/GetAllPatientWaitListByLocation', {
      params: params
    });
  }

}
