import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxRruleComponent} from '../../../../../../../ngx-rrule/src/lib/ngx-rrule.component';
import * as moment from 'moment';
import {Break, RecurringDates, Schedule} from '../../../../../../models/app.appointment.model';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {AppointmentService} from '../../../../../../services/app.appointment.service';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {Constant} from '../../../../../../shared/Constant';
import {AppointmentType, EditMode, Frequency} from '../../../../../../enum/application-data-enum';
import {BehaviorSubject, Subscription} from 'rxjs';
import {DateUtils} from '../../../../../../shared/DateUtils';
import {startEndTimeValidator} from '../../../../../../shared/validation';
import {ApplicationDataModel} from '../../../../../../models/app.settings.model';
import {AppState} from '../../../../../../app.state';
import {SchedulerManageService} from '../../../scheduler-manage.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-add-edit-schedule',
  templateUrl: './add-edit-schedule.component.html',
  styleUrls: ['./add-edit-schedule.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AddEditScheduleComponent implements OnInit, OnDestroy {
  readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

  readonly EDIT_DIALOG = 'EDIT_DIALOG';
  readonly DELETE_DIALOG = 'DELETE_DIALOG';
  readonly CONFLICT_DIALOG = 'CONFLICT_DIALOG';

  readonly TYPE_SCHEDULE = AppointmentType.SCHEDULE;
  readonly TYPE_BREAK = AppointmentType.BREAK;

  private openedDialog = null;

  @ViewChild('rrule', {static: true}) public rrule: NgxRruleComponent;

  @Input()
  public startTime = 8;

  @Input()
  public endTime = 17;

  @Input()
  public practitioners: PractitionerSpecialityModel[] = [];
  @Input()
  public isNew = false;
  @Input()
  public interval = 15;
  @Input()
  public editMode: EditMode = null;

  @Input()
  public set schedule(ev: any) {
    if (ev) {
      this.isNew = !ev.id || ev.id == '';
      this.isRecurringSchedule = ev.recurrenceRule && ev.recurrenceRule != '';

      if (ev.frequencyType == Frequency.SameAsSchedule) {
        ev.recurrenceRule = ev.recurrenceRule.replace('DAILY', 'ROSD');
      }
      this.editForm.patchValue(AppUtils.refrenceClone(ev));
      this.data = AppUtils.refrenceClone(ev);
      this.setStartAndEndDate();
    }
  }

  @Input()
  public targetData: Schedule = null;

  @Output()
  public cancel: EventEmitter<any> = new EventEmitter();

  @Output()
  public save: EventEmitter<any> = new EventEmitter();

  data: Schedule = null;
  public _startTime: any = new Date();
  public _endTime: any = new Date();
  public _endTimeForEndTime: any = new Date();
  apperance: string = 'outline';
  isDialogOpen: boolean = false;
  isDADialogOpen: boolean = false;
  dialogTitle: string = 'Title';
  yesButtonName: string = 'Yes';
  noButtonName: string = 'No';
  dateConflicts: RecurringDates[] = [];
  isUpserting: boolean = false;
  isClickedOnlyThisOccurrence: boolean = false;
  isClickedThisNFutureOccurrence: boolean = false;
  dialogContent: string = '';
  isRecurringSchedule: boolean = false;
  isDisplayNoButton: boolean = true;
  isDeleting: boolean = false;
  min;
  endError: string = null;

  public editForm: FormGroup = new FormGroup({
    'id': new FormControl(''),
    'startDateUTC': new FormControl('', Validators.required),
    'endDateUTC': new FormControl('', Validators.required),
    'isAllDay': new FormControl(false),
    'recurrenceRule': new FormControl(),
    'recurrenceException': new FormControl(),
    'bookOnline': new FormControl(false),
    'note': new FormControl(),
    'locationId': new FormControl(''),
    // 'relateTo': new FormControl(''),
    'frequencyType': new FormControl(''),
    'practitionerId': new FormControl('', Validators.required),
    'breakType': new FormControl(''),
    'title': new FormControl(''),
    '_startDate': new FormControl('', Validators.required),
    '_endDate': new FormControl('', Validators.required),
    'type': new FormControl('', Validators.required),
  }, startEndTimeValidator('startDateUTC', 'endDateUTC'));

  formData;

  // OPEN DIALOG SUBSCRIBER
  DASubject: BehaviorSubject<RecurringDates[]> = new BehaviorSubject<RecurringDates[]>(null);

  breaks: ApplicationDataModel[] = [];

  recurrFrequency = ['No Recurrence', 'Daily', 'Monthly', 'Weekly', 'Yearly'];
  private subscription: Subscription[] = [];

  constructor(
    private appState: AppState,
    private appointmentService: AppointmentService,
    private alertService: AppAlertService,
    public manageScheduleService: SchedulerManageService) {
    const sub = manageScheduleService.breaks$.subscribe(data => {
      this.breaks = data;
    });
    this.subscription.push(sub);
    manageScheduleService.getBreaks();
  }

  ngOnInit() {
    if (this.isBreak) {
      this.recurrFrequency = ['No Recurrence', 'ROSD', 'Daily', 'Monthly', 'Weekly', 'Yearly'];
      this.editForm.get('breakType').setValidators(Validators.required);
      this.editForm.get('breakType').setValue(this.editForm.get('title').value);
      this.editForm.updateValueAndValidity();
    }
    this.endError = null;
    if (this.startTime || this.startTime == 0) {
      this._startTime = new Date(moment().set({date: new Date(this.editForm.get('startDateUTC').value).getDate(), 'hour': this.startTime, 'minute': 0}).format());
    }

    if (this.endTime || this.endTime == 0) {
      this._endTime = moment().set({date: new Date(this.editForm.get('startDateUTC').value).getDate(), 'hour': this.endTime, 'minute': 0});
      this._endTimeForEndTime = new Date(moment().set({date: new Date(this.editForm.get('startDateUTC').value).getDate(), 'hour': this.endTime, 'minute': 0}).format());
    }

    this.min = new Date(this._startTime);
    this.getMinHour(false);
    this.editForm.get('startDateUTC').valueChanges.subscribe(value => {
      this.getMinHour(true);
    });

    this.DASubject.subscribe(value => {
      if (value) {
        this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
        if (this.isRecurringSchedule || (this.formData.recurrenceRule && this.formData.recurrenceRule != '')) {
          this.dateConflicts = value;
          this.openDateAvailabilityDialog();
          this.isUpserting = false;
          this.isClickedThisNFutureOccurrence = false;
          this.isClickedOnlyThisOccurrence = false;
          return;
        }
        this.conflictScheduleDialog();
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.forEach(value => {
      if (value) {
        value.unsubscribe();
      }
    });
  }

  get selectedPractitioner(): PractitionerSpecialityModel {
    let pId = this.editForm.get('practitionerId').value;

    return pId && this.practitioners ? this.practitioners.find(p => p.practitionerId == pId) : null;
  }

  upsert(data, onCompleted?: BehaviorSubject<boolean>, isHidePopup: boolean = true) {
    if (this.isNew) {
      delete data.id;
    }
    this.removeExtraKeys(data);
    this.formData = data;

    if (this.isNew) {
      if (AppoinmentDateUtils.isLessStrict(data._startDate, new Date())) {
        this.alertService.displayErrorMessage(`You can\'t create Appointment in past.`);
        return;
      }
      this.add(data, onCompleted, isHidePopup);
    } else {
      this.update(data, onCompleted, isHidePopup);
    }
  }

  prepareEmitData(data, type, isError: boolean, isHidePopup: boolean, secondaryType: string) {
    let emitData = {};
    emitData['data'] = data;
    emitData['type'] = type;
    emitData['isError'] = isError;
    emitData['isHidePopup'] = isHidePopup;
    emitData['secondaryType'] = secondaryType;
    emitData['isNotify'] = true;
    return emitData;
  }

  private add(data, onCompleted, isHidePopup: boolean) {
    let secondryType = this.isEditingThisAndFutureSeries || this.isEditingCurrentOnly ? Constant.EDIT : this.saveEventType;
    this.appointmentService.createSchedule(data).subscribe(response => {
      if (onCompleted) {
        onCompleted.next(true);
      }
      if (response.response.toLocaleLowerCase() === 'success') {
        this.closeDateAvailabilityDialog();
        this.notify(response.practitionerSchedules, Constant.ADD, false, isHidePopup, secondryType);
      } else if (response.response.toLocaleLowerCase() === 'error') {
        this.DASubject.next(response.recurringDateResults);
      }
      this.isUpserting = false;
      this.isDeleting = false;
      this.isClickedThisNFutureOccurrence = false;
      this.isClickedOnlyThisOccurrence = false;
    }, error => {
      this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
      this.notify(data, Constant.ADD, true, false, secondryType);
      this.isUpserting = false;
      this.isDeleting = false;
      this.isClickedThisNFutureOccurrence = false;
      this.isClickedOnlyThisOccurrence = false;
    });
  }

  private update(data, onCompleted, isHidePopup: boolean) {
    this.appointmentService.updateSchedule(data).subscribe(response => {
      if (response.response.toLocaleLowerCase() === 'success') {
        this.closeDateAvailabilityDialog();
        this.notify(response.practitionerSchedules, Constant.EDIT, false, isHidePopup, this.saveEventType);
        if (onCompleted) {
          onCompleted.next(true);
        }
      } else if (response.response.toLocaleLowerCase() === 'error') {
        this.DASubject.next(response.recurringDateResults);
      }
      this.isUpserting = false;
      this.isDeleting = false;
      this.isClickedThisNFutureOccurrence = false;
      this.isClickedOnlyThisOccurrence = false;
    }, error => {
      this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
      this.notify(data, Constant.EDIT, true, false, this.saveEventType);
      this.isUpserting = false;
      this.isDeleting = false;
      this.isClickedThisNFutureOccurrence = false;
      this.isClickedOnlyThisOccurrence = false;
    });
  }

  private delete(id: string) {
    this.appointmentService.deleteSchedule(id).subscribe(response => {
      this.save.emit(this.prepareEmitData({id: id, type: this.data.type}, Constant.DELETE, false, true, Constant.DELETE));
      this.isUpserting = false;
      this.closeDialog();
      this.isDeleting = false;
    }, error => {
      this.save.emit(this.prepareEmitData({id: id, type: this.data.type}, Constant.DELETE, true, false, Constant.DELETE));
      this.isUpserting = false;
      this.isDeleting = false;
      this.isClickedThisNFutureOccurrence = false;
      this.isClickedOnlyThisOccurrence = false;
    });
  }

  onSave(isForUpdate: boolean = true) {
    if (this.editForm.invalid) {
      for (let controlsKey in this.editForm.controls) {
        this.editForm.get(controlsKey).markAsTouched();
        this.editForm.get(controlsKey).updateValueAndValidity();
      }
      return;
    }

    if (this.isBreak && this.recurrenceRule.repeat.frequency == 'ROSD') {
      let noOfDays = this.recurrenceRule.end.after;
      if (this.endBy === 'On date') {
        noOfDays = AppoinmentDateUtils.getDiff(this.editForm.get('_startDate').value, this.recurrenceRule.end.endAt);
      }
      if (noOfDays > 1) {
        this.appointmentService.getPractitionerWithNoScheduleDates(AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(AppoinmentDateUtils.getDateWithTz(this.editForm.get('_startDate').value), {
          'hour': this.getHour(this.editForm.get('startDateUTC').value),
          'minute': this.getMinute(this.editForm.get('startDateUTC').value)
        })), this.appState.selectedUserLocationId, this.editForm.get('practitionerId').value, noOfDays).subscribe(response => {
          if (response && response.length > 0) {
            let unAvailableDates = response[0]['notSchedule'];
            if (unAvailableDates && unAvailableDates.length > 0) {
              let exception = unAvailableDates.map((value) => AppoinmentDateUtils.toUtcRecurrenceException(AppoinmentDateUtils.combineDateTime(AppoinmentDateUtils.getDateWithTz(value), {
                'hour': this.getHour(value),
                'minute': this.getMinute(value),
                'second': 1
              })));
              // console.log(exception)
              // let exception = unAvailableDates.map((value) => value.replace(/[-\:]/g, ''));
              this.editForm.get('recurrenceException').setValue(exception.toString());
              this.processData(isForUpdate);
            } else {
              this.processData(isForUpdate);
            }
          }
        });
      } else {
        this.processData(isForUpdate);
      }
    } else {
      this.processData(isForUpdate);
    }
  }

  processData(isForUpdate: boolean = true) {
    this.isUpserting = true;

    if (!this.isNew && isForUpdate && this.isRecurringSchedule) {
      this.editScheduleDialog();
      return;
    }

    this.editForm.get('title').setValue(this.editForm.get('breakType').value);
    if (!this.isNew) {
      let isTargetEqual = AppoinmentDateUtils.isEqual(this.data.startDate, this.targetData.startDate) && AppoinmentDateUtils.isEqual(this.data.endDate, this.targetData.endDate);
      if (this.isEditingCurrentOnly) {
        let d: Schedule = this.prepareData(this.data);
        let td: Schedule = this.prepareData(this.targetData);
        d.recurrenceException = this.formatREString(d.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(td.startDateUTC));

        let completed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
        if (!this.isDeleting) {
          completed.subscribe(value => {
            if (value != null && value) {
              this.initFormFromTargetData();
              this.isNew = !this.isDeleting;
              this.onSave(true);
              completed.unsubscribe();
            }
          });
        }

        this.upsert(d, completed, (this.isDeleting || isTargetEqual));
      } else if (this.isEditingThisAndFutureSeries) {
        let d: Schedule;
        let td: Schedule = null;
        if (!isTargetEqual) {
          d = this.prepareData(this.data); // Prepare current form date
          td = this.prepareData(this.targetData); // Prepare Targeted Data
          if (d.recurrenceRule.includes(';UNTIL')) { // Check Already has UNTIL
            d.recurrenceRule = d.recurrenceRule.substring(0, d.recurrenceRule.lastIndexOf(';UNTIL')); // If Found then get REMOVE IT TO ADD NEW
          }
          let until = AppoinmentDateUtils.addAndGetNewValue(td.endDateUTC, -1, 'd');
          let h = parseInt(AppoinmentDateUtils.format(d.endDateUTC, 'HH'));
          let m = parseInt(AppoinmentDateUtils.format(d.endDateUTC, 'mm'));
          let s = parseInt(moment(d.endDateUTC).format('ss'));
          if (d.recurrenceRule.includes(';COUNT')) {
            d.recurrenceRule = d.recurrenceRule.substring(0, d.recurrenceRule.lastIndexOf(';COUNT'));
          }
          d.recurrenceRule += ';UNTIL=' + AppoinmentDateUtils.toUtcUntilRecurrenceException(until, h, m, s, true);
        } else {
          let value = this.editForm.value;
          value.recurrenceRule = value.recurrenceRule.rRule.toString();
          d = this.prepareData(value);
        }
        let completed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
        if (!this.isDeleting) {
          completed.subscribe(value => {
            if (value != null && value) {
              if (!isTargetEqual) {
                this.initFormFromTargetData();
                this.isNew = !this.isDeleting;
                this.onSave(true);
              }
              completed.unsubscribe();
            }
          });
        }
        this.upsert(d, completed, (this.isDeleting || isTargetEqual));
      } else {
        let value = this.editForm.value;
        value.recurrenceRule = value.recurrenceRule.rRule.toString();
        let data = this.prepareData(value);
        this.upsert(data, null, true);
      }
    } else {
      let value = this.editForm.value;
      value.recurrenceRule = value.recurrenceRule.rRule.toString();
      if (this.isEditingThisAndFutureSeries) {
        const endBy = this.recurrenceRule.end ? this.recurrenceRule.end.mode : null;
        const frequency = this.recurrenceRule.repeat ? this.recurrenceRule.repeat.frequency : null;
        if (endBy === 'After' && frequency != 'Weekly') {
          const diff = AppoinmentDateUtils.getDiff(this.data.startDate, value.startDateUTC);
          const endAfter = this.recurrenceRule.end.after - diff;
          value.recurrenceRule = value.recurrenceRule.replace('COUNT=' + this.recurrenceRule.end.after, 'COUNT=' + endAfter);
        }
      }
      // if (this.isEditingCurrentOnly) {
      //   value.recurrenceRule = '';
      //   value.recurrenceException = null;
      // }
      let data = this.prepareData(value);
      this.upsert(data, null, true);
    }
  }

  notify(data, type: string, isError?: boolean, isHidePopup?: boolean, secondaryType?: string) {
    if (!secondaryType || secondaryType == '') {
      secondaryType = type;
    }
    let _data = null;
    if (data.type == AppointmentType.BREAK) {
      _data = new Break(data);
    } else {
      _data = new Schedule(data);
    }
    let nData = this.prepareEmitData(AppUtils.refrenceClone(_data), type, isError, isHidePopup, secondaryType);
    this.manageScheduleService.notify(nData);
  }

  formatRRule(rrule: string) {
    return (rrule.includes('UNTIL') && rrule.lastIndexOf('Z') < 0) ? rrule + 'Z' : rrule;
  }

  prepareData(value) {
    let data: Schedule = JSON.parse(JSON.stringify(value));
    delete data._startDate;
    delete data._endDate;

    data.recurrenceRule = this.formatRRule(value.recurrenceRule.toString().replace('RRULE:', ''));
    data.frequencyType = this.getFrequencyEnum(this.recurrenceRule.repeat.frequency);
    if (this.isBreak && this.recurrenceRule.repeat.frequency == 'ROSD') {
      data.recurrenceRule = 'FREQ=DAILY;INTERVAL=1;' + data.recurrenceRule.replace('FREQ=ROSD;INTERVAL=1;', '');
    }
    if (data.recurrenceRule && data.recurrenceRule.includes('FREQ=ROSD')) {
      data.recurrenceRule = data.recurrenceRule.replace('ROSD', 'DAILY');
    }
    // data.recurrenceRule = this.data.recurrenceRule.replace('ROSD', 'DAILY');

    value._startDate = AppoinmentDateUtils.getDateWithTz(value._startDate);
    data.startDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._startDate, {
      'hour': this.getHour(data.startDateUTC),
      'minute': this.roundOffMinute(data.startDateUTC),
      'second': 1
    }));
    value._endDate = AppoinmentDateUtils.getDateWithTz(value._endDate);

    data.endDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._endDate, {
      'hour': this.getHour(data.endDateUTC),
      'minute': this.roundOffMinute(data.endDateUTC)
    }));
    return data;
  }

  initFormFromTargetData() {
    let startDate = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTimeDefault(this.targetData.startDate, {
      'hour': this.getHour(this.editForm.get('startDateUTC').value),
      'minute': this.getMinute(this.editForm.get('startDateUTC').value),
      'second': 1
    }));

    let endDate = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTimeDefault(this.targetData.endDate, {
      'hour': this.getHour(this.editForm.get('endDateUTC').value),
      'minute': this.getMinute(this.editForm.get('endDateUTC').value),
    }));

    // this.editForm.get('relateTo').setValue(this.targetData.id);
    if (this.isEditingCurrentOnly) {
      if (this.targetData.recurrenceRule == this.editForm.get('recurrenceRule').value.rRule.toString().replace('RRULE:', '')) {
        this.editForm.get('recurrenceRule').setValue('');
      }
      this.editForm.get('recurrenceException').setValue('');
    }
    this.editForm.get('id').setValue(null);
    this.editForm.get('_startDate').setValue(startDate);
    this.editForm.get('_endDate').setValue(endDate);
    this.editForm.get('startDateUTC').setValue(startDate);
    this.editForm.get('endDateUTC').setValue(endDate);
  }

  manageDelete() {
    this.isDeleting = true;
    let isTargetEqual = AppoinmentDateUtils.isEqual(this.data.startDate, this.targetData.startDate) && AppoinmentDateUtils.isEqual(this.data.endDate, this.targetData.endDate);
    if (isTargetEqual && !this.isEditingCurrentOnly) {
      this.delete(this.editForm.get('id').value);
    } else {
      this.onSave(false);
    }
  }

  onCancel() {
    this.cancel.emit();
    this.isUpserting = false;
  }

  openConfirmDialog() {
    this.isDialogOpen = true;
  }

  closeDialog() {
    this.isDialogOpen = false;
    this.isDisplayNoButton = true;
    this.isUpserting = false;
    this.isClickedThisNFutureOccurrence = false;
    this.isClickedOnlyThisOccurrence = false;
  }

  clickCancel() {
    this.isClickedThisNFutureOccurrence = true;
    // this.isClickedOnlyThisOccurrence = true;
    if (!this.isRecurringSchedule) {
      if (this.openedDialog == this.DELETE_DIALOG) {
        this.closeDialog();
        // this.manageDelete();
      } else if (this.openedDialog == this.EDIT_DIALOG) {

      }
    } else {
      this.editMode = EditMode.ThisAndFuture;
      if (this.openedDialog == this.DELETE_DIALOG) {
        this.manageDelete();
      } else if (this.openedDialog == this.EDIT_DIALOG) {
        this.onSave(false);
      }
    }
    this.closeDialog();
  }

  clickOk() {
    // this.isClickedThisNFutureOccurrence = true;
    this.isClickedOnlyThisOccurrence = true;
    if (!this.isRecurringSchedule) {
      if (this.openedDialog == this.DELETE_DIALOG) {
        this.manageDelete();
      } else if (this.openedDialog == this.CONFLICT_DIALOG) {
        this.closeDialog();
      } else if (this.openedDialog == this.EDIT_DIALOG) {

      }
    } else {
      this.editMode = EditMode.Occurrence;
      if (this.openedDialog == this.DELETE_DIALOG) {
        this.manageDelete();
      } else if (this.openedDialog == this.EDIT_DIALOG) {
        this.onSave(false);
      }
    }
  }

  clickConfirm() {
    let conflicts: RecurringDates[] = this.dateConflicts.filter(value => value.isAvailable === false);
    let string: any = '';
    if (conflicts) {
      let hour = AppoinmentDateUtils.formatDateTimeDefault(this.formData.startDateUTC, 'HH', false);
      let minute = AppoinmentDateUtils.formatDateTimeDefault(this.formData.startDateUTC, 'mm', false);
      conflicts.forEach(value => {
        string += ',' + AppoinmentDateUtils.toUtcRecurrenceException(AppoinmentDateUtils.combineDateTimeDefault(value.dateTimeUTC, {
          'hour': hour,
          'minute': minute,
        }));
      });
    }
    string = string.replaceAll('-', '').replaceAll(':', '').substr(1);
    this.formData.recurrenceException = this.formatREString(this.formData.recurrenceException, string);
    this.upsert(this.formData, null, true);
  }

  deleteSchedule() {
    this.openedDialog = this.DELETE_DIALOG;
    this.deleteScheduleDialog();
  }

  recurringScheduleButton() {
    if (this.isRecurringSchedule) {
      this.noButtonName = 'This and All Future Occurences';
      this.yesButtonName = 'Only this Occurence';
    }
  }

  deleteScheduleDialog() {
    if (this.isBreak) {
      if (this.isRecurringSchedule) {
        this.dialogTitle = 'delete ' + this.typeToString;
        this.dialogContent = 'Do you wish to delete only this occurence or this and all future occurences?';
      } else {
        this.dialogContent = `Deleted break cannot be restored.`;
        this.dialogTitle = 'Do you wish to delete this break?';
      }
    } else {
      this.dialogTitle = 'delete ' + this.typeToString;
      this.dialogContent = `Do you still wish to delete this ${this.typeToString.toLowerCase()}?`;
    }
    this.recurringScheduleButton();
    this.openConfirmDialog();
  }

  conflictScheduleDialog() {
    this.openedDialog = this.CONFLICT_DIALOG;
    this.isDisplayNoButton = false;
    this.dialogTitle = this.typeToString + ' Availability Conflicts';
    this.dialogContent = 'The selected time slot is unavailable.\n' +
      '\n' +
      `Please choose a different time slot to create the ${this.typeToString.toLowerCase()}.`;
    this.yesButtonName = 'Ok';
    this.openConfirmDialog();
  }

  editScheduleDialog() {
    this.openedDialog = this.EDIT_DIALOG;
    this.dialogTitle = 'Edit ' + this.typeToString;
    this.dialogContent = 'Do you wish to apply the changes only for this occurence or this and all future occurences?';
    this.recurringScheduleButton();
    this.openConfirmDialog();
  }

  openDateAvailabilityDialog() {
    this.isDADialogOpen = true;
  }

  closeDateAvailabilityDialog() {
    this.isDADialogOpen = false;
  }

  getMinHour(isApplyToForm: boolean) {
    if (!this.editForm.get('startDateUTC').value) {
      return;
    }
    let sd = new Date(AppUtils.refrenceClone(this.editForm.get('startDateUTC').value));
    let md = new Date(this.min);

    md.setHours(sd.getHours());
    md.setMinutes(sd.getMinutes() + this.interval);
    this.min = md;

    let isLessEnd = DateUtils.compareTime(this.editForm.get('endDateUTC').value, this.min) <= 0;
    let isGreterThanEnd = DateUtils.compareTime(this.min, this._endTime) <= 0;

    if (isApplyToForm && isLessEnd && isGreterThanEnd) {
      this.endError = null;
      this.editForm.get('endDateUTC').setValue(this.min);
    } else if (isGreterThanEnd) {
      this.endError = 'End time greater than start time';
    }
    // console.log(this.endError)
  }

  appIdBreakHandler($event) {
    this.editForm.get('breakType').setValue($event);
  }

  get isEditingSeries(): boolean {
    return this.editMode === EditMode.Series;
  }

  get isEditingThisAndFutureSeries(): boolean {
    return this.editMode === EditMode.ThisAndFuture;
  }

  get isEditingCurrentOnly(): boolean {
    return this.editMode === EditMode.Occurrence;
  }

  get isSchedule() {
    return this.editForm.get('type').value == this.TYPE_SCHEDULE;
  }

  get isBreak() {
    return this.editForm.get('type').value == this.TYPE_BREAK;
  }

  private getHour(date: Date) {
    return parseInt(moment(date).format('HH'));
  }

  private getMinute(date: Date) {
    return parseInt(moment(date).format('mm'));
  }

  private formatREString(cRe: string, re: string) {
    return cRe && cRe != '' ? cRe.concat(','.concat(re)) : re;
  }

  private setStartAndEndDate() {
    this.editForm.get('_startDate').setValue(new Date(this.data._startDate));
    this.editForm.get('_endDate').setValue(new Date(this.data._endDate));
    if (this.data) {
      this.data._startDate = new Date(this.data._startDate);
      this.data._endDate = new Date(this.data._endDate);
    }
  }

  private roundOffMinute(date) {
    let min = this.getMinute(date);
    let divideBy = min;

    if (min > 0 && min < 5) {
      divideBy = 5;
    } else if (min > 5 && min < 10) {
      divideBy = 10;
    } else if (min > 15 && min < 20) {
      divideBy = 20;
    } else if (min > 20 && min < 25) {
      divideBy = 25;
    } else if (min > 25 && min < 30) {
      divideBy = 30;
    } else if (min > 30 && min < 35) {
      divideBy = 35;
    } else if (min > 35 && min < 40) {
      divideBy = 40;
    } else if (min > 40 && min < 45) {
      divideBy = 45;
    } else if (min > 45 && min < 50) {
      divideBy = 50;
    } else if (min > 50 && min < 55) {
      divideBy = 55;
    } else if (min > 55 && min < 60) {
      divideBy = 60;
    }

    if (divideBy == min) {
      return min;
    }

    let decimalPoint = Math.round((min / 5) % 1);

    if (decimalPoint <= 0) {
      return 0;
    } else {
      return divideBy;
    }
  }

  private removeExtraKeys(data: Schedule) {
    // delete data.text;
    // delete data.type;
    delete data.mode;
    delete data._startDate;
    delete data._endDate;
    delete data.startDate;
    delete data.endDate;
    // delete data.uuid;
  }

  // BREAK METHODS
  private getCategoryName(id) {
    let list = this.breaks.filter(value => value.categoryName == id);
    return (list && list.length > 0) ? list[0].categoryName : '';
  }

  private getFrequencyEnum(frequency): Frequency {
    switch (frequency.toUpperCase()) {
      case 'DAILY':
        return Frequency.Day;
      case 'ROSD':
        return Frequency.SameAsSchedule;
      case 'WEEKLY':
        return Frequency.Weekly;
      case 'MONTHLY':
        return Frequency.Monthly;
      case 'YEARLY':
        return Frequency.Yearly;
      default:
        return Frequency.None;
    }
  }

  // POPUP CONFIGURATION
  get getTitle() {
    let title = this.isNew ? 'Add ' : 'Edit ';

    if (this.isBreak) {
      title += 'Break';
    } else if (this.isSchedule) {
      title += 'Schedule';
    }

    return title;
  }

  get typeToString(): string {
    if (this.isBreak) {
      return 'Break';
    }
    return 'Schedule';
  }

  get endBy() {
    return this.recurrenceRule.repeat.frequency != 'Never' && this.recurrenceRule.end ? this.recurrenceRule.end.mode : 'Never';
  }

  get recurrenceRule() {
    return this.editForm.get('recurrenceRule').value.raw;
  }

  get saveEventType() {
    return this.isDeleting ? Constant.DELETE : (this.isNew ? Constant.ADD : Constant.EDIT);
  }

  get selectedDataDate() {
    return this.targetData ? this.targetData._startDate : this.editForm.get('_startDate').value;
  }
}
