import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RouterState} from '../../../../../../shared/interface';

@Component({
  selector: 'app-empty-practitioner',
  templateUrl: './empty-practitioner.component.html',
  styleUrls: ['./empty-practitioner.component.scss']
})
export class EmptyPractitionerComponent implements OnInit {

  @Input()
  small: boolean = false;
  @Input()
  urlBtn: boolean = false;
  @Input()
  title: string = "";
  @Input()
  message: string = "";
  @Input()
  queryParams: any = {};
  @Input()
  url: string = "";
  @Input()
  urlTitle: string = "";
  @Input()
  state: RouterState;

  @Output()
  clickUrlBtn: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

}
