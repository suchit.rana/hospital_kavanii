import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AppDialogService, IPopupAction} from '../../../../../../../shared/services/app-dialog.service';

@Component({
  selector: 'app-recurrence',
  templateUrl: './recurrence.component.html',
  styleUrls: ['./recurrence.component.scss']
})
export class RecurrenceComponent implements OnInit {

  constructor() { }

  @Input()
  formGroup: FormGroup;

  @Input()
  recurrFrequency: string[] = [];

  ngOnInit(): void {
  }

  get recurrenceRule() {
    return this.formGroup.get('recurrenceRule').value.raw;
  }

  static open(dialogService: AppDialogService, data: {[x: string]: any}): IPopupAction {
    return dialogService.open(RecurrenceComponent, {
      title: 'SET RECURRENCE',
      width: '393px',
      saveBtnTitle: 'Apply',
      data: data
    });
  }
}
