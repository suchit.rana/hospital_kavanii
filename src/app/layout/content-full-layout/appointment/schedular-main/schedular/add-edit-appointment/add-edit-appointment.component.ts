import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {PractitionerSpecialityModel} from '../../../../../../models/app.staff.model';
import {AppointmentType, EditMode, Frequency} from '../../../../../../enum/application-data-enum';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {Appointment, RecurringDates, Schedule, TreatmentType} from '../../../../../../models/app.appointment.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {startEndTimeValidator} from '../../../../../../shared/validation';
import * as moment from 'moment';
import {DateUtils} from '../../../../../../shared/DateUtils';
import {SettingsService} from 'src/app/services/app.settings.service';
import {AppointmentService} from 'src/app/services/app.appointment.service';
import {AppState} from 'src/app/app.state';
import {ContactService} from 'src/app/services/app.contact.service';
import {ApplicationDataService} from 'src/app/services/app.applicationdata.service';
import {NavigationExtras, Router} from '@angular/router';
import {PatientModel} from '../../../../../../models/app.patient.model';
import {AppoinmentDateUtils} from '../../../AppoinmentDateUtils';
import {SchedulerManageService} from '../../../scheduler-manage.service';
import {BehaviorSubject} from 'rxjs';
import {Constant} from 'src/app/shared/Constant';
import {PatientService} from 'src/app/services/app.patient.service';
import {formatREString} from '../../../../../../app.component';
import {isString} from 'util';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';
import {RecurrenceComponent} from './recurrence/recurrence.component';
import {AppDialogService} from '../../../../../../shared/services/app-dialog.service';

@Component({
    selector: 'app-add-edit-appointment',
    templateUrl: './add-edit-appointment.component.html',
    styleUrls: ['./add-edit-appointment.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddEditAppointmentComponent implements OnInit, OnDestroy {
    readonly APPT_BASIC = RP_MODULE_MAP.appointment_class_break_appointment;

    readonly EDIT_DIALOG = 'EDIT_DIALOG';
    readonly DELETE_DIALOG = 'DELETE_DIALOG';
    readonly CONFLICT_DIALOG = 'CONFLICT_DIALOG';

    @Input()
    startTime = 8;
    @Input()
    endTime = 17;
    @Input()
    practitioners: PractitionerSpecialityModel[] = [];
    @Input()
    isNew = false;
    @Input()
    interval = 15;
    @Input()
    editMode: EditMode = null;
    contactList: any[];
    playerList: any[];
    SelectedCaseData: any;
    isManualSelection = false;
    isLoaded = false;

    @Input()
    set schedule(ev: any) {
        if (ev) {
            this.isManualSelection = ev['isManualSelection'];
            this.isNew = !ev.id || ev.id == '';
            this.isRecurringSchedule = ev.recurrenceRule && ev.recurrenceRule != '';

            if (ev.frequencyType == Frequency.SameAsSchedule) {
                ev.recurrenceRule = ev.recurrenceRule.replace('DAILY', 'ROSD');
            }
            this.editForm.patchValue(AppUtils.refrenceClone(ev));
            this.data = AppUtils.refrenceClone(ev);
            if (this.data.treatmentDetails) {
                this.treatmentDetailsFG.patchValue(this.data.treatmentDetails);
            }
            if (this.data && this.data.treatmentDetails && this.data.treatmentDetails.treatmentServiceProduct && this.data.treatmentDetails.treatmentServiceProduct.length > 0) {
                this.serviceNProductFormArray.clear();
                this.data.treatmentDetails.treatmentServiceProduct.forEach((value, index) => {
                    this.addSPFG();
                    const fg = this.serviceNProductFormArray.at(index);
                    if (fg) {
                        fg.patchValue(value);
                    }
                    this.selectedserviceProductsList.push(value);
                });
            }
            this.setStartAndEndDate();
        }
    }

    @Input()
    targetData: Schedule = null;
    @Input()
    selectedPatient: PatientModel = null;
    @Output()
    cancel: EventEmitter<any> = new EventEmitter();
    @Output()
    save: EventEmitter<any> = new EventEmitter();
    @Output()
    onAddClick: EventEmitter<any> = new EventEmitter();

    isRecurringSchedule: boolean = false;
    data: Appointment = null;
    _startTime: any = new Date();
    _endTime: any = new Date();
    _endTimeForEndTime: any = new Date();
    apperance: string = 'outline';
    isUpserting: boolean = false;

    /** Popup Variables */
    isOpenRecurrencePopup: boolean = false;
    isOpenAESPPopup: boolean = false;
    isDialogOpen: boolean = false;
    isOpenPatientDetailsPopup: boolean = false;

    dialogTitle: string = 'Title';
    yesButtonName: string = 'Yes';
    noButtonName: string = 'No';

    isDisplayNoButton: boolean = true;
    isClickedOnlyThisOccurrence: boolean = false;
    isClickedThisNFutureOccurrence: boolean = false;

    dialogContent: string = '';
    private openedDialog = null;

    /** Time Variables */
    min;
    endError: string = null;

    /** Expand/Collapse Button */
    isApptDetailsExpanded = true;
    istreatmentDetailsExpanded = true;
    isCCDetailsExpanded = true;
    serviceProductsOriginal: any = [];
    serviceProducts: any = {};

    /** Recurrence Config */
    recurrFrequency = ['No Recurrence', 'ROSD', 'Daily', 'Monthly', 'Weekly', 'Yearly'];

    serviceNProductFormArray = new FormArray([]);
    treatmentDetailsFG = new FormGroup({
        treatmentServiceProduct: new FormArray([]),
        id: new FormControl(''),
        practitionerId: new FormControl(''),
        appointmentsId: new FormControl(''),
        concessionId: new FormControl(''),
    });
    editForm: FormGroup = new FormGroup({
        id: new FormControl(''),
        locationId: new FormControl(''),
        practitionerId: new FormControl('', Validators.required),
        appointmentType: new FormControl(),
        treatmentRoomId: new FormControl(),
        patientId: new FormControl('', Validators.required),
        referralId: new FormControl('00000000-0000-0000-0000-000000000000', Validators.required),
        payerId: new FormControl('00000000-0000-0000-0000-000000000000', Validators.required),
        caseId: new FormControl('', Validators.required),
        telehealth: new FormControl(false),
        title: new FormControl(''),
        description: new FormControl(),
        startDateUTC: new FormControl('', Validators.required),
        endDateUTC: new FormControl('', Validators.required),
        treatmentDetails: this.treatmentDetailsFG,
        isAllDay: new FormControl(false),
        recurrenceRule: new FormControl(),
        recurrenceException: new FormControl(),
        bookOnline: new FormControl(false),
        note: new FormControl(),
        frequencyType: new FormControl(Frequency.None),
        breakTypeId: new FormControl(''),
        _startDate: new FormControl('', Validators.required),
        _endDate: new FormControl('', Validators.required),
        type: new FormControl('', Validators.required),
    }, startEndTimeValidator('startDateUTC', 'endDateUTC'));

    AppointmentTypeList: any = [];
    TreatmentRoomsList: any = [];
    ConcessionsList: any = [];
    CasesList: any = [];
    AppointmentTypeAdded: any;
    stateValues: any;
    practitionerName: any;
    isDeleting: boolean = false;
    formData;
    isDADialogOpen: boolean = false;
    DASubject: BehaviorSubject<RecurringDates[]> = new BehaviorSubject<RecurringDates[]>(null);
    dateConflicts: RecurringDates[] = [];
    serviceProductsList: any = [];
    selectedserviceProductsList: any[] = [];
    selectedPS = {};

    /** Patient Full Details */
    selectedPatientDetails: PatientModel;

    @ViewChild('apptDetailsElem', {static: false}) apptDetailsElem: ElementRef;

    constructor(
        private settingService: SettingsService,
        private appointmentService: AppointmentService,
        public appState: AppState,
        private contactService: ContactService,
        private applicationDataService: ApplicationDataService,
        private _router: Router,
        public manageScheduleService: SchedulerManageService,
        public patientService: PatientService,
        public alertService: AppAlertService,
        public appDialogService: AppDialogService,
    ) {
        if (this._router.getCurrentNavigation() != null) {
            this.AppointmentTypeAdded = this._router.getCurrentNavigation().extras.state.AppointmentTypeAdded;
            this.stateValues = this.AppointmentTypeAdded ? JSON.parse(this._router.getCurrentNavigation().extras.state.stateValues) : null;
        } else {
            this.AppointmentTypeAdded = history.state.AppointmentTypeAdded;
            this.stateValues = this.AppointmentTypeAdded ? JSON.parse(history.state.stateValues) : null;
        }
        this.getAllAppointmentTypes();
        this.getAllTreatmentRooms();
        this.getAllConcessions();
        this.getAllContact();

        this.DASubject.subscribe(value => {
            if (value) {
                this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
                if (this.isRecurringSchedule || (this.formData.recurrenceRule && this.formData.recurrenceRule != '')) {
                    this.dateConflicts = value;
                    this.openDateAvailabilityDialog();
                    this.isUpserting = false;
                    this.isClickedThisNFutureOccurrence = false;
                    this.isClickedOnlyThisOccurrence = false;
                    return;
                }
                this.conflictScheduleDialog();
            }
        });
    }

    ngOnInit() {
        if (this.isNew || (this.data && this.data.treatmentDetails.treatmentServiceProduct && this.data.treatmentDetails.treatmentServiceProduct.length == 0)) {
            this.serviceNProductFormArray.push(AddEditAppointmentComponent.spFormGroup());
        }
        this.endError = null;
        if (this.startTime || this.startTime == 0) {
            this._startTime = moment().set({'hour': this.startTime, 'minute': 0});
        }

        if (this.endTime || this.endTime == 0) {
            this._endTime = moment().set({'hour': this.endTime, 'minute': 0});
            this._endTimeForEndTime = new Date(moment().set({'hour': this.endTime, 'minute': this.interval}).format());
        }

        this.min = new Date(this._startTime);
        this.getMinHour(false);
        this.editForm.get('startDateUTC').valueChanges.subscribe(value => {
            this.getMinHour(true);
        });

    }

    ngAfterViewInit(): void {
        this.editForm.get('patientId').setValue(this.patientId);
        this.getPatientDetails();
        if (this.AppointmentTypeAdded && this.stateValues) {
            this.data = this.stateValues.addEditScheduleData;
            this.isNew = true;
            this.practitionerName = {
                displayName: this.stateValues.displayName
            };
            if (this.targetData) {
                this.targetData['_startDate'] = this.stateValues.addEditScheduleData._startDate;
            }
            this.practitioners = this.stateValues.practitioners;
            this.selectedPatient = new PatientModel();
            this.selectedPatient.id = this.stateValues.addEditScheduleData.patientId;
            this.editForm.get('startDateUTC').setValue(new Date(this.stateValues.addEditScheduleData._startDate));
            this.editForm.get('endDateUTC').setValue(new Date(this.stateValues.addEditScheduleData._endDate));
            this.editForm.get('_startDate').setValue(new Date(this.data._startDate));
            this.editForm.get('_endDate').setValue(new Date(this.data._endDate));
            this.editForm.get('practitionerId').setValue(this.stateValues.addEditScheduleData.practitionerId);
            this.editForm.get('patientId').setValue(this.stateValues.addEditScheduleData.patientId);
            this.editForm.get('payerId').setValue(this.stateValues.addEditScheduleData.payerId);
            this.editForm.get('referralId').setValue(this.stateValues.addEditScheduleData.referralId);
        }
        this.isLoaded = true;
        this.getCases();
    }

    ngOnDestroy() {
        this.selectedPatient = null;
    }

    AddNewClick(data) {
        let obj = {
            addEditScheduleData: this.editForm.value,
            targetAddEditScheduleData: this.targetData,
            interval: this.interval,
            startTime: this.startTime,
            endTime: this.endTime,
            selectedPatient: this.selectedPatient,
            practitioners: this.practitioners
        };
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                stateValues: JSON.stringify(obj)
            }
        };
        if (data == 'appointmentType') {
            this._router.navigate(['tools/appointmentType/add'], fromState);
        } else if (data == 'treatmentType') {
            this._router.navigate(['tools/treatmentRoom/add'], fromState);
        } else if (data == 'concessionType') {
            this._router.navigate(['tools/concession/add'], fromState);
        } else if (data == 'contactType') {
            let fromState: NavigationExtras = {
                state: {
                    fromState: JSON.stringify({}),
                    fromStateReferral: true,
                    stateValues: JSON.stringify(obj),
                    stateCaseValues: JSON.stringify(this.SelectedCaseData)
                }
            };
            this._router.navigate(['patients/edit/' + this.editForm.get('patientId').value], fromState);
        } else if (data == 'playerType') {
            let fromState: NavigationExtras = {
                state: {
                    fromState: JSON.stringify({}),
                    fromStateThirdParty: true,
                    stateValues: JSON.stringify(obj),
                    stateCaseValues: JSON.stringify(this.SelectedCaseData)
                }
            };
            this._router.navigate(['patients/edit/' + this.editForm.get('patientId').value], fromState);
        } else if (data == 'categoryName') {
            this._router.navigate(['patients/edit/' + this.editForm.get('patientId').value], fromState);
        }
    }

    getAllAppointmentTypes() {
        this.settingService.getAllAppointmentTypes().subscribe((data) => {
            this.AppointmentTypeList = data;
        });
    }

    getProductAndServiceSearch() {
        this.appointmentService.getProductAndServiceSearch(this.appState.selectedUserLocationId, this.patientId).subscribe((data) => {
            this.serviceProductsOriginal = data;
            this.prepareServiceNProducts();
        });
    }

    getAllTreatmentRooms() {
        this.settingService.getAllTreatmentRooms().subscribe((data) => {
            this.TreatmentRoomsList = [];
            data.forEach(d => {
                if (d.isStatus) {
                    const dd = {
                        id: d.id,
                        treatmentType: d.treatmentType
                    };
                    this.TreatmentRoomsList.push(dd);
                }
            });
        });
    }

    getAllConcessions() {
        this.settingService.getAllConcessions().subscribe((data) => {
            this.ConcessionsList = [];
            data.forEach(d => {
                const dd = {
                    id: d.id,
                    concessionType: d.concessionType
                };
                this.ConcessionsList.push(dd);
            });
        });
    }

    getAllContact() {
        this.contactService.getAllContact().subscribe((data) => {
            this.contactList = [];
            this.playerList = [];

            const self = {
                id: '00000000-0000-0000-0000-000000000000',
                playerType: 'Self'
            };
            const noReferral = {
                id: '00000000-0000-0000-0000-000000000000',
                contactType: 'No Referral'
            };
            this.playerList.push(self);
            this.contactList.push(noReferral);
            data.forEach(d => {
                if (d.contactType == 1) {
                    var dd = {
                        id: d.id,
                        contactType: `${d.firstName} ${d.lastName}`
                    };
                    this.contactList.push(dd);
                } else if (d.contactType == 2) {
                    var d2 = {

                        id: d.id,
                        playerType: d.organisationName
                    };

                    this.playerList.push(d2);
                }
            });

        });
    }

    getCases() {
        if (!this.patientId) {
            return;
        }
        this.patientService.getPatientCases(this.appState.selectedUserLocationId, this.editForm.get('patientId').value).subscribe(data => {
            this.CasesList = [];
            let isdefaultId = 0;
            data.forEach(d => {
                if (d.caseStatus == 0 || d.caseStatus == 1) {
                    if (d.isdefault && this.isNew) {
                        isdefaultId = d.id;
                        this.editForm.get('caseId').setValue(isdefaultId);
                    }
                    const dd = {
                        id: d.id,
                        categoryName: d.caseName
                    };
                    this.CasesList.push(dd);
                }
            });
            // if (isdefaultId != 0) {
            //   this.getSelectedCaseRecord(isdefaultId);
            // }
            // if (!this.isNew) {
            this.getSelectedCaseRecord(this.editForm.get('caseId').value);
            // }
        });
    }

    getSelectedCaseRecord(caseId) {
        this.patientService.getCaseDetailbyCaseId(caseId, this.editForm.get('patientId').value).subscribe(data => {
            this.SelectedCaseData = data;
            data.caseContacts.forEach(d => {
                if (d.type == 1) {
                    if (d.isDefault) {
                        this.editForm.get('referralId').setValue(d.id);
                    }
                    let cdata = {
                        id: d.id,
                        contactType: d.name
                    };
                    this.contactList.push(cdata);
                } else {
                    if (d.isDefault) {
                        this.editForm.get('payerId').setValue(d.id);
                    }
                    let cdata = {
                        id: d.id,
                        playerType: d.name
                    };
                    this.playerList.push(cdata);
                }
            });
            if (!this.editForm.get('payerId').value) {
                this.editForm.get('payerId').setValue('00000000-0000-0000-0000-000000000000');
            }
        });
    }

    onSelectServiceProduct($event, index: number) {
        const list = this.getServiceProducts(index);
        const listObj = list.find(value => value.id == $event);
        const s = this.serviceProductsList.find(x => x.productId == $event || x.serviceId == $event || x.id == $event);

        const plannedService = this.serviceProductsOriginal.patientPlannedService;
        if (plannedService && plannedService.length > 0) {
            const obj = plannedService.find(value => value.id == $event);
            if (obj) {
                obj.isUsed = true;
                obj['usedIn'] = index;
            }
        }

        const available = this.selectedserviceProductsList.find(x => x.productId == $event || x.serviceId == $event || x.id == $event);
        if (available && listObj) {
            available['units'] = listObj['actualQty'];
            available['duration'] = listObj['actualDuration'];
            available['price'] = listObj['price'];
            const fg = this.serviceNProductFormArray.at(index);
            if (fg) {
                fg.patchValue(available);
            }
        } else if (s) {
            s['units'] = listObj['actualQty'];
            s['duration'] = listObj['actualDuration'];
            s['price'] = listObj['price'];
            const fg = this.serviceNProductFormArray.at(index);
            if (fg) {
                fg.patchValue(s);
            }
            this.selectedserviceProductsList.push(s);
        }
        if ($event == null) {
            this.selectedserviceProductsList.splice(index);
        }
        let totalServiceMin = 0;
        this.selectedserviceProductsList.forEach(value => {
            if (value['treatementType'] == TreatmentType.Service) {
                totalServiceMin += value['duration'];
            }
        });
        this.prepareServiceNProducts();
        const startDateTime = this.editForm.get('startDateUTC').value;
        let endTime;
        if (totalServiceMin == 0) {
            endTime = AppoinmentDateUtils.addAndGetNewValue(startDateTime, this.interval, 'minutes');
        } else {
            const startDateTime = this.editForm.get('startDateUTC').value;
            endTime = AppoinmentDateUtils.addAndGetNewValue(startDateTime, totalServiceMin, 'minutes');
        }
        this.editForm.get('endDateUTC').setValue(endTime);
    }

    openConfirmDialog() {
        this.isDialogOpen = true;
    }

    closeDialog() {
        this.isDialogOpen = false;
        this.isDisplayNoButton = true;
        this.isUpserting = false;
        this.isClickedThisNFutureOccurrence = false;
        this.isClickedOnlyThisOccurrence = false;
    }

    clickCancel() {
        this.isClickedThisNFutureOccurrence = true;
        if (!this.isRecurringSchedule) {
            if (this.openedDialog == this.DELETE_DIALOG) {
                this.closeDialog();
            } else if (this.openedDialog == this.EDIT_DIALOG) {

            }
        } else {
            this.editMode = EditMode.ThisAndFuture;
            if (this.openedDialog == this.DELETE_DIALOG) {
                this.manageDelete();
            } else if (this.openedDialog == this.EDIT_DIALOG) {
                this.onSave(false);
            }
        }
        this.closeDialog();
    }

    clickOk() {
        this.isClickedOnlyThisOccurrence = true;
        if (!this.isRecurringSchedule) {
            if (this.openedDialog == this.DELETE_DIALOG) {
                this.manageDelete();
            } else if (this.openedDialog == this.CONFLICT_DIALOG) {
                this.closeDialog();
            } else if (this.openedDialog == this.EDIT_DIALOG) {

            }
        } else {
            this.editMode = EditMode.Occurrence;
            if (this.openedDialog == this.DELETE_DIALOG) {
                this.manageDelete();
            } else if (this.openedDialog == this.EDIT_DIALOG) {
                this.onSave(false);
            }
        }
    }

    clickConfirm() {
        let conflicts: RecurringDates[] = this.dateConflicts.filter(value => value.isAvailable === false);
        let string: any = '';
        if (conflicts) {
            let hour = AppoinmentDateUtils.formatDateTimeDefault(this.formData.startDateUTC, 'HH', false);
            let minute = AppoinmentDateUtils.formatDateTimeDefault(this.formData.startDateUTC, 'mm', false);
            conflicts.forEach(value => {
                string += ',' + AppoinmentDateUtils.toUtcRecurrenceException(AppoinmentDateUtils.combineDateTimeDefault(value.dateTimeUTC, {
                    'hour': hour,
                    'minute': minute,
                }));
            });
        }
        string = string.replaceAll('-', '').replaceAll(':', '').substr(1);
        this.formData.recurrenceException = formatREString(this.formData.recurrenceException, string);
        this.upsert(this.formData, null, true);
    }

    recurringScheduleButton() {
        if (this.isRecurringSchedule) {
            this.noButtonName = 'This and All Future Occurences';
            this.yesButtonName = 'Only this Occurence';
        }
    }

    deleteScheduleDialog() {
        if (this.isRecurringSchedule) {
            this.dialogTitle = 'delete appointment';
            this.dialogContent = 'Do you wish to delete only this occurence or this and all future occurences?';
        } else {
            this.dialogContent = `Deleted appointment cannot be restored.`;
            this.dialogTitle = 'Do you wish to delete this appointment?';
        }
        this.recurringScheduleButton();
        this.openConfirmDialog();
    }

    conflictScheduleDialog() {
        this.openedDialog = this.CONFLICT_DIALOG;
        this.isDisplayNoButton = false;
        this.dialogTitle = 'Appointment Availability Conflicts';
        this.dialogContent = 'The selected time slot is unavailable.\n' +
            '\n' +
            `Please choose a different time slot to create the appointment.`;
        this.yesButtonName = 'Ok';
        this.openConfirmDialog();
    }

    editScheduleDialog() {
        this.openedDialog = this.EDIT_DIALOG;
        this.dialogTitle = 'Edit Appointment';
        this.dialogContent = 'Do you wish to apply the changes only for this occurence or this and all future occurences?';
        this.recurringScheduleButton();
        this.openConfirmDialog();
    }

    /** Check End TIme & Validate it should be greater than start time */
    getMinHour(isApplyToForm: boolean) {
        if (!this.editForm.get('startDateUTC').value) {
            return;
        }
        let sd = new Date(AppUtils.refrenceClone(this.editForm.get('startDateUTC').value));
        let md = new Date(this.min);

        md.setHours(sd.getHours());
        md.setMinutes(sd.getMinutes() + this.interval);
        this.min = md;

        let isLessEnd = DateUtils.compareTime(this.editForm.get('endDateUTC').value, this.min) <= 0;
        let isGreterThanEnd = DateUtils.compareTime(this.min, this._endTime) <= 0;

        if (isApplyToForm && isLessEnd && isGreterThanEnd) {
            this.endError = null;
            this.editForm.get('endDateUTC').setValue(this.min);
        } else if (isGreterThanEnd) {
            this.endError = 'End time greater than start time';
        }
    }

    get typeToString(): string {
        return 'Appointment';
    }

    /** Add Form Group To Service & Product Array */
    addSPFG() {
        this.serviceNProductFormArray.push(AddEditAppointmentComponent.spFormGroup());
        this.serviceProducts[Object.keys(this.serviceProducts).length] = [];
        this.prepareServiceNProducts();
    }

    /** Toggle Recurrence Popup */
    toggleRecurrencePopup(value: boolean) {
        if (value) {
            const rruleObj = this.editForm.get('recurrenceRule').value;
            if (rruleObj && !isString(rruleObj)) {
                this.editForm.get('recurrenceRule').setValue(rruleObj.rRule.toString().replace('RRULE:', ''));
            }
            RecurrenceComponent.open(this.appDialogService, {
                formGroup: this.editForm,
                recurrFrequency: this.recurrFrequency
            }).clickSave.subscribe(this.applyRecurrence);
        }
        // this.isOpenRecurrencePopup = value;
    }

    /** Toggle Add/Edit Product/service Popup */
    toggleAESPPopup(value: boolean) {
        if (!value) {
            this.selectedPS = {};
        }
        this.isOpenAESPPopup = value;
    }

    /** Toggle Patient Details Popup */
    togglePatientDetailsPopup(value: boolean) {
        this.isOpenPatientDetailsPopup = value;
    }

    /** Delete Appointment */
    deleteAppointment() {
        this.openedDialog = this.DELETE_DIALOG;
        this.deleteScheduleDialog();
    }

    private getHour(date: Date) {
        return parseInt(moment(date).format('HH'));
    }

    private getMinute(date: Date) {
        return parseInt(moment(date).format('mm'));
    }

    /** Save Appointment */
    onSave(isForUpdate: boolean = true) {
        if (this.editForm.invalid) {
            for (let controlsKey in this.editForm.controls) {
                this.editForm.get(controlsKey).markAsTouched();
                this.editForm.get(controlsKey).updateValueAndValidity();
            }
            return;
        }

        if (this.recurrenceRule && this.recurrenceRule.repeat.frequency == 'ROSD') {
            let noOfDays = this.recurrenceRule.end.after;
            if (this.endBy === 'On date') {
                noOfDays = AppoinmentDateUtils.getDiff(this.editForm.get('_startDate').value, this.recurrenceRule.end.endAt);
            }
            if (noOfDays > 1) {
                this.appointmentService.getPractitionerWithNoScheduleDates(AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(AppoinmentDateUtils.getDateWithTz(this.editForm.get('_startDate').value), {
                    'hour': this.getHour(this.editForm.get('startDateUTC').value),
                    'minute': this.getMinute(this.editForm.get('startDateUTC').value)
                })), this.appState.selectedUserLocationId, this.editForm.get('practitionerId').value, noOfDays).subscribe(response => {
                    if (response && response.length > 0) {
                        let unAvailableDates = response[0]['notSchedule'];
                        if (unAvailableDates && unAvailableDates.length > 0) {
                            let exception = unAvailableDates.map((value) => AppoinmentDateUtils.toUtcRecurrenceException(AppoinmentDateUtils.combineDateTime(AppoinmentDateUtils.getDateWithTz(value), {
                                'hour': this.getHour(value),
                                'minute': this.getMinute(value),
                                'second': 1
                            })));
                            // console.log(exception)
                            // let exception = unAvailableDates.map((value) => value.replace(/[-\:]/g, ''));
                            this.editForm.get('recurrenceException').setValue(exception.toString());
                            this.processData(isForUpdate);
                        } else {
                            this.processData(isForUpdate);
                        }
                    }
                });
            } else {
                this.processData(isForUpdate);
            }
        } else {
            this.processData(isForUpdate);
        }
    }

    applyRecurrence() {
        this.toggleRecurrencePopup(false);
    }

    applyQtyDurationChange() {
        if (this.psType == 0) {
            this.selectedPS['name'] = this.selectedPS['actualName'] + '($' + this.psPrice + ', ' + this.selectedPS['actualDuration'] + ' min)';
        } else {
            this.selectedPS['name'] = this.selectedPS['actualName'] + ' ($' + this.psPrice + ')';
        }
        this.toggleAESPPopup(false);
    }

    clearOrRemoveSP(index?: number, id?: string) {
        if (id) {
            const plannedService = this.serviceProductsOriginal.patientPlannedService;
            if (plannedService && plannedService.length > 0) {
                const obj = plannedService.find(value => value.id == id);
                if (obj) {
                    obj.isUsed = false;
                    obj['usedIn'] = null;
                }
            }
        }
        this.serviceNProductFormArray.removeAt(this.psIndex || index);
        this.rearrangeSPIndex(index);
        if (this.serviceNProductFormArray.length <= 0) {
            this.addSPFG();
        }
        this.prepareServiceNProducts();
        this.toggleAESPPopup(false);
    }

    editProductService($event, index: number) {
        const list = this.getServiceProducts(index);
        this.selectedPS = list.find(value => value.id == $event);
        this.selectedPS['index'] = index;
        this.toggleAESPPopup(true);
    }

    changeQtyMin(value: number) {
        if (this.psType == 0) {
            this.selectedPS['actualDuration'] = value;
        } else {
            this.selectedPS['actualQty'] = value;
        }
    }

    private rearrangeSPIndex(index: number) {
        delete this.serviceProducts[index];
        let i = 0;
        Object.keys(this.serviceProducts).forEach(value => {
            if (value == (index + 1).toString()) {
                this.serviceProducts[index + i] = this.serviceProducts[value];
                i++;
            }
        });
    }

    /** Product & Service Edit */
    get psIndex() {
        return this.selectedPS['index'];
    }

    get psName() {
        return this.selectedPS['name'];
    }

    get psType() {
        return this.selectedPS['type'];
    }

    get psADuration() {
        return this.selectedPS['actualDuration'];
    }

    get psDuration() {
        return this.selectedPS['duration'];
    }

    get psAQuantity() {
        return this.selectedPS['actualQty'];
    }

    get psPrice() {
        return this.psType == 0 ? (this.psADuration) * this.selectedPS['price'] : this.psAQuantity * this.selectedPS['price'];
    }

    get psServiceId() {
        return this.selectedPS['serviceId'];
    }

    get psProductId() {
        return this.selectedPS['productId'];
    }

    get psTax() {
        return this.selectedPS['tax'];
    }

    get psDQFieldVal() {
        return this.psType == 0 ? this.psADuration : this.psAQuantity;
    }

    /** END - Product & Service Edit */

    get isEditingCurrentOnly(): boolean {
        return this.editMode === EditMode.Occurrence;
    }

    get isEditingThisAndFutureSeries(): boolean {
        return this.editMode === EditMode.ThisAndFuture;
    }

    get saveEventType() {
        return this.isDeleting ? Constant.DELETE : (this.isNew ? Constant.ADD : Constant.EDIT);
    }

    // private formatREString(cRe: string, re: string) {
    //   return cRe && cRe != '' ? cRe.concat(','.concat(re)) : re;
    // }

    formatRRule(rrule: string) {
        return (rrule.includes('UNTIL') && rrule.lastIndexOf('Z') < 0) ? rrule + 'Z' : rrule;
    }

    private getFrequencyEnum(frequency): Frequency {
        switch (frequency.toUpperCase()) {
            case 'DAILY':
                return Frequency.Day;
            case 'ROSD':
                return Frequency.SameAsSchedule;
            case 'WEEKLY':
                return Frequency.Weekly;
            case 'MONTHLY':
                return Frequency.Monthly;
            case 'YEARLY':
                return Frequency.Yearly;
            default:
                return Frequency.None;
        }
    }

    private roundOffMinute(date) {
        let min = this.getMinute(date);
        let divideBy = min;

        if (min > 0 && min < 5) {
            divideBy = 5;
        } else if (min > 5 && min < 10) {
            divideBy = 10;
        } else if (min > 15 && min < 20) {
            divideBy = 20;
        } else if (min > 20 && min < 25) {
            divideBy = 25;
        } else if (min > 25 && min < 30) {
            divideBy = 30;
        } else if (min > 30 && min < 35) {
            divideBy = 35;
        } else if (min > 35 && min < 40) {
            divideBy = 40;
        } else if (min > 40 && min < 45) {
            divideBy = 45;
        } else if (min > 45 && min < 50) {
            divideBy = 50;
        } else if (min > 50 && min < 55) {
            divideBy = 55;
        } else if (min > 55 && min < 60) {
            divideBy = 60;
        }

        if (divideBy == min) {
            return min;
        }

        let decimalPoint = Math.round((min / 5) % 1);

        if (decimalPoint <= 0) {
            return 0;
        } else {
            return divideBy;
        }
    }

    initFormFromTargetData() {
        let startDate = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTimeDefault(this.targetData.startDate, {
            'hour': this.getHour(this.editForm.get('startDateUTC').value),
            'minute': this.getMinute(this.editForm.get('startDateUTC').value),
            'second': 1
        }));

        let endDate = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTimeDefault(this.targetData.endDate, {
            'hour': this.getHour(this.editForm.get('endDateUTC').value),
            'minute': this.getMinute(this.editForm.get('endDateUTC').value),
        }));

        // this.editForm.get('relateTo').setValue(this.targetData.id);
        if (this.isEditingCurrentOnly) {
            if (this.targetData.recurrenceRule == this.editForm.get('recurrenceRule').value.rRule.toString().replace('RRULE:', '')) {
                this.editForm.get('recurrenceRule').setValue('');
            }
            this.editForm.get('recurrenceException').setValue('');
        }
        this.editForm.get('id').setValue(null);
        this.editForm.get('_startDate').setValue(startDate);
        this.editForm.get('_endDate').setValue(endDate);
        this.editForm.get('startDateUTC').setValue(startDate);
        this.editForm.get('endDateUTC').setValue(endDate);
    }

    private removeExtraKeys(data: Schedule) {
        // delete data.text;
        // delete data.type;
        delete data.mode;
        delete data._startDate;
        delete data._endDate;
        delete data.startDate;
        delete data.endDate;
        // delete data.uuid;
    }

    prepareEmitData(data, type, isError: boolean, isHidePopup: boolean, secondaryType: string) {
        let emitData = {};
        emitData['data'] = data;
        emitData['type'] = type;
        emitData['isError'] = isError;
        emitData['isHidePopup'] = isHidePopup;
        emitData['secondaryType'] = secondaryType;
        emitData['isNotify'] = true;
        return emitData;
    }

    notify(data, type: string, isError?: boolean, isHidePopup?: boolean, secondaryType?: string) {
        if (!secondaryType || secondaryType == '') {
            secondaryType = type;
        }
        let _data = null;
        if (data) {
            _data = new Appointment(data);
            let nData = this.prepareEmitData(AppUtils.refrenceClone(_data), type, isError, isHidePopup, secondaryType);
            this.manageScheduleService.notify(nData);
        }
    }

    upsert(data, onCompleted?: BehaviorSubject<boolean>, isHidePopup: boolean = true) {
        if (this.isNew) {
            delete data.id;
        }
        this.removeExtraKeys(data);
        this.formData = data;

        if (this.isNew) {
            if (AppoinmentDateUtils.isLessStrict(data._startDate, new Date())) {
                this.alertService.displayErrorMessage(`You can\'t create Appointment in past.`);
                return;
            }
            this.add(data, onCompleted, isHidePopup);
        } else {
            this.update(data, onCompleted, isHidePopup);
        }
    }

    manageDelete() {
        this.isDeleting = true;
        let isTargetEqual = AppoinmentDateUtils.isEqual(this.data.startDate, this.targetData.startDate) && AppoinmentDateUtils.isEqual(this.data.endDate, this.targetData.endDate);
        if (isTargetEqual && !this.isEditingCurrentOnly) {
            this.delete(this.editForm.get('id').value);
        } else {
            this.onSave(false);
        }
    }

    closeDateAvailabilityDialog() {
        this.isDADialogOpen = false;
    }

    openDateAvailabilityDialog() {
        this.isDADialogOpen = true;
    }

    private add(data, onCompleted, isHidePopup: boolean) {
        let secondryType = this.isEditingThisAndFutureSeries || this.isEditingCurrentOnly ? Constant.EDIT : this.saveEventType;
        this.appointmentService.createAppointment(data).subscribe(response => {
            if (onCompleted) {
                onCompleted.next(true);
            }
            if (response.response.toLocaleLowerCase() === 'success') {
                this.closeDateAvailabilityDialog();
                this.notify(response.appointmentsModel, Constant.ADD, false, isHidePopup, secondryType);
            } else if (response.response.toLocaleLowerCase() === 'error') {
                this.DASubject.next(response.recurringDateResults);
            }
            this.isUpserting = false;
            this.isDeleting = false;
            this.isClickedThisNFutureOccurrence = false;
            this.isClickedOnlyThisOccurrence = false;
        }, error => {
            this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
            this.notify(null, Constant.ADD, true, false, secondryType);
            this.isUpserting = false;
            this.isDeleting = false;
            this.isClickedThisNFutureOccurrence = false;
            this.isClickedOnlyThisOccurrence = false;
        });
    }

    private update(data, onCompleted, isHidePopup: boolean) {
        this.appointmentService.updateAppointment(data).subscribe(response => {
            if (response.response.toLocaleLowerCase() === 'success') {
                this.closeDateAvailabilityDialog();
                this.notify(response.appointmentsModel, Constant.EDIT, false, isHidePopup, this.saveEventType);
                if (onCompleted) {
                    onCompleted.next(true);
                }
            } else if (response.response.toLocaleLowerCase() === 'error') {
                this.DASubject.next(response.recurringDateResults);
            }
            this.isUpserting = false;
            this.isDeleting = false;
            this.isClickedThisNFutureOccurrence = false;
            this.isClickedOnlyThisOccurrence = false;
        }, error => {
            this.editForm.get('recurrenceRule').setValue(this.formData.recurrenceRule);
            this.notify(null, Constant.EDIT, true, false, this.saveEventType);
            this.isUpserting = false;
            this.isDeleting = false;
            this.isClickedThisNFutureOccurrence = false;
            this.isClickedOnlyThisOccurrence = false;
        });
    }

    private delete(id: string) {
        this.appointmentService.deleteAppointment(id, this.appState.selectedUserLocationId).subscribe(response => {
            this.save.emit(this.prepareEmitData({id: id, type: AppointmentType.APPOINMENT}, Constant.DELETE, false, true, Constant.DELETE));
            this.isUpserting = false;
            this.closeDialog();
            this.isDeleting = false;
        }, error => {
            this.save.emit(this.prepareEmitData({id: id, type: AppointmentType.APPOINMENT}, Constant.DELETE, true, false, Constant.DELETE));
            this.isUpserting = false;
            this.isDeleting = false;
            this.isClickedThisNFutureOccurrence = false;
            this.isClickedOnlyThisOccurrence = false;
        });
    }

    prepareData(value) {
        let data: Appointment = JSON.parse(JSON.stringify(value));
        delete data._startDate;
        delete data._endDate;
        if (this.isNew) {
            delete data.treatmentDetails.id;
            delete data.treatmentDetails.appointmentsId;
        }
        data.treatmentDetails.practitionerId = this.selectedPractitioner ? this.selectedPractitioner.id : '';
        data.treatmentDetails.treatmentServiceProduct = this.selectedserviceProductsList;

        if (value.recurrenceRule && value.recurrenceRule != '') {
            data.recurrenceRule = this.formatRRule(value.recurrenceRule.toString().replace('RRULE:', ''));
            data.frequencyType = this.getFrequencyEnum(this.recurrenceRule.repeat.frequency);
            if (this.recurrenceRule && this.recurrenceRule.repeat.frequency == 'ROSD') {
                data.recurrenceRule = 'FREQ=DAILY;INTERVAL=1;' + data.recurrenceRule.replace('FREQ=ROSD;INTERVAL=1;', '');
            }
            if (!this.recurrenceRule) {
                data.recurrenceException = '';
            }
            if (data.recurrenceRule && data.recurrenceRule.includes('FREQ=ROSD')) {
                data.recurrenceRule = data.recurrenceRule.replace('ROSD', 'DAILY');
            }
            // data.recurrenceRule = this.data.recurrenceRule.replace('ROSD', 'DAILY');
        }

        value._startDate = AppoinmentDateUtils.getDateWithTz(value._startDate);
        data.startDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._startDate, {
            'hour': this.getHour(data.startDateUTC),
            'minute': this.roundOffMinute(data.startDateUTC),
            'second': 1
        }));
        value._endDate = AppoinmentDateUtils.getDateWithTz(value._endDate);

        data.endDateUTC = AppoinmentDateUtils.toUtc(AppoinmentDateUtils.combineDateTime(value._endDate, {
            'hour': this.getHour(data.endDateUTC),
            'minute': this.roundOffMinute(data.endDateUTC)
        }));
        return data;
    }

    processData(isForUpdate: boolean = true) {
        this.isUpserting = true;

        if (!this.isNew && isForUpdate && this.isRecurringSchedule) {
            this.editScheduleDialog();
            return;
        }

        // this.editForm.get('title').setValue(this.getCategoryName(this.editForm.get('breakTypeId').value));
        if (!this.isNew) {
            let isTargetEqual = AppoinmentDateUtils.isEqual(this.data.startDate, this.targetData.startDate) && AppoinmentDateUtils.isEqual(this.data.endDate, this.targetData.endDate);
            if (this.isEditingCurrentOnly) {
                let d: Schedule = this.prepareData(this.data);
                let td: Schedule = this.prepareData(this.targetData);
                d.recurrenceException = formatREString(d.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(td.startDateUTC));

                let completed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
                if (!this.isDeleting) {
                    completed.subscribe(value => {
                        if (value != null && value) {
                            this.initFormFromTargetData();
                            this.isNew = !this.isDeleting;
                            this.onSave(true);
                            completed.unsubscribe();
                        }
                    });
                }

                this.upsert(d, completed, (this.isDeleting || isTargetEqual));
            } else if (this.isEditingThisAndFutureSeries) {
                let d: Schedule;
                let td: Schedule = null;
                if (!isTargetEqual) {
                    d = this.prepareData(this.data); // Prepare current form date
                    td = this.prepareData(this.targetData); // Prepare Targeted Data
                    if (d.recurrenceRule.includes(';UNTIL')) { // Check Already has UNTIL
                        d.recurrenceRule = d.recurrenceRule.substring(0, d.recurrenceRule.lastIndexOf(';UNTIL')); // If Found then get REMOVE IT TO ADD NEW
                    }
                    let until = AppoinmentDateUtils.addAndGetNewValue(td.endDateUTC, -1, 'd');
                    let h = parseInt(AppoinmentDateUtils.format(d.endDateUTC, 'HH'));
                    let m = parseInt(AppoinmentDateUtils.format(d.endDateUTC, 'mm'));
                    let s = parseInt(moment(d.endDateUTC).format('ss'));
                    if (d.recurrenceRule.includes(';COUNT')) {
                        d.recurrenceRule = d.recurrenceRule.substring(0, d.recurrenceRule.lastIndexOf(';COUNT'));
                    }
                    d.recurrenceRule += ';UNTIL=' + AppoinmentDateUtils.toUtcUntilRecurrenceException(until, h, m, s, true);
                } else {
                    let value = this.editForm.value;
                    value.recurrenceRule = value.recurrenceRule.rRule.toString();
                    d = this.prepareData(value);
                }
                let completed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
                if (!this.isDeleting) {
                    completed.subscribe(value => {
                        if (value != null && value) {
                            if (!isTargetEqual) {
                                this.initFormFromTargetData();
                                this.isNew = !this.isDeleting;
                                this.onSave(true);
                            }
                            completed.unsubscribe();
                        }
                    });
                }
                this.upsert(d, completed, (this.isDeleting || isTargetEqual));
            } else {
                let value = this.editForm.value;
                if (value.recurrenceRule) {
                    value.recurrenceRule = value.recurrenceRule.rRule.toString();
                }
                let data = this.prepareData(value);
                this.upsert(data, null, true);
            }
        } else {
            let value = this.editForm.value;
            if (value.recurrenceRule && value.recurrenceRule != '') {
                value.recurrenceRule = value.recurrenceRule.rRule.toString();
            }
            if (this.recurrenceRule && this.isEditingThisAndFutureSeries) {
                const endBy = this.recurrenceRule.end ? this.recurrenceRule.end.mode : null;
                const frequency = this.recurrenceRule.repeat ? this.recurrenceRule.repeat.frequency : null;
                if (endBy === 'After' && frequency != 'Weekly') {
                    const diff = AppoinmentDateUtils.getDiff(this.data.startDate, value.startDateUTC);
                    const endAfter = this.recurrenceRule.end.after - diff;
                    value.recurrenceRule = value.recurrenceRule.replace('COUNT=' + this.recurrenceRule.end.after, 'COUNT=' + endAfter);
                }
            }
            let data = this.prepareData(value);
            this.upsert(data, null, true);
        }
    }

    /** Cancel Appointment */
    onCancel() {
        this.cancel.emit();
        this.isUpserting = false;
    }

    /** Date Selection Change */
    dateSelectionChange(value: Date) {
        const date = new Date(value).getDate();
        const month = new Date(value).getMonth();
        const year = new Date(value).getFullYear();

        if (this.targetData) {
            const t_s = this.targetData._startDate;
            const t_e = this.targetData._endDate;
            const tsu = AppoinmentDateUtils._toDate(this.targetData.startDateUTC);
            const teu = AppoinmentDateUtils._toDate(this.targetData.endDateUTC);
            const ts = AppoinmentDateUtils._toDate(this.targetData.startDate);
            const te = AppoinmentDateUtils._toDate(this.targetData.endDate);

            t_s.setDate(date);
            t_s.setMonth(month);
            t_s.setFullYear(year);
            t_e.setDate(date);
            t_e.setMonth(month);
            t_e.setFullYear(year);
            tsu.setDate(date);
            tsu.setMonth(month);
            tsu.setFullYear(year);
            teu.setDate(date);
            teu.setMonth(month);
            teu.setFullYear(year);
            ts.setDate(date);
            ts.setMonth(month);
            ts.setFullYear(year);
            te.setDate(date);
            te.setMonth(month);
            te.setFullYear(year);

            this.targetData._startDate = t_s;
            this.targetData._endDate = t_e;
            this.targetData.startDateUTC = AppoinmentDateUtils.toUtc(tsu);
            this.targetData.endDateUTC = AppoinmentDateUtils.toUtc(teu);
            this.targetData.startDate = AppoinmentDateUtils.toUtc(ts);
            this.targetData.endDate = AppoinmentDateUtils.toUtc(te);
        }

        const fsu = AppoinmentDateUtils._toDate(this.editForm.get('startDateUTC').value, true);
        const feu = AppoinmentDateUtils._toDate(this.editForm.get('endDateUTC').value, true);
        const f_s = AppoinmentDateUtils._toDate(this.editForm.get('_startDate').value, true);
        const f_e = AppoinmentDateUtils._toDate(this.editForm.get('_endDate').value, true);

        fsu.setDate(date);
        fsu.setMonth(month);
        fsu.setFullYear(year);
        feu.setDate(date);
        feu.setMonth(month);
        feu.setFullYear(year);
        f_s.setDate(date);
        f_s.setMonth(month);
        f_s.setFullYear(year);
        f_e.setDate(date);
        f_e.setMonth(month);
        f_e.setFullYear(year);

        this.editForm.get('startDateUTC').setValue(AppoinmentDateUtils.toUtc(fsu));
        this.editForm.get('endDateUTC').setValue(AppoinmentDateUtils.toUtc(feu));
        this.editForm.get('_startDate').setValue(f_s);
        this.editForm.get('_endDate').setValue(f_e);
    }

    /** Get Value On Patient Changed */
    onSelectPatientForAppt($event) {
        this.selectedPatient = $event;
        this.togglePatientDetailsPopup(false);
        this.editForm.get('patientId').setValue(this.patientId);
        if (this.selectedPatient) {
            this.getPatientDetails();
            this.getCases();
        }
    }

    /** All Private Methods */
    private setStartAndEndDate() {
        this.editForm.get('_startDate').setValue(new Date(this.data._startDate));
        this.editForm.get('_endDate').setValue(new Date(this.data._endDate));
        if (this.data) {
            this.data._startDate = new Date(this.data._startDate);
            this.data._endDate = new Date(this.data._endDate);
        }
    }

    /** Create FormGroup For Service & Product */
    private static spFormGroup(): FormGroup {
        return new FormGroup({
            id: new FormControl(''),
            treatmentDetailId: new FormControl(''),
            treatementType: new FormControl(''),
            productId: new FormControl(''),
            serviceId: new FormControl(''),
            units: new FormControl(''),
            duration: new FormControl(''),
            price: new FormControl(''),
            tax: new FormControl(''),
            isStatus: new FormControl(''),
        });
    }

    private getPatientDetails() {
        if (!this.patientId) {
            return;
        }
        this.patientService.getPatientById(this.patientId).subscribe(data => {
            this.selectedPatientDetails = data;
            if (this.isNew) {
                this.treatmentDetailsFG.get('concessionId').setValue(this.selectedPatientDetails.concession);
            }
            this.getProductAndServiceSearch();
        });
    }

    private prepareServiceNProducts() {
        if (Object.keys(this.serviceProducts).length <= 0) {
            for (let i = 0; i < this.serviceNProductFormArray.length; i++) {
                this.serviceProducts[i] = [];
            }
        }
        let idInc = 1;
        const plannedService: any[] = this.serviceProductsOriginal.patientPlannedService;
        if (plannedService && plannedService.length > 0) {
            const dd = {
                id: idInc,
                name: 'PLANNED SERVICE',
                index: 0,
                _class: 'main-header',
            };
            idInc++;
            this.addToServiceNProductList(dd);
            plannedService.forEach((f, index) => {
                // if (!f.isUsed) {
                let price = f.standardPrice;
                f['index'] = 1 + index;
                const dd = {
                    id: f.id,
                    name: (f.serviceCode ? f.serviceCode + ' - ' : '') + f.serviceName + '($' + price + ', ' + f.duration + ' min)',
                    actualName: (f.serviceCode ? f.serviceCode + ' - ' : '') + f.serviceName,
                    actualDuration: f.duration,
                    duration: f.duration,
                    price: f.duration != 0 ? ((price / f.duration) || 0) : price,
                    type: 0,
                    plannedServiceId: f.id,
                    isPlannedService: true,
                    isUsed: f.isUsed,
                    usedIn: f['usedIn'],
                    index: f['index'],
                    _class: 'item-under-2'
                };
                this.addToServiceNProductList(dd);
                this.serviceProductsList.push(this.preparePSObj(f, TreatmentType.Service));
                // }
            });
        }
        if (this.serviceProductsOriginal.service) {
            this.serviceProductsOriginal.service.speciality.forEach(e => {
                const dd = {
                    id: idInc,
                    name: e.specialityName,
                    _class: 'main-header',
                };
                idInc++;
                this.addToServiceNProductList(dd);
                e.category.forEach(d => {
                    const dd = {
                        id: idInc,
                        name: d.categoryName,
                        _class: 'sub-header-1'
                    };
                    idInc++;
                    this.addToServiceNProductList(dd);
                    d.serviceItems.forEach(f => {
                        let price = f.standardPrice;
                        const selectedConcession = this.selectedConcession;
                        if (selectedConcession && f.serviceConcession && f.serviceConcession.length > 0) {
                            for (const concession of f.serviceConcession) {
                                if (concession['concessionId'] == selectedConcession) {
                                    price = concession['concessionAmount'];
                                    break;
                                }
                            }
                        }
                        const dd = {
                            id: f.id,
                            name: (f.productCode ? f.productCode + ' - ' : '') + f.name + '($' + price + ', ' + f.duration + ' min)',
                            actualName: (f.productCode ? f.productCode + ' - ' : '') + f.name,
                            actualDuration: f.duration,
                            duration: f.duration,
                            price: f.duration != 0 ? price / f.duration : price,
                            type: 0,
                            _class: 'item-under-2'
                        };
                        this.addToServiceNProductList(dd);
                        this.serviceProductsList.push(this.preparePSObj(f, TreatmentType.Service));
                    });
                });
            });
        }

        if (this.serviceProductsOriginal.product) {
            this.serviceProductsOriginal.product.forEach(e => {
                const dd = {
                    id: idInc,
                    name: 'PRODUCTS',
                    _class: 'main-header'
                };
                idInc++;
                this.addToServiceNProductList(dd);
                e.productItems.forEach(d => {
                    const dd = {
                        id: d.id,
                        name: d.productCode + ' - ' + d.productName + ' ($' + d.salePrice + ')',
                        actualName: d.productCode + ' - ' + d.productName,
                        actualQty: 1,
                        price: d.salePrice,
                        type: 1,
                        _class: 'item-under-2'
                    };
                    this.addToServiceNProductList(dd);
                    this.serviceProductsList.push(this.preparePSObj(d, TreatmentType.Product));
                });
            });
        }
    }

    private preparePSObj(d: any, type: TreatmentType) {
        return {
            treatmentDetailId: this.treatmentDetailsFG.get('id').value,
            treatementType: type,
            productId: type == TreatmentType.Product ? d['id'] : '00000000-0000-0000-0000-000000000000',
            serviceId: type == TreatmentType.Service ? d['id'] : '00000000-0000-0000-0000-000000000000',
            units: type == TreatmentType.Service ? 0 : 1,
            duration: type == TreatmentType.Service ? d['duration'] : 0,
            price: type == TreatmentType.Service ? d['salePrice'] : d['standardPrice'],
            tax: d.taxRate,
            isStatus: true
        };
    }

    private addToServiceNProductList(data: any) {
        Object.keys(this.serviceProducts).forEach(value => {
            if (this.serviceProducts[value] && this.serviceProducts[value].length <= 0) {
                if (!data['isPlannedService']) {
                    this.serviceProducts[value].push(data);
                } else if (value != data['usedIn'] && !data['isUsed']) {
                    this.serviceProducts[value].push(data);
                }
            } else {
                let found = false;
                let plannedServicesLength = this.serviceProducts[value][0]['name'] != 'PLANNED SERVICE' ? 0 : 1;
                const list = AppUtils.refrenceClone(this.serviceProducts[value]);
                list.forEach((sp, index) => {
                    if (sp['isPlannedService'] && !data['isUsed']) {
                        plannedServicesLength++;
                    }
                    if (sp.id == data.id) {
                        found = true;
                        data['actualQty'] = sp['actualQty'];
                        data['actualDuration'] = sp['actualDuration'];
                        data['duration'] = sp['duration'];
                        this.serviceProducts[value][index] = data;
                    }
                });
                if (!found) {
                    if (data['isPlannedService'] && !data['isUsed']) {
                        const plannedService: any[] = this.serviceProductsOriginal.patientPlannedService;
                        let filterUnused = [];
                        if (plannedService) {
                            filterUnused = plannedService.filter(value => value.isUsed);
                        }
                        // const length = filterUnused ? filterUnused.length : 0;
                        // const dataIndex = data['index'];
                        // const indexToAdd = length > dataIndex ? length - dataIndex : dataIndex - length;
                        this.serviceProducts[value].insert(plannedServicesLength, data);
                    } else if (!data['isPlannedService']) {
                        this.serviceProducts[value].push(data);
                    }
                } else if (value != data['usedIn'] && data['isPlannedService'] && data['isUsed']) {
                    const dataIndex = (this.serviceProducts[value] as any[]).findIndex(x => x['isPlannedService'] && x.id == data['id']);
                    this.serviceProducts[value].splice(dataIndex, 1);
                }
                const isAddPlannedService = this.serviceProducts[value].findIndex(value => value['isPlannedService']) > -1;
                if (isAddPlannedService && this.serviceProducts[value][0]['name'] != 'PLANNED SERVICE') {
                    this.serviceProducts[value].insert(0, {
                        id: 0,
                        name: 'PLANNED SERVICE',
                        _class: 'main-header'
                    });
                } else if (!isAddPlannedService && this.serviceProducts[value][0]['name'] == 'PLANNED SERVICE') {
                    this.serviceProducts[value].splice(0, 1);
                }
            }
        });
    }

    concessionChange() {
        this.prepareServiceNProducts();
    }

    get getTitle() {
        return this.isNew ? 'Add Appointment ' : 'Edit Appointment';
    }

    get selectedPractitioner(): PractitionerSpecialityModel {
        let pId = this.editForm.get('practitionerId').value;
        return pId && this.practitioners ? this.practitioners.find(p => p.practitionerId == pId) : (this.practitionerName ? this.practitionerName : null);
    }

    get endBy() {
        return this.recurrenceRule && this.recurrenceRule.repeat.frequency != 'Never' && this.recurrenceRule.end ? this.recurrenceRule.end.mode : 'Never';
    }

    get recurrenceRule() {
        return this.editForm.get('recurrenceRule').value.raw;
    }

    get selectedDataDate() {
        return this.targetData && Object.keys(this.targetData).length > 0 ? this.targetData._startDate : this.editForm.get('_startDate').value;
    }

    get aePopupAnimation() {
        return {
            type: 'slide',
            direction: 'top',
            duration: 200,
        };
    }

    get patientId() {
        return this.selectedPatient ? this.selectedPatient.id : null;
    }

    /** Get Patient Details */
    get patientNumber() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.patientId : '';
    }

    get patientName() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.firstName + ' ' + this.selectedPatientDetails.lastName : '';
    }

    get patientDOB() {
        const dob = this.selectedPatientDetails.dob;
        if (dob) {
            const yearsOld = AppoinmentDateUtils.getDiff(new Date(), dob, 'years');
            const formattedDob = DateUtils.format(dob, 'DD MMM YYYY');
            return yearsOld + ' Yrs,' + formattedDob;
        }
        return '';
    }

    get patientGender() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.gender : '';
    }

    get patientHome() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.homePhone : '';
    }

    get patientMobile() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.mobile : '';
    }

    get patientEmail() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.email : '';
    }

    get patientOutstanding() {
        return 'NILL';
    }

    get patientMedicalCondition() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.medicalCondition : '';
    }

    get patientAllergy() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.allergy : '';
    }

    get patientMedication() {
        return this.selectedPatientDetails ? this.selectedPatientDetails.medication : '';
    }

    get selectedConcession() {
        return this.treatmentDetailsFG.get('concessionId').value;
    }

    getServiceProducts(index: number) {
        return this.serviceProducts[index] || null;
    }

    /** Redirect to Patient to add new one */
    redirectPatient(data: any) {
        let obj = {
            addEditScheduleData: this.editForm.value,
            resource: this.practitioners,
            SearchTextPatient: '',
        };
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                stateValues: JSON.stringify(obj)
            }
        };
        this._router.navigate(['patients/add'], fromState);
    }
}
