import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/shared/dt-format';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BusinessService } from 'src/app/services/app.business.service';
import { AppState } from 'src/app/app.state';
import { ImageUploadComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import { ImageSnippet, MessageType } from 'src/app/models/app.misc';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MiscService } from 'src/app/services/app.misc.service';
import { AppUtils } from '../../../../../shared/AppUtils';
import { DateUtils } from '../../../../../shared/DateUtils';
import { Router } from '@angular/router';
import { AppAlertService } from '../../../../../shared/services/app-alert.service';
import { ToolsStateService } from '../../tools-state.service';
import { DashboardModel } from 'src/app/models/app.dashboard.model';
import { DashboardService } from 'src/app/services/app.dashboard.service';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';
import {AuthService} from '../../../../../services/app.authenication.service';


@Component({
  selector: 'app-edit-business',
  templateUrl: './edit-business.component.html',
  styleUrls: ['./edit-business.component.css'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class EditBusinessComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(ImageUploadComponent, { static: true }) image: ImageUploadComponent;

  message: string;
  type: MessageType;

  color: any;

  countries: any[];

  submitting: boolean = false;
  steps: any = { hour: 0, minute: 30, second: 0 };
  min: any = new Date();

  businessForm: FormGroup = new FormGroup({
    businessName: new FormControl('', Validators.required),
    businessLogo: new FormControl(''),
    emailId: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$')]),
    businessRegistrationName: new FormControl(''),
    businessRegistrationNo: new FormControl(''),
    contactPerson: new FormControl(''),
    address1: new FormControl(''),
    address2: new FormControl(''),
    country: new FormControl('Australia'),
    state: new FormControl(''),
    city: new FormControl(''),
    postCode: new FormControl(''),
    phone: new FormControl(''),
    mobile: new FormControl(''),
    fax: new FormControl(''),
    website: new FormControl('', Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')),
    status: new FormControl(true),
    isDefault: new FormControl(false),
    bankName: new FormControl(''),
    bsbNumber: new FormControl(''),
    accountNumber: new FormControl(''),
    invoiceTerms: new FormControl(''),
    notes: new FormControl(''),
    timeZone: new FormControl(''),
    startTime: new FormControl(null),
    endTime: new FormControl(null),
    timeSlot: new FormControl(''),
    timeSlotSize: new FormControl(''),
    calendarColor: new FormControl('#8b2e2e'),
    isDoubleBooking: new FormControl(true),
    isPatientOnlineBooking: new FormControl(true)
  });

  apperance: string = 'outline';
  isForEdit = true;
  private dashboard: DashboardModel;
  constructor(private businessService: BusinessService,
    public appState: AppState,
    private location: Location,
    private miscService: MiscService,
    public authService: AuthService,
    private router: Router,
    private alertService: AppAlertService,
    private toolsStateService: ToolsStateService,
    private dashboardService: DashboardService
  ) {
    this.toolsStateService.setTitle('');
    const url = location.path();
    this.isForEdit = url.indexOf('edit') > -1;
  }

  ngOnInit() {
    this.blockUI.start();

    let startTime = new Date();
    startTime.setHours(0);
    startTime.setMinutes(0);
    this.checkForMinEndTime(startTime);
    this.miscService.getCountries().subscribe(countries => {
      this.countries = countries;
    });

    this.dashboardService.getDashboardCheckListView().subscribe(d => {
      this.dashboard = d;
    });
    this.businessService.getParentBusiness(this.appState.UserProfile.parentBusinessId).subscribe(data => {
      if (data.startTime) {
        data.startTime = new Date(data.startTime);
      } else {
        let start = new Date();
        start.setHours(8);
        start.setMinutes(0);
        data.startTime = start;
      }

      if (data.endTime) {
        data.endTime = new Date(data.endTime);
      } else {
        let end = new Date();
        end.setHours(17);
        end.setMinutes(0);
        data.endTime = end;
      }

      // data.status = data.status ? "active" : "inactive";

      if (!data.country) {
        //todo: map this to location service
        data.country = 'Australia';
      }

      if (data.businessLogo) {
        this.image.selectedFile = new ImageSnippet(`data:image/jpeg;base64,${data.businessLogo}`, null);
      }

      this.businessForm.patchValue(data);
      this.checkForMinEndTime(data.startTime);
      this.blockUI.stop();
    });
  }


  createBusiness() {
    let el = document.getElementById('heading');
    if (!this.businessForm.invalid) {
      this.blockUI.start();
      this.submitting = true;
      // this.businessForm.value.status = this.businessForm.value.status == "active";

      if (this.image.selectedFile) {
        this.businessForm.value.businessLogo = this.image.selectedFile.src.replace('data:', '').replace(/^.+,/, '');;
      } else {
        this.businessForm.value.businessLogo = '';
      }

      this.businessService.updateBusiness(this.businessForm.value).subscribe(data => {
        this.submitting = false;
        this.appState.refresh();
        this.dashboard.isBusinessAdded = true;
        this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u => {
          // this.message = 'Business updated successfully.';
          // this.type = MessageType.success;
          this.alertService.displaySuccessMessage('Business updated successfully.');
          el.scrollIntoView();
          if (this.isForEdit) {
            this.location.back();
          } else {
            this.router.navigate(['/dashboard']);
          }
          this.blockUI.stop();
        },err=>{
          this.alertService.displaySuccessMessage('Error occurred while updating business, please try again.');
          this.submitting = false;
          el.scrollIntoView();
          this.blockUI.stop();
        });

      }, error => {
        // this.message = 'Error occurred while updating business, please try again.';
        // this.type = MessageType.error;
        this.alertService.displaySuccessMessage('Error occurred while updating business, please try again.');
        this.submitting = false;
        el.scrollIntoView();
        this.blockUI.stop();
      });
    }
  }

  cancel() {
    this.location.back();
  }

  getMinHour(isApplyToForm: boolean) {
    if (!this.businessForm.get('startTime').value) {
      return;
    }
    let sd = new Date(AppUtils.refrenceClone(this.businessForm.get('startTime').value));
    let md = new Date(this.min);

    md.setHours(sd.getHours());
    md.setMinutes(sd.getMinutes() + 30);
    this.min = md;

    let isLessEnd = DateUtils.compareTime(this.businessForm.get('endTime').value, this.min) <= 0;
    let endTime = new Date();
    endTime.setHours(23);
    endTime.setMinutes(0);
    let isGreterThanEnd = DateUtils.compareTime(this.min, endTime) <= 0;

    if (isApplyToForm && isLessEnd && isGreterThanEnd) {
      this.businessForm.get('endTime').setErrors(null);
      this.businessForm.get('endTime').setValue(this.min);
    } else if (isGreterThanEnd) {
      this.businessForm.get('endTime').setErrors({ endTimeGreater: true });
    }
    // console.log(this.endError)
  }

  private checkForMinEndTime(startTime: Date) {
    this.min = new Date(startTime);
    this.getMinHour(false);
    this.businessForm.get('startTime').valueChanges.subscribe(value => {
      this.getMinHour(true);
    });
  }
}
