import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import { SettingsService } from 'src/app/services/app.settings.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {AppState} from '../../../../app.state';

@Component({
  selector: 'app-discount-type',
  templateUrl: './discount-type.component.html',
  styleUrls: ['./discount-type.component.css']
})
export class DiscountTypeComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.tools_billing;
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private settingService: SettingsService, protected appState: AppState) {
    super(null, appState);
  }

  ngOnInit() {
    this.applyToggle.emit(this.data);
    if(this.data == true) {
      this.setActiveFilter();
    }
    else {
      this.setInactiveFilter();
    }

    this.settingService.getAllDiscountTypes().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.discountLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems('discountName');
    });
  }

}
