import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { SettingsService } from 'src/app/services/app.settings.service';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {AppState} from '../../../../app.state';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-concession',
  templateUrl: './concession.component.html',
  styleUrls: ['./concession.component.css']
})
export class ConcessionComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.tools_patient;
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private settingService: SettingsService, protected appState: AppState) {
    super(null, appState);
  }

  ngOnInit() {
    this.applyToggle.emit(this.data);
    if(this.data == true) {
      this.setActiveFilter();
    }
    else {
      this.setInactiveFilter();
    }

    this.settingService.getAllConcessions().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.concessionLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems('concessionType');
    });
  }
}
