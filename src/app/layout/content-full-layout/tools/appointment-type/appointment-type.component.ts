import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import { SettingsService } from 'src/app/services/app.settings.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {AppState} from '../../../../app.state';

@Component({
  selector: 'app-appointment-type',
  templateUrl: './appointment-type.component.html',
  styleUrls: ['./appointment-type.component.css']
})
export class AppointmentTypeComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.tools_appointment;
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private settingService: SettingsService, protected appState: AppState) {
    super(null, appState);
  }

  ngOnInit() {
    this.applyToggle.emit(this.data);
    if(this.data) {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }

    this.settingService.getAllAppointmentTypes().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.appointmentTypesLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems('appointmentType');
    });
  }

}
