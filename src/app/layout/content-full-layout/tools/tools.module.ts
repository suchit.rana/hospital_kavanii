import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatAutocompleteModule,
  MatDialogModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTableModule
} from '@angular/material';
import {ColorPickerModule} from 'ngx-color-picker';
import {BlockUIModule} from 'ng-block-ui';
import {OrderModule} from 'ngx-order-pipe';
import {TimePickerModule} from '@progress/kendo-angular-dateinputs';
import {ToolsRoutingModule} from './tools-routing.module';
import {BusinessComponent} from './business/business.component';
import {AddBusinessComponent} from './business/add/add.component';
import {ViewBusinessComponent} from './business/view-business/view-business.component';
import {EditBusinessComponent} from './business/edit-business/edit-business.component';
import {LocationsComponent} from './locations/locations.component';
import {AddLocationComponent} from './locations/add-location/add-location.component';
import {ToolsComponent} from './tools.component';
import {SpecialityComponent} from './speciality/speciality.component';
import {TreatmentRoomComponent} from './treatment-room/treatment-room.component';
import {TaxComponent} from './tax/tax.component';
import {PaymentTypeComponent} from './payment-type/payment-type.component';
import {ConcessionComponent} from './concession/concession.component';
import {DiscountTypeComponent} from './discount-type/discount-type.component';
import {AddSpecialityComponent} from './speciality/add-speciality/add-speciality.component';
import {AddTaxComponent} from './tax/add-tax/add-tax.component';
import {AddConcessionComponent} from './concession/add-concession/add-concession.component';
import {AddTreatmentRoomComponent} from './treatment-room/add-treatment-room/add-treatment-room.component';
import {CancelReasonComponent} from './cancel-reason/cancel-reason.component';
import {AddCancelReasonComponent} from './cancel-reason/add-cancel-reason/add-cancel-reason.component';
import {MissedReasonComponent} from './missed-reason/missed-reason.component';
import {AddMissedReasonComponent} from './missed-reason/add-missed-reason/add-missed-reason.component';
import {BlockoutTypeComponent} from './blockout-type/blockout-type.component';
import {AddBlockoutTypeComponent} from './blockout-type/add-blockout-type/add-blockout-type.component';
import {MarketingReferralSourceComponent} from './marketing-referral-source/marketing-referral-source.component';
import {AddMarketingReferralSourceComponent} from './marketing-referral-source/add-marketing-referral-source/add-marketing-referral-source.component';
import {AddPaymentTypeComponent} from './payment-type/add-payment-type/add-payment-type.component';
import {AddAppointmentTypeComponent} from './appointment-type/add-appointment-type/add-appointment-type.component';
import {AppointmentTypeComponent} from './appointment-type/appointment-type.component';
import {AddDiscountTypeComponent} from './discount-type/add-discount-type/add-discount-type.component';
import {DocumentTypeComponent} from './document-type/document-type.component';
import {AddDocumentTypeComponent} from './document-type/add-document-type/add-document-type.component';
import {AppCommonModule} from '../common/app-common.module';
import {CalendarOptionsComponent} from './locations/calendar-options/calendar-options.component';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {ManagingModule} from '../../module/managing.module';
import {AppTimePickerModule} from '../common/time-picker/app-time-picker.module';
import {ToolsStateService} from './tools-state.service';
import {LocationManageService} from './locations/location-manage.service';
import {CharacterLimitModule} from '../../../directive/character-limit/character-limit.module';
import {OnlyNumberModule} from '../../../directive/only-number/only-number.module';
import {AppKendoMultiSelectModule} from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import {AddressFieldModule} from '../shared-content-full-layout/address-field/address-field.module';
import {AccessDirectiveModule} from '../../../shared/directives/access-directive/access-directive.module';

@NgModule({
    declarations: [
        ToolsComponent,
        LocationsComponent,
        AddLocationComponent,
        CalendarOptionsComponent,
        SpecialityComponent,
        TreatmentRoomComponent,
        TaxComponent,
        PaymentTypeComponent,
        ConcessionComponent,
        DiscountTypeComponent,
        AppointmentTypeComponent,
        CancelReasonComponent,
        MissedReasonComponent,
        MarketingReferralSourceComponent,
        DocumentTypeComponent,
        BlockoutTypeComponent,
        BusinessComponent,
        AddBusinessComponent,
        ViewBusinessComponent,
        EditBusinessComponent,
        // LocationsComponent,
        // AddLocationComponent,
        // TabContentComponent,
        // SpecialityComponent,
        // TreatmentRoomComponent,
        // TaxComponent,
        // PaymentTypeComponent,
        // ConcessionComponent,
        // DiscountTypeComponent,
        AddSpecialityComponent,
        AddTaxComponent,
        AddConcessionComponent,
        AddTreatmentRoomComponent,
        // CancelReasonComponent,
        AddCancelReasonComponent,
        // MissedReasonComponent,
        AddMissedReasonComponent,
        AddBlockoutTypeComponent,
        // MarketingReferralSourceComponent,
        AddMarketingReferralSourceComponent,
        // PaymentTypeComponent,
        AddPaymentTypeComponent,
        // AppointmentTypeComponent,
        AddAppointmentTypeComponent,
        // DiscountTypeComponent,
        AddDiscountTypeComponent,
        // DocumentTypeComponent,
        AddDocumentTypeComponent,
    ],
    imports: [
        CommonModule,
        SharedContentFullLayoutModule,
        ManagingModule,
        AppTimePickerModule,
        // SharedModule,
        ToolsRoutingModule,
        AppCommonModule,
        BlockUIModule.forRoot(),
        MatListModule,
        // MatFormFieldModule,
        // MatIconModule,
        MatSelectModule,
        // MatCheckboxModule,
        // MatSidenavModule,
        // MatToolbarModule,
        // MatDividerModule,
        // MatMenuModule,
        // MatListModule,
        // MatTooltipModule,
        // MatExpansionModule,
        // MatDatepickerModule,
        MatSlideToggleModule,
        MatRadioModule,
        // ReactiveFormsModule,
        ColorPickerModule,
        // FormsModule,
        // MatInputModule,
        MatTableModule,
        // MatButtonModule,
        // MatProgressSpinnerModule,
        // DateInputsModule,
        // CalendarModule,
        // IntlModule,
        TimePickerModule,
        // MatButtonToggleModule,
        // MatIconModule,
        // BlockUIModule.forRoot(),
        MatDialogModule,
        OrderModule,
        CharacterLimitModule,
        OnlyNumberModule,
        MatAutocompleteModule,
        AppKendoMultiSelectModule,
        AppTimePickerModule,
        TrimValueAccessorModule,
        AddressFieldModule,
        AccessDirectiveModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [
        SpecialityComponent,
        TreatmentRoomComponent,
        TaxComponent,
        PaymentTypeComponent,
        ConcessionComponent,
        DiscountTypeComponent,
        AppointmentTypeComponent,
        CancelReasonComponent,
        MarketingReferralSourceComponent,
        MissedReasonComponent,
        DocumentTypeComponent,
    ],
    bootstrap: [CalendarOptionsComponent],
    exports: [
        CalendarOptionsComponent
    ],
    providers: [ToolsStateService, LocationManageService]
})
export class ToolsModule {
}
