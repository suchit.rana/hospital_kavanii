import {Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocationModel } from '../../../../models/app.location.model';


@Injectable()
export class LocationManageService {

  private _isAddingLocationSubject = new BehaviorSubject<boolean>(null);
  private _isAddingLocation: boolean = true;
  private _isDisplayMessage: boolean = false;

  constructor() {
  }

  get isAddingLocationSubject(): BehaviorSubject<boolean> {
    return this._isAddingLocationSubject;
  }

  get isAddingLocation(): boolean {
    return this._isAddingLocation;
  }

  set isAddingLocation(value: boolean) {
    this._isAddingLocation = value;
  }

  get isDisplayMessage(): boolean {
    return this._isDisplayMessage;
  }

  set isDisplayMessage(value: boolean) {
    this._isDisplayMessage = value;
  }
  
}
