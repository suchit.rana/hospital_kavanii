import {Component, Input, OnInit} from '@angular/core';
import {BusinessService} from 'src/app/services/app.business.service';
import {AppState} from 'src/app/app.state';
import {BaseGridComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {DateUtils} from '../../../../shared/DateUtils';
import {ToolsStateService} from '../tools-state.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent extends BaseGridComponent implements OnInit {
  @Input() data: boolean;
  recentlyAddedData;
  message;
  type;
  moduleName = RP_MODULE_MAP.tools_location;

  constructor(private businessService: BusinessService,
              protected appState: AppState,
              private toolsStateService: ToolsStateService,
              // private locationManageService: LocationManageService
  ) {
    super(null, appState);
    this.toolsStateService.setTitle('Tools');
  }

  ngOnInit() {
    // this.permission = this.appState.getSubModulePermission('Tools', 'Business Information');

    // const isAddingStaff = this.locationManageService.isAddingLocation;
    // const isDisplayMessage = this.locationManageService.isDisplayMessage;

    // console.log(isDisplayMessage,"this.isDisplayMessage");
    // console.log(isAddingStaff,"isAddingStaff");

    // if (isDisplayMessage) {
    //   const message = isAddingStaff ? 'Location created successfully.' : 'Location updated successfully.';
    //   this.displaySuccessMessage(message);
    // }


    if (this.data == true) {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }

    this.businessService.getLocationsByBusiness(this.appState.UserProfile.parentBusinessId).subscribe(data => {
      data.forEach(d => {
        d['isStatus'] = d['status'];
        d.timeString = '';
        if (d.startTime && d.endTime) {
          let start = new Date(DateUtils.combineDateTime(new Date(), {'hour': parseInt(d.startTime), 'minute': 0}));
          let end = new Date(DateUtils.combineDateTime(new Date(), {'hour': parseInt(d.endTime), 'minute': 0}));
          let startString = start.toLocaleString('en-AU', {hour: 'numeric', minute: 'numeric', hour12: true});
          let endString = end.toLocaleString('en-AU', {hour: 'numeric', minute: 'numeric', hour12: true});
          d.timeString = `${startString} to ${endString}`;
          d.isStatus = d.status;
        }
      });

      this.gridData = data;
      // this.loadItems();
      this.sortChange([{dir: undefined, field: 'locationName'}]);
    });
  }

  // displaySuccessMessage(message: string) {
  //   this.message = message;
  //   this.type = MessageType.success;
  //   // setInterval((a) => {
  //   //   this.message = '';
  //   //   this.type = MessageType.success;
  //   // }, 5000, [])
  //   setTimeout(() => {
  //     this.message = '';
  //     this.type = MessageType.success;
  //   }, 5000);
  // }
}
