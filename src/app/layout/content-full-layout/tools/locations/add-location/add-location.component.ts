import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {AppState} from 'src/app/app.state';
import {ImageUploadComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {CalendarOptionsComponent} from '../calendar-options/calendar-options.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ImageSnippet} from 'src/app/models/app.misc';
import {LocationModel} from 'src/app/models/app.location.model';
import {MiscService} from 'src/app/services/app.misc.service';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {DateUtils} from '../../../../../shared/DateUtils';
import {ToolsStateService} from '../../tools-state.service';
import {StaffService} from 'src/app/services/app.staff.service';
import {LocationManageService} from '../location-manage.service';
import {DashboardModel} from 'src/app/models/app.dashboard.model';
import {DashboardService} from 'src/app/services/app.dashboard.service';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';
import {AuthService} from '../../../../../services/app.authenication.service';

@Component({
    selector: 'app-add-location',
    templateUrl: './add-location.component.html',
    styleUrls: ['./add-location.component.css']
})
export class AddLocationComponent extends BaseItemComponent implements OnInit {
    moduleName = RP_MODULE_MAP.tools_location;
    @BlockUI() blockUI: NgBlockUI;
    @ViewChild(ImageUploadComponent, {static: true}) image: ImageUploadComponent;
    public steps: any = {hour: 0, minute: 15, second: 0};
    contactPerson: any;
    redirectURL: string;

    filteredOptions: any[];
    dashboard: DashboardModel;

    locationForm: FormGroup = new FormGroup({
        locationName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
        locationLogo: new FormControl(''),
        emailId: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')]),
        locationRegistrationName: new FormControl(''),
        locationRegistrationNo: new FormControl(''),
        contactPerson: new FormControl(''),
        address1: new FormControl(''),
        address2: new FormControl(''),
        country: new FormControl('Australia'),
        state: new FormControl(''),
        city: new FormControl(''),
        postCode: new FormControl(''),
        phone: new FormControl(''),
        mobile: new FormControl(''),
        fax: new FormControl(''),
        website: new FormControl('', Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')),
        status: new FormControl(true),
        isDefault: new FormControl(false),
        bankName: new FormControl(''),
        bsbNumber: new FormControl(''),
        accountNumber: new FormControl(''),
        invoiceTerms: new FormControl(''),
        notes: new FormControl(''),
        timeZone: new FormControl('', Validators.required),
        startTime: new FormControl(8),
        endTime: new FormControl(17),
        timeSlot: new FormControl(15),
        timeSlotSize: new FormControl('Default'),
        calendarColor: new FormControl('#ffffff'),
        isDoubleBooking: new FormControl(false),
        isPatientOnlineBooking: new FormControl(false),
        isShowWaitRoom: new FormControl(true),
        isAllowBookingOutsideScheduleHours: new FormControl(true)
    });

    apperance: string = 'outline';

    countries: any[];
    timezone: any[];
    clickedCalendarOptions: boolean = false;
    isFromDashBoard: boolean = false;

    constructor(private businessService: BusinessService,
                protected appState: AppState,
                protected authService: AuthService,
                public location: Location,
                public dialog: MatDialog,
                private _route: ActivatedRoute,
                protected router: Router,
                private miscService: MiscService,
                private toolsStateService: ToolsStateService,
                private staffService: StaffService,
                private locationManageService: LocationManageService,
                private dashboardService: DashboardService
    ) {
        super(location);
        const url = location.path();
        this.isFromDashBoard = url.indexOf('edit') > -1;
        this.locationManageService.isDisplayMessage = false;
    }

    ngOnInit() {

        this.getAllStaffsList();

        this.miscService.getCountries().subscribe(countries => {
            this.countries = countries;
        });

        this.dashboardService.getDashboardCheckListView().subscribe(d => {
            this.dashboard = d;
        });

        this._route.params.subscribe(params => {
            if (params.locationId) {
                this.blockUI.start();
                this.addItem = false;
                this.itemid = params.locationId;
                this.clickedCalendarOptions = true;

                this.businessService.getLocation(params.locationId).subscribe(data => {
                    if (data.startTime) {
                        data.startTime = new Date(DateUtils.combineDateTime(new Date(), {'hour': parseInt(data.startTime), 'minute': 0}));
                        console.log(data.startTime);
                    }
                    if (data.endTime) {
                        data.endTime = new Date(DateUtils.combineDateTime(new Date(), {'hour': parseInt(data.endTime), 'minute': 0}));
                    }

                    // console.log(data);

                    this.locationForm.patchValue(data);
                    this.locationForm.get('timeSlot').patchValue(data.timeSlot || 15);
                    if (data.locationLogo) {
                        this.image.selectedFile = new ImageSnippet(`data:image/jpeg;base64,${data.locationLogo}`, null);
                    }
                    this.blockUI.stop();
                });
            } else {
                this.populateDefaultValues();
            }
        });
        this.toolsStateService.setTitle(this.addItem ? 'Add Location' : 'Edit Location');

    }

    populateDefaultValues() {
        this.locationForm.get('isDoubleBooking').patchValue(false);
        this.locationForm.get('isPatientOnlineBooking').patchValue(false);
        this.locationForm.get('startTime').patchValue(8);
        this.locationForm.get('endTime').patchValue(17);
        this.locationForm.get('timeSlot').patchValue(15);
        this.locationForm.get('timeSlotSize').patchValue('Default');
        this.locationForm.get('calendarColor').patchValue('#ffffff');
        this.locationForm.get('isShowWaitRoom').patchValue(true);
        this.locationForm.get('isAllowBookingOutsideScheduleHours').patchValue(true);
    }

    openDialog(event: Event) {
        event.preventDefault();
        const dialogRef = this.dialog.open(CalendarOptionsComponent, {
            width: '800px',
            panelClass: 'calender-option-popup'
        });

        let instance = dialogRef.componentInstance;
        instance.formGroup = this.locationForm;

        dialogRef.afterClosed().subscribe(result => {
            this.clickedCalendarOptions = true;
        });
    }

    createlocation(event: Event) {
        if (this.locationForm.invalid) {
            this.locationForm.markAllAsTouched();
            this.locationForm.updateValueAndValidity();
            if (this.locationForm.get('timeZone').invalid) {
                this.openDialog(event);
            }
            return;
        }
        let el = document.getElementById('heading');
        let locationUpdate: LocationModel = this.locationForm.value;


        if (!this.locationForm.invalid) {
            this.blockUI.start();
            this.submitting = true;

            locationUpdate.startTime = (DateUtils.format(this.locationForm.get('startTime').value, 'HH'));
            locationUpdate.endTime = (DateUtils.format(this.locationForm.get('endTime').value, 'HH'));

            if (this.image.selectedFile) {
                locationUpdate.locationLogo = this.image.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
            }

            if (this.addItem) {
                this.locationManageService.isAddingLocation = true;
                this.locationManageService.isDisplayMessage = true;
                this.businessService.addLocation(locationUpdate).subscribe(data => {
                    // this.dashboard.isBusinessAdded = true;
                    this.authService.updateToken();
                    const isLocationAdded = this.dashboard.isLocationAdded;
                    this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u => {
                        this.submitting = false;
                        this.appState.refresh();
                        this.blockUI.stop();
                        if (data) {
                            this.appState.loadUserLocations();
                            this.locationManageService.isAddingLocationSubject.next(true);
                            if (!isLocationAdded) {
                                this.router.navigate(['/dashboard']);
                                return;
                            }
                            if (!this.isFromDashBoard) {
                                this.router.navigate(['/tools']);
                            } else {
                                this.router.navigate(['/dashboard']);
                            }
                        }
                    }, error => {
                        this.displayErrorMessage('Error occurred while adding location, please try again.');
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    });

                    // this.displaySuccessMessage('Location added successfully.');
                    // el.scrollIntoView();
                    // this.itemid = data;
                    // this.addItem = false;
                }, error => {
                    this.displayErrorMessage('Error occurred while adding location, please try again.');
                    this.submitting = false;
                    el.scrollIntoView();
                    this.blockUI.stop();
                });
            } else {
                this.locationManageService.isAddingLocation = false;
                this.locationManageService.isDisplayMessage = true;
                locationUpdate.id = this.itemid;
                this.businessService.updateLocation(locationUpdate).subscribe(data => {
                    this.submitting = false;
                    this.appState.refresh();
                    this.blockUI.stop();
                    if (data) {
                        this.locationManageService.isAddingLocationSubject.next(true);
                        this.router.navigate(['/tools']);
                    }
                    // this.displaySuccessMessage('Location updated successfully.');
                    // el.scrollIntoView();
                    // this.blockUI.stop();
                }, error => {
                    this.displayErrorMessage('Error occurred while updating location, please try again.');
                    this.submitting = false;
                    el.scrollIntoView();
                    this.blockUI.stop();
                });
            }
        }
    }

    // Contact Person from staff
    getAllStaffsList() {
        this.staffService.getAllStaffsList().subscribe((data: any[]) => {
            this.contactPerson = data.filter(item => {
                if (item.isStatus) {
                    item['name'] = item.firstName + ' ' + item.lastName;
                    return item;
                }
            });
            this.filteredOptions = this.contactPerson;
        });
    }

    // handleContactPersonFilter(value: string) {
    //   console.log(value)
    //   if (!value) {
    //     this.filteredOptions = this.contactPerson;
    //     return;
    //   }
    //   this.filteredOptions = this.contactPerson.filter((s) => value && s['name'].toLowerCase().indexOf(value.toLowerCase().trim()) !== -1);
    // }

    // contactPerson filter
    // private _filter(value: string): string[] {
    //   console.log(this.contactPerson, 'contactPerson');
    //   const filterValue = value.toLowerCase();
    //   return this.contactPerson.filter(option => option.firstName.toLowerCase().includes(filterValue) || option.lastName.toLowerCase().includes(filterValue));
    // }


    // Max length Validation
    // restrictMaxLength($event, formControl, maxLength: number) {
    //   const length = AppUtils.getStringDefault($event.target.value, '').length + 1;
    //   if (length > maxLength) {
    //     console.log(formControl);
    //     this.locationForm.get(formControl).setErrors({maxLength: true});
    //     this.locationForm.get(formControl).markAsTouched();
    //     this.locationForm.get(formControl).updateValueAndValidity();
    //     console.log(this.locationForm);
    //     $event.preventDefault();
    //   }
    // }

    // restrictAl phabets Validation
    // restrictAlphabets(event) {
    //   const keyCode = event.keyCode;
    //   const excludedKeys = [8, 37, 39, 46];
    //   if (!((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (excludedKeys.includes(keyCode)))) {
    //     event.preventDefault();
    //   }
    // }

}
