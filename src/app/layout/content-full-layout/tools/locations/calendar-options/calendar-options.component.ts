import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MiscService} from 'src/app/services/app.misc.service';
import {DateUtils} from '../../../../../shared/DateUtils';
import {AppUtils} from '../../../../../shared/AppUtils';
import {startEndTimeValidator} from '../../../../../shared/validation';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-calendar-options',
  templateUrl: './calendar-options.component.html',
  styleUrls: ['./calendar-options.component.css']
})
export class CalendarOptionsComponent implements OnInit {
  @Input() formGroup: FormGroup;

  apperance: string = 'outline';
  min;

  public steps: any = {hour: 0, minute: 15, second: 0};

  constructor(private miscService: MiscService,
              private dialogRef: MatDialogRef<CalendarOptionsComponent>
  ) {
  }

  timezone: any[];

  ngOnInit() {
    this.formGroup.get('startTime').setValue(new Date(DateUtils.combineDateTime(new Date(), {'hour': 8, 'minute': 0})));
    this.formGroup.get('endTime').setValue(new Date(DateUtils.combineDateTime(new Date(), {'hour': 17, 'minute': 0})));
    this.formGroup.get('timeSlot').setValue(parseInt(this.formGroup.get('timeSlot').value));    

    this.min = new Date(this.formGroup.get('endTime').value)
    this.getMinHour(false);    
    this.formGroup.get('startTime').valueChanges.subscribe(value => {
      this.getMinHour(true);
    });

    let country = this.formGroup.get('country').value;
    if (country) {
      this.miscService.getTimezone(country).subscribe(tz => {
        this.timezone = tz;
      });
    }
  }

  getMinHour(isApplyToForm: boolean) {
    let sd = new Date(AppUtils.refrenceClone(this.formGroup.get('startTime').value));
    let md = new Date(this.min);
    md.setHours((sd.getHours() + 1));
    md.setMinutes(0);
    this.min = md;
    if (isApplyToForm && DateUtils.compareTime(this.formGroup.get('endTime').value, this.min) <= 0){
      this.formGroup.get('endTime').setValue(this.min);
    }
  }

  onSave() {
    if (this.formGroup.get('timeZone').invalid) {
      // this.formGroup.get('endTime').markAsTouched({ onlySelf: true });
      // this.formGroup.get('endTime').markAsDirty();
      // this.formGroup.get('endTime').updateValueAndValidity();
      this.formGroup.get('timeZone').markAllAsTouched();
      this.formGroup.get('timeZone').updateValueAndValidity();
      return;
    }
    this.dialogRef.close(true);
  }
}
