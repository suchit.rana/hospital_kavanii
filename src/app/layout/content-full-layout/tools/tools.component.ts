import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {BusinessService} from '../../../services/app.business.service';
import {AppState} from '../../../app.state';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {BusinessFormModel} from '../../../models/app.business.model';
import {Tab} from '../../../models/app.tab.model';
import {SpecialityComponent} from './speciality/speciality.component';
import {TreatmentRoomComponent} from './treatment-room/treatment-room.component';
import {TaxComponent} from './tax/tax.component';
import {ConcessionComponent} from './concession/concession.component';
import {PaymentTypeComponent} from './payment-type/payment-type.component';
import {DiscountTypeComponent} from './discount-type/discount-type.component';
import {MatTabChangeEvent, MatButtonToggleChange} from '@angular/material';
import {MessageType} from '../../../models/app.misc';
import {MarketingReferralSourceComponent} from './marketing-referral-source/marketing-referral-source.component';
import {DocumentTypeComponent} from './document-type/document-type.component';
import {AppointmentTypeComponent} from './appointment-type/appointment-type.component';
import {CancelReasonComponent} from './cancel-reason/cancel-reason.component';
import {MissedReasonComponent} from './missed-reason/missed-reason.component';
import {LocationsComponent} from './locations/locations.component';
import {LocationRoleAndPermissionModel} from '../../../models/app.role.model';
import {ToolsStateService} from './tools-state.service';
import {LocationManageService} from './locations/location-manage.service';
import {RP_MODULE_MAP} from '../../../shared/model/RolesAndPermissionModuleMap';
import {BaseItemComponent} from '../../../shared/base-item/base-item.component';
import {Location} from '@angular/common';
import {AuthService} from '../../../services/app.authenication.service';
import {TabContentComponent} from '../common/tab-content/tab-content.component';

@Component({
    selector: 'app-tools',
    templateUrl: './tools.component.html',
    styleUrls: ['./tools.component.css']
})

export class ToolsComponent extends BaseItemComponent implements OnInit, AfterViewInit {
    readonly _any = ['C', 'V', 'E', 'D'];

    // Permissions
    readonly TOOLS_BUSINESS_LOCATION = RP_MODULE_MAP.tools_location;
    readonly TOOLS_SPECIALISATION = RP_MODULE_MAP.tools_specialisation;
    readonly TOOLS_APPTSETTINGS = RP_MODULE_MAP.tools_appointment;
    readonly TOOLS_PATIENTSETTINGS = RP_MODULE_MAP.tools_patient;
    readonly TOOLS_BILLING = RP_MODULE_MAP.tools_billing;

    @ViewChild(LocationsComponent, {static: false}) locations: LocationsComponent;
    @ViewChild('specializationActiveTab', {static: false}) specializationActiveTab: TabContentComponent;

    @BlockUI() blockUI: NgBlockUI;
    messageType = MessageType;

    business: BusinessFormModel;

    isRoot: boolean = false;
    isError: boolean = false;
    isLoading: boolean = true;

    isTablet: boolean;

    businessAccess: string[] = [];
    locationAccess: string[] = [];

    showBusinessInformationTab: boolean = false;
    showSpecialisationTab: boolean = false;
    showAppointmentSettingsTab: boolean = false;
    showPatientSettingsTab: boolean = false;
    showBillingTab: boolean = false;
    specialisationTabsToggle: string = 'active';
    apptSettingsTabsToggle: string = 'active';
    patientSettingsTabsToggle: string = 'active';
    billingTabsToggle: string = 'active';

    specialisationTabs: Tab[] = [
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: SpecialityComponent,
            title: 'Speciality',
            addLabel: 'Add Speciality',
            addUrl: '/tools/speciality/add',
            moduleName: this.TOOLS_SPECIALISATION
        }
    ];

    appointmentTabs: Tab[] = [
        {
            active: true,
            toggleValue: 'active',
            id: 2,
            component: TreatmentRoomComponent,
            title: 'Treatment Room',
            addLabel: 'Add Treatment Room',
            addUrl: '/tools/treatmentRoom/add',
            moduleName: this.TOOLS_APPTSETTINGS
        },
        {
            active: true,
            id: 2,
            toggleValue: 'active',
            component: AppointmentTypeComponent,
            title: 'Appointment Type',
            addLabel: 'Add Appointment Type',
            addUrl: '/tools/appointmentType/add',
            moduleName: this.TOOLS_APPTSETTINGS
        },
        {
            active: true,
            id: 2,
            toggleValue: 'active',
            component: CancelReasonComponent,
            title: 'Cancel Reason',
            addLabel: 'Add Cancel Reason',
            addUrl: '/tools/cancelReason/add',
            moduleName: this.TOOLS_APPTSETTINGS
        },
        {
            active: true,
            id: 2,
            toggleValue: 'active',
            component: MissedReasonComponent,
            title: 'Missed Reason',
            addLabel: 'Add Missed Reason',
            addUrl: '/tools/missedReason/add',
            moduleName: this.TOOLS_APPTSETTINGS
        },
    ];

    patientTabs: Tab[] = [
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: MarketingReferralSourceComponent,
            title: 'Marketing Referral Source',
            addLabel: 'Add Marketing Referral Source',
            addUrl: '/tools/marketingReferralSource/add',
            moduleName: this.TOOLS_PATIENTSETTINGS
        },
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: ConcessionComponent,
            title: 'Concession',
            addLabel: 'Add Concession',
            addUrl: '/tools/concession/add',
            moduleName: this.TOOLS_PATIENTSETTINGS
        },
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: DocumentTypeComponent,
            title: 'Document Type',
            addLabel: 'Add Document Type',
            addUrl: '/tools/documentType/add',
            moduleName: this.TOOLS_PATIENTSETTINGS
        },
    ];


    billingsTabs: Tab[] = [
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: TaxComponent,
            title: 'Tax',
            addLabel: 'Add Tax',
            addUrl: '/tools/tax/add',
            moduleName: this.TOOLS_BILLING
        },
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: DiscountTypeComponent,
            title: 'Discount Type',
            addLabel: 'Add Discount Type',
            addUrl: '/tools/discountType/add',
            moduleName: this.TOOLS_BILLING
        },
        {
            active: true,
            id: 1,
            toggleValue: 'active',
            component: PaymentTypeComponent,
            title: 'Payment Type',
            addLabel: 'Add Payment Type',
            addUrl: '/tools/paymentType/add',
            moduleName: this.TOOLS_BILLING
        }
    ];

    message;
    type;

    constructor(
        public location: Location,
        public router: Router,
        private businessService: BusinessService,
        public appState: AppState,
        public toolsStateService: ToolsStateService,
        private locationManageService: LocationManageService,
        public authService: AuthService,
    ) {
        super(location, router, appState);
    }

    ngOnInit() {
        this.toolsStateService.setTitle('Tools');
        this.isRoot = this.router.url == '/tools';
        if (this.isRoot) {
            this.populateLanding();
        }
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.isRoot = event.url == '/tools';
                if (this.isRoot) {
                    this.populateLanding();
                }
            }
        });


        this.locationManageService.isAddingLocationSubject.subscribe(value => {
            if (value) {
                const isAddingStaff = this.locationManageService.isAddingLocation;
                const isDisplayMessage = this.locationManageService.isDisplayMessage;

                if (isDisplayMessage) {
                    const message = isAddingStaff ? 'Location created successfully.' : 'Location updated successfully.';
                    this.displaySuccessMessage(message);
                }
                this.locationManageService.isAddingLocationSubject.next(false);
            }
        });

        if (this.appState.applicableForPermission()) {
            this.appState.permissionState.subscribe(p => {
                let tools = p.find(p => p.moduleName == 'Tools');
                this.showSpecialisationTab = this.tabAccess(tools, 'Specialisation');
                this.showPatientSettingsTab = this.tabAccess(tools, 'Patient Settings');
                this.showBusinessInformationTab = this.tabAccess(tools, 'Business Information');
                this.showBillingTab = this.tabAccess(tools, 'Billing');
                this.showAppointmentSettingsTab = this.tabAccess(tools, 'Appointment Settings');
                if (tools) {
                    let business = tools.appSubModules.find(m => m.subModuleName == 'Business Information');
                    let biPermission: string[] = [];
                    if (tools.isFullAccess) {
                        biPermission = ['V', 'E', 'D'];
                    } else {
                        biPermission = business && business.accessLevel ? business.accessLevel.split(',') : [];
                    }
                    this.businessAccess = biPermission;
                    this.locationAccess = biPermission;
                }
            });
        } else {
            this.showSpecialisationTab = true;
            this.showPatientSettingsTab = true;
            this.showBusinessInformationTab = true;
            this.showBillingTab = true;
            this.showAppointmentSettingsTab = true;
            this.businessAccess = ['V', 'E', 'D'];
            this.locationAccess = ['V', 'E', 'D'];
        }
    }

    ngAfterViewInit() {
        switch (this.appState.selectedTab) {
            case 0:
                break;
            case 1:
                switch (this.appState.selectedSpecialisationTab) {
                    case 0:
                        this.specialisationTabsToggle = this.specialisationTabs[this.appState.selectedSpecialisationTab].component['data'];
                        break;
                }
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }

    tabAccess(permission: LocationRoleAndPermissionModel, name: string) {
        return permission && (permission.isFullAccess || (permission.appSubModules && permission.appSubModules.find(a => a.subModuleName == name) != null));
    }


    populateLanding() {
        this.blockUI.start();
        this.businessService.getParentBusiness(this.appState.UserProfile.parentBusinessId).subscribe(data => {
            if (data.businessLogo) {
                data.businessLogo = `data:image/jpg;base64,${data.businessLogo}`;
            }

            this.business = data;

            if (data.startTime && data.endTime) {
                let start = new Date(data.startTime);
                let end = new Date(data.endTime);
                let startString = start.toLocaleString('en-AU', {hour: 'numeric', minute: 'numeric', hour12: true});
                let endString = end.toLocaleString('en-AU', {hour: 'numeric', minute: 'numeric', hour12: true});
                this.business.timeString = `${startString} to ${endString}`;
            }

            this.blockUI.stop();
        }, error => {
            console.log(error);
            this.blockUI.stop();
            this.isError = true;
        });
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent) {
        this.appState.selectedTabState.next(tabChangeEvent.index);
    }

    specialisationTabChanged(tabChangeEvent: MatTabChangeEvent) {
        this.appState.selectedSpecialisationTabState.next(tabChangeEvent.index);
    }

    appointmentTabChanged(tabChangeEvent: MatTabChangeEvent) {
        this.appState.selectedAppointmentTabState.next(tabChangeEvent.index);
    }

    patientTabChanged(tabChangeEvent: MatTabChangeEvent) {
        this.appState.selectedPatientTabState.next(tabChangeEvent.index);
    }

    billingTabChanged(tabChangeEvent: MatTabChangeEvent) {
        this.appState.selectedBillingTabState.next(tabChangeEvent.index);
    }

    locationActiveChanged(event: MatButtonToggleChange) {
        if (event.value == 'active') {
            this.locations.setActiveFilter();
        } else {
            this.locations.setInactiveFilter();
        }
        this.locations.loadItems();
    }

    specialisationActiveChanged(event: MatButtonToggleChange) {
        let tab = this.specialisationTabs.find(t => t.title == event.source.name);
        tab.active = event.value == 'active';
    }

    appointmentActiveChanged(event: MatButtonToggleChange) {
        let tab = this.appointmentTabs.find(t => t.title == event.source.name);
        tab.active = event.value == 'active';
    }

    patientActiveChanged(event: MatButtonToggleChange) {
        let tab = this.patientTabs.find(t => t.title == event.source.name);
        tab.active = event.value == 'active';
    }

    billingActiveChanged(event: MatButtonToggleChange) {
        let tab = this.billingsTabs.find(t => t.title == event.source.name);
        tab.active = event.value == 'active';
    }

    displaySuccessMessage(message: string) {
        this.message = message;
        this.type = MessageType.success;
        // setInterval((a) => {
        //   this.message = '';
        //   this.type = MessageType.success;
        // }, 5000, [])
        setTimeout(() => {
            this.message = '';
            this.type = MessageType.success;
        }, 5000);
    }

    changeSpecializationToggle($event: boolean, tabId: number) {
        this.specialisationTabs[tabId].toggleValue = $event ? 'active' : 'inactive';
    }

    changeApptSettingsToggle($event: boolean, tabId: number) {
        this.appointmentTabs[tabId].toggleValue = $event ? 'active' : 'inactive';
    }

    changePatientSettingToggle($event: boolean, tabId: number) {
        this.patientTabs[tabId].toggleValue = $event ? 'active' : 'inactive';
    }

    changeBillingToggle($event: boolean, tabId: number) {
        this.billingsTabs[tabId].toggleValue = $event ? 'active' : 'inactive';
    }
}
