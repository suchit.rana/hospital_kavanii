import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import { SettingsService } from 'src/app/services/app.settings.service';
import {AppState} from '../../../../app.state';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-payment-type',
  templateUrl: './payment-type.component.html',
  styleUrls: ['./payment-type.component.css']
})
export class PaymentTypeComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.tools_billing;
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private settingService: SettingsService, protected appSate: AppState) {
    super(null, appSate);
  }

  ngOnInit() {
    this.applyToggle.emit(this.data);
    if(this.data == true) {
      this.setActiveFilter();
    }
    else {
      this.setInactiveFilter();
    }

    this.settingService.getAllPaymentTypes().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.paymentLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems('paymentType');
    });
  }
}
