import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { AppState } from 'src/app/app.state';
import { SettingsService } from 'src/app/services/app.settings.service';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {Tab} from '../../../../models/app.tab.model';

@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.css']
})
export class SpecialityComponent extends BaseGridComponent implements OnInit {
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();
  moduleName = RP_MODULE_MAP.tools_specialisation;

  constructor(private settingService: SettingsService,
              public appState: AppState) {
    super(null, appState);
  }

  ngOnInit() {
    // this.permission = this.appState.getSubModulePermission("Tools", "Specialisation");

    this.applyToggle.emit(this.data);
    if(this.data == true) {
      this.setActiveFilter();
    }
    else {
      this.setInactiveFilter();
    }

    this.settingService.getAllSpecialties().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.specialityLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems('specialityName');
    });
  }
}
