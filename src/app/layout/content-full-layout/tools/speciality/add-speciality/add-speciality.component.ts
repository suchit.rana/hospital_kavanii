import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { BusinessService } from 'src/app/services/app.business.service';
import { AppState } from 'src/app/app.state';
import { ActivatedRoute } from '@angular/router';
import { SettingsService } from 'src/app/services/app.settings.service';
import { SpecialityModel, SpecialityLocationModel } from 'src/app/models/app.settings.model';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BaseItemComponent } from '../../../../../shared/base-item/base-item.component';
import { LocationModel, LocationGridModel } from 'src/app/models/app.location.model';
import { noWhitespaceValidator } from 'src/app/shared/validation';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-add-speciality',
  templateUrl: './add-speciality.component.html',
  styleUrls: ['./add-speciality.component.css', '../../../../../shared/base-item/base-item.component.css']
})

export class AddSpecialityComponent extends BaseItemComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  moduleName = RP_MODULE_MAP.tools_specialisation;

  specialityForm: FormGroup = new FormGroup({
    specialityName: new FormControl("", [Validators.required, noWhitespaceValidator]),
    locationName: new FormControl("", Validators.required),
    isAllowAllLocation: new FormControl(false),
    isEnableOnlineBooking: new FormControl(false),
    isStatus: new FormControl(true)
  });

  constructor(public businessService: BusinessService,
    public appState: AppState,
    public location: Location,
    public _route: ActivatedRoute,
    public settingsService: SettingsService,
    public alertService: AppAlertService,
  ) {
    super(location, null, appState);
  }

  ngOnInit() {

    this.businessService.getLocationsByBusiness(this.appState.userProfile.parentBusinessId).subscribe(locations => {
      this.locationList = locations;
      this._route.params.subscribe(params => {
        if(params.specialityId) {
            this.addItem = false;
            this.itemid = params.specialityId;
            this.settingsService.getSpeciality(this.itemid).subscribe(s => {
              this.specialityForm.patchValue(s);
              this.onApplyLocationChanged(s.isAllowAllLocation);
              let locationName = [];
              s.specialityLocation.forEach(sl => {
                locationName.push(this.locationList.find(l => l.id == sl.locationId));
              });
              this.specialityForm.get("locationName").patchValue(locationName);
            });
        }
      });
    });
  }

  submitSpeciality() {
    let speciality: SpecialityModel = this.specialityForm.value;

    if(!this.specialityForm.invalid) {
      this.blockUI.start();
      this.submitting = true;
      let locations: SpecialityLocationModel[] = [];
      if(!speciality.isAllowAllLocation) {
        let locationSelections: LocationGridModel[] = this.specialityForm.get("locationName").value;
        if(locationSelections) {
          locationSelections.forEach(l => {
            let m = new SpecialityLocationModel();
            m.locationId = l.id;
            m.locationName = l.locationName;
            m.specialityId = this.itemid;
            locations.push(m);
          });
        }
      }

      speciality.specialityLocation = locations;

      if(this.addItem) {
        this.settingsService.createSpeciality(speciality).subscribe(d => {
          this.submitting = false;
          this.alertService.displaySuccessMessage("Speciality added successfully.");
          this.blockUI.stop();
          this.itemid = d;
          this.addItem = false;
          this.cancel();
        }, error => {
          this.alertService.displayErrorMessage(error);
          this.submitting = false;
          this.blockUI.stop();
        });
      }
      else {
        speciality.id = this.itemid;
        this.settingsService.updateSpeciality(speciality).subscribe(d => {
          this.submitting = false;
          this.alertService.displaySuccessMessage("Speciality updated successfully.");
          this.blockUI.stop();
          this.cancel();
        }, error => {
          this.alertService.displayErrorMessage(error);
          this.submitting = false;
          this.blockUI.stop();
        });
      }
    }
  }

  onApplyLocationChanged(event) {
    if(event === true) {
      this.specialityForm.get("locationName").clearValidators();
    }
    else {
      this.specialityForm.get("locationName").setValidators([Validators.required]);
    }
    this.specialityForm.get("locationName").updateValueAndValidity();
  }
}
