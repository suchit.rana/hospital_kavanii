import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {AppState} from 'src/app/app.state';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {SettingsService} from 'src/app/services/app.settings.service';
import {TreatmentRoomLocation, TreatmentRoomModel} from 'src/app/models/app.settings.model';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {LocationGridModel} from 'src/app/models/app.location.model';
import {noWhitespaceValidator} from 'src/app/shared/validation';
import {For, FromPage} from '../../../../../shared/interface';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-add-treatment-room',
  templateUrl: './add-treatment-room.component.html',
  styleUrls: ['./add-treatment-room.component.css', '../../../../../shared/base-item/base-item.component.css']
})
export class AddTreatmentRoomComponent extends BaseItemComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  moduleName = RP_MODULE_MAP.tools_appointment;
  fromstate: any;
  stateValues: any;

  treatmentRoomForm: FormGroup = new FormGroup({
    treatmentType: new FormControl('', [Validators.required, noWhitespaceValidator]),
    locationName: new FormControl('', Validators.required),
    isAllowAllLocation: new FormControl(false),
    isStatus: new FormControl(true)
  });

  constructor(
    public businessService: BusinessService,
    public appState: AppState,
    public location: Location,
    public _route: ActivatedRoute,
    public settingsService: SettingsService,
    protected router: Router,
    protected alertService: AppAlertService,
  ) {
    super(location, router);
    if (this.router.getCurrentNavigation() != null) {
      this.fromstate = this.router.getCurrentNavigation().extras.state.fromState;
      this.stateValues = this.fromstate ? this.router.getCurrentNavigation().extras.state.stateValues : null;
    } else {
      this.fromstate = history.state.fromState;
      this.stateValues = this.fromstate ? history.state.stateValues : null;
    }
  }

  ngOnInit() {
    this.businessService.getLocationsByBusiness(this.appState.userProfile.parentBusinessId).subscribe(locations => {
      this.locationList = locations;
      this._route.params.subscribe(params => {
        if (params.treatmentRoomId) {
          this.addItem = false;
          this.itemid = params.treatmentRoomId;
          this.settingsService.getTreatmentRoom(this.itemid).subscribe(t => {
            this.treatmentRoomForm.patchValue(t);
            this.onApplyLocationChanged(t.isAllowAllLocation);
            let locationName = [];
            t.treatmentLocation.forEach(sl => {
              locationName.push(this.locationList.find(l => l.id == sl.locationId));
            });
            this.treatmentRoomForm.get('locationName').patchValue(locationName);
          });
        }
      });
    });
  }

  submitTreatmentRoom() {
    let treatment: TreatmentRoomModel = this.treatmentRoomForm.value;

    if (!this.treatmentRoomForm.invalid) {
      this.blockUI.start();
      this.submitting = true;
      let locations: TreatmentRoomLocation[] = [];
      if (!treatment.isAllowAllLocation) {
        let locationSelections: LocationGridModel[] = this.treatmentRoomForm.get('locationName').value;
        if (locationSelections) {
          locationSelections.forEach(l => {
            let m = new TreatmentRoomLocation();
            m.locationId = l.id;
            m.locationName = l.locationName;
            m.treatmentId = this.itemid;
            locations.push(m);
          });
        }
      }

      treatment.treatmentLocation = locations;

      if (this.addItem) {
        this.settingsService.createTreatmentRoom(treatment).subscribe(d => {
          if (this.isFromState()) {
            const state: NavigationExtras = {
              state: {
                fromState: true,
                fromPage: FromPage.TreatmentRoom,
                for: For.TreatmentRoom
              }
            };
            this.router.navigate(['/appointment'], state);
            return;
          }
          if (this.fromstate) {
            const data = JSON.parse(this.stateValues);
            data.addEditScheduleData['treatmentRoomId'] = d;
            this.stateValues = JSON.stringify(data);
            let fromState: NavigationExtras = {
              state: {
                AppointmentTypeAdded: true,
                stateValues: this.stateValues
              }
            };
            this.router.navigate(['/appointment'], fromState);
          } else {
            this.submitting = false;
            this.alertService.displaySuccessMessage('Treatment Room added successfully.');
            this.blockUI.stop();
            this.itemid = d;
            this.addItem = false;
            this.cancel();
          }
        }, error => {
          this.alertService.displayErrorMessage(error);
          this.submitting = false;
          this.blockUI.stop();
        });
      } else {
        treatment.id = this.itemid;
        this.settingsService.updateTreatmentRoom(treatment).subscribe(d => {
          this.submitting = false;
          this.alertService.displaySuccessMessage('Treatment Room updated successfully.');
          this.blockUI.stop();
          this.cancel();
        }, error => {
          this.alertService.displayErrorMessage(error);
          this.submitting = false;
          this.blockUI.stop();
        });
      }
    }
  }

  onApplyLocationChanged(event) {
    if (event === true) {
      this.treatmentRoomForm.get('locationName').clearValidators();
    } else {
      this.treatmentRoomForm.get('locationName').setValidators([Validators.required]);
    }
    this.treatmentRoomForm.get('locationName').updateValueAndValidity();
  }

}
