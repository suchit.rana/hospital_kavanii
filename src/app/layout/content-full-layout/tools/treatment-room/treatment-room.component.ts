import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { SettingsService } from 'src/app/services/app.settings.service';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {AppState} from '../../../../app.state';

@Component({
  selector: 'app-treatment-room',
  templateUrl: './treatment-room.component.html',
  styleUrls: ['./treatment-room.component.css']
})
export class TreatmentRoomComponent extends BaseGridComponent implements OnInit {
  @Input() data: boolean;
  @Output() applyToggle: EventEmitter<boolean> = new EventEmitter<boolean>();
   moduleName = RP_MODULE_MAP.tools_appointment;

  constructor(private settingService: SettingsService, protected appState: AppState) {
    super(null, appState);
  }

  ngOnInit() {
    this.applyToggle.emit(this.data);
    if(this.data == true) {
      this.setActiveFilter();
    }
    else {
      this.setInactiveFilter();
    }

    this.settingService.getAllTreatmentRooms().subscribe(data => {
      data.forEach(d => {
        d.locationName = d.isAllowAllLocation ? "All Locations" : d.treatmentLocation.map(l => l.locationName).join(",");
      });
      this.gridData = data;
      this.loadItems();
    });
  }
}
