import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolsComponent } from './tools.component';
import { BusinessComponent } from './business/business.component';
import { EditBusinessComponent } from './business/edit-business/edit-business.component';
import { LocationsComponent } from './locations/locations.component';
import { AddLocationComponent } from './locations/add-location/add-location.component';
import { SpecialityComponent } from './speciality/speciality.component';
import { AddSpecialityComponent } from './speciality/add-speciality/add-speciality.component';
import { TaxComponent } from './tax/tax.component';
import { AddTaxComponent } from './tax/add-tax/add-tax.component';
import { ConcessionComponent } from './concession/concession.component';
import { AddConcessionComponent } from './concession/add-concession/add-concession.component';
import { TreatmentRoomComponent } from './treatment-room/treatment-room.component';
import { AddTreatmentRoomComponent } from './treatment-room/add-treatment-room/add-treatment-room.component';
import { CancelReasonComponent } from './cancel-reason/cancel-reason.component';
import { AddCancelReasonComponent } from './cancel-reason/add-cancel-reason/add-cancel-reason.component';
import { MissedReasonComponent } from './missed-reason/missed-reason.component';
import { AddMissedReasonComponent } from './missed-reason/add-missed-reason/add-missed-reason.component';
import { BlockoutTypeComponent } from './blockout-type/blockout-type.component';
import { AddBlockoutTypeComponent } from './blockout-type/add-blockout-type/add-blockout-type.component';
import { MarketingReferralSourceComponent } from './marketing-referral-source/marketing-referral-source.component';
import { AddMarketingReferralSourceComponent } from './marketing-referral-source/add-marketing-referral-source/add-marketing-referral-source.component';
import { PaymentTypeComponent } from './payment-type/payment-type.component';
import { AddPaymentTypeComponent } from './payment-type/add-payment-type/add-payment-type.component';
import { AppointmentTypeComponent } from './appointment-type/appointment-type.component';
import { AddAppointmentTypeComponent } from './appointment-type/add-appointment-type/add-appointment-type.component';
import { DiscountTypeComponent } from './discount-type/discount-type.component';
import { AddDiscountTypeComponent } from './discount-type/add-discount-type/add-discount-type.component';
import { DocumentTypeComponent } from './document-type/document-type.component';
import { AddDocumentTypeComponent } from './document-type/add-document-type/add-document-type.component';


const routes: Routes = [
  {
    path: '',
    component: ToolsComponent,
    children: [
      {
        'path': 'business', component: BusinessComponent
      },
      {
        'path': 'business/edit', component: EditBusinessComponent
      },
      {
        'path': 'business/setup', component: EditBusinessComponent
      },
      {
        'path': 'locations', component: LocationsComponent
      },
      {
        'path': 'locations/add', component: AddLocationComponent
      },
      {
        'path': 'locations/setup', component: AddLocationComponent
      },
      {
        'path': 'locations/edit/:locationId', component: AddLocationComponent
      },
      {
        'path': 'speciality', component: SpecialityComponent,
      },
      {
        'path': 'speciality/add', component: AddSpecialityComponent
      },
      {
        'path': 'speciality/edit/:specialityId', component: AddSpecialityComponent
      },
      {
        'path': 'tax', component: TaxComponent,
      },
      {
        'path': 'tax/add', component: AddTaxComponent
      },
      {
        'path': 'tax/edit/:taxId', component: AddTaxComponent
      },
      {
        'path': 'concession', component: ConcessionComponent,
      },
      {
        'path': 'concession/add', component: AddConcessionComponent
      },
      {
        'path': 'concession/edit/:concessionId', component: AddConcessionComponent
      },
      {
        'path': 'treatmentRoom', component: TreatmentRoomComponent,
      },
      {
        'path': 'treatmentRoom/add', component: AddTreatmentRoomComponent
      },
      {
        'path': 'treatmentRoom/edit/:treatmentRoomId', component: AddTreatmentRoomComponent
      },
      {
        'path': 'cancelReason', component: CancelReasonComponent,
      },
      {
        'path': 'cancelReason/add', component: AddCancelReasonComponent
      },
      {
        'path': 'cancelReason/edit/:cancelReasonId', component: AddCancelReasonComponent
      },
      {
        'path': 'missedReason', component: MissedReasonComponent,
      },
      {
        'path': 'missedReason/add', component: AddMissedReasonComponent
      },
      {
        'path': 'missedReason/edit/:missedReasonId', component: AddMissedReasonComponent
      },
      {
        'path': 'blockoutType', component: BlockoutTypeComponent,
      },
      {
        'path': 'blockoutType/add', component: AddBlockoutTypeComponent
      },
      {
        'path': 'blockoutType/edit/:blockoutTypeId', component: AddBlockoutTypeComponent
      },
      {
        'path': 'marketingReferralSource', component: MarketingReferralSourceComponent,
      },
      {
        'path': 'marketingReferralSource/add', component: AddMarketingReferralSourceComponent
      },
      {
        'path': 'marketingReferralSource/edit/:marketingReferralSourceId', component: AddMarketingReferralSourceComponent
      },
      {
        'path': 'paymentType', component: PaymentTypeComponent,
      },
      {
        'path': 'paymentType/add', component: AddPaymentTypeComponent
      },
      {
        'path': 'paymentType/edit/:paymentTypeId', component: AddPaymentTypeComponent
      },
      {
        'path': 'appointmentType', component: AppointmentTypeComponent,
      },
      {
        'path': 'appointmentType/add', component: AddAppointmentTypeComponent
      },
      {
        'path': 'appointmentType/edit/:appointmentTypeId', component: AddAppointmentTypeComponent
      },
      {
        'path': 'discountType', component: DiscountTypeComponent,
      },
      {
        'path': 'discountType/add', component: AddDiscountTypeComponent
      },
      {
        'path': 'discountType/edit/:discountTypeId', component: AddDiscountTypeComponent
      },
      {
        'path': 'documentType', component: DocumentTypeComponent,
      },
      {
        'path': 'documentType/add', component: AddDocumentTypeComponent
      },
      {
        'path': 'documentType/edit/:documentTypeId', component: AddDocumentTypeComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolsRoutingModule { }
