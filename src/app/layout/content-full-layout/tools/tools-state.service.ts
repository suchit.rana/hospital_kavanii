import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolsStateService {

  private _title: BehaviorSubject<string> = new BehaviorSubject<string>('Tools');

  title$ = this.title.asObservable();

  constructor() {
  }

  get title(): BehaviorSubject<string> {
    return this._title;
  }

  setTitle(title: string) {
    this._title.next(title);
  }
}
