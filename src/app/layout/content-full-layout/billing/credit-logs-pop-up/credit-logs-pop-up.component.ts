import { Component, Input, OnInit } from '@angular/core';
import { DialogRef } from '@progress/kendo-angular-dialog';
import { AppDialogService, IPopupAction } from 'src/app/shared/services/app-dialog.service';
import { CreditLogsComponent } from '../credit-logs/credit-logs.component';
import { CreditLogModel } from 'src/app/models/app.billing.model';

@Component({
  selector: 'app-credit-logs-pop-up',
  templateUrl: './credit-logs-pop-up.component.html',
  styleUrls: ['./credit-logs-pop-up.component.css']
})
export class CreditLogsPopUpComponent implements OnInit {

  @Input()
  logs:CreditLogModel[];

  static ref: IPopupAction;

  constructor(
    private dialog:DialogRef
  ) { }

  ngOnInit() {
    console.log(this.logs);
  }

  // static open(dialogService:AppDialogService):IPopupAction{
  //   return this.ref = dialogService.open(CreditLogsComponent,{
  //     title: 'CREDIT LOG',
  //     width: '778px',
  //     cancelBtnTitle: 'No',
  //     saveBtnTitle: 'Yes',
  //     hideSave: false,
  //     hideOnSave: false,
  //     hideFooter: true,
  //     data: {

  //     }
  //   });
  // }

}
