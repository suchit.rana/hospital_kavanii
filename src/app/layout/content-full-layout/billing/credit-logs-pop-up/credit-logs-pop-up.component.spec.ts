import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditLogsPopUpComponent } from './credit-logs-pop-up.component';

describe('CreditLogsPopUpComponent', () => {
  let component: CreditLogsPopUpComponent;
  let fixture: ComponentFixture<CreditLogsPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditLogsPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditLogsPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
