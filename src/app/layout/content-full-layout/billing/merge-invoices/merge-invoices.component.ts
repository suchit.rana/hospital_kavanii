import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import { DxPopoverComponent } from 'devextreme-angular';
import { InvoiceModel } from 'src/app/models/app.billing.model';
import { PaymentTypeModel } from 'src/app/models/app.settings.model';
import { AppDialogService } from 'src/app/shared/services/app-dialog.service';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';
import {GridDataResult} from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-merge-invoices',
  templateUrl: './merge-invoices.component.html',
  styleUrls: ['./merge-invoices.component.scss']
})
export class MergeInvoicesComponent extends BaseGridComponent implements OnInit, OnChanges {

  readonly LOG = 'LOGS';
  readonly PREVIEW = 'PREVIEW';
  readonly REFUND = 'REFUND';

  invoiceActionItem: InvoiceModel; //Item that was selected through action(GEAR ICON)

  @Input() invoices: InvoiceModel[];

  @Input() paymentTypes:PaymentTypeModel[];

  @Output()
  cancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private appDialogService: AppDialogService,
  ) {
    super();
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('invoices')) {
      this.loadData();
    }
  }

  back() {
    this.cancel.emit(true);
  }

  private loadData() {
    if (this.invoices) {
      this.gridData = this.invoices;
      if (this.gridData && this.gridData.length <= 0) {
        return;
      }
      this.state.filter = {
        logic: 'and',
        filters: [{field: 'status', operator: 'eq', value: 0}]
      };
      this.loadItems();
    }
  }
}
