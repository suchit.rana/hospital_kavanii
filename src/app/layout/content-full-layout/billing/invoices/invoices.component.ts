import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DxPopoverComponent } from 'devextreme-angular';
import { CreditType, InvoiceModel } from 'src/app/models/app.billing.model';
import { AppDialogService } from 'src/app/shared/services/app-dialog.service';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';
import { InvoicePreviewPopUpComponent } from '../invoice-preview-pop-up/invoice-preview-pop-up.component';
import { CreditLogsPopUpComponent } from '../credit-logs-pop-up/credit-logs-pop-up.component';
import { PaymentTypeModel } from '../../../../models/app.settings.model';
import { BillingService } from '../billing.service';
import { AppAlertService } from 'src/app/shared/services/app-alert.service';
import { CreditManagerPopUpComponent } from '../credit-manager/credit-manager-pop-up.component';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { SAVE } from '../../../../shared/popup-component/popup-component.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent extends BaseGridComponent implements OnInit {

    readonly PREVIEW = 'PREVIEW';
    readonly REFUND = 'REFUND';
    readonly EMAIL = 'EMAIL';
    readonly PRINT = 'PRINT';
    readonly EXPORT = 'EXPORT';
    readonly LOCK = 'LOCK';
    readonly UNLOCK = 'UNLOCK';
    readonly LOG = 'LOGS';

    readonly TYPE_CREDIT = CreditType.Credit;

    invoiceActionItem: InvoiceModel; //Item that was selected through action(GEAR ICON)

    @ViewChild('creditActionPopup', { static: true }) creditActionPopup: DxPopoverComponent;

    @Input() invoices: InvoiceModel[];

    @Input() paymentTypes: PaymentTypeModel[];

    @Input() patientId: any;

    @Input() patients: Array<any> = new Array<any>();

    @Input() voidedTab: boolean;

    @Output() loadData: EventEmitter<any> = new EventEmitter<any>();

    @BlockUI() blockUI: NgBlockUI;

    @Input() billingPage: boolean = false; // Determines whether it's from Billing Page or Patient Billing Page

    constructor(
        private appDialogService: AppDialogService,
        private alertService: AppAlertService,
        private billingService: BillingService,
        public router: Router
    ) {
        super();
    }

    ngOnInit(): void {
    }

    openActionPopup(dataItem: any, event: any) {
        this.invoiceActionItem = dataItem;

        // if(this.invoiceActionItem.status == CreditType.Credit &&  this.invoiceActionItem.refundCreditSummary.totalCreditBalance == 0){
        //   console.log("NON REFUND");
        //   this.invoiceActionPopup = this.nonRefundActionPopup;
        // }

        this.creditActionPopup.target = event.target;
        this.creditActionPopup.instance.show();
    }

    doAction(action: any) {
        if (this.creditActionPopup) {
            this.creditActionPopup.instance.hide();
        }
        if (!this.invoiceActionItem) {
            return;
        }

        switch (action) {
            case this.PREVIEW:
                this.openInvoicePreviewPopUp(this.invoiceActionItem);
                break;
            case this.REFUND:
                this.openRefundPopUp(this.invoiceActionItem);
                break;
            case this.EMAIL:
                break;
            case this.PRINT:
                break;
            case this.EXPORT:
                break;
            case this.LOCK:
                break;
            case this.UNLOCK:
                break;
            case this.LOG:
                this.openLogPopUp(this.invoiceActionItem);
                break;
        }
    }


    openLogPopUp(data: InvoiceModel) {
        this.appDialogService.open(CreditLogsPopUpComponent, {
            title: 'CREDIT LOG',
            width: '778px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
            hideSave: false,
            hideOnSave: false,
            hideFooter: true,
            data: {
                logs: data.logs
            }
        });
    }

    openInvoicePreviewPopUp(data: InvoiceModel) {
        this.appDialogService.open(InvoicePreviewPopUpComponent, {
            title: 'PREVIEW',
            width: '778px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
            hideSave: false,
            hideOnSave: false,
            hideFooter: true,
            data: {
                logs: data.logs
            }
        });
    }

    openRefundPopUp(data: InvoiceModel) {
        this.blockUI.start();
        this.billingService.getCredit(data.id).subscribe((res: any) => {
            this.blockUI.stop();
            const dialogRef = CreditManagerPopUpComponent.open(this.appDialogService, res, {
                isCredit: false,
                patientId: this.patientId,
                model: res,
                billingPage: this.billingPage
            }, true);
            dialogRef.close.subscribe((value) => {
                if (value === SAVE) {
                    this.loadData.emit();
                }
            });
        },
            error => {
                this.blockUI.stop();
                this.alertService.displayErrorMessage(error || 'There is an issue while fetching Credit Details');
            });
    }

    getAmountColor(data: InvoiceModel) {
        if (data.status === CreditType.Credit) {
            return 'green';
        } else {
            return 'red';
        }
    }

    onClickAddInvoice() {
        if (this.patientId) {
            this.router.navigate(['/patients/bills/add']);
        } else {
            this.router.navigate(['/bills/add']);
        }
    }
    onClickEditInvoice(id: string) {
        if (this.patientId) {
            this.router.navigate(['/patients/bills/edit', id]);
        } else {
            this.router.navigate(['/bills/edit', id]);
        }
    }
    onEditCreditClick(model: any) {
        this.blockUI.start();
        this.billingService.getCredit(model.id).subscribe((res: any) => {
            this.blockUI.stop();
            const dialogRef = CreditManagerPopUpComponent.open(this.appDialogService, res, {
                isCredit: model.status == CreditType.Credit,
                patientId: this.patientId,
                model: res,
                patients: this.patients,
                billingPage: this.billingPage
            }, model.status == CreditType.Refund);
            dialogRef.close.subscribe((value) => {
                if (value === SAVE) {
                    this.loadData.emit();
                }
            });
        },
            error => {
                this.blockUI.stop();
                this.alertService.displayErrorMessage(error || 'There is an issue while fetching Credit Details');
            });
    }
}
