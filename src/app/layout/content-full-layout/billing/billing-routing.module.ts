import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {BillingComponent} from './billing.component';
import {InvoiceFormComponent} from './invoice-form/invoice-form.component';
import {BillingMainComponent} from './billing-main/billing-main.component';

const routes: Routes = [
    {
        path: '',
        component: BillingComponent,
        children: [
            {
                path: 'add',
                component: InvoiceFormComponent,
                data: {
                    billingPage: true
                }
            },
            {
                path: 'edit/:invoiceId',
                component: InvoiceFormComponent,
                data: {
                    billingPage: true
                }
            },
            {
                path: '',
                component: BillingMainComponent,
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillingRoutingModule {
}
