import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePreviewPopUpComponent } from './invoice-preview-pop-up.component';

describe('InvoicePreviewPopUpComponent', () => {
  let component: InvoicePreviewPopUpComponent;
  let fixture: ComponentFixture<InvoicePreviewPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePreviewPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePreviewPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
