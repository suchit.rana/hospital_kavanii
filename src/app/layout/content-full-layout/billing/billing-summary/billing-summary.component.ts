import { Component, Input, OnInit } from '@angular/core';
import { BillingSummary } from 'src/app/models/app.billing.model';

@Component({
  selector: 'app-billing-summary',
  templateUrl: './billing-summary.component.html',
  styleUrls: ['./billing-summary.component.scss']
})
export class BillingSummaryComponent implements OnInit {

  @Input()
  model:BillingSummary;

  constructor() { }

  ngOnInit() {
  }

}
