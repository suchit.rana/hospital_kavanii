import { Component, Input, OnInit } from '@angular/core';
import { CreditLogModel } from 'src/app/models/app.billing.model';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';

@Component({
  selector: 'app-credit-logs',
  templateUrl: './credit-logs.component.html',
  styleUrls: ['./credit-logs.component.css']
})
export class CreditLogsComponent extends BaseGridComponent implements OnInit {

  @Input()
  logs:CreditLogModel[];

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
