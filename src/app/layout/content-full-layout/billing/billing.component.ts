import {Component, OnInit} from '@angular/core';
import {AppDialogService} from 'src/app/shared/services/app-dialog.service';
import {BillingService} from './billing.service';
import {CreateInvoiceBatchComponent} from './create-invoice-batch/create-invoice-batch.component';
import {AppState} from 'src/app/app.state';
import {BillingSummary, InvoiceModel} from 'src/app/models/app.billing.model';
import {PaymentTypeModel} from 'src/app/models/app.settings.model';
import {SettingsService} from 'src/app/services/app.settings.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {CreditManagerPopUpComponent} from './credit-manager/credit-manager-pop-up.component';
import {PatientService} from 'src/app/services/app.patient.service';
import {SAVE} from '../../../shared/popup-component/popup-component.component';

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {

    constructor() {

    }

    ngOnInit() {
    }


}
