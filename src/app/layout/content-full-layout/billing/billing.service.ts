import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppState } from 'src/app/app.state';
import { CreditModel } from 'src/app/models/app.billing.model';
import { BaseService } from 'src/app/services/app.base.service';

@Injectable({
  providedIn: 'root'
})
export class BillingService extends BaseService {

  constructor(public http: HttpClient, private appState: AppState) {
    super(http);
  }

  getBillingsByPatient(locationId: string, patientId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetAllPatientBillingByLocation?locationId=' + locationId + '&patientId=' + patientId);
  }

  getBillingsByLocation(locationId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetAllBillingByLocation?locationId=' + locationId);
  }

  getCredit(id: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetBillingCreditById?id=' + id);
  }

  addCredit(model: CreditModel) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/CreateBillingCredit', model);
  }

  updateCredit(model: CreditModel) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/UpdateBillingCredit', model);
  }

  deleteCredit(id: string) {
    return this.http.delete(this.environmentSettings.apiBaseUrl + '/RemoveBillingCredit?id=' + id);
  }

  getPatientBillingSummary(locationId: string, patientId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetPatientBillingSummaryByLocation?locationId=' + locationId + "&patientId=" + patientId);

  }
  getAllPatientInvoiceByLocation(locationId: string, patientId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetAllPatientInvoiceByLocation?locationId=' + locationId + "&patientId=" + patientId);

  }

  getAllInvoiceByLocation(locationId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetAllInvoiceByLocation?locationId=' + locationId);

  }

  getBillingSummary(locationId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetBillingSummaryByLocation?locationId=' + locationId);

  }
  getInvoiceById(invoiceId: string) {
    return this.http.get(this.environmentSettings.apiBaseUrl + '/GetInvoiceById?id=' + invoiceId);

  }

  createInvoice(model: any) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/CreateInvoice', model);
  }

  updateInvoice(model: any) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/UpdateInvoice', model);
  }
  createInvoicePayment(model: any) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/CreateInvoicePayment', model);
  }

  updateInvoicePayment(model: any) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/UpdateInvoicePayment', model);
  }

  deleteInvoice(invoiceId: string) {
    return this.http.delete(this.environmentSettings.apiBaseUrl + '/RemoveInvoice', {
      params: {
        id: invoiceId
      }
    });
  }
}
