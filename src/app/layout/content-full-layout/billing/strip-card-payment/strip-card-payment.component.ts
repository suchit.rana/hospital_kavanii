import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {StripeCardElementService} from './../../../../shared/services/stripe-card/stripe-card-element.service';

@Component({
    selector: 'app-strip-card-payment',
    templateUrl: './strip-card-payment.component.html',
    styleUrls: ['./strip-card-payment.component.css']
})
export class StripCardPaymentComponent implements OnInit {
    @ViewChild('cardInfo', {static: false}) cardInfo: ElementRef;

    card: any;
    stripe;
    cardHandler = this.onChange.bind(this);
    error: string;

    @Input() amount: any;
    @Input() patientId: string;
    @Input() billingPage: boolean;

    constructor(
        private cd: ChangeDetectorRef,
        private stripeElementService: StripeCardElementService) {
    }

    ngOnInit(): void {
        this.stripeElementService.setPublishableKey('pk_test_2syov9fTMRwOxYG97AAXbOgt008X6NL46o').then(
            stripe => {
                this.stripe = stripe;
                const elements = stripe.elements();
                this.card = elements.create('card');
                this.card.mount(this.cardInfo.nativeElement);
                this.card.addEventListener('change', this.cardHandler);
            });
    }

    onChange({error}) {
        if (error) {
            this.error = error.message;
        } else {
            this.error = null;
        }
        this.cd.detectChanges();
    }
}
