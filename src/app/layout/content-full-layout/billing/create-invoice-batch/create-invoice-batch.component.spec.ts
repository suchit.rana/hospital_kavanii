import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInvoiceBatchComponent } from './create-invoice-batch.component';

describe('CreateInvoiceBatchComponent', () => {
  let component: CreateInvoiceBatchComponent;
  let fixture: ComponentFixture<CreateInvoiceBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInvoiceBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInvoiceBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
