import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-invoice-batch',
  templateUrl: './create-invoice-batch.component.html',
  styleUrls: ['./create-invoice-batch.component.css']
})
export class CreateInvoiceBatchComponent implements OnInit {

  form:FormGroup = new FormGroup({
    fromDate:new FormControl(Validators.required),
    toDate:new FormControl(Validators.required),
    tpp:new FormControl(Validators.required),
    practitioner:new FormControl(Validators.required),
    batchNo:new FormControl(),
    batchNotes:new FormControl()
  });
  constructor() { }

  ngOnInit() {
  }

}
