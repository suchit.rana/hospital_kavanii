import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GridComponent } from '@progress/kendo-angular-grid';
import { BILLING_DATA, invoiceBatch } from 'src/app/models/app.billing.model';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';

@Component({
  selector: 'app-invoice-batches',
  templateUrl: './invoice-batches.component.html',
  styleUrls: ['./invoice-batches.component.css']
})
export class InvoiceBatchesComponent extends BaseGridComponent implements OnInit,AfterViewInit {

  @ViewChild(GridComponent, { static: false })
  public grid: GridComponent;

  batches:invoiceBatch[];

  constructor() {
    super();
  }

  ngOnInit() {
    this.batches = BILLING_DATA.Batches;
  }


  public ngAfterViewInit(): void {
  }

}
