import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { BillingComponent } from './billing.component';
import { BillingSummaryComponent } from './billing-summary/billing-summary.component';
import { FormsModule } from '@angular/forms';
import { MatListModule, MatFormFieldModule, MatIconModule, MatSelectModule, MatCheckboxModule, MatSidenavModule, MatToolbarModule, MatDividerModule, MatMenuModule, MatTooltipModule, MatExpansionModule, MatDatepickerModule, MatSlideToggleModule, MatTabsModule, MatRadioModule, MatCardModule, MatInputModule, MatTableModule, MatButtonModule, MatProgressSpinnerModule, MatButtonToggleModule, MatDialogModule, MatProgressBarModule, MatSliderModule, MatAutocompleteModule, MatNativeDateModule, MatBottomSheetModule, MatOptionModule } from '@angular/material';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule, CalendarModule, TimePickerModule } from '@progress/kendo-angular-dateinputs';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { EditorModule } from '@progress/kendo-angular-editor';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ProgressBarModule } from '@progress/kendo-angular-progressbar';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { CKEditorModule } from 'ckeditor4-angular';
import { DxDateBoxModule, DxCalendarModule, DxPopoverModule } from 'devextreme-angular';
import { BlockUIModule } from 'ng-block-ui';
import { NgImageFullscreenViewModule } from 'ng-image-fullscreen-view';
import { CharacterLimitModule } from 'src/app/directive/character-limit/character-limit.module';
import { AppTimePipeModule } from 'src/app/pipe/module/app-time-pipe.module';
import { ManagingModule } from '../../module/managing.module';
import { AppCommonModule } from '../common/app-common.module';
import { AppDatePickerModule } from '../common/app-date-picker/app-date-picker.module';
import { AppCalenderPopupModule } from '../common/calender-popup/app-calender-popup.module';
import { AppTimePickerModule } from '../common/time-picker/app-time-picker.module';
import { AddGeneralModule } from '../contacts/add-general/add-general.module';
import { AppKendoMultiSelectModule } from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import { SharedContentFullLayoutModule } from '../shared-content-full-layout/shared-content-full-layout.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { CreditLogsComponent } from './credit-logs/credit-logs.component';
import { CreditLogsPopUpComponent } from './credit-logs-pop-up/credit-logs-pop-up.component';
import { InvoicePreviewPopUpComponent } from './invoice-preview-pop-up/invoice-preview-pop-up.component';
import { AppDateDiffPipeModule } from 'src/app/pipe/module/app-date-diff-pipe.module';
import { OnlyNumberModule } from 'src/app/directive/only-number/only-number.module';
import { ApptDropDownAddNewModule } from '../appointment/schedular-main/schedular/shared/appt-drop-down-add-new/appt-drop-down-add-new.module';
import { AppointmentPopupModule } from '../patients/patient-appointments/appointment-popup/appointment-popup.module';
import { CreditManagerPopUpComponent } from './credit-manager/credit-manager-pop-up.component';
import { InvoiceBatchesComponent } from './invoice-batches/invoice-batches.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceAcceptPaymentPopUpComponent } from './invoice-accept-payment-pop-up/invoice-accept-payment-pop-up.component';
import { MergeInvoicesComponent } from './merge-invoices/merge-invoices.component';
import { CreateInvoiceBatchComponent } from './create-invoice-batch/create-invoice-batch.component';
import { StripCardPaymentComponent } from './strip-card-payment/strip-card-payment.component';
import { BillingMainComponent } from './billing-main/billing-main.component';
import {DropdownTreeViewModule} from '../../../shared/dropdown-treeview/dropdown-tree-view.module';


@NgModule({
  declarations: [
    BillingComponent,
    BillingSummaryComponent,
    InvoicesComponent,
    CreditLogsComponent,
    CreditLogsPopUpComponent,
    InvoicePreviewPopUpComponent,
    CreditManagerPopUpComponent,
    InvoiceBatchesComponent,
    InvoiceFormComponent,
    InvoiceAcceptPaymentPopUpComponent,
    MergeInvoicesComponent,
    CreateInvoiceBatchComponent,
    StripCardPaymentComponent,
    BillingMainComponent
  ],
  imports: [
    CommonModule,
    AppCommonModule,
    ManagingModule,
    SharedContentFullLayoutModule,
    AddGeneralModule,
    // SharedModule,
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDividerModule,
    MatMenuModule,
    MatListModule,
    MatTooltipModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatRadioModule,
    MatCardModule,
    // ColorPickerModule,
    FormsModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    DateInputsModule,
    CalendarModule,
    // IntlModule,
    TimePickerModule,
    InputsModule,
    MatButtonToggleModule,
    MatIconModule,
    BlockUIModule.forRoot(),
    ButtonsModule,
    MatDialogModule,
    // OrderModule,
    LayoutModule,
    InputsModule,
    DialogModule,
    MatProgressBarModule,
    NgImageFullscreenViewModule,
    DropDownsModule,
    ProgressBarModule,
    // TooltipModule,
    // EditorModule,
    // MaterialTimePickerModule,
    MatSliderModule,
    UploadsModule,
    LabelModule,
    CKEditorModule,
    DxDateBoxModule,
    DxCalendarModule,
    // DevExtremeModule,
    MatAutocompleteModule,
    AppCalenderPopupModule,
    AppTimePipeModule,
    AppTimePickerModule,
    AppDatePickerModule,
    CharacterLimitModule,
    TreeViewModule,
    PopupModule,
    EditorModule,
    PDFExportModule,
    DxPopoverModule,
    AppKendoMultiSelectModule,
    AppTimePipeModule,
    AppDateDiffPipeModule,
    OnlyNumberModule,
    ApptDropDownAddNewModule,
    AppointmentPopupModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BillingRoutingModule,
    DropdownTreeViewModule
  ],
  entryComponents:[
    CreditLogsComponent,
    CreditLogsPopUpComponent,
    InvoicePreviewPopUpComponent,
    CreditManagerPopUpComponent,
    InvoiceAcceptPaymentPopUpComponent,
    CreateInvoiceBatchComponent,
    StripCardPaymentComponent
  ],
  exports: [
    BillingSummaryComponent,
    InvoicesComponent,
    MergeInvoicesComponent,
    InvoiceFormComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BillingModule { }
