import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceAcceptPaymentPopUpComponent } from './invoice-accept-payment-pop-up.component';

describe('InvoiceAcceptPaymentPopUpComponent', () => {
  let component: InvoiceAcceptPaymentPopUpComponent;
  let fixture: ComponentFixture<InvoiceAcceptPaymentPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceAcceptPaymentPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceAcceptPaymentPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
