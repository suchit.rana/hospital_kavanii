import { Component, Input, OnInit, ChangeDetectorRef, ViewChild, ElementRef, Type } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentTypeModel } from 'src/app/models/app.settings.model';
import { AppState } from '../../../../app.state';
import { DialogRef } from '@progress/kendo-angular-dialog';
import { AppAlertService } from '../../../../shared/services/app-alert.service';
import { BillingService } from '../billing.service';
import { AppDialogService, IPopupAction } from '../../../../shared/services/app-dialog.service';
import { SettingsService } from '../../../../services/app.settings.service';
import { PatientService } from '../../../../services/app.patient.service';
import { StripeCardElementService } from '../../../../shared/services/stripe-card/stripe-card-element.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatFormFieldAppearance } from '@angular/material/form-field/typings/form-field';
import { CreditModel } from '../../../../models/app.billing.model';
import { AppoinmentDateUtils } from '../../appointment/AppoinmentDateUtils';

@Component({
  selector: 'app-invoice-accept-payment-pop-up',
  templateUrl: './invoice-accept-payment-pop-up.component.html',
  styleUrls: ['./invoice-accept-payment-pop-up.component.css']
})
export class InvoiceAcceptPaymentPopUpComponent implements OnInit {

  @Input() paymentTypes: PaymentTypeModel[];

  apperance = 'outline';

  formGroup: FormGroup = new FormGroup({
    payDate: new FormControl(Validators.required),
    paymentType: new FormControl(Validators.required),
    amount: new FormControl(Validators.required),
    notes: new FormControl()
  });

  static ref: IPopupAction;
  paymentHandler: any = null;

  @BlockUI()
  blockUI: NgBlockUI;

  @Input()
  invoiceId: string;


  @Input()
  billingPage: boolean = false; // Determines whether it's from Billing Page or Patient Billing Page

  @Input()
  model: any;

  appearance: MatFormFieldAppearance = 'outline';
  selectedPayType: string;
  patients: Array<any> = new Array<any>();


  constructor(
    private appState: AppState,
    private dialogRef: DialogRef,
    private alertService: AppAlertService,
    private billingService: BillingService,
    private cd: ChangeDetectorRef,
    private appDialogService: AppDialogService,
    private settingsService: SettingsService,
    private patientsService: PatientService,
    private stripeElementService: StripeCardElementService
  ) { }

  ngOnInit() {
    this.formGroup.get('payDate').setValue(AppoinmentDateUtils.toUtc(new Date()));
  }



  createInvoice() {
    this.blockUI.start();
    this.billingService.createInvoice(this.model).subscribe((val: any) => {

      this.blockUI.stop();
      if (val) {
        this.createInvoicePayment(val);
      }
    },
      () => {
        this.alertService.displayErrorMessage(
          'Error occurred while Creating Invoice, please try again.'
        );
        this.blockUI.stop();
      });
  }

  createInvoicePayment(invoiceId: string) {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      this.formGroup.updateValueAndValidity();
      return;
    }
    if (invoiceId) {
      let obj = this.formGroup.value;
      obj.invoiceId = invoiceId;
      this.billingService.createInvoicePayment(obj).subscribe((val: any) => {
        console.log(val);
        this.alertService.displaySuccessMessage('Invoice Payment Created Successfully');
        InvoiceAcceptPaymentPopUpComponent.ref.closeDialog("Save");
      })
    } else {
      this.createInvoice();
    }

  }

  static open(dialogService: AppDialogService, data: { [x: string]: any }): IPopupAction {
    this.ref = dialogService.open(InvoiceAcceptPaymentPopUpComponent, {
      title: 'Accept Payment',
      width: '778px',
      cancelBtnTitle: 'Cancel',
      saveBtnTitle: 'Save',
      hideSave: true,
      hideCancel: true,
      hideOnSave: false,
      hideFooter: true,
      height: 'auto',
      data: data
    });
    return this.ref;
  }

  cancel() {
    InvoiceAcceptPaymentPopUpComponent.ref.closeDialog();

  }
}
