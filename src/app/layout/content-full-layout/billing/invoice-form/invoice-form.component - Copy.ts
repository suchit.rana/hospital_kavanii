import {
    Component,
    NgZone,
    OnInit,
    Renderer2,
    ViewChild,
    OnDestroy,
    AfterViewInit,
    ViewEncapsulation,
    Output,
    EventEmitter
} from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { GridComponent, RowClassArgs } from '@progress/kendo-angular-grid';
import { PaymentTypeModel } from 'src/app/models/app.settings.model';
import { SettingsService } from 'src/app/services/app.settings.service';
import { AppDialogService } from 'src/app/shared/services/app-dialog.service';
import { InvoiceItemModel } from './../../../../models/app.billing.model';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';
import { ReminderConfiguration } from '../../settings/reminder-settings/interface';
import { AppState } from '../../../../app.state';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PatientService } from '../../../../services/app.patient.service';
import { PatientAppointmentService } from '../../patients/rest-service/app.patient-appointment.service';
import { StaffService } from '../../../../services/app.staff.service';
import { map } from 'rxjs/operators';
import {ContactService} from 'src/app/services/app.contact.service';

const tableRow = node => node.tagName.toLowerCase() === 'tr';

const closest = (node, predicate) => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }

    return node;
};


@Component({
    selector: 'app-invoice-form',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InvoiceFormComponent extends BaseGridComponent implements OnInit, AfterViewInit, OnDestroy {

    @Output()
    cancel: EventEmitter<any> = new EventEmitter<any>();
    invoiceId: string;
    paymentTypes: PaymentTypeModel[];
    isBillingPage: boolean = false;
    isPaid: boolean = false;
    allPatientList: any = [];
    selectedPatientAppointments: any = [];
    allPractitioners: any = [];
    selectedPatientCases: any = [];
    selectedCasePayers: any = [];
    selectedCaseReferrals: any = [];
    allConcessions: any = [];

    invoiceForm: FormGroup = new FormGroup({
        id: new FormControl(''),
        locationId: new FormControl(this.appState.selectedUserLocationId),
        patientId: new FormControl('', [Validators.required]),
        appointmentsEntryId: new FormControl('', [Validators.required]),
        appointmentDate: new FormControl(''),
        invoiceDate: new FormControl(new Date(), [Validators.required]),
        billTo: new FormControl('', [Validators.required]),
        practitionerId: new FormControl('', [Validators.required]),
        caseId: new FormControl('', [Validators.required]),
        referralId: new FormControl('', [Validators.required]),
        payerId: new FormControl('', [Validators.required]),
        concessionId: new FormControl('', [Validators.required]),
        invoiceNo: new FormControl(''),
        invoiceStatus: new FormControl(0),
        totalAmount: new FormControl(0),
        totalPaid: new FormControl(0),
        balance: new FormControl(0),
        isLock: new FormControl(false),
        notes: new FormControl(''),
        invoiceAutoId: new FormControl(0),
        invoiceItems: new FormArray([]),
        invoicePayment: new FormArray([]),
    });

    subTotal: number = 0;
    discount: number = 0;
    tax: number = 0;

    /**
     * Drag & Drop Rows
     * */
    public dropIndex;
    public noRecordsMsg = 'No records available yet. Please drop a row.';
    public targetCells: NodeListOf<HTMLTableDataCellElement>;

    constructor(
        public location: Location,
        private renderer: Renderer2,
        protected appState: AppState,
        protected route: ActivatedRoute,
        private settingsService: SettingsService,
        private appDialogService: AppDialogService,
        private patientService: PatientService,
        private contactService: ContactService,
        private patientAppointmentService: PatientAppointmentService,
        public staffService: StaffService
    ) {
        super();
        this.isBillingPage = route.snapshot.data['billingPage'];
    }

    ngOnInit() {
        const params = this.route.snapshot.params;
        this.invoiceId = params['invoiceId'];
        this.invoiceForm.get('invoiceItems').valueChanges.subscribe(value => {
            this.gridData = value;
            this.state.filter = null;
            this.loadItems();
        });
        this.addInvoiceItem();
        this.getPatientList();
        this.getAllPractitioners();
        this.getAllConcessions();
        this.getAllPayerOrReferral();
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {

    }

    getFormControl(dataItem: ReminderConfiguration, field: string): FormControl {
        // return the FormControl for the respective column editor
        return <FormControl>(
            (this.invoiceForm.get('invoiceItems') as FormArray).controls
                .find((i) => i.value.index === dataItem.index)
                .get(field)
        );
    }

    addInvoiceItem(index: number = -1) {
        if (index > -1) {
            (this.invoiceForm.get('invoiceItems') as FormArray).insert(index, this.invoiceItemFG());
        } else {
            (this.invoiceForm.get('invoiceItems') as FormArray).push(this.invoiceItemFG());
        }
        this.rearrangeIndex();
    }

    deleteRow(index: number) {
        (this.invoiceForm.get('invoiceItems') as FormArray).removeAt(index);
        this.rearrangeIndex();
    }

    public getAllRows(): Array<HTMLTableRowElement> {
        return Array.from(document.querySelectorAll('.k-grid tbody tr'));
    }

    // Prevents dragging header and 'no records' row.
    public onDragStart(e: DragEvent, index: number): void {
        index--;
        const draggedElement: string = (e.target as HTMLElement).innerText;
        if (draggedElement.includes('ID') || draggedElement === this.noRecordsMsg) {
            e.preventDefault();
        }

        const row = this.getAllRows()[index];
        const selectedItem = this.invoiceItems[index];
        const dataItem = JSON.stringify(selectedItem);
        e.dataTransfer.setData('text/plain', dataItem);
        try {
            e.dataTransfer.setDragImage(row, 0, 0);
        } catch (err) {
            // IE doesn't support setDragImage
        }
    }

    public onDrop(e: DragEvent): void {
        let data = e.dataTransfer.getData('text/plain');
        let droppedItem: ReminderConfiguration = JSON.parse(data);


        this.updateGridsData(droppedItem);

        this.removeLineIndicators();
    }

    public onDragOver(e: DragEvent): void {
        e.preventDefault();
        if (this.targetCells) {
            this.removeLineIndicators();
        }

        const targetEl = e.target as HTMLElement;
        if (
            (targetEl.tagName === 'TD' || targetEl.tagName === 'TH')
        ) {
            // Set drop line indication
            this.targetCells = targetEl.parentElement.querySelectorAll('td, th');
            this.targetCells.forEach((td: HTMLElement) => {
                const gridData: any[] = this.invoiceItems;
                if (td.tagName === 'TH' && gridData.length !== 0) {
                    this.renderer.addClass(td, 'th-line');
                    this.dropIndex = 0;
                } else if (td.tagName === 'TD') {
                    this.renderer.addClass(td, 'td-line');
                    this.dropIndex = closest(e.target, tableRow).rowIndex + 1;
                }
            });
        }
    }

    public removeLineIndicators() {
        this.targetCells.forEach((td: HTMLElement) => {
            this.renderer.removeAttribute(td, 'class');
        });
    }

    public updateGridsData(droppedItem: ReminderConfiguration): void {
        let index: number = droppedItem.index;
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(index);
        (this.invoiceForm.get('invoiceItems') as FormArray).removeAt(index);
        (this.invoiceForm.get('invoiceItems') as FormArray).insert(this.dropIndex - 1, fg);
        this.rearrangeIndex();
    }

    back() {
        if (this.isBillingPage) {
            this.location.back();
        }
        this.cancel.emit(true);
    }

    get invoiceItems(): InvoiceItemModel[] {
        return (this.invoiceForm.get('invoiceItems') as FormArray).value;
    }

    private invoiceItemFG(): FormGroup {
        return new FormGroup({
            id: new FormControl(),
            invoiceId: new FormControl(),
            staffId: new FormControl(),
            offeringType: new FormControl(0),
            offeringId: new FormControl(),
            qtyOrDuration: new FormControl(0),
            discountType: new FormControl(0),
            discount: new FormControl(0),
            amount: new FormControl(0),
            seqNo: new FormControl(0),
            isVoid: new FormControl(false),
            actionType: new FormControl(0),
            taxRate: new FormControl(0),
            /*Need To Add*/
            due: new FormControl(0),

            /*Extra Controls*/
            index: new FormControl(this.invoiceItems.length),
            fees: new FormControl()
        });
    }

    private invoicePaymentFG(): FormGroup {
        return new FormGroup({
            id: new FormControl(''),
            invoiceId: new FormControl(''),
            paymentNo: new FormControl(''),
            payDate: new FormControl(''),
            payType: new FormControl(''),
            chequeNo: new FormControl(''),
            bankBranch: new FormControl(''),
            referenceNo: new FormControl(''),
            payerName: new FormControl(''),
            amount: new FormControl(0),
            notes: new FormControl(''),
            isVoid: new FormControl(false),
            paymentAutoId: new FormControl(),
            createdBy: new FormControl(''),
            createdDate: new FormControl(''),
            modifiedBy: new FormControl(''),
            modifiedDate: new FormControl(''),
            actionType: new FormControl(0)
        });
    }

    private rearrangeIndex() {
        (this.invoiceForm.get('invoiceItems') as FormArray).controls.forEach((fg, index) => {
            fg.get('index').setValue(index);
        });
    }

    public getPatientList() {
        this.patientService.getAllPatientsByLocationId(this.appState.selectedUserLocationId).subscribe((data) => {
            if (data.length != 0) {
                this.allPatientList = data;
                console.log(this.allPatientList);

            }
        });
    }

    public onChangePatient(event: any) {
        console.log(event);
        this.selectedPatientAppointments = [];
        this.selectedPatientCases = [];
        if (event) {
            this.patientAppointmentService.get(event).subscribe((data: any) => {
                if (data.length != 0) {
                    this.selectedPatientAppointments = data.gridData;
                    console.log(this.selectedPatientAppointments);

                }
            });

            this.getAllPatientCases(event);
        }
    }

    public getAllPractitioners() {
        this.staffService.getAllPractitonersByLocationId(this.appState.selectedUserLocationId).subscribe((data) => {
            if (data.length != 0) {
                data.forEach((element: any) => {
                    element.patientSearchName = element.title + ' ' + element.firstName;
                    this.allPractitioners.push(element);
                });

            }
        });
    }
    
    public getAllPatientCases(patientId: string) {
        this.patientService.getAllCasesByLocationAndPatient(this.appState.selectedUserLocationId, patientId).subscribe((data) => {
            if (data.length != 0) {
                this.selectedPatientCases = data;
                console.log(this.selectedPatientCases);
            }
        });
    }
    
    public getAllConcessions() {
        this.settingsService.getAllConcessions()
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.isStatus) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe(data => {
                this.allConcessions = data;
            });
    }

    public getAllPayerOrReferral(){
        this.contactService.getAllContact().subscribe((data) => {
            console.log(data,"dataRRRRRRRRRRRR")
            if (data.length != 0) {
                this.selectedCasePayers.push(data);
                console.log(this.selectedCasePayers);
            }
        })
    }

}
