import {
    Component,
    NgZone,
    OnInit,
    Renderer2,
    ViewChild,
    OnDestroy,
    AfterViewInit,
    ViewEncapsulation,
    Output,
    EventEmitter
} from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { GridComponent, RowClassArgs } from '@progress/kendo-angular-grid';
import { PaymentTypeModel } from 'src/app/models/app.settings.model';
import { SettingsService } from 'src/app/services/app.settings.service';
import { AppDialogService } from 'src/app/shared/services/app-dialog.service';
import { InvoiceItemModel } from './../../../../models/app.billing.model';
import { BaseGridComponent } from '../../shared-content-full-layout/base-grid/base-grid.component';
import { ReminderConfiguration } from '../../settings/reminder-settings/interface';
import { AppState } from '../../../../app.state';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { PatientService } from '../../../../services/app.patient.service';
import { PatientAppointmentService } from '../../patients/rest-service/app.patient-appointment.service';
import { StaffService } from '../../../../services/app.staff.service';
import { AppointmentService } from '../../../../services/app.appointment.service';
import { ContactService } from '../../../../services/app.contact.service';
import { map } from 'rxjs/operators';
import { OfferingsService } from '../../../../services/app.offerings.service';
import { BillingService } from '../billing.service';
import { AppAlertService } from '../../../../shared/services/app-alert.service';
import { AppUtils } from '../../../../shared/AppUtils';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { AppoinmentDateUtils } from '../../appointment/AppoinmentDateUtils';
import { InvoiceAcceptPaymentPopUpComponent } from '../invoice-accept-payment-pop-up/invoice-accept-payment-pop-up.component';
import { SAVE } from '../../../../shared/popup-component/popup-component.component';
import { Input } from '@angular/core';
import { DropDownTreeComponent } from '@progress/kendo-angular-dropdowns';
import { DeleteConfirmationDialogComponent } from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import { FromPage } from 'src/app/shared/interface';
import '../../settings/lettertemplate/lettertemplate.component.css';

const tableRow = node => node.tagName.toLowerCase() === 'tr';

const closest = (node, predicate) => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }

    return node;
};


@Component({
    selector: 'app-invoice-form',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InvoiceFormComponent extends BaseGridComponent implements OnInit, AfterViewInit, OnDestroy {

    @Output()
    cancel: EventEmitter<any> = new EventEmitter<any>();
    @BlockUI() blockUI: NgBlockUI;
    invoiceId: string;

    @Input()
    patientId: string;

    paymentTypes: PaymentTypeModel[];

    @Input()
    isBillingPage: boolean;
    isPaid: boolean = false;
    allPatientList: any = [];
    selectedPatientAppointments: any = [];
    allPractitioners: any = [];
    selectedPatientCases: any = [];
    selectedCasePayers: any = [];
    selectedCaseReferrals: any = [];
    selectedPatientBillingSummary: any = [];
    allOfferings: any[] = [];

    allConcessions: any = [];
    allStaffs: any = [];
    allDiscountTypes: any = [];
    allTaxes: any = [];

    @ViewChild('dTreeView', { static: false }) dTreeView: DropDownTreeComponent;

    allBillTo: any = [
        { id: 'Patient', name: 'Patient' },
        { id: 'Insurer', name: 'Insurer' },
        { id: 'Employer', name: 'Employer' },
        { id: 'Referrer', name: 'Referrer' },
        { id: 'Medicare(PCI \ BB)', name: 'Medicare(PCI \ BB)' },
        { id: 'DVA(Allied \ Paperless)', name: 'DVA(Allied \ Paperless)' },
    ]

    invoiceForm: FormGroup = new FormGroup({
        id: new FormControl(''),
        locationId: new FormControl(this.appState.selectedUserLocationId),
        patientId: new FormControl(''),
        appointmentsEntryId: new FormControl(''),
        appointmentDate: new FormControl(),
        invoiceDate: new FormControl(AppoinmentDateUtils.toUtc(new Date()), Validators.required),
        dueDate: new FormControl(AppoinmentDateUtils.toUtc(new Date())),
        billTo: new FormControl('', [Validators.required]),
        practitionerId: new FormControl('', [Validators.required]),
        caseId: new FormControl('', [Validators.required]),
        referralId: new FormControl('', [Validators.required]),
        payerId: new FormControl('', [Validators.required]),
        concessionId: new FormControl('', [Validators.required]),
        invoiceNo: new FormControl(),
        invoiceStatus: new FormControl(0),
        invoiceStatusText: new FormControl(0),
        totalAmount: new FormControl(0),
        totalPaid: new FormControl(0),
        balance: new FormControl(0),
        isLock: new FormControl(false),
        notes: new FormControl(''),
        invoiceAutoId: new FormControl(0),
        invoiceItems: new FormArray([]),
        invoicePayment: new FormArray([]),
        createdBy: new FormControl(''),
        createdDate: new FormControl(''),
        modifiedBy: new FormControl(''),
        modifiedDate: new FormControl(''),
    });

    subTotal: number = 0;
    discount: number = 0;
    tax: number = 0;

    /**
     * Drag & Drop Rows
     * */
    public dropIndex;
    public noRecordsMsg = 'No records available yet. Please drop a row.';
    public targetCells: NodeListOf<HTMLTableDataCellElement>;

    constructor(
        public location: Location,
        private renderer: Renderer2,
        protected appState: AppState,
        protected route: ActivatedRoute,
        private settingsService: SettingsService,
        private appDialogService: AppDialogService,
        private patientService: PatientService,
        private patientAppointmentService: PatientAppointmentService,
        private appointnentService: AppointmentService,
        public staffService: StaffService,
        public contactService: ContactService,
        public offeringsService: OfferingsService,
        public billingService: BillingService,
        public alertService: AppAlertService,
        public appRouter: Router
    ) {
        super();
        this.isBillingPage = route.snapshot.data['billingPage'];
        this.getAllServices();
        this.getProducts();
        this.getAllClasses();
        this.getPatientList();
        this.getAllPractitioners();
        this.getAllConcessions();
        this.getAllStaffs();
        this.getAllContacts();
        this.getDiscounTypes();
        this.getAllTaxes();
       
        this.getAllPayementTypes();
    }

    ngOnInit() {

        const params = this.route.snapshot.params;
        this.invoiceId = params['invoiceId'];
        // this.invoiceForm.get('invoiceDate').setValue(new Date());

        if (this.isBillingPage) {
            this.invoiceForm.get('patientId').setValidators([Validators.required]);
            this.invoiceForm.get('appointmentsEntryId').setValidators([Validators.required]);

        } else {

        }
        this.invoiceForm.get('invoiceItems').valueChanges.subscribe(value => {
            this.gridData = value;
            this.state.filter = null;
            this.loadItems();
        });

        

        if (this.invoiceId) {
            this.getInvoiceById(this.invoiceId);
        } else {
            this.addInvoiceItem();
            console.log("PatientID", this.patientId)
            if (this.patientId) {

                this.onChangePatient(this.patientId);
            }
        }
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {
        this.invoiceForm.reset();
    }

    getFormControl(dataItem: ReminderConfiguration, field: string): FormControl {
        // return the FormControl for the respective column editor

        return <FormControl>(
            (this.invoiceForm.get('invoiceItems') as FormArray).controls
                .find((i) => i.value.index === dataItem.index)
                .get(field)
        );
    }

    addInvoiceItem(index: number = -1) {
        if (index > -1) {
            (this.invoiceForm.get('invoiceItems') as FormArray).insert(index, this.invoiceItemFG());
        } else {
            (this.invoiceForm.get('invoiceItems') as FormArray).push(this.invoiceItemFG());
        }
        this.rearrangeIndex();
    }

    deleteRow(index: number) {
        (this.invoiceForm.get('invoiceItems') as FormArray).removeAt(index);
        this.rearrangeIndex();
        this.calculateinvoiceAmount();
    }

    public getAllRows(): Array<HTMLTableRowElement> {
        return Array.from(document.querySelectorAll('.k-grid tbody tr'));
    }

    // Prevents dragging header and 'no records' row.
    public onDragStart(e: DragEvent, index: number): void {
        index--;
        const draggedElement: string = (e.target as HTMLElement).innerText;
        if (draggedElement.includes('ID') || draggedElement === this.noRecordsMsg) {
            e.preventDefault();
        }

        const row = this.getAllRows()[index];
        const selectedItem = this.invoiceItems[index];
        const dataItem = JSON.stringify(selectedItem);
        e.dataTransfer.setData('text/plain', dataItem);
        try {
            e.dataTransfer.setDragImage(row, 0, 0);
        } catch (err) {
            // IE doesn't support setDragImage
        }
    }

    public onDrop(e: DragEvent): void {
        let data = e.dataTransfer.getData('text/plain');
        let droppedItem: ReminderConfiguration = JSON.parse(data);


        this.updateGridsData(droppedItem);

        this.removeLineIndicators();
    }

    public onDragOver(e: DragEvent): void {
        e.preventDefault();
        if (this.targetCells) {
            this.removeLineIndicators();
        }

        const targetEl = e.target as HTMLElement;
        if (
            (targetEl.tagName === 'TD' || targetEl.tagName === 'TH')
        ) {
            // Set drop line indication
            this.targetCells = targetEl.parentElement.querySelectorAll('td, th');
            this.targetCells.forEach((td: HTMLElement) => {
                const gridData: any[] = this.invoiceItems;
                if (td.tagName === 'TH' && gridData.length !== 0) {
                    this.renderer.addClass(td, 'th-line');
                    this.dropIndex = 0;
                } else if (td.tagName === 'TD') {
                    this.renderer.addClass(td, 'td-line');
                    this.dropIndex = closest(e.target, tableRow).rowIndex + 1;
                }
            });
        }
    }

    public removeLineIndicators() {
        this.targetCells.forEach((td: HTMLElement) => {
            this.renderer.removeAttribute(td, 'class');
        });
    }

    public updateGridsData(droppedItem: ReminderConfiguration): void {
        let index: number = droppedItem.index;
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(index);
        (this.invoiceForm.get('invoiceItems') as FormArray).removeAt(index);
        (this.invoiceForm.get('invoiceItems') as FormArray).insert(this.dropIndex - 1, fg);
        this.rearrangeIndex();
    }

    back() {
        if (this.isBillingPage) {
            this.location.back();
        }
        this.cancel.emit(true);
    }

    get invoiceItems(): InvoiceItemModel[] {
        return (this.invoiceForm.get('invoiceItems') as FormArray).value;
    }

    private invoiceItemFG(): FormGroup {
        return new FormGroup({
            id: new FormControl(),
            invoiceId: new FormControl(),
            staffId: new FormControl('', [Validators.required]),
            offeringType: new FormControl(0),
            offeringId: new FormControl('', [Validators.required]),
            qtyOrDuration: new FormControl(1, [Validators.required]),
            discountType: new FormControl(0),
            discount: new FormControl(0),
            amount: new FormControl(0),
            seqNo: new FormControl(0),
            isVoid: new FormControl(false),
            actionType: new FormControl(0),
            taxRate: new FormControl(0),
            /*Need To Add*/
            due: new FormControl(0, [Validators.required]),

            /*Extra Controls*/
            index: new FormControl(this.invoiceItems.length),
            fees: new FormControl(null, [Validators.required]),
            createdBy: new FormControl(''),
            createdDate: new FormControl(''),
            modifiedBy: new FormControl(''),
            modifiedDate: new FormControl(''),
        });
    }

    private invoicePaymentFG(): FormGroup {
        return new FormGroup({
            id: new FormControl(''),
            invoiceId: new FormControl(''),
            paymentNo: new FormControl(''),
            payDate: new FormControl(''),
            payType: new FormControl(''),
            chequeNo: new FormControl(''),
            bankBranch: new FormControl(''),
            referenceNo: new FormControl(''),
            payerName: new FormControl(''),
            amount: new FormControl(0),
            notes: new FormControl(''),
            isVoid: new FormControl(false),
            paymentAutoId: new FormControl(),
            createdBy: new FormControl(''),
            createdDate: new FormControl(''),
            modifiedBy: new FormControl(''),
            modifiedDate: new FormControl(''),
            actionType: new FormControl(0)
        });
    }

    private rearrangeIndex() {
        (this.invoiceForm.get('invoiceItems') as FormArray).controls.forEach((fg, index) => {
            fg.get('index').setValue(index);
        });
    }

    public getPatientList() {
        this.patientService.getAllPatientsByLocationId(this.appState.selectedUserLocationId).subscribe((data) => {
            if (data.length != 0) {
                this.allPatientList = data;
                console.log(this.allPatientList);

            }
        });
    }

    public onChangePatient(event: any) {
        console.log(event);
        this.patientId = event;
        this.selectedPatientAppointments = [];
        this.selectedPatientCases = [];
        this.selectedPatientBillingSummary = [];
        if (event) {
            this.appointnentService.GetAllPatientAppointmentInvoiceByLocation(this.appState.selectedUserLocationId, event).subscribe((data: any) => {
                if (data.length != 0) {

                    this.selectedPatientAppointments = data.filter(val => val).map(val => {
                        return { id: val.id, startDateUTC: AppoinmentDateUtils.formatDateTime(val.startDateUTC, 'DD-MMM-yyyy @ HH:mm A', false) }
                    });;
                    console.log(this.selectedPatientAppointments);

                }
            });

            this.getAllPatientCases(event);
            this.getPatientBillngSummary(event);
        }
    }
    public getAllPractitioners() {
        this.staffService.getAllPractitonersByLocationId(this.appState.selectedUserLocationId).subscribe((data) => {
            if (data.length != 0) {
                data.forEach((element: any) => {
                    element.patientSearchName = element.title + ' ' + element.firstName;
                    this.allPractitioners.push(element);
                });

            }
        });
    }
    public getAllStaffs() {
        this.staffService.getAllStaffsList().subscribe((data: any[]) => {
            this.allStaffs = data.filter(item => {
                if (item.isStatus) {
                    item['name'] = item.firstName + ' ' + item.lastName;
                    return item;
                }
            });
            console.log(this.allStaffs);

        });
    }
    public getAllPatientCases(patientId: string) {
        this.patientService.getAllCasesByLocationAndPatient(this.appState.selectedUserLocationId, patientId).subscribe((data) => {
            if (data.length != 0) {
                this.selectedPatientCases = data;
                console.log(this.selectedPatientCases);
            }
        });
    }
    public getAllContacts() {
        this.contactService.getAllContact().subscribe((data) => {
            if (data.length != 0) {
                this.selectedCasePayers = data;
                this.selectedCaseReferrals = data;
                console.log(data);
            }
        });
    }
    public getDiscounTypes() {
        this.settingsService.getAllDiscountTypes().subscribe((data) => {
            if (data.length != 0) {
                this.allDiscountTypes = data;
                console.log(this.allDiscountTypes);
            }
        });
    }
    public getAllTaxes() {
        this.settingsService.getAllTaxes().subscribe((data) => {
            if (data.length != 0) {
                this.allTaxes = data;
                console.log(this.allTaxes);
            }
        });
    }
    public getPatientBillngSummary(patientId: string) {
        this.billingService.getAllPatientInvoiceByLocation(this.appState.selectedUserLocationId
            , patientId).subscribe((data: any) => {
                if (data.length != 0) {
                    this.selectedPatientBillingSummary = data;
                    console.log(this.selectedPatientBillingSummary);
                }
            });
    }
    public  getAllServices() {

        this.offeringsService.getAllService().subscribe((data: any[]) => {
            if (data.length != 0) {
                // const obj = {
                //     name: "Service",
                //     items: data.filter(val => val).map(val => {
                //         return { name: val.id, id: val.id };
                //     }),
                // };

                const d = data.filter(val => val).map(val => {
                    val['name'] = val.serviceName;
                    val['type'] = 'Service';
                    return val
                })
                const obj: { name: string, items: any[] } = {
                    name: "Service",
                    items: d
                };
                const list = [...this.allOfferings];
                list.push(obj);
                this.allOfferings = list;
                console.log(typeof data);

                console.log('Offerings-Service', typeof this.allOfferings);


            }
        });
    }
    public getProducts() {
        this.offeringsService.getAllProducts().subscribe((data: any) => {
            if (data.length != 0) {

                const d = data.filter(val => val).map(val => {
                    val['name'] = val.productName;
                    val['type'] = 'Product';
                    return val
                })
                const obj: { name: string, items: any[] } = {
                    name: "Product",
                    items: d
                };
                const list = [...this.allOfferings];
                list.push(obj);
                this.allOfferings = list;

                // data.filter(val => {
                //     const obj: any = { name: val.productName, id: val.id, type: 'Product' };
                //     val['name'] = val.productName;

                // });

                // this.allOfferings = data;
                // console.log('Data', data);
                console.log('Offerings-Products', this.allOfferings);


            }
        });
    }
    public getAllClasses() {
        this.offeringsService.getAllClasses().subscribe((data: any[]) => {
            if (data.length != 0) {


                const d = data.filter(val => val).map(val => {
                    val['name'] = val.c_Name;
                    val['type'] = 'Class';
                    return val
                })
                const obj: { name: string, items: any[] } = {
                    name: "Class",
                    items: d
                };
                const list = [...this.allOfferings];
                list.push(obj);
                this.allOfferings = list;
                // data.filter(val => {
                //     const obj: any = { name: val.c_Name, id: val.id, type: 'Class' };
                //     this.allOfferings.push(obj);
                // });
                console.log('Offerings-Classes', this.allOfferings);

            }
        });
    }

    public getAllConcessions() {
        this.settingsService.getAllConcessions()
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.isStatus) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe(data => {
                this.allConcessions = data;
            });
    }

    public onSelectOffering(data: any, index: any, obj) {
        console.log(data)
        console.log(index);
        const obj1 = data;
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(obj.index);
        let fees = 0;
        switch (data.type) {
            case "Product":
                fees = obj1.salePrice;
                fg.get('fees').setValue(obj1.salePrice);
                // fg.get('offeringId').setValue(obj1.id);
                // fg.get('offer').setValue(obj1);

                break;
            case "Service":
            case "Class":
                fees = obj1.standardPrice;
                fg.get('fees').setValue(obj1.standardPrice);
                // fg.get('offeringId').setValue(obj1.id);
                // fg.get('offer').setValue(obj1);
                break;

            default:
                break;
        }

        const qty = fg.get('qtyOrDuration').value;
        const amt = fees * qty;
        fg.get('amount').setValue(amt)
        const taxRateObj = this.allTaxes.find(val => val.id === obj1.taxTypeId);
        fg.get('taxRate').setValue(taxRateObj ? taxRateObj.taxRate : 0);
        const dueAmt = +amt + (taxRateObj ? taxRateObj.taxRate : 0);
        fg.get('due').setValue(dueAmt);
        console.log(fg);
        console.log(obj1);

        console.log(obj);
        this.calculateinvoiceAmount();
    }
    public onSelectDiscount(data: number, index: any, obj: any) {
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(obj.index);
        fg.get('due').setValue(0);
        const amt = fg.get('amount').value;
        const taxRate = fg.get('taxRate').value;
        const fee = fg.get('fees').value;

        const discount = amt * data / 100;
        const dueAmt = (+amt + +taxRate) - discount;

        fg.get('amount').setValue(dueAmt);
        fg.get('due').setValue(dueAmt);
        this.calculateinvoiceAmount();
    }

    public onChangeFee(event: any, obj: any) {
        const value = event.target.value;
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(obj.index);
        const qty = fg.get('qtyOrDuration').value;
        const amt = value * qty;
        const discount = fg.get('discount').value;
        const taxRate = fg.get('taxRate').value;
        let dueAmt = +amt + +taxRate;
        if (discount) {
            const discAmt = (dueAmt * +discount) / 100;
            dueAmt -= discAmt;
        }

        fg.get('amount').setValue(dueAmt);
        fg.get('due').setValue(dueAmt);
        this.calculateinvoiceAmount();

    }
    public onChangeQty(event: any, obj: any) {
        const value = event.target.value
        console.log(value);
        const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(obj.index);
        const amt = +fg.get('fees').value * +value;
        const taxRate = fg.get('taxRate').value;
        const dueAmt = +amt + +taxRate;
        fg.get('amount').setValue(amt);
        fg.get('due').setValue(dueAmt);

        this.calculateinvoiceAmount();
    }

    public calculateinvoiceAmount() {
        this.subTotal = 0;
        this.tax = 0;
        this.discount = 0;
        this.invoiceForm.get('totalAmount').setValue(0);

        const items = (this.invoiceForm.get('invoiceItems') as FormArray).value;
        items.filter(val => {
            this.tax += val.taxRate;
            let discAmt = 0;
            if (val.discount) {
                discAmt = (val.fees * val.qtyOrDuration) * val.discount / 100;
                this.discount += discAmt;
            }
            this.subTotal += val.fees * val.qtyOrDuration - discAmt;

        });
        const total = this.subTotal + this.tax;
        this.invoiceForm.get('totalAmount').setValue(total);

    }
    public saveOrUpdateInvoice() {
        if (this.invoiceForm.invalid) {
            this.invoiceForm.markAllAsTouched();
            this.invoiceForm.updateValueAndValidity();
            return;
        }
        console.log(this.invoiceForm.get('invoiceDate').value);

        const el = document.getElementById('heading');
        const items = (this.invoiceForm.get('invoiceItems') as FormArray).value;
        // items.filter(val => { delete val.index; })
        items.filter(val => {
            if (typeof val.offeringId == "object") { val.offeringId = val.offeringId.id } delete val.index;
            if (!this.invoiceId) {
                delete val.id;
                delete val.invoiceId;
            }
        })
        const req = {
            "id": this.invoiceId,
            "parentBusinessId": this.appState.userProfile.parentBusinessId,
            "locationId": this.appState.selectedUserLocationId,
            "patientId": this.invoiceForm.get('patientId').value,
            "appointmentsEntryId": this.invoiceForm.get('appointmentsEntryId').value,
            "billTo": this.invoiceForm.get('billTo').value,
            // "appointmentDate": this.invoiceForm.get('appointmentDate').value,
            "invoiceDate": this.invoiceForm.get('invoiceDate').value ? this.invoiceForm.get('invoiceDate').value : new Date(),
            "dueDate": this.invoiceForm.get('invoiceDate').value ? this.invoiceForm.get('invoiceDate').value : new Date(),
            "practitionerId": this.invoiceForm.get('practitionerId').value,
            "caseId": this.invoiceForm.get('caseId').value,
            "referralId": this.invoiceForm.get('referralId').value,
            "payerId": this.invoiceForm.get('payerId').value,
            "concessionId": this.invoiceForm.get('concessionId').value,
            "invoiceNo": this.invoiceForm.get('invoiceNo').value,
            "invoiceStatus": this.invoiceForm.get('invoiceStatus').value,
            "invoiceStatusText": this.invoiceForm.get('invoiceStatusText').value,
            "totalAmount": this.invoiceForm.get('totalAmount').value,
            "totalPaid": this.invoiceForm.get('totalPaid').value,
            "balance": this.invoiceForm.get('balance').value,
            "isLock": this.invoiceForm.get('isLock').value,
            "notes": this.invoiceForm.get('notes').value,
            "invoiceAutoId": this.invoiceForm.get('invoiceAutoId').value,
            "invoiceItems": items,
            "invoicePayment": {}
        }
        console.log(req);

        AppUtils.customJsonInclude(req);
        req["invoicePayment"] = {}
        this.blockUI.start();
        if (this.invoiceId) {
            this.billingService.updateInvoice(req).subscribe(() => {
                this.alertService.displaySuccessMessage('Invoice updated successfully.');
                // this.patientService.sharedData = 'invoice updated successfully.';
                // el.scrollIntoView();
                this.blockUI.stop();
                this.redirectPageWise();
            },
                () => {
                    this.alertService.displayErrorMessage(
                        'Error occurred while updating Invoice, please try again.'
                    );
                    el.scrollIntoView();
                    this.blockUI.stop();
                });
        } else {

            this.billingService.createInvoice(req).subscribe(() => {
                this.alertService.displaySuccessMessage('Invoice created successfully.');
                // this.patientService.sharedData = 'invoice created successfully.';
                el.scrollIntoView();
                this.blockUI.stop();
                this.redirectPageWise();
            },
                () => {
                    this.alertService.displayErrorMessage(
                        'Error occurred while creating Invoice, please try again.'
                    );
                    el.scrollIntoView();
                    this.blockUI.stop();
                });
        }

    }

    getInvoiceById(id: string) {
        this.billingService.getInvoiceById(id).subscribe((data: any) => {


            if (data) {

                // if (!this.isBillingPage) {
                this.onChangePatient(data.patientId);
                // }
                // const req = {
                //     // "id": this.invoiceId,
                //     // "locationId": this.appState.selectedUserLocationId,
                //     // "invoiceItems": [],
                //     "invoiceItems": items,
                //     "invoicePayment": {},
                // }
                this.invoiceForm.get('patientId').setValue(data.patientId)

                this.invoiceForm.get('billTo').setValue(data.billTo ? data.billTo : '')
                this.invoiceForm.get('appointmentDate').setValue(data.appointmentDate)
                this.invoiceForm.get('invoiceDate').setValue(data.invoiceDate)
                this.invoiceForm.get('dueDate').setValue(data.dueDate)
                this.invoiceForm.get('practitionerId').setValue(data.practitionerId)
                this.invoiceForm.get('caseId').setValue(data.caseId)
                this.invoiceForm.get('referralId').setValue(data.referralId)
                this.invoiceForm.get('payerId').setValue(data.payerId)
                this.invoiceForm.get('concessionId').setValue(data.concessionId)
                this.invoiceForm.get('invoiceNo').setValue(data.invoiceNo)
                this.invoiceForm.get('invoiceStatus').setValue(data.invoiceStatus)
                this.invoiceForm.get('invoiceStatusText').setValue(data.invoiceStatusText)
                this.invoiceForm.get('totalAmount').setValue(data.totalAmount)
                this.invoiceForm.get('totalPaid').setValue(data.totalPaid)
                this.invoiceForm.get('balance').setValue(data.balance)
                this.invoiceForm.get('isLock').setValue(data.isLock)
                this.invoiceForm.get('notes').setValue(data.notes)
                this.invoiceForm.get('invoiceAutoId').setValue(data.invoiceAutoId)
                this.invoiceForm.get('createdBy').setValue(data.createdBy)
                this.invoiceForm.get('createdDate').setValue(data.createdDate)
                this.invoiceForm.get('modifiedBy').setValue(data.modifiedBy)
                this.invoiceForm.get('modifiedDate').setValue(data.modifiedDate)
                this.invoiceForm.get('invoicePayment').setValue([])
                this.invoiceForm.get('appointmentsEntryId').setValue(data.appointmentsEntryId)
                const items = data.invoiceItems.filter(val => val).map((val, index: any) => {
                    val['index'] = index;
                    val['due'] = val['due'] ? val['due'] : 0;
                    val['fees'] = +val['amount'] / +val['qtyOrDuration'];
                    this.allOfferings.filter(offer => {
                        const obj = offer.items.find(ob => ob.id == val['offeringId']);
                        if (obj) {
                            val['offeringId'] = obj;
                        }
                    });
                    this.addInvoiceItem();
                    const fg = (this.invoiceForm.get('invoiceItems') as FormArray).at(index);
                    fg.patchValue(val);
                    return val;
                }
                );
                if (data.invoicePayment) {
                    this.isPaid = true;
                }
                // this.invoiceForm.setValue(req);
                // this.invoiceForm.get('invoiceItems').setValue(items);
                this.calculateinvoiceAmount();

                console.log(data);
            }
        });
    }

    getAllPayementTypes() {
        this.settingsService.getAllPaymentTypes().subscribe((types: any) => {
            this.paymentTypes = types;
        });
    }

    openPaymentpopup() {

        if (this.invoiceForm.invalid) {
            this.invoiceForm.markAllAsTouched();
            this.invoiceForm.updateValueAndValidity();
            return;
        }
        const items = (this.invoiceForm.get('invoiceItems') as FormArray).value;
        items.filter(val => {
            if (typeof val.offeringId == "object") { val.offeringId = val.offeringId.id } delete val.index;
            if (!this.invoiceId) {
                delete val.id;
                delete val.invoiceId;
            }
        })
        const req = {
            "id": this.invoiceId,
            "parentBusinessId": this.appState.userProfile.parentBusinessId,
            "locationId": this.appState.selectedUserLocationId,
            "patientId": this.invoiceForm.get('patientId').value,
            "appointmentsEntryId": this.invoiceForm.get('appointmentsEntryId').value,
            "appointmentDate": this.invoiceForm.get('appointmentDate').value,
            "billTo": this.invoiceForm.get('billTo').value,
            "dueDate": this.invoiceForm.get('invoiceDate').value ? this.invoiceForm.get('invoiceDate').value : new Date(),
            "invoiceDate": this.invoiceForm.get('invoiceDate').value ? this.invoiceForm.get('invoiceDate').value : new Date(),
            "practitionerId": this.invoiceForm.get('practitionerId').value,
            "caseId": this.invoiceForm.get('caseId').value,
            "referralId": this.invoiceForm.get('referralId').value,
            "payerId": this.invoiceForm.get('payerId').value,
            "concessionId": this.invoiceForm.get('concessionId').value,
            "invoiceNo": this.invoiceForm.get('invoiceNo').value,
            "invoiceStatus": this.invoiceForm.get('invoiceStatus').value,
            "invoiceStatusText": this.invoiceForm.get('invoiceStatusText').value,
            "totalAmount": this.invoiceForm.get('totalAmount').value,
            "totalPaid": this.invoiceForm.get('totalPaid').value,
            "balance": this.invoiceForm.get('balance').value,
            "isLock": this.invoiceForm.get('isLock').value,
            "notes": this.invoiceForm.get('notes').value,
            "invoiceAutoId": this.invoiceForm.get('invoiceAutoId').value,
            "invoiceItems": (this.invoiceForm.get('invoiceItems') as FormArray).value,
            "invoicePayment": {}
        }
        AppUtils.customJsonInclude(req);
        const dialogRef = InvoiceAcceptPaymentPopUpComponent.open(this.appDialogService, {
            invoiceId: this.invoiceId,
            paymentTypes: this.paymentTypes,
            billingPage: this.isBillingPage,
            model: req
        });
        dialogRef.close.subscribe((value) => {
            if (value === "Save") {
                if (!this.invoiceId) {
                    this.alertService.displaySuccessMessage('Invoice created successfully.');
                    this.redirectPageWise();
                }
            }
        });
    }
    oepnDeletPopup() {
        const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
            hideOnSave: false,
            title: 'Do you wish to delete this Invoice ?'
        });
        ref.clickSave.subscribe(() => {
            this.blockUI.start();
            this.billingService.deleteInvoice(this.invoiceId).subscribe((data) => {
                if (data) {

                    this.alertService.displaySuccessMessage('Invoice Deleted successfully.');
                    this.blockUI.stop();
                    ref.closeDialog();
                    this.redirectPageWise();
                } else {
                    this.alertService.displayErrorMessage('Failed to delete Invoice, please try again later');
                    this.blockUI.stop();
                }
            }, () => {
                this.alertService.displayErrorMessage('Failed to delete Invoice, please try again later');
                this.blockUI.stop();
            });
        });
    }

    redirectPageWise() {
        if (this.isBillingPage) {
            this.appRouter.navigate(['/bills'])
        } else {
            this.appRouter.navigate(['/patients'])
        }

    }


}
