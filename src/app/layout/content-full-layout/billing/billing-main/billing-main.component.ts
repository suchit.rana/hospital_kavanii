import {Component, OnInit} from '@angular/core';
import {PaymentTypeModel} from '../../../../models/app.settings.model';
import {BillingSummary, InvoiceModel} from '../../../../models/app.billing.model';
import {AppState} from '../../../../app.state';
import {SettingsService} from '../../../../services/app.settings.service';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {BillingService} from '../billing.service';
import {PatientService} from '../../../../services/app.patient.service';
import {CreditManagerPopUpComponent} from '../credit-manager/credit-manager-pop-up.component';
import {SAVE} from '../../../../shared/popup-component/popup-component.component';
import {CreateInvoiceBatchComponent} from '../create-invoice-batch/create-invoice-batch.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
    selector: 'app-billing-main',
    templateUrl: './billing-main.component.html',
    styleUrls: ['./billing-main.component.scss']
})
export class BillingMainComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    paymentTypes: PaymentTypeModel[];

    invoices: InvoiceModel[] = [];

    patients: any;

    nonVoidedInvoices: Array<InvoiceModel> = new Array<InvoiceModel>();
    voidedInvoices: Array<InvoiceModel> = new Array<InvoiceModel>();

    invoiceTab = true;
    billingSummary: BillingSummary = new BillingSummary();

    constructor(private appState: AppState,
                private settingsService: SettingsService,
                private appDialogService: AppDialogService,
                private billingService: BillingService,
                private patientsService: PatientService) {
    }

    ngOnInit() {
        this.blockUI.start();
        this.settingsService.getAllPaymentTypes().subscribe((types: any) => {
            this.paymentTypes = types;
            this.blockUI.stop();
        });

        this.patientsService.getallpatientsmininfobylocationid(this.appState.selectedUserLocationId).subscribe((data: any) => {
            this.patients = data;
        });

        this.getBillingData();
    }

    getBillingData() {
        this.loadBillingSummary();
        this.loadInvoices();
    }

    loadInvoices() {
        this.blockUI.start();
        this.billingService.getAllInvoiceByLocation(this.appState.selectedUserLocationId).subscribe((res: any) => {
            this.invoices = res;
            this.voidedInvoices = this.invoices.filter(i => i.isVoid);
            this.nonVoidedInvoices = this.invoices.filter(i => !i.isVoid);
            this.blockUI.stop();

        });
    }

    loadBillingSummary() {
        this.billingService.getBillingSummary(this.appState.selectedUserLocationId).subscribe((res: any) => {
            this.billingSummary = res;
        });
    }

    onInvoiceTabClick() {
        this.invoiceTab = true;
    }

    onVoidTabClick() {
        this.invoiceTab = false;
    }

    onAddCreditClick() {
        const dialogRef = CreditManagerPopUpComponent.open(this.appDialogService, null, {
            isCredit: true,
            patientId: null,
            patients: this.patients,
            billingPage: true
        });
        dialogRef.close.subscribe((value) => {
            if (value === SAVE) {
                this.getBillingData();
            }
        });
    }

    openCreateBatchInvoiceModal() {
        this.appDialogService.open(CreateInvoiceBatchComponent, {
            title: 'CREATE BATCH',
            width: '778px',
            cancelBtnTitle: 'No',
            saveBtnTitle: 'Yes',
            hideSave: false,
            hideOnSave: false,
            hideFooter: true,
            data: {}
        });
    }

}
