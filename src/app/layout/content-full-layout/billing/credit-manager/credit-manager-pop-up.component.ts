import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnInit, Type, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppState} from 'src/app/app.state';
import {cardCheck} from 'src/app/helpers/card-check.helper';
import {CreditModel, CreditType} from 'src/app/models/app.billing.model';
import {AppAlertService} from 'src/app/shared/services/app-alert.service';
import {AppDialogService, IPopupAction} from 'src/app/shared/services/app-dialog.service';
import {BillingService} from '../billing.service';
import {StripCardPaymentComponent} from '../strip-card-payment/strip-card-payment.component';
import {StripeCardElementService} from './../../../../shared/services/stripe-card/stripe-card-element.service';
import {MatFormFieldAppearance} from '@angular/material/form-field/typings/form-field';
import {
    DeleteConfirmationDialogComponent
} from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {SettingsService} from '../../../../services/app.settings.service';
import {PatientService} from '../../../../services/app.patient.service';
import {SAVE} from '../../../../shared/popup-component/popup-component.component';

@Component({
    selector: 'app-add-edit-credit-pop-up',
    templateUrl: './credit-manager-pop-up.component.html',
    styleUrls: ['./credit-manager-pop-up.component.css']
})
export class CreditManagerPopUpComponent implements OnInit, AfterViewInit {

    readonly CASH = 'CASH';
    readonly CREDIT_CARD = 'CREDIT CARD';
    readonly DEBIT_CARD = 'DEBIT CARD';
    readonly EFT = 'EFT';
    readonly CHEQUE = 'CHEQUE';

    static ref: IPopupAction;

    constructor(private appState: AppState,
                private dialogRef: DialogRef,
                private alertService: AppAlertService,
                private billingService: BillingService,
                private cd: ChangeDetectorRef,
                private appDialogService: AppDialogService,
                private settingsService: SettingsService,
                private patientsService: PatientService,
                private stripeElementService: StripeCardElementService) {
    }

    paymentHandler: any = null;

    @BlockUI()
    blockUI: NgBlockUI;

    @Input()
    patientId: string;

    @Input()
    isCredit: boolean = false; //Represents Credit Or Refund True = Credit, False = Refund

    @Input()
    billingPage: boolean = false; // Determines whether it's from Billing Page or Patient Billing Page

    @Input()
    model: CreditModel;


    @ViewChild('cardInfo', {static: false}) cardInfo: ElementRef;
    @ViewChild('deleteButtonTemplate', {static: false}) deleteButtonTemplate: Type<any>;

    appearance: MatFormFieldAppearance = 'outline';
    selectedPayType: string = this.CASH;
    paymentTypes: any;
    patients: Array<any> = new Array<any>();

    formGroup: FormGroup = new FormGroup({
        billingDate: new FormControl(null, [Validators.required]),
        creditType: new FormControl('', [Validators.required]),
        amount: new FormControl('', [Validators.required]),
        notes: new FormControl('', []),
        cardNumber: new FormControl('', []),
        chequeNo: new FormControl('', []),
        bankBranch: new FormControl('', []),
        referenceNo: new FormControl('', []),
        payerName: new FormControl('', []),
        cvv: new FormControl('', []),
        expiryMonth: new FormControl('', []),
        expiryYear: new FormControl('', []),
        patientId: new FormControl(null, [Validators.required])
    });

    ngOnInit() {
        this.clickAction();
        this.loadBasicDetails();

        if (this.model) {
            if (!this.isCredit) {
                this.formGroup.get('amount').setValidators(Validators.max(this.model.refundCreditSummary.totalCreditBalance));
                if (!this.model.creditId) {
                    //Reset the model as we need to create a New Refund to the Credit
                    const creditId = this.model.id;
                    const billingCreditId = this.model.billingId;
                    const refundSummary = this.model.refundCreditSummary;

                    this.model = new CreditModel();
                    this.model.creditId = creditId;
                    this.model.billingDate = null;
                    this.model.billingCreditId = billingCreditId;
                    this.model.refundCreditSummary = refundSummary;
                }
            }
            this.formGroup.patchValue(this.model);

            if (
                (this.isCredit && this.model.id) ||
                (!this.isCredit && this.model.id && this.model.creditId)) {
                this.setFormToEditMode();
            }
        }

        if (!this.model) {
            this.model = new CreditModel();
        }

        this.model.locationId = this.appState.selectedUserLocationId;
        this.model.parentBusinessId = this.appState.userProfile.parentBusinessId;
        this.model.patientId = this.patientId;
        if (this.patientId) {
            this.formGroup.get('patientId').setValue(this.patientId);
        }
        this.model.status = this.isCredit ? CreditType.Credit : CreditType.Refund;
        this.model.statusAsText = this.isCredit ? 'Credit' : 'Refund';
        this.formGroup.valueChanges.subscribe((formData: any) => {
            for (const prop in formData) {
                this.model[prop] = formData[prop];
            }
        });
    }

    ngAfterViewInit() {
        if (CreditManagerPopUpComponent.ref) {
            CreditManagerPopUpComponent.ref.footerTemplate(this.deleteButtonTemplate);
        }
    }

    cardValidatorChange(e) {
        if (
            this.formGroup.get('cardNumber').value !== '' &&
            !cardCheck(e)
        ) {
            this.formGroup
                .get('cardNumber')
                .setErrors({'server-error': 'Invalid credit Card Number'});
        }
    }

    onPaymentTypeChange(e) {
        this.clearForm();

        const paymentType = this.paymentTypes.filter(p => p.id === e.value)[0];
        this.selectedPayType = this.getSelectedPaymentType(paymentType.paymentType);

        if (this.selectedPayType === this.CREDIT_CARD || this.selectedPayType === this.DEBIT_CARD) {
            this.resetValidators();
            this.formGroup.get('cardNumber').setValidators([Validators.required]);
            this.formGroup.get('expiryMonth').setValidators([Validators.required]);
            this.formGroup.get('expiryYear').setValidators([Validators.required]);
            this.formGroup.get('cvv').setValidators([Validators.required]);

            this.formGroup.get('bankBranch').clearValidators();
            this.formGroup.get('chequeNo').clearValidators();

            this.formGroup.get('referenceNo').clearValidators();
            this.formGroup.get('payerName').clearValidators();


            this.formGroup.get('bankBranch').updateValueAndValidity();
            this.formGroup.get('chequeNo').updateValueAndValidity();
            this.formGroup.get('referenceNo').updateValueAndValidity();
            this.formGroup.get('payerName').updateValueAndValidity();
            this.formGroup.get('cardNumber').updateValueAndValidity();
            this.formGroup.get('expiryMonth').updateValueAndValidity();
            this.formGroup.get('expiryYear').updateValueAndValidity();
            this.formGroup.get('cvv').updateValueAndValidity();

            //this.invokeStripe();

        }

        if (this.selectedPayType === this.EFT) {
            this.formGroup.get('cardNumber').clearValidators();
            this.formGroup.get('expiryMonth').clearValidators();
            this.formGroup.get('expiryYear').clearValidators();
            this.formGroup.get('cvv').clearValidators();
            this.formGroup.get('referenceNo').setValidators([Validators.required]);
            this.formGroup.get('payerName').setValidators([Validators.required]);

            this.formGroup.get('bankBranch').clearValidators();
            this.formGroup.get('chequeNo').clearValidators();


            this.formGroup.get('chequeNo').updateValueAndValidity();
            this.formGroup.get('bankBranch').updateValueAndValidity();
            this.formGroup.get('referenceNo').updateValueAndValidity();
            this.formGroup.get('payerName').updateValueAndValidity();

            this.formGroup.get('cardNumber').updateValueAndValidity();
            this.formGroup.get('expiryMonth').updateValueAndValidity();
            this.formGroup.get('expiryYear').updateValueAndValidity();
            this.formGroup.get('cvv').updateValueAndValidity();
        }

        if (this.selectedPayType === this.CHEQUE) {
            this.formGroup.get('cardNumber').clearValidators();
            this.formGroup.get('expiryMonth').clearValidators();
            this.formGroup.get('expiryYear').clearValidators();
            this.formGroup.get('cvv').clearValidators();
            this.formGroup.get('referenceNo').clearValidators();
            this.formGroup.get('payerName').clearValidators();

            this.formGroup.get('bankBranch').setValidators([Validators.required]);
            this.formGroup.get('chequeNo').setValidators([Validators.required]);


            this.formGroup.get('chequeNo').updateValueAndValidity();
            this.formGroup.get('bankBranch').updateValueAndValidity();
            this.formGroup.get('referenceNo').updateValueAndValidity();
            this.formGroup.get('payerName').updateValueAndValidity();

            this.formGroup.get('cardNumber').updateValueAndValidity();
            this.formGroup.get('expiryMonth').updateValueAndValidity();
            this.formGroup.get('expiryYear').updateValueAndValidity();
            this.formGroup.get('cvv').updateValueAndValidity();
        }

        if (this.selectedPayType === this.CASH) {
            this.formGroup.get('cardNumber').clearValidators();
            this.formGroup.get('expiryMonth').clearValidators();
            this.formGroup.get('expiryYear').clearValidators();
            this.formGroup.get('cvv').clearValidators();
            this.formGroup.get('referenceNo').clearValidators();
            this.formGroup.get('payerName').clearValidators();

            this.formGroup.get('bankBranch').clearValidators();
            this.formGroup.get('chequeNo').clearValidators();


            this.formGroup.get('chequeNo').updateValueAndValidity();
            this.formGroup.get('bankBranch').updateValueAndValidity();
            this.formGroup.get('referenceNo').updateValueAndValidity();
            this.formGroup.get('payerName').updateValueAndValidity();

            this.formGroup.get('cardNumber').updateValueAndValidity();
            this.formGroup.get('expiryMonth').updateValueAndValidity();
            this.formGroup.get('expiryYear').updateValueAndValidity();
            this.formGroup.get('cvv').updateValueAndValidity();
        }
    }

    getSelectedPaymentType(paymentType: string) {
        switch (paymentType.toUpperCase()) {
            case 'CREDIT CARD':
                return 'CREDIT CARD';
            case 'DEBIT CARD':
                return 'DEBIT CARD';
            case 'CASH':
                return 'CASH';
            case 'CHEQUE':
                return 'CHEQUE';
            case 'EFT':
                return 'EFT';
            default:
                return 'CASH';
        }
    }

    clearForm() {
        this.formGroup.get('referenceNo').patchValue(null);
        this.formGroup.get('payerName').patchValue(null);
        this.formGroup.get('chequeNo').patchValue('');
        this.formGroup.get('bankBranch').patchValue('');
        this.formGroup.get('cardNumber').patchValue('');
        this.formGroup.get('expiryMonth').patchValue('');
        this.formGroup.get('expiryYear').patchValue('');
        this.formGroup.get('cvv').patchValue('');

    }

    setFormToEditMode() {
        this.formGroup.get('creditType').disable();
        this.formGroup.get('amount').disable();
        this.formGroup.get('referenceNo').disable();
        this.formGroup.get('payerName').disable();
        this.formGroup.get('chequeNo').disable();
        this.formGroup.get('bankBranch').disable();
        this.formGroup.get('cardNumber').disable();
        this.formGroup.get('expiryMonth').disable();
        this.formGroup.get('expiryYear').disable();
        this.formGroup.get('cvv').disable();
        this.formGroup.get('patientId').disable();
    }

    resetValidators() {
        this.formGroup.get('cardNumber').setErrors(null);
        this.formGroup.get('expiryMonth').setErrors(null);
        this.formGroup.get('expiryYear').setErrors(null);
        this.formGroup.get('cvv').setErrors(null);
        this.formGroup.get('referenceNo').setErrors(null);
        this.formGroup.get('payerName').setErrors(null);
        this.formGroup.get('bankBranch').setErrors(null);
        this.formGroup.get('chequeNo').setErrors(null);

        this.formGroup.get('cardNumber').clearValidators();
        this.formGroup.get('expiryMonth').clearValidators();
        this.formGroup.get('expiryYear').clearValidators();
        this.formGroup.get('cvv').clearValidators();
        this.formGroup.get('referenceNo').clearValidators();
        this.formGroup.get('payerName').clearValidators();
        this.formGroup.get('bankBranch').clearValidators();
        this.formGroup.get('chequeNo').clearValidators();


        this.formGroup.get('chequeNo').updateValueAndValidity();
        this.formGroup.get('bankBranch').updateValueAndValidity();
        this.formGroup.get('referenceNo').updateValueAndValidity();
        this.formGroup.get('payerName').updateValueAndValidity();

        this.formGroup.get('cardNumber').updateValueAndValidity();
        this.formGroup.get('expiryMonth').updateValueAndValidity();
        this.formGroup.get('expiryYear').updateValueAndValidity();
        this.formGroup.get('cvv').updateValueAndValidity();
    }

    cancel() {
        CreditManagerPopUpComponent.ref.closeDialog(SAVE);
    }

    save() {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            this.formGroup.updateValueAndValidity();
            return;
        }

        if (this.selectedPayType == this.CREDIT_CARD || this.selectedPayType == this.DEBIT_CARD) {
            //Show Stripe Payment -- This will get Changed to Stripe Payment Type instead of Credit Or Debit Type
            const dialogRef = this.appDialogService.open(StripCardPaymentComponent, {
                title: 'CARD DETAILS',
                width: '778px',
                cancelBtnTitle: 'No',
                saveBtnTitle: 'Yes',
                hideSave: false,
                hideOnSave: false,
                hideFooter: true,
                data: {
                    amount: this.model.amount,
                    paymentTypes: this.paymentTypes,
                    isCredit: true,
                    patientId: this.model.patientId,
                    billingPage: true
                }
            });
            dialogRef.close.subscribe((res: any) => {
                // this.getBillingData();
            });
        } else {
            this.blockUI.start();
            this.billingService.addCredit(this.model).subscribe((res: any) => {
                if (res) {
                    this.alertService.displaySuccessMessage(`Amount is ${this.isCredit ? 'Credited' : 'Refunded'} successfully`);
                }
                this.blockUI.stop();
                CreditManagerPopUpComponent.ref.closeDialog(SAVE);
            }, error => {
                this.alertService.displayErrorMessage(error || `Failed to ${this.isCredit ? 'Credit' : 'Refund'} Amount`);
            });
        }

        if (this.isCredit) {
            // if (this.model.id) {
            //   this.billingService.updateCredit(this.model).subscribe((res: any) => {
            //     this.alertService.displaySuccessMessage(this.model.id ? "Credit amount updated successfully" : "Credit amount is added successfully");
            //     CreditManagerPopUpComponent.ref.closeDialog(SAVE);
            //   },
            //     error => {
            //       this.alertService.displayErrorMessage(error || "Failed to add Credit");
            //     });
            // }
            // else {
            //   this.billingService.addCredit(this.model).subscribe((res: any) => {
            //     this.alertService.displaySuccessMessage("Credit amount is added successfully");
            //     CreditManagerPopUpComponent.ref.closeDialog(SAVE);
            //   },
            //     error => {
            //       this.alertService.displayErrorMessage(error || "Failed to add Credit");
            //     });

            //   //this.makePayment(this.model.amount);
            // }
        } else {

        }


    }

    confirmDeleteCredit() {
        const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
            hideOnSave: false,
            title: 'ARE YOU SURE TO DELETE THIS CREDIT?'
        });
        ref.clickSave.subscribe(() => {
            this.deleteCredit(ref);
        });
    }

    deleteCredit(ref: IPopupAction) {
        this.blockUI.start();
        this.billingService.deleteCredit(this.model.id).subscribe((res: any) => {
            this.alertService.displaySuccessMessage('Credit amount is deleted successfully');
            ref.closeDialog();
            CreditManagerPopUpComponent.ref.closeDialog(SAVE);
            this.blockUI.stop();
        }, error => {
            this.alertService.error(error);
            this.blockUI.stop();
        });
    }

    invokeStripe() {
        if (!window.document.getElementById('stripe-script')) {
            const script = window.document.createElement('script');
            script.id = 'stripe-script';
            script.type = 'text/javascript';
            script.src = 'https://checkout.stripe.com/checkout.js';
            script.onload = () => {
                this.paymentHandler = (<any> window).StripeCheckout.configure({
                    key: 'pk_test_51L33TFSDtmDfdiXSkitGE1ty33nAEnolZ1Z4wdpY6ooGKmkxOhUsRJOWuAgFFu0HC1UNzsnvjMn9Qzwbev6Ut00M00CFam5Msd',
                    locale: 'auto',
                    token: function(stripeToken: any) {
                        console.log(stripeToken);
                        alert('Payment has been successfull!');
                    },
                });
            };
            window.document.body.appendChild(script);
        }
    }

    makePayment(amount: any) {
        const paymentHandler = (<any> window).StripeCheckout.configure({
            key: 'pk_test_51L33TFSDtmDfdiXSkitGE1ty33nAEnolZ1Z4wdpY6ooGKmkxOhUsRJOWuAgFFu0HC1UNzsnvjMn9Qzwbev6Ut00M00CFam5Msd',
            locale: 'auto',
            token: function(stripeToken: any) {
                console.log(stripeToken);
                alert('Stripe token generated!');
            },
        });
        paymentHandler.open({
            name: 'Kavanii',

            amount: amount * 100,
        });
    }

    private amountValidator(control: AbstractControl): { [key: string]: boolean } | null {
        if (control.value != undefined && (isNaN(control.value) || control.value > this.model.refundCreditSummary.totalCreditBalance)) {
            return {'amountRange': true};
        }
        return null;
    }

    private loadBasicDetails() {
        this.settingsService.getAllPaymentTypes().subscribe((types: any) => {
            this.paymentTypes = types;
            if (this.model && this.model.id) {
                const paymentType = this.paymentTypes.filter(p => p.id === this.model.creditType)[0];
                this.selectedPayType = this.getSelectedPaymentType(paymentType.paymentType);
            }
            this.blockUI.stop();
        });

        this.patientsService.getallpatientsmininfobylocationid(this.appState.selectedUserLocationId).subscribe((data: any) => {
            this.patients = data;
        });
    }

    private clickAction() {
        if (CreditManagerPopUpComponent.ref) {
            CreditManagerPopUpComponent.ref.clickSave.subscribe(() => {
                this.save();
            });
        }
    }

    get showCreditOrDebitCardContainer() {
        return this.selectedPayType === 'CREDIT CARD' || this.selectedPayType === 'DEBIT CARD';
    }

    get showCashContainer() {
        return this.selectedPayType === 'CASH';
    }

    get showChequeContainer() {
        return this.selectedPayType === 'CHEQUE';
    }

    get showEFTContainer() {
        return this.selectedPayType === 'EFT';
    }

    static open(dialogService: AppDialogService, credit: CreditModel, data: { [x: string]: any }, isRefund: boolean = false): IPopupAction {
        this.ref = dialogService.open(CreditManagerPopUpComponent, {
            title: !isRefund ? (credit && !!credit.id ? 'EDIT CREDIT' : 'ADD CREDIT') : (credit && !credit.creditId ? 'REFUND CREDIT' : 'EDIT CREDIT REFUND'),
            width: '778px',
            cancelBtnTitle: 'Cancel',
            saveBtnTitle: isRefund ? 'Refund' : 'Save',
            hideSave: false,
            hideOnSave: false,
            height: 'auto',
            data: data
        });
        return this.ref;
    }
}
