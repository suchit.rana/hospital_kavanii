import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import {ContentFullLayoutRoutingModule} from './content-full-layout-routing.module';
import {ContentFullLayoutComponent} from './content-full-layout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SharedModuleModule} from '../../shared/shared-module.module';
import {
  MatDividerModule,
  MatExpansionModule,
  MatListModule,
  MatMenuModule,
  MatOptionModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import {SidenavComponent} from '../../sidenav/sidenav.component';
import {MiscService} from '../../services/app.misc.service';
import {OfferingsService} from '../../services/app.offerings.service';
import {MapService} from '../../services/app.map.service';
import {SettingsService} from '../../services/app.settings.service';
import {ApplicationDataService} from '../../services/app.applicationdata.service';
import {PatientService} from '../../services/app.patient.service';
import {ContactService} from '../../services/app.contact.service';
import {StaffService} from '../../services/app.staff.service';
import {AppointmentService} from '../../services/app.appointment.service';
import {ScheduleEditService} from '../../services/app.schedule.editservice';
import {AppointmentEditService} from '../../services/app.appointment.editservice';
import {RoomEditService} from '../../services/app.room.editservice';
import {IntlModule} from '@progress/kendo-angular-intl';
import {DxPopoverModule} from 'devextreme-angular';
import {LetterTagListModule} from '../../shared/letter-tag-list/letter-tag-list.module';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {UsersModule} from './staffs/users/users.module';
import {KnowledgeBaseComponent} from './knowledge-base/knowledge-base.component';
import {AccessDirectiveModule} from '../../shared/directives/access-directive/access-directive.module';
import {AppDateRangePickerModule} from './common/app-date-range-picker/app-date-range-picker.module';
import {IndicatorsModule} from '@progress/kendo-angular-indicators';

@NgModule({
  declarations: [
    ContentFullLayoutComponent,
    DashboardComponent,
    SidenavComponent,
    KnowledgeBaseComponent
  ],
    imports: [
        CommonModule,
        SharedModuleModule,
        ContentFullLayoutRoutingModule,
        MatProgressBarModule,
        MatSidenavModule,
        MatMenuModule,
        MatDividerModule,
        MatListModule,
        MatOptionModule,
        MatSelectModule,
        MatExpansionModule,
        DxPopoverModule,
        IntlModule,
        LetterTagListModule,
        DialogModule,
        UsersModule,
        AccessDirectiveModule,
        IndicatorsModule
    ],
  providers: [
    MiscService,
    ApplicationDataService,
    MapService,
    OfferingsService,
    PatientService,
    ContactService,
    SettingsService,
    SidenavComponent,
    StaffService,
    AppointmentService,
    ScheduleEditService,
    AppointmentEditService,
    RoomEditService,
    DatePipe
  ]
})
export class ContentFullLayoutModule {
}
