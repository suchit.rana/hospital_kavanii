import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDrawer } from '@angular/material';
import { AppService } from '../../app.service';
import { AppState } from '../../app.state';
import { AuthService } from '../../services/app.authenication.service';
import { Router } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { onMainContentChange } from '../../animations/animations';
import { DialogService } from '@progress/kendo-angular-dialog';
import { AddEditStaffComponent } from './staffs/users/add-edit-staff/add-edit-staff.component';
import { Observable } from 'rxjs';
import { BusinessFormModel } from 'src/app/models/app.business.model';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { StaffService } from './../../services/app.staff.service';
import { ImageSnippet } from 'src/app/models/app.misc';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-content-full-layout',
  templateUrl: './content-full-layout.component.html',
  styleUrls: ['./content-full-layout.component.css'],
  animations: [onMainContentChange]
})
export class ContentFullLayoutComponent implements OnDestroy {
  @ViewChild("drawer", { read: true, static: true }) drawer: MatDrawer;
  public onSideNavChange: boolean;
  title = 'kavaniiUI';
  mobileQuery: MediaQueryList;
  isResponsive = false;
  private _mobileQueryListener: () => void;
  helpDialog: boolean;
  photo:any;

  constructor(
    // public appService: AppService,
    public appState: AppState,
    private authService: AuthService,
    private sanitizer: DomSanitizer,
    public router: Router,
    public changeDetectorRef: ChangeDetectorRef,
    public media: MediaMatcher,
    // private dialogService:DialogService,

    public dialog: MatDialog,
    private staffService:StaffService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 992px)');
    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      this.isResponsive = true;
    }
    this.mobileQuery.addListener(this._mobileQueryListener);

    let profile = this.appState.userProfile;
    this.staffService.getStaffById(this.appState.userProfile.id).subscribe((data:any)=>{
      if (data.photo) {
        this.photo =  this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
        + data.photo);
      }
      else{
        this.photo = '../../assets/user.jpg';
      }
      profile.photo = this.photo;
      this.appState.setUserProfileSubject(profile);

    });
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  closeDrawer() {
    this.drawer.opened = false;
  }

  openMyProfile() {
  //  const dialogRef = this.dialogService.open({
  //   content: AddEditStaffComponent,
  //     width:1350
  //   });

  const dialogRef = this.dialog.open(AddEditStaffComponent,{

      width:'1350px'
    });

    let instance = dialogRef.componentInstance;
    instance.isMyProfileDialog = true;

  //  const staffComponent = dialogRef.content.instance;
  //   staffComponent.isMyProfileDialog = true;

    instance.closeMyprofile.subscribe(()=>{
      dialogRef.close();
    });
  }
  get businessName(): Observable<BusinessFormModel> {
    return this.appState.businessDetailsSubject.asObservable();
  }
 showHelpDialog(){
   this.helpDialog = true;
 }
  closeHelpDialog(){
    this.helpDialog = false;
  }

  openKnowledgebase(){
    this.router.navigate([]).then(res=>{window.open('/#/knowledgebase','_blank')});
  }
  logout() {
    this.appState.resetState();
    this.authService.logout();
    this.router.navigate(["/account/login"], {queryParams: {"screen": "logout", "code": 0}});

    }

}
