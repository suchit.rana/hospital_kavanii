import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {ContactService} from '../../../../services/app.contact.service';
import {ApplicationDataService} from '../../../../services/app.applicationdata.service';
import {AppState} from '../../../../app.state';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {
    ContactModel,
    ContactLocationModel,
    ContactSpecialityModel,
} from '../../../../models/app.contact.model';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {MiscService} from '../../../../services/app.misc.service';
import {SettingsService} from '../../../../services/app.settings.service';
import {GroupResult, groupBy} from '@progress/kendo-data-query';
import {validateProviderNo} from '../../../../shared/validation';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {
    DeleteConfirmationDialogComponent
} from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../shared/services/app-alert.service';

@Component({
    selector: 'app-add-referral-contacts',
    templateUrl: './add-referral.component.html',
    styleUrls: ['./add-referral.component.css'],
})
export class AddReferralContactsComponent
    extends BaseItemComponent
    implements OnInit {
    readonly REFERRAL_MODULE = RP_MODULE_MAP.contacts_referrals;

    @BlockUI() blockUI: NgBlockUI;
    locationList: any[];
    titles: any[];
    titleData: any[];
    public filter: string;
    titleId: number;
    public expandCss: string;
    countries: any[];
    specialties: any[];
    titleParentId: number;
    deleteButton: boolean;
    contactForm: FormGroup = new FormGroup({
        locationName: new FormControl('', Validators.required),
        specialityName: new FormControl('', Validators.required),
        organisationName: new FormControl('', Validators.required),
        titleId: new FormControl(0),
        firstName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
        lastName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
        providerNo: new FormControl('', validateProviderNo),
        address: new FormControl(''),
        address2: new FormControl(''),
        country: new FormControl('Australia'),
        state: new FormControl(''),
        city: new FormControl(''),
        postCode: new FormControl(''),
        workPhone: new FormControl(''),
        mobile: new FormControl(''),
        fax: new FormControl(''),
        emailId: new FormControl(
            '',
            Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$')
        ),
        website: new FormControl('', [
            Validators.pattern(
                '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
            ),
        ]),
        notes: new FormControl(''),
        isAllowAllLocation: new FormControl(false),
        status: new FormControl(true),
    });

    public specialityData: any[] = [];
    public groupedSpecialityData: GroupResult[];
    specialityAllList = [
        {specialityId: 1, specialityName: 'Chiropratic'},
        {specialityId: 2, specialityName: 'Dietetics'},
        {specialityId: 3, specialityName: 'Exercise Physiology'},
        {specialityId: 4, specialityName: 'General'},
        {specialityId: 5, specialityName: 'Massage Therapy'},
        {specialityId: 6, specialityName: 'Occupational therapy'},
        {specialityId: 7, specialityName: 'Optometry'},
        {specialityId: 8, specialityName: 'Osteopathy'},
        {specialityId: 9, specialityName: 'Physiotherapy'},
        {specialityId: 10, specialityName: 'Podiatry'},
        {specialityId: 11, specialityName: 'Psychology'},
        {specialityId: 12, specialityName: 'Speech Pathology'},
    ];
    specialityEnable = true;
    specialityDataTemp: any[] = [];
    country: string;
    addressManual: string;
    selectedAddress: string;
    contactData: ContactModel;
    selectedLocation: any[];
    contactId: string;
    fromstate: any;
    fromStateReferral: any;
    stateValues: any;

    constructor(
        private businessService: BusinessService,
        protected appState: AppState,
        private _route: ActivatedRoute,
        public location: Location,
        public contactService: ContactService,
        private settingsService: SettingsService,
        private miscService: MiscService,
        public applicationDataService: ApplicationDataService,
        protected router: Router,
        private dialogService: AppDialogService,
        private alertService: AppAlertService,
    ) {
        super(location);
        this.fillTitle();
        if (this.router.getCurrentNavigation().extras.state != null) {
            this.fromstate = JSON.parse(this.router.getCurrentNavigation().extras.state.fromState);
        }
        if (this.router.getCurrentNavigation().extras.state != null) {
            this.fromStateReferral = this.router.getCurrentNavigation().extras.state.fromStateReferral;
            this.stateValues = this.fromStateReferral ? this.router.getCurrentNavigation().extras.state.stateValues : null;
        } else {
            this.fromStateReferral = history.state.fromStateReferral;
            this.stateValues = this.fromStateReferral ? history.state.stateValues : null;
        }
        if (this.fromStateReferral) {
            this.fromstate = null;
        }
    }

    ngOnInit() {
        this.contactData = new ContactModel();
        this.selectedAddress = this.contactForm.get('address').value;

        this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
            this.locationList = locations;
            this.locationPopulate();
        });
        this.titleParentId = ApplicationDataEnum.Title;

        this.miscService.getCountries().subscribe((countries) => {
            this.countries = countries;
        });

        // this.settingsService.getAllSpecialties().subscribe((specialityData) => {
        //   this.specialityAllList = specialityData;
        //   this.specialityPopulate();
        // });

        this.expandCss = 'col-xl-6';
        this.country = 'au';
        this.deleteButton = false;
        this._route.params.subscribe((params) => {
            // console.log("params",params);
            // console.log("params.contactId",params.contactId);
            // params.contactId undefined
            if (params.contactId) {
                this.blockUI.start();
                this.contactId = params.contactId;
                this.addItem = false;
                this.itemid = params.contactId;
                this.deleteButton = true;
                this.contactService
                    .getContactById(params.contactId)
                    .subscribe((data: ContactModel) => {
                        this.contactForm.patchValue(data);
                        // this.contactForm.patchValue(data.address);
                        this.contactData = data;
                        const locationName = [];
                        if (this.locationList !== undefined) {
                            data.contactLocation.forEach((pl) => {
                                locationName.push(
                                    this.locationList.find((l) => l.id === pl.locationId)
                                );
                            });
                            this.selectedLocation = locationName;
                            this.contactForm.get('locationName').patchValue(locationName);

                            this.selectedAddress = this.addressManual = data.address;
                            this.specialityEnable = false;

                            // const specialityName = [];
                            // this.specialityDataTemp = [];

                            // if (this.specialityAllList !== undefined) {
                            //   // this.populateSpeciality(locationName);
                            //   data.contactSpeciality.forEach((s) => {
                            //     const location = this.locationList.find(
                            //       (x) => x.id === s.locationId
                            //     );
                            //     const speciality = this.specialityAllList.find(
                            //       (x) => x.specialityId.toString() === s.specialityId
                            //     );

                            //     const sData = {
                            //       id: "",
                            //       idAndLocationId: "",
                            //       specialityName: "",
                            //       locationId: "",
                            //       locationName: "",
                            //     };
                            //     sData.id = s.specialityId;
                            //     sData.idAndLocationId = s.specialityId + location.id;
                            //     sData.specialityName =
                            //       speciality.specialityName +
                            //       " (" +
                            //       location.locationName +
                            //       ")";
                            //     sData.locationId = location.id;
                            //     sData.locationName = location.locationName;

                            //     this.specialityDataTemp.push(sData);

                            //     specialityName.push(
                            //       this.specialityAllList.find(
                            //         (sl) => sl.specialityId.toString() === s.specialityId
                            //       )
                            //     );
                            //   });
                            // }
                            // this.contactForm
                            //   .get("specialityName")
                            //   .patchValue(this.specialityDataTemp);
                        }
                    });

                this.blockUI.stop();
            }
        });
    }

    deleteContact() {
        const ref = DeleteConfirmationDialogComponent.open(this.dialogService, '', {
            hideOnSave: false,
            title: 'Do you wish to delete this contact ?'
        });
        ref.clickSave.subscribe(() => {
            this.contactService.deleteContact(this.contactId).subscribe(
                (e) => {
                    this.alertService.displaySuccessMessage('Contact added successfully.');
                    this.cancel();
                    this.blockUI.stop();
                    ref.closeDialog();
                },
                () => {
                    this.alertService.displayErrorMessage(
                        'Failed to delete Referral, please try again later'
                    );
                    this.blockUI.stop();
                }
            );
        });

    }


    // specialityPopulate() {
    //   if (this.contactData.contactSpeciality !== undefined) {
    //     const locationName = this.selectedLocation;
    //     this.populateSpeciality(locationName);
    //     const specialityName = [];
    //     this.specialityDataTemp = [];
    //     this.contactData.contactSpeciality.forEach((s) => {
    //       const location = this.locationList.find((x) => x.id === s.locationId);
    //       const speciality = this.specialityAllList.find(
    //         (x) => x.specialityId.toString() === s.specialityId
    //       );

    //       const sData = {
    //         id: "",
    //         idAndLocationId: "",
    //         specialityName: "",
    //         locationId: "",
    //         locationName: "",
    //       };
    //       sData.id = s.specialityId;
    //       sData.idAndLocationId = s.specialityId + location.id;
    //       sData.specialityName =
    //         speciality.specialityName + " (" + location.locationName + ")";
    //       sData.locationId = location.id;
    //       sData.locationName = location.locationName;

    //       this.specialityDataTemp.push(sData);

    //       specialityName.push(
    //         this.specialityAllList.find((sl) => sl.specialityId.toString() === s.specialityId)
    //       );
    //     });
    //     this.contactForm
    //       .get("specialityName")
    //       .patchValue(this.specialityDataTemp);
    //   }
    // }

    allLocationChange($event) {
        if ($event === true) {
            const locationName = [];
            if (this.locationList !== undefined) {
                this.locationList.forEach((l) => {
                    locationName.push(l);
                });
                this.selectedLocation = locationName;
                this.contactForm.get('locationName').patchValue(locationName);
                // this.populateSpeciality(locationName);
            }
        } else {
            this.contactForm.get('locationName').patchValue('');
            this.contactForm
                .get('specialityName')
                .patchValue('');
            this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
        }
    }

    locationPopulate() {
        const locationName = [];
        this.miscService.getCountries().subscribe((countries) => {
            const c = this.locationList.find(
                (l) =>
                    l.locationName === this.appState.selectedUserLocation.locationName
            );

            const country_Code = countries.find((x) => x.country_Name === c.country)
                .country_Code;

            this.country = country_Code.toLowerCase();
            console.log(country_Code.toLowerCase());
            this.contactForm.get('country').patchValue(c.country);
        });
        if (this.contactData.contactLocation !== undefined) {
            this.contactData.contactLocation.forEach((pl) => {
                locationName.push(
                    this.locationList.find((l) => l.id === pl.locationId)
                );
            });
            this.selectedLocation = locationName;
            this.contactForm.get('locationName').patchValue(locationName);
            // this.populateSpeciality(locationName);
        }
    }

    handleFilter(value) {
        if (this.titles !== undefined && this.titles.length > 0) {
            this.titleData = this.titles.filter(
                (s) => s.categoryName.toLowerCase().indexOf(value.toLowerCase()) !== -1
            );
        }
        this.filter = value;
    }

    handleCategoryChange(value) {
        this.titleId = value;
    }

    fillTitle() {
        this.applicationDataService
            .GetApplicationDataByCategoryId(
                ApplicationDataEnum.Title,
                this.appState.selectedUserLocation.id
            )
            .subscribe((data) => {
                this.titles = this.titleData = data;
            });
    }

    createReferralContact() {
        const el = document.getElementById('heading');
        const contactModel: ContactModel = this.contactForm.value;
        const locations: ContactLocationModel[] = [];

        if (!this.contactForm.invalid) {
            this.blockUI.start();
            this.submitting = true;

            if (contactModel.locationName !== '') {
                contactModel.locationName.forEach((l) => {
                    const model = new ContactLocationModel();
                    model.locationId = l.id;
                    model.contactId = this.itemid;
                    locations.push(model);
                });
            }

            // if (contactModel.specialityName !== "") {
            //   contactModel.specialityName.forEach((s) => {
            //     const model = new ContactSpecialityModel();
            //     model.specialityId = s.id;
            //     model.locationId = s.locationId;
            //     model.contactId = this.itemid;
            //     speciality.push(model);
            //   });
            // }

            contactModel.titleId = this.titleId;
            // contactModel.address = this.addressManual;
            // mytest
            contactModel.address = this.addressManual = this.contactForm.get('address').value;
            ;

            contactModel.contactLocation = locations;
            // contactModel.contactSpeciality = speciality;
            contactModel.contactType = 1;
            contactModel.postCode = String(
                contactModel.postCode === null ? '' : contactModel.postCode
            );
            contactModel.workPhone = String(
                contactModel.workPhone === null ? '' : contactModel.workPhone
            );
            contactModel.mobile = String(
                contactModel.mobile === null ? '' : contactModel.mobile
            );
            contactModel.fax = String(
                contactModel.fax === null ? '' : contactModel.fax
            );

            if (this.addItem) {
                this.contactService.createContact(contactModel).subscribe(
                    (res) => {
                        if (this.fromStateReferral) {
                            const data = JSON.parse(this.stateValues);
                            data.addEditScheduleData['referralId'] = res;
                            this.stateValues = JSON.stringify(data);
                            let fromStateReferral: NavigationExtras = {
                                state: {
                                    AppointmentTypeAdded: true,
                                    stateValues: this.stateValues
                                }
                            };
                            this.router.navigate(['/appointment'], fromStateReferral);
                        }
                        this.submitting = false;
                        this.alertService.displaySuccessMessage('Contact added successfully.');
                        this.blockUI.stop();
                        if (this.fromstate) {
                            if (this.fromstate.fromState == 'patient-contact-referral') {
                                let fromState: NavigationExtras = {
                                    state: {
                                        fromState: 'patient-contact-referral',
                                        referralId: res
                                    }
                                };
                                this.router.navigate(['/patients/edit/' + this.fromstate.patientId], fromState);
                            } else {
                                this.cancel();
                                el.scrollIntoView();
                            }
                        } else {
                            this.cancel();
                        }
                    },
                    () => {
                        this.alertService.displayErrorMessage(
                            // "Error occurred while adding Contact, please try again."
                            'Failed to add Referral, please try again later'
                        );
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    }
                );
            } else {
                contactModel.id = this.itemid;
                this.contactService.updateContact(contactModel).subscribe(
                    () => {
                        this.submitting = false;
                        this.alertService.displaySuccessMessage('Contact updated successfully.');
                        this.cancel();
                        el.scrollIntoView();
                        this.blockUI.stop();
                    },
                    () => {
                        this.alertService.displayErrorMessage(
                            'Failed to update Referral, please try again later'
                            // "Error occurred while updating Contact, please try again."
                        );
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    appIdTitleHandler($event) {
        this.titleId = $event;
    }

    // addressHandler($event) {
    //   this.addressManual = $event.name;
    //   if ($event !== undefined) {
    //     this.contactForm.get("country").patchValue($event.countryName);
    //     this.contactForm.get("state").patchValue($event.stateCode);
    //     this.contactForm.get("city").patchValue($event.cityName);
    //   }
    // }
    //
    // addressManualHandler($event) {
    //   this.addressManual = $event;
    // }

    onLocationChange(locationData) {
        if (locationData.length === 0) {
            this.groupedSpecialityData = groupBy([], [{field: 'locationName'}]);
            this.specialityData = [];
            this.contactForm.get('specialityName').patchValue('');
        } else {
            // this.removeSpeciality(locationData);
            // this.populateSpeciality(locationData);
        }
    }

    // removeSpeciality(locationData) {
    //   this.specialityDataTemp = [];
    //   const specialityNameData = this.contactForm.get("specialityName").value;
    //   if (specialityNameData !== null && specialityNameData.length > 0) {
    //     locationData.forEach((e) => {
    //       const speciality = specialityNameData.find(
    //         (x) => x.locationName === e.locationName
    //       );

    //       speciality.forEach((element) => {
    //         const sData = {
    //           id: element.id,
    //           idAndLocationId: element.idAndLocationId,
    //           specialityName: element.specialityName,
    //           locationId: element.locationId,
    //           locationName: element.locationName,
    //         };

    //         this.specialityDataTemp.push(sData);
    //       });
    //     });
    //     this.contactForm
    //       .get("specialityName")
    //       .patchValue(this.specialityDataTemp);
    //   }
    // }

    // populateSpeciality(locationData) {
    //   this.specialityData = [];
    //   locationData.forEach((e) => {
    //     this.specialityEnable = false;
    //     this.specialityAllList.forEach((s) => {
    //       const sLocation = s.specialityLocation.find(
    //         (x) => x.locationId === e.id
    //       );
    //       if (sLocation !== undefined) {
    //         const specialityDataCheck = this.specialityData.find(
    //           (x) => x.locationId === e.id && x.id === s.specialityId
    //         );

    //         const sData = {
    //           id: "",
    //           idAndLocationId: "",
    //           specialityName: "",
    //           locationId: "",
    //           locationName: "",
    //         };
    //         sData.id = s.specialityId.toString();
    //         sData.idAndLocationId = s.specialityId + sLocation.locationId;
    //         sData.specialityName =
    //           s.specialityName + " (" + sLocation.locationName + ")";
    //         sData.locationId = sLocation.locationId;
    //         sData.locationName = sLocation.locationName;
    //         if (specialityDataCheck === undefined) {
    //           this.specialityData.push(sData);
    //           this.groupedSpecialityData = groupBy(this.specialityData, [
    //             { field: "locationName" },
    //           ]);
    //         }
    //       }
    //     });
    //   });

    //   if (this.specialityData.length > 0) {
    //     this.specialityEnable = false;
    //   } else {
    //     this.specialityEnable = true;
    //   }
    // }
}
