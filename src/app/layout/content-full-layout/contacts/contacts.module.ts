import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactsRoutingModule } from './contacts-routing.module';
import {AppCommonModule} from '../common/app-common.module';
import {ContactsComponent} from './contacts.component';
import {AddReferralContactsComponent} from './add-referral/add-referral.component';
import {AddThirdPartyContactsComponent} from './add-third-party/add-third-party.component';
import {ManagingModule} from '../../module/managing.module';
import {BlockUIModule} from 'ng-block-ui';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {AddGeneralModule} from './add-general/add-general.module';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import {CharacterLimitModule} from '../../../directive/character-limit/character-limit.module';
import {AddressFieldModule} from '../shared-content-full-layout/address-field/address-field.module';
import {AccessDirectiveModule} from '../../../shared/directives/access-directive/access-directive.module';
import {OnlyNumberModule} from '../../../directive/only-number/only-number.module';

@NgModule({
  declarations: [
    ContactsComponent,
    AddReferralContactsComponent,
    AddThirdPartyContactsComponent,
  ],
    imports: [
        CommonModule,
        AppCommonModule,
        SharedContentFullLayoutModule,
        ManagingModule,
        ContactsRoutingModule,
        AddGeneralModule,
        MatSelectModule,
        FormsModule,
        BlockUIModule.forRoot(),
        CharacterLimitModule,
        AddressFieldModule,
        AccessDirectiveModule,
        OnlyNumberModule
    ],
})
export class ContactsModule { }
