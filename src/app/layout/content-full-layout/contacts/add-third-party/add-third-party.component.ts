import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BusinessService } from 'src/app/services/app.business.service';
import { ContactService } from '../../../../services/app.contact.service';
import { ApplicationDataService } from '../../../../services/app.applicationdata.service';
import { AppState } from '../../../../app.state';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { ContactLocationModel, ContactModel, } from '../../../../models/app.contact.model';
import { ApplicationDataEnum } from '../../../../enum/application-data-enum';
import { MiscService } from '../../../../services/app.misc.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {
  DeleteConfirmationDialogComponent
} from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';

@Component({
  selector: 'app-add-third-party-contacts',
  templateUrl: './add-third-party.component.html',
  styleUrls: ['./add-third-party.component.css'],
})
export class AddThirdPartyContactsComponent
  extends BaseItemComponent
  implements OnInit {
  readonly TP_MODULE = RP_MODULE_MAP.contacts_third_party;

  @BlockUI() blockUI: NgBlockUI;
  locationList: any[];
  titles: any[];
  titleData: any[];
  public filter: string;
  titleId: number;
  titleParentId: number;
  public expandCss: string;

  contactForm: FormGroup = new FormGroup({
    locationName: new FormControl('', Validators.required),
    organisationName: new FormControl('', Validators.required),
    departmentName: new FormControl(''),
    titleId: new FormControl(0),
    firstName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
    lastName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
    referenceNo: new FormControl(''),
    address: new FormControl(''),
    address2: new FormControl(''),
    country: new FormControl('Australia'),
    state: new FormControl(''),
    city: new FormControl(''),
    postCode: new FormControl(''),
    workPhone: new FormControl(''),
    mobile: new FormControl(''),
    fax: new FormControl(''),
    emailId: new FormControl(
      '',
      Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$')
    ),
    website: new FormControl('', [
      Validators.pattern(
        '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
      ),
    ]),
    notes: new FormControl(''),
    isAllowAllLocation: new FormControl(false),
    status: new FormControl(true),
  });
  countries: any[];
  deleteButton: Boolean;
  country: string;
  address: any;
  selectedAddress: string = '';
  addressManual: string;
  contactData: ContactModel;
  contactId: string;
  fromstate: any;
  fromStateThirdParty: any;
  stateValues: any;

  constructor(
    private businessService: BusinessService,
    protected appState: AppState,
    private _route: ActivatedRoute,
    private miscService: MiscService,
    public location: Location,
    public contactService: ContactService,
    public applicationDataService: ApplicationDataService,
    protected router: Router,
    protected alertService: AppAlertService,
    protected dialogService: AppDialogService,
  ) {
    super(location);
    this.fillTitle();
    if (this.router.getCurrentNavigation().extras.state != null) {
      this.fromstate = JSON.parse(this.router.getCurrentNavigation().extras.state.fromState);
    }
    if (this.router.getCurrentNavigation().extras.state != null) {
      this.fromStateThirdParty = this.router.getCurrentNavigation().extras.state.fromStateThirdParty;
      this.stateValues = this.fromStateThirdParty ? this.router.getCurrentNavigation().extras.state.stateValues : null;
    } else {
      this.fromStateThirdParty = history.state.fromStateThirdParty;
      this.stateValues = this.fromStateThirdParty ? history.state.stateValues : null;
    }
    if (this.fromStateThirdParty) {
      this.fromstate = null;
    }
  }

  ngOnInit() {

    this.contactData = new ContactModel();
    this.selectedAddress = this.contactForm.get('address').value;

    this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
      this.locationList = locations;
      this.locationPopulate();
    });

    this.titleParentId = ApplicationDataEnum.Title;
    this.miscService.getCountries().subscribe((countries) => {
      this.countries = countries;
    });

    this.expandCss = 'col-xl-6';
    this.country = 'au';
    this.deleteButton = false;
    this._route.params.subscribe((params) => {
      if (params.contactId) {
        this.blockUI.start();
        this.contactId = params.contactId;
        this.addItem = false;
        this.deleteButton = true;
        this.itemid = params.contactId;
        this.contactService
          .getContactById(params.contactId)
          .subscribe((data: ContactModel) => {
            this.contactForm.patchValue(data);
            this.contactData = data;
            this.titleId = data.titleId;
            this.selectedAddress = this.addressManual = data.address;

            // this.contactForm.patchValue(data.address);
            const locationName = [];
            if (this.locationList !== undefined) {
              data.contactLocation.forEach((pl) => {
                locationName.push(
                  this.locationList.find((l) => l.id === pl.locationId)
                );
              });
              this.contactForm.get('locationName').patchValue(locationName);
            }
          });
        this.blockUI.stop();
      }
    });
  }

  allLocationChange($event) {
    if ($event === true) {
      const locationName = [];
      if (this.locationList !== undefined) {
        this.locationList.forEach((l) => {
          locationName.push(l);
        });
        this.contactForm.get('locationName').patchValue(locationName);
      }
    } else {
      this.contactForm.get('locationName').patchValue('');
    }
  }

  deleteContact() {
    const ref = DeleteConfirmationDialogComponent.open(this.dialogService, '', {
      hideOnSave: false,
      title: 'Do you wish to delete this contact ?'
    });
    ref.clickSave.subscribe(() => {
      this.contactService.deleteContact(this.contactId).subscribe(
          (e) => {
            this.alertService.displaySuccessMessage('Contact added successfully.');
            this.cancel();
            this.blockUI.stop();
            ref.closeDialog();
          },
          () => {
            this.alertService.displayErrorMessage(
                // 'Error occurred while adding Contact, please try again.'
                'Failed to delete Referral, please try again later'
            );
            this.blockUI.stop();
          }
      );
    });
  }


  locationPopulate() {
    const locationName = [];
    this.miscService.getCountries().subscribe((countries) => {
      const c = this.locationList.find(
        (l) =>
          l.locationName === this.appState.selectedUserLocation.locationName
      );

      const country_Code = countries.find((x) => x.country_Name === c.country)
        .country_Code;

      this.country = country_Code.toLowerCase();
      console.log(country_Code.toLowerCase());
      this.contactForm.get('country').patchValue(c.country);
    });
    if (this.contactData.contactLocation !== undefined) {
      this.contactData.contactLocation.forEach((pl) => {
        locationName.push(
          this.locationList.find((l) => l.id === pl.locationId)
        );
      });
      this.contactForm.get('locationName').patchValue(locationName);
    }
  }

  handleFilter(value) {
    if (this.titles !== undefined && this.titles.length > 0) {
      this.titleData = this.titles.filter(
        (s) => s.categoryName.toLowerCase().indexOf(value.toLowerCase()) !== -1
      );
    }
    this.filter = value;
  }

  handleCategoryChange(value) {
    this.titleId = value;
  }

  fillTitle() {
    this.applicationDataService
      .GetApplicationDataByCategoryId(
        ApplicationDataEnum.Title,
        this.appState.selectedUserLocation.id
      )
      .subscribe((data) => {
        this.titles = this.titleData = data;
      });
  }

  addressHandler($event) {
    this.addressManual = $event.name;
    if ($event !== undefined) {
      this.contactForm.get('country').patchValue($event.countryName);
      this.contactForm.get('state').patchValue($event.stateCode);
      this.contactForm.get('city').patchValue($event.cityName);
    }
  }

  addressManualHandler($event) {
    this.addressManual = $event;
  }

  createThirdPartyContact() {

    const el = document.getElementById('heading');
    const contactModel: ContactModel = this.contactForm.value;
    const locations: ContactLocationModel[] = [];

    if (!this.contactForm.invalid) {
      this.blockUI.start();
      this.submitting = true;

      if (contactModel.locationName !== '') {
        contactModel.locationName.forEach((l) => {
          const model = new ContactLocationModel();
          model.locationId = l.id;
          model.contactId = this.itemid;
          locations.push(model);
        });
      }

      // mytest
      contactModel.address = this.addressManual = this.contactForm.get('address').value;
      contactModel.contactLocation = locations;
      // contactModel.address = this.addressManual;

      contactModel.contactType = 2;
      contactModel.titleId = this.titleId;
      contactModel.postCode = String(
        contactModel.postCode === null ? '' : contactModel.postCode
      );
      contactModel.workPhone = String(
        contactModel.workPhone === null ? '' : contactModel.workPhone
      );
      contactModel.mobile = String(
        contactModel.mobile === null ? '' : contactModel.mobile
      );
      contactModel.fax = String(
        contactModel.fax === null ? '' : contactModel.fax
      );

      if (this.addItem) {
        this.contactService.createContact(contactModel).subscribe(
          (res) => {
            if (this.fromStateThirdParty) {
              const data = JSON.parse(this.stateValues);
              data.addEditScheduleData['payerId'] = res;
              this.stateValues = JSON.stringify(data);
              let fromStateThirdParty: NavigationExtras = {
                state: {
                  AppointmentTypeAdded: true,
                  stateValues: this.stateValues
                }
              };
              this.router.navigate(['/appointment'], fromStateThirdParty);
            }
            this.submitting = false;
            this.alertService.displaySuccessMessage('Contact added successfully.');
            this.blockUI.stop();
            if (this.fromstate) {
              if (this.fromstate.fromState == 'patient-contact-third-party') {
                let fromState: NavigationExtras = {
                  state: {
                    fromState: 'patient-contact-third-party',
                    thirdPartyId: res
                  }
                };
                this.router.navigate(['/patients/edit/' + this.fromstate.patientId], fromState);
                return;
              } else {
                this.cancel();
                el.scrollIntoView();
              }
            }
            this.cancel();
          },
          (error) => {
            this.alertService.displayErrorMessage(
              // 'Error occurred while adding Contact, please try again.'
              error || 'Failed to add Referral, please try again later'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      } else {
        contactModel.id = this.itemid;
        this.contactService.updateContact(contactModel).subscribe(
          () => {
            this.submitting = false;
            this.alertService.displaySuccessMessage('Contact updated successfully.');
            this.cancel();
            el.scrollIntoView();
            this.blockUI.stop();
          },
          (error) => {
            this.displayErrorMessage(
              // 'Error occurred while updating Contact, please try again.'
              error || 'Failed to update Referral, please try again later'
            );
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
          }
        );
      }
    }
  }


  appIdTitleHandler($event) {
    this.titleId = $event;
  }
}
