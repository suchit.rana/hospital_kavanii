import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddGeneralContactsComponent} from './add-general.component';
import {BlockUIModule} from 'ng-block-ui';
import {ManagingModule} from '../../../module/managing.module';
import {SharedContentFullLayoutModule} from '../../shared-content-full-layout/shared-content-full-layout.module';
import {CharacterLimitModule} from '../../../../directive/character-limit/character-limit.module';
import {AddressFieldModule} from '../../shared-content-full-layout/address-field/address-field.module';
import {AccessDirectiveModule} from '../../../../shared/directives/access-directive/access-directive.module';
import {OnlyNumberModule} from '../../../../directive/only-number/only-number.module';


@NgModule({
  declarations: [
    AddGeneralContactsComponent
  ],
    imports: [
        CommonModule,
        SharedContentFullLayoutModule,
        BlockUIModule.forRoot(),
        ManagingModule,
        CharacterLimitModule,
        AddressFieldModule,
        AccessDirectiveModule,
        OnlyNumberModule
    ],
  exports: [
    AddGeneralContactsComponent
  ]
})
export class AddGeneralModule {
}
