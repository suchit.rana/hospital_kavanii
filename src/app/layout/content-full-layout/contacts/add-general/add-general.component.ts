import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BusinessService} from 'src/app/services/app.business.service';
import {ContactService} from '../../../../services/app.contact.service';
import {ApplicationDataService} from '../../../../services/app.applicationdata.service';
import {AppState} from '../../../../app.state';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute} from '@angular/router';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {ContactLocationModel, ContactModel,} from '../../../../models/app.contact.model';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';
import {MiscService} from '../../../../services/app.misc.service';
import {DashboardService} from 'src/app/services/app.dashboard.service';
import {DashboardModel} from 'src/app/models/app.dashboard.model';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';
import {
    DeleteConfirmationDialogComponent
} from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../shared/services/app-alert.service';

@Component({
    selector: 'app-add-general-contacts',
    templateUrl: './add-general.component.html',
    styleUrls: ['./add-general.component.css'],
})
export class AddGeneralContactsComponent
    extends BaseItemComponent
    implements OnInit {
    readonly GENERAL_MODULE = RP_MODULE_MAP.contacts_general_contacts;

    @BlockUI() blockUI: NgBlockUI;
    titleParentId: number = ApplicationDataEnum.Title;
    locationList: any[];
    titles: any[];
    titleData: any[];
    public contactFilter: string;
    titleId: number;
    categoryId: number;
    public expandCss: string;
    countries: any[];
    @Input() private contact: EventEmitter<string>;
    @Input() public generalType;
    deleteButton: Boolean;
    country: string;
    contactId: string;

    contactForm: FormGroup = new FormGroup({
        locationName: new FormControl('', Validators.required),
        organisationName: new FormControl('', Validators.required),
        departmentName: new FormControl(''),
        categoryId: new FormControl('', Validators.required),
        titleId: new FormControl(0),
        firstName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
        lastName: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$'), Validators.required]),
        referenceNo: new FormControl(''),
        address: new FormControl(''),
        address2: new FormControl(''),
        country: new FormControl('Australia'),
        state: new FormControl(''),
        city: new FormControl(''),
        postCode: new FormControl(''),
        workPhone: new FormControl(''),
        mobile: new FormControl(''),
        fax: new FormControl(''),
        emailId: new FormControl(
            '',
            Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$')
        ),
        website: new FormControl('', [
            Validators.pattern(
                '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
            ),
        ]),
        notes: new FormControl(''),
        isAllowAllLocation: new FormControl(false),
        status: new FormControl(true),
    });

    @Output('parentProductCallBack')
    parentProductCallBack: EventEmitter<any> = new EventEmitter();
    contactCategoryId: number;
    contactCategoryParentId: number;
    selectedAddress: string;
    addressManual: any;
    contactData: ContactModel;
    isDisabled: boolean;
    private dashboard: DashboardModel;
    taxOptions = [
        {
            id: 1,
            name: 'Inclusive',
        },
        {
            id: 2,
            name: 'Exclusive',
        },
    ];

    constructor(
        private businessService: BusinessService,
        protected appState: AppState,
        private _route: ActivatedRoute,
        private miscService: MiscService,
        public location: Location,
        public contactService: ContactService,
        public applicationDataService: ApplicationDataService,
        private dashboardService: DashboardService,
        private dialogService: AppDialogService,
        private alertService: AppAlertService,
    ) {
        super(location);
        this.fillTitle();
        this.fillContactCategory();
    }

    ngOnInit() {
        this.isDisabled = false;
        this.contactData = new ContactModel();
        this.selectedAddress = this.contactForm.get('address').value;
        console.log('first', this.selectedAddress);

        this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
            this.locationList = locations;
            this.locationPopulate();
        });

        this.dashboardService.getDashboardCheckListView().subscribe(d => {
            this.dashboard = d;
        });

        this.   contactCategoryParentId = ApplicationDataEnum.ContactCategory;
        this.contactForm.get('status').patchValue(true);

        this.miscService.getCountries().subscribe((countries) => {
            this.countries = countries;
        });
        this.expandCss = 'col-xl-6';

        if (this.contact !== undefined) {
            this.contact.subscribe((e) => {
                this.expandCss = e;
            });
        }

        this.deleteButton = false;
        this._route.params.subscribe((params) => {
            if (params.contactId) {
                this.blockUI.start();
                this.addItem = false;
                this.itemid = params.contactId;
                this.contactId = params.contactId;
                this.deleteButton = true;
                this.contactService
                    .getContactById(params.contactId)
                    .subscribe((data: ContactModel) => {
                        this.contactForm.patchValue(data);
                        // this.contactForm.get('address').patchValue(data.address);
                        this.contactData = data;

                        // test
                        // console.log('form',this.contactData,"?",data ,"data" ,data.address,data.address2)
                        this.contactCategoryId = data.categoryId;

                        this.selectedAddress = this.addressManual = data.address;

                        const locationName = [];
                        if (this.locationList !== undefined) {
                            data.contactLocation.forEach((pl) => {
                                locationName.push(
                                    this.locationList.find((l) => l.id === pl.locationId)
                                );
                            });
                            this.contactForm.get('locationName').patchValue(locationName);
                        }
                    });
                this.blockUI.stop();
            }
        });
    }

    allLocationChange($event) {
        if ($event === true) {
            const locationName = [];
            if (this.locationList !== undefined) {
                this.locationList.forEach((l) => {
                    locationName.push(l);
                });
                this.contactForm.get('locationName').patchValue(locationName);
            }
        } else {
            this.contactForm.get('locationName').patchValue('');
        }
    }

    locationPopulate() {
        const locationName = [];

        this.miscService.getCountries().subscribe((countries) => {
            const c = this.locationList.find(
                (l) =>
                    l.locationName === this.appState.selectedUserLocation.locationName
            );

            const country_Code = countries ? countries.find((x) => x.country_Name === c.country).country_Code : [];

            this.country = country_Code.toLowerCase();
            this.contactForm.get('country').patchValue(c.country);
        });

        if (this.contactData.contactLocation !== undefined) {
            this.contactData.contactLocation.forEach((pl) => {
                locationName.push(
                    this.locationList.find((l) => l.id === pl.locationId)
                );
            });
            this.contactForm.get('locationName').patchValue(locationName);
        }
    }

    // addressHandler($event) {
    //   this.addressManual = $event.name;
    //   if ($event !== undefined) {
    //     this.contactForm.get('country').patchValue($event.countryName);
    //     this.contactForm.get('state').patchValue($event.stateCode);
    //     this.contactForm.get('city').patchValue($event.cityName);
    //   }
    // }
    //
    // addressManualHandler($event) {
    //   this.addressManual = $event;
    // }

    fillTitle() {
        this.applicationDataService
            .GetApplicationDataByCategoryId(
                ApplicationDataEnum.Title,
                this.appState.selectedUserLocationId
            )
            .subscribe((data) => {
                this.titles = this.titleData = data;
            });
    }

    fillContactCategory() {
        this.applicationDataService
            .GetApplicationDataByCategoryId(
                ApplicationDataEnum.ContactCategory,
                this.appState.selectedUserLocationId
            )
            .subscribe((data) => {
                if (this.generalType !== undefined) {
                    const catId = data.find((x) => x.categoryName === this.generalType);
                    this.contactCategoryId = catId.id;
                    //this.isDisabled = true;
                }
            });
    }

    deleteContact() {
        const ref = DeleteConfirmationDialogComponent.open(this.dialogService, '', {
            hideOnSave: false,
            title: 'Do you wish to delete this contact ?'
        });
        ref.clickSave.subscribe(() => {
            this.contactService.deleteContact(this.contactId).subscribe(
                () => {
                    this.alertService.displaySuccessMessage('Contact deleted successfully.');
                    this.cancel();
                    this.blockUI.stop();
                    ref.closeDialog();
                },
                (error) => {
                    // this.displayErrorMessage(error || 'Failed to delete General Contact, please try again later');
                    this.alertService.displayErrorMessage(error || 'Failed to delete Referral, please try again later');
                    this.blockUI.stop();
                    ref.closeDialog();
                }
            );
        });
    }

    createGenaeralContact() {
        const el = document.getElementById('heading');
        const contactModel: ContactModel = this.contactForm.value;

        const locations: ContactLocationModel[] = [];
        if (!this.contactForm.invalid) {
            this.blockUI.start();
            this.submitting = true;

            if (contactModel.locationName !== '') {
                contactModel.locationName.forEach((l) => {
                    const model = new ContactLocationModel();
                    model.locationId = l.id;
                    model.contactId = this.itemid;
                    locations.push(model);
                });
            }

            if (
                this.contactCategoryId === undefined ||
                this.contactCategoryId === null
            ) {
                this.alertService.displayErrorMessage('Contact Category is Mandatory.');
                this.submitting = false;
                this.blockUI.stop();
                return;
            }
            // contactModel.address = this.addressManual;

            // mytest
            contactModel.address = this.addressManual = this.contactForm.get('address').value;
            ;
            console.log('add??', contactModel.address);
            contactModel.contactLocation = locations;
            contactModel.contactType = 3;
            // contactModel.categoryId = this.contactCategoryId;
            contactModel.titleId = this.titleId;
            contactModel.postCode = String(
                contactModel.postCode === null ? '' : contactModel.postCode
            );
            contactModel.workPhone = String(
                contactModel.workPhone === null ? '' : contactModel.workPhone
            );
            contactModel.mobile = String(
                contactModel.mobile === null ? '' : contactModel.mobile
            );
            contactModel.fax = String(
                contactModel.fax === null ? '' : contactModel.fax
            );
            if (this.addItem) {
                this.contactService.createContact(contactModel).subscribe(
                    (e) => {
                        this.dashboard.isContactAdded = true;
                        this.dashboardService.updateDashboardCheckList(this.dashboard).subscribe(u => {
                            this.submitting = false;
                            if (this.contact !== undefined) {
                                this.parentProductCallBack.emit(e);

                                this.cancelGenaeral();
                            } else {
                                this.alertService.displaySuccessMessage('Contact added successfully.');
                                this.cancel();
                            }
                            el.scrollIntoView();
                            this.contactForm.reset();
                            this.blockUI.stop();
                        }, error => {
                            this.alertService.displayErrorMessage(
                                // 'Error occurred while adding Contact, please try again.'
                                'Failed to add General, please try again later'
                            );
                            this.submitting = false;
                            el.scrollIntoView();
                            this.blockUI.stop();
                        });

                    },
                    () => {
                        this.alertService.displayErrorMessage(
                            // 'Error occurred while adding Contact, please try again.'
                            'Failed to add General, please try again later'
                        );
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    }
                );
            } else {
                contactModel.id = this.itemid;
                this.contactService.updateContact(contactModel).subscribe(
                    (e) => {
                        this.submitting = false;
                        this.parentProductCallBack.emit(e);
                        this.contactForm.reset();
                        this.cancel();
                        el.scrollIntoView();
                        this.blockUI.stop();
                    },
                    () => {
                        this.alertService.displayErrorMessage(
                            // 'Error occurred while updating Contact, please try again.'
                            'Failed to update General, please try again later'
                        );
                        this.submitting = false;
                        el.scrollIntoView();
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    cancelGenaeral() {
        if (document.getElementById('sliderShadow').style.display === 'none') {
            this.cancel();
        } else {
            document.getElementById('slider').style.width = '0px';
            document.getElementById('sliderShadow').style.display = 'none';
        }
    }

    appIdTitleHandler($event) {
        this.titleId = $event;
    }

    appIdContactCategoryHandler($event: number) {
        this.contactCategoryId = $event;
    }
}
