import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddReferralContactsComponent} from './add-referral/add-referral.component';
import {AddThirdPartyContactsComponent} from './add-third-party/add-third-party.component';
import {AddGeneralContactsComponent} from './add-general/add-general.component';
import {ContactsComponent} from './contacts.component';


const routes: Routes = [
  {path: '', component: ContactsComponent},
  {path: 'referral/add', component: AddReferralContactsComponent},
  {path: 'referral/edit/:contactId', component: AddReferralContactsComponent},
  {path: 'thirdparty/add', component: AddThirdPartyContactsComponent},
  {path: 'thirdparty/edit/:contactId', component: AddThirdPartyContactsComponent},
  {path: 'general/add', component: AddGeneralContactsComponent},
  {path: 'general/edit/:contactId', component: AddGeneralContactsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule {
}
