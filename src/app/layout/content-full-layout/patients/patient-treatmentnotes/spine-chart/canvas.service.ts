import 'fabric';
import { BehaviorSubject } from 'rxjs';
declare const fabric: any;

const objectConfig = {
    cornerColor: '#ffffff',
    borderColor: '#ffffff',
    cornerSize: 12,
    cornerStrokeColor: '#ffffff',
    cornerStyle: 'circle',
    hasControls: true,
    transparentCorners: false,
    cornerShadowColor: '#35394188',
    strokeShadowBlur: 2,
    cornerShadowBlur: 10,
    cornerShadowOffsetX: 0,
    cornerShadowOffsetY: 0,
    lockUniScaling: false,
    borderOpacityWhenMoving: 0,
    centeredScaling: true,
    centeredRotation: true,
    lockScalingFlip: true,
    snapAngle: 45,
    snapThreshold: 5,
    controls: {}
};

fabric.Object.prototype.set(objectConfig);

export class canvasService {

    private EVENT_OBJECT_SELECTED = new BehaviorSubject(Event);
    onObjectSelected = this.EVENT_OBJECT_SELECTED.asObservable();
    private EVENT_OBJECT_SELECTION_CLEARED = new BehaviorSubject(Event);
    onObjectDeSelected = this.EVENT_OBJECT_SELECTION_CLEARED.asObservable();

    canvas: any;
    isEraser: boolean = false;
    canvasProps: any = {
        canvasWidth: 1080,
        canvasHeight: 1920
    };
    feature: any = {
        pencil: false,
        pen: false,
        marker: false,
        highlighter: false,
        eraser: false,
        clearAll: false
    };

    historyCallback: any;

    constructor(id, options) {
        this.canvas = new fabric.Canvas(id, {
            width: options.width || this.canvasProps.canvasWidth,
            height: options.height || this.canvasProps.canvasHeight,
            _width: options.width || this.canvasProps.canvasWidth,
            _height: options.height || this.canvasProps.canvasHeight,
            preserveObjectStacking: true,
            selection: false,
            isDrawingMode: true,
            ...options
        });
        this.historyCallback = options.historyCallback;
        console.log("drasing mode", options);
        this.canvas.freeDrawingBrush = new fabric.PencilBrush(this.canvas);
        this.canvas.freeDrawingBrush.width = options.lineWidth;
        this.canvas.freeDrawingBrush.color = options.lineColor;
        this.AddEvents();
    }

    async AddEvents() {
        this.canvas.on({
            'path:created': (e) => {
                this.historyCallback();
                if (this.isEraser == true) {
                    e.path.set({
                        selectable: false,
                        evented: false,
                        stroke: '#3AC9A2',
                        globalCompositeOperation: 'destination-out',
                        lockMovementX: true,
                        lockMovementY: true,
                        lockScalingX: true,
                        lockScalingY: true,
                        lockUniScaling: true,
                        lockRotation: true,
                    })
                    this.canvas.renderAll();
                }
            }
        });
    }

    disableEraser() {
        this.isEraser = false
    }

    enableEraser() {
        this.isEraser = true;
    }

    async getOriginalCanvasSize() {
        return {
            width: this.canvas.getWidth() / this.canvas.getZoom(),
            height: this.canvas.getHeight() / this.canvas.getZoom(),
            zoom: this.canvas.getZoom()
        };
    }

    async getCanvas() {
        return this.canvas;
    }

    async renderAll() {
        this.canvas.renderAll();
    }

    async clearSelection() {
        this.canvas.discardActiveObject().renderAll();
    }

    setBackground(image): Promise<any> {
        return new Promise((resolve, reject) => {
            fabric.util.loadImage(image.src, (img) => {
                const background = new fabric.Pattern({
                    source: img,
                    repeat: 'no-repeat',
                    scaleX: this.canvas.width / image.width,
                    scaleY: this.canvas.height / image.height,
                });
                this.canvas.backgroundColor = background;
                this.canvas.renderAll();
                resolve(null);
            });
        })
    }



} 