import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import 'fabric';
import { exit } from 'process';
import { editor } from '../editor.class';
declare const fabric: any;

interface SPChartData {
  canvasInstance?: any,
  spineNotes?: string,
  finalImage?: string
}

@Component({
  selector: 'app-spine-chart',
  templateUrl: './spine-chart.component.html',
  styleUrls: ['./spine-chart.component.css']
})
export class SpineChartComponent extends editor implements OnInit {

  @Input() ngData: SPChartData = {};
  @Output() deleteChartClick: EventEmitter<any> = new EventEmitter();
  @Output() moveDown: EventEmitter<any> = new EventEmitter();
  @Output() moveUp: EventEmitter<any> = new EventEmitter();
  leftSpinenotSelect = 0;
  rightSpinenotSelect = 0;
  loading: boolean = false;
  randomCanvasId: string;
  data: any;
  isTitleInEditingSpine: boolean = false;
  isClearSketchDialog: boolean = false;
  imageUrl: any = '';
  dummyData: SPChartData = {
    spineNotes: ""
  };
  opendeleteform: boolean;
  constructor() {
    super();
    this.randomCanvasId = 'canvas_' + JSON.stringify(Math.round(Math.random() * 100000000));
    this.defaultSelectedToolName = "pencil";
    this.generatePreview()
  }

  ngOnInit() {
    this.data = { ...this.ngData }
    let x = this.data.chartImage.image.split("/");
    this.loading = true;
    this.setDefaultData();
    setTimeout(() => {
      this.initCanvas(this.randomCanvasId, {
        width: 1080,
        height: 1080,
        _width: 1080,
        _height: 1080,
        preserveObjectStacking: true
      }).then(_result => {
        if (x[4] == 'dropfileimage.png') {
          this.imageUrl = "./assets/spine.jpg";
          this.setBackgroundImage(this.imageUrl).then(image => {
            this.resizeCanvasToSize(image).then(_newCanvas => {
              this.enableDrawingMode().then(_result => {
                this.resetTools();
                this.initBrush().then(_result => {
                  this.onPathCreatedEvent()
                  this.putHistory();
                  this.on(['undo', 'redo']);
                  this.loading = false;
                });
              });
            })
          })
        }
        else {
          this.imageUrl = this.data.chartImage.image;
          this.setBackgroundImage(this.imageUrl).then(image => {
            this.resizeCanvasToSize(image).then(_newCanvas => {
              this.enableDrawingMode().then(_result => {
                this.resetTools();
                this.initBrush().then(_result => {
                  this.onPathCreatedEvent()
                  this.putHistory();
                  this.on(['undo', 'redo']);
                  this.loading = false;
                });
              });
            })
          })
        }
      });
    }, 0);
  }

  doMoveDown() {
    this.moveDown.emit();
  }
  doMoveUp() {
    this.moveUp.emit();
  }

  setDefaultData() {
    !this.data.itemLabel ? this.data.itemLabel = "Spine" : null;
    !this.data.helpText ? this.data.helpText = "Draw or type notes on the provided Body Chart or any image of your choice" : null;
    !this.data.isMendatory ? this.data.isMendatory = true : false;
    !this.data.spineNotes ? this.data.spineNotes = "" : null;
    !this.data.leftData ? this.data.leftData = this.data.leftData.map(item => { return { label: item, value: false } }) : null;
    !this.data.rightData ? this.data.rightData = this.data.leftData.map(item => { return { label: item, value: false } }) : null;
  }

  editTitle() {
    this.isTitleInEditingSpine = true;
    this.dummyData = { ...this.data }
  }

  closeClearSketchDialog(state) {
    this.isClearSketchDialog = false;
    if (state != "cancel") {
      this.clearAllSketch();
    }
  }

  closeTitleEditing(state) {
    this.isTitleInEditingSpine = false;
    if (state == "cancel") {
      this.data = { ...this.dummyData }
    }
    else {
      // Call api here for save
    }
  }

  changeMendatory($event) {
    console.log("event", $event);
  }
  deleteChartForm() {
    this.opendeleteform = true;
    this.deleteChartClick.emit();
  }
  deleteChart() {
    // delete chart
    this.isTitleInEditingSpine = false;
    this.opendeleteform = true;
    this.deleteChartClick.emit();
  }
  deleteitem(type) {
    if (type == 'delete') {
      this.opendeleteform = false;
      this.deleteChartClick.emit();
    }
    else {
      this.opendeleteform = false;
    }
  }
  saveTitleChanges() {
    this.isTitleInEditingSpine = false;
  }

  openFilePicker() {
    document.getElementById("upload").click();
  }

  enableEraserTool() {
    this.activateDrawingTool('eraser');
    this.canvas.isDrawingMode = false;
    this.canvas.isDrawingMode = true;
    fabric.PencilBrush.prototype.globalCompositeOperation = "destination-out";
    this.canvas.renderAll();
  }

  clearAllSketch() {
    this.disableDrawingMode();
    this.canvas.getObjects().forEach(object => {
      this.canvas.remove(object);
    });
    this.enableDrawingMode();
    this.resetTools();
  }
  leftcheckClick(event) {
    console.log(event)
  }

  generatePreview() {
    this.generateImage(this.imageUrl).then(result => {
      this.data.chartImage.image = result;
      console.log(this.data)
      let Leftcount = 0;
      for (let index = 0; index < this.data.leftData.length; index++) {
        if (this.data.leftData[index].value == true) {
          Leftcount++;
        }
      }
      if (Leftcount == 0) {
        this.leftSpinenotSelect = 1;
      } else {
        this.leftSpinenotSelect = 0;
      }
      console.log(Leftcount)
      let Rightcount = 0;
      for (let index = 0; index < this.data.rightData.length; index++) {
        if (this.data.rightData[index].value == true) {
          Rightcount++;
        }
      }
      if (Rightcount == 0) {
        this.rightSpinenotSelect = 1;
      } else {
        this.rightSpinenotSelect = 0;
      }
    })
  }
  getbodydata() {
    this.generatePreview()
  }
  mandatoryValueChange($event: { checked: boolean }) {
    this.data.isMandatory = $event.checked
  }
  public toggle(dataItem) {
    if (this.data.isMendatory == true) {
      console.log(dataItem)
    }

  }
  onChange(e, from) {
    console.log(e)
    // if (from == 'left') {
    //   this.leftArray = []
    //   for (let index = 0; index < this.data.leftData.length; index++) {
    //     if (this.data.leftData[index].value == true) {
    //       this.leftArray.push(this.data.leftData[index])
    //     }
    //   }
    // }
    // if (from == 'right') {
    //   this.rightArray = []
    //   for (let index = 0; index < this.data.leftData.length; index++) {
    //     if (this.data.leftData[index].value == true) {
    //       this.rightArray.push(this.data.leftData[index])
    //     }
    //   }
    // }
  }
  close() {
    this.isTitleInEditingSpine = false;
  }
}
