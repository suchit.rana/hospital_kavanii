import { Component, ElementRef, HostListener, Input, OnChanges, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ImageSnippet, MessageType } from 'src/app/models/app.misc';
import {
  CheckBoxModel,
  DateTimeModel,
  DropdownModel,
  ItemTypeModel,
  KeySubjectModel,
  KeyValueModel,
  PatinetTreatmentNotesModel,
  RadioButtonModel,
} from 'src/app/models/app.treatmentnotes.model';
// import '@vaadin/vaadin-date-time-picker/vaadin-date-time-picker.js';
import '@vaadin/vaadin-time-picker/vaadin-time-picker.js';
import * as _ from 'lodash';
import * as moment from 'moment';
import { PatientService } from '../../../../../services/app.patient.service';

// import { PickerInteractionMode } from 'igniteui-angular';
import { AppUtils } from '../../../../../shared/AppUtils';
interface ComponentEvent {
  name: string;
}

export class Dte {
  date: Date;
  text: string;
  id: string = '0';
  isDateTemplate: boolean = true;


  constructor(date: Date) {
    this.date = date;
    this.text = date.toDateString();
  }
}

@Component({
  selector: 'app-item-type-preview',
  templateUrl: './item-type-preview.component.html',
  styleUrls: ['../patient-treatmentnotes.component.css'],
})
export class ItemTypePreviewComponent implements OnChanges {
  isCalendarOpen: boolean = false;
  isCalendarOpenDuplicate: boolean = false;
  currentView = 'day';
  public calendarPopUpMargin = { horizontal: -60, vertical: 10 };
  @ViewChild('calendarOpenBtn', { static: false }) public anchor: ElementRef;
  firstDay: number = 1;
  currentDate: Date = moment().toDate();
  currentDuplicateDate: Date = moment().toDate();
  dateFormat: string = 'ddd, D MMM YYYY';
  maxDOBDate = this.currentDate;
  currHvrDateStart: Date;
  currHvrDateend: Date;
  hoverDate: Date = new Date();
  selectedDate: Dte[];
  vitalsChangeStatus = 0;
  data = `<p>test</p>`;

  @ViewChildren('dateInput') dateInputElem: QueryList<ElementRef>;


  @Input() treatmentNotes: PatinetTreatmentNotesModel;
  @Input() treatmentNoteForm: FormGroup;
  @Input() showflag: boolean;
  // public mode: PickerInteractionMode = PickerInteractionMode.DropDown;
  // public format = 'hh:mm tt';
  // public date: Date = new Date();
  vitalModel: any = {
    selectedVitalValue: null
  };
  selectedArray: any = [];
  selectedCheckBoxArray: any = [];
  selectedCheckBoxDuplicateArray: any = [];
  templatedataLength: number;
  apperance = 'outline';
  isActive: boolean = false;
  selectedId: any;
  openheadingeditform: boolean = false;
  selectedFile?: ImageSnippet;
  getFile: any;
  file: File;
  isDisplay: boolean = true;
  titleData: any[];
  titleDataduplicate: any[];
  public selectedValue: number;
  displaySuccessMessage: string;
  messageType = MessageType;
  public dateValue: Date = new Date(2000, 2, 10);
  public shortFormat = ' dd MMM yyy';
  heightvlue: any;
  weightValue: any;
  selectedItemsList = [];
  keyValue: any;
  itemValue: any;
  subjectValue: any;
  choiceValue: any;
  selectedOption: boolean;
  readonly: boolean;
  openheadingeditform1: boolean = false;
  numberModel: any = {
    number: {
      minimum: '',
      maximum: '',
      default: '',
    },
    numberDuplicate: {
      minimum: '',
      maximum: '',
      default: '',
    }
  };
  rangeScaleModel: any = {
    rangeOrScale: {
      minimum: '',
      maximum: '',
      default: '',
    },
    rangeOrScaleDuplicate: {
      minimum: '',
      maximum: '',
      default: '',
    }
  };
  optionListStyles = [
    {
      id: 1,
      type: 'Button',
    },
    {
      id: 2,
      type: 'Checkbox',
    },
    {
      id: 3,
      type: 'Dropdown',
    },
    {
      id: 4,
      type: 'Radio button',
    },
  ];
  maxTableRowsColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  vitalsLabel: string;
  spine = [
    {
      id: 1,
      name: 'C0',
    },
    {
      id: 2,
      name: 'C1',
    },
    {
      id: 3,
      name: 'C2',
    },
    {
      id: 4,
      name: 'C3',
    },
    {
      id: 5,
      name: 'C4',
    },
    {
      id: 6,
      name: 'C5',
    },
    {
      id: 7,
      name: 'C6',
    },
    {
      id: 8,
      name: 'C7',
    },
    {
      id: 9,
      name: 'T1',
    },
    {
      id: 10,
      name: 'T2',
    },
    {
      id: 11,
      name: 'T3',
    },
    {
      id: 12,
      name: 'T4',
    },
    {
      id: 13,
      name: 'T5',
    },
    {
      id: 14,
      name: 'T6',
    },
    {
      id: 15,
      name: 'T7',
    },
    {
      id: 16,
      name: 'T8',
    },
    {
      id: 17,
      name: 'T9',
    },
    {
      id: 18,
      name: 'T10',
    },
    {
      id: 19,
      name: 'T11',
    },
    {
      id: 20,
      name: 'T12',
    },
    {
      id: 21,
      name: 'L1',
    },
    {
      id: 22,
      name: 'L2',
    },
    {
      id: 23,
      name: 'L3',
    },
    {
      id: 24,
      name: 'L4',
    },
    {
      id: 25,
      name: 'L5',
    },
    {
      id: 26,
      name: 'Sacrum',
    },
    {
      id: 27,
      name: 'SI AS',
    },
    {
      id: 28,
      name: 'SI PI',
    },
    {
      id: 29,
      name: 'Coccyx',
    },
  ];
  metric = [
    {
      id: 1,
      name: 'Body Temp (C)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 2,
      name: 'Pulse (BPM)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 3,
      name: 'Resp. Rate (bpm)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 4,
      name: 'BP (mm/ Hg)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 5,
      name: 'Weight (Kg)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 6,
      name: 'Height (cm)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 7,
      name: 'BMI (kg/m2)',
      value: '',
      disable: true,
      isChecked: true
    },
  ];

  imperial = [
    {
      id: 1,
      name: 'Body Temp (F)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 2,
      name: 'Pulse (BPM)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 3,
      name: 'Resp. Rate (bpm)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 4,
      name: 'BP (mm/ Hg)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 5,
      name: 'Weight (lbs)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 6,
      name: 'Height (in)',
      value: '',
      disable: false,
      isChecked: true
    },
    {
      id: 7,
      name: 'BMI (lb/in2)',
      value: '',
      disable: true,
      isChecked: true

    },
  ];
  unitOfMeasurement: { id: number; name: string, isChecked: boolean; value: string; disable: boolean }[];
  public step = 1;
  private numbers = [];
  public value: number;
  public min: number = 0;
  public max: number = 1;
  public valueDuplicate: number;
  public minDuplicate: number;
  public maxDuplicate: number;
  // public interval: any = { minute: 15 };
  ticks = Date.now().valueOf();
  openeditform: boolean;
  opendeleteform: boolean;
  opendeletenote: boolean;
  openexportnote: boolean;
  openexportnotepreview: boolean;
  openemailnoteemail: boolean;
  openfilterpractitioner: boolean;
  openfiltercase: boolean;
  opencasestatusform: boolean;
  openchiefeditform: boolean;
  openatatchfileditform: boolean;
  openoptionlisteditform: boolean;
  openrangeeditform: boolean;
  openfilterdaterange: boolean;
  showchangedatetime: boolean;
  showchangepatient: boolean;
  showchangepatientcase: boolean;
  showchangepatientnote: boolean;
  showchangepatientdelete: boolean;
  openChiedform: boolean;
  openvitaleditform: boolean;
  showopticalarea: boolean;
  opencheckboxeditform: boolean;
  openradioeditform: boolean;
  opendischargeform: boolean;
  opendroupdwoneditform: boolean;
  opennumbereditform: boolean;
  opendatetimeeditform: boolean;
  opentabledataeditform: boolean;
  openinstructioneditform: boolean;
  isTitleInEditing: boolean;
  isTitleInEditingSpine: boolean;
  itemType: ItemTypeModel;
  deletedItemTypeIndex: number;
  currentItemTypeId: number;
  public watermark: string = 'Select a time';
  // sets the format property to display the time value in 24 hours format.
  public formatString: string = 'HH:mm';
  public interval: number = 15;
  tempItemTemplates: ItemTypeModel[] = [];
  startDate = new Date();
  lstTime: any[];
  selectedItem: any;
  selectedItemDuplicate: any;
  // selectedDate: any;
  selectedDateDuplicate: any;
  ckeConfig: any;
  maxChars: number = 100;
  public events: ComponentEvent[] = [];
  public selectDatevalue: Date = new Date();
  selectedDropdownValue: { id; value; default };
  default: number;
  defaultDuplicate: number;

  now: any = new Date();
  nowDuplicate: any = new Date();
  dateNow: any = new Date();
  minDate: Date = new Date(1900, 0, 1);
  dateClear = new Date(2015, 11, 1, 6);
  dateDuplicateClear = new Date(2015, 11, 1, 6);
  dateDuplicateNow: any = new Date();
  timeInterval = 30;
  isCalenderInjuryFocus: boolean = false;
  isCalenderStartFocus: boolean = false;
  isCalenderEndFocus: boolean = false;
  // @ViewChild('calendarOpenBtn', { static: false }) public anchor: ElementRef;
  @ViewChild('calendarOpenBtn1', { static: false }) public anchor1: ElementRef;
  @ViewChild('calendarOpenBtn2', { static: false }) public anchor2: ElementRef;
  // @ViewChild('popup', { static: false, read: ElementRef }) public popup: ElementRef;
  @ViewChild('popup1', { static: false, read: ElementRef }) public popup1: ElementRef;
  @ViewChild('popup2', { static: false, read: ElementRef }) public popup2: ElementRef;

  @HostListener('document:click', ['$event'])


  public documentClick(event: any): void {
    // if (!this.isCalenderInjuryFocus && !this.contains(event.target)) {
    //   this.toggleCalendarPopup(false, 'injuryDate');
    // }
    if (!this.isCalenderStartFocus && !this.contains1(event.target)) {
      this.toggleCalendarPopup(false, 'date');
    }
    if (!this.isCalenderEndFocus && !this.contains2(event.target)) {
      this.toggleCalendarPopup(false, 'duplicate');
    }
  }

  // contains(target: any): boolean {
  //   if (this.anchor != undefined && this.popup != undefined) {
  //     return (
  //       this.anchor.nativeElement.contains(target) ||
  //       (this.popup ? this.popup.nativeElement.contains(target) : false)
  //     );
  //   }
  // }
  contains1(target: any): boolean {
    if (this.anchor1 != undefined && this.popup1 != undefined) {
      return (
        this.anchor1.nativeElement.contains(target) ||
        (this.popup1 ? this.popup1.nativeElement.contains(target) : false)
      );
    }
  }

  contains2(target: any): boolean {
    if (this.anchor2 != undefined && this.popup2 != undefined) {
      return (
        this.anchor2.nativeElement.contains(target) ||
        (this.popup2 ? this.popup2.nativeElement.contains(target) : false)
      );
    }
  }

  constructor(private patientService: PatientService) {
    for (let index = 0; index < 50; index++) {
      this.numbers[index] = index;
    }
  }

  ngOnChanges() {
    console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', this.treatmentNotes.treatmentData);
    this.treatmentNoteForm.controls['selectDate'].setValue(this.currentDate);
    this.treatmentNoteForm.controls['selectTime'].setValue(this.now);
    this.treatmentNoteForm.controls['duplicateselectTime'].setValue(this.currentDuplicateDate);
    this.treatmentNoteForm.controls['duplicateselectDate'].setValue(this.nowDuplicate);
    this.updateRange();
    setTimeout(() => {
      for (let index = 0; index < this.treatmentNotes.treatmentData.length; index++) {
        if (this.treatmentNotes.treatmentData[index].number && this.treatmentNotes.treatmentData[index].number.default) {
          this.min = this.treatmentNotes.treatmentData[index].number.minimum;
          this.max = this.treatmentNotes.treatmentData[index].number.maximum;
          this.default = this.treatmentNotes.treatmentData[index].number.default;
          // this.treatmentNotes.treatmentData[index].helpText = "Enter values between" + this.min + " to " + this.max;
          this.treatmentNoteForm.get('number').setValue(this.default);
          this.treatmentNoteForm.controls['number'].setValidators([Validators.max(this.max), Validators.min(this.min)]);
        }
        if (this.treatmentNotes.treatmentData[index].numberDuplicate && this.treatmentNotes.treatmentData[index].numberDuplicate.default) {
          this.minDuplicate = this.treatmentNotes.treatmentData[index].numberDuplicate.minimum;
          this.maxDuplicate = this.treatmentNotes.treatmentData[index].numberDuplicate.maximum;
          this.defaultDuplicate = this.treatmentNotes.treatmentData[index].numberDuplicate.default;
          // this.treatmentNotes.treatmentData[index].helpTextDuplicate = "Enter values between" + this.minDuplicate + " to " + this.maxDuplicate;
          this.treatmentNoteForm.get('duplicateNumber').setValue(this.defaultDuplicate);
          this.treatmentNoteForm.controls['duplicateNumber'].setValidators([Validators.max(this.maxDuplicate), Validators.min(this.minDuplicate)]);
        }
        if (this.treatmentNotes.treatmentData[index].checkBoxes.length != 0) {
          this.selectedCheckBoxArray = _.filter(this.treatmentNotes.treatmentData[index].checkBoxes, function (eachItem) {
            return eachItem.default == true;
          });
        }
        if (this.treatmentNotes.treatmentData[index].checkBoxesDuplicate.length != 0) {
          this.selectedCheckBoxDuplicateArray = _.filter(this.treatmentNotes.treatmentData[index].checkBoxesDuplicate, function (eachItem) {
            return eachItem.default == true;
          });
        }
        if (this.treatmentNotes.treatmentData[index].vitals && this.treatmentNotes.treatmentData[index].vitals.measurements.length != 0) {
          this.unitOfMeasurement = [];
          this.treatmentNotes.treatmentData[index].vitals.measurements.forEach(val => this.unitOfMeasurement.push(Object.assign({}, val)));
          this.vitalModel.selectedVitalValue = this.treatmentNotes.treatmentData[index].vitals.vitalType;
        }
        if (this.treatmentNotes.treatmentData[index].dateTime != null && this.treatmentNotes.treatmentData[index].dateTime != undefined) {
          if (this.treatmentNotes.treatmentData[index].dateTime.date != null && this.treatmentNotes.treatmentData[index].dateTime.date != undefined) {
            this.treatmentNoteForm.controls['selectDate'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTime.date));
            this.currentDate = new Date(this.treatmentNotes.treatmentData[index].dateTime.date);
          } else {
            const m = new DateTimeModel();
            m.date = this.currentDate;
            this.treatmentNotes.treatmentData[index].dateTime.date = (m.date).toUTCString();
          }
          if (this.treatmentNotes.treatmentData[index].dateTime.time != null && this.treatmentNotes.treatmentData[index].dateTime.time != undefined) {
            this.treatmentNoteForm.controls['selectTime'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTime.time));
            this.now = new Date(this.treatmentNotes.treatmentData[index].dateTime.time);
          } else {
            const m = new DateTimeModel();
            m.time = this.now;
            this.treatmentNotes.treatmentData[index].dateTime.time = (m.time).toUTCString();
          }
        }
        if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate != undefined) {
          if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date != undefined) {
            this.treatmentNoteForm.controls['duplicateselectDate'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date));
            this.currentDuplicateDate = new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date);
          } else {
            const m = new DateTimeModel();
            m.date = this.currentDuplicateDate;
            this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date = (m.date).toUTCString();
          }
          if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time != undefined) {
            this.treatmentNoteForm.controls['duplicateselectTime'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time));
            this.nowDuplicate = new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time);
          } else {
            const m = new DateTimeModel();
            m.time = this.nowDuplicate;
            this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time = (m.time).toUTCString();
          }
        }

      }
    }, 2000);

  }

  onReady(): void {
    console.log('%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', this.treatmentNotes.treatmentData);

    for (let index = 0; index < this.treatmentNotes.treatmentData.length; index++) {
      if (this.treatmentNotes.treatmentData[index].dateTime != null && this.treatmentNotes.treatmentData[index].dateTime != undefined) {
        if (this.treatmentNotes.treatmentData[index].dateTime.date != null && this.treatmentNotes.treatmentData[index].dateTime.date != undefined) {
          this.treatmentNoteForm.controls['selectDate'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTime.date));
          this.currentDate = new Date(this.treatmentNotes.treatmentData[index].dateTime.date);
        } else {
          const m = new DateTimeModel();
          m.date = this.currentDate;
          this.treatmentNotes.treatmentData[index].dateTime.date = (m.date).toUTCString();
        }
        if (this.treatmentNotes.treatmentData[index].dateTime.time != null && this.treatmentNotes.treatmentData[index].dateTime.time != undefined) {
          this.treatmentNoteForm.controls['selectTime'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTime.time));
          this.now = new Date(this.treatmentNotes.treatmentData[index].dateTime.time);
        } else {
          const m = new DateTimeModel();
          m.time = this.now;
          this.treatmentNotes.treatmentData[index].dateTime.time = (m.time).toUTCString();
        }
      }
      if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate != undefined) {
        if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date != undefined) {
          this.treatmentNoteForm.controls['duplicateselectDate'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date));
          this.currentDuplicateDate = new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date);
        } else {
          const m = new DateTimeModel();
          m.date = this.currentDuplicateDate;
          this.treatmentNotes.treatmentData[index].dateTimeDuplicate.date = (m.date).toUTCString();
        }
        if (this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time != null && this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time != undefined) {
          this.treatmentNoteForm.controls['duplicateselectTime'].setValue(new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time));
          this.nowDuplicate = new Date(this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time);
        } else {
          const m = new DateTimeModel();
          m.time = this.nowDuplicate;
          this.treatmentNotes.treatmentData[index].dateTimeDuplicate.time = (m.time).toUTCString();
        }
      }
    }
    if (this.treatmentNotes && this.treatmentNotes.treatmentData) {
      for (let index = 0; index < this.treatmentNotes.treatmentData.length; index++) {
        if (this.treatmentNotes.treatmentData[index].checkBoxes.length != 0) {
          this.selectedCheckBoxArray = _.filter(this.treatmentNotes.treatmentData[index].checkBoxes, function (eachItem) {
            return eachItem.default == true;
          });
        }
        if (this.treatmentNotes.treatmentData[index].checkBoxesDuplicate.length != 0) {
          this.selectedCheckBoxDuplicateArray = _.filter(this.treatmentNotes.treatmentData[index].checkBoxesDuplicate, function (eachItem) {
            return eachItem.default == true;
          });
        }
        if (this.treatmentNotes.treatmentData[index].optionListSubjects && this.treatmentNotes.treatmentData[index].optionListSubjects.length > 0) {
          for (let i = 0; i < this.treatmentNotes.treatmentData[index].optionListSubjects.length; i++) {
            this.treatmentNotes.treatmentData[index].optionListSubjects[i].selectedModel = [];
            for (let j = 0; j < this.treatmentNotes.treatmentData[index].optionListSubjects[i].optionListChoices.length; j++) {
              this.treatmentNotes.treatmentData[index].optionListSubjects[i].optionListChoices[j].selectedIndex = i;
              if (this.treatmentNotes.treatmentData[index].optionListSubjects[i].optionListChoices[j].selected == true) {
                this.treatmentNotes.treatmentData[index].optionListSubjects[i].selectedModel.push(this.treatmentNotes.treatmentData[index].optionListSubjects[i].optionListChoices[j].id);
              }
            }
          }
        }
        if (this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate && this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate.length > 0) {
          for (let i = 0; i < this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate.length; i++) {
            this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].selectedModelDuplicate = [];
            for (let j = 0; j < this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].optionListChoicesDuplicate.length; j++) {
              this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].optionListChoicesDuplicate[j].selectedIndex = i;
              if (this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].optionListChoicesDuplicate[j].selected == true) {
                this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].selectedModelDuplicate.push(this.treatmentNotes.treatmentData[index].optionListSubjectsDuplicate[i].optionListChoicesDuplicate[j].id);
              }
            }
          }
        }
      }
    }
  }

  selectedRadioforOptionList(optionValues) {
    let Selecteddata = _.filter(optionValues, function (eachData) {
      if (eachData.selected == true) {
        return eachData;
      }
    });
    if (Selecteddata.length > 0) {
      for (let index = 0; index < Selecteddata.length - 1; index++) {
        Selecteddata[index].selected = false;
      }
      return Selecteddata[Selecteddata.length - 1].id;
    } else {
      return 0;
    }
  }


  stripHtml(html) {
    var tmp = document.createElement('DIV');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
  }

  truncateText(selector, maxLength) {
    // var element = document.querySelector(selector),
    //   truncated = element.innerText;

    // if (truncated.length > maxLength) {
    //   truncated = truncated.substr(0, maxLength) + '...';
    // }
    const temporalDivElement = document.createElement('div');
    if (selector && selector.includes('/&nbsp;+/g')) {
      let data = selector.replace(/&nbsp;+/g, ' ');
      //text.replace(/&nbsp;/gi, '').trim();
      data = data.split('<p></p>').join('');
      data = data.split('<p>').join(' ');
      temporalDivElement.innerHTML = data;
    }
    // tslint:disable-next-line:max-line-length
    return this.stripNoteText(temporalDivElement.textContent, maxLength) || this.stripNoteText(temporalDivElement.innerText, maxLength) || '';
    // return truncated;
  }

  stripNoteText(str, max) {
    var translate_re = /&(nbsp|amp|quot|lt|gt);/g;
    var translate = {
      'nbsp': '',
      'amp': '&',
      'quot': '"',
      'lt': '<',
      'gt': '>'
    };
    str = str.replace(translate_re, function (match, entity) {
      return translate[entity];
    });
    max = max || 10;
    var len = str.length;
    if (len > max) {
      var sep = '...';
      var seplen = sep.length;
      if (seplen > max) {
        return str.substr(len - max);
      }
      var front = str.substr(0, max);
      return front + sep;
    }
    return str;
  }

  async onChange(text) {
    this.truncateText(text, 25);
    var con = await this.stripHtml(text);
    if (con.length > 25) {
      // e.stopPropagation
    }
  }

  private logEvent(eventName: string): void {
    if (this.events.length > 19) {
      this.events.pop();
    }
    const eventData = {
      name: eventName,
    };
    this.events.unshift(eventData);
  }

  // public getConfig(height: number, maxCharCount: number){
  //   return {
  //     customConfig: '/assets/js/ckeditor/ckeditor-config.js',
  //     height: height,
  //     wordcount: {
  //       showParagraphs: false,
  //       showWordCount: false,
  //       showCharCount: true,
  //       maxCharCount: maxCharCount
  //     }
  //   };
  // }
  ngOnInit() {
    this.value = 1;
    this.vitalsLabel = 'Metric';
    // this.itemType.vitals.measurements = this.unitOfMeasurement;
    this.titleData = [
      {
        'id': 1,
        'value': '1',
        'default': true
      },
      {
        'id': 2,
        'value': '2',
        'default': false
      },
      {
        'id': 3,
        'value': '3',
        'default': false
      },
      {
        'id': 4,
        'value': '4',
        'default': false
      },
      {
        'id': 5,
        'value': '5',
        'default': false
      },
    ];
    this.ckeConfig = {
      removeButtons: 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Strike,Subscript,Superscript,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Blockquote,BidiLtr,BidiRtl,Language,Unlink,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Link',
      removePlugins: 'elementspath',
      height: 140,
      maxLength: 25,
      // extraPlugins: 'wordcount',
      // extraPlugins: 'placeholder',
      // editorplaceholder: 'Type here',
      wordcount: {
        showCharCount: true,
        maxCharCount: 25,
        charLimit: 25,
      }
    };
    this.lstTime = [
      { id: '12:00 PM', value: '12:00 PM' },
      { id: '12:15 PM', value: '12:15 PM' },
      { id: '12:30 PM', value: '12:30 PM' },
      { id: '12:45 PM', value: '12:45 PM' },

      { id: '1:00 PM', value: '1:00 PM' },
      { id: '1:15 PM', value: '1:15 PM' },
      { id: '1:30 PM', value: '1:30 PM' },
      { id: '1:45 PM', value: '1:45 PM' },

      { id: '2:00 PM', value: '2:00 PM' },
      { id: '2:15 PM', value: '2:15 PM' },
      { id: '2:30 PM', value: '2:30 PM' },
      { id: '2:45 PM', value: '2:45 PM' },

      { id: '3:00 PM', value: '3:00 PM' },
      { id: '3:15 PM', value: '3:15 PM' },
      { id: '3:30 PM', value: '3:30 PM' },
      { id: '3:45 PM', value: '3:45 PM' },

      { id: '4:00 PM', value: '4:00 PM' },
      { id: '4:15 PM', value: '4:15 PM' },
      { id: '4:30 PM', value: '4:30 PM' },
      { id: '4:45 PM', value: '4:45 PM' },

      { id: '5:00 PM', value: '5:00 PM' },
      { id: '5:15 PM', value: '5:15 PM' },
      { id: '5:30 PM', value: '5:30 PM' },
      { id: '5:45 PM', value: '5:45 PM' },

      { id: '6:00 PM', value: '6:00 PM' },
      { id: '6:15 PM', value: '6:15 PM' },
      { id: '6:30 PM', value: '6:30 PM' },
      { id: '6:45 PM', value: '6:45 PM' },

      { id: '7:00 PM', value: '7:00 PM' },
      { id: '7:15 PM', value: '7:15 PM' },
      { id: '7:30 PM', value: '7:30 PM' },
      { id: '7:45 PM', value: '7:45 PM' },

      { id: '8:00 PM', value: '8:00 PM' },
      { id: '8:15 PM', value: '8:15 PM' },
      { id: '8:30 PM', value: '8:30 PM' },
      { id: '8:45 PM', value: '8:45 PM' },

      { id: '9:00 PM', value: '9:00 PM' },
      { id: '9:15 PM', value: '9:15 PM' },
      { id: '9:30 PM', value: '9:30 PM' },
      { id: '9:45 PM', value: '9:45 PM' },

      { id: '10:00 PM', value: '10:00 PM' },
      { id: '10:15 PM', value: '10:15 PM' },
      { id: '10:30 PM', value: '10:30 PM' },
      { id: '10:45 PM', value: '10:45 PM' },

      { id: '11:00 PM', value: '11:00 PM' },
      { id: '11:15 PM', value: '11:15 PM' },
      { id: '11:30 PM', value: '11:30 PM' },
      { id: '11:45 PM', value: '11:45 PM' },

      { id: '12:00 AM', value: '12:00 AM' },
      { id: '12:15 AM', value: '12:15 AM' },
      { id: '12:30 AM', value: '12:30 AM' },
      { id: '12:45 AM', value: '12:45 AM' },

      { id: '1:00 AM', value: '1:00 AM' },
      { id: '1:15 AM', value: '1:15 AM' },
      { id: '1:30 AM', value: '1:30 AM' },
      { id: '1:45 AM', value: '1:45 AM' },

      { id: '2:00 AM', value: '2:00 AM' },
      { id: '2:15 AM', value: '2:15 AM' },
      { id: '2:30 AM', value: '2:30 AM' },
      { id: '2:45 AM', value: '2:45 AM' },

      { id: '3:00 AM', value: '3:00 AM' },
      { id: '3:15 AM', value: '3:15 AM' },
      { id: '3:30 AM', value: '3:30 AM' },
      { id: '3:45 AM', value: '3:45 AM' },

      { id: '4:00 AM', value: '4:00 AM' },
      { id: '4:15 AM', value: '4:15 AM' },
      { id: '4:30 AM', value: '4:30 AM' },
      { id: '4:45 AM', value: '4:45 AM' },

      { id: '5:00 AM', value: '5:00 AM' },
      { id: '5:15 AM', value: '5:15 AM' },
      { id: '5:30 AM', value: '5:30 AM' },
      { id: '5:45 AM', value: '5:45 AM' },

      { id: '6:00 AM', value: '6:00 AM' },
      { id: '6:15 AM', value: '6:15 AM' },
      { id: '6:30 AM', value: '6:30 AM' },
      { id: '6:45 AM', value: '6:45 AM' },

      { id: '7:00 AM', value: '7:00 AM' },
      { id: '7:15 AM', value: '7:15 AM' },
      { id: '7:30 AM', value: '7:30 AM' },
      { id: '7:45 AM', value: '7:45 AM' },

      { id: '8:00 AM', value: '8:00 AM' },
      { id: '8:15 AM', value: '8:15 AM' },
      { id: '8:30 AM', value: '8:30 AM' },
      { id: '8:45 AM', value: '8:45 AM' },

      { id: '9:00 AM', value: '9:00 AM' },
      { id: '9:15 AM', value: '9:15 AM' },
      { id: '9:30 AM', value: '9:30 AM' },
      { id: '9:45 AM', value: '9:45 AM' },

      { id: '10:00 AM', value: '10:00 AM' },
      { id: '10:15 AM', value: '10:15 AM' },
      { id: '10:30 AM', value: '10:30 AM' },
      { id: '10:45 AM', value: '10:45 AM' },

      { id: '11:00 AM', value: '11:00 AM' },
      { id: '11:15 AM', value: '11:15 AM' },
      { id: '11:30 AM', value: '11:30 AM' },
      { id: '11:45 AM', value: '11:45 AM' }];
  }

  updateRange(): void {
    this.templatedataLength = this.treatmentNotes.treatmentData.length;
    if (
      this.treatmentNotes !== undefined &&
      this.treatmentNotes.treatmentData !== undefined &&
      this.treatmentNotes.treatmentData.length > 0
    ) {
      this.treatmentNotes.treatmentData.map((x) => {
        if (x.itemTypeText == 'Range or Scale') {
          if (
            x.rangeOrScale !== null &&
            x.rangeOrScale.minimum !== undefined &&
            x.rangeOrScale.maximum !== undefined &&
            x.rangeOrScale.default !== undefined
          ) {
            this.min = Number(x.rangeOrScale.minimum);
            this.max = Number(x.rangeOrScale.maximum);
            this.value = Number(x.rangeOrScale.default);
            this.range(this.value);
          }

          if (
            x.rangeOrScaleDuplicate !== null &&
            x.rangeOrScaleDuplicate.minimum !== undefined &&
            x.rangeOrScaleDuplicate.maximum !== undefined &&
            x.rangeOrScaleDuplicate.default !== undefined
          ) {
            this.minDuplicate = Number(x.rangeOrScaleDuplicate.minimum);
            this.maxDuplicate = Number(x.rangeOrScaleDuplicate.maximum);
            this.valueDuplicate = Number(x.rangeOrScaleDuplicate.default);
            this.rangeDuplicate(this.valueDuplicate);
          }
        }
        if (x.itemTypeText == 'Dropdown') {
          if (
            x.dropdowns !== null
          ) {
            this.titleData = x.dropdowns;
            this.titleDataduplicate = x.duplicateDropdowns;
          }

        }
      });
    }
  }

  moveUp(index: number, typedata) {
    if (index >= 1) {
      this.swap(index, index - 1);
    }
  }

  moveDown(index: number) {
    if (index < this.treatmentNotes.treatmentData.length - 1) {
      this.swap(index, index + 1);
    }
  }

  private swap(x: any, y: any) {
    var b = this.treatmentNotes.treatmentData[x];
    this.treatmentNotes.treatmentData[x] = this.treatmentNotes.treatmentData[y];
    this.treatmentNotes.treatmentData[y] = b;
  }

  public range = (value: number) => {
    return this.numbers[value];
  };

  public rangeDuplicate = (value: number) => {
    return this.numbers[value];
  };

  public close() {
    this.openeditform = false;
    this.opendeleteform = false;
    this.opendeletenote = false;
    this.openexportnote = false;
    this.openexportnotepreview = false;
    this.openemailnoteemail = false;
    this.openfiltercase = false;
    this.openfilterpractitioner = false;
    this.openfilterdaterange = false;
    this.opencasestatusform = false;
    this.showchangedatetime = false;
    this.showchangepatient = false;
    this.showchangepatientcase = false;
    this.showchangepatientnote = false;
    this.showchangepatientdelete = false;
    this.openChiedform = false;
    this.openvitaleditform = false;
    this.showopticalarea = false;
    this.opendatetimeeditform = false;
    this.opennumbereditform = false;
    this.opendroupdwoneditform = false;
    this.opendischargeform = false;
    this.openradioeditform = false;
    this.opencheckboxeditform = false;
    this.openrangeeditform = false;
    this.openoptionlisteditform = false;
    this.openatatchfileditform = false;
    this.openchiefeditform = false;
    this.opentabledataeditform = false;
    this.openinstructioneditform = false;
    this.isTitleInEditing = false;
    this.isTitleInEditingSpine = false;
    this.showflag = false;
    if (this.openheadingeditform1 == true) {
      this.openheadingeditform = true;
    } else {
      this.openheadingeditform = false;
    }
  }

  deletedata(itemTypeId: number, type: string, from: any) {
    if (type == 'heading' && from == 'show') {
      this.openheadingeditform1 = true;
    } else {
      this.openheadingeditform1 = false;
    }
    this.opendeleteform = true;
    this.openeditform = false;
    this.opennumbereditform = false;
    this.openheadingeditform = false;
    this.opendatetimeeditform = false;

    this.opendroupdwoneditform = false;
    this.openradioeditform = false;
    this.opencheckboxeditform = false;
    this.openrangeeditform = false;

    this.openoptionlisteditform = false;
    this.opentabledataeditform = false;
    this.openatatchfileditform = false;
    this.isTitleInEditing = false;
    this.isTitleInEditingSpine = false;
    this.openinstructioneditform = false;
    this.openchiefeditform = false;
    this.currentItemTypeId = itemTypeId;
  }

  deleteitem(itemTypeId: number) {
    console.log(AppUtils.refrenceClone(this.treatmentNotes.treatmentData));

    // if (this.treatmentNotes.treatmentData.length >= 2) {
    const id = itemTypeId === 0 ? this.currentItemTypeId : itemTypeId;
    //this.itemType = new ItemTypeModel();
    this.deletedItemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === id
    );
    this.treatmentNotes.treatmentData.splice(this.deletedItemTypeIndex, 1);
    this.openheadingeditform = false;
    if (this.currentItemTypeId !== 0) {
      this.opendeleteform = false;
    }
    this.currentItemTypeId = 0;
    // }
    // else {
    //   this.opendeleteform = false;
    //   this.displayMessage('Atleast one item need to save record.');
    // }

    console.log(AppUtils.refrenceClone(this.treatmentNotes.treatmentData));
    
  }

  savedata(itemTypeId: number, type: string) {

    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    if (type == 'heading') {
      if (this.itemType.itemLabel !== '') {
        if (this.itemType.showMirrored === true) {
          if (this.itemType.itemLabelDuplicate === '') {
            this.openheadingeditform = true;
            return;
          } else {
            this.openheadingeditform = false;
          }
        } else {
          this.itemType.itemLabelDuplicate = '';
          this.openheadingeditform = false;
        }
      } else {
        this.openheadingeditform = true;
        return;
      }
    } else if (type == 'textarea') {
      if (this.itemType.itemLabel !== '') {
        if (this.itemType.showMirrored === true) {
          if (this.itemType.itemLabelDuplicate === '') {
            this.openeditform = true;
            return;
          } else {
            this.openeditform = false;
          }
        } else {
          this.itemType.itemLabelDuplicate = '';
          this.openeditform = false;
        }
      } else {
        if (this.itemType.isMandatory == true) {
          this.openeditform = true;
          return;
        } else {
          this.openeditform = false;
        }
      }
    } else if (type == 'datetime') {
      if (this.itemType.itemLabel !== '') {
        if (this.itemType.showMirrored === true) {
          if (this.itemType.itemLabelDuplicate === '') {
            this.itemType.itemLabelDuplicate = 'Date Time 2';
            this.opendatetimeeditform = false;
          } else {
            this.itemType.dateTimeDuplicate = new DateTimeModel();
            let m = new DateTimeModel();
            m.date = this.currentDuplicateDate;
            m.time = this.nowDuplicate;
            this.itemType.dateTimeDuplicate.date = (m.date).toISOString();
            this.itemType.dateTimeDuplicate.time = (m.time).toISOString();
            this.opendatetimeeditform = false;
          }
        } else {
          this.itemType.itemLabelDuplicate = 'Date Time 2';
          this.itemType.dateTimeDuplicate = null;
          this.opendatetimeeditform = false;
        }
      } else {
        if (this.itemType.isMandatory == true) {
          this.opendatetimeeditform = true;
          return;
        } else {
          this.opendatetimeeditform = false;
        }
      }
    } else if (type == 'number') {
      if (this.numberModel.number.maximum != '') {
        this.treatmentNotes.treatmentData[itemTypeIndex].number.maximum = this.numberModel.number.maximum;
        this.itemType.number.maximum = this.numberModel.number.maximum;
        this.max = this.numberModel.number.maximum;
      } else {
        this.max = this.itemType.number.maximum;
        this.treatmentNotes.treatmentData[itemTypeIndex].number.maximum = this.itemType.number.maximum;
      }

      if (this.numberModel.number.minimum != '') {
        this.treatmentNotes.treatmentData[itemTypeIndex].number.minimum = this.numberModel.number.minimum;
        this.itemType.number.minimum = this.numberModel.number.minimum;
        this.max = this.numberModel.number.minimum;
      } else {
        this.min = this.itemType.number.minimum;
        this.treatmentNotes.treatmentData[itemTypeIndex].number.minimum = this.itemType.number.minimum;
      }

      if (this.numberModel.number.default != '') {
        this.treatmentNotes.treatmentData[itemTypeIndex].number.default = this.numberModel.number.default;
        this.itemType.number.default = this.numberModel.number.default;
      } else {
        this.treatmentNotes.treatmentData[itemTypeIndex].number.default = this.itemType.number.default;
      }
      this.treatmentNotes.treatmentData[itemTypeIndex].helpText = 'Enter values between ' + this.treatmentNotes.treatmentData[itemTypeIndex].number.minimum + ' to ' + this.treatmentNotes.treatmentData[itemTypeIndex].number.maximum;
      this.itemType.helpText = 'Enter values between ' + this.itemType.number.minimum + ' to ' + this.itemType.number.maximum;
      this.treatmentNoteForm.controls['number'].setValidators([Validators.min(this.itemType.number.minimum), Validators.max(this.itemType.number.maximum)]);
      if (this.itemType.numberDuplicate && this.numberModel.numberDuplicate) {
        if (this.numberModel.numberDuplicate.minimum != '') {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.minimum = this.numberModel.numberDuplicate.minimum;
          this.itemType.numberDuplicate.minimum = this.numberModel.numberDuplicate.minimum;
        } else {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.minimum = this.itemType.numberDuplicate.minimum;
        }

        if (this.numberModel.numberDuplicate.maximum != '') {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.maximum = this.numberModel.numberDuplicate.maximum;
          this.itemType.numberDuplicate.maximum = this.numberModel.numberDuplicate.maximum;
        } else {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.maximum = this.itemType.numberDuplicate.maximum;
        }

        if (this.numberModel.numberDuplicate.default != '') {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.default = this.numberModel.numberDuplicate.default;
          this.itemType.numberDuplicate.default = this.numberModel.numberDuplicate.default;
        } else {
          this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.default = this.itemType.numberDuplicate.default;
        }
        this.treatmentNotes.treatmentData[itemTypeIndex].helpTextDuplicate = 'Enter values between ' + this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.minimum + ' to ' + this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.maximum;
        this.itemType.helpTextDuplicate = 'Enter values between ' + this.itemType.numberDuplicate.minimum + ' to ' + this.itemType.numberDuplicate.maximum;
        this.treatmentNoteForm.controls['duplicateNumber'].setValidators([Validators.min(this.itemType.numberDuplicate.minimum), Validators.max(this.itemType.numberDuplicate.maximum)]);
      }

      if (
        this.itemType.itemLabel !== '' &&
        this.itemType.number.default !== undefined &&
        this.itemType.number.maximum !== undefined &&
        this.itemType.number.minimum !== undefined
      ) {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate !== '' &&
            this.itemType.numberDuplicate !== undefined &&
            this.itemType.numberDuplicate.default !== undefined &&
            this.itemType.numberDuplicate.maximum !== undefined &&
            this.itemType.numberDuplicate.minimum !== undefined
          ) {
            if (this.itemType.isMandatory == true) {
              if (this.itemType.numberDuplicate.default == undefined ||
                this.itemType.numberDuplicate.maximum == undefined ||
                this.itemType.numberDuplicate.minimum == undefined) {
                this.opennumbereditform = true;
              } else {
                this.opennumbereditform = false;
              }
            } else {
              this.opennumbereditform = false;
            }
          } else {
            if (this.itemType.isMandatory == true) {
              if (this.itemType.number.default == undefined ||
                this.itemType.number.maximum == undefined ||
                this.itemType.number.minimum == undefined) {
                this.opennumbereditform = true;
              } else {
                this.opennumbereditform = false;
              }
            }
          }
        } else {
          if (this.itemType.isMandatory == true) {
            if (this.itemType.number.default == undefined ||
              this.itemType.number.maximum == undefined ||
              this.itemType.number.minimum == undefined) {
              this.opennumbereditform = true;
            } else {
              this.opennumbereditform = false;
            }

          } else {
            this.opennumbereditform = false;
          }
        }
      } else {
        if (this.itemType.isMandatory == true) {
          this.opennumbereditform = true;
        } else {
          this.opennumbereditform = false;
        }
      }
    } else if (type == 'dropdown') {
      if (this.itemType.itemLabel !== '' && this.itemType.helpText !== '') {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate === '' ||
            this.itemType.helpTextDuplicate === '' ||
            this.itemType.duplicateDropdowns === undefined ||
            this.itemType.helpTextDuplicate === '' ||
            (this.itemType.duplicateDropdowns !== undefined &&
              this.itemType.duplicateDropdowns.length <= 0)
          ) {
            if (this.itemType.isMandatory === true) {
              if (this.itemType.itemLabelDuplicate === '' || this.itemType.helpTextDuplicate === '') {
                this.opendroupdwoneditform = true;
                return;
              } else {
                this.opendroupdwoneditform = false;
              }
            } else {
              this.opendroupdwoneditform = false;
            }

          } else {
            this.itemType.itemLabelDuplicate = 'Dropdown 2';
            this.itemType.helpTextDuplicate = 'Select any of the dropdown values.';
            this.titleData = this.itemType.dropdowns;
            for (let i = 0; i < this.itemType.duplicateDropdowns.length; i++) {
              if (this.itemType.duplicateDropdowns[i].value == '') {
                break;
              } else {
                this.titleData = this.itemType.dropdowns;
                this.titleDataduplicate = this.itemType.duplicateDropdowns;
                this.opendroupdwoneditform = false;
              }
            }
          }
        } else {
          for (let i = 0; i < this.itemType.dropdowns.length; i++) {
            if (this.itemType.dropdowns[i].value == '') {
              this.opendroupdwoneditform = true;
              return;
            } else {
              this.itemType.itemLabelDuplicate = '';
              this.titleData = this.itemType.dropdowns;
              this.itemType.duplicateDropdowns = null;
              this.opendroupdwoneditform = false;
            }
          }
        }
      } else {
        if (this.itemType.isMandatory === true) {
          if (this.itemType.itemLabel === '' || this.itemType.helpText === '') {
            this.opendroupdwoneditform = true;
            return;
          } else {
            this.opendroupdwoneditform = false;
          }
        } else {
          this.opendroupdwoneditform = false;
        }
      }
    } else if (type == 'radio') {
      if (this.itemType.itemLabel !== '' && this.itemType.helpText !== '') {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate === '' ||
            this.itemType.duplicateRadioButtons === undefined ||
            this.itemType.helpTextDuplicate === '' ||
            (this.itemType.duplicateRadioButtons !== undefined &&
              this.itemType.duplicateRadioButtons.length <= 0)
          ) {

            if (this.itemType.isMandatory === true) {
              if (this.itemType.itemLabelDuplicate === '' || this.itemType.helpTextDuplicate === '') {
                this.openradioeditform = true;
                return;
              } else {
                this.openradioeditform = false;
              }
            } else {
              this.openradioeditform = false;
            }
          } else {
            for (let i = 0; i < this.itemType.duplicateRadioButtons.length; i++) {
              if (this.itemType.duplicateRadioButtons[i].value == '') {
                //  this.displayMessage('Please fill all the field.');
                let responseHandle = {
                  message: 'Please fill all the field.',
                  type: 'error'
                };
                this.patientService.publishSomeData(JSON.stringify(responseHandle));
                break;
              } else {
                this.titleData = this.itemType.duplicateRadioButtons;
                this.titleDataduplicate = this.itemType.duplicateRadioButtons;
                this.openradioeditform = false;
              }
            }
            // this.openradioeditform = false;
          }
        } else {
          for (let i = 0; i < this.itemType.radioButtons.length; i++) {
            if (this.itemType.radioButtons[i].value == '') {
              //  this.displayMessage('Please fill all the field.');
              let responseHandle = {
                message: 'Please fill all the field.',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              break;
            } else {
              this.itemType.itemLabelDuplicate = '';
              this.titleData = this.itemType.radioButtons;
              this.itemType.duplicateRadioButtons = null;
              this.openradioeditform = false;
            }
          }
        }
      } else {
        if (this.itemType.isMandatory === true) {
          if (this.itemType.itemLabel === '' || this.itemType.helpText === '') {
            this.openradioeditform = true;
            return;
          } else {
            this.openradioeditform = false;
          }
        } else {
          this.openradioeditform = false;
        }
      }
    } else if (type == 'checkbox') {

      if (this.itemType.itemLabel !== '' && this.itemType.helpText !== '') {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate === '' ||
            this.itemType.checkBoxesDuplicate === undefined ||
            this.itemType.helpTextDuplicate === '' ||
            (this.itemType.checkBoxesDuplicate !== undefined &&
              this.itemType.checkBoxesDuplicate.length <= 0)
          ) {
            if (this.itemType.isMandatory === true) {
              if (this.itemType.itemLabelDuplicate === '' || this.itemType.helpTextDuplicate === '') {
                this.opencheckboxeditform = true;
                return;
              } else {
                this.opencheckboxeditform = false;
              }
            } else {
              this.opencheckboxeditform = false;
            }
          } else {
            if (this.itemType.isMandatory === true) {
              if (this.itemType.itemLabelDuplicate === '' || this.itemType.helpTextDuplicate === '') {
                this.opencheckboxeditform = true;
                return;
              } else {
                this.opencheckboxeditform = false;
              }
            } else {
              this.opencheckboxeditform = false;
            }
          }
        } else {
          if (this.itemType.isMandatory === true) {
            if (this.itemType.itemLabel === '' || this.itemType.helpText === '') {
              this.opencheckboxeditform = true;
              return;
            } else {
              this.opencheckboxeditform = false;
            }
          } else {
            this.opencheckboxeditform = false;
          }
        }
      } else {
        if (this.itemType.isMandatory === true) {
          if (this.itemType.itemLabel === '' || this.itemType.helpText === '') {
            this.opencheckboxeditform = true;
            return;
          } else {
            this.opencheckboxeditform = false;
          }
        } else {
          this.opencheckboxeditform = false;
        }
      }
    } else if (type == 'optionlist') {
      if (this.itemType.itemLabel !== '') {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate === '' ||
            this.itemType.optionListChoicesDuplicate === undefined ||
            (this.itemType.optionListChoicesDuplicate !== undefined &&
              this.itemType.optionListChoicesDuplicate.length <= 0)
          ) {
            this.openoptionlisteditform = true;
            return;
          } else {

            for (let i = 0; i < this.itemType.optionListSubjects.length; i++) {
              if (this.itemType.optionListSubjects[i].value == '') {
                //  this.displayMessage('Please fill all the field.');
                let responseHandle = {
                  message: 'Please fill all the field.',
                  type: 'error'
                };
                this.patientService.publishSomeData(JSON.stringify(responseHandle));
                this.openoptionlisteditform = true;
                break;
              } else {
                for (let j = 0; j < this.itemType.optionListChoices.length; j++) {
                  if (this.itemType.optionListChoices[j].value == '') {
                    //  this.displayMessage('Please fill all the field.');
                    let responseHandle = {
                      message: 'Please fill all the field.',
                      type: 'error'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    this.openoptionlisteditform = true;
                    break;
                  } else {
                    for (let k = 0; k < this.itemType.optionListSubjectsDuplicate.length; k++) {
                      if (this.itemType.optionListSubjectsDuplicate[k].value == '') {
                        //  this.displayMessage('Please fill all the field.');
                        let responseHandle = {
                          message: 'Please fill all the field.',
                          type: 'error'
                        };
                        this.patientService.publishSomeData(JSON.stringify(responseHandle));
                        this.openoptionlisteditform = true;
                        break;
                      } else {
                        for (let l = 0; l < this.itemType.optionListChoicesDuplicate.length; l++) {
                          if (this.itemType.optionListChoicesDuplicate[l].value == '') {
                            //  this.displayMessage('Please fill all the field.');
                            let responseHandle = {
                              message: 'Please fill all the field.',
                              type: 'error'
                            };
                            this.patientService.publishSomeData(JSON.stringify(responseHandle));
                            this.openoptionlisteditform = true;
                            break;
                          } else {
                            this.itemType.itemLabelDuplicate = '';
                            this.openoptionlisteditform = false;
                            // for (let index = 0; index < this.itemType.optionListSubjectsDuplicate.length; index++) {
                            //   this.itemType.optionListSubjectsDuplicate[index].selectedModelDuplicate = []
                            //   this.itemType.optionListSubjectsDuplicate[index].optionListChoicesDuplicate = this.itemType.optionListChoicesDuplicate
                            //   for (let i = 0; i < this.itemType.optionListSubjectsDuplicate[index].optionListChoicesDuplicate.length; i++) {
                            //     if (this.itemType.optionListSubjectsDuplicate[index].optionListChoicesDuplicate[i].selected == true) {
                            //       this.itemType.optionListSubjectsDuplicate[index].selectedModelDuplicate.push(this.itemType.optionListSubjectsDuplicate[index].optionListChoicesDuplicate[i].id)
                            //     }
                            //   }
                            // }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

          }
        } else {
          // for (let index = 0; index < this.itemType.optionListSubjects.length; index++) {
          //   this.itemType.optionListSubjects[index].selectedModel = []
          //   for (let i = 0; i < this.itemType.optionListSubjects[index].optionListChoices.length; i++) {
          //     if (this.itemType.optionListSubjects[index].optionListChoices[i].selected == true) {
          //       this.itemType.optionListSubjects[index].selectedModel.push(this.itemType.optionListSubjects[index].optionListChoices[i].id)
          //     }
          //   }
          // }

          for (let i = 0; i < this.itemType.optionListSubjects.length; i++) {
            if (this.itemType.optionListSubjects[i].value == '') {
              //  this.displayMessage('Please fill all the field.');
              let responseHandle = {
                message: 'Please fill all the field.',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              this.openoptionlisteditform = true;
              break;
            } else {
              for (let j = 0; j < this.itemType.optionListChoices.length; j++) {
                if (this.itemType.optionListChoices[j].value == '') {
                  //  this.displayMessage('Please fill all the field.');
                  let responseHandle = {
                    message: 'Please fill all the field.',
                    type: 'error'
                  };
                  this.patientService.publishSomeData(JSON.stringify(responseHandle));
                  this.openoptionlisteditform = true;
                  break;
                } else {
                  // this.itemType.itemLabelDuplicate = "";
                  // this.itemType.optionListChoicesDuplicate = [];
                  // this.openoptionlisteditform = false;
                }
              }
            }
          }
        }
      } else {
        this.openoptionlisteditform = true;
        return;
      }
    } else if (type == 'tabledata') {
      if (this.itemType.itemLabel !== '') {
        this.opentabledataeditform = false;
      } else {
        this.openoptionlisteditform = true;
        return;
      }
    } else if (type == 'range') {
      if (this.itemType.itemLabel !== '' && this.itemType.helpText !== '') {
        if (this.itemType.showMirrored === true) {
          if (
            this.itemType.itemLabelDuplicate === '' ||
            this.itemType.rangeOrScale === undefined ||
            (this.itemType.rangeOrScaleDuplicate !== undefined &&
              this.itemType.rangeOrScale.length <= 0)
          ) {
            this.openrangeeditform = true;
            this.min = Number(this.itemType.rangeOrScale.minimum);
            this.max = Number(this.itemType.rangeOrScale.maximum);
            this.value = Number(this.itemType.rangeOrScale.default);
            // if (this.rangeScaleModel.rangeOrScale.maximum != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
            //   this.itemType.rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
            //   this.max = this.rangeScaleModel.rangeOrScale.maximum;
            // } else {
            //   this.max = this.itemType.rangeOrScale.maximum;
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.itemType.rangeOrScale.maximum
            // }

            // if (this.rangeScaleModel.rangeOrScale.minimum != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
            //   this.itemType.rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
            //   this.max = this.rangeScaleModel.rangeOrScale.minimum;
            // } else {
            //   this.min = this.itemType.rangeOrScale.minimum;
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.itemType.rangeOrScale.minimum
            // }

            // if (this.rangeScaleModel.rangeOrScale.default != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
            //   this.itemType.rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
            // } else {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.itemType.rangeOrScale.default
            // }
            this.range(this.value);
            this.minDuplicate = Number(this.itemType.rangeOrScaleDuplicate.minimum);
            this.maxDuplicate = Number(this.itemType.rangeOrScaleDuplicate.maximum);
            this.valueDuplicate = Number(this.itemType.rangeOrScaleDuplicate.default);
            // if (this.rangeScaleModel.rangeOrScale.maximum != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.maximum = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
            //   this.itemType.rangeOrScaleDuplicate.maximum = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
            //   this.max = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
            // } else {
            //   this.max = this.itemType.rangeOrScaleDuplicate.maximum;
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.maximum = this.itemType.rangeOrScaleDuplicate.maximum
            // }

            // if (this.rangeScaleModel.rangeOrScaleDuplicate.minimum != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.minimum = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
            //   this.itemType.rangeOrScaleDuplicate.minimum = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
            //   this.max = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
            // } else {
            //   this.min = this.itemType.rangeOrScaleDuplicate.minimum;
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.minimum = this.itemType.rangeOrScaleDuplicate.minimum
            // }

            // if (this.rangeScaleModel.rangeOrScaleDuplicate.default != '') {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.default = this.rangeScaleModel.rangeOrScaleDuplicate.default;
            //   this.itemType.rangeOrScaleDuplicate.default = this.rangeScaleModel.rangeOrScaleDuplicate.default;
            // } else {
            //   this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.default = this.itemType.rangeOrScaleDuplicate.default
            // }
            this.rangeDuplicate(this.valueDuplicate);
            // return;
          } else {

            if (this.itemType.rangeOrScale.minimum == undefined) {
              //  this.displayMessage('Please fill all the field.');
              let responseHandle = {
                message: 'Please fill all the field.',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              this.openrangeeditform = true;
            } else {
              if (this.itemType.rangeOrScale.maximum == undefined) {
                //  this.displayMessage('Please fill all the field.');
                let responseHandle = {
                  message: 'Please fill all the field.',
                  type: 'error'
                };
                this.patientService.publishSomeData(JSON.stringify(responseHandle));
                this.openrangeeditform = true;
              } else {
                if (this.itemType.rangeOrScale.default == undefined) {
                  //  this.displayMessage('Please fill all the field.');
                  let responseHandle = {
                    message: 'Please fill all the field.',
                    type: 'error'
                  };
                  this.patientService.publishSomeData(JSON.stringify(responseHandle));
                  this.openrangeeditform = true;
                } else {
                  if (this.itemType.rangeOrScaleDuplicate.minimum == undefined) {
                    //  this.displayMessage('Please fill all the field.');
                    let responseHandle = {
                      message: 'Please fill all the field.',
                      type: 'error'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    this.openrangeeditform = true;
                  } else {
                    if (this.itemType.rangeOrScaleDuplicate.maximum == undefined) {
                      //  this.displayMessage('Please fill all the field.');
                      let responseHandle = {
                        message: 'Please fill all the field.',
                        type: 'error'
                      };
                      this.patientService.publishSomeData(JSON.stringify(responseHandle));
                      this.openrangeeditform = true;
                    } else {

                      if (this.itemType.rangeOrScaleDuplicate.default == undefined) {
                        //  this.displayMessage('Please fill all the field.');
                        let responseHandle = {
                          message: 'Please fill all the field.',
                          type: 'error'
                        };
                        this.patientService.publishSomeData(JSON.stringify(responseHandle));
                        this.openrangeeditform = true;
                      } else {
                        // this.min = Number(this.itemType.rangeOrScale.minimum);
                        // this.max = Number(this.itemType.rangeOrScale.maximum);
                        // this.value = Number(this.itemType.rangeOrScale.default);
                        if (this.rangeScaleModel.rangeOrScale.maximum != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
                          this.itemType.rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
                          this.max = this.rangeScaleModel.rangeOrScale.maximum;
                        } else {
                          this.max = this.itemType.rangeOrScale.maximum;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.itemType.rangeOrScale.maximum;
                        }

                        if (this.rangeScaleModel.rangeOrScale.minimum != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
                          this.itemType.rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
                          this.max = this.rangeScaleModel.rangeOrScale.minimum;
                        } else {
                          this.min = this.itemType.rangeOrScale.minimum;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.itemType.rangeOrScale.minimum;
                        }

                        if (this.rangeScaleModel.rangeOrScale.default != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
                          this.itemType.rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
                          this.value = this.rangeScaleModel.rangeOrScale.default;
                        } else {
                          this.value = this.itemType.rangeOrScale.default;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.itemType.rangeOrScale.default;
                        }
                        this.range(this.itemType.rangeOrScale.default);
                        // this.minDuplicate = Number(this.itemType.rangeOrScaleDuplicate.minimum);
                        // this.maxDuplicate = Number(this.itemType.rangeOrScaleDuplicate.maximum);
                        // this.valueDuplicate = Number(this.itemType.rangeOrScaleDuplicate.default);
                        if (this.rangeScaleModel.rangeOrScale.maximum != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.maximum = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
                          this.itemType.rangeOrScaleDuplicate.maximum = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
                          this.max = this.rangeScaleModel.rangeOrScaleDuplicate.maximum;
                        } else {
                          this.max = this.itemType.rangeOrScaleDuplicate.maximum;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.maximum = this.itemType.rangeOrScaleDuplicate.maximum;
                        }

                        if (this.rangeScaleModel.rangeOrScaleDuplicate.minimum != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.minimum = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
                          this.itemType.rangeOrScaleDuplicate.minimum = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
                          this.max = this.rangeScaleModel.rangeOrScaleDuplicate.minimum;
                        } else {
                          this.min = this.itemType.rangeOrScaleDuplicate.minimum;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.minimum = this.itemType.rangeOrScaleDuplicate.minimum;
                        }

                        if (this.rangeScaleModel.rangeOrScaleDuplicate.default != '') {
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.default = this.rangeScaleModel.rangeOrScaleDuplicate.default;
                          this.itemType.rangeOrScaleDuplicate.default = this.rangeScaleModel.rangeOrScaleDuplicate.default;
                          this.valueDuplicate = this.rangeScaleModel.rangeOrScale.default;
                        } else {
                          this.valueDuplicate = this.itemType.rangeOrScaleDuplicate.default;
                          this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScaleDuplicate.default = this.itemType.rangeOrScaleDuplicate.default;
                        }
                        this.rangeDuplicate(this.rangeScaleModel.rangeOrScaleDuplicate.default);
                        this.openrangeeditform = false;
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
        } else {
          if (this.itemType.rangeOrScale.minimum == undefined) {
            //  this.displayMessage('Please fill all the field.');
            let responseHandle = {
              message: 'Please fill all the field.',
              type: 'error'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            this.openrangeeditform = true;
          } else {
            if (this.itemType.rangeOrScale.maximum == undefined) {
              //  this.displayMessage('Please fill all the field.');
              let responseHandle = {
                message: 'Please fill all the field.',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              this.openrangeeditform = true;
            } else {
              if (this.itemType.rangeOrScale.default == undefined) {
                //  this.displayMessage('Please fill all the field.');
                let responseHandle = {
                  message: 'Please fill all the field.',
                  type: 'error'
                };
                this.patientService.publishSomeData(JSON.stringify(responseHandle));
                this.openrangeeditform = true;
              } else {
                this.itemType.itemLabelDuplicate = '';
                // this.min = Number(this.itemType.rangeOrScale.minimum);
                // this.max = Number(this.itemType.rangeOrScale.maximum);
                // this.value = Number(this.itemType.rangeOrScale.default);
                // this.range(this.value);
                if (this.rangeScaleModel.rangeOrScale.maximum != '') {
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
                  this.itemType.rangeOrScale.maximum = this.rangeScaleModel.rangeOrScale.maximum;
                  this.max = this.rangeScaleModel.rangeOrScale.maximum;
                } else {
                  this.max = this.itemType.rangeOrScale.maximum;
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.maximum = this.itemType.rangeOrScale.maximum;
                }

                if (this.rangeScaleModel.rangeOrScale.minimum != '') {
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
                  this.itemType.rangeOrScale.minimum = this.rangeScaleModel.rangeOrScale.minimum;
                  this.max = this.rangeScaleModel.rangeOrScale.minimum;
                } else {
                  this.min = this.itemType.rangeOrScale.minimum;
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.minimum = this.itemType.rangeOrScale.minimum;
                }

                if (this.rangeScaleModel.rangeOrScale.default != '') {
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
                  this.itemType.rangeOrScale.default = this.rangeScaleModel.rangeOrScale.default;
                  this.value = this.rangeScaleModel.rangeOrScale.default;
                } else {
                  this.value = this.itemType.rangeOrScale.default;
                  this.treatmentNotes.treatmentData[itemTypeIndex].rangeOrScale.default = this.itemType.rangeOrScale.default;
                }
                this.range(this.itemType.rangeOrScale.default);
                this.openrangeeditform = false;
                return;
              }
            }
          }
        }
      } else {
        this.openrangeeditform = true;
        return;
      }
    } else if (type == 'attachfile') {
      this.openatatchfileditform = false;
    } else if (type == 'instruction') {
      this.openinstructioneditform = false;
    } else if (type == 'chief') {
      this.openchiefeditform = false;
    } else if (type == 'vitals') {
      let unitOfMeasurement = this.unitOfMeasurement;
      this.openvitaleditform = false;
      if (this.vitalModel.selectedVitalValue != null && this.vitalModel.selectedVitalValue != undefined) {
        this.treatmentNotes.treatmentData[itemTypeIndex].vitals.vitalType = this.vitalModel.selectedVitalValue;
      }
      if (this.vitalModel.selectedVitalValue === 'Metric' || this.treatmentNotes.treatmentData[itemTypeIndex].vitals.vitalType == 'Metric') {
        for (let index = 0; index < this.metric.length; index++) {
          for (let i = 0; i < unitOfMeasurement.length; i++) {
            if (unitOfMeasurement[i].id == this.metric[index].id) {
              this.metric[index].isChecked = unitOfMeasurement[i].isChecked;
            }
          }
        }
        let arr = [];
        for (let index = 0; index < this.metric.length; index++) {
          if (this.metric[index].isChecked == true) {
            arr.push(this.metric[index]);
          }
        }
        this.unitOfMeasurement = arr;
        this.treatmentNotes.treatmentData[itemTypeIndex].vitals.measurements = this.unitOfMeasurement;
        this.itemType.vitals.measurements = this.unitOfMeasurement;
        this.treatmentNotes.treatmentData[itemTypeIndex].vitals.vitalType = 'Metric';
        this.itemType.vitals.vitalType = 'Metric';
        this.vitalsLabel = 'Metric';
      } else {
        for (let index = 0; index < this.imperial.length; index++) {
          for (let i = 0; i < unitOfMeasurement.length; i++) {
            if (unitOfMeasurement[i].id == this.imperial[index].id) {
              this.imperial[index].isChecked = unitOfMeasurement[i].isChecked;
            }
          }
        }
        let arr = [];
        for (let index = 0; index < this.metric.length; index++) {
          if (this.metric[index].isChecked == true) {
            arr.push(this.metric[index]);
          }
        }
        this.unitOfMeasurement = arr;
        this.treatmentNotes.treatmentData[itemTypeIndex].vitals.measurements = this.unitOfMeasurement;
        this.itemType.vitals.measurements = this.unitOfMeasurement;
        this.treatmentNotes.treatmentData[itemTypeIndex].vitals.vitalType = 'Imperial';
        this.itemType.vitals.vitalType = 'Imperial';
        this.vitalsLabel = 'Imperial';
      }
      // this.itemType.vitals.vitalType = this.vitalModel.selectedVitalValue;
      // this.vitalsLabel = this.vitalModel.selectedVitalValue;      

      // vitals values null when Unit of Measurement change
      if (this.vitalsChangeStatus == 1) {
        this.unitOfMeasurement = this.treatmentNotes.treatmentData[itemTypeIndex].vitals.measurements;
        this.unitOfMeasurement.forEach(element => {
          element.value = "";
        });
        this.vitalsChangeStatus = 0;
      }
    }
    this.itemType = JSON.parse(JSON.stringify(this.itemType));
    this.treatmentNotes.treatmentData[itemTypeIndex] = this.itemType;
  }

  displayMessage(message) {
    if (
      message != undefined &&
      message != ''
    ) {
      this.displaySuccessMessage = message;
      setInterval(
        (a) => {
          this.displaySuccessMessage = '';
          message = '';
        },
        10000,
        []
      );
    }
  }

  numberChange(itemTypeId: number, value: any, type: string) {
    if (type === 'Minimum') {
      this.numberModel.number.minimum = parseInt(value);
    } else if (type === 'Maximum') {
      this.numberModel.number.maximum = parseInt(value);
    } else if (type === 'Default') {
      this.numberModel.number.default = parseInt(value);
    } else if (type === 'Minimum Duplicate') {
      this.numberModel.numberDuplicate.minimum = parseInt(value);
    } else if (type === 'Maximum Duplicate') {
      this.numberModel.numberDuplicate.maximum = parseInt(value);
    } else if (type === 'Default Duplicate') {
      this.numberModel.numberDuplicate.default = parseInt(value);
    }
  }

  edit(itemTypeId: number, type: string) {
    this.itemType = new ItemTypeModel();
    const data = this.treatmentNotes.treatmentData.find(
      (m) => m.id === itemTypeId
    );
    this.itemType = JSON.parse(JSON.stringify(data));
    if (type == 'textarea') {
      this.openeditform = true;
    }
    if (type == 'cheif') {
      this.openChiedform = true;
    }
    if (type == 'vitals') {
      this.openvitaleditform = true;
      // already commented
      // this.itemType.vitals.vitalType = "Metric";
      // this.itemType.vitals.measurements = this.unitOfMeasurement;
      // this.unitOfMeasurement = []
      
      // new commented
      // for (let index = 0; index < this.metric.length; index++) {
      //   this.metric[index].isChecked = false;
      //   for (let i = 0; i < this.treatmentNotes.treatmentData[itemTypeId - 1].vitals.measurements.length; i++) {
      //     if (this.treatmentNotes.treatmentData[itemTypeId - 1].vitals.measurements[i].id == this.metric[index].id) {
      //       this.metric[index].isChecked = true;
      //       this.metric[index].value = this.treatmentNotes.treatmentData[itemTypeId - 1].vitals.measurements[i].value;
      //     }
      //   }
      // }

      this.unitOfMeasurement = this.metric;
      // this.treatmentNotes.treatmentData[itemTypeId - 1].vitals.measurements.forEach(val => this.unitOfMeasurement.push(Object.assign({}, val)))      
    }
    if (type == 'heading') {
      this.openheadingeditform = true;
    }
    if (type == 'number') {
      this.opennumbereditform = true;
      // this.itemType.helpText = "Enter values between" + this.itemType.number.minimum + " to " + this.itemType.number.maximum;
      // if (this.itemType.numberDuplicate) {
      // this.itemType.helpTextDuplicate = "Enter values between" + this.itemType.numberDuplicate.minimum + " to " + this.itemType.numberDuplicate.maximum;
      // }
      // this.itemType.number.minimum = 1;
      // this.itemType.number.maximum = 5;
    }
    if (type == 'datetime') {
      this.opendatetimeeditform = true;
    }
    if (type == 'dropdown') {
      this.itemType.helpTextDuplicate = 'Select any of the dropdown values';
      this.itemType.itemLabelDuplicate = 'Dropdown 2';
      this.opendroupdwoneditform = true;
    }
    if (type == 'radio') {
      this.openradioeditform = true;
    }
    if (type == 'checkbox') {
      this.opencheckboxeditform = true;
    }
    if (type == 'range') {
      this.openrangeeditform = true;
      this.itemType.helpTextDuplicate = 'Move the slider to select a value';
      this.itemType.itemLabelDuplicate = 'Range or Scale 2';
    }
    if (type == 'optionlist') {
      this.openoptionlisteditform = true;
      this.itemType.helpTextDuplicate = 'Select a choice(s) from a list of subjects';
      this.itemType.itemLabelDuplicate = 'Option List 2';
    }
    if (type == 'tabledata') {
      this.opentabledataeditform = true;
    }
    if (type == 'attachfile') {
      this.openatatchfileditform = true;
    }
    if (type == 'instruction') {
      this.openinstructioneditform = true;
    }
    if (type == 'chief') {
      this.openchiefeditform = true;
    }
  }

  // Range or Scale
  rangeOrScaleChange(value: any, type: string) {
    if (type === 'Minimum') {
      this.rangeScaleModel.rangeOrScale.minimum = parseInt(value);
    } else if (type === 'Maximum') {
      this.rangeScaleModel.rangeOrScale.maximum = parseInt(value);
    } else if (type === 'Default') {
      this.rangeScaleModel.rangeOrScale.default = parseInt(value);
    } else if (type === 'Minimum Duplicate') {
      this.rangeScaleModel.rangeOrScaleDuplicate.minimum = parseInt(value);
    } else if (type === 'Maximum Duplicate') {
      this.rangeScaleModel.rangeOrScaleDuplicate.maximum = parseInt(value);
    } else if (type === 'Default Duplicate') {
      this.rangeScaleModel.rangeOrScaleDuplicate.default = parseInt(value);
    }
    //this.updateRange();
  }

  mirroredValueChange($event: { checked: boolean }, type) {
    let selectedData = _.find(this.treatmentNotes.treatmentData, { itemLabel: 'Range or Scale' });
    if (selectedData) {
      selectedData.showMirrored = $event.checked;
    }
    this.itemType.showMirrored = $event.checked;
    if (type == 'heading') {
      this.itemType.itemLabelDuplicate = 'Heading 2';
    }
    if (type == 'datetime') {
      this.itemType.itemLabelDuplicate = 'Date Time 2';
    }
    if (type == 'textarea') {
      this.itemType.itemLabelDuplicate = 'Text';
    }
    if (type == 'radio') {
      this.itemType.helpTextDuplicate = 'Select any of the values.';
      this.itemType.itemLabelDuplicate = 'Radio Button 2';
    }
    if (type == 'checkbox') {
      this.itemType.helpTextDuplicate = 'Select one or more checkboxes';
      this.itemType.itemLabelDuplicate = 'Check box 2';
      this.itemType.checkBoxesDuplicate = [];
      for (let i = 1; i <= 5; i++) {
        const m = new CheckBoxModel();
        m.id = i;
        m.value = '' + i;
        // if (i == 1) {
        //   m.default = true;
        // }
        // else {
        m.default = false;
        // }
        this.itemType.checkBoxesDuplicate.push(m);
      }
      this.itemType.selectedDuplicateDropdown = '';
    }
    if (type == 'number') {
      this.itemType.itemLabelDuplicate = 'Number 2';
      this.itemType.numberDuplicate.default = null;
      this.itemType.numberDuplicate.maximum = 5;
      this.itemType.numberDuplicate.minimum = 0;
      // selectedData.numberDuplicate.minimum = 0;
      // selectedData.numberDuplicate.maximum = 5;
      // selectedData.numberDuplicate.default = null;
      this.itemType.helpTextDuplicate = 'Enter values between 1 to 5';
      // selectedData.helpTextDuplicate = "Enter values between 1 to 5";
    }
    if (type == 'dropdown') {
      this.itemType.duplicateDropdowns = [];
      for (let i = 1; i <= 5; i++) {
        const m = new DropdownModel();
        m.id = i;
        m.value = '' + i;
        // if (i == 1) {
        //   m.default = true;
        // }
        // else {
        m.default = false;
        // }
        this.itemType.duplicateDropdowns.push(m);
      }
      this.itemType.selectedDuplicateDropdown = '';
    }
    if (type == 'radio') {
      this.itemType.duplicateRadioButtons = [];
      for (let i = 1; i <= 5; i++) {
        const m = new DropdownModel();
        m.id = i;
        m.value = '' + i;
        // if (i == 1) {
        //   m.default = true;
        // }
        // else {
        m.default = false;
        // }
        this.itemType.duplicateRadioButtons.push(m);
      }
      this.itemType.selectedDuplicateDropdown = '';
    }
    if (type == 'range') {
      this.itemType.rangeOrScaleDuplicate.minimum = 0;
      this.itemType.rangeOrScaleDuplicate.maximum = 5;
      this.itemType.rangeOrScaleDuplicate.default = 1;
      selectedData.rangeOrScaleDuplicate.minimum = 0;
      selectedData.rangeOrScaleDuplicate.maximum = 5;
      selectedData.rangeOrScaleDuplicate.default = 1;
    }
    if (type == 'optionlist') {
      this.itemType.itemLabelDuplicate = 'Option List';
      this.itemType.helpTextDuplicate = 'Select a choice(s) from a list of subjects';
      this.itemType.optionListStyleDuplicateId = 1;
      this.itemType.optionListSubjectsDuplicate = [];
      this.itemType.optionListChoicesDuplicate = [];
      let optionvalues = 65;
      for (let i = 1; i <= 3; i++) {
        const m = new KeySubjectModel();
        m.id = i;
        m.value = String.fromCharCode(optionvalues);
        this.itemType.optionListSubjectsDuplicate.push(m);
        optionvalues++;
      }
      for (let i = 1; i <= 3; i++) {
        const m = new KeyValueModel();

        m.id = i;
        if (i == 1) {
          m.value = 'worse';
          m.selected = false;
        } else if (i == 2) {
          m.value = 'same';
          m.selected = false;
        } else {
          m.value = 'better';
          m.selected = false;
        }
        this.itemType.optionListChoicesDuplicate.push(m);
        optionvalues++;
        this.itemType.optionListSubjectsDuplicate.forEach((element) => {
          element.optionListChoicesDuplicate.push(m);
        });
      }
    }
    this.itemType = JSON.parse(JSON.stringify(this.itemType));
  }

  mandatoryValueChange($event: { checked: boolean }) {
    this.itemType.isMandatory = $event.checked;
    if (this.itemType.isMandatory) {
      if (this.itemType.itemTypeText === 'Text') {
        this.treatmentNoteForm.controls['text'].setValidators([Validators.required]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['duplicatetext'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Date Time') {
        this.treatmentNoteForm.controls['selectTime'].setValidators([Validators.required]);
        this.treatmentNoteForm.controls['selectDate'].setValidators([Validators.required]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['duplicateselectTime'].setValidators([Validators.required]);
          this.treatmentNoteForm.controls['duplicateselectDate'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Number') {
        this.treatmentNoteForm.controls['number'].setValidators(Validators.required);
        this.treatmentNoteForm.controls['number'].setValidators([Validators.min(this.min), Validators.max(this.max)]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['duplicateNumber'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Dropdown') {
        this.treatmentNoteForm.controls['dropDown'].setValidators([Validators.required]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['duplicatedropDown'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Radio Buttons') {
        this.treatmentNoteForm.controls['radio'].setValidators([Validators.required]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['duplicateradio'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Check Boxes') {
        this.treatmentNoteForm.controls['checkBox'].setValidators([Validators.required]);
        if (this.itemType.showMirrored == true) {
          this.treatmentNoteForm.controls['checkBoxDuplicate'].setValidators([Validators.required]);
        }
      } else if (this.itemType.itemTypeText === 'Chief Complaint') {
        this.treatmentNoteForm.controls['chiefComplaint'].setValidators([Validators.required]);
      }
    }
  }

  // Date Time
  onDateChange(itemTypeId: number, $event: any, type) {
    if ($event && $event.value != null && (type != null && type != undefined)) {
      const m = new DateTimeModel();
      m.date = $event.value;
      if (this.treatmentNotes.treatmentData != undefined && this.treatmentNotes.treatmentData != null) {
        const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
          (x) => x.id === itemTypeId
        );
        if (type === 'time') {
          this.dateNow = m.date;
          this.treatmentNotes.treatmentData[itemTypeIndex].dateTime.date = m.date;
        } else {
          this.dateDuplicateNow = m.date;
          this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate.date = m.date;
        }
        this.treatmentNotes.treatmentData = JSON.parse(JSON.stringify(this.treatmentNotes.treatmentData));
      }
      if (type === 'time') {
        this.dateNow = m.date;
        this.treatmentNoteForm.controls['selectDate'].setValue(m.date);
      } else {
        this.dateDuplicateNow = m.date;
        this.treatmentNoteForm.controls['duplicateselectDate'].setValue(m.date);
      }
      if (this.itemType != null && this.itemType != undefined) {
        if (type === 'time') {
          this.dateNow = m.date;
          this.itemType.dateTime.date = m.date;
        } else {
          this.dateDuplicateNow = m.date;
          this.itemType.dateTimeDuplicate.date = m.date;
        }
        this.itemType = JSON.parse(JSON.stringify(this.itemType));
      }
    }
  }

  // Date Time
  onTimeChange(itemTypeId: number, $event: Date) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    if (
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTime === undefined
    ) {
      const m = new DateTimeModel();
      m.time = $event;
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTime = m;
    } else {
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTime.time = $event;
    }
  }

  // Duplicate Date Time
  onDuplicateDateChange(itemTypeId: number, $event: Date) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    if (
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate ===
      undefined
    ) {
      const m = new DateTimeModel();
      m.date = $event;
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate = m;
    } else {
      this.treatmentNotes.treatmentData[
        itemTypeIndex
      ].dateTimeDuplicate.date = $event;
    }
  }

  // Duplicate Date Time
  onDuplicateTimeChange(itemTypeId: number, $event: Date) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    if (
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate ===
      undefined
    ) {
      const m = new DateTimeModel();
      m.time = $event;
      this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate = m;
    } else {
      this.treatmentNotes.treatmentData[
        itemTypeIndex
      ].dateTimeDuplicate.time = $event;
    }
  }


  // Dropdown
  addDropDownOption() {
    const model = new DropdownModel();
    if (this.itemType.dropdowns.length < 10) {
      let max = 0;
      if (this.itemType.dropdowns && this.itemType.dropdowns.length > 0) {
        this.itemType.dropdowns.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let valuedata = max + 1;
        model.id = max + 1;
        model.value = '' + valuedata;
      } else {
        model.id = 1;
        model.value = '' + 1;
      }
      model.default = false;
      this.itemType.dropdowns.push(model);
    }

  }

  removeDropDownOption(id: number) {
    if (id != 1) {
      const ddIndex = this.itemType.dropdowns.findIndex((x) => x.id === id);
      if (ddIndex !== -1) {
        this.itemType.dropdowns.splice(ddIndex, 1);
      }
    } else {
      this.displayMessage('Atleast one value required.');
    }
  }

  selectedDropDownDefault(id: number) {
    if (this.itemType.dropdowns.length > 0) {
      this.itemType.dropdowns.forEach((c) => {
        const dIndex = this.itemType.dropdowns.findIndex(
          (x) => x.id === c.id
        );
        this.itemType.dropdowns[dIndex].default = false;
      });

      const ddIndex = this.itemType.dropdowns.findIndex(
        (x) => x.id === id
      );
      this.itemType.dropdowns[ddIndex].default = true;
      this.itemType.setAsDefault = true;

      this.itemType.selectedDropdown = id;
      this.selectedDropdownValue = this.itemType.dropdowns[ddIndex];
    }
  }

  inputDropDownValue(id: number, event: { target: { value: string } }) {
    const ddIndex = this.itemType.dropdowns.findIndex((x) => x.id === id);
    if (this.itemType.dropdowns[ddIndex].value.length < 0) {
      alert('Please fill all the text');
    } else {
      this.itemType.dropdowns[ddIndex].value = event.target.value;
    }
  }

  // Duplicate Dropdown
  addDuplicateDropDownOption() {
    const model = new DropdownModel();
    if (this.itemType.duplicateDropdowns.length < 10) {
      let max = 0;
      if (this.itemType.duplicateDropdowns.length > 0) {
        this.itemType.duplicateDropdowns.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let duplicatevaluedata = max + 1;
        model.id = max + 1;
        model.value = '' + duplicatevaluedata;
      } else {
        model.id = 1;
        model.value = '' + 1;
      }
      model.default = false;
      this.itemType.duplicateDropdowns.push(model);
    }
  }

  removeDuplicateDropDownOption(id: number) {
    if (id != 1) {
      const ddIndex = this.itemType.duplicateDropdowns.findIndex(
        (x) => x.id === id
      );
      if (ddIndex !== -1) {
        this.itemType.duplicateDropdowns.splice(ddIndex, 1);
      }
    } else {
      this.displayMessage('Atleast one value required.');
    }
  }

  selectedDropDownDuplicateDefault(id: number) {
    if (this.itemType.duplicateDropdowns.length > 0) {
      this.itemType.duplicateDropdowns.forEach((c) => {
        const dIndex = this.itemType.duplicateDropdowns.findIndex(
          (x) => x.id === c.id
        );
        this.itemType.duplicateDropdowns[dIndex].default = false;
      });

      const ddIndex = this.itemType.duplicateDropdowns.findIndex(
        (x) => x.id === id
      );
      this.itemType.duplicateDropdowns[ddIndex].default = true;
      this.itemType.setAsDuplicateDefault = true;
      this.itemType.selectedDuplicateDropdown = id;
    }
  }

  inputDropDownDuplicateValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.duplicateDropdowns.findIndex(
      (x) => x.id === id
    );
    this.itemType.duplicateDropdowns[ddIndex].value = event.target.value;
  }

  removeDropdownSetAsDefault() {
    this.itemType.dropdowns.map((x) => {
      x.default = false;
    });
    this.itemType.selectedDropdown = 0;
  }

  removeDuplicateDropdownSetAsDefault() {
    this.itemType.duplicateDropdowns.map((x) => {
      x.default = false;
    });
    this.itemType.selectedDuplicateDropdown = 0;
  }

  // Radio Button
  addRadioButtonOption() {
    const model = new RadioButtonModel();
    if (this.itemType.radioButtons.length < 10) {
      let max = 0;
      if (this.itemType.radioButtons.length > 0) {
        this.itemType.radioButtons.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let radiovalue = max + 1;
        model.id = max + 1;
        model.value = '' + radiovalue;
      } else {
        model.id = 1;
        model.value = '' + 1;
      }
      model.default = false;
      this.itemType.radioButtons.push(model);
    }
  }

  removeRadioButtonOption(id: number) {
    if (id != 1) {
      const ddIndex = this.itemType.radioButtons.findIndex((x) => x.id === id);
      if (ddIndex !== -1) {
        this.itemType.radioButtons.splice(ddIndex, 1);
      }
    } else {
      this.displayMessage('Atleast one value required.');
    }
  }

  selectedRadioButtonDefault(id: number) {
    if (this.itemType.radioButtons.length > 0) {
      this.itemType.radioButtons.forEach((c) => {
        const dIndex = this.itemType.radioButtons.findIndex(
          (x) => x.id === c.id
        );
        this.itemType.radioButtons[dIndex].default = false;
      });
      const ddIndex = this.itemType.radioButtons.findIndex((x) => x.id === id);
      this.itemType.radioButtons[ddIndex].default = true;
      this.itemType.setAsDefault = true;
      this.itemType.selectedDropdown = id;
    }
  }

  inputRadioButtonValue(id: number, event: { target: { value: string } }) {
    const ddIndex = this.itemType.radioButtons.findIndex((x) => x.id === id);
    this.itemType.radioButtons[ddIndex].value = event.target.value;
  }

  // Duplicate Radio Button
  addDuplicateRadioButtonOption() {
    const model = new RadioButtonModel();
    if (this.itemType.duplicateRadioButtons.length < 10) {
      let max = 0;
      if (this.itemType.duplicateRadioButtons.length > 0) {
        this.itemType.duplicateRadioButtons.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let dradiovalue = max + 1;
        model.id = max + 1;
        model.value = '' + dradiovalue;
      } else {
        model.id = 1;
        model.value = '' + 1;
      }

      model.default = false;
      this.itemType.duplicateRadioButtons.push(model);
    }
  }

  removeDuplicateRadioButtonOption(id: number) {
    if (id != 1) {
      const ddIndex = this.itemType.duplicateRadioButtons.findIndex(
        (x) => x.id === id
      );
      if (ddIndex !== -1) {
        this.itemType.duplicateRadioButtons.splice(ddIndex, 1);
      }
    } else {
      this.displayMessage('Atleast one value required.');
    }
  }

  selectedRadioButtonDuplicateDefault(id: number) {
    if (this.itemType.duplicateRadioButtons.length > 0) {
      this.itemType.duplicateRadioButtons.forEach((c) => {
        const dIndex = this.itemType.duplicateRadioButtons.findIndex(
          (x) => x.id === c.id
        );
        this.itemType.duplicateRadioButtons[dIndex].default = false;
      });

      const ddIndex = this.itemType.duplicateRadioButtons.findIndex(
        (x) => x.id === id
      );
      this.itemType.duplicateRadioButtons[ddIndex].default = true;
      this.itemType.setAsDuplicateDefault = true;
      this.itemType.selectedDuplicateDropdown = id;
    }
  }

  inputRadioButtonDuplicateValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.duplicateRadioButtons.findIndex(
      (x) => x.id === id
    );
    this.itemType.duplicateRadioButtons[ddIndex].value = event.target.value;
  }

  // Check Box
  addCheckBox() {
    const model = new CheckBoxModel();
    let max = 0;
    if (this.itemType.checkBoxes.length < 10) {
      if (this.itemType.checkBoxes.length > 0) {
        this.itemType.checkBoxes.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let chaeckvalue = max + 1;
        model.id = max + 1;
        model.value = '' + chaeckvalue;
      } else {
        model.id = 1;
        model.value = '';
      }
      model.default = false;
      this.itemType.checkBoxes.push(model);
    }
  }

  selectedCheckBoxDefault(id: number, $event) {
    if (this.itemType.checkBoxes.length > 0) {
      const ddIndex = this.itemType.checkBoxes.findIndex((x) => x.id === id);
      this.itemType.checkBoxes[ddIndex].default = $event;
      this.itemType.setAsDefault = true;
      this.itemType.selectedDropdown = id;
    }
  }

  inputCheckBoxValue(id: number, event: { target: { value: string } }) {
    const ddIndex = this.itemType.checkBoxes.findIndex((x) => x.id === id);
    this.itemType.checkBoxes[ddIndex].value = event.target.value;
  }

  // Duplicate Check Box
  addDuplicateCheckBox() {
    const model = new CheckBoxModel();
    let max = 0;
    if (this.itemType.checkBoxesDuplicate.length < 10) {
      if (this.itemType.checkBoxesDuplicate.length > 0) {
        this.itemType.checkBoxesDuplicate.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        let dchaeckvalue = max + 1;
        model.id = max + 1;
        model.value = '' + dchaeckvalue;
      } else {
        model.id = 1;
        model.value = '';
      }
      model.default = false;
      this.itemType.checkBoxesDuplicate.push(model);
    }
  }

  removeCheckboxOption(id: number) {
    const ddIndex = this.itemType.checkBoxes.findIndex(
      (x) => x.id === id
    );
    if (ddIndex !== -1) {
      this.itemType.checkBoxes.splice(ddIndex, 1);
    }
  }

  removeDuplicateCheckBox(id: number) {
    if (id != 1) {
      const ddIndex = this.itemType.checkBoxesDuplicate.findIndex((x) => x.id === id);
      if (ddIndex !== -1) {
        this.itemType.checkBoxesDuplicate.splice(ddIndex, 1);
      }
    } else {
      this.displayMessage('Atleast one value required.');
    }
  }

  selectedCheckBoxDuplicateDefault(id: number, $event) {
    if (this.itemType.checkBoxesDuplicate.length > 0) {
      const ddIndex = this.itemType.checkBoxesDuplicate.findIndex((x) => x.id === id);
      this.itemType.checkBoxesDuplicate[ddIndex].default = $event;
      this.itemType.setAsDuplicateDefault = true;
      this.itemType.selectedDuplicateDropdown = id;
    }
  }

  inputCheckBoxDuplicateValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.checkBoxesDuplicate.findIndex(
      (x) => x.id === id
    );
    this.itemType.checkBoxesDuplicate[ddIndex].value = event.target.value;
  }

  includeNotesValueChange($event: { checked: boolean }) {
    this.itemType.includeNotes = $event.checked;
  }

  removeRadiobuttonSetAsDefault() {
    this.itemType.radioButtons.map((x) => {
      x.default = false;
    });
  }

  removeDuplicateRadiobuttonSetAsDefault() {
    this.itemType.duplicateRadioButtons.map((x) => {
      x.default = false;
    });
  }

  removedCheckBoxSetAsDefault() {
    this.itemType.checkBoxes.map((x) => {
      x.default = false;
    });
  }

  removedDuplicateCheckBoxSetAsDefault() {
    this.itemType.checkBoxesDuplicate.map((x) => {
      x.default = false;
    });
  }

  // Option List

  selectedData(s, c) {
    this.subjectValue = c.value;
    this.choiceValue = c.value;
    if (s.value == 'A' || s.value == 'B' || s.value == 'C') {
      if (c.value == 'worse') {
        c.selected = true;
      } else if (c.value == 'same') {
        c.selected = true;
      } else {
        c.selected = true;
      }
    }
  }

  unselectedData(s, c) {
    this.subjectValue = c.value;
    this.choiceValue = c.value;
    if (s.value == 'A' || s.value == 'B' || s.value == 'C') {
      if (c.value == 'worse') {
        c.selected = false;
      } else if (c.value == 'same') {
        c.selected = false;
      } else {
        c.selected = false;
      }
    }
  }

  addOptionListSubject() {
    const model = new KeySubjectModel();
    let max = 0;
    if (this.itemType.optionListSubjects.length < 10) {
      if (this.itemType.optionListSubjects.length > 0) {
        this.itemType.optionListSubjects.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        model.id = max + 1;
      } else {
        model.id = 1;
      }
      model.value = '';
      model.selectedModel = [];
      this.itemType.optionListSubjects.push(model);
      // model.optionListChoices.push(this.itemType.optionListChoices)
    }
  }

  optionListStylesChange($event: number) {
    this.itemType.optionListStyleId = $event;
    // this.itemType.optionListStyleDuplicateId = $event;
  }

  removeOptionListSubject(id: number) {
    const ddIndex = this.itemType.optionListSubjects.findIndex(
      (x) => x.id === id
    );
    if (ddIndex !== -1) {
      this.itemType.optionListSubjects.splice(ddIndex, 1);
    }
  }

  inputOptionListSubjectValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.optionListSubjects.findIndex(
      (x) => x.id === id
    );
    this.itemType.optionListSubjects[ddIndex].value = event.target.value;
  }

  addOptionListChoice() {
    const model = new KeyValueModel();
    let max = 0;
    if (this.itemType.optionListChoices.length < 10) {
      if (this.itemType.optionListChoices.length > 0) {
        this.itemType.optionListChoices.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        model.selectedIndex = max;
        model.id = max + 1;
      } else {
        model.id = 1;
      }
      model.value = '';

      this.itemType.optionListChoices.push(model);

    }
  }

  removeOptionListChoice(id: number) {
    const ddIndex = this.itemType.optionListChoices.findIndex(
      (x) => x.id === id
    );
    if (ddIndex !== -1) {
      this.itemType.optionListChoices.splice(ddIndex, 1);
    }
  }

  inputOptionListChoiceValue(id: number, event: { target: { value: string } }) {
    const ddIndex = this.itemType.optionListChoices.findIndex(
      (x) => x.id === id
    );
    this.itemType.optionListChoices[ddIndex].value = event.target.value;
  }

  // Duplicate Option List
  addOptionListSubjectDuplicate(itemTypeId: number) {
    const model = new KeySubjectModel();
    let max = 0;
    if (this.itemType.optionListSubjectsDuplicate.length < 10) {
      if (this.itemType.optionListSubjectsDuplicate.length > 0) {
        this.itemType.optionListSubjectsDuplicate.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        model.id = max + 1;
        model.selectedIndex = max;
      } else {
        model.id = 1;
      }
      model.value = '';
      this.itemType.optionListSubjectsDuplicate.push(model);
    }
  }

  optionListStylesDuplicateChange($event: number) {
    this.itemType.optionListStyleDuplicateId = $event;
  }

  removeOptionListSubjectDuplicate(id: number) {
    const ddIndex = this.itemType.optionListSubjectsDuplicate.findIndex(
      (x) => x.id === id
    );
    if (ddIndex !== -1) {
      this.itemType.optionListSubjectsDuplicate.splice(ddIndex, 1);
    }
  }

  inputOptionListSubjectDuplicateValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.optionListSubjectsDuplicate.findIndex(
      (x) => x.id === id
    );
    this.itemType.optionListSubjectsDuplicate[ddIndex].value =
      event.target.value;
  }

  addOptionListChoiceDuplicate(itemTypeId: number) {
    const model = new KeyValueModel();
    let max = 0;
    if (this.itemType.optionListChoicesDuplicate.length < 10) {
      if (this.itemType.optionListChoicesDuplicate.length > 0) {
        this.itemType.optionListChoicesDuplicate.forEach((x) => {
          if (x.id > max) {
            max = x.id;
          }
        });
        model.selectedIndex = max;
        model.id = max + 1;
      } else {
        model.id = 1;
      }
      model.value = '';
      model.selected = false;
      this.itemType.optionListChoicesDuplicate.push(model);
    }
  }

  removeOptionListChoiceDuplicate(id: number) {
    const ddIndex = this.itemType.optionListChoicesDuplicate.findIndex(
      (x) => x.id === id
    );
    if (ddIndex !== -1) {
      this.itemType.optionListChoicesDuplicate.splice(ddIndex, 1);
    }
  }

  inputOptionListChoiceDuplicateValue(
    id: number,
    event: { target: { value: string } }
  ) {
    const ddIndex = this.itemType.optionListChoicesDuplicate.findIndex(
      (x) => x.id === id
    );
    this.itemType.optionListChoicesDuplicate[ddIndex].value =
      event.target.value;
  }

  // Table

  tableRowChange(itemTypeId: number, $event: number) {
    let tabledata = (($event) - (this.itemType.table.rowHeader.length));
    if (tabledata >= 0) {
      for (let i = 1; i <= tabledata; i++) {
        const m = new KeyValueModel();
        m.id = this.itemType.table.rowHeader.length + 1;
        m.value = '';
        this.itemType.table.rowHeader.push(m);
      }
    } else {
      let newValue = Math.abs(tabledata);
      let spliceRecord = this.itemType.table.rowHeader.length - newValue;
      this.itemType.table.rowHeader.splice(spliceRecord, newValue);
    }
    this.itemType.table.rows = this.itemType.table.rowHeader.length;
  }

  tableColumnChange(itemTypeId: number, $event: number) {
    let tabledata = (($event) - (this.itemType.table.columnHeader.length));
    if (tabledata >= 0) {
      for (let i = 1; i <= tabledata; i++) {
        const m = new KeyValueModel();
        m.id = this.itemType.table.columnHeader.length + 1;
        m.value = '';
        this.itemType.table.columnHeader.push(m);
      }
    } else {
      let newValue = Math.abs(tabledata);
      let spliceRecord = this.itemType.table.columnHeader.length - newValue;
      this.itemType.table.columnHeader.splice(spliceRecord, newValue);
    }
    this.itemType.table.columns = this.itemType.table.columnHeader.length;
  }

  tableRowHeaderInput(
    itemTypeId: number,
    rowId: number,
    event: { target: { value: string } }
  ) {

    const rowIndex = this.itemType.table.rowHeader.findIndex(
      (x) => x.id === rowId
    );
    this.itemType.table.rowHeader[rowIndex].id = rowId;
    this.itemType.table.rowHeader[rowIndex].value = event.target.value;
  }

  tableColumnHeaderInput(
    itemTypeId: number,
    columnId: number,
    event: { target: { value: string } }
  ) {
    const columnIndex = this.itemType.table.columnHeader.findIndex(
      (x) => x.id === columnId
    );
    this.itemType.table.columnHeader[columnIndex].id = columnId;
    this.itemType.table.columnHeader[columnIndex].value = event.target.value;
  }

  vitalsChange(itemTypeId: number, value: string) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    this.vitalModel.selectedVitalValue = value;

    if (this.vitalsChangeStatus == 0) {
      this.vitalsChangeStatus = 1;
    } else {
      this.vitalsChangeStatus = 0;
    }        
  }

  onInput(itemTypeId) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    this.unitOfMeasurement = this.treatmentNotes.treatmentData[itemTypeIndex].vitals.measurements;
    this.getChecked();
    setTimeout(() => {
      if (this.keyValue == true) {
        let bmivalue;
        if (this.vitalsLabel == 'Metric') {
          let height = this.heightvlue / 100;
          let hv = height * height;
          let wv = this.weightValue;
          bmivalue = wv / hv;
        } else if (this.vitalsLabel == 'Imperial') {
          let hv = this.heightvlue * this.heightvlue;
          let wv = this.weightValue;
          bmivalue = wv / hv;
        }

        this.unitOfMeasurement.forEach(element => {
          if (element.name == 'BMI (kg/m2)' || element.name == 'BMI (lb/in2)') {
            element.value = bmivalue;
            // element.isChecked = true;
          }
        });

      }
    }, 100);


  }

  getChecked() {
    var key = [];
    // setTimeout(() => {
    this.unitOfMeasurement.forEach(element => {
      if (element.name == 'Height (cm)' || element.name == 'Height (in)') {
        if (element.isChecked == true) {
          key.push(element.value);
          this.heightvlue = element.value;
        }
      }
      if (element.name == 'Weight (Kg)' || element.name == 'Weight (lbs)') {
        if (element.isChecked == true) {
          key.push(element.value);
          this.weightValue = element.value;
        }
      }


    });

    if (key.length == 2) {
      this.itemValue = key;
      this.keyValue = true;
    } else {
      this.keyValue = false;
    }
    // }, 500);
  }


  unitOfMeasurementChange(
    itemTypeId: number,
    value: { checked: boolean },
    vitalId: any, index
  ) {
    vitalId.isChecked = value.checked;
    // this.unitOfMeasurement[index].isChecked = value.checked
    this.getChecked();
    setTimeout(() => {

      this.unitOfMeasurement.forEach(element => {
        if (this.keyValue == true) {
          if (element.name == 'BMI (kg/m2)' || element.name == 'BMI (lb/in2)') {
            element.disable = false;
            element.isChecked = true;
          }
        } else if (element.name == 'BMI (kg/m2)' || element.name == 'BMI (lb/in2)') {
          element.disable = true;
          element.isChecked = false;

        }
      });

    }, 500);

    // let heightcheck:boolean;
    // let weightcheck:boolean;
    // if(vitalId.name == "Height (cm)"  && value.checked === true ){
    //   heightcheck = true
    // }
    // if(vitalId.name == "Weight (Kg)" && value.checked === true){
    //   weightcheck = true
    // }
    // if(heightcheck == true  && weightcheck == true ){
    //   this.unitOfMeasurement.forEach(element => {
    //     if(element.name == "BMI (kg/m2)"){
    //       element.disable = false;
    //     }
    //   })
    // }
    // const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
    //   (x) => x.id === itemTypeId
    // );
    // // this.itemType.vitals.measurements = []
    // if (value.checked === true) {
    //   // this.itemType.vitals.measurements.push(
    //   //   vitalId
    //   // );
    //   // this.unitOfMeasurement.push(vitalId)
    //   vitalId.isChecked = true;
    // } else {
    //   vitalId.isChecked = false;
    //   this.isDisplay = vitalId.isChecked;
    //   const index = this.treatmentNotes.treatmentData[
    //     itemTypeIndex
    //   ].vitals.measurements.findIndex(
    //     (x) => x.id === vitalId.id
    //   );
    //   // const index = this.treatmentNotes.treatmentData[
    //   //   itemTypeIndex
    //   // ].vitals.measurements.indexOf(vitalId);
    //   if (index !== -1) {
    //     value.checked = false
    //     this.treatmentNotes.treatmentData[
    //       itemTypeIndex
    //     ].vitals.measurements.splice(index, 1);

    //   }

    //   this.itemType.vitals.measurements = [];
    //   this.itemType.vitals.measurements = this.treatmentNotes.treatmentData[itemTypeIndex].vitals.measurements;
    // }


  }

  processFile(imageInput: any) {
    this.file = imageInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, this.file);
      this.getFile = this.selectedFile.src;
      this.itemType.fileImage = this.getFile;
    });
    reader.readAsDataURL(this.file);
  }

  downloadFile() {
    const downloadLink = document.createElement('a');
    const fileName = this.file.name;
    downloadLink.href = this.selectedFile.src;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  deleteimage() {
    this.selectedFile = null;
  }

  keyPressAlphaNumeric(event) {
    if (this.itemType.isMandatory == true) {
      var inp = String.fromCharCode(event.keyCode);
      if (/[a-zA-Z0-9\s]/.test(inp)) {
        return true;
      } else {
        event.preventDefault();
        return false;
      }
    } else {
      var inp = String.fromCharCode(event.keyCode);
      if (/[a-zA-Z0-9\s]/.test(inp)) {
        return true;
      } else {
        event.preventDefault();
        return false;
      }
    }
  }

  onClick(data) {
    this.selectedId = data.id;
    this.isActive = true;
  }

  //radio change
  selectedRadioButton(selectedRadio, item) {
    for (let index = 0; index < item.length; index++) {
      if (item[index].id == selectedRadio.id) {
        item[index].default = true;
      } else {
        item[index].default = false;
      }
    }
  }

  //checkbox change
  selectedCheckboxButton(selectedCheckbox, item, index, type) {
    if (selectedCheckbox.default == true) {
      item[index].default = false;
    } else {
      item[index].default = true;
    }
    if (type == 'checkbox') {
      this.selectedCheckBoxArray = _.filter(item, function (eachItem) {
        return eachItem.default == true;
      });
      if (this.selectedCheckBoxArray.length != 0) {
        this.treatmentNoteForm.controls['checkBox'].setValue(true);
      } else {
        this.treatmentNoteForm.controls['checkBox'].setValue([]);
      }
    } else {
      this.selectedCheckBoxDuplicateArray = _.filter(item, function (eachItem) {
        return eachItem.default == true;
      });
      if (this.selectedCheckBoxDuplicateArray.length != 0) {
        this.treatmentNoteForm.controls['checkBoxDuplicate'].setValue(true);
      } else {
        this.treatmentNoteForm.controls['checkBoxDuplicate'].setValue([]);
      }
    }

  }

  // dropdown change
  handleselectDropdownChange(event, item) {
    for (let index = 0; index < item.dropdowns.length; index++) {
      item.dropdowns[index].default = false;
    }
    var selectedDropdown = _.find(item.dropdowns, { id: event });
    selectedDropdown.default = true;
    item.selectedDropdown = event;
  }

  //duplicate dropdown change
  handleselectduplicateDropdownChange(event, item) {
    item.selectedDuplicateDropdown = event;
  }

  //checkbox change in option list
  selectedDatanew(selected, s, c) {
    if (selected == true) {
      this.unselectedData(s, c);
    } else {
      this.selectedData(s, c);
    }
  }

  //dropdown change in option list
  selectedValueofOptionList(event, modal, index, list) {
    // if (event.length == 0) {
    //   for (let i = 0; i < modal.list.length; i++) {
    //     if (index == modal.list[i].selectedIndex) {
    //       modal.list[i].selected = false;
    //     }
    //   }
    // }
    // else {
    //   for (let j = 0; j < modal.list.length; j++) {
    //     modal.list[j].selected = false;
    //     for (let i = 0; i < event.length; i++) {
    //       if (index == modal.list[j].selectedIndex) {
    //         if (event[i] == modal.list[j].id) {
    //           modal.list[j].selected = true;
    //         }
    //       }
    //     }
    //   }
    // }
    modal = _.filter(modal, function (eachData) {
      eachData.selected = false;
      return eachData;
    });

    var objData = _.find(modal, { id: event });
    if (objData) {
      objData.selected = true;
    }
  }

  //radio change in option list
  radioChange(list, id) {
    list = _.filter(list, function (eachData) {
      eachData.selected = false;
      return eachData;
    });

    var objData = _.find(list, { id: id });
    if (objData) {
      objData.selected = true;
    }
  }

  numberAdd(itemTypeId, event, type) {
    const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
      (x) => x.id === itemTypeId
    );
    if (type == 'number') {
      this.treatmentNotes.treatmentData[itemTypeIndex].number.default = parseInt(event.target.value);
      if (this.itemType) {
        this.itemType.number.default = parseInt(event.target.value);
      }
    }
    if (type == 'duplicatenumber') {
      this.treatmentNotes.treatmentData[itemTypeIndex].numberDuplicate.default = parseInt(event.target.value);
      if (this.itemType) {
        this.itemType.numberDuplicate.default = parseInt(event.target.value);
      }
    }
  }

  handleTimeValueChange(event, itemTypeId, type) {
    if (event && event.value != null && (type != null && type != undefined)) {
      const m = new DateTimeModel();
      m.time = event.value;
      if (this.treatmentNotes.treatmentData != undefined && this.treatmentNotes.treatmentData != null) {
        const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
          (x) => x.id === itemTypeId
        );
        if (type === 'time') {
          this.now = m.time;
          this.treatmentNotes.treatmentData[itemTypeIndex].dateTime.time = m.time;
        } else {
          this.nowDuplicate = m.time;
          this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate.time = m.time;
        }
        this.treatmentNotes.treatmentData[itemTypeIndex] = JSON.parse(JSON.stringify(this.treatmentNotes.treatmentData[itemTypeIndex]));
      }
      if (type === 'time') {
        this.now = m.time;
        this.treatmentNoteForm.controls['selectTime'].setValue(m.time);
      } else {
        this.nowDuplicate = m.time;
        this.treatmentNoteForm.controls['duplicateselectTime'].setValue(m.time);
      }
      if (this.itemType != null && this.itemType != undefined) {
        if (type === 'time') {
          this.now = m.time;
          this.itemType.dateTime.time = m.time;
        } else {
          this.nowDuplicate = m.time;
          this.itemType.dateTimeDuplicate.time = m.time;
        }
        this.itemType = JSON.parse(JSON.stringify(this.itemType));
      }
    }
  }


  //date picker start
  toggleCalendarPopup(val, from) {
    if (from == 'date') {
      this.isCalendarOpen = val;
    } else {
      this.isCalendarOpenDuplicate = val;
    }
  }

  get getSelectedFormattedDate() {
    if (this.currentView != 'day') {
      return this.getFormatedDateForWeek(this.currentDate, this.getDaysToAdd());
    }
    return this.getFormatedDate(this.currentDate);
  }

  get getSelectedFormattedDateDuplicate() {
    if (this.currentView != 'day') {
      return this.getFormatedDateForWeek(this.currentDuplicateDate, this.getDaysToAdd());
    }
    return this.getFormatedDate(this.currentDuplicateDate);
  }

  private getDaysToAdd(): number {
    return this.currentView == 'day' ? 1 : this.currentView == 'week' ? 6 : 4;
  }


  getFormatedDateForWeek(currentDate: Date, daysToAdd: number): string {
    let start = this.getFormatedDate(this.getStartDateOfWeek(currentDate));
    let end = this.getFormatedDate(this.getEndDateOfWeek(currentDate, daysToAdd));
    return start + ' to ' + end;
  }

  getFormatedDate(date) {
    return moment(date).format(this.dateFormat);
  }

  getStartDateOfWeek(date: Date): Date {
    return moment(date).startOf('isoWeek').toDate();
  }

  getEndDateOfWeek(date: Date, period: number): Date {
    return this.addAndGetNewValue(date, period, 'd');
  }

  addAndGetNewValue(date: Date, num, type: string): Date {
    return moment(date).clone().add(type, num).toDate();
  }

  calenderPopupFocus(isFocus: boolean, from: any) {
    // if (from == 'injuryDate') {
    //   this.isCalenderInjuryFocus = isFocus;
    // }
    if (from == 'date') {
      this.isCalenderStartFocus = isFocus;
    }
    if (from == 'duplicate') {
      this.isCalenderEndFocus = isFocus;
    }
  }

  openCalendarPopup(from) {
    this.isCalenderStartFocus = false;
    this.isCalenderInjuryFocus = false;
    this.isCalenderEndFocus = false;
    if (from == 'date') {
      let leftArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    } else {
      let leftArrContainer = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }

    this.changeDayLabelName(from);
  }

  optionChanged(from) {
    this.changeDayLabelName(from);
  }

  private changeDayLabelName(from) {
    let tables = null;
    if (from == 'date') {
      tables = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-body table');
    } else {
      tables = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-body table');
    }
    if (tables) {
      Array.from(tables).forEach((table: any) => {
        let nameLabes = table.querySelectorAll('thead tr th');
        Array.from(nameLabes).forEach((th: any) => {
          let name = th.getAttribute('abbr');
          th.innerHTML = name.substr(0, 2);
        });
      });
    }
  }

  onDateSelect(from, itemTypeId, event) {
    const m = new DateTimeModel();
    if (this.treatmentNotes.treatmentData != undefined && this.treatmentNotes.treatmentData != null) {
      const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
        (x) => x.id === itemTypeId
      );
      if (from == 'date') {
        this.treatmentNoteForm.controls['selectDate'].setValue(event);
        // this.treatmentNoteForm.controls['selectDate'].setValue(this.currentDate)
        this.treatmentNoteForm.controls['selectDate'].updateValueAndValidity()
        m.date = this.currentDate
        this.treatmentNotes.treatmentData[itemTypeIndex].dateTime.date = m.date
      } else {
        this.treatmentNoteForm.controls['duplicateselectDate'].setValue(event);
        this.treatmentNoteForm.controls['duplicateselectDate'].updateValueAndValidity()
        m.date = this.currentDuplicateDate
        this.treatmentNotes.treatmentData[itemTypeIndex].dateTimeDuplicate.date = m.date
      }
    }
    this.toggleCalendarPopup(false, from);
  }

  isApplyWeekOrMF(date: Date) {
    return (this.isInWeek(date) || this.isCurrDateInWeek(date)) && this.currentView != 'day';
  }

  isInWeek(date: Date) {
    return this.isBetween(this.currHvrDateStart, this.currHvrDateend, date);
  }

  isBetween(start: Date, end: Date, date: Date): boolean {
    let mm = moment(date);
    return mm.isSameOrAfter(start) && mm.isSameOrBefore(end);
  }

  isCurrDateInWeek(date: Date) {
    let currHvrDateStart = this.getStartDateOfWeek(this.currentDate);
    let currHvrDateend = this.getEndDateOfWeek(currHvrDateStart, this.getDaysToAdd());
    return this.isBetween(currHvrDateStart, currHvrDateend, date);
  }


  mouseHover(cell: any) {
    this.hoverDate = cell.date;
    this.setupStartEndDayOfWeek();
  }

  setupStartEndDayOfWeek() {
    this.currHvrDateStart = this.getStartDateOfWeek(this.hoverDate);
    this.currHvrDateend = this.getEndDateOfWeek(this.currHvrDateStart, this.getDaysToAdd());
  }

  setTodayDate(from) {
    if (from == 'date') {
      this.currentDate = this.getCurrentDate();
    } else {
      this.currentDuplicateDate = this.getCurrentDate();
    }
    this.prepareDateArrayForSdlr(from);
  }

  getCurrentDate(): Date {
    return moment().toDate();
  }

  prepareDateArrayForSdlr(from) {
    if (this.currentView == 'day') {
      if (from == 'date') {
        this.selectedDate = [new Dte(this.currentDate)];
      } else {
        this.selectedDateDuplicate = [new Dte(this.currentDuplicateDate)];
      }
    } else if (this.currentView == 'week') {
      if (from == 'date') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.currentDuplicateDate);
        this.selectedDateDuplicate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDateDuplicate.push(new Dte(date));
        }
      }
    } else {
      if (from == 'date') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.currentDuplicateDate);
        this.selectedDateDuplicate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDateDuplicate.push(new Dte(date));
        }
      }
    }
  }

  nextPrevDate(num: number, from) {
    if (from == 'date') {
      this.currentDate = this.addAndGetNewValue(this.currentDate, num * this.getDaysToAdd(), 'd');
    } else {
      this.currentDuplicateDate = this.addAndGetNewValue(this.currentDuplicateDate, num * this.getDaysToAdd(), 'd');
    }
    this.prepareDateArrayForSdlr(from);
  }


  getDatePickerAnchor(i: number): ElementRef | null {
    const elems = this.dateInputElem.toArray();
    return elems && elems.length > 0 ? elems[i] : null;
  }  
  //date picker end

}
