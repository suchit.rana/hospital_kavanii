import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemTypePreviewComponent } from './item-type-preview.component';

describe('ItemTypePreviewComponent', () => {
  let component: ItemTypePreviewComponent;
  let fixture: ComponentFixture<ItemTypePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemTypePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTypePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
