import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PatientService} from 'src/app/services/app.patient.service';
import {
  EligibleTreatmentNoteTemplatesModel,
  PatientModel,
  PatientTreatmentDetailsModel,
  PatientTreatmentNoteModel,
  PatientTreatmentNotesDetailsFinalizeModel,
  PatientTreatmentNotesModel,
  PractitonerLocationSpecialityModel,
  TemplateNameList,
} from 'src/app/models/app.patient.model';
import {Location} from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import {APP_DATE_FORMATS, AppDateAdapter} from 'src/app/shared/dt-format';
import {AppState} from 'src/app/app.state';
import {BusinessService} from 'src/app/services/app.business.service';
import {SettingsService} from 'src/app/services/app.settings.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ImageSnippet} from 'src/app/models/app.misc';
import {BaseItemComponent} from 'src/app/shared/base-item/base-item.component';
import {TreatmentNotesService} from 'src/app/services/app.treatmentnotes.services';
import {
  ChartModel,
  CheckBoxModel,
  DateTimeModel,
  DropdownModel,
  ItemTypeModel,
  KeySubjectModel,
  KeyValueModel,
  NumberModel,
  PatinetTreatmentNotesModel,
  RadioButtonModel,
  TableModel,
  TreatmentNotesModel,
  VitalsModel,
} from 'src/app/models/app.treatmentnotes.model';
import {ItemTypePreviewComponent} from './item-type-preview/item-type-preview.component';
import * as _ from 'lodash';
import {StaffService} from 'src/app/services/app.staff.service';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import {AppUtils} from 'src/app/shared/AppUtils';
import {OfferingsService} from '../../../../services/app.offerings.service';
import {BubbleAppointment, Offerings} from '../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {AppoinmentDateUtils} from '../../appointment/AppoinmentDateUtils';
import {FromPage, RouterState} from '../../../../shared/interface';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

const uniqueBy = require('lodash/uniqBy');

export class Dte {
  date: Date;
  text: string;
  id: string = '0';
  isDateTemplate: boolean = true;


  constructor(date: Date) {
    this.date = date;
    this.text = date.toDateString();
  }
}

@Component({
  selector: 'app-patient-treatmentnotes',
  templateUrl: './patient-treatmentnotes.component.html',
  styleUrls: ['./patient-treatmentnotes.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
  ],
})
export class PatientTreatmentnotesComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.patients_treatment_notes;

  routerState: RouterState;
  selectedFirstRecord: any;
  //date
  isCalendarOpen: boolean = false;
  isCalendarOpenDuplicate: boolean = false;
  @ViewChild('dateInput', {static: false}) dateInputElem: ElementRef;
  currentView = 'day';
  public calendarPopUpMargin = {horizontal: -60, vertical: 10};
  @ViewChild('calendarOpenBtn', {static: false}) public anchor: ElementRef;
  firstDay: number = 1;
  currentDate: Date = moment().toDate();
  currentDuplicateDate: Date = moment().toDate();
  dateFormat: string = 'ddd, D MMM YYYY';
  currHvrDateStart: Date;
  currHvrDateend: Date;
  hoverDate: Date = new Date();
  selectedDate: Dte[];
  selectedDateDuplicate: any;
  //time
  // now: any = new Date();
  nowDuplicate: any = new Date();
  dateNow: any = new Date();
  minDate: Date = new Date(1900, 0, 1);
  dateClear = new Date(2015, 11, 1, 6);
  dateDuplicateClear = new Date(2015, 11, 1, 6);
  dateDuplicateNow: any = new Date();
  timeInterval = 30;
  showFilters: boolean = true;
  show_in_seperate_page: boolean = false;
  showFlag: boolean = false;
  showmainFilterFlag: boolean = false;
  public steps: any = {hour: 0, minute: 30, second: 0};
  public mintime: Date = new Date(2000, 2, 10, 8, 0);
  public maxtime: Date = new Date(3000, 12, 31, 17, 0);
  practitionerList: any = [];
  finalisedPatientNoteId: any;
  @ViewChild('printContent', {static: false}) printContent: ElementRef;
  showflag: boolean = false;
  selectedFile?: ImageSnippet;
  exportWindowShow: boolean = true;
  file: File;
  treatmentTitle = '';
  treatmentRouteLink = '';
  treatmentRouteName = '';
  treatmentDescription = '';
  @Input() patientId: string;
  selectedOption = '1';
  IsShow: boolean = false;
  mytemplateshow: boolean = true;
  casetstaus: string;
  pendingcase: string = 'pending';
  @BlockUI() blockUI: NgBlockUI;
  apperance = 'outline';

  @ViewChild(ItemTypePreviewComponent, {static: false})
  @Output() finalizeButtonPress: EventEmitter<any> = new EventEmitter();
  childItemTypePreviewComponent: ItemTypePreviewComponent;
  lstcaseName: any;
  lstfiltercase: any;
  lsteditoptions: any;
  lstpindata: any = [];
  showprogressbar: boolean = true;
  showdraft: boolean = false;
  copttext: boolean = false;
  opendraftDetailpage: boolean = false;
  lstcasefilterdata: any = [];
  lstcaselistdata: any = {};
  lstdraftdata: any = [];
  lstdraftdata1: any = [];
  draftlist: any;
  lstprogressdata: any = [];
  lstfinaldata: any = [];
  new_lstfinaldata: any = [];
  lastCopyFinaldata: any;
  progressnotelist: any;
  ShowProgresslist: boolean = false;

  casefilterdata: any;
  opendeleteformsconfirmation: boolean = false;
  showdeletebtn: boolean = false;
  lstgeneralCase: any[];
  lsttreatmentNotesFor: any[];
  lstappointment: any[];
  public shortFormat = 'EEE, dd MMM yyy';
  // Text area
  showTextarea: boolean = true;

  // Heading Area
  openheadingeditform: boolean = false;
  showheadingarea: boolean = false;

  // Date time Area
  opendatetimeeditform: boolean = false;
  showdatetimearea: boolean = false;

  // Number area
  opennumbereditform: boolean = false;
  shownumber: boolean = false;

  // Droupdown
  opendroupdwoneditform: boolean = false;
  showdroupdown: boolean = false;
  droupdownarray: Array<any> = [];

  // Radio Button Area
  openradioeditform: boolean = false;
  showradio: boolean = false;
  radioattribute: any = [];
  radioarray: Array<any> = [];

  // CheckBox Area
  opencheckboxeditform: boolean = false;
  showcheckbox: boolean = false;
  checkboxattribute: any = [];
  checkboxarray: Array<any> = [];

  // Cheif Area
  showChiefarea: boolean = false;
  openChiedform: boolean = false;

  // Vital Area
  showvitalarea: boolean = false;
  openvitaleditform: boolean = false;
  showopticalarea: boolean = false;
  showMetric: boolean = true;
  showImperial: boolean = false;
  showeImperial: boolean = false;
  shomanvitals: boolean = false;
  public modeselect = 'Metric';

  // Optical Area

  // Range Scale
  max = 5;
  min = 0;
  showTicks = false;
  step = 1;
  tickInterval = 1;
  displayvalue: number;
  openrangeeditform: boolean = false;
  showrange: boolean = false;

  // Option List
  abcvalue: string;
  showoptionlist: boolean = false;
  openoptionlisteditform: boolean = false;
  buttonselection: boolean = true;
  checkboxselection: boolean = false;
  droupdownselection: boolean = false;
  radioselection: boolean = false;
  shoabcdata: string;
  optionlistarray: Array<any> = [];
  optionlistchoicearray: any = [];
  optionlistarrayattribute: Array<any> = [];

  // Table Data
  showtable: boolean = false;
  opentabledataeditform: boolean = false;

  // File Attachment data
  showattachfile: boolean = false;
  openatatchfileditform: boolean = false;

  // Instruction
  openinstructioneditform: boolean = false;
  showinstructionfile: boolean = false;

  // Chief Complaint
  openchiefeditform: boolean = false;
  showchieffile: boolean = false;

  // Vitals
  showvitalsarea: boolean = false;
  vitalstitle: string;
  showmatric: boolean = false;
  showimperial: boolean = false;
  degreesign: any;

  // Chart Data
  showChart: boolean = false;

  // Spine Data
  showSpineChart: boolean = false;

  addNewtemplateshow: boolean = false;
  IsAddItemShow: boolean = false;
  patitonDraftData: any = [];
  isShowAppointmet: boolean = false;
  isShowPerDate: boolean = false;
  openeditform: boolean = false;
  opendeleteform: boolean = false;
  edittemplateshow: boolean = false;
  opendeletenote: boolean = false;
  ShowMedicalDetails: boolean = false;
  showalerticon: boolean = false;
  openexportnote: boolean = false;
  openexportnotepreview: boolean = false;
  openemailnoteemail: boolean = false;
  expandclass: boolean = false;
  IsShowfilter: boolean = false;
  ShowGeneralTemplate: boolean = true;
  ShowGastroTemplate: boolean = true;
  openfiltercase: boolean = false;
  openfilterpractitioner: boolean = false;
  openfilterdaterange: boolean = false;
  opencasestatusform: boolean = false;
  showexpand: boolean = true;
  showcolapse: boolean = false;
  Showreopencase: boolean = false;
  showEditOptions: boolean = false;
  showchangedatetime: boolean = false;
  showchangepatient: boolean = false;
  showchangepatientcase: boolean = false;
  showchangepatientnote: boolean = false;
  showchangepatientdelete: boolean = false;
  pinnotes: boolean = false;
  opendischargeform: boolean = false;
  itemIndex: number;
  modaltitle: string;
  displaydata: any;
  medicalCondition: string;
  medication: string;
  allergy: string;
  casenamedata: string = 'all';
  generalcaseTemplatelist: any;
  gastrocaseTemplatelist: any;
  progresscasenoteList: any;
  progressgeneralcasenoteList: any;
  casestatustitle: string;
  storevalue: string;
  showpinEditOptions: boolean = false;
  drafttext: boolean = false;

  // for Progress Note Start
  lstselectcasedata: any = [];
  selectedRangeandAllCaseData: any = [];
  lstpreview: any = [];
  treatmentlistData: any = [];
  treatdatas: any;
  treatmentDatabtID: any;
  pinnotesData: any;
  openpopupDetails: any;
  changeselectdate: any = new Date();
  changeselecttime: any = new Date();
  changeselectappointment: string;
  changeselectCase: string;
  patientlistData: any = [];
  patientlistDataSearch: any = [];
  changeselectPatient: string;
  changeselectPatientId: string;
  changeselectNotes: string;
  deletetype: string;
  selectedCasefilter: string;
  checkvalue: any = [];
  select_all = false;
  lstpractitionerlist: any[];
  checkpractitionervalue: any = [];
  addNewPatienttemplateshow: boolean = false;
  showdatetimes: boolean = false;
  addNewTreatmenttemplateshow: boolean = false;
  selectFromDate: any;
  selectToDate: any;
  dischargeCaseData: any;
  dischargedescText: string;
  duplicateButton: boolean = false;
  duplicatetypetext: string;
  closepopupData: boolean = false;
  isLoading: boolean;
  dataLoaded: boolean;
  dischargebutton: boolean = false;
  reopenbutton: boolean = false;
  // for Progress Note End

  // For Validation Start
  datetimealert: any;

  public liked = false;
  patientTreatmentNotesModel: PatientTreatmentNotesModel;
  PatientTreatmentDetails: PatientTreatmentDetailsModel;
  treatmentNotes: PatinetTreatmentNotesModel;
  tempItemTemplates: ItemTypeModel[] = [];
  treatmentNotesData: TreatmentNotesModel;
  patientTreatmentNote: PatientTreatmentNoteModel[] = [];
  eligibleTreatment: EligibleTreatmentNoteTemplatesModel[] = [];
  templateNameTypeDisplay: string;
  templateNameDisplay: string;
  selectedTemplateId: string;
  isMandatoryFilled: boolean;
  isItemAdded: boolean;
  shownGroup = null;

  // SingleshownGroup = null;
  public heartIcon(): string {
    return this.liked ? 'k-icon k-i-upload' : 'k-icon k-i-download';
  }

  public toggleLike(): void {
    this.liked = !this.liked;
  }

  selected = '';
  public value: Date = new Date();
  public lsrservices: any = [];
  public selectedSizes: any = [];
  metric = [
    {
      id: 1,
      name: 'Body Temp (C)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 2,
      name: 'Pulse (BPM)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 3,
      name: 'Resp. Rate (bpm)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 4,
      name: 'BP (mm/ Hg)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 5,
      name: 'Weight (Kg)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 6,
      name: 'Height (cm)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 7,
      name: 'BMI (kg/m2)',
      value: '',
      isChecked: true,
      disable: true
    },
  ];

  imperial = [
    {
      id: 1,
      name: 'Body Temp (F)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 2,
      name: 'Pulse (BPM)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 3,
      name: 'Resp. Rate (bpm)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 4,
      name: 'BP (mm/ Hg)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 5,
      name: 'Weight (lbs)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 6,
      name: 'Height (in)',
      value: '',
      isChecked: true,
      disable: false

    },
    {
      id: 7,
      name: 'BMI (lb/in2)',
      value: '',
      isChecked: true,
      disable: true
    },
  ];
  unitOfMeasurement: { id: number; name: string }[];

  public itemTypes: Array<any> = [
    {
      id: 1,
      title: 'Heading',
      description: 'Section Heading',
      icon: '../../../assets/price-tag 3.jpg',
      json: '',
    },
    {
      id: 2,
      title: 'Date Time',
      description: 'Field to enter Date \ Time',
      icon: '../../../assets/calendar 4.jpg',
      json: '',
    },
    {
      id: 3,
      title: 'Text',
      description: 'A plain text area to type notes',
      icon: '../../../assets/A.png',
      json: '',
    },
    {
      id: 4,
      title: 'Number',
      description: 'Field to enter a single number',
      icon: './../../assets/one 2.png',
      json: '',
    },
    {
      id: 5,
      title: 'Dropdown',
      description:
        'Select one option from a list of options in a dropdown menu',
      icon: './../../assets/Group 334.png',
      json: '',
    },
    {
      id: 6,
      title: 'Radio Buttons',
      description: 'Select one option from a list of options in a radio button',
      icon: './../../assets/Group 170.png',
      json: '',
    },
    {
      id: 7,
      title: 'Check Boxes',
      description:
        'Select one or more checkboxes and optionally add a note to each',
      icon: './../../assets/Group 184.png',
      json: '',
    },
    {
      id: 8,
      title: 'Range or Scale',
      description:
        'Customizable range / scale / clider allows you to choose from a range of values',
      icon: './../../assets/range-scale.png',
      json: '',
    },
    {
      id: 9,
      title: 'Option List',
      description: 'Select a choice from list of subjects',
      icon: './../../assets/Group 182.png',
      json: '',
    },
    {
      id: 10,
      title: 'Table',
      description:
        'Set of facts or figures systematically displayed in rows and column',
      icon: './../../assets/Group 167.png',
      json: '',
    },
    {
      id: 11,
      title: 'Chart',
      description:
        'Draw or type notes on the provided Body chart or any image of your choice',
      icon: './../../assets/body_chart.svg',
      json: '',
    },
    {
      id: 12,
      title: 'File Attachment',
      description:
        'Upload any type of file,with a preview of most common file types',
      icon: './../../assets/paperclip 1.png',
      json: '',
    },
    {
      id: 13,
      title: 'Instruction',
      description:
        'A plain text area to type instructions.It will not appear in printed or exported Treatment Note',
      icon: './../../assets/Group 183.png',
      json: '',
    },
    {
      id: 14,
      title: 'Chief Complaint',
      description: 'Record the chief complaint or diagnosis',
      icon: './../../assets/Untitled-2 3.png',
      json: '',
    },
    {
      id: 15,
      title: 'Vitals',
      description:
        'Record weight, height, blood pressure, respiratory rate, and calculate BMI.',
      icon: './../../assets/vitals 1.png',
      json: '',
    },
    {
      id: 16,
      title: 'Spine',
      description: 'Checkboxes for each joint, sketch on the spine diagram',
      icon: './../../assets/spine 2.png',
      json: '',
    },
    {
      id: 17,
      title: 'Optical Measurements',
      description: 'Optical Measurements',
      icon: './../../assets/optical.png',
      json: '',
    },
  ];
  spine = [
    {
      id: 1,
      name: 'C0',
    },
    {
      id: 2,
      name: 'C1',
    },
    {
      id: 3,
      name: 'C2',
    },
    {
      id: 4,
      name: 'C3',
    },
    {
      id: 5,
      name: 'C4',
    },
    {
      id: 6,
      name: 'C5',
    },
    {
      id: 7,
      name: 'C6',
    },
    {
      id: 8,
      name: 'C7',
    },
    {
      id: 9,
      name: 'T1',
    },
    {
      id: 10,
      name: 'T2',
    },
    {
      id: 11,
      name: 'T3',
    },
    {
      id: 12,
      name: 'T4',
    },
    {
      id: 13,
      name: 'T5',
    },
    {
      id: 14,
      name: 'T6',
    },
    {
      id: 15,
      name: 'T7',
    },
    {
      id: 16,
      name: 'T8',
    },
    {
      id: 17,
      name: 'T9',
    },
    {
      id: 18,
      name: 'T10',
    },
    {
      id: 19,
      name: 'T11',
    },
    {
      id: 20,
      name: 'T12',
    },
    {
      id: 21,
      name: 'L1',
    },
    {
      id: 22,
      name: 'L2',
    },
    {
      id: 23,
      name: 'L3',
    },
    {
      id: 24,
      name: 'L4',
    },
    {
      id: 25,
      name: 'L5',
    },
    {
      id: 26,
      name: 'Sacrum',
    },
    {
      id: 27,
      name: 'SI AS',
    },
    {
      id: 28,
      name: 'SI PI',
    },
    {
      id: 29,
      name: 'Coccyx',
    },
  ];
  SpineNodes: any = [
    {label: 'C0', value: false},
    {label: ' C1', value: false},
    {label: ' C2', value: false},
    {label: ' C3', value: false},
    {label: ' C4', value: false},
    {label: ' C5', value: false},
    {label: ' C6', value: false},
    {label: ' C7', value: false},
    {label: ' T1', value: false},
    {label: ' T2', value: false},
    {label: ' T3', value: false},
    {label: ' T4', value: false},
    {label: ' T5', value: false},
    {label: ' T6', value: false},
    {label: ' T7', value: false},
    {label: ' T8', value: false},
    {label: 'T9', value: false},
    {label: ' T10', value: false},
    {label: ' T11', value: false},
    {label: ' T12', value: false},
    {label: ' L1', value: false},
    {label: ' L2', value: false},
    {label: ' L3', value: false},
    {label: ' L4', value: false},
    {label: ' L5', value: false},
    {label: ' Sacrum', value: false},
    {label: ' SI AS', value: false},
    {label: ' SI PI', value: false},
    {label: ' Coccyx', value: false}
  ];
  RightSpineNodes: any = [
    {label: 'C0', value: false},
    {label: ' C1', value: false},
    {label: ' C2', value: false},
    {label: ' C3', value: false},
    {label: ' C4', value: false},
    {label: ' C5', value: false},
    {label: ' C6', value: false},
    {label: ' C7', value: false},
    {label: ' T1', value: false},
    {label: ' T2', value: false},
    {label: ' T3', value: false},
    {label: ' T4', value: false},
    {label: ' T5', value: false},
    {label: ' T6', value: false},
    {label: ' T7', value: false},
    {label: ' T8', value: false},
    {label: 'T9', value: false},
    {label: ' T10', value: false},
    {label: ' T11', value: false},
    {label: ' T12', value: false},
    {label: ' L1', value: false},
    {label: ' L2', value: false},
    {label: ' L3', value: false},
    {label: ' L4', value: false},
    {label: ' L5', value: false},
    {label: ' Sacrum', value: false},
    {label: ' SI AS', value: false},
    {label: ' SI PI', value: false},
    {label: ' Coccyx', value: false}
  ];
  zDate = new Date(Date.UTC(2012, 12, 5, 9, 0));
  treatmentNoteForm: FormGroup = new FormGroup({
    casesId: new FormControl('', Validators.required),
    treatmentNotesFor: new FormControl('', Validators.required),
    appointmentId: new FormControl(''),
    service: new FormControl(''),
    entryOrAppointmentDate: new FormControl(''),
    treatmentName: new FormControl(''),
    text: new FormControl(''),
    duplicatetext: new FormControl(''),
    selectDate: new FormControl(''),
    duplicateselectDate: new FormControl(''),
    duplicateselectTime: new FormControl(''),
    selectTime: new FormControl(''),
    number: new FormControl(''),
    duplicateNumber: new FormControl(''),
    dropDown: new FormControl(''),
    duplicatedropDown: new FormControl(''),
    radio: new FormControl(''),
    duplicateradio: new FormControl(''),
    checkBox: new FormControl(''),
    checkBoxDuplicate: new FormControl(''),
    complaint: new FormControl(''),
    // spine:new FormControl(""),
    inputnotes: new FormControl(''),
    inputnotesDuplicate: new FormControl(''),
    chiefComplaint: new FormControl('')
  });

  locationList: any[];
  public specialityData: any[] = [];
  practitonerLocationSpecialityModel: PractitonerLocationSpecialityModel[] = [];
  CopypractitonerLocationSpecialityModel: PractitonerLocationSpecialityModel[] = [];
  SecoundCopypractitonerLocationSpecialityModel: PractitonerLocationSpecialityModel[] = [];
  // selectedStates = this.patientlistData;
  anotherArray = this.patientlistData;

  filterListCareUnit(val) {
    this.patientlistData = this.anotherArray.filter(
      (patients) => {
        // patients.patientSearchName.indexOf(val) > -1
        if (patients.patientSearchName.toString().toLowerCase().indexOf(val.toLowerCase()) !== -1) {
          return true;
        }
        return false;
      }
    );
    this.patientlistDataSearch = this.patientlistData;
  }

  public placeHolder: any = {id: '', caseName: 'Case *'};
  public placeHolderTrest: any = {id: '', value: 'Select'};
  public placeHolderAppointment: any = {id: '', value: 'Select Appointment'};
  public placeHolderservice: any = {value: '', text: 'Select Service'};
  isCalenderStartFocus: boolean = false;
  defaultTemplateId: string;

  @ViewChild('calendarOpenBtn1', {static: false}) public anchor1: ElementRef;
  // @ViewChild('popup', { static: false, read: ElementRef }) public popup: ElementRef;
  @ViewChild('popup1', {static: false, read: ElementRef}) public popup1: ElementRef;

  shiftKeyPress: boolean = false;

  @HostListener('document:click', ['$event'])
  documentClick(event: any): void {
    if (event.type == 'click' && this.shiftKeyPress) {
      this.shiftKeyPress = false;
      this.selectRangeofPreview();
    }
    if (!this.isCalenderStartFocus && !this.contains1(event.target)) {
      this.toggleCalendarPopup(false, 'selectdate');
    }
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(e) {
    this.shiftKeyPress = false;
    if (e.shiftKey) {
      console.log('shift key pressed.');
      this.shiftKeyPress = true;
    }
  }


  // public documentClick(event: any): void {
  //   console.log(event)
  //   if (!this.isCalenderStartFocus && !this.contains1(event.target)) {
  //     this.toggleCalendarPopup(false, 'selectdate');
  //   }
  // }

  contains1(target: any): boolean {
    if (this.anchor1 != undefined && this.popup1 != undefined) {
      return (
        this.anchor1.nativeElement.contains(target) ||
        (this.popup1 ? this.popup1.nativeElement.contains(target) : false)
      );
    }
  }

  constructor(
    public location: Location,
    protected router: Router,
    public appState: AppState,
    public businessService: BusinessService,
    private settingsService: SettingsService,
    private patientService: PatientService,
    private staffService: StaffService,
    public _route: ActivatedRoute,
    private treatmentNotesService: TreatmentNotesService,
    private render: Renderer2,
    private offeringsService: OfferingsService
  ) {
    super(location, null, appState);
    if (this.router.getCurrentNavigation() != null) {
      this.routerState = this.router.getCurrentNavigation().extras.state as RouterState;
    } else {
      this.routerState = history.state;
    }
    let count = 0;
    let countshow = 0;
    this.render.listen('window', 'click', (e: Event) => {
      if (this.ShowMedicalDetails == true) {
        this.ShowMedicalDetails = false;
      }
      // if (this.IsShowfilter == true) {
      setTimeout(() => {
        if (this.showmainFilterFlag == true) {
          countshow++;
        } else {
          countshow = 0;
          this.IsShowfilter = false;
        }
        if (countshow > 2) {
          countshow = 0;
          this.IsShowfilter = false;
        }
      }, 100);
      //   this.IsShowfilter = false;
      // }
      if (this.shownGroup != null) {
        this.shownGroup = null;
      }
      setTimeout(() => {
        if (this.showFlag == true) {
          count++;
        } else {
          count = 0;
          this.IsShow = false;
        }
        if (count > 2) {
          count = 0;
          this.IsShow = false;
        }
      }, 100);

      if (this.IsAddItemShow == true) {
        this.IsAddItemShow = false;
      }
    });
  }

  ngOnInit() {

    this.changeselectdate = '';
    this.changeselectappointment = '';
    this.changeselectCase = '';
    this.changeselectPatient = '';
    this.changeselectPatientId = '';
    this.changeselectNotes = '';
    this.selectedCasefilter = '';
    this.selectFromDate = '';
    this.selectToDate = '';
    this.dischargedescText = '';
    this.isItemAdded = false;
    this.treatmentNotes = new PatinetTreatmentNotesModel();
    console.log('**********************************************************', this.treatmentNotes.treatmentData);

    //this.treatmentNotes.treatmentName = "";
    this._route.params.subscribe((params) => {
      if (params.patientId) {
        this.patientId = params.patientId;
      }
    });
    this.lsttreatmentNotesFor = [
      {id: '1', value: 'Existing Appointment'},
      {id: '2', value: 'A Particular Date'},
    ];
    if (this.ifFromState()) {
      this.treatmentNoteForm.get('treatmentNotesFor').setValue('1');
      this.getselectionChange({value: '1'});
    }
    this.getServices();
    this.getAppointments();
    this.populateLanding();
    this.casetemplate(this.casenamedata, 'fromInit');
    this.lstfiltercase = [
      {
        name: 'case',
      },
      {
        name: 'practitioner',
      },
      {
        name: 'date range',
      },
    ];
    this.lsteditoptions = [
      {
        name: 'change date / time',
      },
      {
        name: 'change case',
      },
      {
        name: 'change patient',
      },
      {
        name: 'change notes name',
      },
      {
        name: 'delete',
      },
    ];
    this.getPatientList();
    this.getCaseFilterData();
  }

  // Get Services for service field
  getServices() {
    if (this.ifFromState()) {
      // { id: '6f208b68-07c3-4555-8b1a-046112b60751', value: '30-Sept-2020 @ 2:30 PM - Pending' }
      const service = this.routerState.data.service as Offerings;
      const data = {
        value: service.serviceId,
        text: service.code + ' - ' + service.name
      };
      this.lsrservices = [data];
      this.treatmentNoteForm.get('service').setValue([data]);
      this.onSizeChange([data]);
    } else {
      this.offeringsService.getAllService().subscribe(response => {
        this.lsrservices = response;
        this.lsrservices.map(data => {
          data['value'] = data['id'];
          data['text'] = data.serviceCode + ' - ' + data.serviceName;
        });
      });
    }
  }

  // Get Appointments For Related Appointment Field
  getAppointments() {
    if (this.ifFromState()) {
      // { id: '6f208b68-07c3-4555-8b1a-046112b60751', value: '30-Sept-2020 @ 2:30 PM - Pending' }
      const appt = this.routerState.data.appointment as BubbleAppointment;
      const data = {
        id: appt.appointmentEntryId,
        value: AppoinmentDateUtils.format(appt.startDateTime, 'DD-MMMM-YYYY @ HH:mm A') + ' - ' + appt.statusValue
      };
      this.lstappointment = [data];
      this.treatmentNoteForm.get('appointmentId').setValue(appt.appointmentEntryId);
    } else {
      /**
       * Fetch Data From API
       */
    }
  }

  public onSizeChange(value) {
    this.selectedSizes = value;
  }

  public valueChange(value: any): void {
  }

  getselectionChange(optionlistd) {
    this.treatmentNoteForm.get('entryOrAppointmentDate').updateValueAndValidity();
    if (optionlistd.value == '1') {
      this.isShowAppointmet = true;
      this.isShowPerDate = false;
      this.treatmentNoteForm.get('appointmentId').setValidators([Validators.required]);
      this.treatmentNoteForm.get('service').setValidators([Validators.required]);
      this.treatmentNoteForm.get('entryOrAppointmentDate').setValue('');
      this.treatmentNoteForm.get('entryOrAppointmentDate').setValidators([]);
      this.treatmentNoteForm.get('appointmentId').updateValueAndValidity();
      this.treatmentNoteForm.get('service').updateValueAndValidity();
      this.treatmentNoteForm.get('entryOrAppointmentDate').updateValueAndValidity();
      // setTimeout(() => {
      //   if (this.lsrservices.length == 1) {
      //     this.onSizeChange(this.lsrservices)
      //     this.treatmentNoteForm.controls['service'].setValue([this.lsrservices[this.lsrservices.length - 1].value])
      //     this.treatmentNoteForm.controls['service'].updateValueAndValidity()
      //   }
      // }, 500);
    } else if (optionlistd.value == '2') {
      this.isShowAppointmet = false;
      this.isShowPerDate = true;
      this.treatmentNoteForm.get('entryOrAppointmentDate').setValidators([Validators.required]);
      this.treatmentNoteForm.get('entryOrAppointmentDate').updateValueAndValidity();
      this.treatmentNoteForm.controls['entryOrAppointmentDate'].setValue(this.currentDuplicateDate);
      this.treatmentNoteForm.get('appointmentId').setValue('');
      this.treatmentNoteForm.get('service').setValue([]);
      this.treatmentNoteForm.get('appointmentId').setValidators([]);
      this.treatmentNoteForm.get('service').setValidators([]);
      this.treatmentNoteForm.get('appointmentId').updateValueAndValidity();
      this.treatmentNoteForm.get('service').updateValueAndValidity();
    } else {
      this.isShowAppointmet = false;
      this.isShowPerDate = false;
    }
  }

  clickevnt() {
    this.IsAddItemShow = false;
  }

  populateLanding() {
    this.patientMedicalDetails();
    this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
      this.locationList = locations;
    });

    this.settingsService.getAllSpecialties().subscribe((specialties) => {
      this.specialityData = specialties;
    });
    this.patientService
      .getLocationSpecialityByPractitonerId(this.appState.userProfile.id)
      .subscribe((data: PractitonerLocationSpecialityModel[]) => {
        if (data.length > 0) {
          this.practitonerLocationSpecialityModel = data;
          this.CopypractitonerLocationSpecialityModel = data;

          this.getTemplateNameList();
        }
      });
    this.getCaseFilterData();

    this.staffService.getAllPractitonersByLocationId(this.appState.selectedUserLocation.id).subscribe((data) => {
      this.lstpractitionerlist = data;
      for (let index = 0; index < this.lstpractitionerlist.length; index++) {
        this.lstpractitionerlist[index].checked = false;
      }
    });
    this.getTemplateNameList();
  }

  getCaseFilterData() {
    this.blockUI.start();
    this.patientService
      .getapplicationdatacases(
        this.appState.selectedUserLocation.id,
        this.patientId
      )
      .subscribe((data) => {
        // for (let index = 0; index < data.length; index++) {
        //   data[index].caseStatusName = "";
        //   if (data[index].caseStatus == 0) {
        //     data[index].caseStatusName ='Open'
        //   }
        //   if (data[index].caseStatus == 1) {
        //     data[index].caseStatusName = "Re-opened"
        //   }
        //   if (data[index].caseStatus == 2) {
        //     data[index].caseStatusName = "ReOpen"
        //   }
        // }
        this.lstgeneralCase = [];
        for (let index = 0; index < data.length; index++) {
          // if (data[index].caseStatus == 0 || data[index].caseStatus == 2) {
          data[index].checked = false;
          this.lstgeneralCase.push(data[index]);
          // }
        }
        this.blockUI.stop();
      });
  }

  openeditoption(group) {
    setTimeout(() => {
      if (this.isGroupShown(group.i)) {
        this.shownGroup = null;
      } else {
        this.shownGroup = group.i;
      }
      this.IsShowfilter = false;
      this.IsShow = false;
    }, 100);
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  public getTemplateNameList() {
    this.pinnotes = false;
    this.showdraft = false;
    this.blockUI.start();
    this.patientService
      .getEligibleTreatmentNotes(
        this.appState.selectedUserLocation.id,
        this.patientId
      )
      .subscribe(async (data: PatientTreatmentNotesModel) => {
        this.patientTreatmentNotesModel = data;


        this.eligibleTreatment = this.patientTreatmentNotesModel.eligibleTreatment.filter(
          (x) => x.locationId === this.appState.selectedUserLocation.id
        );
        this.SecoundCopypractitonerLocationSpecialityModel = [];
        this.practitonerLocationSpecialityModel.map((pls) => {
          let tnList: TemplateNameList[] = [];
          const et = this.eligibleTreatment.filter(
            (d) => d.specialityId === pls.specialityId
          );
          if (et !== undefined && et.length > 0) {
            this.SecoundCopypractitonerLocationSpecialityModel.push(pls);
            et.forEach((element) => {
              const m = new TemplateNameList();
              m.treatmentId = element.id;
              m.treatmentName = element.treatmentName;
              tnList.push(m);
            });
            pls.templateNameList = tnList;
          }
        });
        // if (this.patitonDraftData.status == 0) {
        //   this.showdraft = true;
        //   this.lstdraftdata.push(this.patitonDraftData);
        // }

        console.log(data.patientTreatmentNote, 'data.patientTreatmentNote');

        // get 'Open' Status data
        data.patientTreatmentNote.forEach(element => {
          if (element.status == 0) {
            this.showdraft = true;
            this.lstdraftdata1.push(element);
          }
        });
        this.lstdraftdata = uniqueBy(this.lstdraftdata1, 'id');
        // console.log(this.lstdraftdata, "lstdraftdata");

        if (this.ifFromState() && this.lstdraftdata && this.lstdraftdata.length > 0) {
          const forAdd = this.routerState.data.forAdd;
          if (!forAdd) {
            const treatmentNoteId = this.routerState.data.treatmentNoteId;
            const find = this.lstdraftdata.find(x => x.id === treatmentNoteId);
            if (find) {
              this.onClickPatientTemplateName(find.id, find.treatmentName, 'draft');
              return;
            }
          }
        }

        let j = 1;
        data.patientTreatmentNote.forEach(async element => {
          if (element.status == 1 || element.status == 2 || element.status == 3) {
            if (element.isPinned == true) {
              for (let j = 0; j < this.lsrservices.length; j++) {
                for (let i = 0; i < element.service.length; i++) {
                  if (element.service[i] == this.lsrservices[j].value) {
                    element.service[i] = this.lsrservices[j].text;
                    break;
                  }
                }
              }
              this.pinnotes = true;
              element.i = j;
              this.lstpindata.push(element);
              console.log(this.lstpindata, 'this.lstpindata');
            }
          }
          j++;
        });
        if (data.patientTreatmentNote.length == 0) {
          this.ShowProgresslist = false;
          this.treatmentTitle = 'You haven\'t added a Finalized Treatment note.';
          this.treatmentDescription =
            'Please select a Template to enter treatment notes for this patient.';
        } else {
          let i = 1;
          let detailsArray = [];
          data.patientTreatmentNote.forEach(async (element: any) => {
            if (element.status == 1 || element.status == 2 || element.status == 3) {
              if (element.isPinned == false || element.isPinned == null) {
                for (let j = 0; j < this.lsrservices.length; j++) {
                  for (let i = 0; i < element.service.length; i++) {
                    if (element.service[i] == this.lsrservices[j].value) {
                      element.service[i] = this.lsrservices[j].text;
                      break;
                    }
                  }
                }
                // this.patientService
                //   .gettreatmentnotesdetailsById(this.patientId, element.id)
                //   .subscribe((dataByTreatemnetId: any) => {
                //     let stringdata: any = JSON.parse(dataByTreatemnetId.treatmentData);
                //     element.traetDatalist = stringdata
                //     this.ShowProgresslist = true;
                //   });
                // element.i = i;
                // this.ShowProgresslist = true;

                // this.lstprogressdata.push(element);
                detailsArray.push(element.id);
              }
            } else {
              this.ShowProgresslist = false;
              this.treatmentTitle = 'You haven\'t added a Finalized Treatment note.';
              this.treatmentDescription =
                'Please select a Template to enter treatment notes for this patient.';
            }
            i++;
          });
          let obj = {
            patientId: this.patientId,
            patientTreatmentNotesIds: detailsArray
          };
          this.patientService
            .gettreatmentnotesdetailsByArrayIds(obj)
            .subscribe((res: any) => {
              this.blockUI.stop();
              this.lstprogressdata = [];
              for (let index = 0; index < res.patientTreatmentNotesDetailsModel.length; index++) {
                for (let i = 0; i < data.patientTreatmentNote.length; i++) {
                  if (res.patientTreatmentNotesDetailsModel[index].id == data.patientTreatmentNote[i].id) {
                    data.patientTreatmentNote[i].i = i;
                    data.patientTreatmentNote[i].traetDatalist = JSON.parse(res.patientTreatmentNotesDetailsModel[index].treatmentData);
                    this.lstprogressdata.push(data.patientTreatmentNote[i]);
                  }
                }
              }
              this.ShowProgresslist = true;
              if (!this.ifFromState() || (this.routerState && this.routerState.data && this.routerState.data.forAdd)) {
                this.lstprogressdata = _.groupBy(this.lstprogressdata, 'casesName');
              } else if(this.ifFromState() && !this.routerState.data.forAdd) {
                const data = this.lstprogressdata.filter(x => x.id === this.routerState.data.treatmentNotesId);
                data.forEach(val => {
                  val['expand'] = true;
                })
                this.lstprogressdata = _.groupBy(data, 'casesName');
              }
              let finaldata = _.map(this.lstprogressdata, function(value, key) {
                return {
                  key: key,
                  value: value
                };
              });
              this.lstfinaldata = finaldata;
              console.log(this.lstfinaldata, 'lstfinaldata');

              this.staffService.getPractitioners().subscribe((practitionersData) => {
                this.practitionerList = practitionersData;
              });

              // return Status: Open & Re-Open Data
              this.lstfinaldata = finaldata.filter(data => data.value[0].caseStatus != 1);
              // this.lstfinaldata = _.filter(finaldata, function (data) {
              //   if (data.value[0].caseStatus != 1) {
              //     return data;
              //   }
              // });

              // Show, When Open & Re-Open Data Null
              if (this.lstfinaldata.length == 0) {
                this.ShowProgresslist = false;
                this.treatmentTitle = 'You haven\'t added a Finalized Treatment note.';
                this.treatmentDescription =
                  'Please select a Template to enter treatment notes for this patient.';
              }


              if (this.lstfinaldata.length != 0 || finaldata.length != 0) {
                this.showFilters = true;
                for (let index = 0; index < this.lstfinaldata.length; index++) {
                  for (let i = 0; i < this.lstfinaldata[index].value.length; i++) {
                    this.lstfinaldata[index].value[i].default = false;
                    this.lstfinaldata[index].value[i].dataIndex = index;
                    this.lstfinaldata[index].value[i].expand = false;
                    this.lstfinaldata[index].value[i].showService = this.lstfinaldata[index].value[i].service.join(' ,  ');
                    if (this.lstfinaldata[index].value[i].id == this.finalisedPatientNoteId) {
                      this.lstfinaldata[index].value[i].expand = true;
                    }
                    if (this.lstfinaldata[index].value[i].practitionerId == this.lstfinaldata[index].value[i].createdPractitionerId) {
                      this.lstfinaldata[index].value[i].createdPractitionerPractitionerName = this.lstfinaldata[index].value[i].practitionerName;
                    } else {
                      for (let index = 0; index < this.practitionerList.length; index++) {
                        if (this.practitionerList[index].id == this.lstfinaldata[index].value[i].createdPractitionerId) {
                          this.lstfinaldata[index].value[i].createdPractitionerPractitionerName = this.practitionerList[index].firstName;
                        }
                      }
                    }
                  }
                }
              } else {
                this.showFilters = false;
              }
              this.lastCopyFinaldata = finaldata;
            });
        }
        if (this.ifFromState() && this.SecoundCopypractitonerLocationSpecialityModel && this.SecoundCopypractitonerLocationSpecialityModel.length > 0) {
          const forAdd = this.routerState.data.forAdd;
          const service = this.routerState.data.service as Offerings;
          for (let practitonerLocationSpecialityModel of this.SecoundCopypractitonerLocationSpecialityModel) {
            const find = practitonerLocationSpecialityModel.templateNameList.find(x => x.treatmentId === service.defaultTreatmentNotesTemplateId);
            if (find && forAdd) {
              this.onClickTreatmentTemplateName(find.treatmentId, find.treatmentName);
              break;
            }
          }
        }
        this.blockUI.stop();
      });
  }

  openfilter() {
    this.IsShow = !this.IsShow;
    if (this.IsShow == true) {
      this.showFlag = true;
    } else {
      this.showFlag = false;
    }
    this.IsShowfilter = false;
  }

  casetemplate(casenamedata, from = null) {
    if (from != 'fromInit') {
      this.openfilter();
    }
    if (casenamedata == 'all') {
      this.SecoundCopypractitonerLocationSpecialityModel = this.CopypractitonerLocationSpecialityModel;
    } else {
      this.SecoundCopypractitonerLocationSpecialityModel = [];
      this.CopypractitonerLocationSpecialityModel.forEach(PractionerData => {
        if (PractionerData.specialityName == casenamedata) {
          this.practitonerLocationSpecialityModel = [];
          let tnList: TemplateNameList[] = [];
          const et = this.eligibleTreatment.filter(
            (d) => d.specialityId === PractionerData.specialityId
          );
          if (et !== undefined && et.length > 0) {
            et.forEach((element) => {
              const m = new TemplateNameList();
              m.treatmentId = element.id;
              m.treatmentName = element.treatmentName;
              tnList.push(m);
            });
            PractionerData.templateNameList = tnList;
            this.SecoundCopypractitonerLocationSpecialityModel.push(PractionerData);
          }
        }
      });
      // this.practitonerLocationSpecialityModel = this.CopypractitonerLocationSpecialityModel
    }
  }

  onClickTreatmentTemplateName(id: string, treatmentName: string) {
    if (!this.hasPermission(this.moduleName, ['C', 'E'])) {
      return;
    }
    if (id != '0') {
      this.treatmentNotesService
        .getTreatmentNotesTemplateById(id)
        .subscribe((data: any) => {
          this.treatmentDatabtID = data;
          // return;
          this.addItem = true;
          this.treatmentNoteForm.patchValue(data);
          if (this.ifFromState()) {
            this.treatmentNoteForm.get('treatmentNotesFor').setValue('1');
            const appt = this.routerState.data.appointment as BubbleAppointment;
            this.treatmentNoteForm.get('appointmentId').setValue(appt.appointmentEntryId);
            const service = this.routerState.data.service as Offerings;
            const data = {
              value: service.serviceId,
              text: service.code + ' - ' + service.name
            };
            this.treatmentNoteForm.get('service').setValue([data]);
            this.onSizeChange([data]);
            this.getselectionChange({value: '1'});
            this.treatmentNoteForm.get('casesId').setValue(this.routerState.data.caseId);
          }
          if (this.treatmentNoteForm.get('treatmentNotesFor').value == 1) {
            this.isShowAppointmet = true;
            this.isShowPerDate = false;
          } else if (this.treatmentNoteForm.get('treatmentNotesFor').value == 2) {
            this.isShowPerDate = true;
            this.isShowAppointmet = false;
          } else {
            this.isShowPerDate = false;
            this.isShowAppointmet = false;
          }
          this.treatmentNoteForm.get('treatmentName').setValue(data.templateName);
          this.treatmentNotes.treatmentData = [];
          let stringdata: any = JSON.parse(data.itemType);
          this.treatmentNotes.treatmentData = stringdata;

          this.blockUI.stop();
        });
      this.mytemplateshow = false;
      this.addNewPatienttemplateshow = true;
      this.templateNameTypeDisplay = 'Add Treatment Notes';
      this.templateNameDisplay = treatmentName;
      this.selectedTemplateId = id;
      this.showdeletebtn = false;
    }
  }

  onClickPatientTemplateName(id: any, treatmentName: string, type) {
    if (!this.hasPermission(this.moduleName, ['C', 'E'])) {
      return;
    }
    this.showflag = !this.showflag;
    // this.treatmentNotes.treatmentData = this.lstdraftdata[itemTypeIndex]
    this.blockUI.start();
    let itemTypeModel: ItemTypeModel;
    if (id != '0') {
      this.patientService
        .gettreatmentnotesdetailsById(this.patientId, id)
        .subscribe((data: any) => {
          this.blockUI.stop();
          let servicedatat: any = [];
          this.itemid = id;
          this.treatmentNoteForm.get('casesId').patchValue(data.casesId);
          this.treatmentNoteForm.get('treatmentName').patchValue(data.treatmentName);
          this.treatmentNoteForm.get('treatmentNotesFor').patchValue(data.treatmentNotesFor.toString());
          if (data.treatmentNotesFor == 1) {
            this.isShowAppointmet = true;
            this.treatmentNoteForm.get('appointmentId').patchValue(data.appointmentId);
            this.treatmentNoteForm.get('appointmentId').updateValueAndValidity();
            // this.selectedSizes = data.service
            data.service.forEach((pl) => {
              // let obj = {
              //   value: (this.lsrservices.find((l) => l.value === pl)).value
              // }
              servicedatat.push(
                (this.lsrservices.find((l) => l.value === pl))
              );
            });
            this.treatmentNoteForm.get('service').patchValue(servicedatat);
            this.isShowPerDate = false;
          } else if (data.treatmentNotesFor == 2) {
            this.isShowPerDate = true;
            this.treatmentNoteForm.get('entryOrAppointmentDate').patchValue(new Date(data.entryDatetime));
            this.isShowAppointmet = false;
          } else {
            this.isShowPerDate = false;
            this.isShowAppointmet = false;
          }
          if (type == 'duplicate') {
            this.duplicatetypetext = type;
            this.duplicateButton = true;
            this.addItem = true;
            // this.treatmentNoteForm
            //   .get("treatmentName")
            //   .patchValue("Copy");
          } else if (type == 'draft') {
            this.showdeletebtn = true;
            this.duplicateButton = false;
            this.addItem = false;
            this.treatmentNoteForm
              .get('treatmentName')
              .patchValue(treatmentName);
            this.treatmentNoteForm.get('appointmentId').patchValue(data.appointmentId);
          }
          this.treatmentNotes.treatmentData = JSON.parse(data.treatmentData);
          this.blockUI.stop();
        });
      this.mytemplateshow = false;
      this.addNewPatienttemplateshow = true;
      this.templateNameTypeDisplay = 'Edit Treatment Notes';
      this.templateNameDisplay = treatmentName;
      this.selectedTemplateId = id;
      this.showdeletebtn = true;
    } else {
      this.blockUI.stop();
      this.mytemplateshow = false;
      this.addItem = true;
      this.isShowPerDate = false;
      this.isShowAppointmet = false;
      this.showdeletebtn = false;
      this.templateNameTypeDisplay = 'Add Treatment Notes';
      this.templateNameDisplay = 'Standard Template';
      this.treatmentNoteForm
        .get('treatmentName')
        .patchValue('');
      this.treatmentNoteForm.reset();
      if (this.ifFromState() && this.routerState.data.forAdd) {
        this.treatmentNoteForm.get('treatmentNotesFor').setValue('1');
        const appt = this.routerState.data.appointment as BubbleAppointment;
        this.treatmentNoteForm.get('appointmentId').setValue(appt.appointmentEntryId);
        const service = this.routerState.data.service as Offerings;
        const data = {
          value: service.serviceId,
          text: service.code + ' - ' + service.name
        };
        this.treatmentNoteForm.get('service').setValue([data]);
        this.onSizeChange([data]);
        this.getselectionChange({value: '1'});
        this.treatmentNoteForm.get('casesId').setValue(this.routerState.data.caseId);
      }
      this.treatmentNotes.treatmentData = [];
      if (this.treatmentNotes.treatmentData.length === 0) {

        let self = this;
        const itemTypesId = this.itemTypes.find((x) => x.title === 'Text');
        this.defaultItemType(undefined, function(res) {

          itemTypeModel = res;
          itemTypeModel.itemTypeId = itemTypesId.id;
          itemTypeModel.itemTypeText = 'Text';
          itemTypeModel.itemLabel = 'Text';
          itemTypeModel.text = '';
          self.treatmentNotes.treatmentData.push(itemTypeModel);
        });

      }
      this.addNewPatienttemplateshow = true;
    }
  }

  cleartextbox() {
    this.treatmentNoteForm.get('treatmentName').patchValue('');
  }

  openItem() {
    setTimeout(() => {
      if (this.IsAddItemShow == false) {
        // if (this.isShowPerDate == true) {
        //   this.isShowPerDate = false;
        // }
        this.IsAddItemShow = true;
      } else {
        this.IsAddItemShow = false;
      }
    }, 100);

  }

  defaultItemType(itemTypeModel: ItemTypeModel[] = undefined, callback) {

    let model = new ItemTypeModel();

    let max = 0;
    if (itemTypeModel !== undefined) {
      itemTypeModel.forEach((x) => {
        if (x.id > max) {
          max = x.id;
        }
      });
      model.id = max + 1;
    } else {
      model.id = 1;
    }
    model.itemLabel = '';
    model.itemTypeText = 'Please select an item type';
    model.itemLabelDuplicate = '';
    model.itemTypeId = 0;
    model.helpText = '';
    model.isValid = true;
    model.helpTextDuplicate = '';
    model.showMirrored = false;
    model.isMandatory = false;
    model.dateTime = new DateTimeModel();

    return callback(model);
  }

  private clearItemType(itemTypeIndex: number, callback) {
    console.log(itemTypeIndex, 'itemTypeIndex');
    console.log(this.treatmentNotes, 'treatmentNotes');
    console.log(this.treatmentNotes.treatmentData[itemTypeIndex], 'this.treatmentNotes.treatmentData[itemTypeIndex]');
    if (this.treatmentNotes.treatmentData[itemTypeIndex]) {
      this.treatmentNotes.treatmentData[itemTypeIndex].itemLabel = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].itemLabelDuplicate = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].helpText = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].helpTextDuplicate = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].itemTypeId = 0;
      this.treatmentNotes.treatmentData[itemTypeIndex].itemTypeText = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].text = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].textDuplicate = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].chiefComplaint = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].instruction = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].instructionDuplicate = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].dropdowns = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].duplicateDropdowns = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].radioButtons = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].duplicateRadioButtons = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].checkBoxes = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].checkBoxesDuplicate = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].optionListStyleId = 0;
      this.treatmentNotes.treatmentData[itemTypeIndex].optionListStyleDuplicateId = 0;
      this.treatmentNotes.treatmentData[itemTypeIndex].optionListSubjects = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].optionListChoices = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].optionListChoicesDuplicate = [];
      this.treatmentNotes.treatmentData[itemTypeIndex].fileImage = '';
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].optionListStyleDuplicateId = 0;
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].optionListSubjectsDuplicate = [];
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].dateTime = new DateTimeModel();
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].dateTimeDuplicate = new DateTimeModel();
      this.treatmentNotes.treatmentData[itemTypeIndex].number = new NumberModel();
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].numberDuplicate = new NumberModel();
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].rangeOrScale = new NumberModel();
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].rangeOrScaleDuplicate = new NumberModel();
      this.treatmentNotes.treatmentData[itemTypeIndex].table = new TableModel();
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].tableDuplicate = new TableModel();
      this.treatmentNotes.treatmentData[itemTypeIndex].selectedDropdown = 0;
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].selectedDuplicateDropdown = 0;
      this.treatmentNotes.treatmentData[itemTypeIndex].setAsDefault = false;
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].setAsDuplicateDefault = false;
      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].chartImage = new ChartModel('../../../assets/dropfileimage.png');

      this.treatmentNotes.treatmentData[
        itemTypeIndex
        ].chartDuplicateImage = new ChartModel('../../../assets/dropfileimage.png');
      this.treatmentNotes.treatmentData[itemTypeIndex].vitals = new VitalsModel();
      this.treatmentNotes.treatmentData[itemTypeIndex].isOpen = true;
      this.treatmentNotes.treatmentData[itemTypeIndex].showMirrored = false;
      this.treatmentNotes.treatmentData[itemTypeIndex].isMandatory = false;
      this.treatmentNotes.treatmentData[itemTypeIndex].includeNotes = false;
      this.treatmentNotes.treatmentData[itemTypeIndex].isOpen = true;
      this.treatmentNotes.treatmentData[itemTypeIndex].inputnotes = '';
      this.treatmentNotes.treatmentData[itemTypeIndex].inputnotesDuplicate = '';

      console.log(this.treatmentNotes, 'treatmentNotes');
      console.log(this.treatmentNotes.treatmentData[itemTypeIndex], 'this.treatmentNotes.treatmentData[itemTypeIndex]');
      return callback(this.treatmentNotes.treatmentData[itemTypeIndex]);
    } else {

    }
  }

  showoptions(option) {
    console.log(option);
    this.isItemAdded = true;
    let itemTypeModel = new ItemTypeModel();
    var self = this;

    console.log(self.treatmentNotes.treatmentData.length);

    self.defaultItemType(self.treatmentNotes.treatmentData.length == 0 ? undefined : self.treatmentNotes.treatmentData, function(resType) {
      itemTypeModel = resType;

      if (self.treatmentNotes) {
        console.log(AppUtils.refrenceClone(self.treatmentNotes.treatmentData));
      }

      self.treatmentNotes.treatmentData.push(itemTypeModel);
      console.log(self.treatmentNotes, 'treatmentNotes');

      if (self.treatmentNotes) {
        console.log(AppUtils.refrenceClone(self.treatmentNotes.treatmentData));
      }

      let value = self.treatmentNotes.treatmentData.length;
      console.log(value, 'value');
      let itemTypeIndex = self.treatmentNotes.treatmentData.findIndex(
        (x) =>
          x.id == value
      );


      let newSelf = self;
      console.log(itemTypeIndex, 'itemTypeIndex');
      console.log(AppUtils.refrenceClone(self.treatmentNotes.treatmentData));
      self.clearItemType(itemTypeIndex, function(res) {

        console.log(res, 'res');
        console.log(AppUtils.refrenceClone(self.treatmentNotes.treatmentData));

        itemTypeModel = res;
        const itemTypesId = newSelf.itemTypes.find((x) => x.title === option.title);

        if (option.title == 'Heading') {
          itemTypeModel.itemTypeId = itemTypesId.id;
          itemTypeModel.itemTypeText = 'Heading';
          itemTypeModel.itemLabel = 'Heading';
        }
        if (option.title == 'Date Time') {
          itemTypeModel.itemTypeId = itemTypesId.id;
          itemTypeModel.itemTypeText = 'Date Time';
          itemTypeModel.itemLabel = 'Date Time';
          itemTypeModel.dateTime.date = new Date();
          itemTypeModel.dateTime.time = new Date();
        }
        if (option.title == 'Text') {
          itemTypeModel.itemTypeId = itemTypesId.id;
          itemTypeModel.itemTypeText = 'Text';
          itemTypeModel.itemLabel = 'Text';
        }
        if (option.title == 'Number') {
          itemTypeModel.itemTypeText = 'Number';
          itemTypeModel.itemLabel = 'Number';
          itemTypeModel.helpText = 'Enter values between 1 to 5';
          itemTypeModel.number.default = null;
          itemTypeModel.number.minimum = 0;
          itemTypeModel.number.maximum = 5;
          this.treatmentNoteForm.controls['number'].setValidators([Validators.min(this.min), Validators.max(this.max)]);
          newSelf.shownumber = true;
        }
        if (option.title == 'Dropdown') {
          newSelf.showdroupdown = true;
          itemTypeModel.itemTypeText = 'Dropdown';
          itemTypeModel.itemLabel = 'Dropdown';
          itemTypeModel.helpText = 'Select any of the dropdown values.';
          for (let i = 1; i <= 5; i++) {
            const m = new DropdownModel();
            m.id = i;
            m.value = '' + i;
            // if (i == 1) {
            //   m.default = true;
            // }
            // else {
            m.default = false;
            // }
            itemTypeModel.dropdowns.push(m);
          }
          itemTypeModel.selectedDropdown = '';
        }
        if (option.title == 'Radio Buttons') {
          newSelf.showradio = true;
          itemTypeModel.itemTypeText = 'Radio Buttons';
          itemTypeModel.itemLabel = 'Radio Button';
          itemTypeModel.helpText = 'Select any of the values.';
          for (let i = 1; i <= 5; i++) {
            const m = new RadioButtonModel();
            m.id = i;
            m.value = '' + i;
            // if (i == 1) {
            //   m.default = true;
            // }
            // else {
            m.default = false;
            // }
            itemTypeModel.radioButtons.push(m);
          }
          itemTypeModel.selectedDropdown = '';
        }
        if (option.title == 'Check Boxes') {
          newSelf.showcheckbox = true;
          itemTypeModel.itemTypeText = 'Check Boxes';
          itemTypeModel.itemLabel = 'Check box';
          itemTypeModel.helpText = 'Select one or more checkboxes';
          for (let i = 1; i <= 5; i++) {
            const m = new CheckBoxModel();
            m.id = i;
            m.value = '' + i;
            // if (i == 1) {
            //   m.default = true;
            // }
            // else {
            m.default = false;
            // }
            itemTypeModel.checkBoxes.push(m);
          }
          itemTypeModel.selectedDropdown = '';
        }
        if (option.title == 'Range or Scale') {
          newSelf.showrange = true;
          itemTypeModel.itemTypeText = 'Range or Scale';
          itemTypeModel.itemLabel = 'Range or Scale';
          itemTypeModel.helpText = 'Move the slider to select a value';
          itemTypeModel.helpTextDuplicate = 'Move the slider to select a value';
          itemTypeModel.rangeOrScale.minimum = 0;
          itemTypeModel.rangeOrScale.maximum = 5;
          itemTypeModel.rangeOrScale.default = 1;
        }
        if (option.title == 'Option List') {
          newSelf.showoptionlist = true;
          itemTypeModel.itemTypeText = 'Option List';
          itemTypeModel.itemLabel = 'Option List';
          itemTypeModel.helpText = 'Select a choice(s) from a list of subjects';
          itemTypeModel.optionListStyleId = 1;
          let optionvalues = 65;
          for (let i = 1; i <= 3; i++) {
            const m = new KeySubjectModel();
            m.id = i;
            m.value = String.fromCharCode(optionvalues);
            itemTypeModel.optionListSubjects.push(m);
            optionvalues++;
          }
          for (let i = 1; i <= 3; i++) {
            const m = new KeyValueModel();

            m.id = i;
            if (i == 1) {
              m.value = 'worse';
              m.selected = false;
            } else if (i == 2) {
              m.value = 'same';
              m.selected = false;
            } else {
              m.value = 'better';
              m.selected = false;
            }
            itemTypeModel.optionListChoices.push(m);
            optionvalues++;
            itemTypeModel.optionListSubjects.forEach((element) => {
              element.optionListChoices.push(m);
            });
          }
        }
        if (option.title == 'Table') {
          newSelf.showtable = true;
          itemTypeModel.itemTypeText = 'Table';
          itemTypeModel.itemLabel = 'Table';
          itemTypeModel.table.rows = 5;
          for (let i = 1; i <= 5; i++) {
            const m = new KeyValueModel();
            m.id = i;
            m.value = 'Row ' + i;
            itemTypeModel.table.rowHeader.push(m);
          }
          itemTypeModel.table.columns = 5;
          for (let i = 1; i <= 5; i++) {
            const m = new KeyValueModel();
            m.id = i;
            m.value = 'Column ' + i;
            itemTypeModel.table.columnHeader.push(
              m
            );
          }
          itemTypeModel.helpText = 'Enter values in the table cells';
        }
        if (option.title == 'Chart') {
          newSelf.showChart = true;
          itemTypeModel.itemTypeText = 'Chart';
          itemTypeModel.itemLabel = 'Body Chart';
          itemTypeModel.helpText = 'Draw or type notes on the provided Body Chart or any images of your choice';
        }
        if (option.title == 'File Attachment') {
          newSelf.showattachfile = true;
          itemTypeModel.itemTypeText = 'File Attachment';
          itemTypeModel.itemLabel = 'File Attachment';
          itemTypeModel.fileImage = '../../../assets/upload.png';
        }
        if (option.title == 'Instruction') {
          newSelf.showinstructionfile = true;
          itemTypeModel.itemTypeText = 'Instruction';
          itemTypeModel.itemLabel = 'Instruction';
        }
        if (option.title == 'Chief Complaint') {
          newSelf.showchieffile = true;
          itemTypeModel.itemTypeText = 'Chief Complaint';
          itemTypeModel.itemLabel = 'Chief Complaint';
        }
        if (option.title == 'Vitals') {
          newSelf.showmatric = true;
          newSelf.showvitalsarea = true;
          itemTypeModel.itemTypeText = 'Vitals';
          itemTypeModel.itemLabel = 'Vitals';
          itemTypeModel.vitals.vitalType = 'Metric';
          newSelf.unitOfMeasurement = newSelf.metric;
          newSelf.metric.forEach(element => {
            itemTypeModel.vitals.measurements.push(element);
          });
        }
        if (option.title == 'Spine') {
          newSelf.IsAddItemShow = false;
          newSelf.showSpineChart = true;
          itemTypeModel.itemTypeText = 'Spine';
          itemTypeModel.itemLabel = 'Spine';
          itemTypeModel.leftData = newSelf.SpineNodes;
          itemTypeModel.rightData = newSelf.RightSpineNodes;
          itemTypeModel.spineNotes = '';
        }
        if (option.title == 'Optical Measurements') {
          newSelf.showopticalarea = true;
          itemTypeModel.itemTypeText = 'Optical Measurements';
          itemTypeModel.itemLabel = 'Optical Measurements';
        }
        if (option == 'checkbox') {
          newSelf.IsAddItemShow = false;
          newSelf.buttonselection = false;
          newSelf.droupdownselection = false;
          newSelf.radioselection = false;
          newSelf.checkboxselection = true;
        }
        if (option == 'button') {
          newSelf.IsAddItemShow = false;
          newSelf.buttonselection = true;
          newSelf.droupdownselection = false;
          newSelf.radioselection = false;
          newSelf.checkboxselection = false;
        }
        if (option == 'radio') {
          newSelf.IsAddItemShow = false;
          newSelf.buttonselection = false;
          newSelf.droupdownselection = false;
          newSelf.radioselection = true;
          newSelf.checkboxselection = false;
        }
        if (option == 'droupdown') {
          newSelf.IsAddItemShow = false;
          newSelf.buttonselection = false;
          newSelf.droupdownselection = true;
          newSelf.radioselection = false;
          newSelf.checkboxselection = false;
        }
        newSelf.IsAddItemShow = false;
        newSelf.treatmentNotes.treatmentData = JSON.parse(
          JSON.stringify(newSelf.treatmentNotes.treatmentData)
        );
      });
    });

    console.log(AppUtils.refrenceClone(self.treatmentNotes.treatmentData));

  }

  public close() {
    this.openeditform = false;
    this.opendeleteform = false;
    this.opendeletenote = false;
    this.openexportnote = false;
    this.openexportnotepreview = false;
    this.openemailnoteemail = false;
    this.openfiltercase = false;
    this.openfilterpractitioner = false;
    this.openfilterdaterange = false;
    this.opencasestatusform = false;
    this.showchangedatetime = false;
    this.showchangepatient = false;
    this.showchangepatientcase = false;
    this.showchangepatientnote = false;
    this.showchangepatientdelete = false;
    this.openChiedform = false;
    this.openvitaleditform = false;
    this.showopticalarea = false;
    this.openheadingeditform = false;
    this.opendatetimeeditform = false;
    this.opennumbereditform = false;
    this.opendroupdwoneditform = false;
    this.opendischargeform = false;
    this.openradioeditform = false;
    this.opencheckboxeditform = false;
    this.openrangeeditform = false;
    this.openoptionlisteditform = false;
    this.openatatchfileditform = false;
    this.openchiefeditform = false;
    this.opendeleteformsconfirmation = false;
    this.closepopupData = false;
    this.lstselectcasedata = [];
    this.selectedRangeandAllCaseData = [];
    for (let i = 0; i < this.lstfinaldata.length; i++) {
      for (let index = 0; index < this.lstfinaldata[i].value.length; index++) {
        this.lstfinaldata[i].value[index].default = false;
      }
    }
  }

  closeform() {
    if (this.ifFromState()) {
      this.goBackToSate();
      return;
    }
    this.closepopupData = true;
    this.showflag = false;
  }

  editform(action) {
    if (action == 'draft') {
      this.drafttext = true;
      this.edittemplateshow = true;
      this.mytemplateshow = false;
      this.addNewtemplateshow = false;
    } else {
      this.treatmentNoteForm.controls.traetmentname.setValue('');
      this.copttext = true;
      this.edittemplateshow = true;
      this.mytemplateshow = false;
      this.addNewtemplateshow = false;
    }
  }

  deletenote() {
    this.opendeletenote = true;
  }

  savePatientdraft(type) {
    // for (let index = 0; index < this.treatmentNotes.treatmentData.length; index++) {
    //   if (this.treatmentNotes.treatmentData[index].dateTime) {
    //     this.treatmentNotes.treatmentData[index].dateTime.date = this.treatmentNoteForm.controls['selectDate'].value
    //     // this.treatmentNoteForm.controls['selectTime'].setValue(moment(this.treatmentNotes.treatmentData[index].dateTime.date).format("hh:mm"))
    //   }
    // }
    // this.finalisedPatientNoteId = "87fda3a5-e379-44a7-a3e2-99d19830c83c";
    // this.getTemplateNameList();
    // return;

    // get vitalsData
    let allTreatmentData = _.filter(this.treatmentNotes.treatmentData, function(data) {
      if (data.vitals) {
        return data;
      }
    });
    let vitalsData = _.filter(allTreatmentData, function(data) {
      if (data.isMandatory) {
        if (!data.vitals.measurements[0].value) {
          return data;
        } else if (!data.vitals.measurements[1].value) {
          return data;
        } else if (!data.vitals.measurements[2].value) {
          return data;
        } else if (!data.vitals.measurements[3].value) {
          return data;
        } else if (!data.vitals.measurements[4].value) {
          return data;
        } else if (!data.vitals.measurements[5].value) {
          return data;
        }
      }
    });

    if (vitalsData.length > 0) {
      let responseHandle = {
        message: 'Fill vitals measurement values',
        type: 'error'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    } else {
      if (this.treatmentNotes.treatmentData.length >= 1) {
        let patientTreatmentDetailsModel: PatientTreatmentDetailsModel = this.treatmentNoteForm.value;
        if (this.treatmentNoteForm.valid) {
          this.showprogressbar = true;
          this.showflag = false;
          patientTreatmentDetailsModel.treatmentData = JSON.stringify(this.treatmentNotes.treatmentData);
          if (type == 'savedraft') {
            patientTreatmentDetailsModel.status = 0;
          } else if (type == 'savefinalise') {
            patientTreatmentDetailsModel.status = 1;
          } else if (type == 'Closed_Discharge') {
            patientTreatmentDetailsModel.status = 2;
          } else if (type == 'ReOpen ') {
            patientTreatmentDetailsModel.status = 3;
          } else {
            patientTreatmentDetailsModel.status = 4;
          }
          this.savePatientTreatmentNotes(patientTreatmentDetailsModel);
        }
      } else {
        // this.displayErrorMessage('At least one item need to save record.');
        let responseHandle = {
          message: 'At least one item need to save record.',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      }
      return;
    }
  }

  savePatientTreatmentNotes(patientTreatmentDetailsModel: PatientTreatmentDetailsModel) {
    const el = document.getElementById('heading');
    patientTreatmentDetailsModel.patientId = this.patientId;
    patientTreatmentDetailsModel.locationId = this.appState.selectedUserLocation.id;
    patientTreatmentDetailsModel.practitionerId = '3fa85f64-5717-4562-b3fc-2c963f66afa6';
    patientTreatmentDetailsModel.treatmentNotesFor = Number(this.treatmentNoteForm.controls.treatmentNotesFor.value);
    patientTreatmentDetailsModel.isPinned = false;

    patientTreatmentDetailsModel.service = [];
    this.selectedSizes.forEach(element => {
      patientTreatmentDetailsModel.service.push(element.value);
    });
    patientTreatmentDetailsModel.entryDatetime = (this.value).toUTCString();
    // return;
    this.blockUI.start();
    this.patitonDraftData = patientTreatmentDetailsModel;
    if (this.addItem) {
      this.patientService
        .createPatientTreatmentNotes(patientTreatmentDetailsModel)
        .subscribe(
          (data) => {
            if (this.ifFromState()) {
              this.goBackToSate();
              return;
            }
            this.finalisedPatientNoteId = data;
            this.submitting = false;
            this.addNewPatienttemplateshow = false;
            this.mytemplateshow = true;
            this.lstcasefilterdata = [];
            this.addItem = false;
            this.lstpindata = [];
            this.lstdraftdata = [];
            this.lstprogressdata = [];
            this.lstfinaldata = [];
            this.getTemplateNameList();
            if (this.duplicatetypetext == 'duplicate') {
              this.duplicatetypetext = '';
              let responseHandle = {
                message: 'Treatment Note is duplicated successfully.',
                type: 'success'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displaySuccessMessage("Treatment Note is duplicated successfully.");
            } else if (patientTreatmentDetailsModel.status == 0) {
              let responseHandle = {
                message: 'Treatment Note is saved as draft successfully',
                type: 'success'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displaySuccessMessage("Treatment Note is saved as draft successfully");

            } else {
              let responseHandle = {
                message: 'Treatment Note is finalised successfully.',
                type: 'success'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displaySuccessMessage("Treatment Note is finalised successfully.");
            }

            el.scrollIntoView();
            this.blockUI.stop();
            this.treatmentNoteForm.reset();
            // setInterval(
            //   (a) => {
            //     this.displaySuccessMessage('');
            //   },
            //   10000,
            //   []
            // );
          },
          () => {
            if (patientTreatmentDetailsModel.status == 0) {
              let responseHandle = {
                message: 'Failed to save Treatment Notes as Draft, please try again later',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displayErrorMessage(
              //   "Failed to save Treatment Notes as Draft, please try again later"
              // );
            } else {
              let responseHandle = {
                message: 'Failed to save Treatment Notes as Final, please try again later',
                type: 'error'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displayErrorMessage(
              //   "Failed to save Treatment Notes as Final, please try again later"
              // );
            }

            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
            // setInterval(
            //   (a) => {
            //     this.displayErrorMessage('');
            //   },
            //   10000,
            //   []
            // );
          }
        );
    } else {
      patientTreatmentDetailsModel.id = this.itemid;
      patientTreatmentDetailsModel.patientId = this.patientId;
      this.patientService
        .updatePatientTreatmentNotes(patientTreatmentDetailsModel)
        .subscribe(
          (res) => {
            this.finalisedPatientNoteId = res;
            this.submitting = false;
            this.mytemplateshow = true;
            this.addNewtemplateshow = false;
            this.lstcasefilterdata = [];
            this.lstpindata = [];
            this.lstdraftdata = [];
            this.lstprogressdata = [];
            this.lstfinaldata = [];
            if (patientTreatmentDetailsModel.status = 0) {
              let responseHandle = {
                message: 'Draft Treatment Note is updated successfully.',
                type: 'success'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displaySuccessMessage("Draft Treatment Note is updated successfully");

            } else {
              let responseHandle = {
                message: 'Treatment Note is finalised successfully.',
                type: 'success'
              };
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              // this.displaySuccessMessage("Treatment Note is finalised successfully.");
            }
            this.getTemplateNameList();
            this.addItem = false;
            let responseHandle = {
              message: 'Patient Treatment Notes Template updated successfully.',
              type: 'success'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            // this.displaySuccessMessage("Patient Treatment Notes Template updated successfully.");
            el.scrollIntoView();
            this.blockUI.stop();
            this.treatmentNoteForm.reset();
            // setInterval(
            //   (a) => {
            //     this.displaySuccessMessage('');
            //   },
            //   10000,
            //   []
            // );
          },
          () => {
            // this.displayErrorMessage(
            //   "Failed to edit, please try again later"
            // );
            let responseHandle = {
              message: 'Failed to edit, please try again later',
              type: 'error'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            this.submitting = false;
            el.scrollIntoView();
            this.blockUI.stop();
            // setInterval(
            //   (a) => {
            //     this.displayErrorMessage('');
            //   },
            //   10000,
            //   []
            // );
          }
        );
    }
  }

  finalizePatientAddNote(type) {
    this.savePatientdraft(type);
  }

  deleteTemplate(type) {
    this.deletetype = type;
    this.opendeleteformsconfirmation = true;

  }

  deleteitem() {
    let templateIDs;
    if (this.deletetype == 'deletetemplated' || this.deletetype == 'duplicatetemplate') {
      templateIDs = this.selectedTemplateId;
    } else {
      templateIDs = this.openpopupDetails.id;
    }
    this.blockUI.start();
    this.patientService
      .deletepatienttreatmentnotesbyid(this.patientId, templateIDs)
      .subscribe(
        () => {
          this.opendeleteformsconfirmation = false;
          this.addNewtemplateshow = false;
          this.mytemplateshow = true;
          this.lstpindata = [];
          this.lstdraftdata = [];
          this.lstprogressdata = [];
          this.lstfinaldata = [];

          this.getTemplateNameList();
          if (this.deletetype == 'deletetemplated') {
            let responseHandle = {
              message: 'Draft Treatment Note is deleted successfully.',
              type: 'success'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            // this.displaySuccessMessage("Draft Treatment Note is deleted successfully.")
            // setInterval(
            //   (a) => {
            //     this.displaySuccessMessage('');
            //   },
            //   10000,
            //   []
            // );
          } else if (this.deletetype == 'duplicatetemplate') {
            let responseHandle = {
              message: 'Finalised Note is deleted successfully.',
              type: 'success'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            // this.displaySuccessMessage("Finalised Note is deleted successfully.")
            // setInterval(
            //   (a) => {
            //     this.displaySuccessMessage('');
            //   },
            //   10000,
            //   []
            // );
          } else {
            let responseHandle = {
              message: 'Finalised Note is deleted successfully.',
              type: 'success'
            };
            this.patientService.publishSomeData(JSON.stringify(responseHandle));
            // this.displaySuccessMessage("Finalised Note is deleted successfully.")
            // setInterval(
            //   (a) => {
            //     this.displaySuccessMessage('');
            //   },
            //   10000,
            //   []
            // );
          }

        },
        () => {
          this.opendeleteformsconfirmation = false;
          let responseHandle = {
            message: 'Failed to delete, please try again later',
            type: 'error'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          // this.displayErrorMessage(
          //   "Failed to delete, please try again later"
          // );
          // setInterval(
          //   (a) => {
          //     this.displayErrorMessage('');
          //   },
          //   10000,
          //   []
          // );
        }
      );
    this.blockUI.stop();
  }

  itemMandatoryCheck() {
    throw new Error('Method not implemented.');
  }

  patientMedicalDetails() {
    this.patientService
      .getPatientById(this.patientId)
      .subscribe((data: PatientModel) => {
        if (data.allergy != '' || data.medicalCondition != '' || data.medication != '') {
          this.showalerticon = true;
          this.medicalCondition = data.medicalCondition;
          this.allergy = data.allergy;
          this.medication = data.medication;
        } else {
          this.showalerticon = false;
        }
      });
  }

  dischargeoropen(type, data) {
    // if (this.pendingcase == "pending") {
    //   this.opendischargeform = true;
    // } else {
    this.dischargedescText = '';
    this.dischargeCaseData = data;
    this.opencasestatusform = true;
    if (type == 'discharge') {
      this.casestatustitle = 'DO YOU WANT TO DISCHARGE THIS CASE?';
      this.dischargebutton = true;
      this.reopenbutton = false;
    } else if (type == 'reopen') {
      this.reopenbutton = true;
      this.dischargebutton = false;
      this.casestatustitle = 'DO YOU WISH TO RE-OPEN THIS CASE?';
    }
    // }
  }

  changecasestatus(type) {
    const el = document.getElementById('heading');
    for (let i = 0; i < this.dischargeCaseData.value.length; i++) {
      if (((this.dischargeCaseData.value[i].status == 1) || (this.dischargeCaseData.value[i].status == 2))) {
        if (type == 'discharge') {
          this.patientService
            .gettreatmentnotesdetailsById(this.patientId, this.dischargeCaseData.value[i].id)
            .subscribe((dataByTreatemnetId: any) => {
              let array = [
                {
                  'DischargeText': 'Discharged',
                  'Dischargedesc': this.dischargedescText
                }
              ];

              let patientTreatmentDetailsModel: PatientTreatmentDetailsModel = dataByTreatemnetId;
              patientTreatmentDetailsModel.locationId = this.appState.selectedUserLocation.id;
              patientTreatmentDetailsModel.status = 2;
              patientTreatmentDetailsModel.treatmentData = JSON.stringify(array);
              patientTreatmentDetailsModel.IsDischarge = true;
              this.patientService
                .createPatientTreatmentNotes(patientTreatmentDetailsModel)
                .subscribe(
                  () => {
                    this.submitting = false;
                    this.addItem = false;
                    this.lstpindata = [];
                    this.lstdraftdata = [];
                    this.lstprogressdata = [];
                    this.lstfinaldata = [];
                    this.getCaseFilterData();

                    this.getTemplateNameList();
                    let responseHandle = {
                      message: 'Case discharged successfully.',
                      type: 'success'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    // this.displaySuccessMessage("Case discharged successfully.");
                    el.scrollIntoView();
                    this.blockUI.stop();
                    this.treatmentNoteForm.reset();
                    // setInterval(
                    //   (a) => {
                    //     this.displaySuccessMessage('');
                    //   },
                    //   10000,
                    //   []
                    // );
                  },
                  () => {
                    let responseHandle = {
                      message: 'Failed to discharge case, please try again later',
                      type: 'error'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    // this.displayErrorMessage(
                    //   "Failed to discharge case, please try again later"
                    // );
                    this.submitting = false;
                    el.scrollIntoView();
                    this.blockUI.stop();
                    // setInterval(
                    //   (a) => {
                    //     this.displayErrorMessage('');
                    //   },
                    //   10000,
                    //   []
                    // );
                  }
                );
              this.opencasestatusform = false;
            });
          break;
        } else {
          this.patientService
            .gettreatmentnotesdetailsById(this.patientId, this.dischargeCaseData.value[i].id)
            .subscribe((dataByTreatemnetId: any) => {
              let array = [
                {
                  'DischargeText': 'Re - Opened',
                  'Dischargedesc': this.dischargedescText
                }
              ];
              let patientTreatmentDetailsModel: PatientTreatmentDetailsModel = dataByTreatemnetId;
              patientTreatmentDetailsModel.locationId = this.appState.selectedUserLocation.id;
              patientTreatmentDetailsModel.status = 3;
              patientTreatmentDetailsModel.treatmentData = JSON.stringify(array);
              patientTreatmentDetailsModel.IsReopen = true;
              this.patientService
                .createPatientTreatmentNotes(patientTreatmentDetailsModel)
                .subscribe(
                  () => {
                    this.submitting = false;
                    this.addItem = false;
                    this.lstpindata = [];
                    this.lstdraftdata = [];
                    this.lstprogressdata = [];
                    this.lstfinaldata = [];
                    this.getCaseFilterData();
                    this.getTemplateNameList();
                    let responseHandle = {
                      message: ' Case re-opened successfully.',
                      type: 'success'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    // this.displaySuccessMessage(" Case re-opened successfully.");
                    el.scrollIntoView();
                    this.blockUI.stop();
                    this.treatmentNoteForm.reset();
                    // setInterval(
                    //   (a) => {
                    //     this.displaySuccessMessage('');
                    //   },
                    //   10000,
                    //   []
                    // );
                  },
                  () => {
                    let responseHandle = {
                      message: 'Failed to re-open case, please try again later',
                      type: 'error'
                    };
                    this.patientService.publishSomeData(JSON.stringify(responseHandle));
                    // this.displayErrorMessage(
                    //   "Failed to re-open case, please try again later"
                    // );
                    this.submitting = false;
                    el.scrollIntoView();
                    this.blockUI.stop();
                    // setInterval(
                    //   (a) => {
                    //     this.displayErrorMessage('');
                    //   },
                    //   10000,
                    //   []
                    // );
                  }
                );
              this.opencasestatusform = false;
            });
          break;
        }
      }
    }
  }

  // Combine Function Start
  openpopup(action, details) {
    this.openpopupDetails = details;
    this.IsShow = false;
    setTimeout(() => {
      if (action == 'medicalalert') {
        if (this.ShowMedicalDetails == false) {
          this.ShowMedicalDetails = true;
          this.patientMedicalDetails();
        } else {
          this.ShowMedicalDetails = false;
        }
      }
      if (action == 'exportpatientNote') {
        this.blockUI.start();
        this.dataLoaded = true;
        this.shownGroup = null;
        this.openexportnote = true;
        this.isLoading = false;
        this.blockUI.stop();
      }

      if (action == 'printnotes') {
        this.openexportnote = false;
        this.openexportnotepreview = false;
        alert('PrintFunction');
      }
      if (action == 'emailNotes') {
        this.openexportnotepreview = false;
        this.openemailnoteemail = true;
      }
      if (action == 'sendmail') {
        this.openemailnoteemail = false;
        alert('SendMail');
      }
      if (action == 'case') {
        this.shownGroup = null;
        this.IsShowfilter = false;
        this.openfiltercase = true;
      }
      if (action == 'delete') {
        this.shownGroup = null;
        this.showpinEditOptions = false;
        this.showEditOptions = false;
        this.opendeleteformsconfirmation = true;
      }
      if (action == 'practitioner') {
        this.shownGroup = null;
        this.IsShowfilter = false;
        this.openfilterpractitioner = true;
      }
      if (action == 'date range') {
        this.shownGroup = null;
        this.IsShowfilter = false;
        this.openfilterdaterange = true;
      }
      if (action == 'openeditoptions') {
        this.shownGroup = null;
        if (this.showpinEditOptions == false) {
          this.showpinEditOptions = true;
        } else {
          this.showpinEditOptions = false;
        }
      }
      if (action == 'change date / time') {
        this.shownGroup = null;
        this.showpinEditOptions = false;
        this.showEditOptions = false;
        this.showchangedatetime = true;
      }
      if (action == 'change patient') {
        this.shownGroup = null;
        this.showpinEditOptions = false;
        this.showEditOptions = false;
        this.showchangepatient = true;
      }
      if (action == 'change case') {
        this.shownGroup = null;
        this.showpinEditOptions = false;
        this.showEditOptions = false;
        this.showchangepatientcase = true;
      }
      if (action == 'change notes name') {
        this.shownGroup = null;
        this.showpinEditOptions = false;
        this.showEditOptions = false;
        this.changeselectNotes = details.treatmentName;
        this.showchangepatientnote = true;
      }
      if (action == 'openeditoptionsa') {
        if (this.showEditOptions == false) {
          this.showEditOptions = true;
        } else {
          this.showEditOptions = false;
        }
      }
      if (action == 'expanddetails') {
        this.showexpand = false;
        this.showcolapse = true;
        for (let index = 0; index < this.lstfinaldata.length; index++) {
          for (let i = 0; i < this.lstfinaldata[index].value.length; i++) {
            this.lstfinaldata[index].value[i].expand = true;
          }
        }
        // if (this.expandclass == false) {
        //   this.expandclass = true;
        //   this.showexpand = false;
        //   this.showcolapse = true;
        // } else {
        //   this.expandclass = false;
        //   this.showexpand = true;
        //   this.showcolapse = false;
        // }
      }
      if (action == 'collapsedetails') {
        for (let index = 0; index < this.lstfinaldata.length; index++) {
          for (let i = 0; i < this.lstfinaldata[index].value.length; i++) {
            this.lstfinaldata[index].value[i].expand = false;
          }
        }
        this.showexpand = true;
        this.showcolapse = false;
      }
      if (action == 'openfilterprogress') {
        // if (this.IsShowfilter == false) {
        //   this.IsShowfilter = true;
        // } else {
        //   this.IsShowfilter = false;
        // }
        this.IsShowfilter = !this.IsShowfilter;
        if (this.IsShowfilter == true) {
          this.showmainFilterFlag = true;
        } else {
          this.showmainFilterFlag = false;
        }
        this.IsShow = false;
      }
      if (action == 'redpintnote') {
        alert('RemoveData');
      }
    }, 500);

  }

  getvitals(event) {
    if (event.srcElement.innerText == 'Imperial') {
      this.showeImperial = true;
      this.storevalue = 'true';
    }
    if (event.srcElement.innerText == 'Metric') {
      this.showeImperial = false;
      this.storevalue = 'false';
    }
  }

  closepopupd() {
    this.IsShowfilter = false;
    this.shownGroup = null;
    this.IsAddItemShow = false;
    this.IsShow = false;
  }

  closepopupedit(type) {
    this.IsShowfilter = false;
    this.shownGroup = null;
    this.IsAddItemShow = false;
    this.IsShow = false;
  }

  // Progress Note Start
  pintnote(patientdata) {
    // return;
    this.patientService
      .gettreatmentnotesdetailsById(this.patientId, patientdata.id)
      .subscribe((data: any) => {
        this.treatmentDatabtID = data;
        let patientTreatmentNotesDetailsFinalizeModel: PatientTreatmentNotesDetailsFinalizeModel = patientdata;
        patientTreatmentNotesDetailsFinalizeModel.locationId = this.appState.selectedUserLocation.id;
        patientTreatmentNotesDetailsFinalizeModel.id = patientdata.id;
        patientTreatmentNotesDetailsFinalizeModel.patientId = this.patientId;
        patientTreatmentNotesDetailsFinalizeModel.isPinned = true;
        this.patientService
          .modifypatienttreatmentnotesOnFinalize(patientTreatmentNotesDetailsFinalizeModel)
          .subscribe((data: any) => {
            // this.treatmentDatabtID = data
            this.lstpindata = [];
            this.lstdraftdata = [];
            this.lstprogressdata = [];
            this.lstfinaldata = [];

            this.getTemplateNameList();
          });
      });
  }

  unpinProgressNote(patientdata) {
    this.patientService
      .gettreatmentnotesdetailsById(this.patientId, patientdata.id)
      .subscribe((data: any) => {
        this.treatmentDatabtID = data;
        let patientTreatmentNotesDetailsFinalizeModel: PatientTreatmentNotesDetailsFinalizeModel = patientdata;
        patientTreatmentNotesDetailsFinalizeModel.locationId = this.appState.selectedUserLocation.id;
        patientTreatmentNotesDetailsFinalizeModel.id = patientdata.id;
        patientTreatmentNotesDetailsFinalizeModel.patientId = this.patientId;
        patientTreatmentNotesDetailsFinalizeModel.isPinned = false;
        this.patientService
          .modifypatienttreatmentnotesOnFinalize(patientTreatmentNotesDetailsFinalizeModel)
          .subscribe((data: any) => {
            this.lstpindata = [];
            this.lstdraftdata = [];
            this.lstprogressdata = [];
            this.lstfinaldata = [];

            this.getTemplateNameList();
          });
      });
  }

  expandform() {
    if (this.showprogressbar == true) {
      this.showprogressbar = false;
    } else {
      this.showprogressbar = true;
    }
  }

  getdata(details) {
    this.lstselectcasedata = [];
    this.selectedRangeandAllCaseData = [];
    for (let i = 0; i < this.lstfinaldata.length; i++) {
      for (let index = 0; index < this.lstfinaldata[i].value.length; index++) {
        if (this.lstfinaldata[i].value[index].id == details.id) {
          this.lstfinaldata[i].value[index].default = !this.lstfinaldata[i].value[index].default;
        }
        if (this.lstfinaldata[i].value[index].default == true) {
          this.lstselectcasedata.push(this.lstfinaldata[i].value[index]);
        }
      }
    }
    this.selectedRangeandAllCaseData = this.lstselectcasedata;
  }

  selectRangeofPreview() {
    let arr = [];
    this.selectedRangeandAllCaseData = [];
    if (this.lstselectcasedata.length > 1) {
      var SelectedCaseName = this.lstselectcasedata[this.lstselectcasedata.length - 1].dataIndex;
      for (let j = 0; j < this.lstfinaldata.length; j++) {
        for (let k = 0; k < this.lstfinaldata[j].value.length; k++) {
          if (this.lstselectcasedata[this.lstselectcasedata.length - 2].i < this.lstselectcasedata[this.lstselectcasedata.length - 1].i) {
            for (let i = this.lstselectcasedata[this.lstselectcasedata.length - 2].i; i < this.lstselectcasedata[this.lstselectcasedata.length - 1].i + 1; i++) {
              if (i == this.lstfinaldata[j].value[k].i) {
                if (this.lstfinaldata[j].value[k].dataIndex == SelectedCaseName) {
                  this.lstfinaldata[j].value[k].default = true;
                  arr.push(this.lstfinaldata[j].value[k]);
                }
              }
            }
          }
        }
      }
      this.selectedRangeandAllCaseData = [...new Set([...this.lstselectcasedata, ...arr])];
    }
  }

  opepreviewsection(type, name, from) {
    if (from == 'fromGeneralCase') {
      this.exportWindowShow = false;
    }
    this.shownGroup = null;
    this.blockUI.start();
    if (name == 'exportNoteBytreatment') {
      this.priviewcontent(type);
    } else {
      this.priviewcontent(this.selectedRangeandAllCaseData);
    }
    this.blockUI.stop();
  }

  priviewcontent(treatmenttemplateID) {
    this.lstpreview = [];
    this.blockUI.start();
    if (treatmenttemplateID.length == 0) {
      this.blockUI.stop();
      // this.displayErrorMessage("Please Select Note!")
      let responseHandle = {
        message: 'Please Select Note!',
        type: 'error'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    }
    for (let index = 0; index < treatmenttemplateID.length; index++) {
      this.patientService
        .GetPatientMinInfoById(this.patientId)
        .subscribe((data: PatientModel) => {
          this.patientService
            .GetMinInfoBusinessLocationById(this.appState.selectedUserLocation.id)
            .subscribe((datab: PatientModel) => {
              this.patientService
                .gettreatmentnotesdetailsById(this.patientId, treatmenttemplateID[index].id)
                .subscribe((datatre: any) => {
                  this.blockUI.stop();
                  let caseNamedata;
                  this.lstgeneralCase.forEach(element => {
                    if (treatmenttemplateID[index].casesId == element.id) {
                      caseNamedata = element.caseName;
                    }
                  });
                  let stringdata: any = JSON.parse(datatre.treatmentData);
                  var obj = {
                    patientDetails: data,
                    BusinessDetails: datab,
                    TreatmentDetails: datatre,
                    SelectesTreatment: treatmenttemplateID[index],
                    casename: caseNamedata,
                    treatmentdata: stringdata,
                  };
                  this.lstpreview.push(obj);
                  this.openexportnotepreview = true;
                });
            });
        });
    }

  }

  printProgressNote() {
    const printContent = document.getElementById('printContent').innerHTML;
    const WindowPrt = window.open('', '', 'left=0,top=0,width=1000,height=900,toolbar=0,scrollbars=0,status=0');
    WindowPrt.document.write(`<html><head>
    <style>

  .ppnote-wrapper-box-inner {
      margin: 10px 20px;
  }

  .ppnote-wrapper-box {
      border: 1px solid #ABAAB0;
      border-radius: 4px;
      display: flex;
      width: 100%;
  }

  .ppnote {
      padding: 10px 10px 0;
  }

  .c-heading-name-ppnote {
      min-width: 80px;
      padding-bottom: 20px;
      font-weight: 500;
  }

  .c-heading-name-ppnote1 {
      min-width: 120px;
      padding-bottom: 20px;
      font-weight: 500;
  }

  .ppnote-content-heading {
      display: flex;
      justify-content: space-between;
      padding: 10px 20px;
      border-bottom: 0.5px solid #979797;
  }

  .ppnote-content-heading1 {
      display: flex;
      justify-content: space-between;
      padding: 10px 20px;
  }

  .ppnote-content-heading-sub-left {
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 22px;
      letter-spacing: 0.15px;
      color: #1C1B1B;
  }

  .ppnote-content-heading-left {
      font-style: normal;
      font-weight: normal;
      font-size: 20px;
      line-height: 22px;
      letter-spacing: 0.15px;
      color: #1C1B1B;
  }

  .ppnote-content-heading-right {}

  .ppnote-content {
      border: 1px solid #979797;
      margin: 20px 30px 20px 30px;
      page-break-after: unset !important;
  }
  .ppnote-content-page-break{
    border: 1px solid #979797;
    margin: 20px 30px 20px 30px;
    page-break-after: always !important;
  }
  .modalClosebtn {
      height: unset !important;
  }

  .alert-wrapper {
      position: relative;
  }
   .d-flex {
    display: -webkit-box!important;
    display: flex!important;
}
@media (min-width: 768px)
.col-md-6 {
    -webkit-box-flex: 0;
    flex: 0 0 50%;
    max-width: 50%;
}
.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
  position: relative;
  width: 300px;
  padding-right: 15px;
  padding-left: 15px;
}
.cc-head{
  text-align: right;
}
  }
</style>
   </head><body>` + printContent + `</body></html>`);
    //  WindowPrt.document.write(printContent);
    WindowPrt.document.close();
    WindowPrt.focus();
    WindowPrt.print();
    WindowPrt.close();
  }

  ChangeDatetime(type) {
    let patientTreatmentNotesDetailsFinalizeModel: PatientTreatmentNotesDetailsFinalizeModel = new PatientTreatmentNotesDetailsFinalizeModel();
    patientTreatmentNotesDetailsFinalizeModel.locationId = this.appState.selectedUserLocation.id;
    patientTreatmentNotesDetailsFinalizeModel.id = this.openpopupDetails.id;
    patientTreatmentNotesDetailsFinalizeModel.patientId = this.patientId;
    if (type == 'datetime') {
      if (this.selected == 'currentdate') {
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = new Date().toISOString();
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = null;
        patientTreatmentNotesDetailsFinalizeModel.TreatmentNotesFor = 2;
      } else if (this.selected == 'customdate') {
        this.changeselectdate = new Date(this.changeselectdate);
        this.changeselectdate.setDate(this.changeselectdate.getDate() + 1);
        const time = moment(new Date(this.changeselecttime)).format('hh:mm a');
        const t1: any = time.split(' ');
        const t2: any = t1[0].split(':');
        t2[0] = (t1[1] === 'PM' ? (1 * t2[0] + 12) : t2[0]);
        const time24 = t2[0] + ':' + t2[1] + ':00';
        const completeDate = (this.changeselectdate).toString().replace('00:00:00', time24.toString());
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = new Date(completeDate).toISOString();
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = null;
        patientTreatmentNotesDetailsFinalizeModel.TreatmentNotesFor = 2;
      } else {
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = null;
        patientTreatmentNotesDetailsFinalizeModel.TreatmentNotesFor = 1;
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = this.changeselectappointment;
      }
    } else if (type == 'changecase') {
      patientTreatmentNotesDetailsFinalizeModel.casesId = this.changeselectCase;
    } else if (type == 'patientdata') {
      if (this.selected == 'currentdate') {
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = new Date().toISOString();
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = null;
      } else if (this.selected == 'customdate') {
        let combineDate = this.changeselectdate + '' + this.changeselecttime;
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = new Date().toISOString();
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = null;
      } else {
        patientTreatmentNotesDetailsFinalizeModel.entryDatetime = null;
        patientTreatmentNotesDetailsFinalizeModel.appointmentId = this.changeselectappointment;
      }
      patientTreatmentNotesDetailsFinalizeModel.newPatientId = this.changeselectPatientId;
      patientTreatmentNotesDetailsFinalizeModel.casesId = this.changeselectCase;
    } else if (type == 'ChangePatientNote') {
      patientTreatmentNotesDetailsFinalizeModel.treatmentName = this.changeselectNotes;
    } else {
      console.log('False');
    }
    this.blockUI.start();
    this.patientService
      .modifypatienttreatmentnotesOnFinalize(patientTreatmentNotesDetailsFinalizeModel)
      .subscribe((data: any) => {
        this.treatmentDatabtID = data;
        if (type == 'datetime') {
          let responseHandle = {
            message: 'Finalised Note is updated successfully.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          // this.displaySuccessMessage("Finalised Note is updated successfully.")
          // setInterval(
          //   (a) => {
          //     this.displaySuccessMessage('');
          //   },
          //   10000,
          //   []
          // );
        } else if (type == 'changecase') {
          let responseHandle = {
            message: 'Finalised Note is updated successfully.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          // this.displaySuccessMessage("Finalised Note is updated successfully.")
          // setInterval(
          //   (a) => {
          //     this.displaySuccessMessage('');
          //   },
          //   10000,
          //   []
          // );
        } else if (type == 'patientdata') {
          let responseHandle = {
            message: 'Finalised Note is mapped to correct patient successfully.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          // this.displaySuccessMessage("Finalised Note is mapped to correct patient successfully.")
          // setInterval(
          //   (a) => {
          //     this.displaySuccessMessage('');
          //   },
          //   10000,
          //   []
          // );
        } else if (type == 'ChangePatientNote') {
          let responseHandle = {
            message: 'Finalised Note is updated successfully.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          // this.displaySuccessMessage("Finalised Note is updated successfully.")
          // setInterval(
          //   (a) => {
          //     this.displaySuccessMessage('');
          //   },
          //   10000,
          //   []
          // );
        }
        this.lstpindata = [];
        this.lstdraftdata = [];
        this.lstprogressdata = [];
        this.lstfinaldata = [];
        this.getTemplateNameList();
        this.blockUI.stop();
        this.showchangedatetime = false;
        this.showchangepatientcase = false;
        this.showchangepatient = false;
        this.showchangepatientnote = false;
      });
  }

  getPatientList() {
    // this.patientService.getAllPatients().subscribe((data) => {
    //   if (data.length != 0) {
    //     data.forEach(element => {
    //       if (element.gender == 'Male') {
    //         element.gender = 'M';
    //       }
    //       else {
    //         element.gender = 'F';
    //       }
    //       element.patientSearchName = element.firstName + ' ' + element.middleName + ' ' + element.lastName
    //       this.patientlistData.push(element);
    //     });

    //   }
    // });
    this.patientService.getAllPatientsByLocationId(this.appState.selectedUserLocationId).subscribe((data) => {
      if (data.length != 0) {
        data.forEach((element: any) => {
          if (element.gender == 'Male') {
            element.gender = 'M';
          } else {
            element.gender = 'F';
          }
          element.patientSearchName = element.firstName + ' ' + element.middleName + ' ' + element.lastName;
          this.patientlistData.push(element);
          this.patientlistDataSearch.push(element);
        });

      }
    });
  }

  NextScreen() {
    if (this.changeselectCase && this.changeselectPatientId && this.changeselectCase != '' && this.changeselectPatientId != '') {
      this.showchangedatetime = true;
      this.showchangepatient = false;
      this.showdatetimes = true;
    } else {
      if (this.changeselectPatient == '') {
        // this.displayErrorMessage("Please Select Patient!")
        let responseHandle = {
          message: 'Please Select Patient!',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      } else {
        // this.displayErrorMessage("Please Select Case!")
        let responseHandle = {
          message: 'Please Select Case!',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      }
    }
  }

  onSelectAll(e: any): void {
    for (let i = 0; i < this.lstgeneralCase.length; i++) {
      this.lstgeneralCase[i].checked = event;
      const item = this.lstgeneralCase[i].caseName;
      let obj = {
        caseName: item
      };
      this.checkvalue.push(obj);
    }
  }

  GetcheckCaseValue(event, details, index) {
    this.lstgeneralCase[index].checked = event;
    if (event == true) {
      let obj = {
        caseName: details
      };
      this.checkvalue.push(obj);
    } else {
      this.checkvalue.forEach((element, index) => {
        if (element.caseName == details) {

          this.checkvalue.splice(index, 1);
        }
      });
    }
  }

  GetcheckpractionerValue(event, details, i) {
    if (event == true) {
      let obj = {
        practitionerId: details.id
      };
      this.lstpractitionerlist[i].checked = true;
      this.checkpractitionervalue.push(obj);
    } else {
      this.lstpractitionerlist[i].checked = false;
      this.checkpractitionervalue.forEach((element, index) => {
        if (element.practitionerId == details.id) {
          this.checkpractitionervalue.splice(index, 1);
        }
      });
    }
    return;
  }

  filtercasedata(type) {
    if (this.lastCopyFinaldata) {
      if (type == 'Filterbycase') {
        if (this.checkvalue.length != 0) {
          this.lstfinaldata = [];
          const filterData = [];
          this.pinnotes = false;
          for (let i = 0; i < this.checkvalue.length; i++) {
            for (let j = 0; j < this.lastCopyFinaldata.length; j++) {
              if (this.lastCopyFinaldata[j].key == this.checkvalue[i].caseName) {
                filterData.push(this.lastCopyFinaldata[j]);
                break;
              }
            }
            for (let j = 0; j < this.lstpindata.length; j++) {
              if (this.lstpindata[j].casesName == this.checkvalue[i].caseName) {
                this.pinnotes = true;
                break;
              }
            }
          }
          this.lstfinaldata = filterData;
          if (this.lstfinaldata.length > 0) {
            this.ShowProgresslist = true;
          }
          this.checkvalue = [];
          // this.select_all = false;
        }
        this.openfiltercase = false;
      } else if (type == 'Filterbypractioner') {
        this.lstfinaldata = [];
        this.pinnotes = false;

        for (let i = 0; i < this.checkpractitionervalue.length; i++) {
          for (let j = 0; j < this.lastCopyFinaldata.length; j++) {
            for (let k = 0; k < this.lastCopyFinaldata[j].value.length; k++) {
              if (this.lastCopyFinaldata[j].value[k].practitionerId == this.checkpractitionervalue[i].practitionerId) {
                this.lstfinaldata.push(this.lastCopyFinaldata[j]);
                break;
              }
            }
          }
          for (let k = 0; k < this.lstpindata.length; k++) {
            if (this.lstpindata[k].practitionerId == this.checkpractitionervalue[i].practitionerId) {
              this.pinnotes = true;
              break;
            }
          }
        }
        this.openfilterpractitioner = false;
        this.checkpractitionervalue = [];
      } else if (type == 'Filterbydaterange') {
        let SelectFromDatefromat = moment(this.selectFromDate).format('MM/DD/YYYY');
        let SelectToDatefromat = moment(this.selectToDate).format('MM/DD/YYYY');
        this.lstfinaldata = [];
        for (let j = 0; j < this.lastCopyFinaldata.length; j++) {
          let objValue = [];
          for (let k = 0; k < this.lastCopyFinaldata[j].value.length; k++) {
            let FromDatefromat = moment(this.lastCopyFinaldata[j].value[k].entryOrAppointmentDate).format('MM/DD/YYYY');
            if ((FromDatefromat >= SelectFromDatefromat) && (FromDatefromat <= SelectToDatefromat)) {
              objValue.push(this.lastCopyFinaldata[j].value[k]);
              // break;
            }
          }
          if (objValue.length > 0) {
            this.lastCopyFinaldata[j].value = objValue;
            this.lstfinaldata.push(this.lastCopyFinaldata[j]);
          }
        }

        this.openfilterdaterange = false;
      } else {
        console.log('False');
      }
    } else {
      this.close();
      // this.displayErrorMessage("Treatment Notes Not Found!")
      let responseHandle = {
        message: 'Treatment Notes Not Found!',
        type: 'error'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    }
  }

  confirmDelete(data) {
    if (data == 'yes') {
      this.mytemplateshow = true;
      this.showprogressbar = true;
      this.addNewPatienttemplateshow = false;
    } else {
      this.mytemplateshow = false;
      this.addNewPatienttemplateshow = true;
    }
    this.closepopupData = false;
  }

  expandsinglenote(data, group, subGroup) {
    this.lstfinaldata[group].value[subGroup].expand = !this.lstfinaldata[group].value[subGroup].expand;
    this.IsShowfilter = false;
    this.IsShow = false;
  }

  // isGroupShownSingle(group) {
  //   return this.SingleshownGroup === group;
  // }
  closepopup() {
    this.openexportnotepreview = false;
    if (this.exportWindowShow == true) {
      this.openexportnote = true;
    } else {
      this.openexportnote = false;
    }
  }

  // Progress Note End

  //PDF EXPORT
  exportPDF() {
    const exportContent = document.getElementById('printContent');
    if (exportContent) {
      this.blockUI.start();
      html2canvas(exportContent).then(canvas => {
        let fileWidth = 208;
        let fileHeight = canvas.height * fileWidth / canvas.width;
        const FILEURI = canvas.toDataURL('image/png');
        let PDF = new jsPDF('p', 'mm', 'a4');
        let position = 0;
        PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight);
        PDF.save('export.pdf');
        this.blockUI.stop();
      });
    }
  }

  selectedAppointment(event) {
    this.selectedSizes = [];
    this.treatmentNoteForm.controls['service'].setValue([]);
    this.treatmentNoteForm.controls['service'].updateValueAndValidity();
    setTimeout(() => {
      if (this.lsrservices.length == 1) {
        this.treatmentNoteForm.controls['service'].setValue([this.lsrservices[this.lsrservices.length - 1].value]);
        this.treatmentNoteForm.controls['service'].updateValueAndValidity();
        this.onSizeChange(this.lsrservices);
      }
    }, 1000);
  }

  // closepopupdata(index) {
  //   this.lstpreview.splice(index, 1)
  // }


  //date picker start
  calenderPopupFocus(isFocus: boolean, from: any) {
    console.log(isFocus, from);
    // if (from == 'injuryDate') {
    //   this.isCalenderInjuryFocus = isFocus;
    // }
    if (from == 'selectdate') {
      this.isCalenderStartFocus = isFocus;
    }
    // if (from == 'duplicate') {
    //   this.isCalenderEndFocus = isFocus;
    // }
  }

  toggleCalendarPopup(val, from) {
    if (from == 'date') {
      this.isCalendarOpen = val;
    } else {
      this.isCalendarOpenDuplicate = val;
    }
  }

  get getSelectedFormattedDate() {
    if (this.currentView != 'day') {
      return this.getFormatedDateForWeek(this.currentDate, this.getDaysToAdd());
    }
    return this.getFormatedDate(this.currentDate);
  }

  get getSelectedFormattedDateDuplicate() {
    if (this.currentView != 'day') {
      return this.getFormatedDateForWeek(this.currentDuplicateDate, this.getDaysToAdd());
    }
    return this.getFormatedDate(this.currentDuplicateDate);
  }

  private getDaysToAdd(): number {
    return this.currentView == 'day' ? 1 : this.currentView == 'week' ? 6 : 4;
  }


  getFormatedDateForWeek(currentDate: Date, daysToAdd: number): string {
    let start = this.getFormatedDate(this.getStartDateOfWeek(currentDate));
    let end = this.getFormatedDate(this.getEndDateOfWeek(currentDate, daysToAdd));
    return start + ' to ' + end;
  }

  getFormatedDate(date) {
    return moment(date).format(this.dateFormat);
  }

  getStartDateOfWeek(date: Date): Date {
    return moment(date).startOf('isoWeek').toDate();
  }

  getEndDateOfWeek(date: Date, period: number): Date {
    return this.addAndGetNewValue(date, period, 'd');
  }

  addAndGetNewValue(date: Date, num, type: string): Date {
    return moment(date).clone().add(type, num).toDate();
  }

  openCalendarPopup(from) {
    this.isCalenderStartFocus = false;
    if (from == 'date') {
      let leftArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    } else {
      let leftArrContainer = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }

    this.changeDayLabelName(from);
  }

  optionChanged(from) {
    this.changeDayLabelName(from);
  }

  private changeDayLabelName(from) {
    let tables = null;
    if (from == 'date') {
      tables = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-body table');
    } else {
      tables = document.querySelectorAll('.calendar-popup-duplicate.dx-calendar .dx-calendar-body table');
    }
    if (tables) {
      Array.from(tables).forEach((table: any) => {
        let nameLabes = table.querySelectorAll('thead tr th');
        Array.from(nameLabes).forEach((th: any) => {
          let name = th.getAttribute('abbr');
          th.innerHTML = name.substr(0, 2);
        });
      });
    }
  }

  onDateSelect(from, $event, itemTypeId) {
    const m = new DateTimeModel();
    // if (this.treatmentNotes.treatmentData != undefined && this.treatmentNotes.treatmentData != null) {
    //   const itemTypeIndex = this.treatmentNotes.treatmentData.findIndex(
    //     (x) => x.id === itemTypeId
    //   );
    if (from == 'date') {
      // this.treatmentNoteForm.controls['selectDate'].setValue(this.currentDate)
      // this.treatmentNoteForm.controls['selectDate'].updateValueAndValidity()
      // m.date = this.currentDate
      // this.treatmentNotes.treatmentData[itemTypeIndex].dateTime.date = m.date
      // this.changeselectdate = this.currentDate
    } else {
      // this.treatmentNoteForm.controls['entryOrAppointmentDate'].setValue(this.currentDuplicateDate)
      this.treatmentNoteForm.controls['entryOrAppointmentDate'].setValue($event);
      this.treatmentNoteForm.controls['entryOrAppointmentDate'].updateValueAndValidity();
      m.date = this.currentDuplicateDate;
    }
    // }
    this.toggleCalendarPopup(false, from);

  }

  isApplyWeekOrMF(date: Date) {
    return (this.isInWeek(date) || this.isCurrDateInWeek(date)) && this.currentView != 'day';
  }

  isInWeek(date: Date) {
    return this.isBetween(this.currHvrDateStart, this.currHvrDateend, date);
  }

  isBetween(start: Date, end: Date, date: Date): boolean {
    let mm = moment(date);
    return mm.isSameOrAfter(start) && mm.isSameOrBefore(end);
  }

  isCurrDateInWeek(date: Date) {
    let currHvrDateStart = this.getStartDateOfWeek(this.currentDate);
    let currHvrDateend = this.getEndDateOfWeek(currHvrDateStart, this.getDaysToAdd());
    return this.isBetween(currHvrDateStart, currHvrDateend, date);
  }


  mouseHover(cell: any) {
    this.hoverDate = cell.date;
    this.setupStartEndDayOfWeek();
  }

  setupStartEndDayOfWeek() {
    this.currHvrDateStart = this.getStartDateOfWeek(this.hoverDate);
    this.currHvrDateend = this.getEndDateOfWeek(this.currHvrDateStart, this.getDaysToAdd());
  }

  setTodayDate(from) {
    if (from == 'date') {
      this.currentDate = this.getCurrentDate();
    } else {
      this.currentDuplicateDate = this.getCurrentDate();
    }
    this.prepareDateArrayForSdlr(from);
  }

  getCurrentDate(): Date {
    return moment().toDate();
  }

  prepareDateArrayForSdlr(from) {
    if (this.currentView == 'day') {
      if (from == 'date') {
        this.selectedDate = [new Dte(this.currentDate)];
      } else {
        this.selectedDateDuplicate = [new Dte(this.currentDuplicateDate)];
      }
    } else if (this.currentView == 'week') {
      if (from == 'date') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.currentDuplicateDate);
        this.selectedDateDuplicate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          // this.selectedDateDuplicate.push(new Dte(date));
        }
      }
    } else {
      if (from == 'date') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.currentDuplicateDate);
        this.selectedDateDuplicate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDateDuplicate.push(new Dte(date));
        }
      }
    }
  }

  nextPrevDate(num: number, from) {
    if (from == 'date') {
      this.currentDate = this.addAndGetNewValue(this.currentDate, num * this.getDaysToAdd(), 'd');
    } else {
      this.currentDuplicateDate = this.addAndGetNewValue(this.currentDuplicateDate, num * this.getDaysToAdd(), 'd');
    }
    this.prepareDateArrayForSdlr(from);
  }

  //date picker end

  //time picker start
  handleTimeValueChange(event, itemTypeId, type) {
    if (event && event.value != null && (type != null && type != undefined)) {
    }
  }

  //time picker end

  selectedPatient(patientInfo) {
    this.changeselectPatient = patientInfo.firstName + ' ' + patientInfo.lastName + '(' + patientInfo.gender + ')';
    this.changeselectPatientId = patientInfo.id;
  }

  //filter Patient name
  handleFilterCase(event: any) {
    if (event.target.value != '') {
      this.patientlistDataSearch = this.patientlistData.filter(
        (s) => s.patientSearchName.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.patientlistDataSearch = this.patientlistData;
    }
  }

  checkboxTest(progressdatas, key) {
    console.log(progressdatas, key);
  }

  // remove duplicate data
  uniqueData(data, key) {
    return [
      ...new Map(data.map(x => [key[x], x])).values()
    ];
  }

  goBackToSate() {
    if (this.ifFromState()) {
      if (this.routerState.fromPage == FromPage.Appointment) {
        const state: NavigationExtras = {
          state: {
            fromState: true,
            fromPage: FromPage.Letter,
            data: {initFromHistory: true}
          }
        };
        this.router.navigate(['appointment'], state);
      }
    }
  }

  ifFromState(): boolean {
    return this.routerState && this.routerState.fromState;
  }

}
