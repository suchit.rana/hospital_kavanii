const deviceOffsetX = 40;
const heightOffsetPercent = 2.75;

export class ZoomFeature {

    // SharedService: any;
    device: any = {};
    constructor(private deviceDimension: any) {
        this.device = deviceDimension;
        // this.SharedService = this.Shared;
    }

    /* Zoom canvas fit to screen */
    /* ----------------------------------------------------------------------------------------------------- */
    async fitCanvasIntoDeviceScreen(canvas) {
        this.getScaleToFit(canvas).then((zoomPoint) => {
            this.setZoom(zoomPoint.scale, canvas);
        });
    }

    async getScaleToFit(canvas): Promise<any> {
        return new Promise(resolve => {
            let width;
            let height;
            const canvasOriginalWidth = canvas.getWidth() * canvas.getZoom();
            const canvasOriginalHeight = canvas.getHeight() * canvas.getZoom();
            width = this.device.width - deviceOffsetX;
            height = Math.round(this.device.height - (this.device.height / heightOffsetPercent));
            if (this.isPortrait(canvasOriginalWidth, canvasOriginalHeight)) {
                const scale = height / canvasOriginalHeight;
                if (width < canvasOriginalWidth * scale) {
                    resolve({ width: canvasOriginalWidth, height: canvasOriginalHeight, scale: width / canvasOriginalWidth });
                }
                else {
                    resolve({ width: canvasOriginalWidth, height: canvasOriginalHeight, scale: scale });
                }
            }
            else if (this.isLandscape(canvasOriginalWidth, canvasOriginalHeight)) {
                const scale = width / canvasOriginalWidth;
                if (height < canvasOriginalHeight * scale) {
                    resolve({ width: canvasOriginalWidth, height: canvasOriginalHeight, scale: height / canvasOriginalHeight });
                }
                else {
                    resolve({ width: canvasOriginalWidth, height: canvasOriginalHeight, scale: scale });
                }
            }
            else {
                resolve({ width: canvasOriginalWidth, height: canvasOriginalHeight, scale: width / canvasOriginalWidth });
            }
        });
    }

    setZoom(scale, canvas) {
        canvas.setDimensions({
            width: Math.round(canvas._width * scale),
            height: Math.round(canvas._height * scale)
        });
        canvas.setZoom(scale);
        canvas.calcOffset();
    }

    isPortrait(width, height) {
        return height > width + (height / 6);
    }

    isLandscape(width, height) {
        return width > height + (width / 6);
    }

}