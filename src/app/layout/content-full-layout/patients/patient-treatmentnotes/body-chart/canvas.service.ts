import 'fabric';
import { BehaviorSubject } from 'rxjs';
declare const fabric: any;

const objectConfig = {
    cornerColor: '#ffffff',
    borderColor: '#ffffff',
    cornerSize: 12,
    cornerStrokeColor: '#ffffff',
    cornerStyle: 'circle',
    hasControls: true,
    transparentCorners: false,
    cornerShadowColor: '#35394188',
    strokeShadowBlur: 2,
    cornerShadowBlur: 10,
    cornerShadowOffsetX: 0,
    cornerShadowOffsetY: 0,
    lockUniScaling: false,
    borderOpacityWhenMoving: 0,
    centeredScaling: true,
    centeredRotation: true,
    lockScalingFlip: true,
    snapAngle: 45,
    snapThreshold: 5,
    controls: {}
};

fabric.Object.prototype.set(objectConfig);

export class canvasService {

    private EVENT_OBJECT_SELECTED = new BehaviorSubject(Event);
    onObjectSelected = this.EVENT_OBJECT_SELECTED.asObservable();
    private EVENT_OBJECT_SELECTION_CLEARED = new BehaviorSubject(Event);
    onObjectDeSelected = this.EVENT_OBJECT_SELECTION_CLEARED.asObservable();

    canvas: any;
    canvasProps: any = {
        canvasWidth: 1080,
        canvasHeight: 1920
    };
    feature: any = {
        pointer: true,
        pencil: false,
        pen: false,
        marker: false,
        highlighter: false,
        line: false,
        rectangle: false,
        ellipse: false,
        eraser: false,
        text: false,
        moveImage: false
    };

    constructor(id, options) {
        this.canvas = new fabric.Canvas(id, {
            width: options.width || this.canvasProps.canvasWidth,
            height: options.height || this.canvasProps.canvasHeight,
            _width: options.width || this.canvasProps.canvasWidth,
            _height: options.height || this.canvasProps.canvasHeight,
            preserveObjectStacking: true,
            // selection: options.selection || false
        });
        this.AddEvents();
    }

    async AddEvents() {
        this.canvas.on({
            'selection:created': (e) => {
                this.applyControlHide(e);
                this.EVENT_OBJECT_SELECTED.next(e);
            },
            'selection:updated': (e) => {
                this.applyControlHide(e);
                this.EVENT_OBJECT_SELECTED.next(e);
            },
            'selection:cleared': (e) => {
                this.EVENT_OBJECT_SELECTION_CLEARED.next(e);
            },
            'object:scaling': (e) => {
                this.applyControlHide(e);
            },
            'mouse:up': (e) => {
                console.log(e)
                if (this.feature.pointer == true) {
                    let count = 0;
                    this.canvas.forEachObject(object => {
                        if (object.id = "point") {
                            count++;
                        }
                    });
                    let circle = new fabric.Circle({ radius: 15, fill: '#394085', top: 0, left: 0, originX: 'center', originY: 'center' })
                    let number = new fabric.Text((count + 1).toString(), { top: 0, left: 0, fill: '#fff', fontSize: 12, fontFamily: 'verdana', originX: 'center', originY: 'center' });
                    let group = new fabric.Group([circle, number], {
                        top: Math.round(e.absolutePointer.y - 7.5),
                        left: Math.round(e.absolutePointer.x - 7.5),
                        id: 'point',
                    });
                    this.canvas.add(group);
                    this.canvas.renderAll();
                }
            }
        });
    }

    applyControlHide(e) {
        const sideOffset = 40;
        const cornerOffset = 30;
        const canvasZoom = this.canvas.getZoom();
        const height = e.target.getScaledHeight() * canvasZoom;
        const width = e.target.getScaledWidth() * canvasZoom;

        switch (e.target.type) {
            case 'textbox':
                if (height < cornerOffset || width < cornerOffset) {
                    e.target.setControlsVisibility(
                        { mt: false, mb: false, ml: false, mr: false, bl: false, br: true, tl: false, tr: false, mtr: true }
                    );
                }
                else if (height < sideOffset) {
                    e.target.setControlsVisibility(
                        { mt: false, mb: false, ml: false, mr: false, bl: true, br: true, tl: true, tr: true, mtr: true }
                    );
                }
                else {
                    e.target.setControlsVisibility(
                        { mt: false, mb: false, ml: false, mr: true, bl: true, br: true, tl: true, tr: true, mtr: true }
                    );
                }
                break;
            case 'image':
                if (width < cornerOffset && height < cornerOffset) {
                    e.target.setControlsVisibility(
                        { mt: false, mb: false, ml: false, mr: false, bl: false, br: true, tl: false, tr: false, mtr: true }
                    );
                }
                else {
                    e.target.setControlsVisibility(
                        { mt: false, mb: false, ml: false, mr: false, bl: true, br: true, tl: true, tr: true, mtr: true }
                    );
                }
                break;
            default:
                e.target.setControlsVisibility(
                    { mt: true, mb: true, ml: true, mr: true, bl: true, br: true, tl: true, tr: true, mtr: true }
                );
                break;
        }
    }

    async getOriginalCanvasSize() {
        return {
            width: this.canvas.getWidth() / this.canvas.getZoom(),
            height: this.canvas.getHeight() / this.canvas.getZoom(),
            zoom: this.canvas.getZoom()
        };
    }

    async getCanvas() {
        return this.canvas;
    }

    async renderAll() {
        this.canvas.renderAll();
    }

    async clearSelection() {
        this.canvas.discardActiveObject().renderAll();
    }

    setBackground(image): Promise<any> {
        return new Promise((resolve, reject) => {
            fabric.util.loadImage(image.src, (img) => {
                const background = new fabric.Pattern({
                    source: img,
                    repeat: 'no-repeat',
                    scaleX: this.canvas.width / image.width,
                    scaleY: this.canvas.height / image.height,
                });
                this.canvas.backgroundColor = background;
                this.canvas.renderAll();
                resolve(null);
            });
        })
    }



} 