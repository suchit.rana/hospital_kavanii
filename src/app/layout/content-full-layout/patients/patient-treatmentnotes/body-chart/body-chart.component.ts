import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { editor } from '../editor.class';
import 'fabric';
declare const fabric: any;

interface BChartData {
  title?: string,
  helpText?: string,
  chartSize?: Array<string>,
  selectedChartSize?: string,
  canvasInstance?: any
}

const Size: any = {
  SMALL: "Small",
  MEDIUM: "Medium",
  LARGE: "Large"
}

const CanvasSize: any = [
  { zoomLevel: 0.5, type: Size.SMALL },
  { zoomLevel: 1, type: Size.MEDIUM },
  { zoomLevel: 2, type: Size.LARGE }
]

const maxCanvasSize = 800;

@Component({
  selector: 'app-body-chart',
  templateUrl: './body-chart.component.html',
  styleUrls: ['./body-chart.component.css']
})
export class BodyChartComponent extends editor implements OnInit {

  @Input() ngData: BChartData = {};
  @Output() deleteChartClick: EventEmitter<any> = new EventEmitter();
  @Output() moveDown: EventEmitter<any> = new EventEmitter();
  @Output() moveUp: EventEmitter<any> = new EventEmitter();

  loading: boolean = false;
  randomCanvasId: string;
  drawingTools: Array<string> = ['pencil', 'pen', 'marker', 'highlighter', 'eraser', 'line'];
  data: any;
  isTitleInEditing: boolean = false;
  isClearSketchDialog: boolean = false;
  imageUrl: any = '';
  chartdata: any;
  length: any;
  dummyData: BChartData = {
    title: '',
    helpText: '',
    chartSize: [],
    selectedChartSize: Size.LARGE
  };
  zoomFeature: any;
  zoomLevel: any = 1;
  opendeleteform: boolean = false;
  imageSizeValue: any;
  constructor(public cd: ChangeDetectorRef) {
    super()
    this.randomCanvasId = 'canvas_' + JSON.stringify(Math.round(Math.random() * 100000000));
    this.defaultSelectedToolName = "pointer";
  }

  ngOnInit() {
    this.data = { ...this.ngData }
    let x = this.data.chartImage.image.split("/");
    let y = this.data.pointercount ? this.data.pointercount : 0;
    this.loading = true;
    this.setDefaultData();
    setTimeout(() => {
      this.initCanvas(this.randomCanvasId, {
        width: 1080,
        height: 1080,
        _width: 1080,
        _height: 1080,
        preserveObjectStacking: true
      }).then(_result => {
        if (this.data.chartImage.image == '' || x[4] == 'dropfileimage.png') {
          this.imageUrl = "./assets/chartimage.png";
          this.setBackgroundImage(this.imageUrl).then(image => {
            this.resizeCanvasToSize(image).then(_newCanvas => {
              this.enableDrawingTool('pointer');
              this.onPointeCreatedEvent(y);
              this.onPathCreatedEvent();
              this.putHistory();
              this.on(['undo', 'redo']);
              this.loading = false;
            })
          })
        }
        else {
          this.imageUrl = this.data.chartImage.image;
          this.setBackgroundImage(this.imageUrl).then(image => {
            this.resizeCanvasToSize(image).then(_newCanvas => {
              this.enableDrawingTool('pointer');
              this.onPointeCreatedEvent(y);
              this.onPathCreatedEvent();
              this.putHistory();
              this.on(['undo', 'redo']);
              this.loading = false;
            })
          })
        }
      });
    }, 0);
    this.chartdata = localStorage.getItem("Chartitem");
    this.length = localStorage.getItem("Chartitemlength");
  }

  doMoveDown() {
    this.moveDown.emit();
  }

  doMoveUp() {
    this.moveUp.emit();
  }
  sizeValue(value) {
    this.imageSizeValue = value

  }

  getClass(data) {
    if (data == 'Small') {
      let small_image =
      {
        "width": 300,
        "height": 230
      };
      this.resizeCanvasToSize(small_image)
    } else if (data == 'Medium') {
      let medium_image =
      {
        "width": 530,
        "height": 400
      };
      this.resizeCanvasToSize(medium_image)
    } else if (data == 'Large') {
      let large_image =
      {
        "width": 700,
        "height": 500
      };
      this.resizeCanvasToSize(large_image)
    }
  }
  setDefaultData() {
    !this.data.itemLabel ? this.data.itemLabel = "Heading" : null;
    !this.data.helpText ? this.data.helpText = "Draw or type notes on the provided Body Chart or any image of your choice" : null;
    !this.data.chartSize || this.data.chartSize.length <= 0 ? this.data.chartSize = [Size.SMALL, Size.MEDIUM, Size.LARGE] : null;
    !this.data.selectedChartSize ? this.data.selectedChartSize = Size.LARGE : null;
    if (this.data.chartImage.imageSize == 'Large') {
      this.data.chartImage.imageSize = Size.LARGE
    }
    else if (this.data.chartImage.imageSize == 'Medium') {
      this.data.chartImage.imageSize = Size.MEDIUM
    }
    else {
      this.data.chartImage.imageSize = Size.SMALL
    }
  }

  editTitle() {
    this.isTitleInEditing = true;
    this.dummyData = { ...this.data }
  }

  closeTitleEditing(state) {
    this.isTitleInEditing = false;
    if (state == "cancel") {
      this.data = { ...this.dummyData }
    }
    else {
      // Call api here for save
    }
  }

  closeClearSketchDialog(state) {
    this.opendeleteform = false;
    this.isClearSketchDialog = false;
    if (state != "cancel") {
      this.clearAllSketch();
      for (let i = 0; i <= this.pointArray.length; i++) {
        this.pointArray.splice(i);
      }
      this.onPointeCreatedEvent(0);
    }
    else {
      this.opendeleteform = false;
      this.deleteChartClick.emit();
    }
    // this.generatePreview()
  }

  changeMendatory($event) {
    console.log("event", $event);
  }
  deleteChartForm() {
    this.opendeleteform = true;
    this.deleteChartClick.emit();
  }
  deleteChart() {
    this.isTitleInEditing = false;
    this.opendeleteform = true;
    // delete chart
    this.deleteChartClick.emit();
  }
  deleteitem(type) {
    if (type == 'delete') {
      this.opendeleteform = false;
      this.deleteChartClick.emit();
    }
    else {
      this.opendeleteform = false;
    }
  }
  saveTitleChanges() {
    this.isTitleInEditing = false;
  }
  changeBodyImage($event) {
    this.loading = true;
    this.imageUrl = URL.createObjectURL($event.target.files[0])
    this.setBackgroundImage(this.imageUrl).then(image => {
      this.resizeCanvasToSize(image).then(canvas => {
        this.fitCanvasToView({ width: 800, height: 800 }).then(result => {
          this.loading = false;
        })
      })
    })
  }

  openFilePicker() {
    document.getElementById("upload").click();
  }

  enableLine() {
    this.activateDrawingTool('line');
    this.canvas.isDrawingMode = false;
    this.canvas.isDrawingMode = true;
    fabric.Line.prototype.globalCompositeOperation = "destination-out";
    this.canvas.renderAll();
  }

  enableDrawingTool(toolName) {
    if (this.drawingTools.indexOf(toolName) != -1) {
      if (!this.canvas.isDrawingMode)
        this.enableDrawingMode();
      this.activateDrawingTool(toolName);
    }
    else {
      this.disableDrawingMode();
      this.makeDrawingUnselectable()
      this.activateStandardTools(toolName);
    }
  }

  enableEraserTool() {
    this.activateDrawingTool('eraser');
    this.canvas.isDrawingMode = false;
    this.canvas.isDrawingMode = true;
    fabric.PencilBrush.prototype.globalCompositeOperation = "destination-out";
    this.canvas.renderAll();
  }

  clearAllSketch() {
    this.disableDrawingMode();
    this.canvas.getObjects().forEach(object => {
      this.canvas.remove(object);
    });
    this.resetTools();
  }

  generatePreview() {
    this.generateImage(this.imageUrl).then(result => {
      this.data.chartImage.image = result;
      this.data.pointercount = this.pointArray.length
      setTimeout(() => {
        localStorage.setItem('BodychartData', JSON.stringify(this.data));
      }, 1000);
    })


  }
  getbodydata() {
    this.generatePreview()
  }
  deletedata(point, index) {
    this.pointArray.splice(index, 1);
    this.data.chartTextbox.splice(index, 1)
    this.canvas.getObjects().forEach(object => {
      if (object.toJSON(['id']).id == point.id) {
        this.canvas.remove(object);
      }
    })
    this.canvas.renderAll();

  }
  closedata() {
    alert("hii")
    this.opendeleteform = false;
  }
  mandatoryValueChange($event: { checked: boolean }) {
    this.data.isMandatory = $event.checked
  }
}
