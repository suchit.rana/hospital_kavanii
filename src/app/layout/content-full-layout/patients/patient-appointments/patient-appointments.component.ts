import {ChangeDetectorRef, Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  IPatientAppointment,
  IPatientAppointmentGrid,
  IPatientAppointmentHeaderData,
  IPatientAppointmentRecurringData,
  IPlannedServiceList,
  PatientAppointmentService
} from '../rest-service/app.patient-appointment.service';
import {Router} from '@angular/router';
import {AppoinmentDateUtils} from '../../appointment/AppoinmentDateUtils';
import {AppDialogService} from '../../../../shared/services/app-dialog.service';
import {AddEditPlannedServicesComponent} from './component/add-edit-planned-services/add-edit-planned-services.component';
import {AppointmentsClassesComponent} from './component/appointments-classes/appointments-classes.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppointmentAlertComponent} from './appointment-popup/appointment-alert/appointment-alert.component';
import {FormControl} from '@angular/forms';
import {ExportAppointmentsComponent} from './appointment-popup/export-appointments/export-appointments.component';
import {AppointmentType} from '../../../../enum/application-data-enum';
import {BaseItemComponent} from '../../../../shared/base-item/base-item.component';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-patient-appointments',
  templateUrl: './patient-appointments.component.html',
  styleUrls: ['./patient-appointments.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PatientAppointmentsComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.patients_basic_access;

  readonly APPT_CLASSES = 'APPT_CLASSES';
  readonly PLANNED_SERVICES = 'PLANNED_SERVICES';
  readonly FROM_DATE = 'FROM_DATE';
  readonly TO_DATE = 'TO_DATE';

  @BlockUI() blockUI: NgBlockUI;
  @Input() patientId: string;
  activeBtn = this.APPT_CLASSES;
  patientSummary: any[] = [
    {
      index: 0,
      active: true,
      number: '0',
      title: 'Total Bookings',
      key: 'totalBooking'
    },
    {
      index: 1,
      active: false,
      number: '0',
      title: 'Completed Booking',
      key: 'completedBooking'
    },
    {
      index: 2,
      active: false,
      number: '0',
      title: 'Missed Booking',
      key: 'missedBooking'
    },
    {
      index: 3,
      active: false,
      number: '0',
      title: 'Cancelled Booking',
      key: 'cancelledBooking'
    },
    {
      index: 4,
      active: false,
      number: '0',
      title: 'Future Booking',
      key: 'futureBooking'
    },
    {
      index: 5,
      active: false,
      number: '0',
      title: 'Completed Booking This year',
      key: 'completedBookingThisYear'
    }
  ];

    /** Date filter control */
    fromDateCtrl = new FormControl();
    toDateCtrl = new FormControl();

  headerData: IPatientAppointmentHeaderData;
  recurringData: IPatientAppointmentRecurringData[];
  apptClassesData: IPatientAppointmentGrid[] = [];
  plannedServices: IPlannedServiceList[] = [];
  daysSinceLastVisit = 0;

  title = 'You Haven’t created any appointment yet.';
  description = 'You can create appointment(s) for this patient from the appointment module.';
  isDisplayClear = false;

  @ViewChild(AppointmentsClassesComponent, {static: false}) apptClassesComp;
  @ViewChild('dateFilter', {static: false}) dateFilter;

  constructor(
    protected router: Router,
    private cdr: ChangeDetectorRef,
    private patientAppointmentService: PatientAppointmentService,
    private appDialogService: AppDialogService
  ) {
    super(null);
  }

  ngOnInit() {
    this.getData();
  }

  onSummaryCLick(index: number) {
    this.patientSummary.forEach((p) => {
      p.active = false;
    });
    const pSummary = this.patientSummary.find((x) => x.index === index);
    pSummary.active = true;
    this.apptClassesComp.filterData(pSummary.key);
  }

  appointAlert() {
    this.appDialogService.open(AppointmentAlertComponent, {
      title: 'Appointment Alert',
      width: '660px',
      hideOnSave: false,
      hideOnCancel: false,
      hideFooter: true,
      data: {patientId: this.patientId}
    });
  }

  addEditPlannedService() {
    const ref = this.appDialogService.open(AddEditPlannedServicesComponent, {
      title: 'Add Planned Service',
      width: '660px',
      hideOnSave: false,
      hideOnCancel: false,
      hideFooter: true,
      data: {patientId: this.patientId}
    });
    ref.close.subscribe(() => {
      this.loadPlannedService();
    });
  }

  loadPlannedService() {
    this.blockUI.start();
    this.patientAppointmentService.getPlannedServices(this.patientId).subscribe(response => {
      if (response) {
        this.plannedServices = response;
      }
      this.blockUI.stop();
    });
  }

  loadApptClasses() {
    this.blockUI.start();
    this.patientAppointmentService.get(this.patientId, this.fromDateCtrl.value, this.toDateCtrl.value).subscribe((response: IPatientAppointment) => {
      if (response) {
        this.apptClassesData = response.gridData;
        if (!this.fromDateCtrl.value && !this.toDateCtrl.value) {
          this.headerData = response.headerData;
        }
        this.recurringData = response.recurringData;
        this.patientSummary.forEach(value => {
          value.number = this.headerData[value.key];
        });
        this.daysSinceLastVisit = Math.round(this.headerData['daysSinceLastVisit']);
        this.apptClassesData.forEach(value => {
          const startDate = AppoinmentDateUtils._toDate(value.startDateUTC);
          value['opened'] = false;
          value.startDateUTCStr = AppoinmentDateUtils.formatDateTimeDefault(startDate, 'DD MMM YYYY (ddd)', false);
          value['startDateUTCTime'] = AppoinmentDateUtils.formatDateTimeDefault(startDate, 'hh:mm A', false);
          value.endDateUTC = AppoinmentDateUtils._toDate(value.endDateUTC);
          value['typeVal'] = value.type === AppointmentType.APPOINMENT ? 'Appt' : 'Class';
          value['payerName'] = value.payerName ? value.payerName + '(' + value.payerCount + '/' + value.payerOrdered + ')' : 'Self';
          value['referralName'] = value.referralName ? value.referralName + '(' + value.referralCount + '/' + value.referralOrdered + ')' : 'No Referral';
          let offerings = '';
          value.offerings.forEach(offering => {
            offerings += offering+"\n";
          });
          value['offeringsVal'] = offerings;
        });
      }
      this.blockUI.stop();
    });
  }

  exportAppointment() {
    this.appDialogService.open(ExportAppointmentsComponent, {
      title: 'Export',
      width: '400px',
      data: {'apptList': this.apptClassesData}
    });
  }

  filterApptClasses() {
    this.appDialogService.open(this.dateFilter, {
      title: 'Filter BY DATE RANGE',
      width: '420px',
      saveBtnTitle: 'Apply'
    }).clickSave.subscribe((result) => {
      this.isDisplayClear = true;
      this.loadApptClasses();
    });
  }

  clearFilter() {
    this.isDisplayClear = false;
    this.fromDateCtrl.setValue(null);
    this.toDateCtrl.setValue(null);
    this.loadApptClasses();
  }

  onDateSelect($event: Date | number | string, from: string) {
    if (from == this.FROM_DATE) {
      this.fromDateCtrl.setValue(new Date($event));
    } else {
      this.toDateCtrl.setValue(new Date($event));
    }
  }

  get fromDateCtrlVal() {
    return this.fromDateCtrl.value || new Date();
  }

  get toDateCtrlVal() {
    return this.toDateCtrl.value || new Date();
  }

  private getData() {
    this.loadApptClasses();
    this.loadPlannedService();
  }

}

