import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppointmentAlertComponent} from './appointment-alert/appointment-alert.component';
import {SharedModuleModule} from '../../../../../shared/shared-module.module';
import {AskedCancelledMissedReasonComponent} from './asked-cancelled-missed-reason/asked-cancelled-missed-reason.component';
import {MatSelectModule} from '@angular/material';
import {SharedContentFullLayoutModule} from '../../../shared-content-full-layout/shared-content-full-layout.module';
import { ExportAppointmentsComponent } from './export-appointments/export-appointments.component';
import {AppTimePipeModule} from '../../../../../pipe/module/app-time-pipe.module';


@NgModule({
  declarations: [
    AppointmentAlertComponent,
    AskedCancelledMissedReasonComponent,
    ExportAppointmentsComponent
  ],
  imports: [
    CommonModule,
    SharedModuleModule,
    MatSelectModule,
    SharedContentFullLayoutModule,
    AppTimePipeModule
  ],
  entryComponents: [
    AppointmentAlertComponent,
    AskedCancelledMissedReasonComponent,
    ExportAppointmentsComponent
  ]
})
export class AppointmentPopupModule {
}
