import {Component, Input, OnInit} from '@angular/core';
import {IPatientAppointmentGrid} from '../../../rest-service/app.patient-appointment.service';
import {FormControl} from '@angular/forms';
import {AppointmentStatus} from '../../../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {DialogRef} from '@progress/kendo-angular-dialog';

@Component({
  selector: 'app-export-appointments',
  templateUrl: './export-appointments.component.html',
  styleUrls: ['./export-appointments.component.css']
})
export class ExportAppointmentsComponent implements OnInit {

  excludeCancelled = new FormControl();
  excludeMissed = new FormControl();
  excludeDeleted = new FormControl();

  @Input()
  apptList: IPatientAppointmentGrid[] = [];

  apptListFiltered: IPatientAppointmentGrid[] = [];

  selectedItem: IPatientAppointmentGrid[] = [];

  constructor(
    private dialogRef: DialogRef
  ) {
  }

  ngOnInit() {
    this.apptListFiltered = AppUtils.refrenceClone(this.apptList);
  }

  selectItem($event: MouseEvent, index: number) {
    if ($event.ctrlKey) {
      const item = this.apptListFiltered[index];
      const hasItem = this.selectedItem.find(value => value.id == item.id);
      if (hasItem) {
        item['isSelected'] = false;
        const selItemIndex = this.selectedItem.findIndex(value => value.id == hasItem.id);
        this.selectedItem.slice(selItemIndex, 1);
      } else {
        item['isSelected'] = true;
        this.selectedItem.push(item);
      }
    }
    if ($event.shiftKey) {
      const lastSelectedItem = this.selectedItem[this.selectedItem.length - 1];
      const lastSelectedItemIndex = this.apptListFiltered.findIndex(value => value.id == lastSelectedItem.id);
      for (let i = lastSelectedItemIndex; i < index; i++) {
        const item = this.apptListFiltered[i];
        const hasItem = this.selectedItem.find(value => value.id == item.id);
        if (!hasItem) {
          item['isSelected'] = true;
          this.selectedItem.push(item);
        }
      }
    }
  }

  filterData() {
    let filteredData = this.apptList;
    if (this.excludeCancelled.value) {
      filteredData = this.apptList.filter(value => value.status != AppointmentStatus.Cancelled);
    }
    if (this.excludeMissed.value) {
      filteredData = filteredData.filter(value => value.status != AppointmentStatus.Missed);
    }
    if (this.excludeDeleted.value) {
      filteredData = filteredData.filter(value => value.status != AppointmentStatus.Deleted);
    }
    this.apptListFiltered = filteredData;
  }

  preview() {
    this.dialogRef.close();
  }
}
