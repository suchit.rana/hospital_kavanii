import {Component, Input, OnInit} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../../shared/services/app-dialog.service';
import {
  IPatientAppointmentGrid,
  IPatientAppointmentRecurringData,
  PatientAppointmentService
} from '../../../rest-service/app.patient-appointment.service';
import {SettingsService} from '../../../../../../services/app.settings.service';
import {FormControl, Validators} from '@angular/forms';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {
  AppointmentConfirmationStatus,
  AppointmentStatus
} from '../../../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {AppointmentHandlerService} from '../../../../appointment/schedular-main/schedular/service/appointment-handler.service';
import {CANCEL, SAVE} from '../../../../../../shared/popup-component/popup-component.component';

export const CANCEL_REBOOK = "CANCEL_REBOOK";

@Component({
  selector: 'app-asked-cancelled-missed-reason',
  templateUrl: './asked-cancelled-missed-reason.component.html',
  styleUrls: ['./asked-cancelled-missed-reason.component.css']
})
export class AskedCancelledMissedReasonComponent implements OnInit {
  readonly OTHERS = 'OTHERS';
  readonly SAVE = SAVE;
  static ref: IPopupAction;

  @Input()
  confirmationStatus: AppointmentConfirmationStatus;
  @Input()
  status: AppointmentStatus;
  @Input()
  recurringData: IPatientAppointmentRecurringData;
  @Input()
  isRecurringChange: boolean;
  @Input()
  patientId: string;
  @Input()
  isFor: 'MISSED' | 'CANCELLED' = 'CANCELLED';
  @Input()
  apptEntryId: string;
  @Input()
  customFooter: boolean = false;

  listData: any;
  fieldName: string = 'cancelName';
  reason = new FormControl('', Validators.required);
  message = new FormControl('', Validators.required);
  loading: boolean = false;

  constructor(
    public dialog: DialogRef,
    private settingsService: SettingsService,
    private patientAppointmentService: PatientAppointmentService,
    private alertService: AppAlertService,
    private apptHandler: AppointmentHandlerService,
  ) {
  }

  ngOnInit() {
    if (this.isFor === 'MISSED') {
      this.fieldName = 'missedName';
    }
    this.getData();
    this.save();
  }

  private getData() {
    this.loading = true;
    if (this.isFor === 'MISSED') {
      this.settingsService.getAllMissedReasons().subscribe(response => {
        this.prepareAndAssignData(response, 'missedReasonLocation');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Something went wrong. Try Again.');
        this.loading = false;
      });
    } else {
      this.settingsService.getAllCancelReasons().subscribe(response => {
        this.prepareAndAssignData(response, 'cancelReasonLocation');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Something went wrong. Try Again.');
        this.loading = false;
      });
    }
  }

  private prepareAndAssignData(response, fieldName: string) {
    response.forEach(d => {
      d.locationName = d.isAllowAllLocation ? 'All Locations' : d[fieldName].map(l => l.locationName).join(',');
    });
    const other = {};
    other[this.fieldName] = this.OTHERS;
    response.push(other);
    this.listData = response;
    this.loading = false;
  }

  private save() {
    this.ref.clickSave.subscribe(() => {
      this.update(SAVE);
    });
  }

  update(_for: string) {
    if (this.reason.invalid || (this.reason.value === this.OTHERS && this.message.invalid)) {
      this.reason.markAsTouched();
      this.message.markAsTouched();
      return;
    }
    const statusReasonMessage = this.reason.value === this.OTHERS ? this.message.value : this.reason.value;
    this.patientAppointmentService.update(this.patientId, this.apptEntryId, this.confirmationStatus, this.status, this.recurringData, statusReasonMessage, this.isRecurringChange).subscribe(response => {
      this.ref.closeDialog(_for);
      this.alertService.displaySuccessMessage(`Appointment ${this.isFor === 'CANCELLED' ? 'Cancelled' : 'Missed'} successfully.`);
    }, error => {
      this.alertService.displaySuccessMessage(error || 'Failed to update status');
    });
  }

  updateAndRebook() {
    this.update(CANCEL_REBOOK);
  }

  cancel() {
    this.ref.closeDialog(CANCEL);
  }

  get ref() {
    return AskedCancelledMissedReasonComponent.ref;
  }

  static open(
    dialogService: AppDialogService,
    isFor: 'MISSED' | 'CANCELLED',
    patientId: string,
    apptEntryId: string,
    confirmationStatus: AppointmentConfirmationStatus,
    status: AppointmentStatus,
    recurringData: IPatientAppointmentRecurringData,
    isRecurringChange: boolean,
    customFooter: boolean = false
  ): IPopupAction {
    this.ref = dialogService.open(AskedCancelledMissedReasonComponent, {
      title: isFor === 'CANCELLED' ? 'Do you wish to Mark this appointment as “Cancelled”?' : 'Do you wish to Mark this appointment as “Missed”?',
      width: '778px',
      cancelBtnTitle: 'No',
      saveBtnTitle: 'Yes',
      hideSave: false,
      hideOnSave: false,
      hideFooter: customFooter,
      data: {
        'isFor': isFor,
        'patientId': patientId,
        'apptEntryId': apptEntryId,
        'confirmationStatus': confirmationStatus,
        'status': status,
        'recurringData': recurringData,
        'isRecurringChange': isRecurringChange,
        'customFooter': customFooter
      }
    });
    return this.ref;
  }

}
