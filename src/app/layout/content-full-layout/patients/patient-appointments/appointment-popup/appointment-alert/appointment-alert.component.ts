import {Component, Input, OnInit} from '@angular/core';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {FormControl} from '@angular/forms';
import {IPatientAlert, PatientAppointmentService} from '../../../rest-service/app.patient-appointment.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-appointment-alert',
  templateUrl: './appointment-alert.component.html',
  styleUrls: ['./appointment-alert.component.css']
})
export class AppointmentAlertComponent implements OnInit {

  @Input()
  patientId: string;

  patientAlert: IPatientAlert;
  message = new FormControl('');

  constructor(
    private dialogRef: DialogRef,
    private alertService: AppAlertService,
    private patientAppointmentService: PatientAppointmentService
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    if (this.patientAlert.id) {
      this.patientAlert.message = this.message.value;
      this.patientAppointmentService.updatePatientAlert(this.patientAlert).subscribe(response => {
        this.dialogRef.close();
        this.alertService.displaySuccessMessage('Appoint alert updated successfully.');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Something went wrong. Try Again.');
      });
    } else {
      this.patientAppointmentService.patientAlert(this.patientId, this.message.value).subscribe(response => {
        this.dialogRef.close();
        this.alertService.displaySuccessMessage('Appoint alert saved successfully.');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Something went wrong. Try Again.');
      });
    }
  }

  private getData() {
    this.patientAppointmentService.getPatientAlerts(this.patientId).subscribe((response) => {
      this.patientAlert = response && response.length > 0 ? response[0] : {} as IPatientAlert;
      if (this.patientAlert.id) {
        this.message.setValue(this.patientAlert.message);
      }
    });
  }
}
