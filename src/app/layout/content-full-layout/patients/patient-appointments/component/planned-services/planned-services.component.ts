import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {BaseGridComponent} from '../../../../shared-content-full-layout/base-grid/base-grid.component';
import {IPlannedServiceList, PatientAppointmentService} from '../../../rest-service/app.patient-appointment.service';
import {AddEditPlannedServicesComponent} from '../add-edit-planned-services/add-edit-planned-services.component';
import {AppDialogService} from '../../../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-planned-services',
  templateUrl: './planned-services.component.html',
  styleUrls: ['./planned-services.component.css']
})
export class PlannedServicesComponent extends BaseGridComponent implements OnInit, OnChanges {
  moduleName = RP_MODULE_MAP.patients_basic_access;

  @Input()
  data: IPlannedServiceList[];
  @Input()
  patientId: string;
  @Output()
  load: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('deleteConfirmation', {static: false}) deleteConfirmation;

  constructor(
    private appDialogService: AppDialogService,
    private alertService: AppAlertService,
    private patientAppointmentService: PatientAppointmentService
  ) {
    super();
  }

  ngOnInit() {
    this.statestatus.filter = null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.hasOwnProperty('data')) {
      this.gridData = this.data;
      this.statestatus.filter = null;
      this.loadItemstatus();
    }
  }

  deletePlannedService(id: string) {
    const ref = this.appDialogService.open(this.deleteConfirmation, {
      title: 'Do you wish to delete this Planned Service?',
      width: '778px',
      height: '229px',
      hideOnSave: false,
      saveBtnTitle: 'Yes',
      cancelBtnTitle: 'No'
    });
    ref.clickSave.subscribe((value) => {
      this.patientAppointmentService.deletePlannedService(this.patientId, id).subscribe(response => {
        if (response) {
          this.alertService.displaySuccessMessage("Planned service deleted successfully.");
          ref.closeDialog();
          this.load.emit();
        }
      }, error => {
        this.alertService.displayErrorMessage(error);
      });
    });
  }

  editPlannedService(data: IPlannedServiceList) {
    const ref = this.appDialogService.open(AddEditPlannedServicesComponent, {
      title: 'Edit Planned Service',
      width: '660px',
      hideOnSave: false,
      hideOnCancel: false,
      hideFooter: true,
      data: {patientId: this.patientId, data: data}
    });
    ref.close.subscribe(() => {
      this.load.emit();
    });
  }

}
