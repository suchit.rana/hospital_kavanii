import {Component, Input, OnInit} from '@angular/core';
import {AppState} from '../../../../../../app.state';
import {StaffService} from '../../../../../../services/app.staff.service';
import {AppDialogService} from '../../../../../../shared/services/app-dialog.service';
import {AppointmentService} from '../../../../../../services/app.appointment.service';
import {TreatmentType} from '../../../../../../models/app.appointment.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IPlannedServiceList, PatientAppointmentService} from '../../../rest-service/app.patient-appointment.service';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-add-edit-planned-services',
  templateUrl: './add-edit-planned-services.component.html',
  styleUrls: ['./add-edit-planned-services.component.css']
})
export class AddEditPlannedServicesComponent implements OnInit {

  @Input()
  data: IPlannedServiceList;
  @Input()
  patientId: string;

  apperance = 'outline';
  practitionerList = [];
  serviceProductsList: any = [];
  serviceProductsOriginal: any = [];
  serviceProducts: any = [];
  formGroup: FormGroup = new FormGroup({
    id: new FormControl(''),
    practitionerId: new FormControl('', Validators.required),
    repeat: new FormControl(1),
    patientId: new FormControl(this.patientId),
    locationId: new FormControl(this.appState.selectedUserLocationId),
    serviceId: new FormControl('', Validators.required),
    notes: new FormControl(''),
    isUsed: new FormControl(false),
    isStatus: new FormControl(true),
  });

  constructor(
    private appState: AppState,
    private alertService: AppAlertService,
    private dialogService: AppDialogService,
    private staffService: StaffService,
    private appointmentService: AppointmentService,
    private patientAppointmentService: PatientAppointmentService,
    private dialogRef: DialogRef
  ) {
  }

  ngOnInit() {
    if (this.data) {
      this.formGroup.patchValue(this.data);
    }
    console.log(this.data, this.formGroup.value);
    this.getPractitioner();
    this.getProductAndServiceSearch();
    this.formGroup.get('patientId').setValue(this.patientId);
  }

  onSelectServiceProduct($event) {

  }

  clearOrRemoveSP() {
    this.formGroup.get('serviceId').setValue('');
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.upsert();
  }

  private getPractitioner() {
    this.staffService.getAllPractitionerSpecialityByLocationIdForCalendar(this.appState.selectedUserLocationId).subscribe(data => {
      data.forEach(value => {
        value.displayName = value.firstName + ' ' + value.lastName;
        value.id = value.practitionerId;
        value.photo = value.photo ? `data:image/jpeg;base64,${value.photo}` : null;
        value.practitionerName = value.nickName;
      });
      this.practitionerList = data;
    });
  }

  private getProductAndServiceSearch() {
    this.appointmentService.getProductAndServiceSearch(this.appState.selectedUserLocationId).subscribe((data) => {
      this.serviceProductsOriginal = data;
      this.prepareServiceNProducts();
    });
  }

  private prepareServiceNProducts() {
    let idInc = 1;
    this.serviceProductsOriginal.service.speciality.forEach(e => {
      const dd = {
        id: idInc,
        name: e.specialityName,
        _class: 'main-header',
      };
      idInc++;
      this.addToServiceNProductList(dd);
      e.category.forEach(d => {
        const dd = {
          id: idInc,
          name: d.categoryName,
          _class: 'sub-header-1'
        };
        idInc++;
        this.addToServiceNProductList(dd);
        d.serviceItems.forEach(f => {
          let price = f.standardPrice;
          const dd = {
            id: f.id,
            name: (f.productCode ? f.productCode + ' - ' : '') + f.name + '($' + price + ', ' + f.duration + ' min)',
            actualName: (f.productCode ? f.productCode + ' - ' : '') + f.name,
            actualDuration: f.duration,
            duration: f.duration,
            price: price,
            type: 0,
            _class: 'item-under-2'
          };
          this.addToServiceNProductList(dd);
          this.serviceProductsList.push(this.preparePSObj(f, TreatmentType.Service));
        });
      });
      this.serviceProducts = [...this.serviceProducts];
    });

  }

  private preparePSObj(d: any, type: TreatmentType) {
    return {
      treatementType: type,
      serviceId: d['id'],
      duration: d['duration'],
      price: d['salePrice'],
      tax: d.taxRate,
      isStatus: true
    };
  }

  private addToServiceNProductList(data: any) {
    this.serviceProducts.push(data);
  }

  private upsert() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      this.formGroup.updateValueAndValidity();
      return;
    }
    const data = this.formGroup.value;
    if (!data.id) {
      delete data.id;
      this.patientAppointmentService.plannedService(data).subscribe(response => {
        this.dialogRef.close();
        this.alertService.displaySuccessMessage('Planned service created successfully.');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Failed to update planned service.');
      });
    } else {
      this.patientAppointmentService.updatePlannedService(data).subscribe(response => {
        this.dialogRef.close();
        this.alertService.displaySuccessMessage('Planned service updated successfully.');
      }, error => {
        this.alertService.displayErrorMessage(error || 'Failed to update planned service.');
      });
    }
  }

}
