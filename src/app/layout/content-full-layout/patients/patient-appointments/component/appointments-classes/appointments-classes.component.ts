import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {
  IPatientAppointmentGrid,
  IPatientAppointmentRecurringData,
  PatientAppointmentService
} from '../../../rest-service/app.patient-appointment.service';
import {DxPopoverComponent} from 'devextreme-angular';
import {
  AppointmentConfirmationStatus,
  AppointmentStatus
} from '../../../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {AppoinmentDateUtils} from '../../../../appointment/AppoinmentDateUtils';
import {formatREString} from '../../../../../../app.component';
import {AppointmentType} from '../../../../../../enum/application-data-enum';
import {NavigationExtras, Router} from '@angular/router';
import {BaseGridComponent} from '../../../../shared-content-full-layout/base-grid/base-grid.component';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {AskedCancelledMissedReasonComponent} from '../../appointment-popup/asked-cancelled-missed-reason/asked-cancelled-missed-reason.component';
import {AppDialogService, IPopupAction} from '../../../../../../shared/services/app-dialog.service';
import {SAVE} from '../../../../../../shared/popup-component/popup-component.component';
import {RP_MODULE_MAP} from '../../../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-appointments-classes',
  templateUrl: './appointments-classes.component.html',
  styleUrls: ['./appointments-classes.component.css']
})
export class AppointmentsClassesComponent extends BaseGridComponent implements OnInit, OnChanges {
  moduleName = RP_MODULE_MAP.patients_basic_access;

  readonly TYPE_APPT = AppointmentType.APPOINMENT;
  readonly DELETED = AppointmentStatus.Deleted;
  readonly CANCEL = 'CANCEL';
  readonly MISSED = 'MISSED';
  readonly DELETE = 'DELETE';
  readonly GO_TO = 'GO_TO';
  readonly RESET = 'RESET';
  readonly EDIT_INVOICE = 'EDIT_INVOICE';

  @Input()
  data: IPatientAppointmentGrid[];
  @Input()
  patientId: string;
  @Input()
  recurringData: IPatientAppointmentRecurringData[];
  @Output()
  load: EventEmitter<any> = new EventEmitter<any>();

  clickedItem: IPatientAppointmentGrid;
  actionPopup: DxPopoverComponent;

  @ViewChild('pendingStatusPopup', {static: true}) pendingStatusPopup: DxPopoverComponent;
  @ViewChild('cancelledStatusPopup', {static: true}) cancelledStatusPopup: DxPopoverComponent;
  @ViewChild('missedStatusPopup', {static: true}) missedStatusPopup: DxPopoverComponent;
  @ViewChild('completedStatusPopup', {static: true}) completedStatusPopup: DxPopoverComponent;
  @ViewChild('deleteConfirmation', {static: false}) deleteConfirmation;

  constructor(
    protected router: Router,
    private cdr: ChangeDetectorRef,
    private alertService: AppAlertService,
    private appDialogService: AppDialogService,
    private patientAppointmentService: PatientAppointmentService,
  ) {
    super();
  }

  ngOnInit() {
    this.statestatus.filter = null;
    this.state.filter = null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('data')) {
      this.gridData = this.data;
      this.statestatus.filter = null;
      this.state.filter = null;
      this.loadItemstatus();
    }
  }

  openApptActionPopup(dataItem: IPatientAppointmentGrid, event: any) {
    this.clickedItem = dataItem;
    const status = dataItem.status;
    const confirmationStatus = dataItem.confirmationStatus;
    if (status == AppointmentStatus.Pending) {
      this.actionPopup = this.pendingStatusPopup;
    } else if (status == AppointmentStatus.Missed) {
      this.actionPopup = this.missedStatusPopup;
    } else if (status == AppointmentStatus.Cancelled) {
      this.actionPopup = this.cancelledStatusPopup;
    } else if (status == AppointmentStatus.Completed) {
      this.actionPopup = this.completedStatusPopup;
    }
    if (this.actionPopup) {
      this.actionPopup.target = event.target;
      this.actionPopup.instance.show();
    }
  }

  doApptClassAction(action: string) {
    if (!this.clickedItem) {
      if (this.actionPopup) {
        this.actionPopup.instance.hide();
      }
      return;
    }
    let status = this.clickedItem.status;
    const confirmationStatus = this.clickedItem.confirmationStatus;
    const recurringData = this.recurringData.find(x => x.appointmentsId == this.clickedItem.appointmentsId);
    let isRecurringChange = false;
    if (action == this.CANCEL) {
      status = AppointmentStatus.Cancelled;
      if (recurringData) {
        recurringData.recurrenceException = formatREString(recurringData.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(this.clickedItem.startDateUTC));
        delete recurringData.status;
        delete recurringData.type;
        isRecurringChange = true;
      }
      const ref = AskedCancelledMissedReasonComponent.open(this.appDialogService, 'CANCELLED', this.patientId, this.clickedItem.id, confirmationStatus, status, recurringData, isRecurringChange);
      ref.close.subscribe((value) => {
        if (value == SAVE) {
          this.load.emit();
        }
      });
      return;
    } else if (action == this.MISSED) {
      status = AppointmentStatus.Missed;
      const ref = AskedCancelledMissedReasonComponent.open(this.appDialogService, 'MISSED', this.patientId, this.clickedItem.id, confirmationStatus, status, recurringData, isRecurringChange);
      ref.close.subscribe((value) => {
        if (value == SAVE) {
          this.load.emit();
        }
      });
      return;
    } else if (action == this.DELETE) {
      status = AppointmentStatus.Deleted;
      if (recurringData) {
        recurringData.recurrenceException = formatREString(recurringData.recurrenceException, AppoinmentDateUtils.toUtcRecurrenceException(this.clickedItem.startDateUTC));
        delete recurringData.status;
        delete recurringData.type;
        isRecurringChange = true;
      }
      const ref = this.appDialogService.open(this.deleteConfirmation, {
        title: 'Do you wish to reset this Appointment?',
        width: '778px',
        height: '229px',
        hideOnSave: false,
        saveBtnTitle: 'Yes',
        cancelBtnTitle: 'No'
      });
      ref.clickSave.subscribe((value) => {
        this.update(this.patientId, this.clickedItem.id, confirmationStatus, status, recurringData, '', isRecurringChange, ref);
      });
      return;
    } else if (action == this.GO_TO) {
      let fromState: NavigationExtras = {
        state: {
          forGotoAppt: true,
          stateValues: JSON.stringify({}),
          appointmentData: {apptId: this.clickedItem.appointmentsId, date: this.clickedItem.startDateUTC}
        }
      };
      this.router.navigate(['appointment'], fromState);
      return;
    } else if (action == this.RESET) {
      status = AppointmentStatus.Pending;
    } else if (action == this.EDIT_INVOICE) {

    }
    if (this.actionPopup) {
      this.actionPopup.instance.hide();
    }
    this.update(this.patientId, this.clickedItem.id, confirmationStatus, status, recurringData, '', isRecurringChange);
  }

  getClassByStatus(status: AppointmentStatus, confirmationStatus: AppointmentConfirmationStatus): string {
    if (status == AppointmentStatus.Pending) {
      return 'pending';
    } else if (status == AppointmentStatus.Missed) {
      return 'missed';
    } else if (status == AppointmentStatus.Cancelled) {
      return 'cancelled';
    } else if (status == AppointmentStatus.Completed) {
      return 'completed';
    } else if (status == AppointmentStatus.Deleted) {
      return 'deleted';
    } else {
      return 'unknown';
    }
  }

  filterData(key: string) {
    if (key == 'totalBooking') {
      this.statestatus.filter = null;
      this.state.filter = null;
    } else if (key == 'completedBooking') {
      this.statestatus.filter = {
        logic: 'and',
        filters: [
          {field: 'status', operator: 'eq', value: AppointmentStatus.Completed}
        ]
      };
    } else if (key == 'missedBooking') {
      this.statestatus.filter = {
        logic: 'and',
        filters: [
          {field: 'status', operator: 'eq', value: AppointmentStatus.Missed}
        ]
      };
    } else if (key == 'cancelledBooking') {
      this.statestatus.filter = {
        logic: 'and',
        filters: [
          {field: 'status', operator: 'eq', value: AppointmentStatus.Cancelled}
        ]
      };
    } else if (key == 'futureBooking') {
      this.statestatus.filter = {
        logic: 'and',
        filters: [
          {field: 'startDateUTC', operator: 'gt', value: new Date()},
          {field: 'status', operator: 'neq', value: AppointmentStatus.Cancelled},
          {field: 'status', operator: 'neq', value: AppointmentStatus.Missed},
          {field: 'status', operator: 'neq', value: AppointmentStatus.Deleted},
        ]
      };
    } else if (key == 'completedBookingThisYear') {
      const date = new Date();
      this.statestatus.filter = {
        logic: 'and',
        filters: [
          {field: 'status', operator: 'eq', value: AppointmentStatus.Completed},
          {field: 'startDateUTC', operator: 'lte', value: new Date()},
          {field: 'startDateUTC', operator: 'gte', value: new Date(date.getFullYear(), 0, 1)},
        ]
      };
    }
    this.loadItemstatus();
  }

  private update(patientId: string, appointmentsEntryId: string, confirmationStatus: AppointmentConfirmationStatus, status: AppointmentStatus, recurringData: IPatientAppointmentRecurringData, statusReasonMessage: string, isRecurringChanged: boolean, ref?: IPopupAction) {
    this.patientAppointmentService.update(patientId, appointmentsEntryId, confirmationStatus, status, recurringData, statusReasonMessage, isRecurringChanged).subscribe(response => {
      this.load.emit();
      if (ref) {
        ref.closeDialog();
      }
      this.alertService.displaySuccessMessage('');
    }, error => {
      this.alertService.displaySuccessMessage(error || 'Failed to update status');
    });
  }
}
