import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from 'src/app/app.state';
import { PatientService } from 'src/app/services/app.patient.service';
import { TreatmentNotesService } from 'src/app/services/app.treatmentnotes.services';
import { DateAdapter, MatButtonToggleChange, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/shared/dt-format';
import { PatientCaseModel } from "src/app/models/app.patient.model";
import { StaffService } from 'src/app/services/app.staff.service';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import { TemplateRef } from "@angular/core";
import { DialogService } from "@progress/kendo-angular-dialog";
import { Location } from "@angular/common";
import { ImageSnippet, MessageType } from "src/app/models/app.misc";
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-patient-case',
  templateUrl: './patient-case.component.html',
  styleUrls: ['./patient-case.component.css'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
  ],
})
export class PatientCaseComponent  extends  BaseGridComponent implements OnInit {

  @Input() patientId: string;
  addCase: Boolean = false;
  lstgeneralCase: any[];
  nodata: boolean = false;
  public practitionerData: any[] = [];
  caseList:any[]=[];
  practitionerList: any[] = [];
  patientCaseParentId: number;
  patientCaseName: string;
  patientShow:Boolean = false;
  file: File;
  selectedFile?: ImageSnippet;
  getFile: any;
  percentDone: number;
  uploadSuccess: boolean;
  filename:any;

  constructor(
    protected router: Router,
    private http: HttpClient,
    public location: Location,
    public appState: AppState,
    private patientService: PatientService,
    private treatmentNotesService: TreatmentNotesService,
    public _route: ActivatedRoute,
    private staffService: StaffService,
    private dialogService: DialogService
  ) {
    super();
    //this.patientCaseParentId = ApplicationDataEnum.patientCaseParentId
  }

  caseForm: FormGroup = new FormGroup({
    casesName: new FormControl("", Validators.required),
    injuryDate:new FormControl(""),
    practitioner:new FormControl(""),
    notes:	new FormControl(""),
    referralprovider:new FormControl(""),
    number:	new FormControl(""),
    startDate:	new FormControl(""),
    endDate:	new FormControl(""),
    referredTo:	new FormControl(""),

  });

  ngOnInit() {
    console.log(this.addCase);
    this._route.params.subscribe((params) => {
      if (params.patientId) {
        this.patientId = params.patientId;
        console.log(this.patientId)
      }
    });
    this.populateLanding();
    // if(this.caseList.length == 0){
    //   this.nodata =
    // }
  }

  onClicKAddCase(){
    this.nodata = true;
    this.addCase = true;
  }

  populateLanding() {

    this.patientService
      .getapplicationdatacases(
        this.appState.selectedUserLocation.id,
        this.patientId
      )
      .subscribe((data) => {
        console.log("!!!!!!!!!!!",data)
        this.lstgeneralCase = data
        // data.forEach((element) => {
        //   console.log(element)
        //   this.patientCaseName = element.caseName;
        //   // this.lstgeneralCase = element;
        // })
        this.gridData = data
        console.log(this.patientCaseName)

      });

      this.staffService.getPractitioners().subscribe((practitionersData) => {
        this.practitionerList = practitionersData;
        if (this.appState.selectedUserLocation !== undefined) {
          const pList = this.practitionerList.filter(
            (x) => x.locationId === this.appState.selectedUserLocation.id
          );

          if (pList.length > 0) {
            const pData = {
              id: pList[0].id,
              firstName: pList[0].firstName,
              locationId: this.appState.selectedUserLocation.id,
              locationName: this.appState.selectedUserLocation.locationName,
            };

            this.practitionerData.push(pData);
            // this.practitionerDefault.push(pData);
          }
        }
      });
    // this.getTemplateNameList();
  }

  appIdPatientStatusHandler($event) {
    console.log($event)
    this.patientCaseName = $event;
  }

  dateOfBirthchange(e) {
    console.log(e)
    let years = 0;
    let months = 0;
    if (e !== null) {
      const currentDate = new Date();
      const dateSent = new Date(e);
      let dateCount = Math.floor(
        (Date.UTC(
          currentDate.getFullYear(),
          currentDate.getMonth(),
          currentDate.getDate()
        ) -
          Date.UTC(
            dateSent.getFullYear(),
            dateSent.getMonth(),
            dateSent.getDate()
          )) /
        (1000 * 60 * 60 * 24)
      );

      if (dateCount > 365) {
        const m = Math.floor(dateCount / 30);
        years = Math.floor(m / 12);
        dateCount = dateCount - years * 365;
      }

      if (dateCount > 30) {
        months = Math.floor(dateCount / 30);
      }
    }
    // this.caseForm
    //   .get('dobDisable')
    //   .patchValue(years + ' years ' + months + ' months');
  }

  saveCaseForm(){
    let PatientCaseModel: PatientCaseModel = this.caseForm.value;
    PatientCaseModel.patientId = this.patientId;
    PatientCaseModel.locationId = this.appState.selectedUserLocation.id
    PatientCaseModel.primaryPractitionerId = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
    console.log(PatientCaseModel)

    // this.patientService.createPatientCases(PatientCaseModel).subscribe(data => {
    //   console.log("DDDDDDDDDDDDD",data)
    //   this.patientService.sharedData = "Case created successfully";
    //   // this.displaySuccessMessage("Case created successfully ");
    //   this.addCase = false;
    //   this.patientShow = true
    //   this.caseForm.reset();
    //   this.router.navigate(['/patients/case']);
    // })

  }

  patientActiveChanged(event: MatButtonToggleChange) {
    if (event.value === 'active') {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }
    this.loadItems();
  }

  setActive() {
    this.setActiveFilter();
  }

  setInactive() {
    this.setInactiveFilter();
  }

  public showConfirmation(template: TemplateRef<any>) {
    this.dialogService.open({
      title: "Attach Referral",
      content: template,
      actionsLayout :"normal",
      actions: [{ text: "Cancel" }, { text: "Yes", primary: true }],
    });
  }

  // processFile(imageInput: any) {
  //   this.file = imageInput.files[0];
  //   const reader = new FileReader();
  //   reader.addEventListener('load', (event: any) => {
  //     this.selectedFile = new ImageSnippet(event.target.result, this.file);
  //     this.getFile = this.selectedFile.src;
  //     // this.itemType.fileImage = this.getFile;
  //   });
  //   reader.readAsDataURL(this.file);
  // }

  upload(files: File[]){
    console.log(files)
    this.filename=files[0].name
    //pick from one of the 4 styles of file uploads below
    this.uploadAndProgress(files);
  }

  basicUpload(files: File[]){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f))
    this.http.post('https://file.io', formData)
      .subscribe(event => {
        console.log('done')
      })
  }

  //this will fail since file.io dosen't accept this type of upload
  //but it is still possible to upload a file with this style
  basicUploadSingle(file: File){
    this.http.post('https://file.io', file)
      .subscribe(event => {
        console.log('done')
      })
    }

    uploadAndProgress(files: File[]){
      console.log(files)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f))

      this.http.post('https://file.io', formData, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          console.log("event",event)
          if (event.type === HttpEventType.UploadProgress) {
            this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.uploadSuccess = true;
          }
      });
    }

    //this will fail since file.io dosen't accept this type of upload
    //but it is still possible to upload a file with this style
    uploadAndProgressSingle(file: File){
      this.http.post('https://file.io', file, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.uploadSuccess = true;
          }
      });
    }

    downloadFile() {
      const downloadLink = document.createElement("a");
      const fileName =this.filename;
      downloadLink.href = this.filename;
      downloadLink.download = fileName;
      downloadLink.click();
    }
    deleteimage() {
      this.filename = null;
    }
}
