import { ApplicationDataEnum } from 'src/app/enum/application-data-enum';
import { ApplicationDataService } from 'src/app/services/app.applicationdata.service';
import { AuditTrailComponent } from './../audit-trail/audit-trail.component';
import { FormsComponent } from './../forms/forms.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { BaseItemComponent } from 'src/app/shared/base-item/base-item.component';
import { BusinessService } from 'src/app/services/app.business.service';
import { AppState } from 'src/app/app.state';
import { ActivatedRoute } from '@angular/router';
import { ImageUploadComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/image-upload/image-upload.component';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { PatientTab } from 'src/app/models/app.patient.model';
import { PatientContactComponent } from '../patient-contact/patient-contact.component';
import { PatientCaseComponent } from '../patient-case/patient-case.component';
import { PatientAppointmentsComponent } from '../patient-appointments/patient-appointments.component';
import { PatientTreatmentnotesComponent } from '../patient-treatmentnotes/patient-treatmentnotes.component';
import { PatientLettersComponent } from '../patient-letters/patient-letters.component';
import { PatientBillingComponent } from '../patient-billing/patient-billing.component';
import { PatientAlertrecallComponent } from '../patient-alertrecall/patient-alertrecall.component';
import { PatientCommmunicationsComponent } from '../patient-commmunications/patient-commmunications.component';
import { PatientDocumentsComponent } from '../patient-documents/patient-documents.component';
import { PatientDetailsComponent } from '../patient-details/patient-details.component';
import { MatTabChangeEvent } from '@angular/material';
import { PatientCasescontactsComponent } from '../patient-casescontacts/patient-casescontacts.component';
import { PatientService } from '../../../../services/app.patient.service';
import { MessageType } from '../../../../models/app.misc';
import * as _ from "lodash";
import {RouterState} from '../../../../shared/interface';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-edit-patient',
  templateUrl: './edit-patient.component.html',
  styleUrls: ['./edit-patient.component.css'],
})
export class EditPatientComponent extends BaseItemComponent implements OnInit {
  moduleName = RP_MODULE_MAP.patients;

  @ViewChild(ImageUploadComponent, { static: true })
  image: ImageUploadComponent;
  @BlockUI() blockUI: NgBlockUI;
  apperance = 'outline';
  addPatient = true;
  patientId: any;

  patientTabs: PatientTab[] = [
    {
      active: true,
      component: PatientDetailsComponent,
      title: 'Details',
      addUrl: '/patients/details',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    // {
    //   active: true,
    //   component: PatientContactComponent,
    //   title: 'Contacts',
    //   addUrl: '/patients/contacts',
    // },
    // {
    //   active: true,
    //   component: PatientCaseComponent,
    //   title: 'Case',
    //   addUrl: '/patients/case',
    // },
    {
      active: false,
      component: PatientCasescontactsComponent,
      title: 'Cases & Contacts',
      addUrl: '/patients/casescontacts',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    {
      active: false,
      component: PatientAppointmentsComponent,
      title: 'Appointments',
      addUrl: '/patients/appointments',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    {
      active: false,
      component: PatientTreatmentnotesComponent,
      title: 'Treatment Notes',
      addUrl: '/patients/treatmentnotes',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_treatment_notes
    },
    {
      active: false,
      component: PatientLettersComponent,
      title: 'Letters',
      addUrl: '/patients/letters',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_letters
    },
    {
      active: false,
      component: FormsComponent,
      title: 'Forms',
      addUrl: '/patients/forms',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    {
      active: false,
      component: PatientBillingComponent,
      title: 'Billing',
      addUrl: '/patients/billing',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_bills
    },
    {
      active: false,
      component: PatientAlertrecallComponent,
      title: 'Alert & Recall',
      addUrl: '/patients/alertrecall',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    {
      active: false,
      component: PatientCommmunicationsComponent,
      title: 'Commmunications',
      addUrl: '/patients/commmunications',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_basic_access
    },
    {
      active: false,
      component: PatientDocumentsComponent,
      title: 'Documents',
      addUrl: '/patients/documents',
      IsFrom: null,
      moduleName: RP_MODULE_MAP.patients_documents
    },
    // {
    //   active: false,
    //   component: AuditTrailComponent,
    //   title: 'Audit Trail',
    //   addUrl: '/patients/auditTrail',
    //   IsFrom: null
    // },
  ];

  message: any;
  type: MessageType;
  patientName: any = '';
  allTitles: any = [];

  constructor(
    public businessService: BusinessService,
    public appState: AppState,
    public location: Location,
    public _route: ActivatedRoute,
    private patientService: PatientService,
    public applicationDataService: ApplicationDataService,
  ) {
    super(location);
    this.patientService.getObservable().subscribe(data => {
      if (data) {
        this.message = JSON.parse(data).message;
        if (JSON.parse(data).type == 'success') {
          this.type = MessageType.success;
        }
        if (JSON.parse(data).type == 'error') {
          this.type = MessageType.error;
        }
        if (JSON.parse(data).type == 'warning') {
          this.type = MessageType.warning;
        }
        setTimeout(() => {
          this.message = '';
          this.type = MessageType.success;
        }, 5000);
      }
    })
  }

  ngOnInit() {
    this.appState.selectedTabState.next(0)
    this._route.params.subscribe((params) => {
      if (params.patientId) {
        this.blockUI.start();
        this.getAllTitle();
        this.addPatient = false;
        this.patientId = params.patientId;
      }
    });
    const state = history.state as RouterState;
    if (state.fromState) {
      this.appState.selectedTabState.next(state.data.tabIndex || 1);
      this.patientTabs[state.data.tabIndex || 1].active = true;
      this.patientTabs[state.data.tabIndex || 1].IsFrom = state;
      // this.patientService.getDataforReferralOrThirdParty(history.state)
    }
  }

  ngAfterViewInit(): void {

  }

  tabChanged(tabChangeEvent: MatTabChangeEvent) {
    console.log(tabChangeEvent)
    for (let index = 0; index < this.patientTabs.length; index++) {
      this.patientTabs[index].active = false;
    }
    this.patientTabs[tabChangeEvent.index].active = true;
    this.appState.selectedTabState.next(tabChangeEvent.index);
  }

  getAllTitle() {
    this.allTitles = [];
    this.applicationDataService
      .GetApplicationDataByCategoryId(
        ApplicationDataEnum.Title,
        this.appState.selectedUserLocation.id
      )
      .subscribe((data) => {
        this.allTitles = data;
        this.patientService.getPatientById(this.patientId).subscribe((data: any) => {
          let title = _.find(this.allTitles, { id: data.title })
          this.blockUI.stop();
          if (title != undefined) {
            if (title.categoryName && title.categoryName != undefined) {
              this.patientName = title.categoryName + '. ' + data.firstName + ' ' + data.lastName;
            }
          } else {
            this.patientName = data.firstName + ' ' + data.lastName;
          }
        }), (err) => {
          this.blockUI.stop();
          let responseHandle = {
            message: "Patient Not Found",
            type: "error"
          }
          this.patientService.publishSomeData(JSON.stringify(responseHandle))
        }
      });
  }
}
