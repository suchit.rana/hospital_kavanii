import {CommunicationType} from '../enum/communication-type';
import {
    CommunicationMobile,
    CommunicationStatus,
    EmailAddress,
    EmailAttachment,
    EmailSourceType
} from '../../../communications/model/communication.interface';

export interface CommunicationList {
    id: string,
    communicationType: CommunicationType;
    typeStr: string;
    createdDate: Date;
    createdDateStr: string;
    templateId: string;
    autoDeleteAfter: string;
    recipientBy: string;
    recipientName: string;
    subject: string;
    attachmentStr: string;
    sendBy: string;
    fromEmail: string;
    status: CommunicationStatus;
    statusStr: string;
    parentBusinessId: string,
    locationId: string,
    template: string,
    source: EmailSourceType,
    isSent: boolean,
    sentOn: Date,
    communicationRelatedTo: number,
    madeOfCommunication: number,
    noteStatus: number,
    patientId: string,
    categoriesId: string,
    messageBody: string,
    communicationEmail: EmailAddress[],
    communicationMobile: CommunicationMobile[],
    communicationAttachment: EmailAttachment[],

    autoDeleteDateStr: string;
}
