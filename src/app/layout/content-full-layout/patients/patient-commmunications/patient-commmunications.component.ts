import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DxPopoverComponent} from 'devextreme-angular';
import {AppDialogService} from 'src/app/shared/services/app-dialog.service';
import {AddNoteComponent} from '../../communications/component/list/add-note/add-note.component';
import {
    CampaignSendEmailComponent,
    CommunicationBulkEmailSms, PatientCommunication
} from '../../communications/component/campaign/create-campaign/campaign-send-email/campaign-send-email.component';
import {
    CampaignSendSMSComponent
} from '../../communications/component/campaign/create-campaign/campaign-send-sms/campaign-send-sms.component';
import {BaseGridComponent} from '../../shared-content-full-layout/base-grid/base-grid.component';
import {
    CommunicationGridComponent, DELETE, DOWNLOAD,
    EDIT,
    PREVIEW,
    PRINT
} from '../../communications/shared/communication-grid/communication-grid.component';
import {Success} from '../../../../app.component';
import {CommunicationList} from './interface/communication-list';
import {
    DeleteConfirmationDialogComponent
} from '../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppAlertService} from '../../../../shared/services/app-alert.service';
import {CommunicationService} from '../../communications/api/communication/communication.service';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
    selector: 'app-patient-commmunications',
    templateUrl: './patient-commmunications.component.html',
    styleUrls: ['./patient-commmunications.component.scss']
})
export class PatientCommmunicationsComponent extends BaseGridComponent implements OnInit {
    moduleName = RP_MODULE_MAP.patients_basic_access;

    @BlockUI() blockUI: NgBlockUI;

    @ViewChild('filterActionPopup', {static: true}) filterActionPopup: DxPopoverComponent;
    @ViewChild('communicationGrid', {static: true}) communicationGrid: CommunicationGridComponent;

    @Input() patientId: string;

    isDisplayClear: boolean;

    constructor(
        private appDialogService: AppDialogService,
        private alertService: AppAlertService,
        private campaignService: CommunicationService,
        ) {
        super();
    }

    ngOnInit() {
    }

    onSendEmailClick() {
        CampaignSendEmailComponent.open(this.appDialogService, {
            openFor: PatientCommunication,
            patientId: this.patientId
        }).close.subscribe(value => {
            if (value === Success) {
                this.communicationGrid.loadCommunication();
            }
        });
    }

    onSendSmsClick() {
        CampaignSendSMSComponent.open(this.appDialogService, {
            openFor: PatientCommunication,
            patientId: this.patientId
        }).close.subscribe(value => {
            if (value === Success) {
                this.communicationGrid.loadCommunication();
            }
        });

    }

    onAddNoteClick() {
        const dialogRef = AddNoteComponent.open(this.appDialogService, {patientId: this.patientId});
        dialogRef.close.subscribe(value => {
            if (value === Success) {
                this.communicationGrid.loadCommunication();
            }
        });
    }

    filter(event) {
        this.filterActionPopup.target = event.target;
        this.filterActionPopup.instance.show();
    }

    filterByDate() {
        this.communicationGrid.filterByDate(this.filterActionPopup);
    }

    filterByType() {
        this.communicationGrid.filterByType(this.filterActionPopup);
    }

    clearFilter() {
        this.isDisplayClear = false;
        this.communicationGrid.clearFilter();
    }

    doAction(data: { action: string, item: CommunicationList }) {
        switch (data.action) {
            case EDIT:
                const dialogRef = AddNoteComponent.open(this.appDialogService, {patientId: this.patientId, id: data.item.id});
                dialogRef.close.subscribe(value => {
                    if (value === Success) {
                        this.communicationGrid.loadCommunication();
                    }
                });
                break;
            case PREVIEW:
                break;
            case PRINT:
                break;
            case DELETE:
                const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
                    hideOnSave: false,
                    title: 'Do you wish to delete this Note ?'
                });
                ref.clickSave.subscribe(() => {
                    this.blockUI.start();
                    this.campaignService.delete(data.item.id).subscribe(() => {
                        this.alertService.displaySuccessMessage('Note Deleted successfully.');
                        this.communicationGrid.loadCommunication();
                        ref.closeDialog();
                        this.blockUI.stop();
                    }, () => {
                        this.alertService.displayErrorMessage('Failed to delete Note, please try again later');
                        this.blockUI.stop();
                    });
                });
                break;
            case DOWNLOAD:
                break;
        }
    }
}
