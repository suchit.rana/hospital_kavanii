import { ElementRef, Input, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BaseGridComponent } from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import { BillingSummary, InvoiceModel } from 'src/app/models/app.billing.model';
import { PaymentTypeModel } from 'src/app/models/app.settings.model';
import { SettingsService } from 'src/app/services/app.settings.service';
import { AppDialogService } from 'src/app/shared/services/app-dialog.service';
import { CreditManagerPopUpComponent } from '../../billing/credit-manager/credit-manager-pop-up.component';
import { BillingService } from '../../billing/billing.service';
import { AppAlertService } from 'src/app/shared/services/app-alert.service';
import { AppState } from 'src/app/app.state';
import { SAVE } from '../../../../shared/popup-component/popup-component.component';
import { DxPopoverComponent } from 'devextreme-angular';
import { TextValue } from '../../../../shared/interface';
import { DateFilterComponent } from '../../shared-content-full-layout/dilaog/date-filter/date-filter.component';
import { FilterListSelectionComponent } from '../../shared-content-full-layout/dilaog/filter-list-selection/filter-list-selection.component';
import { PractitionerSpecialityModel } from '../../../../models/app.staff.model';
import { IBubbleApptReferralPayer } from '../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import { GenerateStatementPopupComponent } from './dialog/generate-statement-popup/generate-statement-popup.component';

@Component({
    selector: 'app-patient-billing',
    templateUrl: './patient-billing.component.html',
    styleUrls: ['./patient-billing.component.scss'],
})
export class PatientBillingComponent extends BaseGridComponent implements OnInit {

    readonly FILTER_DATE = 'FILTER_DATE';
    readonly FILTER_STATUS = 'FILTER_STATUS';
    readonly FILTER_PAYER = 'FILTER_PAYER';
    readonly FILTER_PRACTITIONER = 'FILTER_PRACTITIONER';

    readonly MAIN = 'MAIN';
    readonly MERGE_INVOICE = 'MERGE_INVOICE';
    readonly ADD_INVOICE = 'ADD_INVOICE';

    @BlockUI() blockUI: NgBlockUI;

    @Input() patientId: string;

    @ViewChild('filterPopup', { static: false }) filterPopup: DxPopoverComponent;

    paymentTypes: PaymentTypeModel[];
    practitioners: PractitionerSpecialityModel[];
    invoiceStatus: TextValue[];
    invoicePayers: IBubbleApptReferralPayer[];

    invoices: InvoiceModel[] = [];

    nonVoidedInvoices: Array<InvoiceModel> = new Array<InvoiceModel>();
    voidedInvoices: Array<InvoiceModel> = new Array<InvoiceModel>();

    invoiceTab = true;

    billingSummary: BillingSummary;
    currentScreen: string = this.MAIN;
    appliedFilters: TextValue[] = [];

    constructor(
        protected appState: AppState,
        private appDialogService: AppDialogService,
        private alertService: AppAlertService,
        private settingsService: SettingsService,
        private billingService: BillingService
    ) {
        super();
    }

    ngOnInit() {
        this.blockUI.start();
        this.settingsService.getAllPaymentTypes().subscribe((types: any) => {
            this.paymentTypes = types;
            this.blockUI.stop();
        });
        this.getBillingData();
    }

    getBillingData() {
        this.loadInvoices();
        this.loadBillingSummary();
    }

    loadBillingSummary() {
        this.billingService.getPatientBillingSummary(this.appState.selectedUserLocationId, this.patientId).subscribe((res: any) => {
            this.billingSummary = res;
        });
    }

    loadInvoices() {
        this.blockUI.start();
        this.billingService.getAllPatientInvoiceByLocation(this.appState.selectedUserLocationId, this.patientId)
            .subscribe((res: InvoiceModel[]) => {
                this.invoices = res;
                this.voidedInvoices = this.invoices.filter(i => i.isVoid);
                this.nonVoidedInvoices = this.invoices.filter(i => !i.isVoid);
                this.blockUI.stop();
            });
    }

    onInvoiceTabClick() {
        this.invoiceTab = true;
    }

    onVoidTabClick() {
        this.invoiceTab = false;
    }

    onAddCreditClick() {
        const dialogRef = CreditManagerPopUpComponent.open(this.appDialogService, null, {
            isCredit: true,
            patientId: this.patientId,
            billingPage: false
        });
        dialogRef.close.subscribe((value) => {
            if (value === SAVE) {
                this.getBillingData();
            }
        });

    }

    generateInvoice() {
        const dialogRef = GenerateStatementPopupComponent.open(this.appDialogService);
        dialogRef.close.subscribe((value) => {
            if (value === SAVE) {
                this.getBillingData();
            }
        });
    }

    openFilterPopup(event) {
        this.filterPopup.target = event.target;
        this.filterPopup.instance.show();
    }

    filter(filter: string) {
        if (this.filterPopup) {
            this.filterPopup.instance.hide();
        }
        const filterExist = this.appliedFilters.find(f => f.value === filter);
        switch (filter) {
            case this.FILTER_DATE:
                DateFilterComponent.open(this.appDialogService).close.subscribe((result) => {
                    if (!filterExist && result.action === SAVE) {
                        this.appliedFilters.push({ text: this.getFilterName(filter), value: filter });
                    }
                });
                break;
            case this.FILTER_STATUS:
                FilterListSelectionComponent.open(this.appDialogService, this.invoiceStatus, {
                    valueField: 'id',
                    textField: 'paymentType'
                }).close.subscribe((result) => {
                    if (!filterExist && result.action === SAVE) {
                        this.appliedFilters.push({ text: this.getFilterName(filter), value: filter });
                    }
                });
                break;
            case this.FILTER_PAYER:
                FilterListSelectionComponent.open(this.appDialogService, this.invoicePayers, {
                    valueField: 'id',
                    textField: 'paymentType'
                }).close.subscribe((result) => {
                    if (!filterExist && result.action === SAVE) {
                        this.appliedFilters.push({ text: this.getFilterName(filter), value: filter });
                    }
                });
                break;
            case this.FILTER_PRACTITIONER:
                FilterListSelectionComponent.open(this.appDialogService, this.practitioners, {
                    valueField: 'id',
                    textField: 'paymentType'
                }).close.subscribe((result) => {
                    if (!filterExist && result.action === SAVE) {
                        this.appliedFilters.push({ text: this.getFilterName(filter), value: filter });
                    }
                });
                break;
        }
    }

    removeFilter(filter: TextValue, i: number) {
        this.appliedFilters.splice(i, 1);
    }

    private getFilterName(filter: string): string {
        switch (filter) {
            case this.FILTER_DATE:
                return 'Date';
            case this.FILTER_STATUS:
                return 'Status';
            case this.FILTER_PAYER:
                return 'Payer';
            case this.FILTER_PRACTITIONER:
                return 'Practitioner';
        }
    }
}
