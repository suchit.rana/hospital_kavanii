import {Component, OnInit} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../../shared/services/app-dialog.service';
import {TextValue} from '../../../../../../shared/interface';

@Component({
    selector: 'app-generate-statement-popup',
    templateUrl: './generate-statement-popup.component.html',
    styleUrls: ['./generate-statement-popup.component.scss']
})
export class GenerateStatementPopupComponent implements OnInit {

    static ref: IPopupAction;

    constructor() {
    }

    transactionTypes: TextValue[] = [
        {text: 'All transaction', value: 0},
        {text: 'Outstanding only', value: 1},
    ];

    includes: TextValue[] = [
        {text: 'Deleted invoice', value: 0},
        {text: 'Account Credit', value: 1},
    ];

    ngOnInit(): void {
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(GenerateStatementPopupComponent, {
                title: 'Generate Statement',
                width: '660px',
                height: '532px',
                saveBtnTitle: 'Generate',
                data: data
            }
        );
        return this.ref;
    }

}
