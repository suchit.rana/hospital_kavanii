import {ApplicationDataService} from './../../../../services/app.applicationdata.service';
import {ContactService} from 'src/app/services/app.contact.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {AppState} from 'src/app/app.state';
import {PatientService} from 'src/app/services/app.patient.service';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import {APP_DATE_FORMATS, AppDateAdapter} from 'src/app/shared/dt-format';
import {DocumentListModel, PatientCaseModel} from 'src/app/models/app.patient.model';
import {StaffService} from 'src/app/services/app.staff.service';
import {BaseGridComponent} from 'src/app/layout/content-full-layout/shared-content-full-layout/base-grid/base-grid.component';
import {DatePipe, Location} from '@angular/common';
import {ImageSnippet} from 'src/app/models/app.misc';
import {ApplicationDataEnum, CaseStatus, ReferralStatus} from 'src/app/enum/application-data-enum';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

export class Dte {
  date: Date;
  text: string;
  id: string = '0';
  isDateTemplate: boolean = true;

  constructor(date: Date) {
    this.date = date;
    this.text = date.toDateString();
  }
}

@Component({
  selector: 'app-patient-casescontacts',
  templateUrl: './patient-casescontacts.component.html',
  styleUrls: ['./patient-casescontacts.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
  ],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})

export class PatientCasescontactsComponent extends BaseGridComponent implements OnInit, AfterViewInit {
  moduleName = RP_MODULE_MAP.patients_basic_access;

  globalTableData: any = [];
  //date picker
  @ViewChild('dateInput', {static: false}) dateInputElem: ElementRef;
  @ViewChild('startDateInput', {static: false}) startDateInputElem: ElementRef;
  @ViewChild('endtDateInput', {static: false}) endDateInputElem: ElementRef;
  isCalendarOpen: boolean = false;
  isStartCalenderOpen: boolean = false;
  isEndCalenderOpen: boolean = false;
  currentView = 'day';
  public calendarPopUpMargin = {horizontal: -60, vertical: 10};
  firstDay: number = 1;
  // currentDate: Date = moment().toDate();
  // startDate: Date = moment().toDate();
  // endDate: Date = moment().toDate();
  currentDate: Date = null;
  startDate: Date = null;
  endDate: any = null;
  dateFormat: string = 'ddd, D MMM YYYY';
  currHvrDateStart: Date;
  currHvrDateend: Date;
  hoverDate: Date = new Date();
  selectedDate: Dte[];
  selectedEndDate: Dte[];
  selectedStartDate: Dte[];
  // defaultReferralOrThirdPartyFlag: boolean = false;
  defaultReferralFlag: boolean = false;
  defaultThirdPartyFlag: boolean = false;
  caseStatus = 0;
  expanded: boolean = false;
  RightSideList: any = [];
  LeftSideList: any = [];
  selectedDataIndex: number = 0;
  selectedData: any;
  isOpenDropDownList: boolean = false;
  selectedDateAction = null;
  add_case_label: string = 'ADD CASE';
  totalCaseUsed = 0;
  selectedFilesArray: any = [];
  @ViewChild('dropdownlist', {static: false}) public dropdownlist: any;
  @ViewChild('calendarInjury', {static: false}) injuryCalendar: ElementRef;
  dateActions = [
    {id: 0, name: 'indefinte', value: 'indefinte'},
    {id: 1, name: '1 Month', value: 1},
    {id: 2, name: '2 Months', value: 2},
    {id: 3, name: '3 Months', value: 3},
    {id: 4, name: '4 Months', value: 4},
    {id: 5, name: '5 Months', value: 5},
    {id: 6, name: '6 Months', value: 6},
    {id: 7, name: '7 Months', value: 7},
    {id: 8, name: '8 Month', value: 8},
    {id: 9, name: '9 Months', value: 9},
    {id: 10, name: '10 Months', value: 10},
    {id: 11, name: '11 Months', value: 11},
    {id: 12, name: '12 Months', value: 12},
    {id: 13, name: '18 Months', value: 18},
    {id: 14, name: '24 Months', value: 24},
  ];
  documentListModel: DocumentListModel;
  locationId: any;
  toCaseId: any;
  fromCaseId: any = [];
  whichPopup: any;
  mergecaseShow: boolean = false;
  openPopUp: boolean = false;
  popupHeaderTitle: any;
  popupBodyContent: any;
  popupBodyContent1: any;
  popupBodyContent2: any;
  popupBodyContent3: any;
  practitionerListSearch: any;
  @Output() appId: EventEmitter<number> = new EventEmitter<number>();
  filetype: string;
  errorMessage: string;
  files = [];
  isDeleteFlag: boolean = false;
  public filter: string;
  @ViewChildren('caseGrid') caseGrid: QueryList<ElementRef>;
  @ViewChildren('referralGrid') referralGrid: QueryList<ElementRef>;
  referralFlag: boolean = false;
  upload_title: string;
  setDefaultReferralFlag: boolean = false;
  setDefaultCaseFlag: boolean = false;
  addCase: Boolean = false;
  nodata: boolean = true;
  noReferralData: boolean = true;
  uploadSuccess: boolean;
  activedischargecasevisible: boolean;
  mergecasesvisible: boolean;
  isActiveDischargeDisabled: boolean = false;
  showReferralGrid: boolean = false;
  patientCaseParentId: number;
  percentDone: number;
  patientCaseName: string;
  patientId: string;
  caseId: string = null;
  referralGridPresent: string = '';
  file: File;
  caseForm: FormGroup;
  referralForm: FormGroup;
  selectedFile?: ImageSnippet;
  getFile: any;
  filename: any;
  practitionerData: any[] = [];
  practitionerDataSearch: any = [];
  referralSearch: any = [];
  thirdPartySearch: any = [];
  caseDropdownList: any[] = [];
  caseDropdownListSearch: any = [];
  practitionerList: any[] = [];
  referralAddList: any = [];
  referralContactsList: any = [];
  thirdPartyContactsList: any = [];
  @BlockUI() blockUI: NgBlockUI;
  patientCaseModel: PatientCaseModel;
  count = 0;
  referralStatusData: any = [];
  thirdPartyStatusData: any = [];
  editPopUp: boolean = false;
  isDetachFlag: boolean = false;
  cancelCase: boolean = false;
  mergeCaseShowFlag: boolean = false;
  errorMessageCaseNotSelect = null;
  errorMessageCaseNotSelectTo = null;
  fromstate: any;
  deleteImageFlag: boolean = false;
  contactCaseId: any;
  dischargeCaseText: any;
  showConfirmationPopUp: boolean = false;
  allTitles: any = [];
  isCalenderInjuryFocus: boolean = false;
  isCalenderStartFocus: boolean = false;
  isCalenderEndFocus: boolean = false;
  showMergeInfoFlag: boolean = false;
  maxDOBDate = new Date();

  @ViewChild('calendarOpenBtn', {static: false}) public anchor: ElementRef;
  @ViewChild('calendarOpenBtn1', {static: false}) public anchor1: ElementRef;
  @ViewChild('calendarOpenBtn2', {static: false}) public anchor2: ElementRef;
  @ViewChild('popup', {static: false, read: ElementRef}) public popup: ElementRef;
  @ViewChild('popup1', {static: false, read: ElementRef}) public popup1: ElementRef;
  @ViewChild('popup2', {static: false, read: ElementRef}) public popup2: ElementRef;
  fromState: any;
  stateValues: any;
  fromStateThirdParty: any;
  stateCaseValues: any;
  fromStateReferral: any;

  @HostListener('document:click', ['$event'])
  // @ViewChild('elseAddCase', { static: true, read: TemplateRef }) elseAddCase: TemplateRef<any>;
  // @ViewChild('elseAddCase', { static: false })
  // private template: TemplateRef<any>;

  public documentClick(event: any): void {
    if (!this.isCalenderInjuryFocus && !this.contains(event.target)) {
      this.toggleCalendarPopup(false, 'injuryDate');
    }
    if (!this.isCalenderStartFocus && !this.contains1(event.target)) {
      this.toggleCalendarPopup(false, 'startDate');
    }
    if (!this.isCalenderEndFocus && !this.contains2(event.target)) {
      this.toggleCalendarPopup(false, 'endDate');
    }
  }

  contains(target: any): boolean {
    if (this.anchor != undefined && this.popup != undefined) {
      return (
        this.anchor.nativeElement.contains(target) ||
        (this.popup ? this.popup.nativeElement.contains(target) : false)
      );
    }
  }

  contains1(target: any): boolean {
    if (this.anchor1 != undefined && this.popup1 != undefined) {
      return (
        this.anchor1.nativeElement.contains(target) ||
        (this.popup1 ? this.popup1.nativeElement.contains(target) : false)
      );
    }
  }

  contains2(target: any): boolean {
    if (this.anchor2 != undefined && this.popup2 != undefined) {
      return (
        this.anchor2.nativeElement.contains(target) ||
        (this.popup2 ? this.popup2.nativeElement.contains(target) : false)
      );
    }
  }

  subscribe: any;
  @Input() IsFromPage: any;

  constructor(
    public location: Location,
    public appState: AppState,
    private patientService: PatientService,
    private _route: ActivatedRoute,
    private staffService: StaffService,
    private renderer: Renderer2,
    protected router: Router,
    public contactService: ContactService,
    public applicationDataService: ApplicationDataService,
    private vref: ViewContainerRef,
    private cd: ChangeDetectorRef,
    private datePipe: DatePipe
  ) {

    super();
    if (this.router.getCurrentNavigation() != null) {
      this.fromState = this.router.getCurrentNavigation().extras.state.fromState;
      this.fromStateThirdParty = this.router.getCurrentNavigation().extras.state.fromStateThirdParty;
      this.fromStateReferral = this.router.getCurrentNavigation().extras.state.fromStateReferral;
      this.stateValues = (this.fromState || this.fromStateThirdParty) ? this.router.getCurrentNavigation().extras.state.stateValues : null;
      this.stateCaseValues = (this.fromStateThirdParty || this.fromStateReferral) ? JSON.parse(this.router.getCurrentNavigation().extras.state.stateCaseValues) : null;
    } else {
      this.fromState = history.state.fromState;
      this.fromStateThirdParty = history.state.fromStateThirdParty;
      this.fromStateReferral = history.state.fromStateReferral;
      this.stateValues = (this.fromState || this.fromStateThirdParty) ? history.state.stateValues : null;
      this.stateCaseValues = (this.fromStateThirdParty || this.fromStateReferral) ? JSON.parse(history.state.stateCaseValues) : null;
    }
  }

  initialLoaData(callback) {
    this.locationId = this.appState.selectedUserLocation.id;
    this.getReferralStatus();
    this.getThirdPartyStatus();
    this.getAllTitle();

    this.caseForm = this.initializeAddCaseFormGroup();
    this.referralForm = this.initializeReferralForm();
    this.activedischargecasevisible = false;
    this.mergecasesvisible = false;
    this.caseForm.controls['isdefault'].setValue(false);
    this._route.params.subscribe((params) => {
      if (params.patientId) {
        this.patientId = params.patientId;
      }
      this.setActiveFilter();
      this.populateLanding();
      this.getCaseFilterData();
    });
    this.getReferralandThirdPartyContact(function() {
      if (callback) {
        return callback();
      }
    });

  }

  ngOnInit() {
    this.initialLoaData(null);
    if (this.IsFromPage) {
        this.onAddCase();
        if (this.IsFromPage.fromState == 'patient-contact-referral') {
          this.showConfirmation('attach_referral', false);
        }
        if (this.IsFromPage.fromState == 'patient-contact-third-party') {
          this.showConfirmation('third_party', false);
        }
    }
    if ((this.fromStateThirdParty || this.fromStateReferral) && this.stateValues) {
      this.navigateCaseEdit(this.stateCaseValues);
      this.showConfirmation(this.fromStateThirdParty ? 'attach_payer' : 'attach_referral', true);
    }
  }

  initializeAddCaseFormGroup() {
    return new FormGroup({
      id: new FormControl(null),
      caseName: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      patientId: new FormControl(null),
      locationId: new FormControl(null),
      primaryPractitionerId: new FormControl(null),
      primaryPractitionerName: new FormControl(null),
      caseStatus: new FormControl(0),
      injuryDate: new FormControl(null),
      notes: new FormControl(null, Validators.maxLength(250)),
      isdefault: new FormControl(false),
      isSelectedDefault: new FormControl(false)
    });
  }

  initializeReferralForm() {
    return new FormGroup({
      id: new FormControl(''),
      type: new FormControl(null),
      startDate: new FormControl(null),
      endDate: new FormControl(null),
      practitionerReferredTo: new FormControl(null),
      practitionerReferredToName: new FormControl(null),
      notes: new FormControl(null, Validators.maxLength(250)),
      isDefault: new FormControl(false),
      referenceNo: new FormControl(null),
      status: new FormControl(null),
      contactId: new FormControl(null, Validators.required),
      index: new FormControl(null),
      count: new FormControl(0),
      ordered: new FormControl(0),
      documentId: new FormControl(null),
      selectDuration: new FormControl(null),
      isSelectedDefaultReferral: new FormControl(false),
      isSelectedDefaultThirdParty: new FormControl(false),
      hasImage: new FormControl(true)
    });
  }

  //referral status
  getReferralStatus() {
    this.referralStatusData.push({
      id: 0,
      name: 'Active'
    }, {
      id: 1,
      name: 'Completed'
    }, {
      id: 2,
      name: 'Expired'
    }, {
      id: 3,
      name: 'Rejected'
    });
  }

  //third party status
  getThirdPartyStatus() {
    this.thirdPartyStatusData.push({
      id: 0,
      name: 'Active'
    }, {
      id: 1,
      name: 'Completed'
    }, {
      id: 2,
      name: 'Expired'
    });
  }

  getAllTitle() {
    this.allTitles = [];
    this.applicationDataService
      .GetApplicationDataByCategoryIdAndLocationId(
        ApplicationDataEnum.Title,
        this.appState.selectedUserLocation.id
      )
      .subscribe((data) => {
        this.allTitles = data;
      }, (err => {
        console.log(err);
      }));
  }

  ngAfterViewInit() {
    //This code is for resizing the columns of sub-grid (referral-grid) of main grid (case-grid)
    this.resizeColumns();
  }


  //referral & third party contact
  getReferralandThirdPartyContact(callback?) {
    this.blockUI.start();
    this.contactService.getAllContact().subscribe((contactData) => {
      for (let index = 0; index < contactData.length; index++) {
        if (contactData[index].titleId != null && contactData[index].titleId != undefined) {
          let data = _.find(this.allTitles, {id: contactData[index].titleId});
          if (data != undefined) {
            if (data.categoryName != undefined) {
              contactData[index].name = data.categoryName + ' ' + contactData[index].firstName + ' ' + contactData[index].lastName;
            }
          }
        }
      }
      //referral list
      this.referralContactsList = contactData.filter(
        (x) => x.status === true && x.contactType === 1
      );
      this.referralSearch = this.referralContactsList;
      //third party list
      this.thirdPartyContactsList = contactData.filter(
        (x) => x.status === true && x.contactType === 2
      );
      this.thirdPartySearch = this.thirdPartyContactsList;
      this.blockUI.stop();
      // added referral set default
      if (history.state.referralId) {
        this.referralForm.controls['contactId'].setValue(history.state.referralId);
        this.referralForm.controls['contactId'].updateValueAndValidity();
      }
      // added third party set default
      if (history.state.thirdPartyId) {
        this.referralForm.controls['contactId'].setValue(history.state.thirdPartyId);
        this.referralForm.controls['contactId'].updateValueAndValidity();
      }
      if (callback) {
        return callback();
      }
    }, (err => {
      console.log(err);
    }));
  }

  getCaseFilterData() {
    this.blockUI.start();
    this.applicationDataService
      .getAllApplicationDataByLocation(
        this.locationId
      )
      .subscribe((data) => {
        this.blockUI.stop();
        let name = 'Case (Patient)';
        const appData = data.find((x) => x.categoryName === name);
        if (appData !== undefined) {
          this.caseDropdownList = data.filter(
            (x) => x.categoryId === appData.id && x.status === true
          );
        }
        for (let index = 0; index < this.caseDropdownList.length; index++) {
          for (let i = 0; i < this.caseDropdownList[index].applicationDataLocation.length; i++) {
            if (this.caseDropdownList[index].applicationDataLocation[i].locationId == this.locationId) {
              this.caseDropdownListSearch.push(this.caseDropdownList[index]);
            }
          }
        }
        for (let index = 0; index < this.caseDropdownList.length; index++) {
          this.caseDropdownList[index].checked = false;
        }
      }, (err => {
        console.log(err);
      }));
  }

  populateLanding() {
    this.staffService.getAllPractitonersByLocationId(this.locationId).subscribe((practitionersData) => {
      this.practitionerList = practitionersData;
      this.practitionerListSearch = this.practitionerList;
      this.practitionerData = practitionersData;
      this.practitionerDataSearch = practitionersData;
      this.fetchCaseListsForPatientByLocation();
      this.count = 0;
    }, (err => {
      console.log(err);
    }));
    // this.staffService.getPractitioners().subscribe((practitionersData) => {
    //   console.log(practitionersData)
    //   if (this.appState.selectedUserLocation !== undefined) {
    //     const pList = practitionersData.filter(
    //       (x) => x.locationId === this.locationId
    //     );
    //     if (pList.length > 0) {
    //       const pData = {
    //         id: pList[0].id,
    //         firstName: pList[0].firstName,
    //         locationId: this.locationId,
    //         locationName: this.appState.selectedUserLocation.locationName,
    //       };
    //       this.practitionerData.push(pData);
    //       this.practitionerDataSearch.push(pData)
    //       this.fetchCaseListsForPatientByLocation();
    //     }
    //   }
    // })
  }

  dateChanged(e, whichFieldDate) {
    if (whichFieldDate == 'injuryDate') {
      this.caseForm.controls['injuryDate'].setValue(this.caseForm.value.injuryDate.toISOString());
    }
    if (whichFieldDate == 'startDate') {
      this.referralForm.controls['startDate'].setValue(this.referralForm.value.startDate.toISOString());
    }
    if (whichFieldDate == 'endDate') {
      this.referralForm.controls['endDate'].setValue(this.referralForm.value.endDate.toISOString());
    }
  }

  fetchCaseListsForPatientByLocation() {
    this.patientService
      .getapplicationdatacases(
        this.locationId,
        this.patientId,
      )
      .subscribe((data) => {
        this.globalTableData = data;

        this.gridData = data;

        this.showConfirmationPopUp = false;
        for (let index = 0; index < this.gridData.length; index++) {
          this.gridData[index].checked = false;
          this.gridData[index].radioChecked = false;
          this.gridData[index].primaryPractitionerName = this.getPrimoryPractitionerName(this.gridData[index].primaryPractitionerId);
          this.gridData[index].caseStatusName = this.getMainTableStatusName(this.gridData[index].caseStatus);
          if (this.gridData[index].isdefault === true) {
            this.showConfirmationPopUp = true;
          }
        }
        this.nodata = false;
        this.mergecaseShow = false;
        if (this.gridData.length == 0) {
          this.nodata = true;
        }
        if (this.gridData.length > 1) {
          this.mergecaseShow = true;
        }
        let obj: any = [{
          field: 'createdDate',
          dir: 'desc'
        }];
        this.removeFilterCase();
        let event = {
          value: 'active'
        };
        this.caseActiveChanged(event);
        this.sortChangeCase(obj);
      }, (err => {
        console.log(err);
      }));
  }

  getStatusName(caseStatus: number) {
    if (caseStatus === 0) {
      return ReferralStatus.Active;
    } else if (caseStatus === 1) {
      return ReferralStatus.InActive;
    } else {
      return 'N/A';
    }
  }

  getMainTableStatusName(caseStatus: any) {
    if (caseStatus === 0) {
      return CaseStatus.Active;
    } else if (caseStatus === 1) {
      return CaseStatus.Discharge;
    } else if (caseStatus === 2) {
      return CaseStatus.Reopen;
    } else {
      return 'N/A';
    }
  }

  toggleReferralGridIcon() {
    if (this.referralGridPresent.indexOf('plus.png') > 0) {
      this.referralGridPresent = '/assets/minus.png';
      this.showReferralGrid = true;
    } else {
      this.referralGridPresent = '/assets/plus.png';
      this.showReferralGrid = false;
    }
  }

  caseActiveChanged(event: any) {
    if (event.value === 'active') {
      this.setActiveFilterCase();
    } else {
      this.setInactiveFilterCase();
    }
    this.loadCaseItems();
  }

  onAddCase() {
    this.addCase = true;
    this.caseForm.reset();
    this.referralForm.reset();
    this.caseId = '';
    this.add_case_label = 'ADD CASE';
    this.mergeCaseShowFlag = false;
    this.referralAddList = [];
    this.caseStatus = 0;
    this.count = 0;
    this.defaultReferralFlag = false;
    this.defaultThirdPartyFlag = false;
    this.startDate = null;
    this.endDate = null;
    this.currentDate = null;
    this.getFile = '';
    this.filename = null;
    this.filetype = null;
    this.uploadSuccess = false;
  }

  saveReferralForm() {
    //default referral
    if (this.referralForm.value.isSelectedDefaultReferral == null) {
      this.referralForm.value.isSelectedDefaultReferral = false;
    }
    if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
      if (this.defaultReferralFlag == true) {
        if (this.referralForm.value.isDefault == true) {
          if (this.referralForm.value.isSelectedDefaultReferral == true) {
            this.saveReferral();
          }
          if (this.referralForm.value.isSelectedDefaultReferral == false) {
            this.openPopups('defaultReferralSet');
          }
        } else {
          this.saveReferral();
        }
      } else {
        this.saveReferral();
      }
    }

    //default third party
    if (this.referralForm.value.isSelectedDefaultThirdParty == null) {
      this.referralForm.value.isSelectedDefaultThirdParty = false;
    }
    if (this.upload_title != 'Attach Referral' && this.upload_title != 'Edit Attached Referral') {
      if (this.defaultThirdPartyFlag == true) {
        if (this.referralForm.value.isDefault == true) {
          if (this.referralForm.value.isSelectedDefaultThirdParty == true) {
            this.saveReferral();
          }
          if (this.referralForm.value.isSelectedDefaultThirdParty == false) {
            this.openPopups('defaultReferralSet');
          }
        } else {
          this.saveReferral();
        }
      } else {
        this.saveReferral();
      }
    }
  }

  //two date diffrence
  GetDayDifferene(toDate, fromDate) {
    return (toDate.getDate() == fromDate.getDate() && toDate.getMonth() == fromDate.getMonth() && toDate.getFullYear() == fromDate.getFullYear());
  }


  saveReferral() {
    if (this.editPopUp == false) {
      if (new Date(this.referralForm.value.endDate).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
        this.referralForm.controls['status'].setValue(2);
      } else if (this.totalCaseUsed == this.referralForm.value.count) {
        this.referralForm.controls['status'].setValue(1);
      }
    }
    if (this.referralForm.controls['index'].value == null && this.referralForm.controls['index'].value == undefined) {
      this.count++;
      this.referralForm.controls['index'].setValue(this.count);
      this.referralForm.controls['index'].updateValueAndValidity();
    }

    if (this.referralForm.controls['count'].value == null && this.referralForm.controls['count'].value == undefined) {
      this.referralForm.controls['count'].setValue(0);
      this.referralForm.controls['count'].updateValueAndValidity();
    }
    if (this.referralForm.value.practitionerReferredTo == 'No Preference') {
      this.referralForm.value.practitionerReferredTo = null;
    }
    delete this.referralForm.value.practitionerReferredToName;
    let patientCaseModel = new PatientCaseModel();
    patientCaseModel.caseContacts.files = null;

    if (this.referralForm.value.id == null) {
      delete this.referralForm.value.id;
    }
    patientCaseModel.caseContacts = this.referralForm.value;
    let data = _.find(this.referralAddList, {index: this.referralForm.value.index});
    if (data != null && data != undefined) {
      if (this.editPopUp == false) {
        if (this.files.length != 0) {
          patientCaseModel.caseContacts.files = this.files;
        }
        this.referralAddList.push(patientCaseModel.caseContacts);
      }
      if (this.editPopUp == true) {
        patientCaseModel.caseContacts.files = this.files;
        this.referralAddList[data.index - 1] = patientCaseModel.caseContacts;
      }
    } else {
      if (this.files.length != 0) {
        patientCaseModel.caseContacts.files = this.files;
      }
      this.referralAddList.push(patientCaseModel.caseContacts);
    }
    console.log('###############', this.referralAddList, this.referralSearch);
    for (let index = 0; index < this.referralAddList.length; index++) {
      for (let i = 0; i < this.referralSearch.length; i++) {
        if (this.referralAddList[index].contactId == this.referralSearch[i].id) {
          console.log(this.referralSearch[i], this.referralAddList[index]);
          // this.referralSearch.splice(i, 1)
        }
      }
    }
    this.defaultReferralFlag = false;
    this.defaultThirdPartyFlag = false;
    for (let index = 0; index < this.referralAddList.length; index++) {
      if (this.referralAddList[index].type == 1) {
        if (this.referralAddList[index].isDefault == true) {
          this.defaultReferralFlag = true;
          this.referralForm.controls['isSelectedDefaultReferral'].setValue(true);
        }
      }
      if (this.referralAddList[index].type == 2) {
        if (this.referralAddList[index].isDefault == true) {
          this.defaultThirdPartyFlag = true;
          this.referralForm.controls['isSelectedDefaultThirdParty'].setValue(true);
        }
      }
    }
    // this.referralForm.controls['selectDuration'].setValue(this.dateActions[1])
    this.referralFlag = false;
  }

  saveCaseForm() {
    debugger
    if (this.caseForm.value.isSelectedDefault == null) {
      this.caseForm.value.isSelectedDefault = false;
    }
    if (this.showConfirmationPopUp == true) {
      if (this.caseForm.value.isdefault == true) {
        if (this.caseForm.value.isSelectedDefault == true) {
          this.saveForm();
        }
        if (this.caseForm.value.isSelectedDefault == false) {
          this.openPopups('defaultCaseSet');
        }
      } else {
        this.saveForm();
      }
    } else {
      this.saveForm();
    }
  }

  saveForm() {
    this.blockUI.start();
    let availableFile = false;
    let self = this;

    function ForLoop(i) {
      if (self.referralAddList && i < self.referralAddList.length) {
        if (self.referralAddList[i] && self.referralAddList[i].files && self.referralAddList[i].files.length != 0) {
          availableFile = true;
          self.uploadDocument(i, self.referralAddList[i].files);
        }
        ForLoop(i + 1);
      } else {
        setTimeout(() => {
          self.saveFinalCase();
        }, 5000);
      }
    }

    ForLoop(0);
    // if (!availableFile) {
    //   self.saveFinalCase();
    // }
  }

  saveFinalCase() {
    if (this.caseForm.value.caseStatus == null) {
      this.caseForm.value.caseStatus = 0;
    }
    let patientCaseModel: PatientCaseModel = this.caseForm.value;
    delete this.caseForm.value.primaryPractitionerName;
    if (this.caseForm.value.id == null) {
      delete this.caseForm.value.id;
    }
    delete this.caseForm.value.isSelectedDefault;
    Object.assign(patientCaseModel, this.caseForm.value);
    patientCaseModel.patientId = this.patientId;
    patientCaseModel.locationId = this.locationId;

    for (let index = 0; index < this.referralAddList.length; index++) {
      if (this.referralAddList[index].endDate == 'indefinte') {
        this.referralAddList[index].endDate = null;
      }
      delete this.referralAddList[index].selectDuration;
      delete this.referralAddList[index].isSelectedDefaultReferral;
      delete this.referralAddList[index].isSelectedDefaultThirdParty;
    }
    patientCaseModel = Object.assign(patientCaseModel, {
      caseContacts: this.referralAddList
    });
    if (this.caseId == null || this.caseId == undefined || this.caseId == '') {
      this.patientService.createPatientCases(patientCaseModel).subscribe((data) => {
        this.blockUI.stop();
        if (this.fromState) {
          const _data = JSON.parse(this.stateValues);
          _data.addEditScheduleData['caseId'] = data;
          this.stateValues = JSON.stringify(_data);
          let fromStateReferral: NavigationExtras = {
            state: {
              AppointmentTypeAdded: true,
              stateValues: this.stateValues
            }
          };
          this.router.navigate(['/appointment'], fromStateReferral)
        } else {
          let responseHandle = {
            message: 'Case created successfully',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          this.addCase = false;
          this.mergeCaseShowFlag = false;
          this.referralAddList = [];
          this.caseForm.reset();
          this.count = 0;
          this.filename = '';
          this.uploadSuccess = false;
          this.files = [];
          this.getFile = '';
          this.caseForm.controls['caseStatus'].setValue(0);
          this.fetchCaseListsForPatientByLocation();
        }
      }, (err) => {
        let responseHandle = {
          message: 'Failed to create Case, please try again later',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      });
    } else {
      this.patientService.modifyPatientCases(patientCaseModel).subscribe((data) => {
        this.blockUI.stop();
        if (this.fromStateThirdParty || this.fromStateReferral) {
          const _data = JSON.parse(this.stateValues);
          if (this.fromStateThirdParty) {
            _data.addEditScheduleData['payerId'] = data;
          } else if (this.fromStateReferral) {
            _data.addEditScheduleData['referralId'] = data;
          }
          this.stateValues = JSON.stringify(_data);
          let fromStateReferral: NavigationExtras = {
            state: {
              AppointmentTypeAdded: true,
              stateValues: this.stateValues
            }
          };
          this.router.navigate(['/appointment'], fromStateReferral)
        } else {
          let responseHandle = {
            message: 'Case updated successfully',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          this.addCase = false;
          this.mergeCaseShowFlag = false;
          this.referralAddList = [];
          this.caseForm.reset();
          this.caseForm.controls['caseStatus'].setValue(0);
          this.count = 0;
          this.filename = '';
          this.uploadSuccess = false;
          this.files = [];
          this.getFile = '';
          this.fetchCaseListsForPatientByLocation();
        }
      }, (err) => {
        let responseHandle = {
          message: 'Failed to update Case, please try again later',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      });
    }
  }

  navigateCaseEdit(data: any) {
    let dataItem = {
      dataItem: {
        id: data.id
      }
    };
    data.dataItem = dataItem;
    this.detailExpand(data.dataItem);
    setTimeout(() => {
      // if (this.expanded == true) {
      this.caseForm.controls['id'].setValue(data.id);
      this.caseForm.controls['caseName'].setValue(data.caseName);
      if (data.injuryDate != null) {
        this.caseForm.controls['injuryDate'].setValue(data.injuryDate);
        this.currentDate = data.injuryDate;
      }
      if (data.isdefault == true) {
        this.caseForm.controls['isSelectedDefault'].setValue(true);
      } else {
        this.caseForm.controls['isSelectedDefault'].setValue(false);
      }
      this.caseForm.controls['isdefault'].setValue(data.isdefault);
      this.caseForm.controls['locationId'].setValue(data.locationId);
      this.caseForm.controls['notes'].setValue(data.notes);
      this.caseForm.controls['patientId'].setValue(data.patientId);
      this.caseForm.controls['primaryPractitionerId'].setValue(data.primaryPractitionerId);
      this.caseForm.controls['primaryPractitionerName'].setValue(this.getPrimoryPractitionerName(data.primaryPractitionerId));
      this.caseForm.controls['caseStatus'].setValue(data.caseStatus);
      this.caseId = data.id;
      this.caseStatus = data.caseStatus;
      this.addCase = true;
      this.add_case_label = 'EDIT CASE';
      this.mergeCaseShowFlag = false;
      // }
    }, 500);

  }

  public showConfirmation(open_popup_name, isReset) {
    if (isReset == true) {
      this.referralForm.reset();
      this.startDate = null;
      this.endDate = null;
      this.files = [];
      this.filename = '';
      this.filetype = '';
      this.getFile = '';
      this.uploadSuccess = false;
      // this.startDate = moment().toDate()
      // this.endDate = moment().toDate()
      // this.referralForm.controls['startDate'].setValue(this.startDate.toISOString())
      // this.referralForm.controls['endDate'].setValue(this.endDate.toISOString())
      // this.referralForm.controls['selectDuration'].setValue(this.dateActions[1])
      this.referralForm.controls['isDefault'].setValue(false);
      this.referralForm.controls['isDefault'].updateValueAndValidity();
      this.referralForm.controls['status'].setValue(0);
      this.referralForm.controls['status'].updateValueAndValidity();
      this.referralForm.controls['status'].setValue(0);
      this.referralForm.controls['status'].updateValueAndValidity();
      this.editPopUp = false;
    }
    this.referralFlag = true;

    if (open_popup_name == 'attach_referral') {
      if (isReset == true) {
        this.upload_title = 'Attach Referral';
      } else {
        this.upload_title = 'Edit Attached Referral';
      }
      this.referralForm.controls['contactId'].setValidators(Validators.required);
      this.referralForm.controls['contactId'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredTo'].setValidators(Validators.required);
      this.referralForm.controls['practitionerReferredTo'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredToName'].setValidators(Validators.required);
      this.referralForm.controls['practitionerReferredToName'].updateValueAndValidity();

      this.referralForm.controls['type'].setValue(1);
      this.referralForm.controls['type'].updateValueAndValidity();
    } else {
      if (isReset == true) {
        this.upload_title = 'Attach Third Party';
      } else {
        this.upload_title = 'Edit Attached Third Party';
      }
      this.referralForm.controls['contactId'].setValidators([]);
      this.referralForm.controls['contactId'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredTo'].setValidators([]);
      this.referralForm.controls['practitionerReferredTo'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredToName'].setValidators([]);
      this.referralForm.controls['practitionerReferredToName'].updateValueAndValidity();
      this.referralForm.controls['type'].setValue(2);
      this.referralForm.controls['type'].updateValueAndValidity();
    }

    this.referralForm.controls['ordered'].setValue(0);
    this.referralForm.controls['ordered'].updateValueAndValidity();

  }

  getContactType(contactType: number) {
    return (contactType == 1) ? 'Referral' : 'Third Party';
  }

  getCaseStatusName(status: number) {
    if (status == 0) {
      return ReferralStatus.Active;
    } else if (status == 1) {
      return ReferralStatus.InActive;
    } else {
      return 'N/A';
    }
  }

  getReferralOrThirdPartyStatusName(status: number) {
    if (this.referralStatusData.length != 0 && status != null) {
      return this.referralStatusData.find((sttausData) => sttausData.id === status).name;
    }
  }

  getPrimoryPractitionerName(primaryPractitionerId: string) {
    if (this.practitionerData.length != 0 && primaryPractitionerId != null) {
      return this.practitionerData.find((practitioner) => practitioner.id === primaryPractitionerId).firstName;
    }
  }

  getPractitionerName(practitionerReferredTo: string) {
    if (practitionerReferredTo != 'No Preference') {
      if (this.practitionerList.length != 0 && practitionerReferredTo != null) {
        return this.practitionerList.find((practitioner) => practitioner.id === practitionerReferredTo).firstName;
      } else {
        return 'No Preference';
      }
    }
  }

  getReferralName(contactId: string) {
    if (this.referralContactsList.length != 0 && contactId != null) {
      return this.referralContactsList.find((referral) => referral.id === contactId).name;
    }
  }

  getThirdPartyOrganizationName(contactId: string) {
    if (this.thirdPartyContactsList.length != 0 && contactId != null) {
      return this.thirdPartyContactsList.find((thirdParty) => thirdParty.id === contactId).organisationName;
    }
  }

  referralDataStateChange(data: any) {
    this.resizeColumns();
  }

  resizeColumns() {
    let widths = this.caseGrid.map(th => th.nativeElement.offsetWidth);

    this.referralGrid.forEach((th, index) => {
      this.renderer.setStyle(
        th.nativeElement,
        'width',
        `${widths[index]}px`
      );
    });
  }

  // UPLOAD & DOWNLOAD FILES
  upload(files: File[]) {
    this.files = files;
    this.filename = this.files[0].name;
    this.filetype = this.files[0].type;
    if (this.filetype == 'image/jpeg' || this.filetype == 'image/png' || this.filetype == 'application/pdf' || this.filetype == 'image/bmp' || this.filetype == 'application/x-zip-compressed' || this.filetype == 'text/plain' || this.filetype.includes('document')) {
      const filess = Math.round((this.files[0].size / 1024));
      if (filess < 5096) {
        const reader = new FileReader();
        reader.addEventListener('load', (event: any) => {
          this.selectedFile = new ImageSnippet(event.target.result, this.files[0]);
          this.getFile = this.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
        });
        this.uploadSuccess = true;
        reader.readAsDataURL(this.files[0]);
      } else {
        this.errorMessage = 'This file exceeds the maximum size limit of 5MB.';
      }
    } else {
      this.errorMessage = 'Please upload a valid file, .exe and .dll file formats are not allowed';
    }
  }

  uploadDocument(index, selectedfile) {
    this.documentListModel = new DocumentListModel();
    if (this.caseForm.controls['caseName'].value != '' && this.caseForm.controls['caseName'].value != null && this.caseForm.controls['caseName'].value != undefined) {
      this.errorMessage = '';
      this.documentListModel.documentName = selectedfile[0].name;
      this.documentListModel.patientId = this.patientId;
      this.documentListModel.locationId = this.appState.selectedUserLocationId;
      this.documentListModel.fileTypes = selectedfile[0].type;
      this.documentListModel.size = selectedfile[0].size;
      this.documentListModel.streamedFileContent = this.getFile;
      if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
        this.documentListModel.documentTypes = 'Referrals';
      } else {
        this.documentListModel.documentTypes = '';
      }
      this.documentListModel.uploadedDate = new Date().toISOString();
      this.documentListModel.caseName = this.caseForm.controls['caseName'].value;
      this.documentListModel.note = 'New Referral';
      this.blockUI.start();
      this.patientService.uploadpatientdocument(this.documentListModel).subscribe((res) => {
          let responseHandle = {
            message: 'Your file has been successfully uploaded.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          this.uploadSuccess = true;
          this.referralAddList[index].documentId = res.id;
          delete this.referralAddList[index].files;
          this.referralForm.controls['documentId'].setValue(res.id);
          this.blockUI.stop();
        }, (err) => {
          console.log(err);
          this.errorMessage = 'Failed to upload the document. Max. upload size: 5 MB';
          this.blockUI.stop();
        }
      );
    } else {
      this.errorMessage = 'Please select Case Name For upload Document';
    }

  }

  //download uploaded file
  downloadFile() {
    if (this.referralForm.controls['documentId'].value != null && this.referralForm.controls['documentId'].value != undefined && this.referralForm.controls['documentId'].value != '') {
      this.blockUI.start();
      this.patientService.getpatientdocumentcontentbyid(this.patientId, this.referralForm.controls['documentId'].value)
        .subscribe((patientdocument) => {
          let streamedFileContent = patientdocument.streamedFileContent;
          let downloadimages = null;
          if (patientdocument.fileType != '') {
            if (patientdocument.fileType == 'image/jpeg') {
              let imagedata = new ImageSnippet(`data:image/jpeg;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType == 'image/png') {
              let imagedata = new ImageSnippet(`data:image/png;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType == 'application/pdf') {
              let imagedata = new ImageSnippet(`data:application/pdf;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType == 'image/bmp') {
              let imagedata = new ImageSnippet(`data:image/bmp;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType == 'application/x-zip-compressed') {
              let imagedata = new ImageSnippet(`data:application/x-zip-compressed;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType == 'text/plain') {
              let imagedata = new ImageSnippet(`data:text/plain;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            } else if (patientdocument.fileType.includes('document')) {
              let imagedata = new ImageSnippet(`data:text/plain;base64,${streamedFileContent}`, null);
              downloadimages = imagedata.src;
            }
            try {
              FileSaver.saveAs(downloadimages, this.filename);
              this.blockUI.stop();
            } catch (err) {
              console.log(err);
            }
          }
        }, (err => {
          console.log(err);
        }));
    } else {
      let downloadimages = null;
      if (this.filetype != '') {
        if (this.filetype == 'image/jpeg') {
          let imagedata = new ImageSnippet(`data:image/jpeg;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype == 'image/png') {
          let imagedata = new ImageSnippet(`data:image/png;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype == 'application/pdf') {
          let imagedata = new ImageSnippet(`data:application/pdf;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype == 'image/bmp') {
          let imagedata = new ImageSnippet(`data:image/bmp;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype == 'application/x-zip-compressed') {
          let imagedata = new ImageSnippet(`data:application/x-zip-compressed;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype == 'text/plain') {
          let imagedata = new ImageSnippet(`data:text/plain;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        } else if (this.filetype.includes('document')) {
          let imagedata = new ImageSnippet(`data:text/plain;base64,${this.getFile}`, null);
          downloadimages = imagedata.src;
        }
        try {
          FileSaver.saveAs(downloadimages, this.filename);
        } catch (err) {
          console.log(err);
        }
      }
    }
  }

  //delete document
  deleteUploadedFile() {
    if (this.referralForm.controls['documentId'].value != null && this.referralForm.controls['documentId'].value != undefined && this.referralForm.controls['documentId'].value != '') {
      this.patientService.deletepatientdocumentsbyid(this.patientId, this.referralForm.controls['documentId'].value).subscribe(response => {
        if (response) {
          this.deleteImageFlag = false;
          this.filetype = '';
          this.filename = '';
          this.uploadSuccess = false;
          this.files = [];
          this.getFile = '';
          delete this.referralAddList[this.selectedDataIndex].files;
          this.referralForm.controls['documentId'].setValue(null);
          let responseHandle = {
            message: 'Your Document has been successfully deleted.',
            type: 'success'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
        }
      }, (err => {
        console.log(err);
      }));
    } else {
      this.deleteImageFlag = false;
      this.filetype = '';
      this.filename = '';
      this.uploadSuccess = false;
      this.getFile = '';
      this.files = [];
      delete this.referralAddList[this.selectedDataIndex].files;
      this.referralForm.controls['documentId'].setValue(null);
      let responseHandle = {
        message: 'Your Document has been successfully deleted.',
        type: 'success'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    }
  }

  //default referral pop-up open
  defaultValueChange(event, from) {
    if (from == 'referral') {
      this.referralForm.controls['isDefault'].setValue(event.checked);
    }
    if (from == 'case') {
      this.caseForm.controls['isdefault'].setValue(event.checked);
    }
  }

  //edit referral
  editReferral(data) {
    this.selectedData = data;
    this.selectedDataIndex = data.index;
    let open_popup_name = null;
    this.editPopUp = true;
    this.referralForm.controls['isSelectedDefaultReferral'].setValue(false);
    this.referralForm.controls['isSelectedDefaultThirdParty'].setValue(false);
    if (data.type == 1) {
      open_popup_name = 'attach_referral';
      if (data.isDefault == true) {
        this.referralForm.controls['isSelectedDefaultReferral'].setValue(true);
      }
    }
    if (data.type == 2) {
      open_popup_name = 'third_party';
      if (data.isDefault == true) {
        this.referralForm.controls['isSelectedDefaultThirdParty'].setValue(true);
      }
    }
    this.contactCaseId = data.id;
    this.referralForm.controls['id'].setValue(data.id);
    this.referralForm.controls['type'].setValue(data.type);
    this.referralForm.controls['startDate'].setValue(data.startDate);
    this.startDate = data.startDate;
    if (data.endDate == 'indefinte') {
      this.referralForm.controls['endDate'].setValue('indefinte');
      this.endDate = 'indefinte';
    } else {
      this.referralForm.controls['endDate'].setValue(data.endDate);
      this.endDate = data.endDate;
    }
    if (data.selectDuration) {
      this.referralForm.controls['selectDuration'].setValue(data.selectDuration);
    }
    this.referralForm.controls['practitionerReferredTo'].setValue(data.practitionerReferredTo);
    if (data.practitionerReferredTo != 'No Preference') {
      this.referralForm.controls['practitionerReferredToName'].setValue(this.getPractitionerName(data.practitionerReferredTo));
    } else {
      this.referralForm.controls['practitionerReferredToName'].setValue('No Preference');
    }
    this.referralForm.controls['notes'].setValue(data.notes);
    this.referralForm.controls['isDefault'].setValue(data.isDefault);
    this.referralForm.controls['referenceNo'].setValue(data.referenceNo);
    this.referralForm.controls['status'].setValue(data.status);
    this.referralForm.controls['contactId'].setValue(data.contactId);
    this.referralForm.controls['count'].setValue(data.count);
    this.referralForm.controls['ordered'].setValue(data.ordered);

    this.referralForm.controls['index'].setValue(data.index);
    if (data.files && data.files.length != 0) {
      this.filename = data.files[0].name;
      this.filetype = data.files[0].type;
      const reader = new FileReader();
      reader.addEventListener('load', (event: any) => {
        this.selectedFile = new ImageSnippet(event.target.result, this.files[0]);
        this.getFile = this.selectedFile.src.replace('data:', '').replace(/^.+,/, '');
      });
      reader.readAsDataURL(this.files[0]);
      this.uploadSuccess = true;
    } else {
      this.filename = '';
      this.filetype = '';
      this.uploadSuccess = false;
      this.getFile = '';
    }
    if (data.documentId != null && data.documentId != undefined && data.documentId != '') {
      this.referralForm.controls['documentId'].setValue(data.documentId);
      // this.uploadSuccess = true;
    }
    if (data.documentName != null && data.documentName != undefined && data.documentName != '') {
      this.filename = data.documentName;
      this.uploadSuccess = true;
    }
    this.showConfirmation(open_popup_name, false);
  }

  //table expand collapse event for sub table show
  isCaseDetailData(dataItem: any, index: number) {
    return dataItem.isAnyCasesContactsExits == true;
  }

  //expand
  detailExpand(event) {
    this.blockUI.start();
    this.patientService
      .getCaseDetailbyCaseId(
        event.dataItem.id,
        this.patientId
      )
      .subscribe((data) => {
        this.gridData = data;
        // this.expanded = true;
        for (let index = 0; index < this.gridData.length; index++) {
          this.gridData[index].checked = false;
          this.gridData[index].radioChecked = false;
        }
        this.nodata = false;
        if (this.gridData.length == 0) {
          this.nodata = true;
        }
        if (this.gridView) {
          for (let index = 0; index < this.gridView.data.length; index++) {
            if (this.gridView.data[index].id == event.dataItem.id) {
              this.gridView.data[index].caseContacts = this.gridData.caseContacts;
            }
          }
        }
        if (this.gridData.caseContacts) {
          this.referralAddList = [];
          this.defaultReferralFlag = false;
          this.defaultThirdPartyFlag = false;
          this.referralForm.controls['isSelectedDefaultReferral'].setValue(false);
          this.referralForm.controls['isSelectedDefaultThirdParty'].setValue(false);
          for (let index = 0; index < this.gridData.caseContacts.length; index++) {
            this.gridData.caseContacts[index].index = index + 1;
            this.count++;
            if (this.gridData.caseContacts[index].type == 1) {
              if (this.gridData.caseContacts[index].isDefault == true) {
                this.defaultReferralFlag = true;
                this.referralForm.controls['isSelectedDefaultReferral'].setValue(true);
              }
            }
            if (this.gridData.caseContacts[index].type == 2) {
              if (this.gridData.caseContacts[index].isDefault == true) {
                this.defaultThirdPartyFlag = true;
                this.referralForm.controls['isSelectedDefaultThirdParty'].setValue(true);
              }
            }
            if (this.gridData.caseContacts[index].endDate == 'indefinte') {
              this.gridData.caseContacts[index].endDate = 'indefinte';
            }
          }
          this.referralAddList = this.gridData.caseContacts;
        } else {
          this.referralAddList = [];
        }
        this.blockUI.stop();
      }, (err => {
        console.log(err);
      }));
  }

  //collapse
  detailCollapse(event) {
  }

  //merge case open
  goToMergeCase() {
    this.addCase = false;
    this.mergeCaseShowFlag = true;
    this.showMergeInfoFlag = false;
    this.LeftSideList = [];
    this.RightSideList = [];
    for (let index = 0; index < this.globalTableData.length; index++) {
      this.globalTableData[index].checked = false;
      this.globalTableData[index].radioChecked = false;
    }
    this.LeftSideList = this.globalTableData;
    this.RightSideList = this.globalTableData;
    this.errorMessageCaseNotSelect = null;
    this.errorMessageCaseNotSelectTo = null;
  }

  //merge case
  mergeCases() {
    this.toCaseId = null;
    this.fromCaseId = [];
    for (let index = 0; index < this.globalTableData.length; index++) {
      if (this.globalTableData[index].radioChecked == true) {
        this.toCaseId = this.globalTableData[index].id;
        this.errorMessageCaseNotSelectTo = null;
      }
      if (this.globalTableData[index].checked == true) {
        this.fromCaseId.push(this.globalTableData[index].id);
      }
    }
    this.errorMessageCaseNotSelect = null;
    this.errorMessageCaseNotSelectTo = null;
    if (this.fromCaseId.length == 0) {
      this.errorMessageCaseNotSelectTo = 'Please select Case To Merge';
      return;
    }
    if (this.toCaseId == null) {
      this.errorMessageCaseNotSelect = 'Please select Case For Merge';
      return;
    }
    this.openPopups('mergeCase');
  }

  //cancel merge case
  goBackToListing() {
    this.mergeCaseShowFlag = false;
    this.addCase = false;
  }

  //radio Change
  radioChange(i) {
    for (let index = 0; index < this.RightSideList.length; index++) {
      this.RightSideList[index].radioChecked = false;
      if (index == i) {
        this.RightSideList[index].radioChecked = true;
        this.errorMessageCaseNotSelect = null;
      }
    }
  }

  //checkbox change
  checkboxChange() {
    this.showMergeInfoFlag = false;
    this.RightSideList = [];
    for (let index = 0; index < this.LeftSideList.length; index++) {
      if (this.LeftSideList[index].checked == true) {
        this.showMergeInfoFlag = true;
        this.errorMessageCaseNotSelectTo = null;
      } else {
        this.RightSideList.push(this.LeftSideList[index]);
      }
    }
    // let data = _.find(this.LeftSideList,{checked : true})
    // if(data){
    //   this.showMergeInfoFlag = true;
    // }
  }

  //filter case name
  handleFilterCase(event: any) {
    if (event.target.value != '') {
      this.caseDropdownListSearch = this.caseDropdownList.filter(
        (s) => s.categoryName.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.caseDropdownListSearch = this.caseDropdownList;
    }
  }

  //filter primary practitioner
  handleFilterPractitioner(event: any) {
    if (event.target.value != '') {
      this.practitionerDataSearch = this.practitionerData.filter(
        (s) => s.firstName.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.practitionerDataSearch = this.practitionerData;
    }
  }

  //filter referredto
  handleFilterReferredTo(event: any) {
    if (event.target.value != '') {
      this.practitionerListSearch = this.practitionerList.filter(
        (s) => s.firstName.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.practitionerListSearch = this.practitionerList;
    }
  }

  //referral provider filter
  handleFilterReferralProvider(event: any) {
    if (event != '') {
      this.referralSearch = this.referralContactsList.filter(
        (s) => s.name.toLowerCase().indexOf(event.toLowerCase()) !== -1
      );
    } else {
      this.referralSearch = this.referralContactsList;
    }
    this.filter = event;
  }

  //hird party provider filter
  handleFilterThirdPartyProvider(event: any) {
    if (event != '') {
      this.thirdPartySearch = this.thirdPartyContactsList.filter(
        (s) => s.firstName.toLowerCase().indexOf(event.toLowerCase()) !== -1
      );
    } else {
      this.thirdPartySearch = this.thirdPartyContactsList;
    }
    this.filter = event;
  }

  handleChange(event: any) {
    // this.appId.emit(event);
  }

  onKendoDropDownOpen(controlName: string) {
  }

  //add new patient-contact-referral
  addNewPatientContactReferral() {
    let obj = {
      fromState: 'patient-contact-referral',
      patientId: this.patientId
    };
    let fromState: NavigationExtras = {
      state: {
        fromState: JSON.stringify(obj),
      }
    };
    this.router.navigate(['contacts'], fromState);
  }

  //add new patient-contact-third-party
  addNewPatientContactThirdParty() {
    let obj = {
      fromState: 'patient-contact-third-party',
      patientId: this.patientId
    };
    let fromState: NavigationExtras = {
      state: {
        fromState: JSON.stringify(obj),
      }
    };
    this.router.navigate(['contacts'], fromState);
  }

  //show name in dropdown
  selectedReferredTo(obj) {
    if (obj == 'No Preference') {
      this.referralForm.controls['practitionerReferredToName'].setValue('No Preference');
      this.referralForm.controls['practitionerReferredToName'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredTo'].setValue('No Preference');
      this.referralForm.controls['practitionerReferredTo'].updateValueAndValidity();
    } else {
      this.referralForm.controls['practitionerReferredToName'].setValue(obj.firstName);
      this.referralForm.controls['practitionerReferredToName'].updateValueAndValidity();
      this.referralForm.controls['practitionerReferredTo'].setValue(obj.id);
      this.referralForm.controls['practitionerReferredTo'].updateValueAndValidity();
    }
  }

  selectedClient(obj) {
    this.caseForm.controls['primaryPractitionerName'].setValue(obj.firstName);
    this.caseForm.controls['primaryPractitionerName'].updateValueAndValidity();
    this.caseForm.controls['primaryPractitionerId'].setValue(obj.id);
    this.caseForm.controls['primaryPractitionerId'].updateValueAndValidity();
  }

  //open pop-ups
  openPopups(whichPopup) {
    this.whichPopup = whichPopup;
    if (whichPopup == 'dischargeCase') {
      if (this.caseStatus == 0 || this.caseStatus == 2) {
        this.popupHeaderTitle = 'Do you wish to Discharge this Case?';
        this.popupBodyContent = 'Would you like to leave a note?';
      }
      if (this.caseStatus == 1) {
        this.popupHeaderTitle = 'Do you wish to Re-Open this Case?';
        this.popupBodyContent = 'Would you like to leave a note?';
      }
    }
    if (whichPopup == 'cancelCase') {
      this.popupHeaderTitle = 'Do you wish to Cancel this Case Contact?';
      this.popupBodyContent = 'Do you want to Cancel Adding Case Contact Notes?';
    }
    if (whichPopup == 'deleteCase') {
      this.popupHeaderTitle = 'Do you wish to Delete this Case?';
      this.popupBodyContent = 'Deleted Case cannot be restored.';
    }
    if (whichPopup == 'defaultCaseSet') {
      this.popupHeaderTitle = 'Do you wish to make this default Case?';
      this.popupBodyContent = 'Another Case is already marked as default for this Patient. Do you want to set this as default Case?';
    }
    if (whichPopup == 'defaultReferralSet') {
      if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
        this.popupHeaderTitle = 'Do you wish to make this default Referral?';
        this.popupBodyContent = 'Another Referral is already marked as default for this Case. Do you want to set this as default Referral?';
      } else {
        this.popupHeaderTitle = 'Do you wish to make this default Third Party?';
        this.popupBodyContent = 'Another Third Party is already marked as default for this Case. Do you want to set this as default Third Party?';
      }
    }
    if (whichPopup == 'detachReferral') {
      if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
        this.popupHeaderTitle = 'Do you wish to Detach this Referral?';
        this.popupBodyContent = 'Detached referral cannot be restored.';
      } else {
        this.popupHeaderTitle = 'Do you wish to Detach this Third Party?';
        this.popupBodyContent = 'Detached third Party cannot be restored.';
      }
    }
    if (whichPopup == 'deleteImage') {
      this.popupHeaderTitle = 'Do you wish to Delete Uploaded Document?';
      this.popupBodyContent = 'Do you want to Delete this Uploaded Document?';
    }
    if (whichPopup == 'mergeCase') {
      this.popupHeaderTitle = 'Do you wish to Merge these Cases?';
      this.popupBodyContent = 'This is permanent action and cannot be undone.';
    }
    if (whichPopup == 'detachReferralOrThirdPartyError') {
      if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
        this.popupHeaderTitle = 'Referral cannot be detached';
        this.popupBodyContent = 'You cannot detach this referral from this case, as it is already linked to this patient\'s appointment.';
      } else {
        this.popupHeaderTitle = 'Third Party cannot be detached';
        this.popupBodyContent = 'You cannot detach this third party from this case, as it is already linked to this patient\'s appointment.';
      }
    }
    if (whichPopup == 'caseDeleteError') {
      this.popupHeaderTitle = 'Case cannot be deleted';
      this.popupBodyContent = 'You cannot delete this case, as it is already linked to this patient\'s appointment(s) or treatment note(s) or letter(s).';
    }
    if (whichPopup == 'dischargeCaseError') {
      if (this.caseStatus == 0 || this.caseStatus == 2) {
        this.popupHeaderTitle = 'Case cannot be discharged';
        this.popupBodyContent = 'Please complete the following pending actions to discharge this case ';
        this.popupBodyContent1 = '1 Appointment(s) not completed yet.';
        this.popupBodyContent2 = '2 Treatment note(s) not completed yet.';
        this.popupBodyContent3 = '3 Letter(s) not completed yet.';
      }
      if (this.caseStatus == 1) {
        this.popupHeaderTitle = 'Case cannot be Re-Opened';
        this.popupBodyContent = 'Please complete the following pending actions to Re-Opened this case ';
        this.popupBodyContent1 = '1 Appointment(s) not discharged yet.';
        this.popupBodyContent2 = '2 Treatment note(s) not discharged yet.';
        this.popupBodyContent3 = '3 Letter(s) not discharged yet.';
      }
    }
    this.openPopUp = true;
  }

  //close pop-ups
  closePopups(whichPopup) {
    if (whichPopup == 'referralAdd') {
      this.referralFlag = false;
      this.editPopUp = false;
    }
    if (whichPopup == 'defaultReferralSet') {
      this.referralFlag = false;
      this.referralForm.controls['isDefault'].setValue(false);
      if (this.editPopUp == true) {
        for (let index = 0; index < this.referralAddList.length; index++) {
          if (this.referralAddList[index].index == this.selectedDataIndex) {
            this.referralAddList[index].isDefault = false;
            this.referralAddList[index] = this.selectedData;
          }
        }
        this.saveReferral();
      } else {
        this.saveReferral();
      }
    }
    if (whichPopup == 'defaultCaseSet') {
      this.caseForm.controls['isdefault'].setValue(false);
      this.saveForm();
    }
    this.openPopUp = false;
  }

  //on confirm yes action perform
  performSomeActionOnYes(whichPopup) {
    if (whichPopup == 'dischargeCase') {
      if (this.dischargeCaseText != '' && this.dischargeCaseText != null && this.dischargeCaseText != undefined) {
        let DischargeText = null;
        let IsDischarged = false;
        let IsReopen = false;
        if (this.caseStatus == 0 || this.caseStatus == 2) {
          DischargeText = 'Discharged';
          IsDischarged = true;
        }
        if (this.caseStatus == 1) {
          DischargeText = 'Re-Open';
          IsReopen = true;
        }
        let array = [
          {
            'DischargeText': DischargeText,
            'Dischargedesc': this.dischargeCaseText
          }
        ];
        let obj: any =
          {
            'patientId': this.patientId,
            'status': 1,
            'casesId': this.caseId,
            'service': [],
            'treatmentNotesFor': 2,
            'entryDatetime': new Date().toISOString(),
            'isPinned': false,
            'locationId': this.locationId,
            'treatmentData': JSON.stringify(array)
          };
        if (this.caseStatus == 0 || this.caseStatus == 2) {
          obj.IsDischarge = IsDischarged;
        }
        if (this.caseStatus == 1) {
          obj.IsReopen = IsReopen;
        }
        this.blockUI.start();
        this.patientService
          .createPatientTreatmentNotes1(obj)
          .subscribe(
            (data) => {
              this.blockUI.stop();
              this.addCase = false;
              this.dischargeCaseText = '';
              this.mergeCaseShowFlag = false;
              this.count = 0;
              this.openPopUp = false;
              let responseHandle = null;
              if (this.caseStatus == 0 || this.caseStatus == 2) {
                responseHandle = {
                  message: 'Case Discharged Successfully.',
                  type: 'success'
                };
              } else {
                responseHandle = {
                  message: 'Enter Re-open Successfully.',
                  type: 'success'
                };
              }
              this.patientService.publishSomeData(JSON.stringify(responseHandle));
              this.fetchCaseListsForPatientByLocation();
            }, (err) => {
              this.blockUI.stop();
              this.openPopups('dischargeCaseError');
            });
      } else {
        let responseHandle = null;
        if (this.caseStatus == 0 || this.caseStatus == 2) {
          responseHandle = {
            message: 'Enter Discharge Note.',
            type: 'error'
          };
        } else {
          responseHandle = {
            message: 'Enter Re-open Note.',
            type: 'error'
          };
        }
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      }
    }
    if (whichPopup == 'cancelCase') {
      this.cancelCase = false;
      this.addCase = false;
      this.mergeCaseShowFlag = false;
      this.caseForm.reset();
      this.caseForm.controls['caseStatus'].setValue(0);
      this.openPopUp = false;
    }
    if (whichPopup == 'deleteCase') {
      this.blockUI.start();
      this.patientService.DeletePatientCasesData(this.patientId, this.caseId).subscribe((data) => {
        let responseHandle = {
          message: 'Case deleted Successfully.',
          type: 'success'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
        this.blockUI.stop();
        this.openPopUp = false;
        this.addCase = false;
        this.mergeCaseShowFlag = false;
        this.count = 0;
        this.fetchCaseListsForPatientByLocation();
      }, (err) => {
        console.log(err);
        this.openPopups('caseDeleteError');
        this.blockUI.stop();
      });
    }
    if (whichPopup == 'defaultCaseSet') {
      this.caseForm.controls['isdefault'].setValue(true);
      this.saveForm();
      this.openPopUp = false;
    }
    if (whichPopup == 'defaultReferralSet') {
      this.referralFlag = false;
      for (let index = 0; index < this.referralAddList.length; index++) {
        if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
          if (this.referralAddList[index].type == 1) {
            if (this.contactCaseId != null && this.contactCaseId != undefined) {
              this.referralAddList[index].isDefault = false;
              if (this.referralAddList[index].id == this.contactCaseId) {
                this.referralAddList[index].isDefault = true;
                this.referralForm.controls['isDefault'].setValue(true);
              }
            } else {
              this.referralAddList[index].isDefault = false;
              if (this.referralAddList[index].index == this.referralForm.value.index) {
                this.referralAddList[index].isDefault = true;
                this.referralForm.controls['isDefault'].setValue(true);
              }
            }
          }
        }
        if (this.upload_title == 'Attach Third Party' || this.upload_title == 'Edit Attached Third Party') {
          if (this.referralAddList[index].type == 2) {
            if (this.contactCaseId != null && this.contactCaseId != undefined) {
              this.referralAddList[index].isDefault = false;
              if (this.referralAddList[index].id == this.contactCaseId) {
                this.referralAddList[index].isDefault = true;
                this.referralForm.controls['isDefault'].setValue(true);
              }
            } else {
              this.referralAddList[index].isDefault = false;
              if (this.referralAddList[index].index == this.referralForm.value.index) {
                this.referralAddList[index].isDefault = true;
                this.referralForm.controls['isDefault'].setValue(true);
              }
            }
          }
        }
      }
      this.openPopUp = false;
      this.saveReferral();
    }
    if (whichPopup == 'detachReferral') {
      this.openPopUp = false;
      this.detachReferralOrThirdParty();
    }
    if (whichPopup == 'deleteImage') {
      this.openPopUp = false;
      this.deleteUploadedFile();
    }
    if (whichPopup == 'mergeCase') {
      this.blockUI.start();
      let obj: any = {
        locationId: this.locationId,
        patientId: this.patientId,
        fromCases: this.fromCaseId,
        toCases: this.toCaseId
      };
      this.patientService.mergeCaseContact(obj).subscribe((data) => {
        let responseHandle = {
          message: 'Your Cases has been successfully Merged.',
          type: 'success'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
        this.blockUI.stop();
        this.openPopUp = false;
        this.addCase = false;
        this.mergeCaseShowFlag = false;
        this.count = 0;
        this.fetchCaseListsForPatientByLocation();
      }, (err) => {
        let responseHandle = {
          message: 'Failed to Merge case, please try again later',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
      });
    }
    if (whichPopup == 'caseDeleteError') {
      this.closePopups('caseDeleteError');
    }
    if (whichPopup == 'dischargeCaseError') {
      this.closePopups('dischargeCaseError');
    }
    if (whichPopup == 'detachReferralOrThirdPartyError') {
      this.closePopups('detachReferralOrThirdPartyError');
    }
  }

  //date picker start
  toggleCalendarPopup(val, from) {
    if (from == 'injuryDate') {
      // if (val === true) {
      //   this.injuryCalendar.nativeElement.onfocus();
      // }
      this.isCalendarOpen = val;
    }
    if (from == 'startDate') {
      this.isStartCalenderOpen = val;
    }
    if (from == 'endDate') {
      this.isEndCalenderOpen = val;
    }
  }

  get getSelectedFormattedDate() {
    if (this.currentDate != null) {
      if (this.currentView != 'day') {
        return this.getFormatedDateForWeek(this.currentDate, this.getDaysToAdd());
      }
      return this.getFormatedDate(this.currentDate);
    } else {
      return '';
    }
  }

  get getSelectedFormattedDateStart() {
    if (this.startDate != null) {
      if (this.currentView != 'day') {
        return this.getFormatedDateForWeek(this.startDate, this.getDaysToAdd());
      }
      return this.getFormatedDate(this.startDate);
    } else {
      return '';
    }
  }

  get getSelectedFormattedDateEnd() {
    if (this.endDate != null) {
      if (this.endDate != 'indefinte') {
        if (this.currentView != 'day') {
          return this.getFormatedDateForWeek(this.endDate, this.getDaysToAdd());
        }
        return this.getFormatedDate(this.endDate);
      } else {
        return 'indefinte';
      }
    } else {
      return '';
    }
  }

  private getDaysToAdd(): number {
    return this.currentView == 'day' ? 1 : this.currentView == 'week' ? 6 : 4;
  }


  getFormatedDateForWeek(currentDate: Date, daysToAdd: number): string {
    let start = this.getFormatedDate(this.getStartDateOfWeek(currentDate));
    let end = this.getFormatedDate(this.getEndDateOfWeek(currentDate, daysToAdd));
    return start + ' to ' + end;
  }

  getFormatedDate(date) {
    return moment(date).format(this.dateFormat);
  }

  getStartDateOfWeek(date: Date): Date {
    return moment(date).startOf('isoWeek').toDate();
  }

  getEndDateOfWeek(date: Date, period: number): Date {
    return this.addAndGetNewValue(date, period, 'd');
  }

  addAndGetNewValue(date: Date, num, type: string): Date {
    return moment(date).clone().add(type, num).toDate();
  }

  openCalendarPopup(from) {
    this.isCalenderStartFocus = false;
    this.isCalenderInjuryFocus = false;
    this.isCalenderEndFocus = false;

    if (from == 'injuryDate') {
      let leftArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }
    if (from == 'startDate') {
      let leftArrContainer = document.querySelectorAll('.calendar-popup-startDate.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup-startDate.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }
    if (from == 'endDate') {
      let leftArrContainer = document.querySelectorAll('.calendar-popup-endDate.dx-calendar .dx-calendar-navigator-previous-month .dx-button-content');
      let rightArrContainer = document.querySelectorAll('.calendar-popup-endDate.dx-calendar .dx-calendar-navigator-next-month .dx-button-content');
      leftArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_left</mat-icon>';
      rightArrContainer[0].innerHTML = '<mat-icon class="mat-icon material-icons mat-icon-no-color cal-popup-pn-ic">arrow_right</mat-icon>';
    }
    this.changeDayLabelName(from);
  }

  calenderPopupFocus(isFocus: boolean, from: any) {
    if (from == 'injuryDate') {
      this.isCalenderInjuryFocus = isFocus;
    }
    if (from == 'startDate') {
      this.isCalenderStartFocus = isFocus;
    }
    if (from == 'endDate') {
      this.isCalenderEndFocus = isFocus;
    }
  }

  optionChanged(from) {
    this.changeDayLabelName(from);
  }

  private changeDayLabelName(from) {
    let tables = null;
    if (from == 'injuryDate') {
      tables = document.querySelectorAll('.calendar-popup.dx-calendar .dx-calendar-body table');
    }
    if (from == 'startDate') {
      tables = document.querySelectorAll('.calendar-popup-startDate.dx-calendar .dx-calendar-body table');
    }
    if (from == 'endDate') {
      tables = document.querySelectorAll('.calendar-popup-endDate.dx-calendar .dx-calendar-body table');
    }
    if (tables) {
      Array.from(tables).forEach((table: any) => {
        let nameLabes = table.querySelectorAll('thead tr th');
        Array.from(nameLabes).forEach((th: any) => {
          let name = th.getAttribute('abbr');
          th.innerHTML = name.substr(0, 2);
        });
      });
    }
  }

  // onDateselect($event) {
  //   this.caseForm.get('injuryDate').setValue($event);
  // }

  onDateSelect(from, $event) {
    if (from == 'injuryDate') {
      // this.caseForm.controls['injuryDate'].setValue(new Date(this.currentDate).toISOString())
      this.caseForm.get('injuryDate').setValue($event);
    }
    if (from == 'startDate') {
      if (this.endDate != null) {
        if (new Date(this.startDate).toISOString() > new Date(this.endDate).toISOString()) {
          let responseHandle = {
            message: 'Please Select valid Start Date.',
            type: 'error'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          this.referralForm.controls['startDate'].setValue(null);
          this.startDate = null;
        }
      } else {
        // this.referralForm.controls['startDate'].setValue(new Date(this.startDate).toISOString())
        this.referralForm.controls['startDate'].setValue($event);
        // this.referralForm.controls['selectDuration'].setValue(this.dateActions[1])
        // let event = {
        //   value: this.dateActions[1].value
        // }
        // this.performDateAction(event)
      }
    }
    if (from == 'endDate') {
      // if (this.startDate != null) {
      if (this.referralForm.controls['startDate'].value != null) {
        this.referralForm.controls['endDate'].setValue($event);
        // if (new Date(this.endDate).toISOString() < new Date(this.startDate).toISOString()) {
        console.log(this.datePipe.transform(this.referralForm.controls['endDate'].value, 'yyyy-MM-dd'), this.datePipe.transform(this.referralForm.controls['startDate'].value, 'yyyy-MM-dd'));
        console.log(this.datePipe.transform(this.referralForm.controls['endDate'].value, 'yyyy-MM-dd') < this.datePipe.transform(this.referralForm.controls['startDate'].value, 'yyyy-MM-dd'));
        if (this.datePipe.transform(this.referralForm.controls['endDate'].value, 'yyyy-MM-dd') < this.datePipe.transform(this.referralForm.controls['startDate'].value, 'yyyy-MM-dd')) {
          let responseHandle = {
            message: 'Please Select valid End Date.',
            type: 'error'
          };
          this.patientService.publishSomeData(JSON.stringify(responseHandle));
          this.referralForm.controls['endDate'].setValue(null);
          this.endDate = null;
        } else {
          // this.referralForm.controls['endDate'].setValue(new Date(this.endDate).toISOString())
          this.referralForm.controls['endDate'].setValue($event);
        }
      } else {
        let responseHandle = {
          message: 'Please First Select Start Date.',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
        this.referralForm.controls['endDate'].setValue(null);
        this.endDate = null;
      }
    }
    this.toggleCalendarPopup(false, from);
  }

  isApplyWeekOrMF(date: Date) {
    return (this.isInWeek(date) || this.isCurrDateInWeek(date)) && this.currentView != 'day';
  }

  isInWeek(date: Date) {
    return this.isBetween(this.currHvrDateStart, this.currHvrDateend, date);
  }

  isBetween(start: Date, end: Date, date: Date): boolean {
    let mm = moment(date);
    return mm.isSameOrAfter(start) && mm.isSameOrBefore(end);
  }

  isCurrDateInWeek(date: Date) {
    let currHvrDateStart = this.getStartDateOfWeek(this.currentDate);
    let currHvrDateend = this.getEndDateOfWeek(currHvrDateStart, this.getDaysToAdd());
    return this.isBetween(currHvrDateStart, currHvrDateend, date);
  }


  mouseHover(cell: any) {
    this.hoverDate = cell.date;
    this.setupStartEndDayOfWeek();
  }

  setupStartEndDayOfWeek() {
    this.currHvrDateStart = this.getStartDateOfWeek(this.hoverDate);
    this.currHvrDateend = this.getEndDateOfWeek(this.currHvrDateStart, this.getDaysToAdd());
  }

  setTodayDate(from) {
    if (from == 'injuryDate') {
      this.currentDate = this.getCurrentDate();
    }
    if (from == 'startDate') {
      this.startDate = this.getCurrentDate();
    } else {
      this.endDate = this.getCurrentDate();
    }
    this.prepareDateArrayForSdlr(from);
  }

  getCurrentDate(): Date {
    return moment().toDate();
  }

  prepareDateArrayForSdlr(from) {
    if (this.currentView == 'day') {
      if (from == 'injuryDate') {
        this.selectedDate = [new Dte(this.currentDate)];
      }
      if (from == 'startDate') {
        this.selectedStartDate = [new Dte(this.startDate)];
      } else {
        this.selectedEndDate = [new Dte(this.endDate)];
      }
    } else if (this.currentView == 'week') {
      if (from == 'injuryDate') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      }
      if (from == 'startDate') {
        let weekStartDate = this.getStartDateOfWeek(this.startDate);
        this.selectedStartDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.endDate);
        this.selectedEndDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 1; i < 7; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
        }
      }
    } else {
      if (from == 'injuryDate') {
        let weekStartDate = this.getStartDateOfWeek(this.currentDate);
        this.selectedDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedDate.push(new Dte(date));
        }
      }
      if (from == 'startDate') {
        let weekStartDate = this.getStartDateOfWeek(this.startDate);
        this.selectedStartDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedStartDate.push(new Dte(date));
        }
      } else {
        let weekStartDate = this.getStartDateOfWeek(this.endDate);
        this.selectedEndDate = [new Dte(weekStartDate)];
        let date = weekStartDate;
        for (let i = 0; i < 4; i++) {
          date = this.addAndGetNewValue(date, 1, 'd');
          this.selectedEndDate.push(new Dte(date));
        }
      }
    }
  }

  nextPrevDate(num: number, from) {
    if (from == 'injuryDate') {
      this.currentDate = this.addAndGetNewValue(this.currentDate, num * this.getDaysToAdd(), 'd');
    }
    if (from == 'startDate') {
      this.startDate = this.addAndGetNewValue(this.startDate, num * this.getDaysToAdd(), 'd');
    } else {
      this.endDate = this.addAndGetNewValue(this.endDate, num * this.getDaysToAdd(), 'd');
    }
    this.prepareDateArrayForSdlr(from);
  }

  //date picker end

  //detach referral and third party
  detachReferralOrThirdParty() {
    console.log(this.referralAddList, this.selectedDataIndex);
    if (this.caseId != null && this.caseId != '' && this.caseId != undefined && this.contactCaseId != null && this.contactCaseId != '' && this.contactCaseId != undefined) {
      this.patientService.deleteReferralOrThirdParty(this.caseId, this.contactCaseId).subscribe((res) => {
        let responseHandle = null;
        if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
          responseHandle = {
            message: 'Your Referral has been successfully Detached.',
            type: 'success'
          };
        } else {
          responseHandle = {
            message: 'Your Third Party has been successfully Detached.',
            type: 'success'
          };
        }
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
        let dataItem = {
          dataItem: {
            id: this.caseId
          }
        };
        this.detailExpand(dataItem);
        this.fetchCaseListsForPatientByLocation();
        this.referralFlag = false;
        this.blockUI.stop();
      }, (err) => {
        console.log(err);
        this.openPopups('detachReferralOrThirdPartyError');
        let responseHandle = {
          message: 'Unfortunately, Your Referral was Detached unsuccesful.',
          type: 'error'
        };
        this.patientService.publishSomeData(JSON.stringify(responseHandle));
        this.blockUI.stop();
      });
    } else {
      this.referralAddList.splice(this.selectedDataIndex - 1, 1);
      let responseHandle = null;
      if (this.upload_title == 'Attach Referral' || this.upload_title == 'Edit Attached Referral') {
        responseHandle = {
          message: 'Your Referral has been successfully Detached.',
          type: 'success'
        };
      } else {
        responseHandle = {
          message: 'Your Third Party has been successfully Detached.',
          type: 'success'
        };
      }
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
      this.referralFlag = false;
    }
  }

  //open select duration
  openSelectDuration() {
    this.isOpenDropDownList = !this.isOpenDropDownList;
    this.dropdownlist.toggle(this.isOpenDropDownList);
  }

  //Close select duration
  closeDropDown() {
    this.isOpenDropDownList = false;
  }


  //end date set based on select duration
  performDateAction(event) {
    if (this.startDate == null) {
      let responseHandle = {
        message: 'Please Select valid Start Date.',
        type: 'error'
      };
      this.patientService.publishSomeData(JSON.stringify(responseHandle));
    } else {
      if (event.value != 'indefinte') {
        let startDate = new Date(this.startDate);
        this.endDate = new Date(startDate.setMonth(startDate.getMonth() + event.value));
        this.referralForm.controls['endDate'].setValue(this.endDate.toISOString());
      } else {
        this.endDate = 'indefinte';
        this.referralForm.controls['endDate'].setValue('indefinte');
      }
    }
  }

  //selected status
  selectedStatus(status) {
    if (status.id == 3) {
      this.referralForm.controls['notes'].setValidators(Validators.required);
      this.referralForm.controls['notes'].updateValueAndValidity();
    } else {
      this.referralForm.controls['notes'].setValidators([]);
      this.referralForm.controls['notes'].updateValueAndValidity();
    }
  }

  get referralEndDateFormated() {
    return this.datePipe.transform(this.referralForm.controls['endDate'].value, 'dd-MM-yyyy');
  }

  get refferralEndDate() {
    return this.referralForm.get('endDate').value ? new Date(this.referralForm.get('endDate').value) : new Date();
  }
}
