import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientCasescontactsComponent } from './patient-casescontacts.component';

describe('PatientCasescontactsComponent', () => {
  let component: PatientCasescontactsComponent;
  let fixture: ComponentFixture<PatientCasescontactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientCasescontactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientCasescontactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
