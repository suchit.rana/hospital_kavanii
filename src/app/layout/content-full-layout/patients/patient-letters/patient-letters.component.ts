import {Component, Input, OnInit} from '@angular/core';
import {MatButtonToggleChange} from '@angular/material';
import {AppState} from 'src/app/app.state';
import {PatientService} from 'src/app/services/app.patient.service';
import {LetterService} from '../../../../services/app.letter.service';
import {BaseGridComponent} from '../../shared-content-full-layout/base-grid/base-grid.component';
import {LetterAction} from './add-letter/add-letter.component';
import {Router} from '@angular/router';
import {RouterState} from '../../../../shared/interface';
import {RP_MODULE_MAP} from '../../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-patient-letters',
  templateUrl: './patient-letters.component.html',
  styleUrls: ['./patient-letters.component.css']
})
export class PatientLettersComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.patients_letters;

  readonly PREVIEW = 'preview';
  readonly DELETE = 'delete';
  readonly DELETELETTER = 'deleteletter';
  readonly DUPLICATE = 'duplicate';
  readonly EMAIL = 'email';
  readonly ADD_LETTER = 'Add Letter';
  readonly EDIT_LETTER = 'Edit Letter';

  letterlabel: string = '';
  letterId: string = '';
  locationId: any;
  nodata: boolean = true;
  addLetter: boolean = false;
  isCopy: boolean = false;
  letterTitle = '';
  openPreviewPopUp: boolean = false;
  openDeleteLettePopUp: boolean = false;
  openDuplicateLettePopUp: boolean = false;
  openEmailLettePopUp: boolean = false;

  @Input() patientId: string;
  @Input() IsFromPage: any;
  PreviewData;
  routerState: RouterState;


  constructor(
    protected router: Router,
    private patientService: PatientService,
    private letterService: LetterService,
    public appState: AppState,
  ) {
    super(null, appState);
    if (this.router.getCurrentNavigation() != null) {
      this.routerState = this.router.getCurrentNavigation().extras.state as RouterState;
    } else {
      this.routerState = history.state;
    }
    if (this.routerState && this.routerState.fromState) {
      if (this.routerState.data.forAdd) {
        this.onAddLetter();
      } else if (!this.routerState.data.forAdd) {
        this.editLetter(this.routerState.data.letterId);
      }
    }
  }

  ngOnInit() {
    this.prepareData();
    this.setActiveFilter();
  }

  // Prepare Data For Component
  prepareData() {
    this.letterTitle = 'You haven\'t added a Letter';
    this.locationId = this.appState.selectedUserLocation.id;

    // get all patient letters
    this.letterService.getPatientLetter(this.locationId, this.patientId).subscribe((data: any) => {
      if (data.length === 0) {
        this.letterTitle = 'You haven\'t added a Letter';
        this.nodata = true;
        return;
      }
      this.nodata = false;
      data.forEach(value => {
        value['isStatus'] = value.isActive;
      });
      this.gridData = data;
      this.loadItems('createdDate', 'desc');
    });
    this.gridData = [];
    this.loadItems('createdDate', 'desc');

  }

  // Add Letter Method
  onAddLetter() {
    this.addLetter = true;
    this.letterlabel = this.ADD_LETTER;
    this.letterId = null;
    this.isCopy = false;
  }

  // Active/Inactive status Filter
  letterStatusChanged(event: MatButtonToggleChange) {
    if (event.value == 'active') {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }
    this.loadItems('createdDate', 'desc');
  }

  // show alert message
  showAlert(status) {
    let responseHandle;
    if (status === LetterAction.SaveAsDraft) {
      if (this.isCopy) {
        responseHandle = {
          message: 'Your duplicate Letter hase been saved as draft successfully',
          type: 'success'
        };
      } else {
        responseHandle = {
          message: 'Your Letter hase been saved as draft successfully',
          type: 'success'
        };
      }
    } else if (status === LetterAction.Finalise) {
      if (this.isCopy) {

        responseHandle = {
          message: 'Your duplicate letter hase been finalised successfully',
          type: 'success'
        };
      } else {
        responseHandle = {
          message: 'Your Letter hase been finalised successfully',
          type: 'success'
        };
      }
    } else if (status === LetterAction.Delete) {
      responseHandle = {
        message: 'Your Letter hase been deleted successfully',
        type: 'success'
      };
    } else if (status === LetterAction.Exported) {
      responseHandle = {
        message: 'Your Letter hase been exported successfully',
        type: 'success'
      };
    } else if (status === LetterAction.Mailed) {
      responseHandle = {
        message: 'Your Letter hase been emailed successfully',
        type: 'success'
      };
    }
    this.patientService.publishSomeData(JSON.stringify(responseHandle));
    this.prepareData();
  }

  // When cancel button
  goBackToListing() {
    this.addLetter = false;
  }

  // open pop-ups
  openPopups(popName: string) {
    if (popName.toLowerCase() === this.PREVIEW) {
      this.openPreviewPopUp = true;
    }
    if (popName.toLowerCase() === this.DELETE) {
      this.openDeleteLettePopUp = true;
    }
    if (popName.toLowerCase() === this.DUPLICATE) {
      this.openDuplicateLettePopUp = true;
    }
    if (popName.toLowerCase() === this.EMAIL) {
      this.openPreviewPopUp = false;
      this.openEmailLettePopUp = true;
    }
  }

  generatePatientLeters(patientLetterId: string = '') {
    var param = {
      'patientLetterId': patientLetterId,
      'patientId': this.patientId,
      'locationId': this.locationId
    };
    this.letterService.generatePatientLeters(param).subscribe((data: any) => {
      this.PreviewData = data.body ? data.body : '';
    });
  }


  // close pop-ups
  closePopups(popName: string) {
    if (popName.toLowerCase() === this.PREVIEW) {
      this.openPreviewPopUp = false;
      this.showAlert(LetterAction.Finalise);
    }
    if (popName.toLowerCase() === this.DELETE) {
      this.openDeleteLettePopUp = false;
    }
    if (popName.toLowerCase() === this.DELETELETTER) {
      this.deleteLetter();
    }
    if (popName.toLowerCase() === this.DUPLICATE) {
      this.openDuplicateLettePopUp = false;
    }
    if (popName.toLowerCase() === this.EMAIL) {
      this.openEmailLettePopUp = false;
      this.showAlert(LetterAction.Mailed);
    }
  }

  // Export Letter
  exportLetter(exportOption: string) {
    if (exportOption.toLowerCase() === 'pdf') {
      this.openPreviewPopUp = false;
      this.showAlert(LetterAction.Exported);
    }
    if (exportOption.toLowerCase() === 'word') {
      this.openPreviewPopUp = false;
      this.showAlert(LetterAction.Exported);
    }
  }

  // Edit letter
  onCLickLetterName(letterId: string, editStatus: number) {
    if (editStatus === 0) {
      this.editLetter(letterId);
    } else {
      this.openPreviewPopUp = true;
      this.letterId = letterId;
    }
  }

  editLetter(letterId: string) {
    this.addLetter = true;
    this.letterlabel = this.EDIT_LETTER;
    this.letterId = letterId;
    this.isCopy = false;
  }

  duplicateID(id) {
    this.letterId = id;
  }

  // Duplicate letter
  duplicateLetter() {
    this.openPreviewPopUp = false;
    this.openDuplicateLettePopUp = false;
    this.letterlabel = this.ADD_LETTER;
    this.addLetter = true;
    this.isCopy = true;
  }

  // Delete Letter
  deleteLetter() {

    // this.openDeleteLettePopUp = false;
    // this.openPreviewPopUp = false;
    // this.showAlert(LetterAction.Delete);

    this.letterService.deletePatientLetterById(this.patientId, this.letterId).subscribe((res: any) => {
      if (res) {
        this.openDeleteLettePopUp = false;
        this.openPreviewPopUp = false;
        this.showAlert(LetterAction.Delete);
      }
    });
  }

  Export2Word(element, filename = '') {
    var preHtml = '<html xmlns:o=\'urn:schemas-microsoft-com:office:office\' xmlns:w=\'urn:schemas-microsoft-com:office:word\' xmlns=\'http://www.w3.org/TR/REC-html40\'><head><meta charset=\'utf-8\'><title>Export HTML To Doc</title></head><body>';
    var postHtml = '</body></html>';
    var html = preHtml + document.getElementById(element).innerHTML + postHtml;
    var blob = new Blob(['\ufeff', html], {
      type: 'application/msword'
    });
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    filename = filename ? filename + '.doc' : 'document.doc';
    var downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);
    downloadLink.href = url;
    downloadLink.download = filename;
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }
}
