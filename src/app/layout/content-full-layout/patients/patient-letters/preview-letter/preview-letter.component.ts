import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AppState} from '../../../../../app.state';
import {LetterService} from '../../../../../services/app.letter.service';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';

@Component({
  selector: 'app-preview-letter',
  templateUrl: './preview-letter.component.html',
  styleUrls: ['./preview-letter.component.css']
})
export class PreviewLetterComponent implements OnInit {

  static ref: IPopupAction;

  @Input()
  letterId: string;
  @Input()
  patientId: string;

  @ViewChild('deleteConfirm', {static: false}) deleteConfirm;

  generatedPreview = '';

  constructor(
    private appState: AppState,
    private dialogService: AppDialogService,
    private alertService: AppAlertService,
    private letterService: LetterService,
  ) {
  }

  ngOnInit() {
    this.generatePatientLetterPreview();
  }

  generatePatientLetterPreview() {
    const param = {
      'patientLetterId': this.letterId,
      'patientId': this.patientId,
      'locationId': this.appState.selectedUserLocation.id
    };
    this.letterService.generatePatientLeters(param).subscribe((data: any) => {
      this.generatedPreview = data.body;
    });
  }

  static open(dialogService: AppDialogService, letterId: string, patientId: string) {
    this.ref = dialogService.open(PreviewLetterComponent, {
      title: 'Preview',
      minWidth: '778px',
      bodyPadding: true,
      hideFooter: true,
      data: {
        letterId: letterId,
        patientId: patientId
      }
    });
  }

  deleteLetter() {
    this.dialogService.open(this.deleteConfirm, {
      title: 'ARE YOU SURE TO DELETE THIS LETTER?',
      width: '800px',
      cancelBtnTitle: 'No',
      saveBtnTitle: 'Yes'
    }).clickSave.subscribe(() => {
      this.letterService.deletePatientLetterById(this.patientId, this.letterId).subscribe(response => {
        if (response) {
          this.alertService.displaySuccessMessage('Letter deleted successfully.');
          PreviewLetterComponent.ref.closeDialog();
        }
      }, error => {
        this.alertService.displayErrorMessage(error || 'Failed to delete letter. Please try again later');
      });
    });
  }

  export2Word(content: string, filename = '') {
    const preHtml = '<html xmlns:o=\'urn:schemas-microsoft-com:office:office\' xmlns:w=\'urn:schemas-microsoft-com:office:word\' xmlns=\'http://www.w3.org/TR/REC-html40\'><head><meta charset=\'utf-8\'><title>Letter</title></head><body>';
    const postHtml = '</body></html>';
    const html = preHtml + content + postHtml;
    const url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    filename = filename ? filename + '.doc' : 'document.doc';
    const downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);
    downloadLink.href = url;
    downloadLink.download = filename;
    downloadLink.click();
    document.body.removeChild(downloadLink);
    PreviewLetterComponent.ref.closeDialog();
  }
}
