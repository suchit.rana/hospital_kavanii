import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PDFExportModule} from '@progress/kendo-angular-pdf-export';
import {MatButtonModule, MatDividerModule, MatMenuModule} from '@angular/material';
import {PreviewLetterComponent} from './preview-letter.component';



@NgModule({
  declarations: [
    PreviewLetterComponent
  ],
  imports: [
    CommonModule,
    PDFExportModule,
    MatMenuModule,
    MatDividerModule,
    MatButtonModule
  ],
  entryComponents: [PreviewLetterComponent]
})
export class PreviewLetterModule { }
