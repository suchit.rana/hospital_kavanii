import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppState} from 'src/app/app.state';
import {PatientService} from 'src/app/services/app.patient.service';
import {SettingsService} from 'src/app/services/app.settings.service';
import {BaseGridComponent} from '../../../shared-content-full-layout/base-grid/base-grid.component';
import {LetterService} from '../../../../../services/app.letter.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {PatientLetterModel} from '../../../../../models/app.letter.model';
import {FromPage, RouterState} from '../../../../../shared/interface';
import {NavigationExtras, Router} from '@angular/router';
import {BubbleAppointment} from '../../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

export enum LetterStatus {
  SaveAsDraft,
  Finalise,
}

export enum LetterAction {
  SaveAsDraft,
  Finalise,
  Delete,
  Exported,
  Mailed,
  Duplicated
}

@Component({
  selector: 'app-add-letter',
  templateUrl: './add-letter.component.html',
  styleUrls: ['./add-letter.component.css']
})
export class AddLetterComponent extends BaseGridComponent implements OnInit {
  moduleName = RP_MODULE_MAP.patients_letters;

  readonly EXPAND = 'EXPAND';

  ckeConfig: any;
  public expanded = false;
  public btnText = 'More';
  public liked = false;
  templateNames: any;
  caseNames: any[];
  appointments: any[];
  textdata: string = 'expand';
  showDeleteBTN: boolean = false;
  openDeleteLettePopUp: boolean = false;

  // For body
  public expandedKeys = [];
  private selectedKeys = ['Tags'];

  public toggleRecipe(): void {
    this.expanded = !this.expanded;
    this.btnText = this.expanded ? 'Less' : 'More';
    this.heartIcon();
  }

  public heartIcon(): string {
    return this.liked ? 'k-icon k-i-arrow-60-up' : 'k-icon k-i-arrow-60-down';
  }

  public toggleLike(): void {
    this.liked = !this.liked;
  }

  public data: any[] = [
    {
      id: 2, text: 'Business Details', items: [
        {id: 3, text: 'Business Name', class1: '{', class2: '}'},
        {id: 4, text: 'Contact Person', class1: '{', class2: '}'},
        {id: 5, text: 'Address 1', class1: '{', class2: '}'},
        {id: 6, text: 'Address 2', class1: '{', class2: '}'},
        {id: 7, text: 'country', class1: '{', class2: '}'},
        {id: 8, text: 'state', class1: '{', class2: '}'},
        {id: 9, text: 'city', class1: '{', class2: '}'},
        {id: 10, text: 'Post code', class1: '{', class2: '}'},
        {id: 11, text: 'Phone', class1: '{', class2: '}'},
        {id: 12, text: 'Mobile', class1: '{', class2: '}'},
        {id: 13, text: 'Fax', class1: '{', class2: '}'},
        {id: 14, text: 'Email', class1: '{', class2: '}'},
        {id: 15, text: 'Website', class1: '{', class2: '}'},
        {id: 16, text: 'Bank Name', class1: '{', class2: '}'},
        {id: 17, text: 'BSB No', class1: '{', class2: '}'},
        {id: 18, text: 'Account No', class1: '{', class2: '}'},
        {id: 19, text: 'Account Name', class1: '{', class2: '}'},
      ]
    },
    {
      id: 20, text: 'Location Details', items: [
        {id: 21, text: 'Location  Name', class1: '{', class2: '}'},
        {id: 22, text: 'Contact Person', class1: '{', class2: '}'},
        {id: 23, text: 'Address 1', class1: '{', class2: '}'},
        {id: 24, text: 'Address 2', class1: '{', class2: '}'},
        {id: 25, text: 'country', class1: '{', class2: '}'},
        {id: 26, text: 'state', class1: '{', class2: '}'},
        {id: 27, text: 'city', class1: '{', class2: '}'},
        {id: 28, text: 'Post code', class1: '{', class2: '}'},
        {id: 29, text: 'Phone', class1: '{', class2: '}'},
        {id: 30, text: 'Mobile', class1: '{', class2: '}'},
        {id: 31, text: 'Fax', class1: '{', class2: '}'},
        {id: 32, text: 'Email', class1: '{', class2: '}'},
        {id: 33, text: 'Website', class1: '{', class2: '}'},
      ]
    },
    {
      id: 34, text: 'Patient Details', items: [
        {id: 35, text: 'Patient ID', class1: '{', class2: '}'},
        {id: 36, text: 'Title', class1: '{', class2: '}'},
        {id: 37, text: 'Full Name', class1: '{', class2: '}'},
        {id: 38, text: 'Fname', class1: '{', class2: '}'},
        {id: 39, text: 'Lname', class1: '{', class2: '}'},
        {id: 40, text: 'Gender', class1: '{', class2: '}'},
        {id: 41, text: 'DOB', class1: '{', class2: '}'},
        {id: 42, text: 'Age', class1: '{', class2: '}'},
        {id: 43, text: 'First Visit DAte', class1: '{', class2: '}'},
        {id: 44, text: 'Primary Practitioner', class1: '{', class2: '}'},
        {id: 45, text: 'Address', class1: '{', class2: '}'},
        {id: 46, text: 'country', class1: '{', class2: '}'},
        {id: 47, text: 'State', class1: '{', class2: '}'},
        {id: 48, text: 'city', class1: '{', class2: '}'},
        {id: 49, text: 'Post code', class1: '{', class2: '}'},
        {id: 50, text: 'Home Ph', class1: '{', class2: '}'},
        {id: 51, text: 'Work Ph', class1: '{', class2: '}'},
        {id: 52, text: 'Mobile', class1: '{', class2: '}'},
        {id: 53, text: 'Email', class1: '{', class2: '}'},
        {
          id: 54, text: 'Employer Details', items: [
            {id: 55, text: 'Occupation', class1: '{', class2: '}'},
            {id: 56, text: 'Employer', class1: '{', class2: '}'},
            {id: 57, text: 'Contact Person', class1: '{', class2: '}'},
            {id: 58, text: 'Work Phone', class1: '{', class2: '}'},
            {id: 59, text: 'eMail', class1: '{', class2: '}'}
          ]
        },
        {
          id: 60, text: 'Billing', items: [
            {id: 61, text: 'Invoice Notes', class1: '{', class2: '}'}
          ]
        },
        {
          id: 62, text: 'Health Fund', items: [
            {id: 63, text: 'HL', class1: '{', class2: '}'},
            {id: 64, text: 'HL Medicare Membership No', class1: '{', class2: '}'},
            {id: 65, text: 'HL Medicare IRN', class1: '{', class2: '}'},
            {id: 66, text: 'HL DVA Membership No', class1: '{', class2: '}'},
            {id: 67, text: 'HL L DVA IRN,', class1: '{', class2: '}'}
          ]
        }
      ]
    },
    {
      id: 68, text: 'Case', items: [
        {id: 69, text: 'Case Name', class1: '{', class2: '}'},
      ]
    },
    {
      id: 70, text: 'Appointment Details', items: [
        {id: 71, text: 'Show DAte and Time', class1: '{', class2: '}'},
        {id: 72, text: 'Patient First Appt Date Time', class1: '{', class2: '}'},
        {id: 73, text: 'Patient Last Appt', class1: '{', class2: '}'},
        {id: 74, text: 'Patient Current Appt', class1: '{', class2: '}'},
        {id: 75, text: 'Patient Next Appt', class1: '{', class2: '}'},
        {id: 76, text: 'Patient All Past Appointments', class1: '{', class2: '}'},
        {id: 77, text: 'Patient All Suture Appointments', class1: '{', class2: '}'},
      ]
    },
    {
      id: 78, text: 'Practitioner Details', items: [
        {id: 79, text: 'Title', class1: '{', class2: '}'},
        {id: 80, text: 'First Name', class1: '{', class2: '}'},
        {id: 81, text: 'Last Name', class1: '{', class2: '}'},
        {id: 82, text: 'Name (Fname + LName)', class1: '{', class2: '}'},
        {id: 83, text: 'qualification', class1: '{', class2: '}'},
        {id: 84, text: 'position', class1: '{', class2: '}'},
        {id: 85, text: 'Phone', class1: '{', class2: '}'},
        {id: 86, text: 'Mobile', class1: '{', class2: '}'},
        {id: 87, text: 'Email', class1: '{', class2: '}'},
        {id: 88, text: 'Signature', class1: '{', class2: '}'},
        {id: 89, text: 'speciality', class1: '{', class2: '}'},
        {id: 90, text: 'Provider No', class1: '{', class2: '}'},
        {id: 91, text: 'Notes', class1: '{', class2: '}'},
        {id: 92, text: 'Patient First Appt Practitioner', class1: '{', class2: '}'},
        {id: 93, text: 'Patient Last Appt Practitioner', class1: '{', class2: '}'},
        {id: 94, text: 'Patient Current Appt Practitioner', class1: '{', class2: '}'},
        {id: 95, text: 'Patient Next Appt Practitioner', class1: '{', class2: '}'}

      ]
    },
    {
      id: 96, text: 'Referral Details', items: [
        {id: 97, text: 'Organization Name', class1: '{', class2: '}'},
        {id: 98, text: 'Title', class1: '{', class2: '}'},
        {id: 99, text: 'First Name', class1: '{', class2: '}'},
        {id: 100, text: 'Last Name', class1: '{', class2: '}'},
        {id: 101, text: 'Name (Fname + LName)', class1: '{', class2: '}'},
        {id: 102, text: 'speciality', class1: '{', class2: '}'},
        {id: 103, text: 'Provider No', class1: '{', class2: '}'},
        {id: 104, text: 'Phone', class1: '{', class2: '}'},
        {id: 105, text: 'Mobile', class1: '{', class2: '}'},
        {id: 106, text: 'FAX', class1: '{', class2: '}'},
        {id: 107, text: 'Email', class1: '{', class2: '}'},
        {id: 108, text: 'Notes', class1: '{', class2: '}'},
        {id: 109, text: 'start Date', class1: '{', class2: '}'},
        {id: 110, text: 'End Date', class1: '{', class2: '}'},
        {id: 111, text: 'Appointment Count', class1: '{', class2: '}'},
        {id: 112, text: 'Referral Notes', class1: '{', class2: '}'}

      ]
    },
    {
      id: 113, text: 'Third Party Details', items: [
        {id: 114, text: 'Organization Name', class1: '{', class2: '}'},
        {id: 115, text: ' Department Name', class1: '{', class2: '}'},
        {id: 116, text: 'Title', class1: '{', class2: '}'},
        {id: 117, text: 'First Name', class1: '{', class2: '}'},
        {id: 118, text: 'Last Name', class1: '{', class2: '}'},
        {id: 119, text: 'Name (Fname + LName)', class1: '{', class2: '}'},
        {id: 120, text: 'Reference No', class1: '{', class2: '}'},
        {id: 121, text: 'Phone', class1: '{', class2: '}'},
        {id: 122, text: 'Mobile', class1: '{', class2: '}'},
        {id: 123, text: 'FAX', class1: '{', class2: '}'},
        {id: 124, text: 'Email', class1: '{', class2: '}'},
        {id: 125, text: 'Notes', class1: '{', class2: '}'},
        {id: 126, text: 'start Date', class1: '{', class2: '}'},
        {id: 127, text: 'End Date', class1: '{', class2: '}'},
        {id: 128, text: 'Appointment Count', class1: '{', class2: '}'}

      ]
    },
    {
      id: 129, text: 'Invoice Details', items: [
        {id: 130, text: 'Invoice No', class1: '{', class2: '}'},
        {id: 131, text: 'Date', class1: '{', class2: '}'},
        {id: 132, text: 'Invoice Amt', class1: '{', class2: '}'},
        {id: 133, text: 'GST', class1: '{', class2: '}'},
        {id: 134, text: 'Total', class1: '{', class2: '}'},
        {id: 135, text: 'Paid', class1: '{', class2: '}'},
        {id: 136, text: 'Balance', class1: '{', class2: '}'}

      ]
    },
    {
      id: 137, text: 'Misc', items: [
        {id: 138, text: 'Today’s Date', class1: '{', class2: '}'},
        {id: 139, text: 'Current Time', class1: '{', class2: '}'},
        {id: 140, text: 'He \ She', class1: '{', class2: '}'},
        {id: 141, text: ' His or her', class1: '{', class2: '}'},

      ]
    }
  ];

  @Input() letterlabel: string;
  @Input() isCopy: boolean = false;
  @Input() patientId: string;
  @Input() letterId: string;
  addLetterForm: FormGroup;
  @Output() goBackToListing: EventEmitter<any> = new EventEmitter();
  @Output() showAlert: EventEmitter<any> = new EventEmitter();
  @Output() openPopups: EventEmitter<any> = new EventEmitter();
  @Output() duplicateID: EventEmitter<any> = new EventEmitter();
  @Output() generatePatientLeters: EventEmitter<any> = new EventEmitter();

  @BlockUI() blockUI: NgBlockUI;

  constructor(
    protected router: Router,
    public settingsService: SettingsService,
    private patientService: PatientService,
    private letterService: LetterService,
    public appState: AppState,
  ) {
    super(router);
    // if (this.router.getCurrentNavigation() != null) {
    //   this.routerState = this.router.getCurrentNavigation().extras.state as RouterState;
    // } else {
    //   this.routerState = history.state;
    // }
  }

  ngOnInit() {
    this.prepareData();
    this.letterEditor();
  }

  // for ckeditor
  expand() {
    if (this.textdata == 'expand') {
      this.textdata = 'collapse';
      this.data.forEach((i, idx) => {
        this.expandedKeys.push(idx.toString());
      });
    } else {
      this.textdata = 'expand';
      this.expandedKeys = [];
    }
  }

  // Prepare Component Data
  prepareData() {

    this.addLetterForm = this.initializeAddLetterFormGroup();

    // Get Letter Templates For Letter Name Field
    this.settingsService.getAllBussinessTemplate(1).subscribe((data) => {
      this.templateNames = data;
    });

    // Get Cases For Case Name Field
    this.getCases();

    // Get Appointments For Related Appointment Field
    this.getAppointments();

    if (this.isFromState()) {
      const appt = this.routerState.data.appointment as BubbleAppointment;
      this.addLetterForm.get('appointmentId').setValue(appt.appointmentEntryId);
      this.addLetterForm.get('casesId').setValue(this.routerState.data.caseId);
    }

    // Prepared data for edit & duplicate letter
    if (this.letterId) {
      this.showDeleteBTN = true;
      this.letterService.getPatientLetterbyId(this.appState.selectedUserLocation.id, this.patientId, this.letterId).subscribe((res) => {
        if (this.isCopy) {
          this.letterId = null;
          res.id = null;
        }
        res.businessTemplateId = res.businessTemplateId ? res.businessTemplateId : '00000000-0000-0000-0000-000000000000';
        res.appointmentId = res.appointmentId ? res.appointmentId : '00000000-0000-0000-0000-000000000000';
        this.addLetterForm.patchValue(res);
      });
    }

  }

  // Initialize AddLetter FormGroup
  initializeAddLetterFormGroup() {
    return new FormGroup({
      id: new FormControl(),
      businessTemplateId: new FormControl('00000000-0000-0000-0000-000000000000', [Validators.required]),
      patientId: new FormControl(this.patientId),
      name: new FormControl(null, [Validators.required]),
      casesId: new FormControl(null, [Validators.required]),
      byAppointment: new FormControl(true),
      appointmentId: new FormControl('00000000-0000-0000-0000-000000000000', [Validators.required]),
      entryDatetime: new FormControl('2021-10-20T16:25:38.179Z'),
      subject: new FormControl(),
      body: new FormControl(null, [Validators.required]),
      description: new FormControl(""),
      isActive: new FormControl(true),
      editStatus: new FormControl(null),
    });
  }

  // For Letter Editor Field
  letterEditor() {
    this.ckeConfig = {
      removeButtons: 'Save,NewPage,Templates,Preview,Print,Find,Replace,Unlink,Blockquote,Button,selection,Strike,Maximize,ShowBlocks,SelectAll,Select,Textarea,TextField,Form,HiddenField,CreateDiv,Language,Anchor,Flash,Iframe,About,ImageButton,Image,Smiley',
      extraAllowedContent: 'img[width,height,align]',
      allowedContent: 'timestamp',
      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets-all
      extraPlugins: 'timestamp',
      removePlugins: 'elementspath',

      toolbar: [
        {name: 'editing', items: ['Scayt', 'Find', 'Replace', 'SelectAll']},
        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
        {name: 'tools', items: ['Maximize', 'ShowBlocks', 'Preview', 'Print', 'Templates']},
        {name: 'document', items: ['Source']},
        {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe', '']},
        '/',
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'CreateDiv', '-', 'Blockquote']},
        {name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        {name: 'styles', items: ['Styles', 'Format', 'FontSize', '-', 'TextColor', 'BGColor']}
      ],

      height: 350,
      // An array of stylesheets to style the WYSIWYG area.
      // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
      contentsCss: ['https://cdn.ckeditor.com/4.9.2/full-all/contents.css', 'https://ckeditor.com/docs/ckeditor4/4.16.0/examples/assets/stylesheetparser/stylesheetparser.css'],
      stylesSet: [
        /* Inline Styles */
        {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
        {name: 'Cited Work', element: 'cite'},
        {name: 'Inline Quotation', element: 'q'},

        /* Object Styles */
        {
          name: 'Special Container',
          element: 'div',
          styles: {
            padding: '5px 10px',
            background: '#eee',
            border: '1px solid #ccc'
          }
        },
        {
          name: 'Compact table',
          element: 'table',
          attributes: {
            cellpadding: '1',
            cellspacing: '0',
            border: '1',
            bordercolor: '#ccc'
          },
          styles: {
            'border-collapse': 'collapse'
          }
        },
        {name: 'Borderless Table', element: 'table', styles: {'border-style': '1px', 'background-color': '#E6E6FA'}},
        {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
      ]
    };
  }

  // selection Change
  onChange(event) {
    if (event.value !== null) {
      if (event.value === '0') {
        this.addLetterForm.get('name').setValue('');
        this.addLetterForm.get('description').setValue('');
        this.addLetterForm.get('subject').setValue('');
        this.addLetterForm.get('body').setValue('');
      } else {
        this.getLetterTemplate(event.value);
      }
    }
  }

  // Get cases for case name field
  getCases() {
    this.patientService.getapplicationdatacases(this.appState.selectedUserLocation.id, this.patientId).subscribe((data) => {
      this.caseNames = [];
      for (let index = 0; index < data.length; index++) {
        if (data[index].caseStatus == 0 || data[index].caseStatus == 2) {
          this.caseNames.push(data[index]);
        }
      }
    });
  }

  // Get Appointments For Related Appointment Field
  getAppointments() {
    if (this.isFromState()) {
      // { id: '6f208b68-07c3-4555-8b1a-046112b60751', value: '30-Sept-2020 @ 2:30 PM - Pending' }
      const appt = this.routerState.data.appointment as BubbleAppointment;
      const data = {
        id: appt.appointmentEntryId,
        value: AppoinmentDateUtils.format(appt.startDateTime, 'DD-MMMM-YYYY @ HH:mm A') + ' - ' + appt.statusValue
      };
      this.appointments = [data];
    } else {
      /**
       * Fetch Data From API
       */
    }
  }

  // Get Letter Template By Selected Templete ID
  getLetterTemplate(id) {
    this.settingsService.getallbusinesstemplatesById(id).subscribe(s => {
      this.addLetterForm.get('name').setValue(s.name);
      this.addLetterForm.get('description').setValue(s.description);
      this.addLetterForm.get('subject').setValue(s.subject);
      this.addLetterForm.get('body').setValue(s.body);
    });
  }

  // Add or Edit Letter
  upsert(isSaveAsDraft: boolean) {
    this.blockUI.start();
    const data = this.addLetterForm.value;
    data.editStatus = isSaveAsDraft ? LetterStatus.SaveAsDraft : LetterStatus.Finalise;
    if (this.letterId) {
      this.update(data, isSaveAsDraft);
    } else {
      this.save(data, isSaveAsDraft);
    }
  }

  private save(data: PatientLetterModel, isSaveAsDraft: boolean) {
    delete data.id;
    if (data.appointmentId === '00000000-0000-0000-0000-000000000000') {
      data.appointmentId = "";
    }
    if (data.businessTemplateId === '00000000-0000-0000-0000-000000000000') {
      data.businessTemplateId = "";
    }
    this.letterService.createPatientLetter(data).subscribe(res => {
      this.blockUI.stop();
      if (this.isFromState()) {
        this.goBackToSate();
        return;
      }
      this.duplicateID.emit(res);
      this.goBackToListing.emit(null);
      if (isSaveAsDraft) {
        this.showAlert.emit(LetterStatus.SaveAsDraft);
      } else {
        this.openPopups.emit('preview');
      }
    });
  }

  private update(data: PatientLetterModel, isSaveAsDraft: boolean) {
    if (data.appointmentId === '00000000-0000-0000-0000-000000000000') {
      data.appointmentId = "";
    }
    if (data.businessTemplateId === '00000000-0000-0000-0000-000000000000') {
      data.businessTemplateId = "";
    }
    this.letterService.modifyPatientLetter(data).subscribe(res => {
      this.blockUI.stop();
      if (this.isFromState()) {
        this.goBackToSate();
        return;
      }
      this.goBackToListing.emit(null);
      if (isSaveAsDraft) {
        this.showAlert.emit(LetterStatus.SaveAsDraft);
        // this.showAlert.emit({saveAs: LetterStatus.SaveAsDraft, isCopy: this.isCopy});
      } else {
        this.generatePatientLeters.emit(res);
        this.openPopups.emit('preview');
      }
    });
  }

  // Cancel AddLetter Form
  cancel() {
    if (this.isFromState()) {
      this.goBackToSate();
      return;
    }
    this.goBackToListing.emit(null);
  }

  // Open Popup
  openDeletePopups() {
    this.openDeleteLettePopUp = true;
  }

  // Close Popup
  closePopups() {
    this.openDeleteLettePopUp = false;
  }

  // Delete Letter
  deleteLetter() {
    this.blockUI.start();
    this.letterService.deletePatientLetterById(this.patientId, this.letterId).subscribe((res: any) => {
      if (res) {
        this.blockUI.stop();
        if (this.isFromState()) {
          this.goBackToSate();
          return;
        }
        this.goBackToListing.emit(null);
        this.showAlert.emit(LetterAction.Delete);
      }
    });
  }

  goBackToSate() {
    if (this.isFromState()) {
      if (this.routerState.fromPage == FromPage.Appointment) {
        const state: NavigationExtras = {
          state: {
            fromState: true,
            fromPage: FromPage.Letter,
            data: {initFromHistory: true}
          }
        };
        this.router.navigate(['appointment'], state);
      }
    }
  }
}
