import {Injectable} from '@angular/core';
import {BaseService} from '../../../../services/app.base.service';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../app.state';
import {Observable} from 'rxjs';
import {
  AppointmentConfirmationStatus,
  AppointmentStatus
} from '../../appointment/schedular-main/schedular/rest-service/app.appointment-tooltip.service';
import {AppointmentType} from '../../../../enum/application-data-enum';

@Injectable({
  providedIn: 'root'
})
export class PatientAppointmentService extends BaseService {

  constructor(public http: HttpClient, private appState: AppState) {
    super(http);
  }

  get<IPatientAppointment>(patientId: string, startDatetime?: Date, endDateTime?: Date): Observable<IPatientAppointment> {
    const params = {
      'locationId': this.appState.selectedUserLocationId,
      'patientId': patientId
    };
    if (startDatetime) {
      params['startDatetime'] = startDatetime.toJSON();
    }
    if (endDateTime) {
      params['endDateTime'] = endDateTime.toJSON();
    }
    return this.http.get<IPatientAppointment>(
      this.environmentSettings.apiBaseUrl + '/Patientappointments', {
        params: params
      });
  }

  update(patientId: string, appointmentsEntryId: string, confirmationStatus: AppointmentConfirmationStatus, status: AppointmentStatus, recurringData: IPatientAppointmentRecurringData, statusReasonMessage: string, isRecurringChanged = false, isNeedAttention = false) {
    return this.http.put(
      this.environmentSettings.apiBaseUrl + '/appointmentsEntryStatus', {
        'locationId': this.appState.selectedUserLocationId,
        'patientId': patientId,
        'appointmentsEntryId': appointmentsEntryId,
        'confirmationStatus': confirmationStatus,
        'status': status,
        'isRecurringChanged': isRecurringChanged,
        'recurringData': recurringData,
        'statusReasonMessage': statusReasonMessage,
        'isNeedAttention' :isNeedAttention
      });
  }

  getPlannedServices(patientId: string): Observable<IPlannedServiceList[]> {
    return this.http.get<IPlannedServiceList[]>(
      this.environmentSettings.apiBaseUrl + '/allPlannedService', {
        params: {
          'locationId': this.appState.selectedUserLocationId,
          'patientId': patientId
        }
      });
  }

  plannedService(data: any) {
    return this.http.post(this.environmentSettings.apiBaseUrl + '/plannedService', data);
  }

  updatePlannedService(data: any) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/plannedService', data);
  }

  deletePlannedService(patientId: string, patientPlannedServiceId: string) {
    return this.http.delete(this.environmentSettings.apiBaseUrl + '/plannedService', {
      params: {
        'locationId': this.appState.selectedUserLocationId,
        'patientId': patientId,
        'patientPlannedServiceId': patientPlannedServiceId
      }
    });
  }

  patientAlert(patientId: string, message: string) {
    const data = {
      'patientId': patientId,
      'locationId': this.appState.selectedUserLocationId,
      'type': PatientAlertsType.AppointmentAlert,
      'message': message,
      'title': '',
      'status': PatientAlertsStatus.Active,
      'priority': PatientAlertsPriority.Low,
    };
    return this.http.post(this.environmentSettings.apiBaseUrl + '/patientAlerts', data);
  }

  updatePatientAlert(data: IPatientAlert) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/patientAlerts', data);
  }

  deletePatientAlert(patientAlertsId: number, patientId: string) {
    return this.http.delete(this.environmentSettings.apiBaseUrl + '/plannedService', {
      params: {
        'locationId': this.appState.selectedUserLocationId,
        'patientId': patientId,
        'patientAlertsId': patientAlertsId.toString()
      }
    });
  }

  getPatientAlerts(patientId: string) {
    const params = {
      'locationId': this.appState.selectedUserLocationId,
      'patientId': patientId
    };
    return this.http.get<IPatientAlert[]>(
      this.environmentSettings.apiBaseUrl + '/allPatientAlerts', {
        params: params
      });
  }
}

export interface IPatientAppointment {
  headerData: IPatientAppointmentHeaderData,
  gridData: IPatientAppointmentGrid[],
  recurringData: IPatientAppointmentRecurringData[]
}

export interface IPatientAppointmentHeaderData {
  totalBooking: number,
  completedBooking: number,
  missedBooking: number,
  cancelledBooking: number,
  futureBooking: number,
  completedBookingThisYear: number,
  daysSinceLastVisit: number,
}

export interface IPatientAppointmentGrid {
  id: string,
  appointmentsId: string,
  startDateUTC: Date,
  endDateUTC: Date,
  startDateUTCStr: String,
  type: number,
  offerings: string[],
  practitionerId: string,
  practitionerName: string,
  caseId: string,
  caseName: string,
  referralName: string,
  referralOrdered: number,
  referralCount: number,
  payerName: string,
  payerOrdered: number,
  payerCount: number,
  status: number,
  confirmationStatus: number,
  note: string,
  currentStatus: string
}

export interface IPatientAppointmentRecurringData {
  'appointmentsId': string,
  'recurrenceRule': string,
  'recurrenceException': string,
  'startDateUTC': Date,
  'endDateUTC': Date,
  'status': AppointmentStatus,
  'type': AppointmentType,
}

/**  PLANNED SERVICES */
export interface IPlannedServiceList {
  id: string,
  practitionerId: string,
  patientId: string,
  locationId: string,
  serviceId: string,
  serviceCode: string,
  serviceName: string,
  duration: number,
  practitionerName: string,
  notes: string,
  isUsed: boolean,
  rowIndex: number,
  isStatus: boolean,
  createdBy: string,
  createdDate: string,
  modifiedBy: string,
  modifiedDate: Date
}
export interface IPatientAlert {
  "id": number,
  "patientId": string,
  "locationId": string,
  "type": PatientAlertsType,
  "message": string,
  "title": string,
  "status": PatientAlertsStatus,
  "priority": PatientAlertsPriority,
  "createdDate": Date,
  "createdBy": string,
  "modifiedDate": Date,
  "modifiedBy": string
}

export enum PatientAlertsType {
  Default,
  AppointmentAlert,
}

export enum PatientAlertsStatus {
  Active,
  Inactive,
}

export enum PatientAlertsPriority {
  Low,
  High,
}

interface test {

}
