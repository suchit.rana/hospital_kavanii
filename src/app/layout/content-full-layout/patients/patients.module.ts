// import { DevExtremeModule, DxTemplateHost } from "devextreme-angular";
import { DxDateBoxModule } from 'devextreme-angular/ui/date-box';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule, MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
} from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
// import { ColorPickerModule } from 'ngx-color-picker';
// import { IntlModule } from '@progress/kendo-angular-intl';
import { InputsModule } from '@progress/kendo-angular-inputs';
// import { SharedModule } from '../../../shared.modules';
import { BlockUIModule } from 'ng-block-ui';
// import { OrderModule } from 'ngx-order-pipe';
import { CalendarModule, DateInputsModule, TimePickerModule, } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { PatientsRoutingModule } from './patients-routing.module';
import { PatientsComponent } from './patients.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { PatientContactComponent } from './patient-contact/patient-contact.component';
import { PatientCaseComponent } from './patient-case/patient-case.component';
import { PatientAppointmentsComponent } from './patient-appointments/patient-appointments.component';
import { PatientTreatmentnotesComponent } from './patient-treatmentnotes/patient-treatmentnotes.component';
import { PatientLettersComponent } from './patient-letters/patient-letters.component';
import { PatientBillingComponent } from './patient-billing/patient-billing.component';
import { PatientAlertrecallComponent } from './patient-alertrecall/patient-alertrecall.component';
import { PatientCommmunicationsComponent } from './patient-commmunications/patient-commmunications.component';
import { PatientDocumentsComponent } from './patient-documents/patient-documents.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PatientTabContentComponent } from './patient-tab-content/patient-tab-content.component';
import { EditPatientComponent } from './edit-patient/edit-patient.component';
import { PatientSummaryComponent } from './patient-summary/patient-summary.component';
import { PatientControlComponent } from './patient-control/patient-control.component';
import { MergePatientComponent } from './merge-patient/merge-patient.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { AdddocumentsComponent } from './adddocuments/adddocuments.component';
import { EmailtemplateComponent } from './emailtemplate/emailtemplate.component';
import { PreviewdocumentComponent } from './previewdocument/previewdocument.component';
// import { ImgViewerModule } from 'ng-picture-viewer';
import { NgImageFullscreenViewModule } from 'ng-image-fullscreen-view';
import { ProgressBarModule } from '@progress/kendo-angular-progressbar';
// import { TooltipModule } from '@progress/kendo-angular-tooltip';
// import { EditorModule } from '@progress/kendo-angular-editor';
// import { MaterialTimePickerModule } from '../../../material-timepicker/material-timepicker.module';
import { MatSliderModule } from '@angular/material/slider';
import { BodyChartComponent } from './patient-treatmentnotes/body-chart/body-chart.component';
import { SpineChartComponent } from './patient-treatmentnotes/spine-chart/spine-chart.component';
import { ItemTypePreviewComponent } from './patient-treatmentnotes/item-type-preview/item-type-preview.component';
import { TreatmentNotesService } from '../../../services/app.treatmentnotes.services';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { LabelModule } from '@progress/kendo-angular-label';
import { DateAgoPipe } from '../../../shared/date-ago.pipe';
import { CKEditorModule } from 'ckeditor4-angular';
import { PatientCasescontactsComponent } from './patient-casescontacts/patient-casescontacts.component';
import { CustomDatePipe } from '../../../pipe/custom-date.pipe';
import { SharedContentFullLayoutModule } from '../shared-content-full-layout/shared-content-full-layout.module';
import { AppCommonModule } from '../common/app-common.module';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { ManagingModule } from '../../module/managing.module';
import { FormsComponent } from './forms/forms.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';
import { AddGeneralModule } from '../contacts/add-general/add-general.module';
import { DxCalendarModule } from 'devextreme-angular/ui/calendar';
import { AppCalenderPopupModule } from '../common/calender-popup/app-calender-popup.module';
import { AppTimePipeModule } from '../../../pipe/module/app-time-pipe.module';
import { AppTimePickerModule } from '../common/time-picker/app-time-picker.module';
import { AppDatePickerModule } from '../common/app-date-picker/app-date-picker.module';
import { CharacterLimitModule } from 'src/app/directive/character-limit/character-limit.module';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { PopupModule } from '@progress/kendo-angular-popup';
import { EditorModule } from '@progress/kendo-angular-editor';
import { AddLetterComponent } from './patient-letters/add-letter/add-letter.component';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { LetterTagListModule } from '../../../shared/letter-tag-list/letter-tag-list.module';
import { AppKendoMultiSelectModule } from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import { AddressFieldModule } from '../shared-content-full-layout/address-field/address-field.module';
import { MatCardModule } from '@angular/material/card';
import { DxPopoverModule } from 'devextreme-angular';
import { AppDateDiffPipeModule } from '../../../pipe/module/app-date-diff-pipe.module';
import {
    AddEditPlannedServicesComponent
} from './patient-appointments/component/add-edit-planned-services/add-edit-planned-services.component';
import { OnlyNumberModule } from '../../../directive/only-number/only-number.module';
import {
    ApptDropDownAddNewModule
} from '../appointment/schedular-main/schedular/shared/appt-drop-down-add-new/appt-drop-down-add-new.module';
import { PlannedServicesComponent } from './patient-appointments/component/planned-services/planned-services.component';
import { AppointmentsClassesComponent } from './patient-appointments/component/appointments-classes/appointments-classes.component';
import { AppointmentPopupModule } from './patient-appointments/appointment-popup/appointment-popup.module';
import { PreviewLetterModule } from './patient-letters/preview-letter/preview-letter.module';
import { BillingModule } from '../billing/billing.module';
import { AccessDirectiveModule } from '../../../shared/directives/access-directive/access-directive.module';
import { CommunicationsModule } from '../communications/communications.module';
import { SharedCommunicationModule } from '../communications/shared/shared-communication.module';
import { GenerateStatementPopupComponent } from './patient-billing/dialog/generate-statement-popup/generate-statement-popup.component';
import { AppDateRangePickerModule } from '../common/app-date-range-picker/app-date-range-picker.module';
import { AppDateRangeDurationModule } from '../common/app-date-range-duration/app-date-range-duration.module';
import { DropdownTreeViewModule } from '../../../shared/dropdown-treeview/dropdown-tree-view.module';
import { InvoiceFormComponent } from '../billing/invoice-form/invoice-form.component';

@NgModule({
    declarations: [
        PatientsComponent,
        AddPatientComponent,
        PatientDetailsComponent,
        PatientContactComponent,
        PatientCaseComponent,
        PatientAppointmentsComponent,
        PatientTreatmentnotesComponent,
        PatientLettersComponent,
        PatientBillingComponent,
        PatientAlertrecallComponent,
        PatientCommmunicationsComponent,
        PatientDocumentsComponent,
        PatientTabContentComponent,
        EditPatientComponent,
        PatientSummaryComponent,
        PatientControlComponent,
        MergePatientComponent,
        AdddocumentsComponent,
        EmailtemplateComponent,
        PreviewdocumentComponent,
        BodyChartComponent,
        SpineChartComponent,
        ItemTypePreviewComponent,
        DateAgoPipe,
        PatientCasescontactsComponent,
        CustomDatePipe,
        FormsComponent,
        AuditTrailComponent,
        AddLetterComponent,
        AddEditPlannedServicesComponent,
        PlannedServicesComponent,
        AppointmentsClassesComponent,
        GenerateStatementPopupComponent
    ],
    imports: [
        CommonModule,
        AppCommonModule,
        ManagingModule,
        SharedContentFullLayoutModule,
        AddGeneralModule,
        // SharedModule,
        PatientsRoutingModule,
        MatListModule,
        MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatCheckboxModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDividerModule,
        MatMenuModule,
        MatListModule,
        MatTooltipModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        MatTabsModule,
        MatRadioModule,
        MatCardModule,
        // ColorPickerModule,
        FormsModule,
        MatInputModule,
        MatTableModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        DateInputsModule,
        CalendarModule,
        // IntlModule,
        TimePickerModule,
        InputsModule,
        MatButtonToggleModule,
        MatIconModule,
        BlockUIModule.forRoot(),
        ButtonsModule,
        MatDialogModule,
        // OrderModule,
        LayoutModule,
        InputsModule,
        DialogModule,
        MatProgressBarModule,
        NgImageFullscreenViewModule,
        DropDownsModule,
        ProgressBarModule,
        // TooltipModule,
        // EditorModule,
        // MaterialTimePickerModule,
        MatSliderModule,
        UploadsModule,
        LabelModule,
        CKEditorModule,
        DxDateBoxModule,
        DxCalendarModule,
        // DevExtremeModule,
        MatAutocompleteModule,
        AppCalenderPopupModule,
        AppTimePipeModule,
        AppTimePickerModule,
        AppDatePickerModule,
        CharacterLimitModule,
        TreeViewModule,
        PopupModule,
        EditorModule,
        PDFExportModule,
        DxPopoverModule,

        LetterTagListModule,
        AppKendoMultiSelectModule,
        AddressFieldModule,
        AppTimePipeModule,
        AppDateDiffPipeModule,
        OnlyNumberModule,
        ApptDropDownAddNewModule,
        AppointmentPopupModule,
        PreviewLetterModule,
        BillingModule,
        AccessDirectiveModule,
        CommunicationsModule,
        SharedCommunicationModule,
        MatChipsModule,
        AppDateRangePickerModule,
        AppDateRangeDurationModule,
        DropdownTreeViewModule
    ],
    entryComponents: [
        AddPatientComponent,
        PatientDetailsComponent,
        PatientContactComponent,
        PatientCaseComponent,
        PatientAppointmentsComponent,
        PatientTreatmentnotesComponent,
        PatientLettersComponent,
        PatientBillingComponent,
        PatientAlertrecallComponent,
        PatientCommmunicationsComponent,
        PatientDocumentsComponent,
        AdddocumentsComponent,
        EmailtemplateComponent,
        PreviewdocumentComponent,
        PatientCasescontactsComponent,
        AddEditPlannedServicesComponent,
        GenerateStatementPopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [TreatmentNotesService], //DxTemplateHost
})
export class PatientsModule {
}
