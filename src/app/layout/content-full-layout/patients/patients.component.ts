import { ApplicationDataEnum } from 'src/app/enum/application-data-enum';
import { ApplicationDataService } from 'src/app/services/app.applicationdata.service';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AppState } from '../../../app.state';
import { PatientModel } from '../../../models/app.patient.model';
import { PatientService } from '../../../services/app.patient.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatButtonToggleChange } from '@angular/material';
import { BaseGridComponent } from '../shared-content-full-layout/base-grid/base-grid.component';
import '@vaadin/vaadin-time-picker/vaadin-time-picker.js';
import * as _ from "lodash";
import { async } from 'rxjs/internal/scheduler/async';
import {RP_MODULE_MAP} from '../../../shared/model/RolesAndPermissionModuleMap';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css'],
})
export class PatientsComponent extends BaseGridComponent implements OnInit {
  readonly PATIENT_MERGE = RP_MODULE_MAP.patients_basic_access;
  readonly PATIENT_LETTER = RP_MODULE_MAP.patients_letters;
  readonly PATIENT_BILL = RP_MODULE_MAP.patients_bills;
  readonly PATIENT_DOC = RP_MODULE_MAP.patients_documents;
  readonly PATIENT_TN = RP_MODULE_MAP.patients_treatment_notes;
  moduleName = RP_MODULE_MAP.patients_basic_access;

  @BlockUI() blockUI: NgBlockUI;
  apperance = 'outline';
  patients: PatientModel[];
  isRoot = false;
  isError = false;
  patientTitle = '';
  patientRouteLink = '';
  patientRouteName = '';
  patientDescription = '';
  isLoading: boolean;
  patientShow: boolean;
  dataLoaded: boolean;
  status: 'active';
  displaySuccessMessage: any;
  allTitles: any = [];

  constructor(
    protected router: Router,
    public appState: AppState,
    private patientService: PatientService,
    public applicationDataService: ApplicationDataService,
  ) {
    super(null, appState);
  }

  ngOnInit() {
    this.getAllTitle();
    this.appState.selectedUserLocationIdState.subscribe((locationId) => {
      this.populateLanding();
      this.router.navigate(["/patients"]);
    });

    this.isRoot = this.router.url === '/patients';
    if (this.isRoot && !this.dataLoaded) {
      this.populateLanding();
    }
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isRoot = event.url === '/patients';
        if (this.isRoot) {
          this.populateLanding();
        }
      }
    });
    this.setActiveFilter();
  }

  getAllTitle() {
    this.allTitles = [];
    this.applicationDataService
      .GetApplicationDataByCategoryId(
        ApplicationDataEnum.Title,
        this.appState.selectedUserLocationId
      )
      .subscribe((data) => {
        console.log(data)
        this.allTitles = data;
      });
  }

  populateLanding() {
    this.blockUI.start();
    this.dataLoaded = true;
    this.patientService.getallpatientsmininfobylocationid(this.appState.selectedUserLocationId).subscribe((data: any) => {
      if (data.length === 0) {
        this.patientTitle = 'You haven\'t added a Patient';
        this.patientRouteLink = '/patients/add';
        this.patientRouteName = 'Add Patient';
        this.patientDescription =
          'Patient is recipient of allied health ervices from pratitoner in our Business Location';
        this.patientShow = false;
      } else {
        this.patientTitle = '';
        this.patientShow = true;
        for (let index = 0; index < data.length; index++) {
          data[index].name = data[index].firstName + " " + data[index].lastName
          if (data[index].title != null && data[index].title != undefined) {
            let dataNew = _.find(this.allTitles, { id: data[index].title })
            if (dataNew != undefined) {
              if (dataNew.categoryName != undefined) {
                data[index].name = dataNew.categoryName + " " + data[index].firstName + " " + data[index].lastName
              }
            }
          }
        }
      }
      this.gridData = data;
      this.loadItems('name');
    });
    this.isLoading = false;
    this.blockUI.stop();
    this.displayMessage();
  }

  patientActiveChanged(event: MatButtonToggleChange) {
    if (event.value == 'active') {
      this.setActiveFilter();
    } else {
      this.setInactiveFilter();
    }
    this.loadItems();
  }

  setActive() {
    this.setActiveFilter();
  }

  setInactive() {
    this.setInactiveFilter();
  }

  displayMessage() {
    if (
      this.patientService.sharedData !== undefined &&
      this.patientService.sharedData !== ''
    ) {
      //this.displaySuccessMessage(this.patientService.sharedData);
      this.patientService.sharedData = '';
    }

  }
}
