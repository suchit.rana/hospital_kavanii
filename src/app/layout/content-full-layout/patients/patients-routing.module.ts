import { FormsComponent } from './forms/forms.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientsComponent } from './patients.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { PatientContactComponent } from './patient-contact/patient-contact.component';
import { PatientCaseComponent } from './patient-case/patient-case.component';
import { PatientAppointmentsComponent } from './patient-appointments/patient-appointments.component';
import { PatientTreatmentnotesComponent } from './patient-treatmentnotes/patient-treatmentnotes.component';
import { PatientLettersComponent } from './patient-letters/patient-letters.component';
import { PatientBillingComponent } from './patient-billing/patient-billing.component';
import { PatientAlertrecallComponent } from './patient-alertrecall/patient-alertrecall.component';
import { PatientCommmunicationsComponent } from './patient-commmunications/patient-commmunications.component';
import { PatientDocumentsComponent } from './patient-documents/patient-documents.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { EditPatientComponent } from './edit-patient/edit-patient.component';
import { MergePatientComponent } from './merge-patient/merge-patient.component';
import { PatientCasescontactsComponent } from './patient-casescontacts/patient-casescontacts.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';
import { InvoiceFormComponent } from '../billing/invoice-form/invoice-form.component';

const routes: Routes = [
  {
    path: '',
    component: PatientsComponent,
    children: [
      {
        path: 'add',
        component: AddPatientComponent,
      },
      {
        path: 'patients/:patientId',
        component: AddPatientComponent,
      },
      {
        path: 'merge',
        component: MergePatientComponent,
      },
      {
        path: 'edit/:patientId',
        component: EditPatientComponent,
      },
      {
        path: 'details',
        component: PatientDetailsComponent,
      },
      {
        path: 'contacts',
        component: PatientContactComponent,
      },
      {
        path: 'case',
        component: PatientCaseComponent,
      },
      {
        path: 'casescontacts',
        component: PatientCasescontactsComponent,
      },
      {
        path: 'casescontacts/:patientId',
        component: PatientCasescontactsComponent,
      },
      {
        path: 'appointments',
        component: PatientAppointmentsComponent,
      },
      {
        path: 'treatmentnotes',
        component: PatientTreatmentnotesComponent,
      },
      {
        path: 'letters',
        component: PatientLettersComponent,
      },
      {
        path: 'billing',
        component: PatientBillingComponent,
      },
      {
        path: 'alertrecall',
        component: PatientAlertrecallComponent,
      },
      {
        path: 'Commmunications',
        component: PatientCommmunicationsComponent,
      },
      {
        path: 'documents',
        component: PatientDocumentsComponent,
      },
      {
        path: 'forms',
        component: FormsComponent,
      },
      {
        path: 'auditTrail',
        component: AuditTrailComponent,
      },
      {
        path: 'biils/add',
        component: InvoiceFormComponent,
        data: {
            billingPage: false
        }
    },
    {
        path: 'bills/edit/:invoiceId',
        component: InvoiceFormComponent,
        data: {
            billingPage: false
        }
    },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientsRoutingModule { }
