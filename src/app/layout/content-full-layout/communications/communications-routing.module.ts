import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CommunicationsComponent} from './communications.component';
import {BulkComponent} from './component/bulk/bulk.component';
import {CampaignComponent} from './component/campaign/campaign.component';
import {ListComponent} from './component/list/list.component';
import {ListDetailsComponent} from './component/list/list-details/list-details.component';

const routes: Routes = [
    {path: '', redirectTo: 'bulk', pathMatch: 'full'},
    {
        path: '',
        component: CommunicationsComponent,
        children: [
            {path: 'bulk', component: BulkComponent},
            {path: 'campaign', component: CampaignComponent},
            {path: 'list', component: ListComponent},
            {path: 'list/add', component: ListDetailsComponent},
            {path: 'list/edit/:id', component: ListDetailsComponent},
            {path: 'list/copy/:id', component: ListDetailsComponent},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommunicationsRoutingModule {
}
