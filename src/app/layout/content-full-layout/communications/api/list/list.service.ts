import {Injectable} from '@angular/core';
import {BaseService} from '../../../../../services/app.base.service';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {CommunicationList} from '../../../patients/patient-commmunications/interface/communication-list';
import {Communication} from '../../model/communication.interface';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../../app.state';
import {GridList} from '../../model/gridList';


@Injectable({
    providedIn: 'root'
})
export class ListService extends BaseService {

    constructor(
        public http: HttpClient,
        private appState: AppState
    ) {
        super(http);
    }

    getAll(locationId: string) {
        const data = {locationId};
        return this.http.get<GridList[]>(this.environmentSettings.apiBaseUrl + '/GetAllCampaignList', {
            params: data
        });
    }

    getById(id: string) {
        return this.http.get<GridList>(this.environmentSettings.apiBaseUrl + '/GetCampaignListById', {params: {id}});
    }

    create(data: Communication) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/CreateCampaignList', data);
    }

    update(data: Communication | GridList) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/UpdateCampaignList', data);
    }

    delete(id: string) {
        return this.http.delete(this.environmentSettings.apiBaseUrl + '/DeleteCampaignList', {params: {id}});
    }

    basicSearchCampaignEmailLists(data: BasicSearchFilter) {
        data.locationId = this.appState.selectedUserLocationId;

        if (data.dob) {
            data.dob = AppoinmentDateUtils.toUtc(data.dob);
        }
        removeEmptyKey(data);
        console.log(data)
        return this.http.post<CommunicationList>(this.environmentSettings.apiBaseUrl + '/BasicSearchCampaignEmailLists', data);
    }

    listBasedSearchCampaignEmailLists(data: ListBasedSearchFilter) {
        data.locationId = this.appState.selectedUserLocationId;

        if (data.firstVisitStartDate) {
            data.firstVisitStartDate = AppoinmentDateUtils.toUtc(data.firstVisitStartDate);
        }
        if (data.firstVisitEndDate) {
            data.firstVisitEndDate = AppoinmentDateUtils.toUtc(data.firstVisitEndDate);
        }
        if (data.dobStartDate) {
            data.dobStartDate = AppoinmentDateUtils.toUtc(data.dobStartDate);
        }
        if (data.dobEndDate) {
            data.dobEndDate = AppoinmentDateUtils.toUtc(data.dobEndDate);
        }
        removeEmptyKey(data);
        return this.http.post<CommunicationList>(this.environmentSettings.apiBaseUrl + '/ListBasedSearchCampaignEmailLists', data);
    }

    advancedSearchCampaignEmailLists(data: AdvancedSearchFilter) {
        data.locationId = this.appState.selectedUserLocationId;
        if (data.createdDateStartDate) {
            data.createdDateStartDate = AppoinmentDateUtils.toUtc(data.createdDateStartDate);
        }
        if (data.createdDateEndDate) {
            data.createdDateEndDate = AppoinmentDateUtils.toUtc(data.createdDateEndDate);
        }
        removeEmptyKey(data);
        return this.http.post<CommunicationList>(this.environmentSettings.apiBaseUrl + '/AdvancedSearchCampaignEmailLists', data);
    }

}

interface Filter {
    locationId: string;
}

export interface BasicSearchFilter extends Filter {
    firstName: string,
    lastName: string,
    city: string,
    state: string,
    postCode: string,
    dob: Date | string
}

export interface ListBasedSearchFilter extends Filter {
    firstVisitStartDate: Date | string,
    firstVisitEndDate: Date | string,
    dobStartDate: Date | string,
    dobEndDate: Date | string,
    gender: string,
    status: string,
    position: string,
    occupation: string,
    patientClassification: string,
    marketingReferralSource: string,
    concession: string,
    primaryProvider: string
}

export interface AdvancedSearchFilter extends Filter {
    referralPractitionerId: string,
    practitionerId: string,
    offeringType: string,
    serviceId: string,
    createdDateStartDate: Date | string,
    createdDateEndDate: Date | string
}


function removeEmptyKey<T>(obj: Object): T {
    Object.keys(obj).map(key => {
        if (!obj[key]) {
            delete obj[key];
        }
    });
    return (obj) as T;
}
