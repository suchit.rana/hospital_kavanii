import {Injectable} from '@angular/core';
import {CampaignModel} from '../../model/campaign';
import {BaseService} from '../../../../../services/app.base.service';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {CommunicationList} from '../../../patients/patient-commmunications/interface/communication-list';
import {Communication} from '../../model/communication.interface';


@Injectable({
    providedIn: 'root'
})
export class CommunicationService extends BaseService {

    getAll(locationId: string, startDate: Date, endDate: Date) {
        const data = {locationId};
        if (startDate) {
            data['startDate'] = AppoinmentDateUtils.toUtc(startDate);
        }
        if (endDate) {
            data['endDate'] = AppoinmentDateUtils.toUtc(endDate);
        }
        return this.http.get<CommunicationList[]>(this.environmentSettings.apiBaseUrl + '/GetAllCommunication', {
            params: data
        });
    }

    getAllForPatient(locationId: string, patientId: string) {
        const data = {locationId, patientId};
        return this.http.get<CommunicationList[]>(this.environmentSettings.apiBaseUrl + '/GetAllCommunicationByPatientId', {
            params: data
        });
    }

    getById(id: string) {
        return this.http.get<CommunicationList>(this.environmentSettings.apiBaseUrl + '/GetCommunicationById', {params: {id}});
    }

    create(data: Communication) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/CreateCommunication', data);
    }

    update(data: Communication) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/UpdateCommunication', data);
    }

    delete(id: string) {
        return this.http.delete(this.environmentSettings.apiBaseUrl + '/DeleteCommunication', {params: {id}});
    }

}
