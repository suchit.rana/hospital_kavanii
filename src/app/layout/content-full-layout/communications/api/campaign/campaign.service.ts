import {Injectable} from '@angular/core';
import {CampaignModel} from '../../model/campaign';
import {BaseService} from '../../../../../services/app.base.service';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';


@Injectable({
    providedIn: 'root'
})
export class CampaignService extends BaseService {

    getAll(locationId: string) {
        const data = {locationId};
        return this.http.get<CampaignModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllCampaign', {
            params: data
        });
    }

    getById(id: string) {
        return this.http.get<CampaignModel>(this.environmentSettings.apiBaseUrl + '/GetCampaignById', {params: {id}});
    }

    create(data: CampaignModel) {
        return this.http.post<string>(this.environmentSettings.apiBaseUrl + '/CreateCampaign', data);
    }

    update(data: CampaignModel) {
        return this.http.post<string>(this.environmentSettings.apiBaseUrl + '/UpdateCampaign', data);
    }

    delete(id: string) {
        return this.http.delete(this.environmentSettings.apiBaseUrl + '/DeleteCampaign', {params: {id}});
    }

}
