import {Injectable} from '@angular/core';
import {CampaignModel} from '../../model/campaign';
import {BaseService} from '../../../../../services/app.base.service';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {CommunicationList} from '../../../patients/patient-commmunications/interface/communication-list';
import {Communication} from '../../model/communication.interface';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../../app.state';
import {ContactTypeEnum} from '../../../../../enum/application-data-enum';


@Injectable({
    providedIn: 'root'
})
export class EmailSmsService extends BaseService {

    constructor(public http: HttpClient, private appState: AppState) {
        super(http);
    }

    getAllContactEmail(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<{ contactType: ContactTypeEnum, emailId: string }[]>(this.environmentSettings.apiBaseUrl + '/GetAllContactEmail', {
            params: params
        });
    }

    getAllPatientEmergencyEmail(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<string[]>(this.environmentSettings.apiBaseUrl + '/GetAllPatientEmergencyEmail', {
            params: params
        });
    }

    getAllPatientEmail(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<string[]>(this.environmentSettings.apiBaseUrl + '/GetAllPatientEmail', {
            params: params
        });
    }


    getAllContactMobileNo(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<{ contactType: ContactTypeEnum, emailId: string }[]>(this.environmentSettings.apiBaseUrl + '/GetAllContactMobileNo', {
            params: params
        });
    }

    getAllPatientEmergencyMobileNo(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<string[]>(this.environmentSettings.apiBaseUrl + '/GetAllPatientEmergencyMobileNo', {
            params: params
        });
    }

    getAllPatientMobileNo(patientId: string) {
        const params = {locationId: this.locationId()};
        if (patientId) {
            params['patientId'] = patientId;
        }
        return this.http.get<string[]>(this.environmentSettings.apiBaseUrl + '/GetAllPatientMobileNo', {
            params: params
        });
    }

    private locationId(): string {
        return this.appState.selectedUserLocationId;
    }

}
