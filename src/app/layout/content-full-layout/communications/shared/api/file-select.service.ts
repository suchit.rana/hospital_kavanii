import {Injectable} from '@angular/core';
import {AppDialogService} from '../../../../../shared/services/app-dialog.service';
import {AttachmentsPreviewComponent} from '../attachments-preview/attachments-preview.component';


@Injectable({
    providedIn: 'root'
})
export class FileSelectService {

    private fileInput: HTMLInputElement;
    private _files: FileList;
    public selectedFiles: {name: string, size: string, content: any}[] = [];
    private appDialogService: AppDialogService;

    constructor(
    ) {
    }

    selectFile() {
        this.fileInput = document.getElementById('fileInputTemp') as HTMLInputElement;
        if (!this.fileInput) {
            this.fileInput = document.createElement('input');
            this.fileInput.type = 'file';
            this.fileInput.multiple = true;
            this.fileInput.id = 'fileInputTemp';

            this.fileInput.onchange = () => {
                this._files = this.fileInput.files;
                this.selectedFiles = [];
                for (let i = 0; i < this._files.length; i++) {
                    this.blobToBase64(this._files.item(i)).then(result => {
                        this.selectedFiles.push({
                            name: this._files.item(i).name,
                            size: this.formatBytes(this._files.item(i).size),
                            content: result
                        });
                    });
                }
            };
        }
        this.fileInput.click();
    }

    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    private blobToBase64 = blob => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        return new Promise(resolve => {
            reader.onloadend = () => {
                resolve(reader.result);
            };
        });
    };

    get files(): FileList {
        return this._files;
    }

    set dialogService(service: AppDialogService) {
        this.appDialogService = service;
    }

    previewImage(i: number) {
        AttachmentsPreviewComponent.open(this.appDialogService, {data: this.selectedFiles[i].content});
    }

    remove(i: number) {
        this.selectedFiles.splice(i, 1);
    }

    clear() {
        this.selectedFiles = [];
        this._files = null;
    }
}
