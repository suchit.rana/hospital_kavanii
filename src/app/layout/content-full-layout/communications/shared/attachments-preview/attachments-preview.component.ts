import {Component, Input, OnInit, Sanitizer} from '@angular/core';
import {AppDialogService} from '../../../../../shared/services/app-dialog.service';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-attachments-preview',
  templateUrl: './attachments-preview.component.html',
  styleUrls: ['./attachments-preview.component.css']
})
export class AttachmentsPreviewComponent implements OnInit {

  @Input()
  data: string;

  constructor(
      public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    console.log(this.data)
  }

  static open(dialogService: AppDialogService, data: {[x: string]: any}) {
    dialogService.open(AttachmentsPreviewComponent, {
      title: 'PREVIEW',
      width: '500px',
      height: '500px',
      hideSave: true,
      data
    });
  }

}
