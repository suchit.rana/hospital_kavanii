import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-bulk-email',
    templateUrl: './bulk-email.component.html',
    styleUrls: ['./bulk-email.component.css']
})
export class BulkEmailComponent implements OnInit {

    ckeConfig: any;

    categories = [
        {text: 'Patient Email', value: 2},
        {text: 'Patient Emergency Contact', value: 3},
        {text: 'Referral Contact', value: 4},
        {text: 'General Contact', value: 5},
        {text: 'TPP Contact', value: 6},
    ];

    form: FormGroup = new FormGroup({
        category: new FormControl('', [Validators.required]),
        to: new FormControl('', [Validators.required]),
        template: new FormControl('', [Validators.required]),
        subject: new FormControl('', [Validators.required]),
        tags: new FormControl('', [Validators.required]),
        message: new FormControl('', [Validators.required]),
    });

    constructor() {
    }

    ngOnInit() {
    }

    cancel() {

    }

    save() {

    }
}
