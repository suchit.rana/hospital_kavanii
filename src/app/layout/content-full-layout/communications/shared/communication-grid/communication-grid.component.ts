import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CommunicationType} from '../../../patients/patient-commmunications/enum/communication-type';
import {CommunicationList} from '../../../patients/patient-commmunications/interface/communication-list';
import {DxPopoverComponent} from 'devextreme-angular';
import {FormControl} from '@angular/forms';
import {AppDialogService} from '../../../../../shared/services/app-dialog.service';
import {CommunicationPreviewComponent} from '../communication-preview/communication-preview.component';
import {MatCheckboxChange} from '@angular/material';
import {BaseGridComponent} from '../../../shared-content-full-layout/base-grid/base-grid.component';
import {CommunicationService} from '../../api/communication/communication.service';
import {AppState} from '../../../../../app.state';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {CommunicationStatus} from '../../model/communication.interface';
import {AttachmentsPreviewComponent} from '../attachments-preview/attachments-preview.component';
import {RP_MODULE_MAP} from '../../../../../shared/model/RolesAndPermissionModuleMap';

export const PREVIEW = 'PREVIEW';
export const PRINT = 'PRINT';
export const EDIT = 'EDIT';
export const DELETE = 'DELETE';
export const DOWNLOAD = 'DOWNLOAD';

@Component({
    selector: 'app-communication-grid',
    templateUrl: './communication-grid.component.html',
    styleUrls: ['./communication-grid.component.css']
})
export class CommunicationGridComponent extends BaseGridComponent implements OnInit {
    moduleName = RP_MODULE_MAP.patients_basic_access;

    readonly PREVIEW = PREVIEW;
    readonly PRINT = PRINT;
    readonly EDIT = EDIT;
    readonly DELETE = DELETE;
    readonly DOWNLOAD = DOWNLOAD;
    readonly FROM_DATE = 'FROM_DATE';
    readonly TO_DATE = 'TO_DATE';
    readonly Email = CommunicationType.Email;
    readonly Sms = CommunicationType.Sms;
    readonly Note = CommunicationType.Note;

    actionItem: CommunicationList; // Item that was selected through action(GEAR ICON)
    @BlockUI() blockUI: NgBlockUI;
    @ViewChild('actionPopup', {static: true}) actionPopup: DxPopoverComponent;
    @ViewChild('dateFilter', {static: false}) dateFilter;
    @ViewChild('typeFilter', {static: false}) typeFilter;

    @Input() patientId: string;
    @Input() fromTab: boolean = false;
    @Output()
    action: EventEmitter<{ action: string, item: CommunicationList }> = new EventEmitter<{ action: string, item: CommunicationList }>();

    /** Date filter control */
    fromDateCtrl = new FormControl();
    toDateCtrl = new FormControl();
    isDisplayClear = false;
    selectedType: string[] = [];

    communications: CommunicationList[];
    commTypes = [
        {id: '0', type: this.Sms},
        {id: '1', type: this.Email},
        {id: '2', type: this.Note},
    ];

    constructor(
        protected appState: AppState,
        private appDialogService: AppDialogService,
        private communicationService: CommunicationService
    ) {
        super();
    }

    ngOnInit() {
        this.loadCommunication();
        if (!this.fromTab) {
            this.commTypes.splice(2, 1);
        }
    }

    openBulkActionPopup(dataItem, event) {
        this.actionItem = dataItem;

        this.actionPopup.target = event.target;
        this.actionPopup.instance.show();
    }

    doAction(action: string) {
        if (!this.actionItem) {
            return;
        }
        if (this.actionPopup) {
            this.actionPopup.instance.hide();
        }
        this.action.emit({action, item: this.actionItem});
        // const dialogRef = this.appDialogService.open(CommunicationPreviewComponent, {
        //     title: 'PREVIEW',
        //     width: '778px',
        //     maxHeight: '750px',
        //     cancelBtnTitle: 'No',
        //     saveBtnTitle: 'Yes',
        //     hideSave: false,
        //     hideOnSave: false,
        //     hideFooter: true,
        //     data: {}
        // });
    }

    filterByDate(filterActionPopup: DxPopoverComponent) {
        filterActionPopup.instance.hide();
        this.appDialogService.open(this.dateFilter, {
            title: 'Filter BY DATE RANGE',
            width: '420px',
            saveBtnTitle: 'Apply'
        }).clickSave.subscribe(() => {
            this.isDisplayClear = true;
        });
    }

    filterByType(filterActionPopup: DxPopoverComponent) {
        filterActionPopup.instance.hide();
        this.appDialogService.open(this.typeFilter, {
            title: 'Filter BY Type',
            width: '400px',
            saveBtnTitle: 'Apply'
        }).clickSave.subscribe(() => {
            this.isDisplayClear = true;
        });
    }

    onDateSelect($event: Date | number | string, from: string) {
        if (from == this.FROM_DATE) {
            this.fromDateCtrl.setValue(new Date($event));
        } else {
            this.toDateCtrl.setValue(new Date($event));
        }
    }

    clearFilter() {
        this.isDisplayClear = false;
        this.fromDateCtrl.setValue(null);
        this.toDateCtrl.setValue(null);
    }

    toggleType($event: MatCheckboxChange) {
        const value = $event.source.value;
        if ($event.checked && this.selectedType.indexOf(value) < 0) {
            this.selectedType.push(value);
        } else {
            this.selectedType.splice(this.selectedType.indexOf(value), 1);
        }
    }

    toggleAllType($event: MatCheckboxChange) {
        if ($event.checked) {
            this.commTypes.forEach(type => {
                if (this.selectedType.indexOf(type.id) < 0) {
                    this.selectedType.push(type.id);
                }
            });
        } else {
            this.selectedType = [];
        }
    }

    loadCommunication() {
        this.blockUI.start();
        if (!this.patientId) {
            this.communicationService.getAll(this.appState.selectedUserLocationId, this.fromDateCtrl.value, this.toDateCtrl.value).subscribe((response) => {
                this.prepareResponse(response);
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        } else {
            this.communicationService.getAllForPatient(this.appState.selectedUserLocationId, this.patientId).subscribe((response) => {
                this.prepareResponse(response);
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
    }

    private prepareResponse(response: CommunicationList[]) {
        if (response) {
            response.map(obj => {
                obj.typeStr = obj.communicationType === CommunicationType.Email ? 'Email' : (obj.communicationType === CommunicationType.Note ? 'Note' : 'Sms');
                switch (obj.status) {
                    case CommunicationStatus.Draft:
                        obj['statusStr'] = 'Draft';
                        break;
                    case CommunicationStatus.InProgress:
                        obj['statusStr'] = 'In Progress';
                        break;
                    case CommunicationStatus.Failed:
                        obj['statusStr'] = 'Failed';
                        break;
                    case CommunicationStatus.Completed:
                        obj['statusStr'] = 'Sent';
                        break;
                }
                if (obj.communicationAttachment) {
                    obj.attachmentStr = obj.communicationAttachment.map(attachment => attachment.fileName).join(',');
                }
                obj.autoDeleteDateStr = AppoinmentDateUtils.formatDateTime(AppoinmentDateUtils.addAndGetNewValue(obj.createdDate, 90, 'd'), 'DD MMM, YYYY', false);
                obj.createdDateStr = AppoinmentDateUtils.formatDateTime(obj.createdDate, 'DD MMM, YYYY hh:mm A', false);
            });
            this.communications = response;
            this.gridData = response;
            this.state.filter = null;
            this.loadItems();
        }
    }

    get fromDateCtrlVal() {
        return this.fromDateCtrl.value || new Date();
    }

    get toDateCtrlVal() {
        return this.toDateCtrl.value || new Date();
    }

    previewFile(dataItem: CommunicationList) {
        if (dataItem.communicationAttachment && dataItem.communicationAttachment.length === 1) {
            AttachmentsPreviewComponent.open(this.appDialogService, {data: dataItem.communicationAttachment[0].attachedFile});
        } else if (dataItem.communicationAttachment && dataItem.communicationAttachment.length > 1) {
            AttachmentsPreviewComponent.open(this.appDialogService, {data: dataItem.communicationAttachment});
        }
    }
}
