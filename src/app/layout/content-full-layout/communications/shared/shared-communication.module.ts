import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommunicationGridComponent} from './communication-grid/communication-grid.component';
import {SharedContentFullLayoutModule} from '../../shared-content-full-layout/shared-content-full-layout.module';
import {AppCommonModule} from '../../common/app-common.module';
import {AppTimePipeModule} from '../../../../pipe/module/app-time-pipe.module';
import {DxPopoverModule} from 'devextreme-angular';
import {AppCalenderPopupModule} from '../../common/calender-popup/app-calender-popup.module';
import {AccessDirectiveModule} from '../../../../shared/directives/access-directive/access-directive.module';
import {AppDatePickerModule} from '../../common/app-date-picker/app-date-picker.module';


@NgModule({
    declarations: [
        CommunicationGridComponent
    ],
    exports: [
        CommunicationGridComponent
    ],
    imports: [
        CommonModule,
        SharedContentFullLayoutModule,
        AppCommonModule,
        AppTimePipeModule,
        DxPopoverModule,
        AppCalenderPopupModule,
        AccessDirectiveModule,
        AppDatePickerModule
    ]
})
export class SharedCommunicationModule {
}
