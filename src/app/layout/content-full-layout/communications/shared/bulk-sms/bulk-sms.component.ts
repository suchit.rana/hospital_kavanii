import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-bulk-sms',
  templateUrl: './bulk-sms.component.html',
  styleUrls: ['./bulk-sms.component.css']
})
export class BulkSmsComponent implements OnInit {

  ckeConfig: any;

  form: FormGroup = new FormGroup({
    category: new FormControl('', [Validators.required]),
    to: new FormControl('', [Validators.required]),
    template: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    tags: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
 });
  constructor() { }

  ngOnInit() {
  }

  cancel() {

  }

  save() {

  }

}
