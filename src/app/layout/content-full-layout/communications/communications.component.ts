import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-communications',
    templateUrl: './communications.component.html',
    styleUrls: ['./communications.component.scss']
})
export class CommunicationsComponent implements OnInit {

    activeLinkIndex = -1;
    navLinks: any[] = [];
    constructor(private router: Router) {
        this.navLinks = [
            {
                label: 'Bulk',
                link: 'bulk',
                index: 0
            }, {
                label: 'Campaign',
                link: 'campaign',
                index: 1
            }, {
                label: 'List',
                link: 'list',
                index: 2
            },
        ];
    }

    ngOnInit(): void {
        this.router.events.subscribe((res) => {
            this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
        });
    }

}
