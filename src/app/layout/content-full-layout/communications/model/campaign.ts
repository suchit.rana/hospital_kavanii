import {CommunicationType} from '../../patients/patient-commmunications/enum/communication-type';
import {CommunicationStatus} from './communication.interface';

export interface CampaignModel {
    // type: CommunicationType;
    // date: Date;
    // name: string;
    // sender: string;
    // listName: string;
    // totalContact: number;
    // autoDeleteAfter: string;
    // status: string;
    communicationType: CommunicationType;

    id: string,
    parentBusinessId: string,
    locationId: string,
    campaignName: string,
    subject: string,
    templateId: string,
    template: string,
    isSent: boolean,
    sentOn: Date,
    campaignAndList: CampaignList[];
    messageBody: string,
    status: CommunicationStatus,
    senderName: string;
    createdBy: string;
    createdDate: Date;
}

export interface CampaignList {
    listName?: string;
    id?: string;
    campaignId?: string;
    campaignListId: string;
}
