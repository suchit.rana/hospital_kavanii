import {CommunicationType} from '../../patients/patient-commmunications/enum/communication-type';

export interface Communication {
    id?: string,
    parentBusinessId: string,
    locationId: string,
    fromEmail: string,
    subject: string,
    templateId: string,
    template: string,
    source: EmailSourceType,
    status: CommunicationStatus,
    isSent: boolean,
    sentOn: Date,
    communicationType: CommunicationType,
    communicationRelatedTo: number,
    madeOfCommunication: number,
    noteStatus: number,
    patientId: string,
    categoriesId: string,
    messageBody: string,
    communicationEmail: EmailAddress[],
    communicationMobile: CommunicationMobile[],
    communicationAttachment: EmailAttachment[]
}

export interface EmailAttachment {
    id?: string,
    fileName: string,
    attachedFile: string
}

export interface EmailAddress {
    id?: string,
    email: string,
    emailSendType: EmailSendType
}

export interface CommunicationMobile {
    id?: string,
    mobile: string,
}

export enum EmailSourceType {
    Single = 0,
    Bulk = 1,
    Campaign = 2,
}

export enum CampaignType {
    Email = 1,
    SMS = 2,
}

export enum EmailSendType {
    To = 1,
    Cc = 2,
}

export enum CommunicationStatus {
    Draft = 1,
    InProgress = 2,
    Failed = 3,
    Completed = 4
}
