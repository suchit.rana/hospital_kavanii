import {CommunicationType} from '../../patients/patient-commmunications/enum/communication-type';
import {CampaignType} from './communication.interface';

export interface GridList {
    createdDateStr: string;
    campaignEmailList: any[];
    createdBy: string;
    createdDate: Date;
    id: string;
    listName: string;
    listType: CampaignType;
    type: string;
    totalContact: number;
    locationId: string;
}

export interface List {
    id: string,
    parentBusinessId: string,
    locationId: string,
    listName: string,
    listType: CampaignType,
    campaignEmailList: CampaignEmailList[];
}

export interface CampaignEmailList {
    id?: string,
    campaignListId?: string,
    name?: string,
    email?: string,
    mobileNo?: string,
    isSent?: boolean,
    sentOnUtc?: Date
}
