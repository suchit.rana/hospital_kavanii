import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CommunicationsRoutingModule} from './communications-routing.module';
import {CommunicationsComponent} from './communications.component';
import {FormsModule} from '@angular/forms';
import {
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDividerModule,
    MatMenuModule,
    MatTooltipModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatRadioModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatProgressBarModule,
    MatSliderModule,
    MatAutocompleteModule,
    MatNativeDateModule
} from '@angular/material';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DateInputsModule, CalendarModule, TimePickerModule} from '@progress/kendo-angular-dateinputs';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {EditorModule} from '@progress/kendo-angular-editor';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {LabelModule} from '@progress/kendo-angular-label';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {PDFExportModule} from '@progress/kendo-angular-pdf-export';
import {PopupModule} from '@progress/kendo-angular-popup';
import {ProgressBarModule} from '@progress/kendo-angular-progressbar';
import {TreeViewModule} from '@progress/kendo-angular-treeview';
import {UploadsModule} from '@progress/kendo-angular-upload';
import {CKEditorModule} from 'ckeditor4-angular';
import {DxDateBoxModule, DxCalendarModule, DxPopoverModule} from 'devextreme-angular';
import {BlockUIModule} from 'ng-block-ui';
import {NgImageFullscreenViewModule} from 'ng-image-fullscreen-view';
import {CharacterLimitModule} from 'src/app/directive/character-limit/character-limit.module';
import {OnlyNumberModule} from 'src/app/directive/only-number/only-number.module';
import {AppDateDiffPipeModule} from 'src/app/pipe/module/app-date-diff-pipe.module';
import {AppTimePipeModule} from 'src/app/pipe/module/app-time-pipe.module';
import {ManagingModule} from '../../module/managing.module';
import {
    ApptDropDownAddNewModule
} from '../appointment/schedular-main/schedular/shared/appt-drop-down-add-new/appt-drop-down-add-new.module';
import {AppCommonModule} from '../common/app-common.module';
import {AppDatePickerModule} from '../common/app-date-picker/app-date-picker.module';
import {AppCalenderPopupModule} from '../common/calender-popup/app-calender-popup.module';
import {AppTimePickerModule} from '../common/time-picker/app-time-picker.module';
import {AddGeneralModule} from '../contacts/add-general/add-general.module';
import {AppointmentPopupModule} from '../patients/patient-appointments/appointment-popup/appointment-popup.module';
import {AppKendoMultiSelectModule} from '../shared-content-full-layout/app-kendo-multi-select/app-kendo-multi-select.module';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout/shared-content-full-layout.module';
import {BulkEmailComponent} from './shared/bulk-email/bulk-email.component';
import {LetterTagListModule} from 'src/app/shared/letter-tag-list/letter-tag-list.module';
import {BulkSmsComponent} from './shared/bulk-sms/bulk-sms.component';
import {AddNoteComponent} from './component/list/add-note/add-note.component';
import {CreateCampaignListComponent} from './component/campaign/create-campaign/create-campaign-list.component';
import {ListDetailsComponent} from './component/list/list-details/list-details.component';
import {CampaignSendEmailComponent} from './component/campaign/create-campaign/campaign-send-email/campaign-send-email.component';
import {CampaignSendSMSComponent} from './component/campaign/create-campaign/campaign-send-sms/campaign-send-sms.component';
import {CommunicationPreviewComponent} from './shared/communication-preview/communication-preview.component';
import {AttachmentsPreviewComponent} from './shared/attachments-preview/attachments-preview.component';
import {DropdownTreeViewModule} from '../../../shared/dropdown-treeview/dropdown-tree-view.module';
import {BulkComponent} from './component/bulk/bulk.component';
import {CampaignComponent} from './component/campaign/campaign.component';
import {ListComponent} from './component/list/list.component';
import {SharedCommunicationModule} from './shared/shared-communication.module';
import {BasicSearchComponent} from './component/list/list-details/filter/basic-search/basic-search.component';
import {ListBasedSearchComponent} from './component/list/list-details/filter/list-based-search/list-based-search.component';
import {AdvancedSearchComponent} from './component/list/list-details/filter/advanced-search/advanced-search.component';
import {AppDateRangePickerModule} from '../common/app-date-range-picker/app-date-range-picker.module';
import {EditListContactComponent} from './component/list/edit-list-contact/edit-list-contact.component';


@NgModule({
    declarations: [CommunicationsComponent, BulkEmailComponent, BulkSmsComponent, AddNoteComponent, CreateCampaignListComponent, ListDetailsComponent, CampaignSendEmailComponent, CampaignSendSMSComponent, CommunicationPreviewComponent, AttachmentsPreviewComponent, BulkComponent, CampaignComponent, ListComponent, BasicSearchComponent, ListBasedSearchComponent, AdvancedSearchComponent, EditListContactComponent],
    imports: [
        CommonModule,
        AppCommonModule,
        ManagingModule,
        SharedContentFullLayoutModule,
        AddGeneralModule,
        // SharedModule,
        LetterTagListModule,
        MatListModule,
        MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatCheckboxModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDividerModule,
        MatMenuModule,
        MatListModule,
        MatTooltipModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        MatTabsModule,
        MatRadioModule,
        MatCardModule,
        // ColorPickerModule,
        FormsModule,
        MatInputModule,
        MatTableModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        DateInputsModule,
        CalendarModule,
        // IntlModule,
        TimePickerModule,
        InputsModule,
        MatButtonToggleModule,
        MatIconModule,
        BlockUIModule.forRoot(),
        ButtonsModule,
        MatDialogModule,
        // OrderModule,
        LayoutModule,
        InputsModule,
        DialogModule,
        MatProgressBarModule,
        NgImageFullscreenViewModule,
        DropDownsModule,
        ProgressBarModule,
        // TooltipModule,
        // EditorModule,
        // MaterialTimePickerModule,
        MatSliderModule,
        UploadsModule,
        LabelModule,
        CKEditorModule,
        DxDateBoxModule,
        DxCalendarModule,
        // DevExtremeModule,
        MatAutocompleteModule,
        AppCalenderPopupModule,
        AppTimePipeModule,
        AppTimePickerModule,
        AppDatePickerModule,
        CharacterLimitModule,
        TreeViewModule,
        PopupModule,
        EditorModule,
        PDFExportModule,
        DxPopoverModule,
        AppKendoMultiSelectModule,
        AppTimePipeModule,
        AppDateDiffPipeModule,
        OnlyNumberModule,
        ApptDropDownAddNewModule,
        AppointmentPopupModule,
        MatDatepickerModule,
        MatNativeDateModule,
        CommunicationsRoutingModule,
        DropdownTreeViewModule,
        SharedCommunicationModule,
        AppDateRangePickerModule,
        CharacterLimitModule
    ],
    entryComponents: [
        BulkEmailComponent,
        BulkSmsComponent,
        AddNoteComponent,
        CreateCampaignListComponent,
        ListDetailsComponent,
        CampaignSendEmailComponent,
        CampaignSendSMSComponent,
        ListDetailsComponent,
        CommunicationPreviewComponent,
        AttachmentsPreviewComponent,
        EditListContactComponent
    ]
})
export class CommunicationsModule {
}
