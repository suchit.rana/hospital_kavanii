import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppDialogService, IPopupAction} from '../../../../../../shared/services/app-dialog.service';
import {TextValue} from '../../../../../../shared/interface';
import {FileSelectService} from '../../../shared/api/file-select.service';
import {CommunicationService} from '../../../api/communication/communication.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {Communication} from '../../../model/communication.interface';
import {AppState} from '../../../../../../app.state';
import {CommunicationType} from '../../../../patients/patient-commmunications/enum/communication-type';
import {Success} from '../../../../../../app.component';
import {CommunicationList} from '../../../../patients/patient-commmunications/interface/communication-list';

@Component({
    selector: 'app-add-note',
    templateUrl: './add-note.component.html',
    styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent implements OnInit {

    constructor(
        public appState: AppState,
        public appDialogService: AppDialogService,
        public alertService: AppAlertService,
        public fileSelectService: FileSelectService,
        public communicationService: CommunicationService,
    ) {
        fileSelectService.dialogService = appDialogService;
    }

    private static ref: IPopupAction;

    @Input()
    patientId: string;
    @Input()
    id: string;

    data:Communication;
    maxChars: number = 500;
    characterlength: number = 0;
    apperance = 'outline';

    form: FormGroup = new FormGroup({
        communicationRelatedTo: new FormControl('', [Validators.required]),
        madeOfCommunication: new FormControl('', [Validators.required]),
        noteStatus: new FormControl('', [Validators.required]),
        messageBody: new FormControl('', [Validators.required])
    });
    communicationRelated: TextValue[] = [
        {text: 'Appointment', value: 1},
        {text: 'Billing', value: 2},
        {text: 'Treatment', value: 3},
        {text: 'Others', value: 4}
    ];
    madeOfCommunication: TextValue[] = [
        {text: 'In Person', value: 1},
        {text: 'Phone', value: 2},
        {text: 'Email', value: 4},
        {text: 'SMS', value: 5},
        {text: 'Others', value: 6},
    ];
    noteStatus: TextValue[] = [
        {text: 'Sent', value: 1},
        {text: 'Received', value: 2},
        {text: 'Others', value: 3}
    ];

    ngOnInit() {
        this.fileSelectService.clear();
        if (this.id) {
            this.communicationService.getById(this.id).subscribe(response => {
                this.data = response;
                this.form.patchValue(this.data);
                this.data.communicationAttachment && this.data.communicationAttachment.forEach(file => {
                    const content = file.attachedFile.slice(file.attachedFile.indexOf(',') + 1);
                    const paddingBit = content.substr(-2) === "==" ?  2 : content.substr(-2)[1] === "=" ? 1 : 0;
                    const fileSizeInByte = (content.length * (3/4)) - paddingBit;
                    this.fileSelectService.selectedFiles.push({name: file.fileName, content: file.attachedFile, size: this.fileSelectService.formatBytes(fileSizeInByte)});
                });
            });
        }

        AddNoteComponent.ref.clickSave.subscribe(() => {
           if (this.form.invalid) {
               this.form.markAllAsTouched();
               return;
           }
           this.save();
        });
    }

    onTextChange(event) {
        let str = event.replace(/(\r\n|\n|\r)/gm, '  ');
        if (str.length > this.maxChars) {
            event = event.slice(0, this.maxChars);
            this.characterlength = event.length;
        } else {
            this.characterlength = str.length;
        }
    }

    cancel() {

    }

    save() {
        const data: Communication = this.form.value;

        data.communicationType = CommunicationType.Note;
        data.patientId = this.patientId;
        data.communicationAttachment = [];
        if (this.fileSelectService.selectedFiles.length > 0) {
            this.fileSelectService.selectedFiles.forEach(value => {
                data.communicationAttachment.push({fileName: value.name, attachedFile: value.content});
            });
        }
        if (!this.id) {
            data.locationId = this.appState.selectedUserLocationId;

            this.communicationService.create(data).subscribe((response) => {
                if (response) {
                    this.alertService.displaySuccessMessage('Note Added successfully.');
                    AddNoteComponent.ref.closeDialog(Success);
                }
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        } else {
            data.id = this.data.id;
            this.communicationService.update(data).subscribe((response) => {
                if (response) {
                    this.alertService.displaySuccessMessage('Note Updated successfully.');
                    AddNoteComponent.ref.closeDialog(Success);
                }
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(AddNoteComponent, {
                title: 'ADD Note',
                width: '549px',
                saveBtnTitle: data['id'] ? 'Update Note' : 'Add Note',
                hideOnSave: false,
                data: data
            }
        );
        return this.ref;
    }

}
