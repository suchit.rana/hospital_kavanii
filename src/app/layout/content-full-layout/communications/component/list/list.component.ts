import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {CommunicationType} from '../../../patients/patient-commmunications/enum/communication-type';
import {DxPopoverComponent} from 'devextreme-angular';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {AppState} from '../../../../../app.state';
import {BaseGridComponent} from '../../../shared-content-full-layout/base-grid/base-grid.component';
import {GridList} from '../../model/gridList';
import {ListService} from '../../api/list/list.service';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';
import {map} from 'rxjs/operators';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {create} from 'domain';
import {CampaignType} from '../../model/communication.interface';
import {
    DeleteConfirmationDialogComponent
} from '../../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {BlockUI} from 'ng-block-ui';
import {EditListContactComponent} from './edit-list-contact/edit-list-contact.component';
import {SAVE} from '../../../../../shared/popup-component/popup-component.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ListComponent extends BaseGridComponent implements OnInit {

    readonly EDIT = 'EDIT';
    readonly PREVIEW = 'PREVIEW';
    readonly PRINT = 'PRINT';
    readonly DELETE = 'DELETE';
    readonly DUPLICATE = 'DUPLICATE';
    readonly Email = CommunicationType.Email;
    readonly Sms = CommunicationType.Sms;
    readonly UnSend = 'UnSend';

    @BlockUI() blockUI;

    actionItem: GridList; // Item that was selected through action(GEAR ICON)
    @ViewChild('actionPopup', {static: true}) actionPopup: DxPopoverComponent;
    @ViewChild('duplicateDialog', {static: false}) duplicateDialog;

    @Input() patientId: string;

    lists: GridList[];
    id: string;
    duplicate: boolean;
    selectedRowIds: string[] = [];

    constructor(
        private appDialogService: AppDialogService,
        private listService: ListService,
        protected appState: AppState,
        protected alertService: AppAlertService,
        protected router: Router,
    ) {
        super(null, appState);
    }

    ngOnInit() {
        this.getList();
    }


    openActionPopup(dataItem, event) {
        this.actionItem = dataItem;

        this.actionPopup.target = event.target;
        this.actionPopup.instance.show();
    }

    doAction(action: string) {
        switch (action) {
            case this.EDIT:
                this.router.navigate(['/communications/list/edit/', this.actionItem.id]);
                break;
            case this.DUPLICATE:
                this.appDialogService.open(this.duplicateDialog, {
                    title: 'DO YOU WISH TO DUPLICATE THIS LIST?',
                    width: '800px',
                    height: '300px',
                    saveBtnTitle: 'Yes',
                    cancelBtnTitle: 'No'
                }).clickSave.subscribe(() => {
                    this.router.navigate(['/communications/list/copy/', this.actionItem.id]);
                });
                break;
            case this.DELETE:
                const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
                    hideOnSave: false,
                    title: 'Do you wish to delete this List ?'
                });
                ref.clickSave.subscribe(() => {
                    this.deleteList(this.actionItem.id, ref);
                });
                break;
        }
    }

    deleteSelected() {
        const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
            hideOnSave: false,
            title: 'Do you wish to delete this List ?'
        });
        ref.clickSave.subscribe(() => {
            this.selectedRowIds.forEach(id => {
                setTimeout(() => {
                    this.deleteList(id, ref, true);
                }, 200);
            });
        });
    }

    editCampaignList(item: GridList) {
        EditListContactComponent.open(this.appDialogService, {id: item.id})
            .close.subscribe((value) => {
                if (value === SAVE) {
                    this.getList();
                }
        });
    }

    private deleteList(id: string, ref?: IPopupAction, multiple = false): void {
        this.listService.delete(id).subscribe(() => {
            this.blockUI.stop();
            this.getList();
            if (multiple && this.selectedRowIds[this.selectedRowIds.length - 1] == id) {
                this.alertService.displaySuccessMessage('List Deleted successfully.');
                ref && ref.closeDialog();
                return;
            }
            this.alertService.displaySuccessMessage('List Deleted successfully.');
        }, () => {
            this.alertService.displayErrorMessage('Failed to delete List, please try again later');
            this.blockUI.stop();
        });
    }

    private getList() {
        this.listService.getAll(this.appState.selectedUserLocationId).pipe(
            map(data => {
                data.map(value => {
                    value.type = value.listType === CampaignType.Email ? 'Email' : 'Sms';
                    value.totalContact = value.campaignEmailList && value.campaignEmailList.length;
                    value.createdDateStr = AppoinmentDateUtils.formatDateTime(value.createdDate, 'DD MMM, YYYY hh:mm A', false);
                    return value;
                });
                return data;
            })
        ).subscribe(response => {
            this.lists = response;
            this.gridData = this.lists;
            this.state.filter = null;
            this.loadItems();
        }, error => {
            this.alertService.displayErrorMessage(error);
        });
    }
}
