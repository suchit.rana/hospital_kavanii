import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../../shared/services/app-dialog.service';
import {BaseGridComponent} from '../../../../shared-content-full-layout/base-grid/base-grid.component';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {ListService} from '../../../api/list/list.service';
import {GridList} from '../../../model/gridList';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {BlockUI} from 'ng-block-ui';
import {SAVE} from '../../../../../../shared/popup-component/popup-component.component';
import {SelectionEvent} from '@progress/kendo-angular-grid';

@Component({
    selector: 'app-edit-list-contact',
    templateUrl: './edit-list-contact.component.html',
    styleUrls: ['./edit-list-contact.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class EditListContactComponent extends BaseGridComponent implements OnInit {

    private static ref: IPopupAction;

    @BlockUI() blockUI;

    @Input()
    id: string;

    selectedRow = [];
    selectedRowEmail = [];
    private list: GridList;

    constructor(
        private alertService: AppAlertService,
        private listService: ListService,
    ) {
        super();
    }

    ngOnInit() {
        this.getListById();
        EditListContactComponent.ref.clickSave.subscribe(() => {
            if (this.selectedRow.length === 0) {
                this.alertService.displayErrorMessage("Select at least one contact.");
                return;
            }
            this.save();
        });
    }

    onSelectRow($event: SelectionEvent) {
        const campaignEmailList = this.list.campaignEmailList;
        $event.deselectedRows.forEach(row => {
            const index = this.selectedRow.findIndex(item => item.email === row.dataItem.email || item.mobileNo === row.dataItem.mobileNo);
            this.selectedRow.splice(index, 1);
        });
        $event.selectedRows.forEach(row => {
            let obj;
            const hasItem = campaignEmailList.find(item => item.email === row.dataItem.email || item.mobileNo === row.dataItem.mobileNo);
            if (hasItem) {
                obj = hasItem;
            } else {
                obj = {
                    name: row.dataItem.name,
                    email: row.dataItem.email,
                    mobileNo: row.dataItem.mobileNo,
                };
            }
            this.selectedRow.push(obj);
        });
    }

    save() {
        this.list.campaignEmailList = this.selectedRow;
        this.blockUI.start();
        this.listService.update(this.list).subscribe(() => {
            this.alertService.displaySuccessMessage('List updated successfully.');
            this.blockUI.stop();
            EditListContactComponent.ref.closeDialog(SAVE);
        }, error => {
            this.blockUI.stop();
            this.alertService.displayErrorMessage(error);
        });
    }

    private getListById() {
        if (this.id) {
            this.listService.getById(this.id).subscribe(response => {
                this.list = response;
                this.selectedRow = response.campaignEmailList;
                this.selectedRowEmail = this.selectedRow.map(row => row.email);
                this.gridData = AppUtils.refrenceClone(this.selectedRow);
                this.state.filter = null;
                this.loadItems();
            }, (error) => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(EditListContactComponent, {
                title: `Edit List Contact`,
                width: '778px',
                saveBtnTitle: 'Update',
                hideOnSave: false,
                data: data
            }
        );
        return this.ref;
    }

}
