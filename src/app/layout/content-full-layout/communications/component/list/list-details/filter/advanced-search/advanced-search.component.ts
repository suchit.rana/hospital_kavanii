import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {InputRule} from '@progress/kendo-angular-editor';
import {FormGroup} from '@angular/forms';
import {AppDateRangePickerComponent} from '../../../../../../common/app-date-range-picker/app-date-range-picker.component';

@Component({
    selector: 'app-advanced-search',
    templateUrl: './advanced-search.component.html',
    styleUrls: ['./advanced-search.component.css']
})
export class AdvancedSearchComponent implements OnInit {

    @Input()
    formGroup: FormGroup;

    apperance = 'outline';

    @ViewChild('referralDR', {static: false}) referralDR: AppDateRangePickerComponent;
    @ViewChild('providerDR', {static: false}) providerDR: AppDateRangePickerComponent;
    @ViewChild('offerDR', {static: false}) offerDR: AppDateRangePickerComponent;

    constructor() {
    }

    ngOnInit(): void {
    }

    dateSelectionChange($event) {
        this.formGroup.get('dateRangeDate').setValue(new Date($event));
    }

    get dateRangeDate() {
        return this.formGroup && this.formGroup.get('firstVisitDate') && this.formGroup.get('firstVisitDate').value ? this.formGroup.get('firstVisitDate').value : new Date();
    }

}
