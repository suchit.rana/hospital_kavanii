import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ApplicationDataService} from '../../../../../../../../services/app.applicationdata.service';
import {ApplicationDataEnum} from '../../../../../../../../enum/application-data-enum';
import {AppState} from '../../../../../../../../app.state';
import {map} from 'rxjs/operators';
import {ApplicationDataModel, ConcessionModel, MarketingSourceModel} from '../../../../../../../../models/app.settings.model';
import {SettingsService} from '../../../../../../../../services/app.settings.service';
import {PractitionerSpecialityModel} from '../../../../../../../../models/app.staff.model';
import {AppUtils} from '../../../../../../../../shared/AppUtils';
import {StaffService} from '../../../../../../../../services/app.staff.service';

@Component({
    selector: 'app-list-based-search',
    templateUrl: './list-based-search.component.html',
    styleUrls: ['./list-based-search.component.css']
})
export class ListBasedSearchComponent implements OnInit {

    @Input()
    formGroup: FormGroup;

    apperance = 'outline';

    constructor(
        private appState: AppState,
        private settingsService: SettingsService,
        private applicationDataService: ApplicationDataService,
        private staffService: StaffService,
    ) {
    }

    patientPosition = ApplicationDataEnum.PatientPosition;
    patientOccupation = ApplicationDataEnum.PatientOccupation;
    patientClassification = ApplicationDataEnum.PatientClassification;

    genders: any[] = [
        {text: 'All'},
        {text: 'Male'},
        {text: 'Female'},
        {text: 'Other'}
    ];

    status: any[] = [
        {text: 'All'},
        {text: 'Active'},
        {text: 'Inactive'}
    ];

    positions: ApplicationDataModel[] = [];
    occupations: ApplicationDataModel[] = [];
    classifications: ApplicationDataModel[] = [];
    marketingReferralSource: MarketingSourceModel[] = [];
    concessions: ConcessionModel[] = [];
    practitioners: PractitionerSpecialityModel[] = [];

    ngOnInit(): void {
        this.getPosition();
        this.getClassification();
        this.getOccupation();
        this.getMarketingReferralSource();
        this.getConcessions();
        this.getPractitioner();
    }

    dateSelectionChange(value: Date, ctrlName: string) {
        this.formGroup.get(ctrlName).setValue(new Date(value));
    }

    private getPosition() {
        this.applicationDataService
            .GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.PatientPosition, this.appState.selectedUserLocationId)
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.status) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe((data) => {
            this.positions = data;
        });
    }

    private getOccupation() {
        this.applicationDataService
            .GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.PatientOccupation, this.appState.selectedUserLocationId)
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.status) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe((data) => {
            this.occupations = data;
        });
    }

    private getClassification() {
        this.applicationDataService
            .GetApplicationDataByCategoryIdAndLocationId(ApplicationDataEnum.PatientClassification, this.appState.selectedUserLocationId)
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.status) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe((data) => {
            this.classifications = data;
        });
    }

    private getMarketingReferralSource() {
        this.settingsService.getAllMarketingSources()
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.isStatus) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe(data => {
            this.marketingReferralSource = data;
        });
    }

    private getConcessions() {
        this.settingsService.getAllConcessions()
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.isStatus) {
                            return value;
                        }
                    });
                    return data;
                })
            ).subscribe(data => {
            this.concessions = data;
        });
    }

    private getPractitioner() {
        this.staffService.getAllPractitionerSpecialityByLocationIdForCalendar(this.appState.selectedUserLocationId)
            .pipe(
                map(data => {
                    data.map(value => {
                        if (value.status) {
                            value.displayName = value.firstName + ' ' + value.lastName;
                            return value;
                        }
                    });
                    return data;
                })
            )
            .subscribe(data => {
            this.practitioners = data;
        });
    }

    get firstVisitDate() {
        return this.formGroup && this.formGroup.get('firstVisitDate') && this.formGroup.get('firstVisitDate').value ? this.formGroup.get('firstVisitDate').value : new Date();
    }

    get dob() {
        return this.formGroup && this.formGroup.get('dob') && this.formGroup.get('dob').value ? this.formGroup.get('dob').value : new Date();
    }

}
