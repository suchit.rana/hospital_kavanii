import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {BaseGridComponent} from '../../../../shared-content-full-layout/base-grid/base-grid.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ListService} from '../../../api/list/list.service';
import {AppState} from '../../../../../../app.state';
import {GridComponent, SelectionEvent} from '@progress/kendo-angular-grid';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {dirtyCheck} from '../../../../../../shared/utils/dirty-check';
import {CampaignType} from '../../../model/communication.interface';
import {AppUtils} from '../../../../../../shared/AppUtils';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {For, FromPage} from '../../../../../../shared/interface';

@Component({
    selector: 'app-list-details',
    templateUrl: './list-details.component.html',
    styleUrls: ['./list-details.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ListDetailsComponent extends BaseGridComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    duplicate: boolean;
    id: string;

    @ViewChild('grid', {static: false}) grid: GridComponent;

    searchType = '1';
    selectedRow = [];
    selectedRowEmail = [];
    emailSms = 'Email';
    isFilterApplied: boolean = false;
    titlePrefix: string = 'CREATE';
    basicSearchFG: FormGroup = new FormGroup({
        locationId: new FormControl(this.appState.selectedUserLocationId),
        firstName: new FormControl(''),
        lastName: new FormControl(''),
        city: new FormControl(''),
        state: new FormControl(''),
        postCode: new FormControl(''),
        dob: new FormControl('')
    });

    listSearchFG: FormGroup = new FormGroup({
        locationId: new FormControl(this.appState.selectedUserLocationId),
        firstVisitStartDate: new FormControl(''),
        firstVisitEndDate: new FormControl(''),
        dobStartDate: new FormControl(''),
        dobEndDate: new FormControl(''),
        gender: new FormControl('All'),
        status: new FormControl('All'),
        position: new FormControl(''),
        patientClassification: new FormControl(''),
        occupation: new FormControl(''),
        marketingReferralSource: new FormControl(''),
        concession: new FormControl(''),
        primaryProvider: new FormControl(''),
    });

    advancedSearchFG: FormGroup = new FormGroup({
        locationId: new FormControl(this.appState.selectedUserLocationId),
        referralPractitionerId: new FormControl(''),
        practitionerId: new FormControl(''),
        offeringType: new FormControl(''),
        serviceId: new FormControl(''),
        createdDateStartDate: new FormControl(''),
        createdDateEndDate: new FormControl(''),
    });

    listForm: FormGroup;
    isDirtyBasicSearch$: Observable<boolean>;
    isDirtyListSearch$: Observable<boolean>;
    isDirtyAdvancedSearch$: Observable<boolean>;

    basicSearch = new BehaviorSubject<any>(this.basicSearchFG.value);
    listSearch = new BehaviorSubject<any>(this.listSearchFG.value);
    advancedSearch = new BehaviorSubject<any>(this.advancedSearchFG.value);

    basicSearch$ = this.basicSearch.asObservable();
    listSearch$ = this.listSearch.asObservable();
    advancedSearch$ = this.advancedSearch.asObservable();

    constructor(
        public appState: AppState,
        public alertService: AppAlertService,
        private listService: ListService,
        protected router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        super(router);
        const params = this.activatedRoute.snapshot.params;
        this.id = params['id'];
        this.duplicate = router.url.includes('/copy/');
    }


    ngOnInit() {
        this.isDirtyBasicSearch$ = dirtyCheck(this.basicSearchFG, this.basicSearch$);
        this.isDirtyListSearch$ = dirtyCheck(this.listSearchFG, this.listSearch$);
        this.isDirtyAdvancedSearch$ = dirtyCheck(this.advancedSearchFG, this.advancedSearch$);

        this.listForm = new FormGroup({
            id: new FormControl(''),
            locationId: new FormControl(this.appState.selectedUserLocationId),
            listName: new FormControl('', Validators.required),
            listType: new FormControl(''),
            campaignEmailList: new FormControl([])
        });

        this.getListById();
    }

    onSelectRow($event: SelectionEvent) {
        const campaignEmailList = this.listForm.get('campaignEmailList').value;
        $event.deselectedRows.forEach(row => {
            const index = this.selectedRow.findIndex(item => item.email === row.dataItem.email || item.mobileNo === row.dataItem.mobileNo);
            this.selectedRow.splice(index, 1);
        });
        $event.selectedRows.forEach(row => {
            let obj;
            if (this.hasId) {
                const hasItem = campaignEmailList.find(item => item.email === row.dataItem.email || item.mobileNo === row.dataItem.mobileNo);
                if (hasItem) {
                    obj = hasItem;
                } else {
                    obj = {
                        name: row.dataItem.name,
                        email: row.dataItem.email,
                        mobileNo: row.dataItem.mobileNo,
                    };
                }
            } else {
                obj = {
                    name: row.dataItem.name,
                    email: row.dataItem.email,
                    mobileNo: row.dataItem.mobileNo,
                };
            }
            this.selectedRow.push(obj);
        });
    }

    onFilterTypeChange() {
        if (this.searchType == '1') {
            this.listSearchFG.reset();
            this.advancedSearchFG.reset();
        } else if (this.searchType == '2') {
            this.basicSearchFG.reset();
            this.advancedSearchFG.reset();
        } else if (this.searchType == '3') {
            this.basicSearchFG.reset();
            this.listSearchFG.reset();
        }

        this.basicSearch.next(this.basicSearchFG.value);
        this.listSearch.next(this.listSearchFG.value);
        this.advancedSearch.next(this.advancedSearchFG.value);
    }

    filterData() {
        this.isFilterApplied = true;
        if (this.searchType == '1') {
            this.basicSearch.next(this.basicSearchFG.value);
            this.listService.basicSearchCampaignEmailLists(this.basicSearchFG.value).subscribe(response => {
                this.gridData = response;
                this.state.filter = null;
                this.loadItems();
            });
        } else if (this.searchType == '2') {
            this.listSearch.next(this.listSearchFG.value);
            this.listService.listBasedSearchCampaignEmailLists(this.listSearchFG.value).subscribe(response => {
                this.gridData = response;
                this.state.filter = null;
                this.loadItems();
            });
        } else if (this.searchType == '3') {
            this.advancedSearch.next(this.advancedSearchFG.value);
            this.listService.advancedSearchCampaignEmailLists(this.advancedSearchFG.value).subscribe(response => {
                this.gridData = response;
                this.state.filter = null;
                this.loadItems();
            });
        }
    }

    clearFilter(isFilterApplied: boolean = false) {
        this.isFilterApplied = isFilterApplied;
        this.basicSearchFG.reset();
        this.listSearchFG.reset();
        this.advancedSearchFG.reset();

        this.basicSearch.next(this.basicSearchFG.value);
        this.listSearch.next(this.listSearchFG.value);
        this.advancedSearch.next(this.advancedSearchFG.value);
    }

    save() {
        const data = this.listForm.value;

        if (!this.hasId || this.duplicate) {
            delete data.id;
        }

        data.campaignEmailList = this.selectedRow;
        data.listType = this.emailSms === 'Email' ? CampaignType.Email : CampaignType.SMS;
        if (!this.hasId) {
            this.listService.create(data).subscribe(response => {
                this.alertService.displaySuccessMessage('List created successfully.');
                if (this.routerState && this.routerState.fromPage === FromPage.CampaignSendEmailSms) {
                    const data = this.routerState.data.formData;
                    data.campaignAndList = data.campaignAndList || [];
                    data.campaignAndList.push({listName: this.listForm.value.listName, campaignListId: response});
                    let fromState: NavigationExtras = {
                        state: {
                            fromState: true,
                            fromPage: FromPage.CommunicationListCreate,
                            for: For.CreateList,
                            data: {formData: data}
                        }
                    };
                    this.router.navigate(['/communications/campaign'], fromState);
                    return;
                }
                this.router.navigate(['/communications/list']);
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        } else {
            this.listService.update(data).subscribe(response => {
                this.alertService.displaySuccessMessage('List updated successfully.');
                this.router.navigate(['/communications/list'])
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    private getListById() {
        if (this.id) {
            this.blockUI.start();
            this.listService.getById(this.id).subscribe(response => {
                this.listForm.patchValue(response);
                if (this.duplicate) {
                    this.titlePrefix = 'COPY';
                    this.listForm.get('id').setValue('');
                } else {
                    this.titlePrefix = 'Update';
                }
                this.selectedRow = response.campaignEmailList;
                this.selectedRowEmail = this.selectedRow.map(row => row.email);
                this.gridData = AppUtils.refrenceClone(this.selectedRow);
                this.state.filter = null;
                this.loadItems();
                this.blockUI.stop();
            }, (error) => {
                this.alertService.displayErrorMessage(error);
                this.blockUI.stop();
            });
        }
    }

    get disabledApply(): Observable<boolean> {
        if (this.searchType == '1') {
            return this.isDirtyBasicSearch$;
        } else if (this.searchType == '2') {
            return this.isDirtyListSearch$;
        } else {
            return this.isDirtyAdvancedSearch$;
        }
    }

    get hasId() {
        return !!this.listForm.get('id').value;
    }
}
