import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AppoinmentDateUtils} from '../../../../../../appointment/AppoinmentDateUtils';

@Component({
  selector: 'app-basic-search',
  templateUrl: './basic-search.component.html',
  styleUrls: ['./basic-search.component.css']
})
export class BasicSearchComponent implements OnInit {

  @Input()
  formGroup: FormGroup;

  apperance = 'outline';

  constructor() { }

  ngOnInit(): void {
  }

  dateSelectionChange(value: Date) {
    this.formGroup.get('dob').setValue(new Date(value));
  }

  get selectedDataDate() {
    return this.formGroup && this.formGroup.get('dob') && this.formGroup.get('dob').value;
  }
}
