import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {AppDialogService, IPopupAction} from '../../../../../../../shared/services/app-dialog.service';
import {CKEditor4} from 'ckeditor4-angular/ckeditor';
import {DummyGUID, Success} from '../../../../../../../app.component';
import {AppState} from '../../../../../../../app.state';
import {EmailSmsService} from '../../../../shared/api/email-sms.service';
import {ContactService} from '../../../../../../../services/app.contact.service';
import {SettingsService} from '../../../../../../../services/app.settings.service';
import {FileSelectService} from '../../../../shared/api/file-select.service';
import {CommunicationService} from '../../../../api/communication/communication.service';
import {CampaignService} from '../../../../api/campaign/campaign.service';
import {ListService} from '../../../../api/list/list.service';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {ContactTypeEnum} from '../../../../../../../enum/application-data-enum';
import {CommunicationType} from '../../../../../patients/patient-commmunications/enum/communication-type';
import {Communication, CommunicationStatus, EmailSendType, EmailSourceType} from '../../../../model/communication.interface';
import {CampaignModel} from '../../../../model/campaign';
import {CampaignEmailSms, CommunicationBulkEmailSms, PatientCommunication} from '../campaign-send-email/campaign-send-email.component';
import {NavigationExtras, Router} from '@angular/router';
import {For, FromPage} from '../../../../../../../shared/interface';

@Component({
    selector: 'app-campaign-send-sms',
    templateUrl: './campaign-send-sms.component.html',
    styleUrls: ['./campaign-send-sms.component.css']
})
export class CampaignSendSMSComponent implements OnInit {
    private static ref: IPopupAction;

    maxChars = 612;
    characterlength = 0;
    smscount = 1;
    letterCount = 0;
    numberCount = 0;
    spaceCount = 0;
    lineBreakCount = 0;
    errormessage = null;
    editor: any;
    editorContent: string;

    readonly PatientEmail = 0;
    readonly PatientEmergencyContact = 1;
    readonly ReferralContact = 2;
    readonly GeneralContact = 3;
    readonly TPPContact = 4;

    fromPatient: boolean = true;
    campaign: boolean = false;

    @Input()
    data: any;
    @Input()
    patientId: string;
    @Input()
    openFor: number = PatientCommunication;
    @Output()
    beforeSave: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    afterSave: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output()
    createList: EventEmitter<boolean> = new EventEmitter<boolean>();

    ckeConfig: any;

    toData = [];

    categories = [
        {text: 'Patient Contact', value: 2},
        {text: 'Patient Emergency Contact', value: 3},
        {text: 'Referral Contact', value: 4},
        {text: 'General Contact', value: 5},
        {text: 'TPP Contact', value: 6},
    ];

    templates = [];

    patientNumbers = [];
    patientEmergencyContactNumbers = [];
    referralContactNumbers = [];
    generalContactNumbers = [];
    TPPContactNumbers = [];

    isDisplayCCField = false;

    ccFormControl = new FormControl([], Validators.required);
    formGroup: FormGroup = new FormGroup({
        to: new FormControl([], [Validators.required]),
        templateId: new FormControl(DummyGUID, [Validators.required]),
        subject: new FormControl('', [Validators.required]),
        messageBody: new FormControl('', [Validators.required]),
    });

    constructor(
        private appState: AppState,
        private emailSmsService: EmailSmsService,
        private contactService: ContactService,
        private settingsService: SettingsService,
        public fileSelectService: FileSelectService,
        public communicationService: CommunicationService,
        public campaignService: CampaignService,
        public listService: ListService,
        public alertService: AppAlertService,
        public appDialogService: AppDialogService,
        public router: Router,
    ) {
        this.fileSelectService.dialogService = this.appDialogService;
    }

    ngOnInit() {
        this.fileSelectService.clear();
        this.fromPatient = this.openFor === PatientCommunication;
        this.campaign = this.openFor === CampaignEmailSms;

        this.handleButtonClick();

        this.getEmailTemplate();
        if (this.openFor === CampaignEmailSms) {
            this.getLists();
        } else {
            this.getNumbers();
        }

        this.ckeConfig = {
            extraAllowedContent: 'img[width,height,align]',
            // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets-all
            extraPlugins: 'easyimage',
            removePlugins: 'elementspath',

            toolbar: [
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                },
                {name: 'styles', items: ['Font', 'FontSize']},
                {name: 'colors', items: ['TextColor', 'BGColor']},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste']},
                {name: 'basicstyles', items: ['Subscript', 'Superscript']},
                {name: 'insert', items: ['PageBreak', 'HorizontalRule']},
                {name: 'links', items: ['Link', 'Unlink']},
                {name: 'insert', items: ['Table', 'Image']},
                {name: 'basicstyles', items: ['RemoveFormat']},
                {name: 'editing', items: ['Scayt']},
                {name: 'clipboard', items: ['Undo', 'Redo']},
                {
                    name: 'forms',
                    items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']
                },
                {name: 'document', items: ['Preview', 'Print']},
                {name: 'clipboard', items: ['PasteText', 'PasteFromWord']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll']},
                {name: 'basicstyles', items: ['Strike', 'CopyFormatting', 'RemoveFormat']},
                {name: 'paragraph', items: ['Outdent', 'Indent', 'BidiLtr', 'BidiRtl']},
                {name: 'insert', items: ['Smiley', 'SpecialChar']}
            ],

            height: 200,
            // An array of stylesheets to style the WYSIWYG area.
            // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
            contentsCss: ['https://cdn.ckeditor.com/4.9.2/full-all/contents.css', 'https://ckeditor.com/docs/ckeditor4/4.16.0/examples/assets/stylesheetparser/stylesheetparser.css'],
            stylesSet: [
                /* Inline Styles */
                {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                {name: 'Cited Work', element: 'cite'},
                {name: 'Inline Quotation', element: 'q'},

                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '1',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                {name: 'Borderless Table', element: 'table', styles: {'border-style': '1px', 'background-color': '#E6E6FA'}},
                {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
            ]
        };
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.hasOwnProperty('data')) {
            if (this.data) {
                this.formGroup.patchValue(this.data);
            }
        }
    }

    ngOnDestroy() {
        CampaignSendSMSComponent.ref = null;
    }

    cancel() {

    }

    filterNumbers(categories: any[], isSelectAll = false) {
        this.toData = [];
        if (isSelectAll) {
            this.toData = [...this.patientNumbers, ...this.patientEmergencyContactNumbers, ...this.referralContactNumbers, ...this.generalContactNumbers, ...this.TPPContactNumbers];
            return;
        }
        if (categories.includes(this.categories[this.PatientEmail])) {
            this.toData = this.patientNumbers;
        }
        if (categories.includes(this.categories[this.PatientEmergencyContact])) {
            this.toData = [...this.toData, ...this.patientEmergencyContactNumbers];
        }
        if (categories.includes(this.categories[this.ReferralContact])) {
            this.toData = [...this.toData, ...this.referralContactNumbers];
        }
        if (categories.includes(this.categories[this.GeneralContact])) {
            this.toData = [...this.toData, ...this.generalContactNumbers];
        }
        if (categories.includes(this.categories[this.TPPContact])) {
            this.toData = [...this.toData, ...this.TPPContactNumbers];
        }
    }

    getEmailTemplateDetails(id: string) {
        this.settingsService.getallbusinesstemplatesById(id).subscribe(response => {
            this.formGroup.get('subject').setValue(response.subject);
            this.formGroup.get('messageBody').setValue(response.body);
        });
    }

    onCLickCreate() {
        const formValues = this.formGroup.value;
        this.prepareBaseData(formValues);
        const data: CampaignModel = formValues;

        data.campaignAndList = [];

        formValues.to.forEach(obj => {
            data.campaignAndList.push({
                listName: obj.text,
                campaignListId: obj.value
            });
        });
        this.beforeSave.emit(data);
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                fromPage: FromPage.CampaignSendEmailSms,
                for: For.CreateList,
                data: {formData: data}
            }
        };
        this.createList.emit(true);
        this.router.navigate(['/communications/list/add'], fromState);
    }

    private async getNumbers() {

        const generalNumbers = await this.emailSmsService.getAllContactMobileNo(this.patientId).toPromise();
        const patientNumbers = await this.emailSmsService.getAllPatientMobileNo(this.patientId).toPromise();
        const patientEmergencyContactNumbers = await this.emailSmsService.getAllPatientEmergencyMobileNo(this.patientId).toPromise();

        generalNumbers.forEach(x => {
            if (x.contactType === ContactTypeEnum.Referral) {
                this.referralContactNumbers.push({text: x.emailId, value: x.emailId});
            }

            if (x.contactType === ContactTypeEnum.General) {
                this.generalContactNumbers.push({text: x.emailId, value: x.emailId});
            }

            if (x.contactType === ContactTypeEnum.ThirdParty) {
                this.TPPContactNumbers.push({text: x.emailId, value: x.emailId});
            }
        });

        patientNumbers.forEach(email => {
            this.patientNumbers.push({text: email, value: email});
        });

        patientEmergencyContactNumbers.forEach(email => {
            this.patientEmergencyContactNumbers.push({text: email, value: email});
        });

        if (this.openFor === PatientCommunication) {
            this.filterNumbers([], true);
        }
    }

    private getLists() {
        this.listService.getAll(this.appState.selectedUserLocationId).subscribe(response => {
            this.toData = response.map(value => {
                return {text: value.listName, value: value.id};
            });
        });
    }

    private getEmailTemplate() {
        this.settingsService.getAllBussinessTemplate(3).subscribe(response => {
            this.templates = response;
            this.templates.insert(0, {id: DummyGUID, name: 'No Template'});
        });
    }

    private handleButtonClick() {
        if (CampaignSendSMSComponent.ref) {
            CampaignSendSMSComponent.ref.clickSave.subscribe(() => {
                if (this.formGroup.invalid) {
                    this.formGroup.markAllAsTouched();
                    return;
                }
                this.save();
            });

            CampaignSendSMSComponent.ref.clickCancel.subscribe(() => {
                if (this.formGroup.invalid) {
                    this.formGroup.markAllAsTouched();
                    return;
                }
                this.save(true);
            });
        }
    }

    private prepareBaseData(data: any) {
        data['locationId'] = this.appState.selectedUserLocationId;
        data['parentBusinessId'] = this.appState.parentBusinessId;
        data['communicationType'] = CommunicationType.Email;
    }

    save(isDraft: boolean = false) {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            return;
        }
        const formValue = this.formGroup.value;
        formValue['locationId'] = this.appState.selectedUserLocationId;
        formValue['parentBusinessId'] = this.appState.parentBusinessId;
        formValue['communicationType'] = CommunicationType.Sms;

        if (this.openFor === CommunicationBulkEmailSms || this.openFor === PatientCommunication) {
            const data: Communication = formValue;

            if (isDraft) {
                data.status = CommunicationStatus.Draft;
            } else if (!this.data || !this.data.id) {
                data.status = CommunicationStatus.InProgress;
            }

            if (this.openFor === PatientCommunication) {
                data.patientId = this.patientId;
                data.source = EmailSourceType.Single;
            } else {
                data.source = EmailSourceType.Bulk;
            }

            data.communicationEmail = [];
            formValue.to.forEach(obj => {
                data.communicationEmail.push({
                    email: obj.value,
                    emailSendType: EmailSendType.To
                });
            });

            this.ccFormControl.value.forEach(obj => {
                let email = '';
                if (typeof obj === 'string') {
                    email = obj;
                } else {
                    email = obj.value;
                }
                data.communicationEmail.push({
                    email: email,
                    emailSendType: EmailSendType.Cc
                });
            });

            data.communicationAttachment = [];
            this.fileSelectService.selectedFiles.forEach(obj => {
                data.communicationAttachment.push({
                    fileName: obj.name,
                    attachedFile: obj.content
                });
            });

            delete data['to'];

            // Emit value to parent to concat the other properties
            this.beforeSave.emit(data);

            this.communicationService.create(data).subscribe(() => {
                this.alertService.displaySuccessMessage('Message created successfully.');
                this.afterSave.emit(true);
                CampaignSendSMSComponent.ref.closeDialog(Success);
            }, error => {
                this.alertService.displayErrorMessage(error);
                this.afterSave.emit(false);
            });
        } else if (this.openFor === CampaignEmailSms) {
            const data: CampaignModel = formValue;

            if (isDraft) {
                data.status = CommunicationStatus.Draft;
            } else if (!this.data || !this.data.id) {
                data.status = CommunicationStatus.InProgress;
            }

            data.campaignAndList = [];

            formValue.to.forEach(obj => {
                data.campaignAndList.push({
                    campaignListId: obj.value
                });
            });

            delete data['to'];

            // Emit value to parent to concat the other properties
            this.beforeSave.emit(data);

            if (this.data && this.data.id) {
                data.id = this.data.id;
                data.status = this.data.status;
                this.campaignService.update(data).subscribe(() => {
                    this.alertService.displaySuccessMessage('Campaign updated successfully.');
                    this.afterSave.emit(true);
                    CampaignSendSMSComponent.ref && CampaignSendSMSComponent.ref.closeDialog(Success);
                }, error => {
                    this.alertService.displayErrorMessage(error);
                    this.afterSave.emit(false);

                });
            } else {
                this.campaignService.create(data).subscribe(() => {
                    this.alertService.displaySuccessMessage('Campaign created successfully.');
                    this.afterSave.emit(true);
                    CampaignSendSMSComponent.ref && CampaignSendSMSComponent.ref.closeDialog();
                }, error => {
                    this.alertService.displayErrorMessage(error);
                    this.afterSave.emit(false);

                });
            }
        }
    }

    onEditorReady($event: CKEditor4.EventInfo) {
        this.editor = $event.editor;
    }

    onTextChange(event: any) {
        this.errormessage = null;
        const value = this.editor.document.getBody().getText();
        if (value && value.length > 612) {
            var bookmarks = this.editor.getSelection().createBookmarks2();

            this.editor.setData(value.substr(0, 612));

            if (bookmarks && bookmarks.length > 0) {
                bookmarks[0].startOffset = 612;
                bookmarks[0].endOffset = 612;
            }

            this.editor.getSelection().selectBookmarks( bookmarks );
            // let range = this.editor.createRange();
            // range.setStart(this.editor.root, 100);
            // this.editor.getSelection().selectRanges([range]);

            // 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890df

            this.errormessage = 'You can\'t type more than 612 characters.';
            return;
        }
        this.letterCount = (value.match(/[a-z]/gi) || []).length;
        this.numberCount = (value.match(/[0-9]/gi) || []).length;
        this.spaceCount = (value.match(/ /gi) || []).length;
        this.lineBreakCount = this.editor.document.getBody().getChildCount();

        let str = value.replace(/(\r\n|\n|\r)/gm, '');

        if (str.length > this.maxChars) {
            event = event.slice(0, this.maxChars);
            this.characterlength = event.length;
        } else {
            this.characterlength = str.length;
        }

        if (this.characterlength >= 592) {
            this.errormessage = 'To avoid SMS failures, it is recommended to leave atleast 20 characters remaining to replace the tags with actual values while sending SMS.';
        } else {
            this.errormessage = null;
        }

        if (this.characterlength <= 160) {
            this.smscount = 1;
        } else if (this.characterlength >= 160 && this.characterlength <= 306) {
            this.smscount = 2;
        } else if (this.characterlength >= 306 && this.characterlength <= 459) {
            this.smscount = 3;
        } else {
            this.smscount = 4;
        }
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(CampaignSendSMSComponent, {
                title: 'SEND SMS',
                width: '778px',
                cancelBtnTitle: 'Save As Draft',
                saveBtnTitle: 'Send',
                hideSave: false,
                hideOnSave: false,
                hideOnCancel: false,
                data: data
            }
        );
        return this.ref;
    }

}
