import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppDialogService, IPopupAction} from 'src/app/shared/services/app-dialog.service';
import {CampaignEmailSms, CampaignSendEmailComponent} from './campaign-send-email/campaign-send-email.component';
import {CampaignModel} from '../../../model/campaign';
import {CommunicationType} from '../../../../patients/patient-commmunications/enum/communication-type';
import {CampaignService} from '../../../api/campaign/campaign.service';
import {AppAlertService} from '../../../../../../shared/services/app-alert.service';
import {Success} from '../../../../../../app.component';
import {CANCEL} from '../../../../../../shared/popup-component/popup-component.component';

@Component({
    selector: 'app-create-campaign',
    templateUrl: './create-campaign-list.component.html',
    styleUrls: ['./create-campaign-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CreateCampaignListComponent implements OnInit {

    private static ref: IPopupAction;

    readonly openFor = CampaignEmailSms;

    @Input()
    isCopying: boolean = false;
    @Input()
    id: string;
    @Input()
    data: CampaignModel;

    @ViewChild(CampaignSendEmailComponent, {static: false}) campaignSendEmailComp: CampaignSendEmailComponent;

    emailSms = 'Email';
    campaign: CampaignModel;

    form: FormGroup = new FormGroup({
        name: new FormControl('', [Validators.required])
    });

    constructor(
        private alertService: AppAlertService,
        private campaignService: CampaignService,
    ) {
    }

    ngOnInit() {
        if (this.data) {
            this.campaign = this.data;
            this.campaign['to'] = this.data.campaignAndList.map(value => {
                return {text: value.listName, value: value.campaignListId};
            });
            this.emailSms = this.data.communicationType == CommunicationType.Email ? 'Email' : 'Sms';
            this.form.get('name').setValue(this.data.campaignName);
        }
        this.getCampaignById();
        CreateCampaignListComponent.ref.clickSave.subscribe(() => {
            if (this.form.invalid) {
                this.form.markAllAsTouched();
                return;
            }
            this.save(false);
        });

        CreateCampaignListComponent.ref.clickCancel.subscribe(() => {
            if (this.id) {
                return;
            }
            if (this.form.invalid) {
                this.form.markAllAsTouched();
                return;
            }
            this.save(true);
        });
    }

    onSelectionChange() {
        CreateCampaignListComponent.ref.changeTitle(`${this.isCopying ? 'COPY' : this.id ? 'EDIT' : 'CREATE'} ${this.emailSms ? this.emailSms.toUpperCase() : 'Email'} CAMPAIGN`);
    }

    save(isDraft: boolean) {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return;
        }

        this.campaignSendEmailComp.save(isDraft);
    }

    beforeSave(data: CampaignModel) {
        data.campaignName = this.form.get('name').value;
        data.communicationType = this.emailSms === 'Email' ? CommunicationType.Email : CommunicationType.Sms;
    }

    afterSave($event: boolean) {
        if ($event) {
            CreateCampaignListComponent.ref.closeDialog(Success);
        }
    }

    private getCampaignById() {
        if (this.id) {
            this.campaignService.getById(this.id).subscribe(response => {
                if (this.isCopying) {
                    delete response.id;
                }
                this.campaign = response;
                this.campaign['to'] = response.campaignAndList.map(value => {
                    return {text: value.listName, value: value.campaignListId};
                });
                this.emailSms = response.communicationType == CommunicationType.Email ? 'Email' : 'Sms';
                this.form.get('name').setValue(response.campaignName);
            }, error => {
                this.alertService.displayErrorMessage(error);
            });
        }
    }

    static open(appDialogService: AppDialogService, item?: CampaignModel, data: { [x: string]: any } = {}): IPopupAction {
        if (item) {
            data = {...data, id: item.id};
        }
        this.ref = appDialogService.open(CreateCampaignListComponent, {
                title: `${data['isCopying'] ? 'COPY' : (item && item.id ? 'EDIT' : 'CREATE')} ${item && item.communicationType === CommunicationType.Email ? 'EMAIL' : 'SMS'} CAMPAIGN`,
                width: '778px',
                saveBtnTitle: 'Save',
                cancelBtnTitle: item ? 'Cancel' : 'Save As Draft',
                hideOnSave: false,
                hideOnCancel: !!item,
                data: data
            }
        );
        return this.ref;
    }

    createList() {
        CreateCampaignListComponent.ref && CreateCampaignListComponent.ref.closeDialog(CANCEL);
    }
}
