import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppDialogService, IPopupAction} from '../../../../../../../shared/services/app-dialog.service';
import {EmailSmsService} from '../../../../shared/api/email-sms.service';
import {ContactService} from '../../../../../../../services/app.contact.service';
import {SettingsService} from '../../../../../../../services/app.settings.service';
import {DummyGUID, Success} from '../../../../../../../app.component';
import {FileSelectService} from '../../../../shared/api/file-select.service';
import {CommunicationService} from '../../../../api/communication/communication.service';
import {AppAlertService} from '../../../../../../../shared/services/app-alert.service';
import {Communication, CommunicationStatus, EmailSendType, EmailSourceType} from '../../../../model/communication.interface';
import {AppState} from '../../../../../../../app.state';
import {CampaignModel} from '../../../../model/campaign';
import {ListService} from '../../../../api/list/list.service';
import {CampaignService} from '../../../../api/campaign/campaign.service';
import {CommunicationType} from '../../../../../patients/patient-commmunications/enum/communication-type';
import {ContactTypeEnum} from '../../../../../../../enum/application-data-enum';
import {NavigationExtras, Router} from '@angular/router';
import {For, FromPage} from '../../../../../../../shared/interface';
import {CANCEL} from '../../../../../../../shared/popup-component/popup-component.component';

export const CommunicationBulkEmailSms = 1;
export const CampaignEmailSms = 2;
export const PatientCommunication = 3;

@Component({
    selector: 'app-campaign-send-email',
    templateUrl: './campaign-send-email.component.html',
    styleUrls: ['./campaign-send-email.component.css']
})
export class CampaignSendEmailComponent implements OnInit, OnChanges, OnDestroy {
    private static ref: IPopupAction;

    readonly CampaignEmailSms = CampaignEmailSms;
    readonly PatientCommunication = PatientCommunication;

    readonly PatientEmail = 0;
    readonly PatientEmergencyContact = 1;
    readonly ReferralContact = 2;
    readonly GeneralContact = 3;
    readonly TPPContact = 4;

    fromPatient: boolean = true;
    campaign: boolean = false;

    @Input()
    data: any;
    @Input()
    patientId: string;
    @Input()
    openFor: number = PatientCommunication;
    @Output()
    beforeSave: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    afterSave: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output()
    createList: EventEmitter<boolean> = new EventEmitter<boolean>();

    ckeConfig: any;

    toData = [];

    categories = [
        {text: 'Patient Email', value: 2},
        {text: 'Patient Emergency Contact', value: 3},
        {text: 'Referral Contact', value: 4},
        {text: 'General Contact', value: 5},
        {text: 'TPP Contact', value: 6},
    ];

    templates = [];

    patientEmails = [];
    patientEmergencyContactEmails = [];
    referralContactEmails = [];
    generalContactEmails = [];
    TPPContactEmails = [];

    isDisplayCCField = false;

    ccFormControl = new FormControl([], Validators.required);
    formGroup: FormGroup = new FormGroup({
        to: new FormControl([], [Validators.required]),
        templateId: new FormControl(DummyGUID, [Validators.required]),
        subject: new FormControl('', [Validators.required]),
        messageBody: new FormControl('', [Validators.required]),
    });

    constructor(
        private appState: AppState,
        private emailSmsService: EmailSmsService,
        private contactService: ContactService,
        private settingsService: SettingsService,
        public fileSelectService: FileSelectService,
        public communicationService: CommunicationService,
        public campaignService: CampaignService,
        public listService: ListService,
        public alertService: AppAlertService,
        public appDialogService: AppDialogService,
        public router: Router,
    ) {
        this.fileSelectService.dialogService = this.appDialogService;
    }

    ngOnInit() {
        this.fileSelectService.clear();
        this.fromPatient = this.openFor === PatientCommunication;
        this.campaign = this.openFor === CampaignEmailSms;

        this.handleButtonClick();

        this.getEmailTemplate();
        if (this.openFor === CampaignEmailSms) {
            this.getLists();
        } else {
            this.getEmails();
        }

        this.ckeConfig = {
            extraAllowedContent: 'img[width,height,align]',
            // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets-all
            extraPlugins: 'easyimage',
            removePlugins: 'elementspath',

            toolbar: [
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                },
                {name: 'styles', items: ['Font', 'FontSize']},
                {name: 'colors', items: ['TextColor', 'BGColor']},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste']},
                {name: 'basicstyles', items: ['Subscript', 'Superscript']},
                {name: 'insert', items: ['PageBreak', 'HorizontalRule']},
                {name: 'links', items: ['Link', 'Unlink']},
                {name: 'insert', items: ['Table', 'Image']},
                {name: 'basicstyles', items: ['RemoveFormat']},
                {name: 'editing', items: ['Scayt']},
                {name: 'clipboard', items: ['Undo', 'Redo']},
                {
                    name: 'forms',
                    items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']
                },
                {name: 'document', items: ['Preview', 'Print']},
                {name: 'clipboard', items: ['PasteText', 'PasteFromWord']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll']},
                {name: 'basicstyles', items: ['Strike', 'CopyFormatting', 'RemoveFormat']},
                {name: 'paragraph', items: ['Outdent', 'Indent', 'BidiLtr', 'BidiRtl']},
                {name: 'insert', items: ['Smiley', 'SpecialChar']}
            ],

            height: 200,
            // An array of stylesheets to style the WYSIWYG area.
            // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
            contentsCss: ['https://cdn.ckeditor.com/4.9.2/full-all/contents.css', 'https://ckeditor.com/docs/ckeditor4/4.16.0/examples/assets/stylesheetparser/stylesheetparser.css'],
            stylesSet: [
                /* Inline Styles */
                {name: 'Marker', element: 'span', attributes: {'class': 'marker'}},
                {name: 'Cited Work', element: 'cite'},
                {name: 'Inline Quotation', element: 'q'},

                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '1',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                {name: 'Borderless Table', element: 'table', styles: {'border-style': '1px', 'background-color': '#E6E6FA'}},
                {name: 'Square Bulleted List', element: 'ul', styles: {'list-style-type': 'square'}}
            ]
        };
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.hasOwnProperty('data')) {
            if (this.data) {
                this.formGroup.patchValue(this.data);
            }
        }
    }

    ngOnDestroy() {
        CampaignSendEmailComponent.ref = null;
    }

    cancel() {

    }

    filterEmails(categories: any[], isSelectAll = false) {
        this.toData = [];
        if (isSelectAll) {
            this.toData = [...this.patientEmails, ...this.patientEmergencyContactEmails, ...this.referralContactEmails, ...this.generalContactEmails, ...this.TPPContactEmails];
            return;
        }
        if (categories.includes(this.categories[this.PatientEmail])) {
            this.toData = [...this.toData, ...this.patientEmails];
        }
        if (categories.includes(this.categories[this.PatientEmergencyContact])) {
            this.toData = [...this.toData, ...this.patientEmergencyContactEmails];
        }
        if (categories.includes(this.categories[this.ReferralContact])) {
            this.toData = [...this.toData, ...this.referralContactEmails];
        }
        if (categories.includes(this.categories[this.GeneralContact])) {
            this.toData = [...this.toData, ...this.generalContactEmails];
        }
        if (categories.includes(this.categories[this.TPPContact])) {
            this.toData = [...this.toData, ...this.TPPContactEmails];
        }
    }

    getEmailTemplateDetails(id: string) {
        this.settingsService.getallbusinesstemplatesById(id).subscribe(response => {
            this.formGroup.get('subject').setValue(response.subject);
            this.formGroup.get('messageBody').setValue(response.body);
        });
    }

    onCLickCreate() {
        const formValues = this.formGroup.value;
        this.prepareBaseData(formValues);
        const data: CampaignModel = formValues;

        data.campaignAndList = [];

        formValues.to.forEach(obj => {
            data.campaignAndList.push({
                listName: obj.text,
                campaignListId: obj.value
            });
        });
        this.beforeSave.emit(data);
        let fromState: NavigationExtras = {
            state: {
                fromState: true,
                fromPage: FromPage.CampaignSendEmailSms,
                for: For.CreateList,
                data: {formData: data}
            }
        };
        this.createList.emit(true);
        this.router.navigate(['/communications/list/add'], fromState);
    }

    private async getEmails() {
        // const allContacts = await this.contactService.getAllContact().toPromise();
        //
        // allContacts.forEach(x => {
        //     if (x.status && x.contactType === 1) {
        //         this.referralContactEmails.push({text: x.emailId, value: x.emailId});
        //     }
        //
        //     if (x.status && x.contactType === 2) {
        //         this.generalContactEmails.push({text: x.emailId, value: x.emailId});
        //     }
        //
        //     if (x.status && x.contactType === 3) {
        //         this.TPPContactEmails.push({text: x.emailId, value: x.emailId});
        //     }
        // });

        const generalEmails = await this.emailSmsService.getAllContactEmail(this.patientId).toPromise();
        const patientEmails = await this.emailSmsService.getAllPatientEmail(this.patientId).toPromise();
        const patientEmergencyContactEmails = await this.emailSmsService.getAllPatientEmergencyEmail(this.patientId).toPromise();

        generalEmails.forEach(x => {
            if (x.contactType === ContactTypeEnum.Referral) {
                this.referralContactEmails.push({text: x.emailId, value: x.emailId});
            }

            if (x.contactType === ContactTypeEnum.General) {
                this.generalContactEmails.push({text: x.emailId, value: x.emailId});
            }

            if (x.contactType === ContactTypeEnum.ThirdParty) {
                this.TPPContactEmails.push({text: x.emailId, value: x.emailId});
            }
        });

        patientEmails.forEach(email => {
            this.patientEmails.push({text: email, value: email});
        });

        patientEmergencyContactEmails.forEach(email => {
            this.patientEmergencyContactEmails.push({text: email, value: email});
        });

        if (this.openFor === PatientCommunication) {
            this.filterEmails([], true);
        }
    }

    private getLists() {
        this.listService.getAll(this.appState.selectedUserLocationId).subscribe(response => {
            this.toData = response.map(value => {
                return {text: value.listName, value: value.id};
            });
        });
    }

    private getEmailTemplate() {
        this.settingsService.getAllBussinessTemplate(2).subscribe(response => {
            this.templates = response;
            this.templates.insert(0, {id: DummyGUID, name: 'No Template'});
        });
    }

    private handleButtonClick() {
        if (CampaignSendEmailComponent.ref) {
            CampaignSendEmailComponent.ref.clickSave.subscribe(() => {
                if (this.formGroup.invalid) {
                    this.formGroup.markAllAsTouched();
                    return;
                }
                this.save();
            });

            CampaignSendEmailComponent.ref.clickCancel.subscribe(() => {
                if (this.formGroup.invalid) {
                    this.formGroup.markAllAsTouched();
                    return;
                }
                this.save(true);
            });
        }
    }

    save(isDraft: boolean = false) {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            return;
        }
        const formValue = this.formGroup.value;
        this.prepareBaseData(formValue);

        if (this.openFor === CommunicationBulkEmailSms || this.openFor === PatientCommunication) {
            const data: Communication = formValue;

            this.prepareData(data, isDraft);

            // Emit value to parent to concat the other properties
            this.beforeSave.emit(data);

            this.communicationService.create(data).subscribe((response) => {
                this.alertService.displaySuccessMessage('Email sent successfully.');
                this.afterSave.emit(true);
                CampaignSendEmailComponent.ref.closeDialog(Success);
            }, error => {
                this.alertService.displayErrorMessage(error);
                this.afterSave.emit(false);
            });
        } else if (this.openFor === CampaignEmailSms) {
            const data: CampaignModel = formValue;

            if (isDraft) {
                data.status = CommunicationStatus.Draft;
            } else if (!this.data || !this.data.id) {
                data.status = CommunicationStatus.InProgress;
            }

            data.campaignAndList = [];

            formValue.to.forEach(obj => {
                data.campaignAndList.push({
                    campaignListId: obj.value
                });
            });

            delete data['to'];

            // Emit value to parent to concat the other properties
            this.beforeSave.emit(data);

            if (this.data && this.data.id) {
                data.id = this.data.id;
                data.status = this.data.status;
                this.campaignService.update(data).subscribe(() => {
                    this.alertService.displaySuccessMessage('Campaign updated successfully.');
                    this.afterSave.emit(true);
                    CampaignSendEmailComponent.ref && CampaignSendEmailComponent.ref.closeDialog(Success);
                }, error => {
                    this.alertService.displayErrorMessage(error);
                    this.afterSave.emit(false);

                });
            } else {
                this.campaignService.create(data).subscribe(() => {
                    this.alertService.displaySuccessMessage('Campaign created successfully.');
                    this.afterSave.emit(true);
                    CampaignSendEmailComponent.ref && CampaignSendEmailComponent.ref.closeDialog();
                }, error => {
                    this.alertService.displayErrorMessage(error);
                    this.afterSave.emit(false);

                });
            }
        }
    }

    private prepareBaseData(data: any) {
        data['locationId'] = this.appState.selectedUserLocationId;
        data['parentBusinessId'] = this.appState.parentBusinessId;
        data['communicationType'] = CommunicationType.Email;
    }

    private prepareData(data: any, isDraft: boolean) {
        if (isDraft) {
            data.status = CommunicationStatus.Draft;
        } else if (!this.data || !this.data.id) {
            data.status = CommunicationStatus.InProgress;
        }

        if (this.openFor === PatientCommunication) {
            data.patientId = this.patientId;
            data.source = EmailSourceType.Single;
        } else {
            data.source = EmailSourceType.Bulk;
        }

        data.communicationEmail = [];
        data.to.forEach(obj => {
            data.communicationEmail.push({
                email: obj.value,
                emailSendType: EmailSendType.To
            });
        });

        this.ccFormControl.value.forEach(obj => {
            let email = '';
            if (typeof obj === 'string') {
                email = obj;
            } else {
                email = obj.value;
            }
            data.communicationEmail.push({
                email: email,
                emailSendType: EmailSendType.Cc
            });
        });

        data.communicationAttachment = [];
        this.fileSelectService.selectedFiles.forEach(obj => {
            data.communicationAttachment.push({
                fileName: obj.name,
                attachedFile: obj.content
            });
        });

        delete data['to'];
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(CampaignSendEmailComponent, {
                title: 'SEND EMAIL',
                width: '778px',
                cancelBtnTitle: 'Save As Draft',
                saveBtnTitle: 'Send',
                hideSave: false,
                hideOnSave: false,
                hideOnCancel: false,
                data: data
            }
        );
        return this.ref;
    }
}
