import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {CommunicationType} from '../../../patients/patient-commmunications/enum/communication-type';
import {DxPopoverComponent} from 'devextreme-angular';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {BaseGridComponent} from '../../../shared-content-full-layout/base-grid/base-grid.component';
import {AppState} from '../../../../../app.state';
import {CampaignModel} from '../../model/campaign';
import {CreateCampaignListComponent} from './create-campaign/create-campaign-list.component';
import {Router} from '@angular/router';
import {CampaignService} from '../../api/campaign/campaign.service';
import {BlockUI} from 'ng-block-ui';
import {map} from 'rxjs/operators';
import {CommunicationStatus} from '../../model/communication.interface';
import {AppoinmentDateUtils} from '../../../appointment/AppoinmentDateUtils';
import {Success} from '../../../../../app.component';
import {
    DeleteConfirmationDialogComponent
} from '../../../shared-content-full-layout/dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {AppAlertService} from '../../../../../shared/services/app-alert.service';
import {FromPage} from '../../../../../shared/interface';

@Component({
    selector: 'app-campaign',
    templateUrl: './campaign.component.html',
    styleUrls: ['./campaign.component.css']
})
export class CampaignComponent extends BaseGridComponent implements OnInit, AfterViewInit {

    readonly EDIT = 'EDIT';
    readonly PRINT = 'PRINT';
    readonly DELETE = 'DELETE';
    readonly COPY = 'COPY';
    readonly Email = CommunicationType.Email;
    readonly Sms = CommunicationType.Sms;
    readonly UnSend = CommunicationStatus.Draft;

    @BlockUI() blockUi;
    actionItem: CampaignModel; // Item that was selected through action(GEAR ICON)
    @ViewChild('actionPopup', {static: true}) actionPopup: DxPopoverComponent;
    @ViewChild('duplicateDialog', {static: false}) duplicateDialog;

    @Input() patientId: string;

    campaigns: CampaignModel[];

    constructor(
        private alertService: AppAlertService,
        private appDialogService: AppDialogService,
        private campaignService: CampaignService,
        protected appState: AppState,
        protected router: Router,
    ) {
        super(router, appState);
    }

    ngOnInit() {
        this.getCampaigns();
    }

    ngAfterViewInit() {
        if (this.routerState && this.routerState.fromPage == FromPage.CommunicationListCreate) {
            const data:CampaignModel  = this.routerState.data.formData as CampaignModel;
            this.callbackAddEditPopup(CreateCampaignListComponent.open(this.appDialogService, null, {data: data}));
        }
    }

    openActionPopup(dataItem, event) {
        this.actionItem = dataItem;

        this.actionPopup.target = event.target;
        this.actionPopup.instance.show();
    }

    doAction(action: string) {
        this.actionPopup.instance.hide();
        switch (action) {
            case this.EDIT:
                this.callbackAddEditPopup(CreateCampaignListComponent.open(this.appDialogService, this.actionItem));
                break;
            case this.PRINT:
                break;
            case this.COPY:
                this.appDialogService.open(this.duplicateDialog, {
                    title: 'DO YOU WISH TO DUPLICATE THIS CAMPAIGN?',
                    width: '800px',
                    height: '300px',
                    saveBtnTitle: 'Yes',
                    cancelBtnTitle: 'No'
                }).clickSave.subscribe(() => {
                    this.callbackAddEditPopup(CreateCampaignListComponent.open(this.appDialogService, this.actionItem, {isCopying: true}));
                });
                break;
            case this.DELETE:
                const ref = DeleteConfirmationDialogComponent.open(this.appDialogService, '', {
                    hideOnSave: false,
                    title: 'Do you wish to delete this Campaign ?'
                });
                ref.clickSave.subscribe(() => {
                    this.blockUi.start();
                    this.campaignService.delete(this.actionItem.id).subscribe(() => {
                        this.alertService.displaySuccessMessage('Campaign Deleted successfully.');
                        this.blockUi.stop();
                        this.getCampaigns();
                        ref.closeDialog();
                    }, () => {
                        this.alertService.displayErrorMessage('Failed to delete Campaign, please try again later');
                        this.blockUi.stop();
                    });
                });
                break;
        }
    }

    create() {
        this.callbackAddEditPopup(CreateCampaignListComponent.open(this.appDialogService));
    }

    private callbackAddEditPopup(popup: IPopupAction) {
        popup && popup.close.subscribe((value) => {
            if (value === Success) {
                setTimeout(() => {
                    this.getCampaigns();
                }, 300);
            }
        });
    }

    private getCampaigns() {
        this.blockUi.start();
        this.campaignService.getAll(this.appState.selectedUserLocationId).pipe(
            map(data => {
                data.map(value => {
                    value['type'] = value.communicationType === CommunicationType.Email ? 'Email' : 'Sms';
                    value['createdDateStr'] = AppoinmentDateUtils.formatDateTime(value.createdDate, 'DD MMM, YYYY hh:mm A', false);
                    switch (value.status) {
                        case CommunicationStatus.Draft:
                            value['statusStr'] = 'Draft';
                            break;
                        case CommunicationStatus.InProgress:
                            value['statusStr'] = 'In Progress';
                            break;
                        case CommunicationStatus.Failed:
                            value['statusStr'] = 'Failed';
                            break;
                        case CommunicationStatus.Completed:
                            value['statusStr'] = 'Sent';
                            break;
                    }
                    value['autoDeleteDateStr'] = AppoinmentDateUtils.formatDateTime(AppoinmentDateUtils.addAndGetNewValue(value.createdDate, 90, 'd'), 'DD MMM, YYYY', false);

                    return value;
                });
                return data;
            })
        ).subscribe(response => {
            this.campaigns = response;
            this.gridData = this.campaigns;
            this.gridData = this.campaigns;
            this.activeFilters.filters = [];
            this.loadItems();
            this.blockUi.stop();
        }, () => {
            this.blockUi.stop();
        });
    }

}
