import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DxPopoverComponent} from 'devextreme-angular';
import {CommunicationGridComponent} from '../../shared/communication-grid/communication-grid.component';
import {AppDialogService} from '../../../../../shared/services/app-dialog.service';
import {
  CampaignSendEmailComponent,
  CommunicationBulkEmailSms
} from '../campaign/create-campaign/campaign-send-email/campaign-send-email.component';
import {CampaignSendSMSComponent} from '../campaign/create-campaign/campaign-send-sms/campaign-send-sms.component';
import {AddNoteComponent} from '../list/add-note/add-note.component';
import {Success} from '../../../../../app.component';

@Component({
  selector: 'app-bulk',
  templateUrl: './bulk.component.html',
  styleUrls: ['./bulk.component.css']
})
export class BulkComponent implements OnInit {

  @ViewChild('filterActionPopup', {static: true}) filterActionPopup: DxPopoverComponent;
  @ViewChild('communicationGrid', {static: true}) communicationGrid: CommunicationGridComponent;

  @Input() patientId: string;

  isDisplayClear: boolean;

  constructor(private appDialogService: AppDialogService) {
  }

  ngOnInit() {
  }

  onSendEmailClick() {
    CampaignSendEmailComponent.open(this.appDialogService, {fromPatient: false, openFor: CommunicationBulkEmailSms})
        .close.subscribe(value => {
          if (value === Success) {
            this.communicationGrid.loadCommunication();
          }
        });
  }

  onSendSmsClick() {
    CampaignSendSMSComponent.open(this.appDialogService, {fromPatient: false, openFor: CommunicationBulkEmailSms})
        .close.subscribe(value => {
          if (value === Success) {
            this.communicationGrid.loadCommunication();
          }
        });
  }

  onAddNoteClick() {
    const dialogRef = AddNoteComponent.open(this.appDialogService);
  }

  filter(event) {
    this.filterActionPopup.target = event.target;
    this.filterActionPopup.instance.show();
  }

  filterByDate() {
    this.communicationGrid.filterByDate(this.filterActionPopup);
  }

  filterByType() {
    this.communicationGrid.filterByType(this.filterActionPopup);
  }

  clearFilter() {
    this.isDisplayClear = false;
    this.communicationGrid.clearFilter();
  }

}
