import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    Optional,
    Output,
    Self,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {from, Subject} from 'rxjs';
import {AppState} from '../../../../app.state';
import {ApplicationDataService} from '../../../../services/app.applicationdata.service';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {debounceTime, map, switchMap, tap} from 'rxjs/operators';
import {MultiSelectComponent} from '@progress/kendo-angular-dropdowns';
import {RemoveTagEvent} from '@progress/kendo-angular-dropdowns/dist/es2015/common/models/remove-tag-event';
import {AppUtils} from '../../../../shared/AppUtils';

@Component({
    selector: 'app-kendo-multi-select',
    templateUrl: './app-kendo-multi-select.component.html',
    styleUrls: ['./app-kendo-multi-select.component.css'],
    providers: [{provide: MatFormFieldControl, useExisting: AppKendoMultiSelectComponent}],
})
export class AppKendoMultiSelectComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnChanges, AfterViewInit, OnDestroy {
    readonly SELECT_ALL = 'SELECT_ALL';
    @ViewChild('list', {static: false}) list: MultiSelectComponent;

    @Input()
    selectAll: boolean = false;

    @Input()
    add: { value: boolean, text: string } = {value: false, text: ''};

    @Input()
    maxDisplay: number = 0;

    @Input()
    checkboxes: boolean = false;

    @Input()
    dropDownArrow: boolean = false;

    @Input()
    disabledTagsVal: string[];

    @Input()
    valueField: string = 'value';

    @Input()
    textField: string = 'text';

    @Input()
    allowCustom: boolean = false;

    @Input()
    filterable: boolean = false;

    @Input()
    isDisabledTag: boolean = false;

    @Input()
    clearButton: boolean = false;

    @Input('data')
    source: any[] = [];

    // @Input()
    _value = null;

    @Output()
    valueChanged: EventEmitter<any[] | null> = new EventEmitter<any[] | null>();
    @Output()
    removeTag: EventEmitter<RemoveTagEvent> = new EventEmitter<RemoveTagEvent>();
    @Output()
    create: EventEmitter<any> = new EventEmitter<any>();

    _data: any[] = [];

    valuePrimitive = false;
    static nextId = 0;
    focused = false;
    controlType = 'app-kendo-multi-select';
    id = `app-kendo-multi-select${AppKendoMultiSelectComponent.nextId++}`;
    _class = `app-kendo-multi-select${AppKendoMultiSelectComponent.nextId++}`;
    describedBy = '';
    stateChanges: Subject<void> = new Subject<void>();
    filter: string;
    onChange = (value) => {
    };
    onTouched = () => {
    };

    constructor(
        private appState: AppState,
        private applicationDataService: ApplicationDataService,
        private _focusMonitor: FocusMonitor,
        private _elementRef: ElementRef<HTMLElement>,
        @Optional() @Self() public ngControl: NgControl
    ) {
        this._data = this.source.slice();
        _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
            if (this.focused && !origin) {
                this.onTouched();
            }
            this.focused = !!origin;
            this.stateChanges.next();
        });

        if (this.ngControl != null) {
            this.ngControl.valueAccessor = this;
        }
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('source')) {
            this.source = changes.source.currentValue;
            this._data = this.source ? this.source.slice() : [].slice();
            console.log(this._data)
            if (this.list && this.filter && !changes.source.firstChange) {
                this.list.handleFilter(this.filter);
            }
        }
        if (changes.hasOwnProperty('value') && changes.value.currentValue && changes.value.previousValue[this.valueField] !== changes.value.currentValue[this.valueField]) {
            this.changeVal(changes.value.currentValue);
        }
    }

    ngAfterViewInit(): void {
        this.disableTags();
        if (this.filterable) {
            const contains = (value) => (s) => s[this.textField].toLowerCase().indexOf(value.toLowerCase()) !== -1;

            this.list.filterChange
                .asObservable()
                .pipe(
                    switchMap((value) =>
                        from([this.source]).pipe(
                            tap(() => (this.list.loading = true)),
                            debounceTime(500),
                            map((data) => data.filter(contains(value)))
                        )
                    )
                )
                .subscribe((x) => {
                    this.list.loading = false;
                    this._data = x;
                });
        }
    }

    handleFilter(value: string) {
        if (!value) {
            this._data = this.source;
            this.filter = value;
            return;
        }
        this.filter = value;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(value: any[]): void {
        this.value = value && value.length > 0 ? value : null;
    }

    markAsTouched() {
        if (!this.touched) {
            this.onTouched();
        }
    }

    setDisabledState(disabled: boolean) {
        this.disabled = disabled;
    }

    changeVal(val: any[]) {
        this.markAsTouched();
        if (!this.disabled) {
            if (val.some(value => value[this.valueField] === this.SELECT_ALL)) {
                val = this._data;
            }
            this.value = val && val.length > 0 ? val : null;
            this.valueChanged.emit(this.value || []);
            this.onChange(this.value);
        }
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return control.errors;
    }

    get touched(): boolean {
        return this.ngControl && this.ngControl.touched;
    }

    get errorState(): boolean {
        return this.ngControl && this.ngControl.invalid && this.touched;
    }

    get empty() {
        return !this.value;
    }

    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    @Input()
    get placeholder(): string {
        return this._placeholder;
    }

    set placeholder(value: string) {
        this._placeholder = value;
        this.stateChanges.next();
    }

    private _placeholder: string;

    @Input()
    get required(): boolean {
        return this._required;
    }

    set required(value: boolean) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _required = false;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _disabled = false;

    @Input()
    get value(): any[] | null {
        return this._value;
    }

    set value(data: any[] | null) {
        this._value = data || null;
        this.stateChanges.next();
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this._focusMonitor.stopMonitoring(this._elementRef);
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }

    onContainerClick(event: MouseEvent) {
        // this.dateBox.instance.open();
    }

    private disableTags() {
        if (this.isDisabledTag && this.list) {
            const tagPrefix = this.list.tagPrefix;
            if (this.disabledTagsVal) {
                let style = '';
                this.disabledTagsVal.forEach(tagVal => {
                    const tagId = tagPrefix + '-' + tagVal;
                    style += '#' + tagId + ' .k-select{' +
                        'display: none;';
                });
                this.findOrCreatStyleTag(style, 'disableTagToRemove' + this.id);
            }
        }
    }

    findOrCreatStyleTag(style: string, id: string) {
        let head = document.getElementsByTagName('head')[0];
        let element = document.getElementById(id);

        if (!element) {
            element = document.createElement('style');
            element.id = id;
            head.appendChild(element);
        }
        element.textContent = '';
        element.appendChild(document.createTextNode(style));
    }

    itemDisabled = (itemArgs: { dataItem: string; index: number }) => {
        return this.isDisabledTag && this.disabledTagsVal.indexOf(itemArgs.dataItem[this.valueField]) > -1;
    };

    get isOpen(): boolean {
        return this.list.isOpen;
    }

    tagMapper = (tags: any[]): any[] => {
        return this.maxDisplay === 0 ? tags : tags.length <= this.maxDisplay ? tags : [tags];
    };

    toggle() {
        this.list.toggle(!this.isOpen);
    }

    selectAllToggle(value) {
        if (value) {
            this.changeVal(this._data);
        } else {
            this.changeVal([]);
        }
    }

    onRemoveTag(e) {
        this.removeTag.emit(e);
        if (this.disabledTagsVal && this.disabledTagsVal.indexOf(e.dataItem[this.valueField].toLowerCase()) > -1) {
            e.preventDefault();
        }
    }

    addNew() {
        this.create.emit(true);
    }
}
