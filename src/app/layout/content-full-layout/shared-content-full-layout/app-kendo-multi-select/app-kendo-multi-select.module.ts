import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {MultiSelectModule} from '@progress/kendo-angular-dropdowns';
import {AppKendoMultiSelectComponent} from './app-kendo-multi-select.component';
import {MatIconModule} from '@angular/material';

@NgModule({
  declarations: [
    AppKendoMultiSelectComponent,
  ],
  imports: [CommonModule, MultiSelectModule, MatIconModule],
  exports: [
    AppKendoMultiSelectComponent
  ],
})
export class AppKendoMultiSelectModule {}
