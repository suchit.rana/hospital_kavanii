import {
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    NgZone,
    OnChanges, OnDestroy,
    Output,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import {Maps, MapService} from 'src/app/services/app.map.service';

const place = null as google.maps.places.PlaceResult;
type Components = typeof place.address_components;

@Component({
    selector: 'app-google-address',
    templateUrl: './google-address.component.html',
    styleUrls: ['./google-address.component.css'],
    providers: [MapService]
})
export class GoogleAddressComponent implements OnChanges, OnDestroy {
    @ViewChild('search', {static: true})
    public searchElementRef: ElementRef<HTMLInputElement>;

    public entries = [];
    @Input() containerClass: string;
    @Input() country: string;
    @Input() country_name: string;

    @Input() selectedAddress: string = '';
    @Output() address: EventEmitter<any> = new EventEmitter<any>();
    @Output() addressManual: EventEmitter<string> = new EventEmitter<string>();

    apperance = 'outline';
    show: boolean = false;
    autocomplete;

    constructor(private apiService: MapService, private ngZone: NgZone) {
        if (this.country) {
            apiService.api.then((maps) => {
                this.initAutocomplete(maps);
            });
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('country')) {
            this.country = changes['country'].currentValue;
            this.apiService.api.then((maps) => {
                this.initAutocomplete(maps);
            });
        }
    }

    ngOnDestroy() {
        if (this.autocomplete) {
            const addressContainers = document.getElementsByClassName('pac-container');
            if (addressContainers) {
                for (let i = 0; i < addressContainers.length; i++) {
                    document.body.removeChild(addressContainers.item(i));
                }
            }
        }
    }

    addressManualChange($event, isCheckForAddress: boolean = true) {
        if (isCheckForAddress) {
            const autocompleteService = new google.maps.places.AutocompleteService();
            const request = {
                input: $event.target.value,
                componentRestrictions: {country: this.country},
                types: ['address']
            };
            autocompleteService.getPlacePredictions(request, this.displaySuggestions);
        }
        this.addressManual.emit($event.target.value);
    }

    displaySuggestions = (predictions, status) => {
        if (this.autocomplete) {
            const addressContainers = document.getElementsByClassName('pac-container');
            if (addressContainers) {
                for (let i = 0; i < addressContainers.length - 1; i++) {
                    document.body.removeChild(addressContainers.item(i));
                }
            }
        }
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
            if (status === 'ZERO_RESULTS') {
                this.show = true;
                this.searchElementRef.nativeElement.click();
            }
            return;
        }
        this.show = false;
    };

    initAutocomplete(maps: Maps) {
        if (this.country === undefined || this.country === null) {
            this.country = '';
        }

        if (this.autocomplete) {
            const addressContainers = document.getElementsByClassName('pac-container');
            if (addressContainers) {
                for (let i = 0; i < addressContainers.length; i++) {
                    document.body.removeChild(addressContainers.item(i));
                }
            }
        }

        const options = {
            componentRestrictions: {country: this.country},
        };

        this.autocomplete = new maps.places.Autocomplete(this.searchElementRef.nativeElement, options);

        if (this.containerClass) {
            const container = document.querySelector('.' + this.containerClass) as HTMLDivElement;

            if (container) {
                container.onscroll = () => {
                    const rect = this.searchElementRef.nativeElement.getBoundingClientRect();
                    const left = rect.left;
                    const top = rect.top + 20;
                    const addressContainers = document.getElementsByClassName('pac-container');
                    if (addressContainers && addressContainers.length > 0) {
                        const gContainer = addressContainers[addressContainers.length - 1] as HTMLDivElement;
                        gContainer.style.left = left + 'px';
                        gContainer.style.top = top + 'px';
                    }
                };
            }
        }

        this.autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
                this.onPlaceChange(this.autocomplete.getPlace());
            });
        });
    }

    onPlaceChange(place: google.maps.places.PlaceResult) {
        const location = this.locationFromPlace(place);
        this.entries.unshift({place, location});
        this.address.emit(location);
    }

    public locationFromPlace(place: google.maps.places.PlaceResult) {
        const components = place.address_components;

        if (!components) {
            return null;
        }

        const areaLevel3 = getShort(components, 'administrative_area_level_3');
        const locality = getLong(components, 'locality');
        const areaLevel2 = getLong(components, 'administrative_area_level_2');
        const colloquialArea = getLong(components, 'colloquial_area');

        const cityName = locality || areaLevel3 || areaLevel2 || colloquialArea;
        const countryName = getLong(components, 'country');
        const countryCode = getShort(components, 'country');
        const stateCode = getShort(components, 'administrative_area_level_1');
        const stateName = getLong(components, 'administrative_area_level_1');
        const name = place.name !== cityName ? place.name : null;

        const postCodeShort = getShort(components, 'postal_code');
        const postCodeLong = getLong(components, 'postal_code');
        const postCodeName = postCodeShort || postCodeLong;


        // postal_code
        const coordinates = {
            latitude: place.geometry.location.lat(),
            longitude: place.geometry.location.lng(),
        };
        const bounds = place.geometry.viewport.toJSON();
        const formattedAddress = place.formatted_address;

        return {
            name,
            cityName,
            countryName,
            countryCode,
            stateCode,
            stateName,
            postCodeName,
            bounds,
            coordinates,
            formattedAddress
        };
    }
}

function getComponent(components: Components, name: string) {
    return components.filter((component) => component.types[0] === name)[0];
}

function getLong(components: Components, name: string) {
    const component = getComponent(components, name);
    // console.log("Long",component)

    return component && component.long_name;
}

function getShort(components: Components, name: string) {
    const component = getComponent(components, name);
    // console.log("Short",component)
    return component && component.short_name;
}
