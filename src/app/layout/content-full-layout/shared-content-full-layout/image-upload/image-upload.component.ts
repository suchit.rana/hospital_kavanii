import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ImageSnippet} from 'src/app/models/app.misc';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent implements OnInit {
  @Input() format: string = 'Image';
  @Input() imgPlaceholder: string = './assets/placeholder-image.png';
  @Input() width: string = '100%';
  @Input() height: string = '350px';
  show: boolean = true;
  private _iconClick: boolean = false;

  @Output()
  validationError: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  fileSelectionChange: EventEmitter<ImageSnippet> = new EventEmitter<ImageSnippet>();

  @ViewChild('container', {static: false}) container: ElementRef;
  error: boolean = false;

  selectedFile?: ImageSnippet;

  file: File;
  private validTypes = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/webp'];

  @ViewChild('extra', {static: false}) extra: ElementRef;
  @ViewChild('imageInput', {static: false}) imageInput: ElementRef;

  @Input()
  get iconClick(): boolean {
    return this._iconClick;
  }

  set iconClick(value: boolean) {
    this._iconClick = value;
    if (this._iconClick === true) {
      // this.processFile()
    }
  }

  private _sizeExceeded = false;

  constructor() {
  }

  ngOnInit(): void {
    if (this.format == 'Signature') {
      this.height = '90px';
      this.validTypes.splice(this.validTypes.indexOf('image/gif'), 1);
    }
  }

  processFile(imageInput: any) {
    this.file = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      if (this.validTypes.indexOf(this.file.type) > -1) {
        if (this.file.size / (1024 * 1024) > 5) {
          this._sizeExceeded = true;
          this.error = false;
          return;
        }
        this._sizeExceeded = false;
        this.error = false;
        this.selectedFile = new ImageSnippet(event.target.result, this.file);
        const tm = setTimeout(() => {
          this.extra.nativeElement.click();
          clearTimeout(tm);
        });
        this.fileSelectionChange.emit(this.selectedFile);
        this.show = false;
      } else {
        this.error = true;
        this._sizeExceeded = false;
        this.validationError.emit(true);
        this.fileSelectionChange.emit(null);
      }
    });
    reader.readAsDataURL(this.file);
  }

  public clearSelection() {
    this.imageInput.nativeElement.value = null;
    this.selectedFile = null;
    this.show = true;


    this.fileSelectionChange.emit(null);
  }

  get getSelectedImage() {
    return this.selectedFile ? `url(${this.selectedFile.src})` : this.format == 'Signature' ? '#f1f2f4' : `url(${this.imgPlaceholder})`;
  }

  get sizeError(): boolean {
    return this._sizeExceeded;
  }

  get widthMain() {
    return this.container ? this.container.nativeElement.offsetWidth + 'px' : this.width;
  }

  get showUploadIcon() {
    return this.show && (!this.selectedFile || !this.selectedFile.src);
  }

  get accept() {
    return this.validTypes.join(",")
  }
}
