import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageUploadComponent} from './image-upload/image-upload.component';
import {GridNoDataComponent} from './grid-no-data/grid-no-data.component';
import {RouterModule} from '@angular/router';
import {AddNewDropdownComponent} from './add-new-dropdown/add-new-dropdown.component';
import {DropDownListModule} from '@progress/kendo-angular-dropdowns';
import {BaseGridComponent} from './base-grid/base-grid.component';
import {GoogleAddressComponent} from './google-address/google-address.component';
import {SharedModuleModule} from '../../../shared/shared-module.module';
import {PopupModule} from '@progress/kendo-angular-popup';
import {DropdownSearchComponent} from './dropdown-search/dropdown-search.component';
import {DeleteConfirmationDialogComponent} from './dilaog/delete-confirmation-dialog/delete-confirmation-dialog.component';
import {DateFilterComponent} from './dilaog/date-filter/date-filter.component';
import {AppCalenderPopupModule} from '../common/calender-popup/app-calender-popup.module';
import {AppDatePickerModule} from '../common/app-date-picker/app-date-picker.module';
import {FilterListSelectionComponent} from './dilaog/filter-list-selection/filter-list-selection.component';

@NgModule({
    declarations: [
        ImageUploadComponent,
        GridNoDataComponent,
        AddNewDropdownComponent,
        BaseGridComponent,
        GoogleAddressComponent,
        DropdownSearchComponent,
        DeleteConfirmationDialogComponent,
        DateFilterComponent,
        FilterListSelectionComponent
    ],
    imports: [CommonModule, RouterModule, SharedModuleModule, DropDownListModule, PopupModule, AppCalenderPopupModule, AppDatePickerModule],
    exports: [
        RouterModule,
        SharedModuleModule,
        ImageUploadComponent,
        GridNoDataComponent,
        AddNewDropdownComponent,
        BaseGridComponent,
        GoogleAddressComponent,
        DropDownListModule,
        DropdownSearchComponent,
    ],
    entryComponents: [DeleteConfirmationDialogComponent, DateFilterComponent, FilterListSelectionComponent]
})
export class SharedContentFullLayoutModule {
}
