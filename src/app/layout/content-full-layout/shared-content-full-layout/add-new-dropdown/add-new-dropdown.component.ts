import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {AppState} from 'src/app/app.state';
import {ApplicationDataLocation, ApplicationDataModel,} from 'src/app/models/app.settings.model';
import {ApplicationDataService} from 'src/app/services/app.applicationdata.service';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {PreventableEvent} from '@progress/kendo-angular-grid';
import {ApplicationDataEnum} from '../../../../enum/application-data-enum';

@Component({
  selector: 'app-add-new-dropdown',
  templateUrl: './add-new-dropdown.component.html',
  styleUrls: ['./add-new-dropdown.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{provide: MatFormFieldControl, useExisting: AddNewDropdownComponent}],
})
export class AddNewDropdownComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, OnDestroy {
  appFilterData: ApplicationDataModel[] = [];
  public filter: string;
  @Input() appData: ApplicationDataModel[];
  @Input() valueField: string = 'categoryName';
  @Input() textField: string = 'categoryName';
  @Input() categoryId: number;
  @Input() selectedId: number;
  @Output() appId: EventEmitter<number> = new EventEmitter<number>();
  @Input() isDisabled: boolean = false;
  @Input() isFetchFromAPI: boolean = true;
  @Input() isRestrictSpecCharNum: boolean = false;
  _value = this.selectedId;

  valuePrimitive = false;
  static nextId = 0;
  focused = false;
  controlType = 'app-auto-complete-with-new';
  id = `app-auto-complete-with-new${AddNewDropdownComponent.nextId++}`;
  _class = `app-auto-complete-with-new${AddNewDropdownComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();
  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private appState: AppState,
    private applicationDataService: ApplicationDataService,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    const obj = new ApplicationDataModel();
    obj.id = -1;
    obj[this.textField] = this.value;
    this.appFilterData.push(obj);
    this.appData = this.appFilterData;
  }

  ngOnInit() {
    if (this.categoryId == ApplicationDataEnum.Title) {
      this.isRestrictSpecCharNum = true;
    }
    if (this.isFetchFromAPI) {
      this.applicationDataService
        .GetApplicationDataByCategoryIdAndLocationId(
          this.categoryId,
          this.appState.selectedUserLocationId
        )
        .subscribe((data) => {
          this.addDummyIfNotPresent(data);
          this.appData = this.appFilterData = data;
        });
    } else {
      this.addDummyIfNotPresent(this.appData);
      this.appFilterData = this.appData;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('appData')) {
      this.addDummyIfNotPresent(this.appData);
      this.appFilterData = this.appData;
      this.value = this.selectedId;
      this.valuePrimitive = this.appFilterData && this.appFilterData.length > 0;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value ? value : null;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  changeVal(val: number) {
    this.markAsTouched();
    if (!this.disabled) {
      this.value = val;
      this.onChange(this.value);
      this.handleChange(val);
    }
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.touched && this.ngControl.invalid;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(date: any | null) {
    this._value = date ? date : null;
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    // this.dateBox.instance.open();
  }

  handleFilter(value: string) {
    if (!value) {
      this.appFilterData = this.appData;
      this.filter = value;
      return;
    }
    this.appFilterData = this.appData.filter((s) => value && s[this.valueField].toLowerCase().indexOf(value.toLowerCase().trim()) !== -1);
    this.filter = value;
  }


  handleChange(id: number) {
    this.appId.emit(id);
  }

  public addNew(): void {
    const adm = new ApplicationDataModel();
    adm.categoryId = this.categoryId;
    adm.categoryName = this.filter;
    adm.status = true;

    const adlm = new ApplicationDataLocation();
    adlm.locationId = this.appState.selectedUserLocation.id;
    adlm.locationName = this.appState.selectedUserLocation.locationName;
    adm.applicationDataLocation.push(adlm);

    this.applicationDataService.createApplicationData(adm).subscribe((id) => {
      adm.id = id;
      adm.categoryName = this.filter;
      this.appData.push(adm);
      this.appFilterData.push(adm);
      this.handleFilter(this.filter);
    });
  }

  private addDummyIfNotPresent(data: any[]) {
    if (!this.value) {
      return;
    }
    let isValFound = false;
    data.forEach(value1 => {
      if (value1[this.valueField] == this.value) {
        isValFound = true;
      }
    });
    if (!isValFound) {
      const obj = {id: -1};
      obj[this.textField] = this.value;
      data.push(obj);
    }
  }

  onOpenList($event: PreventableEvent) {
    const maxLen = this.getMaxLength();
    setTimeout(() => {
      const input: HTMLInputElement = document.querySelector('.app-add-new-dropdown-popup .k-textbox');
      if (input) {
        input.addEventListener('input', ev => {
          const length = ev.target['value'] ? ev.target['value'].length : 0;
          if (maxLen != null && length >= maxLen) {
            const matError = document.createElement('mat-error');
            matError.innerText = 'Exceeds character limit of ' + maxLen + '.';
            matError.classList.add('mat-error');
            matError.id = this.id + '_exceed_char_limit';
            matError.style.paddingLeft = '10px';
            input.value = ev.target['value'].substr(0, maxLen);
            this.filter = input.value;
            input.parentElement.after(matError);
            ev.preventDefault();
          }
        });

        input.addEventListener('keypress', ev => {
          const length = ev.target['value'] ? ev.target['value'].length : 0;
          if (maxLen != null && length >= maxLen) {
            const matError = document.createElement('mat-error');
            matError.innerText = 'Exceeds character limit of ' + maxLen + '.';
            matError.classList.add('mat-error');
            matError.id = this.id + '_exceed_char_limit';
            matError.style.paddingLeft = '10px';
            input.parentElement.after(matError);
            ev.preventDefault();
          }
        });

        input.addEventListener('keydown', ev => {
          const matErrorOld = document.getElementById(this.id + '_exceed_char_limit');
          if (matErrorOld) {
            input.parentElement.parentElement.removeChild(matErrorOld);
          }

          if (this.isRestrictSpecCharNum && !/^[a-zA-Z\s'-]*$/.test(ev.key)) {
            ev.preventDefault();
          }
        });
      }
    });
  }

  private getMaxLength() {
    switch (this.categoryId) {
      case ApplicationDataEnum.Title:
        return 15;
      case ApplicationDataEnum.ContactCategory:
        return 30;
    }
    return 100;
  }
}
