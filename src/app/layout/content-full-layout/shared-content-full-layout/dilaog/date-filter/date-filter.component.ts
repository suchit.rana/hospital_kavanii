import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {SAVE} from '../../../../../shared/popup-component/popup-component.component';

@Component({
    selector: 'app-date-filter',
    templateUrl: './date-filter.component.html',
    styleUrls: ['./date-filter.component.css']
})
export class DateFilterComponent implements OnInit {

    fromDateCtrl = new FormControl();
    toDateCtrl = new FormControl();

    private formGroup = new FormGroup({
        fromDate: this.fromDateCtrl,
        toDate: this.toDateCtrl
    });

    @Input()
    fromDate: Date;
    @Input()
    toDate: Date;

    private static ref: IPopupAction;

    constructor() {
    }

    ngOnInit(): void {
        if (DateFilterComponent.ref) {
            DateFilterComponent.ref.clickSave.subscribe(() => {
                DateFilterComponent.ref.closeDialog({action: SAVE, data: this.formGroup.value});
            });
        }
    }

    static open(appDialogService: AppDialogService, data: { [x: string]: any } = {}): IPopupAction {
        this.ref = appDialogService.open(DateFilterComponent, {
                title: 'Filter BY DATE RANGE',
                width: '420px',
                saveBtnTitle: 'Apply',
                data: data
            }
        );
        return this.ref;
    }

}
