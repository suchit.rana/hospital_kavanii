import {Component, Input, OnInit} from '@angular/core';
import {AppDialogService, IPopupAction, PopupSetting} from '../../../../../shared/services/app-dialog.service';

@Component({
    selector: 'app-delete-confirmation-dialog',
    templateUrl: './delete-confirmation-dialog.component.html',
    styleUrls: ['./delete-confirmation-dialog.component.css']
})
export class DeleteConfirmationDialogComponent implements OnInit {

    @Input()
    msg: string;

    constructor() {
    }

    ngOnInit(): void {
    }

    static open(dialogService: AppDialogService, msg: string, options?: PopupSetting): IPopupAction {
        options = {
            ...{
                title: 'Do you want to delete this?',
                width: '738px',
                height: '295px',
                saveBtnTitle: 'Yes',
                cancelBtnTitle: 'No',
                bodyPadding: true,
                data: {msg}
            }, ...options
        };
        return dialogService.open(DeleteConfirmationDialogComponent, options);
    }

}
