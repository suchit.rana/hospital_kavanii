import {Component, Input, OnInit} from '@angular/core';
import {AppDialogService, IPopupAction} from '../../../../../shared/services/app-dialog.service';
import {PractitionerSpecialityModel} from '../../../../../models/app.staff.model';
import {SAVE} from '../../../../../shared/popup-component/popup-component.component';
import {MatCheckboxChange} from '@angular/material';
import {TextValue} from '../../../../../shared/interface';

@Component({
    selector: 'app-date-filter',
    templateUrl: './filter-list-selection.component.html',
    styleUrls: ['./filter-list-selection.component.scss']
})
export class FilterListSelectionComponent implements OnInit {

    static ref: IPopupAction;

    @Input()
    list: any[] = [];
    @Input()
    valueField: string = 'id';
    @Input()
    textField: string = 'text';
    @Input()
    titleFunction: (args: any) => any;

    selectedListId: string[] = [];
    titleField: string = this.textField;

    constructor() {
    }

    ngOnInit() {
        this.titleField = this.textField;
        if (this.titleFunction) {
            this.titleField = '_' + this.textField;
            this.list.map(l => {
                l[this.titleField] = this.titleFunction(l);
            });
        }
        if (this.ref) {
            this.ref.clickSave.subscribe(() => {
                this.ref.closeDialog({action: SAVE, data: this.selectedListId});
            });
        }
    }

    ngOnDestroy(): void {
        FilterListSelectionComponent.ref = null;
    }

    toggleListItem($event: MatCheckboxChange) {
        const value = $event.source.value;
        if ($event.checked && this.selectedListId.indexOf(value) < 0) {
            this.selectedListId.push(value);
        } else {
            this.selectedListId.splice(this.selectedListId.indexOf(value), 1);
        }
    }

    toggleAllList($event: MatCheckboxChange) {
        if ($event.checked) {
            this.list.forEach(list => {
                if (this.selectedListId.indexOf(list[this.valueField]) < 0) {
                    this.selectedListId.push(list[this.valueField]);
                }
            });
        } else {
            this.selectedListId = [];
        }
    }

    get ref(): IPopupAction {
        return FilterListSelectionComponent.ref;
    }

    static open(dialogService: AppDialogService, list: any[], data: { [x: string]: any } = {}): IPopupAction {
        this.ref = dialogService.open(FilterListSelectionComponent, {
            title: 'Filter By Practitioner',
            width: '400px',
            height: '520px',
            saveBtnTitle: 'Apply',
            hideOnSave: false,
            data: {
                ...data,
                list: list
            }
        });
        return this.ref;
    }

}
