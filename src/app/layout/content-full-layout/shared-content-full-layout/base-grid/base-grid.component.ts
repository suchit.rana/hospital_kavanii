import {Component, OnInit} from '@angular/core';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {CompositeFilterDescriptor, GroupDescriptor, process, SortDescriptor, State} from '@progress/kendo-data-query';
import {RouterState} from '../../../../shared/interface';
import {Router} from '@angular/router';
import {hasPermission} from '../../../../app.component';
import {AppState} from '../../../../app.state';

@Component({
    selector: 'app-base-grid',
    template: `
        <p>
            base-grid works!
        </p>
    `,
    styleUrls: ['./base-grid.component.css']
})
export class BaseGridComponent implements OnInit {
    readonly _any = ['C', 'V', 'E', 'D'];
    public permission: string[];
    moduleName: string;
    routerState: RouterState;

    constructor(protected router?: Router, protected appState?: AppState) {
        if (this.router && this.router.getCurrentNavigation() != null) {
            this.routerState = this.router.getCurrentNavigation().extras.state as RouterState;
        } else {
            this.routerState = history.state;
        }
    }

    get haveDeletePermission(): boolean {
        return this.permission.indexOf('D') >= 0;
    }

    get haveViewPermission(): boolean {
        return this.permission.indexOf('V') >= 0;
    }

    get haveEditPermission(): boolean {
        return this.permission.indexOf('E') >= 0;
    }

    public gridData: any = [];
    public gridView: GridDataResult;
    public pageSize = 10;
    public skip = 0;
    // public locationData: any[];
    public multiple = false;
    public allowUnsort = true;
    public reorder = true;
    public hasItems = false;
    public sort: SortDescriptor[] = [];
    public groups: GroupDescriptor[] = [];

    activeFilters: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [{field: 'isStatus', operator: 'eq', value: true}]
    };

    inactiveFilters: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [{field: 'isStatus', operator: 'eq', value: false}]
    };


    // For Status
    activeFilterstatus: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [{field: 'status', operator: 'eq', value: true}]
    };

    inactiveFilterstatus: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [{field: 'status', operator: 'eq', value: false}]
    };

    // For case
    activeFilterstatuscase: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [
            {logic: 'or', filters: [{field: 'caseStatus', operator: 'eq', value: 0}, {field: 'caseStatus', operator: 'eq', value: 2}]}]
    };

    inactiveFilterstatuscase: CompositeFilterDescriptor = {
        logic: 'and',
        filters: [{field: 'caseStatus', operator: 'eq', value: 1}]
    };
    public state: State = {
        skip: this.skip,
        take: this.pageSize,
        sort: this.sort,
        filter: this.activeFilters
    };

    public statestatus: State = {
        skip: this.skip,
        take: this.pageSize,
        sort: this.sort,
        filter: this.activeFilterstatus
    };
    public stateCase: State = {
        skip: this.skip,
        take: this.pageSize,
        sort: this.sort,
        filter: this.activeFilterstatuscase
    };

    removeFilters() {
        this.state.filter = null;
    }

    removeFilterstatus() {
        this.statestatus.filter = null;
    }

    removeFilterCase() {
        this.stateCase.filter = null;
    }

    ngOnInit() {
// const
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.state.sort = sort;
        this.loadItems();
    }

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.state.skip = event.skip;
        this.loadItems();
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.state.group = groups;
        this.loadItems();
    }

    public setActiveFilter() {
        this.state.filter = this.activeFilters;
    }

    public setInactiveFilter() {
        this.state.filter = this.inactiveFilters;
    }


    public loadItems(fieldName?: string, dir: 'asc' | 'desc' = 'asc'): void {
        // console.log(this.gridData)
        if (fieldName) {
            this.sort = [{dir: dir, field: fieldName}];
            this.state.sort = this.sort;
        }
        this.hasItems = this.gridData && this.gridData.length > 0;
        this.gridView = process(this.gridData, this.state);
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        console.log(state);
        this.loadItems();
    }


    //for case
    public sortChangeCase(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.stateCase.sort = sort;
        this.loadCaseItems();
    }

    public pageChangeCase(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.stateCase.skip = event.skip;
        this.loadCaseItems();
    }

    public groupChangeCase(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.stateCase.group = groups;
        this.loadCaseItems();
    }

    public setActiveFilterCase() {
        this.stateCase.filter = this.activeFilterstatuscase;
    }

    public setInactiveFilterCase() {
        this.stateCase.filter = this.inactiveFilterstatuscase;
    }

    public loadCaseItems(): void {

        this.hasItems = this.gridData && this.gridData.length > 0;
        this.gridView = null;
        this.gridView = process(this.gridData, this.stateCase);

    }

    public dataStateChangeCase(state: DataStateChangeEvent): void {
        this.stateCase = state;
        this.loadCaseItems();
    }

    // Status Sorting
    public sortChangestatus(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.statestatus.sort = sort;
        this.loadItemstatus();
    }

    public pageChangestatus(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.statestatus.skip = event.skip;
        this.loadItemstatus();
    }

    public groupChangestatus(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.statestatus.group = groups;
        this.loadItemstatus();
    }

    public setActiveFilterstatus() {
        this.statestatus.filter = this.activeFilterstatus;
    }

    public setInactiveFilterstatus() {
        this.statestatus.filter = this.inactiveFilterstatus;
    }

    public loadItemstatus(fieldName?: string, dir: 'asc' | 'desc' = 'asc'): void {
        // console.log(this.gridData)
        if (fieldName) {
            this.sort = [{dir: dir, field: fieldName}];
            this.state.sort = this.sort;
        }
        this.hasItems = this.gridData && this.gridData.length > 0;
        this.gridView = process(this.gridData, this.statestatus);
    }

    public dataStateChangestatus(state: DataStateChangeEvent): void {
        this.statestatus = state;
        this.loadItemstatus();
    }

    isFromState(): boolean {
        return this.routerState && this.routerState.fromState;
    }

    hasPermission(moduleName: string, permission: string | string[]) {
        return hasPermission(this.appState, moduleName, permission);
    }
}
