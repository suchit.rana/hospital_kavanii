import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {AppState} from 'src/app/app.state';
import {ApplicationDataModel,} from 'src/app/models/app.settings.model';
import {ApplicationDataService} from 'src/app/services/app.applicationdata.service';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';

@Component({
  selector: 'app-dropdown-search',
  templateUrl: './dropdown-search.component.html',
  styleUrls: ['./dropdown-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{provide: MatFormFieldControl, useExisting: DropdownSearchComponent}],
})
export class DropdownSearchComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, OnDestroy {
  appFilterData: any[] = [];
  public filter: string;
  @Input('data') appData: any[];
  @Input() valueField: string = 'value';
  @Input() textField: string = 'text';
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() isDisabled: boolean = false;
  @Input() filterable: boolean = true;
  _value = null;
  valuePrimitive = false;
  static nextId = 0;
  focused = false;
  controlType = 'app-auto-complete-';
  id = `app-auto-complete-${DropdownSearchComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();
  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private appState: AppState,
    private applicationDataService: ApplicationDataService,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    const obj = new ApplicationDataModel();
    obj.id = -1;
    obj[this.textField] = this.value;
    this.appFilterData.push(obj);
    this.appData = this.appFilterData;
  }

  ngOnInit() {
    this.addDummyIfNotPresent(this.appData);
    this.appFilterData = this.appData;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('appData')) {
      this.addDummyIfNotPresent(this.appData);
      this.appFilterData = this.appData;
      this.value = this._value;
      this.valuePrimitive = this.appFilterData && this.appFilterData.length > 0;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value ? value : null;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  changeVal(val: number) {
    this.markAsTouched();
    if (!this.disabled) {
      this.value = val;
      this.onChange(this.value);
      this.handleChange(val);
    }
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.touched && this.ngControl.invalid;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(data: any | null) {
    this._value = data;
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    // this.dateBox.instance.open();
  }

  handleFilter(value: string) {
    if (!value) {
      this.appFilterData = this.appData;
      this.filter = value;
      return;
    }
    this.appFilterData = this.appData.filter((s) => value && s[this.textField].toLowerCase().indexOf(value.toLowerCase().trim()) !== -1);
    this.filter = value;
  }


  handleChange(id: number) {
    this.valueChange.emit(id);
  }

  private addDummyIfNotPresent(data: any[]) {
    if (!this.value || !data || data.length === 0) {
      return;
    }
    let isValFound = false;
    data.forEach(value1 => {
      if (value1[this.valueField] == this.value) {
        isValFound = true;
      }
    });
    if (!isValFound) {
      const obj = {id: -1};
      obj[this.textField] = this.value;
      data.push(obj);
    }
  }
}
