import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddressFieldComponent} from './address-field.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {SharedContentFullLayoutModule} from '../shared-content-full-layout.module';

@NgModule({
  declarations: [
    AddressFieldComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedContentFullLayoutModule
  ],
  exports: [
    AddressFieldComponent
  ]
})
export class AddressFieldModule {
}
