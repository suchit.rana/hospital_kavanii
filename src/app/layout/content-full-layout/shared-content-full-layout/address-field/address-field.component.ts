import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatFormFieldAppearance} from '@angular/material/form-field/typings/form-field';
import {BusinessService} from '../../../../services/app.business.service';
import {AppState} from '../../../../app.state';
import {MiscService} from '../../../../services/app.misc.service';

@Component({
  selector: 'app-address-field',
  templateUrl: './address-field.component.html',
  styleUrls: ['./address-field.component.css'],
  providers: [MiscService]
})
export class AddressFieldComponent implements OnInit {

  @Input()
  addressForm: FormGroup;
  @Input()
  hasAddress2: boolean = true;
  @Input()
  containerClass: string;
  @Input()
  addressFieldName: string = 'address';
  @Input()
  address2FieldName: string = 'address2';
  @Input()
  countryFieldName: string = 'country';
  @Input()
  stateFieldName: string = 'state';
  @Input()
  cityFieldName: string = 'city';
  @Input()
  postCodeFieldName: string = 'postCode';

  appearance: MatFormFieldAppearance = 'outline';

  country: string;
  addressManual: any;
  locationList: any[];
  countries: any[];
  mainCountryList: any = [];
  disabledInput: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private businessService: BusinessService,
    private miscService: MiscService,
    private appState: AppState,
  ) {
  }

  ngOnInit() {
    this.getUserLocations();
  }


  addressHandler($event) {
    this.addressManual = $event.name;
    if ($event) {
      this.addressForm.get(this.addressFieldName).patchValue($event.formattedAddress);
      this.addressForm.get(this.stateFieldName).patchValue($event.stateName);
      this.addressForm.get(this.cityFieldName).patchValue($event.cityName);
      this.addressForm.get(this.postCodeFieldName).patchValue($event.postCodeName);

      this.disabledInput = true;
    }
  }

  addressManualHandler($event) {
    this.addressManual = $event;
    if (!$event) {
      this.addressManual = null;
      this.getUserLocations();
      this.addressForm.get(this.addressFieldName).patchValue('');
      this.addressForm.get(this.stateFieldName).patchValue('');
      this.addressForm.get(this.cityFieldName).patchValue('');
      this.addressForm.get(this.postCodeFieldName).patchValue('');

      this.disabledInput = false;
    }
  }

  locationValidation($event) {
    const inputVal = $event.keyCode;
    if (!((inputVal >= 65 && inputVal <= 90) || (inputVal >= 97 && inputVal <= 122))) {
      $event.preventDefault();
    }
  }


  getUserLocations() {
    this.businessService.getLocationsForCurrentUser().subscribe((locations) => {
      this.locationList = locations ? locations : [];
      this.miscService.getCountries().subscribe((countries) => {

        this.countries = countries;
        this.mainCountryList = countries;

        //Setting Default Country as Autralia For a Very First Time User
       //(Don't change it Address Field will not work)
        let countryName = 'Australia';
        if (this.appState.selectedUserLocation) {
          countryName = this.locationList.find((l) => l.locationName === this.appState.selectedUserLocation.locationName).country;
        }

        if (countryName) {
          this.country = countries.find((x) => x.country_Name === countryName).country_Code;
          this.addressForm.get(this.countryFieldName).patchValue(countryName);
          this.checkCountryName();
        }
      });
    });
  }

  private checkCountryName() {
    this.addressForm.get(this.countryFieldName).valueChanges.subscribe(countryName => {
      const country = this.countries.find((x) => x.country_Name.toLowerCase() === countryName.toLowerCase());
      if (country) {
        this.country = country.country_Code;
        this.addressForm.get(this.countryFieldName).setErrors(null);
      } else {
        this.addressForm.get(this.countryFieldName).setErrors({invalid: true});
      }
    });
  }
}
