import {Component, OnDestroy, OnInit} from '@angular/core';
import {onMainContentChange} from './animations/animations';

import {AppState} from './app.state';
import {combineLatest, Observable} from 'rxjs';
import {debounceTime, map, startWith} from 'rxjs/operators';


// if (environment.production) {
//   LogRocket.init('lkgehd/test', {
//     console: {
//       isEnabled: {
//         log: false,
//         info: false,
//         debug: false,
//         warn: true,
//         error: true
//       },
//     },
//   });
// }

export const DummyGUID = '00000000-0000-0000-0000-000000000000';
export const Success = 'Success';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [onMainContentChange]
})
export class AppComponent implements OnDestroy, OnInit {
  async ngOnInit() {
    await this.appState.fetchRolesAndPermission();
  }

  // @ViewChild("drawer", { read: true, static: true }) drawer : MatDrawer;
  // public onSideNavChange: boolean;
  // title = 'kavaniiUI';
  // mobileQuery: MediaQueryList;
  // isResponsive = false;
  // private _mobileQueryListener: () => void;
  constructor(
              public appState: AppState,
    ) {
    // this.mobileQuery = media.matchMedia('(max-width: 992px)');
    // this._mobileQueryListener = () => {
    //   changeDetectorRef.detectChanges();
    //   this.isResponsive = true;
    // }
    // this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngOnDestroy(): void {
    // this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  // closeDrawer() {
  //   this.drawer.opened = false;
  // }
  //
  // logout() {
  //   this.appState.resetState();
  //   this.authService.logout();
  //   this.router.navigate(["login"]);
  // }
}

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

document.addEventListener("wheel", function(event){
  if(document.activeElement['type'] === "number"){
    (document.activeElement as HTMLElement).blur();
  }
});

export const formatREString = (cRe: string, re: string): string => {
  return cRe && cRe != '' ? cRe.concat(','.concat(re)) : re;
};

export const hasPermission = (appState: AppState, moduleName: string, permission: string | string[]): boolean => {
    return appState.haveAccessToModule(moduleName, permission)
}

declare global {
  interface Array<T> {
    insert(index: number, ...args: any): Array<T>;
  }

  interface String {
    toSlug(): string;
  }
}

Array.prototype.insert = function(index: number) {
  index = Math.min(index, this.length);
  arguments.length > 1
  && this.splice.apply(this, [index, 0].concat([].pop.call(arguments)))
  && this.insert.apply(this, arguments);
  return this;
};

String.prototype.toSlug = function() {
  let str = this;

  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '_') // collapse whitespace and replace by -
    .replace(/-+/g, '_'); // collapse dashes
  return str;
};
