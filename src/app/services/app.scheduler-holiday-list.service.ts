import {Injectable} from '@angular/core';
import {BaseService} from './app.base.service';
import {SchedulerSettings} from '../models/scheduler-settings';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../app.state';
import {HolidayUtils} from '../shared/holiday-utils';


@Injectable({
  providedIn: 'root'
})
export class AppSchedulerHolidayListService extends BaseService {

  constructor(public http: HttpClient, private appState: AppState) {
    super(http);
  }

  getHolidayList() {
    const countryName = HolidayUtils.getCalenderApiKey(this.appState.selectedUserLocation.country);
    return this.http.get<any>(`https://www.googleapis.com/calendar/v3/calendars/en.${countryName}%23holiday@group.v.calendar.google.com/events?key=${this.environmentSettings.googleApi}`);
  }

}
