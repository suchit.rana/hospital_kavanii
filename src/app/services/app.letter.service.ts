import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { PatientLetterModel } from '../models/app.letter.model';
import { BaseService } from './app.base.service';

@Injectable({
  providedIn: 'root'
})
export class LetterService extends BaseService{  

  getPatientLetter(locationId: string, patientId: string) {
    return this.http.get<any[]>(
      this.environmentSettings.apiBaseUrl + `/getpatientletter`,
      {params: {patientId: patientId, locationId:locationId}}
    );
  }

  getPatientLetterbyId(locationId: string, patientId: string, patientLetterId: string): Observable<PatientLetterModel>{
    return this.http.get<PatientLetterModel>(
      this.environmentSettings.apiBaseUrl + `/getpatientletterbyid`,
      {params: {patientId: patientId ,locationId: locationId, patientLetterId: patientLetterId}}
    );
  }

  createPatientLetter(patientLetterModel: PatientLetterModel) {
    return this.http.post(
      this.environmentSettings.apiBaseUrl + "/createpatientletter", patientLetterModel);
  }

  modifyPatientLetter(patientLetterModel: PatientLetterModel) {
    return this.http.put(
      this.environmentSettings.apiBaseUrl + "/modifypatientletter",patientLetterModel);
  }

  deletePatientLetterById(patientId: string, patientLetterId: string)
  {
    return this.http.delete<any[]>(
      this.environmentSettings.apiBaseUrl + `/deletePatientLetterById?patientId=${patientId}&&patientLetterId=${patientLetterId}`
    );
  }

  generatePatientLeters(param: any){
    return this.http.post(
      this.environmentSettings.apiBaseUrl + "/generatePatientLeters", param);
  }
}
