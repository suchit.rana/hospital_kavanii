import { Injectable } from "@angular/core";
import { BaseService } from "./app.base.service";
import {
  AddTreatmentNotesTemplateModel,
  TotalTreatmentNotesTemplateLibraryModel,
  TreatmentNotesModel,
  TreatmentNotesTemplateLibraryModel,
} from "../models/app.treatmentnotes.model";

@Injectable()
export class TreatmentNotesService extends BaseService {
  messageConfirmation: string;
  createTreatmentNotesTemplate(treatmentNotesModel: TreatmentNotesModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createtreatmentnotestemplate",
      treatmentNotesModel
    );
  }

  getTreatmentNotesTemplateById(treatmentNotesTemplateId: string) {
    return this.http.get<TreatmentNotesModel>(
      this.environmentSettings.apiBaseUrl +
        `/gettreatmentnotestemplatebyid?treatmentNotesTemplateId=${treatmentNotesTemplateId}`
    );
  }

  updateTreatmentNotesTemplate(treatmentNotesModel: TreatmentNotesModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/updatetreatmentnotestemplate",
      treatmentNotesModel
    );
  }

  getAllTreatmentNotesTemplate() {
    return this.http.get<TreatmentNotesModel[]>(
      this.environmentSettings.apiBaseUrl + "/getalltreatmentnotestemplates"
    );
  }

  getAllTreatmentNotesTemplateByLocationId(locationId: string) {
    return this.http.get<TreatmentNotesModel[]>(
      this.environmentSettings.apiBaseUrl +
        `/getalltreatmentnotestemplatesbylocationid?locationId=${locationId}`
    );
  }

  copyTreatmentNotesTemplate(treatmentNotesTemplateId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
        `/copytreatmentnotestemplate?treatmentNotesTemplateId=${treatmentNotesTemplateId}`
    );
  }

  shareTreatmentNotesTemplate(
    templateLibraryModel: TreatmentNotesTemplateLibraryModel
  ) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/sharetreatmentnotestemplate",
      templateLibraryModel
    );
  }

  deleteTreatmentNotesTemplate(treatmentNotesTemplateId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl +
        "/deletetreatmentnotestemplate?treatmentNotesTemplateId=" +
        treatmentNotesTemplateId
    );
  }

  getTotalTemplatesBySpeciality() {
    return this.http.get<TotalTreatmentNotesTemplateLibraryModel[]>(
      this.environmentSettings.apiBaseUrl + "/gettotaltemplatesbyspeciality"
    );
  }

  getAllTreatmentNotesTemplateLibraryBySpecialityId(specialityId: string) {
    return this.http.get<TreatmentNotesTemplateLibraryModel[]>(
      this.environmentSettings.apiBaseUrl +
        `/getalltreatmentnotestemplatelibrarybyspecialityid?specialityId=${specialityId}`
    );
  }

  addToTreatmentNotesTemplate(
    addTreatmentNotesTemplateModel: AddTreatmentNotesTemplateModel
  ) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/addtotreatmentnotestemplate",
      addTreatmentNotesTemplateModel
    );
  }

  getTreatmentNotesTemplateLibraryById(libraryTemplateId: string) {
    return this.http.get<TreatmentNotesTemplateLibraryModel>(
      this.environmentSettings.apiBaseUrl +
        `/gettreatmentnotestemplatelibrarybyid?libraryTemplateId=${libraryTemplateId}`
    );
  }
}
