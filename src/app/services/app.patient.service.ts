import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import {
  MergePatientsModel,
  PatientModel,
  PatientPersonalizeModel,
  DocumentListModel,
  PatientDocumentUpdateLockModel,
  PatientDocumentUpdateModel,
  PractitonerLocationSpecialityModel,
  PatientTreatmentNotesModel,
  PatientTreatmentDetailsModel,
  PatientTreatmentNotesDetailsFinalizeModel,
  PatientMinInfoModel,
  PatientCaseModel,
  CaseContactsModel
} from "../models/app.patient.model";
import { PatinetTreatmentNotesModel } from "../models/app.treatmentnotes.model";
import { BaseService } from "./app.base.service";

@Injectable()
export class PatientService extends BaseService {
  sharedData: string;
  onSuccess = new BehaviorSubject<string>("");
  onSuccess1 = new BehaviorSubject<string>("");
  private displayMessage = new Subject<any>();
  private sendData = new Subject<any>();

  getAllPatients() {
    return this.http.get<any[]>(
      this.environmentSettings.apiBaseUrl + "/getallpatients"
    );
  }

  getAllPatientsByLocationId(locationId: string) {
    return this.http.get<PatientModel[]>(
      this.environmentSettings.apiBaseUrl +
      `/getallpatientsbylocationid?locationId=${locationId}`
    );
  }

  getallpatientsmininfobylocationid(locationId: string) {
    return this.http.get<PatientMinInfoModel[]>(
      this.environmentSettings.apiBaseUrl +
      `/getallpatientsmininfobylocationid?locationId=${locationId}`
    );
  }

  getPatientById(patientId: string) {
    return this.http.get<PatientModel>(
      this.environmentSettings.apiBaseUrl +
      `/getpatientbyid?patientId=${patientId}`
    );
  }

  createPatient(patientModel: PatientModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createpatient",
      patientModel
    );
  }

  createPatientCases(patientCaseModel: PatientCaseModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createPatientCases",
      patientCaseModel
    );
  }

  modifyPatientCases(patientCaseModel: PatientCaseModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/modifyPatientCases",
      patientCaseModel
    );
  }

  getPatientCases(locationId: string, patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl + `/getapplicationdatacases?patientId=${patientId}&&locationId=${locationId}`
    );
  }

  mergePatient(mergeModel: MergePatientsModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/mergepatient",
      mergeModel
    );
  }

  updatePatient(patientModel: PatientModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/updatepatient",
      patientModel
    );
  }

  createPatientPersonalize(patientPersonalizeModel: PatientPersonalizeModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createpatientpersonalize",
      patientPersonalizeModel
    );
  }

  updatePatientPersonalize(patientPersonalizeModel: PatientPersonalizeModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/updatepatientpersonalize",
      patientPersonalizeModel
    );
  }

  getPersonalizeByTypeAndLocation(locationId: string, personalizeType: number) {
    return this.http.get<PatientPersonalizeModel>(
      this.environmentSettings.apiBaseUrl +
      `/getpersonalizebytypeandlocation?locationId=${locationId}&&personalizeType=${personalizeType}`
    );
  }

  getLastPatientIdByLocation(locationId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/getlastpatientidbylocation?locationId=${locationId}`
    );
  }

  getpatientdocuments(patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/getpatientdocuments?patientId=${patientId}`
    );
  }
  getpatientdocumentcontentbyid(patientId: string, documentId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/getpatientdocumentcontentbyid?patientId=${patientId}&documentId=${documentId}`
    );
  }
  deletepatientdocumentsbyid(patientId: string, documentId: string) {
    return this.http.get<DocumentListModel>(
      this.environmentSettings.apiBaseUrl +
      `/deletepatientdocumentsbyid?patientId=${patientId}&documentId=${documentId}`
    );
  }
  uploadpatientdocument(documentlistmodel: DocumentListModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/uploadpatientdocument",
      documentlistmodel
    );
  }
  updatepatientdocument(
    patientdocumentUpdateModel: PatientDocumentUpdateModel
  ) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/updatepatientdocument",
      patientdocumentUpdateModel
    );
  }
  updatepatientdocumentlockstatus(
    patientdocumentUpdateLockModel: PatientDocumentUpdateLockModel
  ) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/updatepatientdocumentlockstatus",
      patientdocumentUpdateLockModel
    );
  }
  openSuccess(msg) {
    this.onSuccess.next(msg);
  }
  openSuccess1(msg) {
    this.onSuccess1.next(msg);
  }

  deletePatient(patientId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl +
      "/deletepatient?patientId=" +
      patientId
    );
  }

  getLocationSpecialityByPractitonerId(practitonerId: string) {
    return this.http.get<PractitonerLocationSpecialityModel[]>(
      this.environmentSettings.apiBaseUrl +
      `/GetLocationSpecialityByPractitonerId?practitonerId=${practitonerId}`
    );
  }

  getEligibleTreatmentNotes(locationId: string, patientId: string) {
    return this.http.get<PatientTreatmentNotesModel>(
      this.environmentSettings.apiBaseUrl +
      `/geteligibletreatmentnotes?locationId=${locationId}&patientId=${patientId}`
    );
  }
  gettreatmentnotesdetailsById(patientId: string, treatmentnotesId: string) {
    return this.http.get<PatinetTreatmentNotesModel>(
      this.environmentSettings.apiBaseUrl +
      `/gettreatmentnotesdetailsById?patientId=${patientId}&treatmentnotesId=${treatmentnotesId}`
    );
  }
  gettreatmentnotesdetailsByArrayIds(obj: any) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/gettreatmentnotesdetailsListByIds",
      obj
    );
  }
  createPatientTreatmentNotes(patientTreatmentDetailsModel: PatientTreatmentDetailsModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createpatienttreatmentnotes",
      patientTreatmentDetailsModel
    );
  }
  updatePatientTreatmentNotes(patientTreatmentDetailsModel: PatientTreatmentDetailsModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/modifypatienttreatmentnotes",
      patientTreatmentDetailsModel
    );
  }
  deletepatienttreatmentnotesbyid(patientId: string, patientTreatmentNotesId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl +
      `/deletepatienttreatmentnotesbyid?patientId=${patientId}&patientTreatmentNotesId=${patientTreatmentNotesId}`
    );
  }
  getapplicationdatacases(locationId: string, patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/getapplicationdatacases?locationId=${locationId}&patientId=${patientId}&hideMerged=true`
    );
  }
  GetPatientMinInfoById(patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetPatientMinInfoById?patientId=${patientId}`
    );
  }
  GetMinInfoBusinessLocationById(Id: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetMinInfoBusinessLocationById/${Id}`
    );
  }
  modifypatienttreatmentnotesOnFinalize(patientTreatmentNotesDetailsFinalizeModel: PatientTreatmentNotesDetailsFinalizeModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/modifypatienttreatmentnotesOnFinalize",
      patientTreatmentNotesDetailsFinalizeModel
    );
  }

  getAllCasesByLocationAndPatient(locationId: string, patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetPatientCasesData?locationId=${locationId}&patientId=${patientId}`
    );
  }

  getCaseDetailbyCaseId(caseId: string, patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetCaseDatabyId?caseId=${caseId}&patientId=${patientId}`
    );
  }
  mergeCaseContact(obj) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/mergePatientCases", obj
    );
  }

  deleteReferralOrThirdParty(caseId: string, contactCaseId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl + `/DeleteCaseContacts?caseId=${caseId}&contactCaseId=${contactCaseId}`
    );
  }

  DeletePatientCasesData(patientId: string, caseId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl +
      `/DeletePatientCasesData?patientId=${patientId}&casesId=${caseId}`
    );
  }

  createPatientTreatmentNotes1(patientTreatmentDetailsModel: any) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + "/createpatienttreatmentnotes",
      patientTreatmentDetailsModel
    );
  }

  publishSomeData(data: any) {
    this.displayMessage.next(data);
  }

  getObservable(): Subject<any> {
    return this.displayMessage;
  }

  getDataforReferralOrThirdParty(data: any) {
    alert(3)
    this.sendData.next(data);
  }

  getReferralOrThirdParty(): Subject<any> {
    return this.sendData;
  }

}
