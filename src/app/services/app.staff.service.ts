import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {BaseService} from './app.base.service';
import {GroupSpecialityPractitonerModel, PractitionerModel, PractitionerSpecialityModel, StaffModel} from '../models/app.staff.model';


@Injectable()
export class StaffService extends BaseService {
  private displayMessage = new Subject<any>();

  addStaff(staff: any) {
    return this.http.post<any>(this.environmentSettings.apiBaseUrl + '/AddStaff', staff);
  }

  updateStaff(staff: StaffModel) {
    return this.http.post<any>(this.environmentSettings.apiBaseUrl + '/UpdateStaff', staff);
  }

  getStaffById(id: string) {
    return this.http.get<StaffModel>(this.environmentSettings.apiBaseUrl + `/GetStaffById/${id}`);
  }

  getStaffs() {
    return this.http.get<StaffModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllStaffs');
  }

  getAllStaffsList() {
    return this.http.get<StaffModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllStaffsList');
  }

  getPractitioners(isStatus?: boolean) {
    let options = {};
    if (typeof isStatus != 'undefined') {
      options = {params: {isStatus: isStatus}};
    }
    return this.http.get<PractitionerModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllPractitoners', options);
  }

  getAllSpecialityPractitoners() {
    return this.http.get<PractitionerSpecialityModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllSpecialityPractitoners');
  }

  getAllGroupSpecialityPractitoners() {
    return this.http.get<GroupSpecialityPractitonerModel[]>(this.environmentSettings.apiBaseUrl + '/GetAllGroupSpecialityPractitoners');
  }

  getAllPractitonersByLocationId(Id: string) {
    return this.http.get<PractitionerSpecialityModel[]>(this.environmentSettings.apiBaseUrl + `/GetAllPractitonersByLocationId/${Id}`);
  }

  getAllSpecialityPractitonersByLocId(id: string) {
    return this.http.get<PractitionerSpecialityModel[]>(this.environmentSettings.apiBaseUrl + `/GetAllSpecialityPractitonersByLocId/${id}`);
  }

  getAllGroupSpecialityPractitonersByLocId(id: string) {
    return this.http.get<GroupSpecialityPractitonerModel[]>(this.environmentSettings.apiBaseUrl + `/GetAllGroupSpecialityPractitonersByLocId/${id}`);
  }

  getAllPractitionerSpecialityByLocationIdForCalendar(locationId: string) {
    return this.http.get<PractitionerSpecialityModel[]>(this.environmentSettings.apiBaseUrl + '/getAllPractitionerSpecialityByLocationIdForCalendar/', {params: {locationId: locationId}});
  }

  publishSomeData(data: any) {
    this.displayMessage.next(data);
  }

  getObservable(): Subject<any> {
    return this.displayMessage;
  }
}
