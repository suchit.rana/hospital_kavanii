import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BaseService } from "./app.base.service";

@Injectable({
  providedIn: 'root'
})
export class GeoLocationService extends BaseService {

    allCountries: any[] = [];
    allStates: any[] = [];
    allCities: any[] = [];

    getAllCountries(): Observable<any[]> {

        return this.http.get<any[]>("http://192.168.1.219:8008/countries.json");

    }

    getAllStates(): Observable<any[]> {

        return this.http.get<any[]>("http://192.168.1.219:8008/states.json");

    }

    getAllCities(): Observable<any[]> {

        return this.http.get<any[]>("http://192.168.1.219:8008/cities.json");

    }

}
