import {Injectable} from '@angular/core';
import {BaseService} from './app.base.service';
import {SchedulerSettings} from '../models/scheduler-settings';
import {PatientModel} from '../models/app.patient.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../app.state';


@Injectable({
  providedIn: 'root'
})
export class SchedulerSettingsService extends BaseService {

  constructor(public http: HttpClient, private appStateService: AppState) {
    super(http);
  }

  getSchedulerSettings(userId: string) {
    return this.http.get<SchedulerSettings>(this.environmentSettings.apiBaseUrl + '/UsersSchedulesSettings', {params: {userId: userId}});
  }

  saveSchedulerSettings(schedulerSettings: SchedulerSettings) {
    return this.http.post<any>(this.environmentSettings.apiBaseUrl + '/UsersSchedulesSettings', schedulerSettings);
  }

  updateSchedulerSettings(schedulerSettings: SchedulerSettings) {
    return this.http.put<any>(this.environmentSettings.apiBaseUrl + '/UsersSchedulesSettings', schedulerSettings);
  }

  deleteSchedulerSettings(id: string) {
    return this.http.delete<any>(this.environmentSettings.apiBaseUrl + '/UsersSchedulesSettings', {params: {usersSchedulesSettingsId: id}});
  }

  getPatientsBySearch(search: string): Observable<PatientModel[]> {
    if (!this.appStateService.selectedUserLocationId) {
      console.error('Location not selected');
      return;
    }
    return this.http.get<PatientModel[]>(this.environmentSettings.apiBaseUrl + '/getallpatientsbysearch', {params: {patientSearch: search, locationId: this.appStateService.selectedUserLocationId}});
  }

}
