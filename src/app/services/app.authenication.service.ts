import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {Register, ForgotPasswordModel, LoginModel, FinaliseModel, LoginSubmitModel, UserProfile} from '../models/app.user.model';
import {BaseService} from './app.base.service';
import {Observable} from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService extends BaseService {
    login(loginSubmit: LoginSubmitModel) {
        return this.http.post<LoginModel>(this.environmentSettings.apiBaseUrl + '/login?username=' + loginSubmit.userid + '&password=' + loginSubmit.password, {});
    }

    refreshToken() {
        return this.http.post<LoginModel>(this.environmentSettings.apiBaseUrl + `/refresh-token`, {}, {params: {token: this.getToken()}});
    }

    updateToken() {
        this.refreshToken().subscribe(result => {
            localStorage.setItem("token", result.token);
        });
    }

    private setSession(authResult) {
        const expiresAt = moment().add(authResult.expiresIn, 'second');

        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    }

    register(registerModel: Register) {
        registerModel.isTermOfService = true;
        return this.http.post<any>(this.environmentSettings.apiBaseUrl + '/register', registerModel);
    }

    forgotPassword(forgotPasswordModel: ForgotPasswordModel): Observable<any> {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/forgotpassword', forgotPasswordModel);
    }

    resetPassword(resetPasswordModel: LoginSubmitModel) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/resetPassword', resetPasswordModel);
    }

    validateResetPasswordToken(userId: string, token: string) {
        return this.http.post(this.environmentSettings.apiBaseUrl + '/validateresetpasswordtoken',
            {}, {params: {'userId': userId, 'token': token}});
    }

    logout() {
        localStorage.removeItem('token');
    }

    public isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem('expires_at');
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    confirmEmail(token: string, userid: string) {
        return this.http.get<any>(this.environmentSettings.apiBaseUrl + `/confirmEmail/?token=${token}&userid=${userid}`);
    }

    createPassword(finalise: FinaliseModel) {
        return this.http.post<any>(this.environmentSettings.apiBaseUrl + '/createPassword', finalise);
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    isOwner(): boolean {
        const idToken = this.getToken();
        if (idToken) {
            const data = jwt_decode(idToken);
            return data && data.Roles && data.Roles.indexOf('Owner') > -1;
        }
        return false;
    }

}
