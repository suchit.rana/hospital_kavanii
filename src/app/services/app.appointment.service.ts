import { Injectable } from '@angular/core';
import {
  Appointment,
  AppointmentResponse,
  BreakEvent,
  Schedule,
  ScheduleCheck,
  ScheduleResponse,
  WaitListEvent,
} from '../models/app.appointment.model';
import { BaseService } from './app.base.service';
import { AppUtils } from '../shared/AppUtils';

@Injectable()
export class AppointmentService extends BaseService {
  // Schedule

  getPractitionerWithNoScheduleDates<T>(startDate, locationId: string, practitionerId: string, noOfDays, type: number = 0) {
    return this.http.get<T[]>(
      this.environmentSettings.apiBaseUrl +
      '/GetPractitionerWithNoScheduleDates',
      {
        params: {
          'startDate': startDate,
          'locationId': locationId,
          'practitionerId': practitionerId,
          'noOfDays': noOfDays,
          'type': AppUtils.getStringDefault(type, '0')
        }
      }
    );
  }

  getAllPractitionerSchedulesDetails<T>(locationId: string, type: number = 0) {
    return this.http.get<T[]>(
      this.environmentSettings.apiBaseUrl +
      '/GetAllPractitionerSchedulesDetails',
      { params: { 'locationId': locationId, 'type': AppUtils.getStringDefault(type, '0') } }
    );
  }

  getPractitionerSchedules(locationId: string, startDate, noOfDays, practitionerId: string = '') {
    return this.http.get<ScheduleCheck[]>(
      this.environmentSettings.apiBaseUrl +
      '/GetPractitionerSchedules',
      { params: { 'locationId': locationId, 'startDate': startDate, 'noOfDays': noOfDays, 'practitionerId': practitionerId } }
    );
  }

  updatePractitionerSort(data: any) {
    return this.http.put(this.environmentSettings.apiBaseUrl + '/UpdateUsersPractitonerSchedulerOrder', data);
  }

  getPractitionerSchedulesDetails(scheduleDetail: any) {
    return this.http.get<any[]>(
      this.environmentSettings.apiBaseUrl +
      `/GetPractitionerSchedulesDetails?scheduleId=${scheduleDetail.scheduleId}&practitonerId=${scheduleDetail.practitonerId}&locationid=${scheduleDetail.locationid}`
    );
  }

  createSchedule(scheduleDetails: Schedule) {
    this.removeExtraKeys(scheduleDetails);
    return this.http.post<ScheduleResponse>(
      this.environmentSettings.apiBaseUrl + '/CreatePractitionerSchedule',
      scheduleDetails
      // { responseType: "text" as "json" }
    );
  }

  createAppointment(scheduleDetails: Schedule) {
    this.removeExtraKeys(scheduleDetails);
    return this.http.post<AppointmentResponse>(
      this.environmentSettings.apiBaseUrl + '/Appointment',
      scheduleDetails
      // { responseType: "text" as "json" }
    );
  }

  updateAppointment(scheduleDetails: Schedule) {
    this.removeExtraKeys(scheduleDetails);
    return this.http.put<AppointmentResponse>(
      this.environmentSettings.apiBaseUrl + '/Appointment',
      scheduleDetails
      // { responseType: "text" as "json" }
    );
  }

  deleteAppointment(id: string, locationId: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl + `/Appointment?locationId=${locationId}&appointmentId=${id}`
    );
  }
  getAppointmentsByLocationAndPatientId(locationId: string, patientId: string) {
    return this.http.get<WaitListEvent>(
      this.environmentSettings.apiBaseUrl +
      `/GetAppointmentsByLocationIdByPatientId?locationid=${locationId}&patientid=${patientId}`
    );


  }

  updateSchedule(scheduleDetails: Schedule) {
    this.removeExtraKeys(scheduleDetails);
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/UpdatePractitionerSchedule',
      scheduleDetails
      // { responseType: "text" as "json" }
    );
  }

  deleteSchedule(id: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl + `/RemovePractitionerSchedule/${id}`
    );
  }

  // Break

  getAllPractitionerBreakDetails() {
    return this.http.get<BreakEvent[]>(
      this.environmentSettings.apiBaseUrl + '/GetAllPractitionerBreakDetails'
    );
  }

  createBreak(breakEvent: BreakEvent) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/CreatePractitionerBreak',
      breakEvent,
      { responseType: 'text' as 'json' }
    );
  }

  updateBreak(breakEvent: BreakEvent) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/UpdatePractitionerBreak',
      breakEvent,
      { responseType: 'text' as 'json' }
    );
  }

  deleteBreak(id: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl + `/RemovePractitionerBreak/${id}`
    );
  }

  //WaitList
  getAllPractitionerWaitListDetails(locationid: string) {
    return this.http.get<WaitListEvent[]>(
      this.environmentSettings.apiBaseUrl +
      `/GetAllPractitionerWaitListDetails/${locationid}`
    );
  }

  createPractitionerWaitList(waitlist: WaitListEvent) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/CreatePractitionerWaitList',
      waitlist,
      { responseType: 'text' as 'json' }
    );
  }

  updatePractitionerWaitList(waitlist: WaitListEvent) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/UpdatePractitionerWaitList',
      waitlist,
      { responseType: 'text' as 'json' }
    );
  }

  removePractitionerWaitList(id: string) {
    return this.http.delete<any>(
      this.environmentSettings.apiBaseUrl + `/RemoveWaitListDetailsById/${id}`
    );
  }

  getAllPractitionerWaitListDetailsById(
    id: string,
    practitonerId: string,
    locationId: string
  ) {
    return this.http.get<WaitListEvent>(
      this.environmentSettings.apiBaseUrl +
      `/GetAllPractitionerWaitListDetailsById?waitlistid=${id}&practitonerId=${practitonerId}&locationid=${locationId}`
    );
  }

  getAllPractitionerEventDetails(id: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetAllPractitionerEventDetails/${id}`
    );
  }

  getProductAndServiceSearch(locationId: string, patientId?: string) {
    const queryParams = { locationId };
    if (patientId) {
      queryParams['patientId'] = patientId;
    }
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      '/getProductAndServiceSearch',
      {
        params: queryParams
      }
    );
  }

  GetAllAppointmentsByLocation(locationId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetAllAppointmentsByLocation?locationid=${locationId}`
    );
  }

  GetAllPatientAppointmentInvoiceByLocation(locationId: string, patientId: string) {
    return this.http.get<any>(
      this.environmentSettings.apiBaseUrl +
      `/GetAllPatientAppointmentInvoiceByLocation?locationid=${locationId}&patientId=${patientId}`
    );
  }

  getAppointmentsById(locationId: string, id: string) {
    return this.http.get<Appointment>(
      this.environmentSettings.apiBaseUrl +
      `/GetAppointmentsById?locationId=${locationId}&appointmentsId=${id}`
    );
  }


  private removeExtraKeys(data: Schedule) {
    // delete data.text;
    // delete data.type;
    delete data.mode;
    delete data._startDate;
    delete data._endDate;
    delete data.startDate;
    delete data.endDate;
    // delete data.uuid;
  }
}

export class Data {
  text!: string;
  practitionerId!: string;
  startDate!: Date;
  endDate!: Date;
}
