import {Injectable} from '@angular/core';
import {ApplicationDataModel} from '../models/app.settings.model';
import {BaseService} from './app.base.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class ApplicationDataService extends BaseService {

  getAllApplicationDataByLocation(locationId: string) {
    return this.http.get<ApplicationDataModel[]>(
      this.environmentSettings.apiBaseUrl + `/getallapplicationdatabylocation?locationId=${locationId}`
    );
  }

  GetApplicationDataByCategoryId(categoryId: number, locationId: string) {
    return this.http.get<ApplicationDataModel[]>(
      this.environmentSettings.apiBaseUrl + `/getapplicationdatabycategoryid?categoryId=${categoryId}`
    );
  }

  GetApplicationDataByCategoryIdAndLocationId(categoryId: number, locationId: string) {
    return this.http.get<ApplicationDataModel[]>(
      this.environmentSettings.apiBaseUrl + `/getapplicationdatabycategoryidandlocationId?categoryId=${categoryId}&locationId=${locationId}`
    );
  }

  getApplicationDataById(id: string) {
    return this.http.get<ApplicationDataModel>(
      this.environmentSettings.apiBaseUrl + `/getapplicationdatabyid?applicationDataId=${id}`
    );
  }

  createApplicationData(applicationDataModel: ApplicationDataModel) {
    return this.http
      .post<any>(
        this.environmentSettings.apiBaseUrl + '/createapplicationdata',
        applicationDataModel
      );
  }

  updateApplicationData(applicationDataModel: ApplicationDataModel) {
    return this.http.post<any>(
      this.environmentSettings.apiBaseUrl + '/updateApplicationData',
      applicationDataModel
    );
  }
}
