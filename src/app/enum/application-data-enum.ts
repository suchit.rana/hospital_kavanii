export enum ApplicationDataEnum {
  Title = 1,
  StaffPosition = 2,
  PatientStatus = 3,
  PatientPosition = 4,
  PatientRelationship = 5,
  PatientClassification = 6,
  PatientOccupation = 7,
  CardType = 8,
  HealthFundName = 9,
  ServiceCategory = 10,
  ClassCategory = 11,
  ProductCategory = 12,
  UnitOfMeasurement = 13,
  ContactCategory = 14,
  BreakName = 15,
  Case = 16,
  StaffOtherIdName
}

export enum HealthFundNameEnum {
  Medicare = 1,
  DVA = 2,
}

export enum GeneralContactCategoryEnum {
  Employer = 1,
  Supplier = 2,
}


export enum ContactTypeEnum {
  Referral = 1,
  ThirdParty = 2,
  General = 3,
}

export enum CaseContactType {
  Referral = 1,
  ThridParty = 2
}

export enum CaseStatus {
  Active = 'Open',
    Reopen = "Re-Opened",
  Discharge = 'Discharged'
}

export enum ReferralStatus {
  Active = 'Active',
  InActive = 'In-Active',
  Completed = 'Completed',
  Expired = 'Expired',
  Rejected = 'Rejected'
}

export enum AppointmentType {
  SCHEDULE ,
  BREAK,
  APPOINMENT ,
  Holiday
}

export enum ScheduleMode {
  NORMAL = 'NORMAL',
  EDIT = 'EDIT'
}

export enum EditMode {
  Series,
  ThisAndFuture,
  Event,
  Occurrence,
}

export enum Frequency {
  None,
  SameAsSchedule,
  Day,
  Weekly,
  Monthly,
  Yearly,
}
