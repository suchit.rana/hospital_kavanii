export enum SchedulerSettingsEnum {
  PrintSchedule,
  AppointmentListView,
  PractitionerAvailability,
  TransferAppointment,
  SchedulerSettings,
  SchedulerLegend
}
