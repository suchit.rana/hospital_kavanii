import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertComponent} from './alert/alert.component';
import {BaseItemComponent} from './base-item/base-item.component';
import {MatIconModule} from '@angular/material/icon';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppAlertService} from './services/app-alert.service';
import {SpinnerComponent} from './spinner/spinner.component';
import {PopupComponentComponent} from './popup-component/popup-component.component';
import {ComponentLoadDirective} from './directives/component-load.directive';
import {AppDialogService} from './services/app-dialog.service';
import { StripeCardElementService } from './services/stripe-card/stripe-card-element.service';

@NgModule({
  declarations: [AlertComponent, BaseItemComponent, SpinnerComponent, PopupComponentComponent, ComponentLoadDirective],
  imports: [
  CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTooltipModule,
    OverlayModule,
    MatProgressSpinnerModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    //Modules
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTooltipModule,

    //COMPONENTS
    AlertComponent,
    BaseItemComponent,
    SpinnerComponent,
  ],
  providers: [AppAlertService, AppDialogService,StripeCardElementService],
  entryComponents: [AlertComponent, PopupComponentComponent]
})
export class SharedModuleModule {
}
