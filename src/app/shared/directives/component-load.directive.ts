import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: "[component-load]"
})
export class ComponentLoadDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
