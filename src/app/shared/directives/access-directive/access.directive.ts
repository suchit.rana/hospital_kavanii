import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {AppState} from '../../../app.state';
import {hasPermission} from '../../../app.component';

@Directive({
    selector: '[appAccess]'
})
export class AccessDirective {

    private _permission: string | string[];

    @Input()
    set appAccessLookFor(permission: string | string[]) {
        this._permission = permission || 'CVED';
    }

    @Input()
    set appAccess(moduleName: string) {
        if (!moduleName) {
            this.viewContainer.createEmbeddedView(this.templateRef);
            return;
        }
        this.checkPermission(moduleName);
    }

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private appState: AppState
    ) {
    }

    private checkPermission(moduleName: string) {
        if (hasPermission(this.appState, moduleName, this._permission)) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }
}
