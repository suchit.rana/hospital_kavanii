import {
    AfterViewInit,
    Component,
    ComponentFactoryResolver,
    ComponentRef, ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnInit,
    TemplateRef,
    Type,
    ViewChild, ViewContainerRef,
    ViewEncapsulation
} from '@angular/core';
import {ComponentLoadDirective} from '../directives/component-load.directive';
import {BehaviorSubject} from 'rxjs';
import {DialogRef} from '@progress/kendo-angular-dialog';
import {IPopupAction, PopupSetting} from '../services/app-dialog.service';
import {ResizeObserverService} from '@progress/kendo-angular-common';
import ResizeObserver from 'resize-observer-polyfill';

export const CANCEL = 'CANCEL';
export const CLOSE = 'CLOSE';
export const SAVE = 'SAVE';
export const DELETE = 'DELETE';

@Component({
    selector: 'app-popup-component',
    templateUrl: './popup-component.component.html',
    styleUrls: ['./popup-component.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PopupComponentComponent implements OnInit, AfterViewInit, IPopupAction {
    @Input('component')
    component: Type<any>;
    @Input()
    data: PopupSetting = {};

    clickCancel: EventEmitter<any> = new EventEmitter<any>();
    clickSave: EventEmitter<any> = new EventEmitter<any>();
    close: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('divElement', {static: true}) divElement: ElementRef;

    @ViewChild(ComponentLoadDirective, {static: true}) contentContainer: ComponentLoadDirective;
    @ViewChild('footerTemplate', {static: false, read: ViewContainerRef}) footerContainer: ViewContainerRef;

    hideFooter: boolean = false;
    componentRef: ComponentRef<any>;

    height$ = new BehaviorSubject<number>(window.outerHeight);
    private _clickedBtn;
    private _footerTemplate;

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        public dialog: DialogRef,
        private _elRef: ElementRef
    ) {
    }

    @HostListener('window:resize')
    public detectResize(): void {
        this.setHeight();
    }

    public resize(): void {
        console.log('Resize');
    }

    ngOnInit() {
        this.dialog.result.subscribe(() => {
            this.close.emit(this._clickedBtn);
        });
        if (this.data.hideFooter) {
            this.hideFooter = this.data.hideFooter;
        }
        if (this.component) {
            this.loadComponent();
        }
    }

    ngAfterViewInit(): void {
        this.createFooterTemplate();
        setTimeout(() => this.setHeight(), 100);
        let ro = new ResizeObserver(entries => {
            for (let entry of entries) {
                this.setHeight();
            }
        });

        // Element for which to observe height and width
        ro.observe(this.divElement.nativeElement);
    }

    loadComponent() {
        const viewContainerRef = this.contentContainer.viewContainerRef;
        viewContainerRef.clear();
        if (this.component instanceof TemplateRef) {
            viewContainerRef.createEmbeddedView(this.component);
        } else {
            this.componentRef = viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(this.component));
            if (this.data.data && Object.keys(this.data.data).length > 0) {
                const instance = this.componentRef.instance;
                Object.keys(this.data.data).forEach(key => {
                    instance[key] = this.data.data[key];
                });
            }
        }
    }

    private setHeight(): void {
        const height = window.innerHeight - 100;
        const width = window.innerWidth - 100;
        const content = document.querySelector('#' + this.dialog.dialog.instance.contentId) as HTMLElement;
        if (content && content.offsetHeight > height) {
            this.dialog.dialog.instance.height = height;
        }
        if (content && content.offsetWidth > width) {
            this.dialog.dialog.instance.width = width;
        }
        this.height$.next(height);
    }

    save(data?: any) {
        this._clickedBtn = SAVE;
        this.clickSave.emit(data || true);
        if (this.data.hideOnSave) {
            this.dialog.close();
        }

    }

    cancel() {
        this._clickedBtn = CANCEL;
        this.clickCancel.emit(true);
        if (this.data.hideOnCancel) {
            this.dialog.close();
        }
    }

    closeDialog(value?: any) {
        this._clickedBtn = value || CLOSE;
        this.dialog.close();
    }

    changeTitle(title: string) {
        this.data.title = title || 'Title';
    }

    footerTemplate(template: Type<any>) {
        this._footerTemplate = template;
    }

    private createFooterTemplate() {
        if (!this.footerContainer || !this._footerTemplate) {
            return;
        }
        const viewContainerRef = this.footerContainer;
        viewContainerRef.clear();
        if (this._footerTemplate instanceof TemplateRef) {
            viewContainerRef.createEmbeddedView(this._footerTemplate);
        } else {
            this.componentRef = viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(this._footerTemplate));
            if (this.data.data && Object.keys(this.data.data).length > 0) {
                const instance = this.componentRef.instance;
                Object.keys(this.data.data).forEach(key => {
                    instance[key] = this.data.data[key];
                });
            }
        }
    }
}
