import {InjectionToken} from '@angular/core';
import {AppAlertData} from './alert/alert.component';

export const APP_ALERT_DATA = new InjectionToken<AppAlertData>('APP_ALERT_DATA', {
  factory(): AppAlertData {
    return undefined;
  },
  providedIn: 'root'
});
export const ERROR_CODE = '0001';
export const SUCCESS_CODE = '0000';

export const PATIENT_TAB_DETAILS = 0;
export const PATIENT_TAB_CASE_CONTACTS = 1;
export const PATIENT_TAB_APPT = 2;
export const PATIENT_TAB_TREATMENT_NOTES = 3;
export const PATIENT_TAB_LETTERS = 4;
export const PATIENT_TAB_FORMS = 5;
export const PATIENT_TAB_BILLING = 6;
export const PATIENT_TAB_ALERT_RECALL = 7;
export const PATIENT_TAB_COMMUNICATIONS = 8;
export const PATIENT_TAB_DOCUMENTS = 9;

export class Constant {
  static readonly ALL = 'ALL';
  static readonly ACTIVE = 'ACTIVE';
  static readonly INACTIVE = 'INACTIVE';

  static readonly ADD_TO_SDLR = 'ADD_TO_SDLR';
  static readonly REMOVE_FRM_SDLR = 'REMOVE_FRM_SDLR';
  static readonly VIEW_PROFILE = 'VIEW_PROFILE';

  static readonly CHANGE_GROUP = 'CHANGE_GROUP';
  static readonly CREATE_APPT = 'CREATE_APPT';
  static readonly PTTTR_TODAY = 'PTTTR_TODAY';
  static readonly SCHEDULE = 'SCHEDULE';
  static readonly WAIT_LIST = 'WAIT_LIST';
  static readonly ROOMS = 'ROOMS';
  static readonly ROOMS_ACTIVE = 'ROOMS_ACTIVE';

  static readonly ADD = 'ADD';
  static readonly EDIT = 'EDIT';
  static readonly DELETE = 'DELETE';

}
