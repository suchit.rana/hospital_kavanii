import * as moment from 'moment';
import {AppoinmentDateUtils} from '../layout/content-full-layout/appointment/AppoinmentDateUtils';

export class DateUtils {

  static dateFormat = 'ddd, D MMM YYYY';
  static standardFormat = 'YYYY-MM-DD';

  static getInstance(date: any, format?: string) {
    if (format) {
      return moment(date, format);
    }
    return moment(date);
  }

  static getDefaultInstance() {
    return moment();
  }

  public static getCurrentFormatedDate() {
    return this.getDefaultInstance().format(this.standardFormat);
  }

  static getCurrentDate() {
    const date = this.getDefaultInstance().format();
    return this._toDate(date);
  }

  static getCurrentDateFormat(format: string) {
    return this.getDefaultInstance().format(format);
  }

  public static getStandardFormatedDate(date) {
    return this.getInstance(date).format(this.standardFormat);
  }

  public static getFormatedDate(date) {
    return this.getInstance(date).format(this.dateFormat);
  }

  static addAndGetNewValue(date: any, num, type: string) {
    return this.getInstance(date).clone().add(type, num).format();
  }

  static getStartDateOfWeek(date: any) {
    return this.getInstance(date).startOf('isoWeek').format();
  }

  static getEndDateOfWeek(date: any, period: number) {
    return this.addAndGetNewValue(date, period, 'd');
  }

  static isBetween(start: any, end: any, date: any): boolean {
    let mm = this.getInstance(DateUtils.getStandardFormatedDate(date), 'YYYY-MM-DD');
    let startDate = this.format(start, 'YYYY-MM-DD');
    let endDate = this.format(end, 'YYYY-MM-DD');

    return (mm.isAfter(startDate) || mm.isSame(startDate)) && (mm.isBefore(endDate) || mm.isSame(endDate));
  }

  static isBetweenStrict(start: any, end: any, date: any): boolean {
    let mm = this.getInstance(DateUtils.getStandardFormatedDate(date), 'YYYY-MM-DD');
    let startDate = this.format(start, 'YYYY-MM-DD');
    let endDate = this.format(end, 'YYYY-MM-DD');
    let isValidStartTime = this.compareTime(date, start) >= 0;
    let isValidEndTime = this.compareTime(date, end) <= 0;

    return (mm.isAfter(startDate) || mm.isSame(startDate)) && (mm.isBefore(endDate) || mm.isSame(endDate)) && (isValidStartTime && isValidEndTime);
  }

  static isEqualStrict(date1: any, date2: any): boolean {
    let mm = this.getInstance(DateUtils.format(date1, 'YYYY-MM-DD HH:mm'), 'YYYY-MM-DD HH:mm');
    let date2Format = this.format(date2, 'YYYY-MM-DD HH:mm');

    return mm.isSame(date2Format);
  }

  static isLessStrict(date1: any, date2: any): boolean {
    let mm = this.getInstance(DateUtils.format(date1, 'YYYY-MM-DD HH:mm'), 'YYYY-MM-DD HH:mm');
    let date2Format = this.format(date2, 'YYYY-MM-DD HH:mm');

    return mm.isBefore(date2Format);
  }

  static isEqual(date1: any, date2: any): boolean {
    let mm = this.getInstance(DateUtils.getStandardFormatedDate(date1), 'YYYY-MM-DD');
    let date2Format = this.format(date2, 'YYYY-MM-DD');

    return mm.isSame(date2Format);
  }

  static isLess(date1: any, date2: any): boolean {
    let mm = this.getInstance(DateUtils.getStandardFormatedDate(date1), 'YYYY-MM-DD');
    let date2Format = this.format(date2, 'YYYY-MM-DD');

    return mm.isBefore(date2Format);
  }

  static isGreater(date1: any, date2: any): boolean {
    let mm = this.getInstance(DateUtils.getStandardFormatedDate(date1), 'YYYY-MM-DD');
    let date2Format = this.format(date2, 'YYYY-MM-DD');

    return mm.isAfter(date2Format);
  }

  static compareTime(date1: any, date2: any): number {
    const first = this.getTime(moment(date1).clone());
    const second = this.getTime(moment(date2).clone());

    // console.log(date1, date2)
    // console.log(first, second)

    if (first.isAfter(second)) {
      return 1;
    } else if (first.isSame(second)) {
      return 0;
    } else if (first.isBefore(second)) {
      return -1;
    }
  }

  static getFormatedDateForWeek(currentDate: any, daysToAdd: number): string {
    let start = this.getFormatedDate(this.getStartDateOfWeek(currentDate));
    let end = this.getFormatedDate(this.getEndDateOfWeek(currentDate, daysToAdd));
    return start + ' to ' + end;
  }

  static format(value: any, format: string) {
    return this.getInstance(value).format(format);
  }

  static formatTime(value: any, format: string) {
    return this.getInstance(value, 'HH:mm A').format(format);
  }

  static formatDateTime(value: any, format: string, isTime: boolean) {
    if (isTime) {
      return this.formatTime(value, format);
    }
    return this.format(value, format);
  }

  static combineDateTime(date, value) {
    return this.getInstance(date).set(value).format();
  }

  static toUtc(date) {
    return this.getInstance(date).utc().format('YYYY-MM-DDTHH:mm:ss[Z]');
  }

  static toUtcWithNoTime(date) {
    return this.getInstance(date).utc().set({'hour': 0, 'minute': 0, 'second': 0}).format('YYYY-MM-DDTHH:mm:ss[Z]');
  }

  static toUtcRecurrenceException(date) {
    return this.getInstance(date).utc().format('YYYYMMDDTHHmmss[Z]');
  }

  static toUtcUntilRecurrenceException(date, hour = 18, minute = 29, seconds = 59, isKeepLocal: boolean = false) {
    return this.getInstance(date).utc(isKeepLocal).set({'hour': hour, 'minute': minute, 'second': seconds}).format('YYYYMMDDTHHmmss[Z]');
  }

  static getTime(dateTime) {
    return moment({h: dateTime.hours(), m: dateTime.minutes()});
  }

  static _toDate(date) {
    return new Date(parseInt(this.format(date, 'YYYY')), (parseInt(this.format(date, 'M')) - 1), parseInt(this.format(date, 'D')), parseInt(this.format(date, 'H')), parseInt(this.format(date, 'm')), parseInt(this.format(date, 's')));
  }

  static getHour(date: Date) {
    return parseInt(moment(date).format('HH'));
  }

  static getMinute(date: Date) {
    return parseInt(moment(date).format('mm'));
  }
}
