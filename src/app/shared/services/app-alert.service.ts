import {ComponentRef, Injectable, Injector} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {MessageType} from '../../models/app.misc';
import {ComponentPortal, PortalInjector} from '@angular/cdk/portal';
import {AlertComponent, AppAlertData} from '../alert/alert.component';
import {APP_ALERT_DATA} from '../Constant';
import {Subscription} from 'rxjs';

@Injectable()
export class AppAlertService {
  private overlayRef: OverlayRef;
  private subs: Subscription;
  private compRef: ComponentRef<AlertComponent>;

  constructor(
    public overlay: Overlay,
    private injector: Injector
  ) {
  }

  private open(message: string, type: MessageType, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = '', autoClose = true):ComponentRef<AlertComponent> {
    let config = new OverlayConfig();

    config.positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally();

    this.overlayRef = this.overlay.create(config);

    this.overlayRef.backdropClick().subscribe(() => {
      this.overlayRef.dispose();
    });

    let data = {
      message: message,
      type: type,
      closable: closable,
      extraClass: extraClass,
      isAbsolute: isAbsolute,
      autoClose: autoClose
    } as AppAlertData;
    this.compRef = this.overlayRef.attach(new ComponentPortal<AlertComponent>(AlertComponent, null, this.createInjector(data)));
    this.subs = this.compRef.instance.detach.subscribe(value => {
      if (value) {
        this.close();
      }
    });
    return this.compRef;
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.compRef.destroy();
      if (this.subs) {
        this.subs.unsubscribe();
      }
    }
  }

  displaySuccessMessage(message: string, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = '') {
    this.open(message, MessageType.success, closable, isAbsolute, extraClass);
    // setTimeout(() => {
    //   this.message = '';
    //   this.type = MessageType.success;
    // }, 5000);
  }

  displayErrorMessage(message: string, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = '') {
    this.open(message, MessageType.error, closable, isAbsolute, extraClass);
    // setInterval((a) => {
    //   this.message = '';
    //   this.type = MessageType.error;
    // }, 5000, [])
    // setTimeout(() => {
    //   this.message = '';
    //   this.type = MessageType.error;
    // }, 5000);
  }

  displayWarningMessage(message: string, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = '') {
    this.open(message, MessageType.warning, closable, isAbsolute, extraClass);
    // setInterval((a) => {
    //     this.message = '';
    //     this.type = MessageType.warning;
    //   },
    //   10000,
    //   []);
  }

  displayInfoMessage(message: string, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = '') {
    this.open(message, MessageType.info, closable, isAbsolute, extraClass);
  }

  error(message: string, autoClose: boolean = true, closable: boolean = true, isAbsolute: boolean = true, extraClass: string = ''): ComponentRef<AlertComponent> {
    return this.open(message, MessageType.error, closable, isAbsolute, extraClass, autoClose);
  }

  private createInjector(data: AppAlertData): PortalInjector {
    const injectorTokens = new WeakMap();
    injectorTokens.set(APP_ALERT_DATA, data);
    return new PortalInjector(this.injector, injectorTokens);
  }
}
