import {ComponentRef, EventEmitter, Injectable, Type} from '@angular/core';
import {DialogService} from '@progress/kendo-angular-dialog';
import {PopupComponentComponent} from '../popup-component/popup-component.component';
import {DialogSettings} from '@progress/kendo-angular-dialog/dist/es2015/dialog/dialog-settings';

@Injectable()
export class AppDialogService {

  constructor(
    private dialogService: DialogService
  ) {
  }

  open<T>(content: Type<any>, option: PopupSetting = {}): IPopupAction {
    if (option.height > window.outerHeight - 150) {
      option.height = window.outerHeight - 150;
    }
    const dialogData = {
      ...option,
      content: PopupComponentComponent,
      cssClass: 'app-popup-component'
    };
    const dialogRef = this.dialogService.open(dialogData);
    option.hideOnCancel = option.hideOnCancel == true || option.hideOnCancel == false ? option.hideOnCancel : true;
    option.hideOnSave = option.hideOnSave == true || option.hideOnSave == false ? option.hideOnSave : true;
    option.hideCancel = option.hideCancel == true || option.hideCancel == false ? option.hideCancel : false;
    option.hideSave = option.hideSave == true || option.hideSave == false ? option.hideSave : false;
    option.cancelBtnTitle = option.cancelBtnTitle || 'Cancel';
    option.saveBtnTitle = option.saveBtnTitle || 'Save';
    option.title = option.title || 'Dialog';
    const instance = dialogRef.content.instance as PopupComponentComponent;
    instance.component = content;
    instance.data = option;
    return dialogRef.content.instance;
  }
}

export interface IPopupAction {
  clickSave: EventEmitter<any>;
  clickCancel: EventEmitter<any>;
  close: EventEmitter<any>;
  componentRef: ComponentRef<any>;
  footerTemplate: Function;
  closeDialog: Function;
  changeTitle: Function;
  save: Function;
  cancel: Function;
}

export interface PopupSetting extends DialogSettings {
  saveBtnTitle?: string;
  cancelBtnTitle?: string;
  hideCancel?: boolean;
  hideSave?: boolean;
  hideFooter?: boolean;
  hideOnCancel?: boolean;
  hideOnSave?: boolean;
  data?: {[x: string]: any};
  bodyPadding?: boolean;
  buttons?: any[];
}
