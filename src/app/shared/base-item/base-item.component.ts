import { Component, OnInit, Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { MessageType } from 'src/app/models/app.misc';
import { LocationGridModel } from 'src/app/models/app.location.model';
import {AppAlertService} from '../services/app-alert.service';
import {Router} from '@angular/router';
import {RouterState} from '../interface';
import {hasPermission} from '../../app.component';
import {AppState} from '../../app.state';

@Component({
  selector: 'app-base-item',
  template: ``,
  styleUrls: ['./base-item.component.css']
})

@Injectable()
export class BaseItemComponent implements OnInit {
  readonly _any = ['C', 'V', 'E', 'D'];
  color: any;
  message: string;
  type: MessageType;
  submitting: boolean = false;
  apperance: string = "outline";
  addItem: boolean = true;
  public locationList: LocationGridModel[];
  itemid: string;

  moduleName: string;
  routerState: RouterState;

  constructor(public location: Location, protected router?: Router, protected appState?: AppState) {
    if (this.router && this.router.getCurrentNavigation() != null) {
      this.routerState = this.router.getCurrentNavigation().extras.state as RouterState;
    } else {
      this.routerState = history.state;
    }
  }

  ngOnInit() {

  }

  displaySuccessMessage(message: string) {
    this.message = message;
    this.type = MessageType.success;
    // setInterval((a) => {
    //   this.message = '';
    //   this.type = MessageType.success;
    // }, 5000, [])
    setTimeout(() => {
      this.message = '';
      this.type = MessageType.success;
    }, 5000);
  }

  displayErrorMessage(message: string) {
    this.message = message;
    this.type = MessageType.error;
    // setInterval((a) => {
    //   this.message = '';
    //   this.type = MessageType.error;
    // }, 5000, [])
    setTimeout(() => {
      this.message = '';
      this.type = MessageType.error;
    }, 5000);
  }

  displayWarningMessage(message: string) {
    this.message = message;
    this.type = MessageType.warning;
    setInterval((a) => {
        this.message = '';
        this.type = MessageType.warning;
      },
      10000,
      [])
  }

  displayInfoMessage(message: string) {
    this.message = message;
    this.type = MessageType.info;
    setInterval((a) => {
        this.message = '';
        this.type = MessageType.info;
      },
      10000,
      [])
  }

  cancel() {
    this.location.back();
  }

  isFromState(): boolean {
    return this.routerState && this.routerState.fromState;
  }

  hasPermission(moduleName: string, permission: string | string[]) {
    return hasPermission(this.appState, moduleName, permission);
  }

}
