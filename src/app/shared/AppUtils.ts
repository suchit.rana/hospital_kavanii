import {AbstractControl, FormGroup, ValidationErrors} from '@angular/forms';

export class AppUtils {

  public static checkFormAndDisplayError(formGroup: FormGroup) {
    for (const controlsKey in formGroup.controls) {
      const ctrl: AbstractControl = formGroup.get(controlsKey);
      if (ctrl.invalid) {
        ctrl.markAsDirty();
      }
    }
  }

  public static isJwtExpired(jwt: any): boolean {
    if (!jwt) {
      return false;
    }
    const current_time = Date.now() / 1000;
    return jwt.exp < current_time;
  }

  public static patternValidator(regex: RegExp, error: ValidationErrors) {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }

      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  static getInteger(value: any) {
    return this.getIntegerDefault(value, null);
  }

  static getIntegerDefault(value: any, defaultVal: number) {
    if (!value) {
      return defaultVal;
    }
    return parseInt(value);
  }

  static getStringDefault(value: any, defaultVal: string) {
    if (!value) {
      return defaultVal;
    }
    return value.toString();
  }

  static getBoolean(val) {
    if (!val) {
      return false;
    }
    return JSON.parse(JSON.stringify(val));
  }

  static refrenceClone(data: any) {
    if (!data) {
      return null;
    }
    return JSON.parse(JSON.stringify(data));
  }

  static isControlValid(formGroup: FormGroup, ...name: string[]): boolean {
    for (const val of name) {
      if (formGroup.get(val).invalid) {
        return false;
      }
    }
    return true;
  }

  static getBase64StrMimeType(base64: string) {
    if (base64) {
      const firstChar = base64.charAt(0);
      switch (firstChar) {
        case '/':
          return 'image/jpeg';
        case 'i':
          return 'image/png';
        case 'R':
          return 'image/gif';
        case 'U':
          return 'image/webp';
      }
    }
    return 'image/jpeg';
  }

  static prepareBase64String(base64: string) {
    return base64 ? `data:${this.getBase64StrMimeType(base64)};base64,` + base64 : '';
  }

  static getPlainBase64str(base64: string) {
    return base64 ? base64.split(',')[1] : '';
  }

  static isValidProviderNumber(value: string) {
    try {
      /**
       *  The Provider Number is comprised of:
       *  Provider Stem - a 6-digit number.
       *  1 Practice Location Character (PLV) - see below
       *  1 Check Digit
       *  The algorithm used for the Provider Check Digit is:
       *  (digit 1 * 3) +
       *  (digit 2 * 5) +
       *  (digit 3 * 8) +
       *  (digit 4 * 4) +
       *  (digit 5 * 2) +
       *  (digit 6) +
       *  (PLV * 6)
       *  Divide the result by 11
       *  The remainder is allocated an alpha that is the provider check digit (See table below for details). */

      const PLV = new Map<string, string>();
      PLV.set('0', '0');
      PLV.set('1', '1');
      PLV.set('2', '2');
      PLV.set('3', '3');
      PLV.set('4', '4');
      PLV.set('5', '5');
      PLV.set('6', '6');
      PLV.set('7', '7');
      PLV.set('8', '8');
      PLV.set('9', '9');
      PLV.set('A', '10');
      PLV.set('B', '11');
      PLV.set('C', '12');
      PLV.set('D', '13');
      PLV.set('E', '14');
      PLV.set('F', '15');
      PLV.set('G', '16');
      PLV.set('H', '17');
      PLV.set('J', '18');
      PLV.set('K', '19');
      PLV.set('L', '20');
      PLV.set('M', '21');
      PLV.set('N', '22');
      PLV.set('P', '23');
      PLV.set('Q', '24');
      PLV.set('R', '25');
      PLV.set('T', '26');
      PLV.set('U', '27');
      PLV.set('V', '28');
      PLV.set('W', '29');
      PLV.set('X', '30');
      PLV.set('Y', '31');

      const checkDigit = new Map<string, string>();
      checkDigit.set('0', 'Y');
      checkDigit.set('1', 'X');
      checkDigit.set('2', 'W');
      checkDigit.set('3', 'T');
      checkDigit.set('4', 'L');
      checkDigit.set('5', 'K');
      checkDigit.set('6', 'J');
      checkDigit.set('7', 'H');
      checkDigit.set('8', 'F');
      checkDigit.set('9', 'B');
      checkDigit.set('10', 'A');

      if (value.length < 8 || value.length > 8) {
        return false;
      }
      if (value.length == 8) {
        if (!/^(\d{7})$/g.test(value.substr(0, 7))) {
          return false;
        }

        let N = parseInt(value[0]) * 3
          + parseInt(value[1]) * 5
          + parseInt(value[2]) * 8
          + parseInt(value[3]) * 4
          + parseInt(value[4]) * 2
          + parseInt(value[5]);
        if (!PLV.has(value[6].toString())) {
          return false;
        }

        N += parseInt(PLV.get(value[6])) * 6;
        N = N % 11;
        if (!checkDigit.has(N.toString())) {
          return false;
        }
        if (checkDigit.get(N.toString()).toString() != value[7].toString()) {
          return false;
        }
      }
      return true;
    } catch (e) {
      return false;
    }
  }

   /**
   * This Method Is Use For Remove Blank And Null Key From Object.
   */
  static customJsonInclude(obj): void {
    for (const key in obj) {
      if (typeof obj[key] === 'object') {
        if (obj[key] && obj[key].length > 0) {
          obj[key] = this.removeEmptyElementsFromArray(obj[key]);
        }
        if (this.isEmptyObject(obj[key])) {
          delete obj[key];
        } else {
          this.customJsonInclude(obj[key]);
        }
      } else {
        if (obj[key] === undefined || obj[key] === null) {
          delete obj[key];
        }
      }
    }
  }

    /**
   * This Method Is Use From Remove Empty Element From Array
   * @param test_array  your selected array pass as args.
   */
  static  removeEmptyElementsFromArray(test_array): Array<any> {
    let index = -1;
    const arr_length = test_array ? test_array.length : 0;
    let resIndex = -1;
    const result = [];

    while (++index < arr_length) {
      const id = test_array[index];
      if (id) {
        result[++resIndex] = id;
      }
    }
    return result;
  }
   /*
  *
  * Used to check if object ios empaty or not..!
  * @param obj = 'indecated object which you want to check'
  * return true if empty..!
  */
 static isEmptyObject(obj): boolean {
  return (obj && (Object.keys(obj).length === 0));
}
}
