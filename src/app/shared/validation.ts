import {FormControl, FormGroup} from '@angular/forms';
import {DateUtils} from './DateUtils';
import {isNumber} from 'util';
import {AppUtils} from './AppUtils';


export function checkPasswordsValidator(group: FormGroup) { // here we have the 'passwords' group
  let pass = group.get('password').value;
  let confirmPass = group.get('confirmPassword').value;
  return pass === confirmPass ? null : {passwordNotMatch: true};
}

export function noWhitespaceValidator(control: FormControl) {
  const isWhitespace = (control.value || '').trim().length === 0;
  const isValid = !isWhitespace;
  return isValid ? null : {'whitespace': true};
}

export function email(control: FormControl) {
  let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(control.value) ? null : {'emailIncorrect': true};
}

export function startEndTimeValidator(startTimeControlName: string = 'startTime', endTimeControlName: string = 'endTime') {
  return (formGroup: FormGroup): { [key: string]: any } => {
    let startVal = formGroup.get(startTimeControlName).value;
    let endVal = formGroup.get(endTimeControlName).value;
    const valid = DateUtils.compareTime(startVal, endVal) < 0;
    if (valid) {
      formGroup.get(endTimeControlName).setErrors(null);
    } else {
      formGroup.get(endTimeControlName).setErrors({'endIsLess': true});
    }
    return;
  };
}

export function charLimit(max: number) {
  return (control: FormControl): { [key: string]: any } => {
    const length = (control.value || '').trim().length;
    return length > max ? {exceedLimit: `You can't type more than ${max} characters.`} : null;
  };
}

export function validateProviderNo(control: FormControl) {
  const value = control.value || '';
  return value ? AppUtils.isValidProviderNumber(value) ? null : {invalid: true} : null;
}
