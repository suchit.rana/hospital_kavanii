import { Injectable } from '@angular/core';
import {BaseService} from '../../services/app.base.service';

@Injectable({
  providedIn: 'root'
})
export class LetterTagListService extends BaseService{

  getTagList() {
    return this.http.get<any[]>(this.environmentSettings.apiBaseUrl + `/GetTagList`);
  }
}
