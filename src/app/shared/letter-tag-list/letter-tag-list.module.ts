import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LetterTagListComponent} from './letter-tag-list.component';
import {DropDownTreesModule} from '@progress/kendo-angular-dropdowns';


@NgModule({
  declarations: [
    LetterTagListComponent
  ],
  imports: [
    CommonModule,
    DropDownTreesModule
  ],
  exports: [
    LetterTagListComponent
  ]
})
export class LetterTagListModule {
}
