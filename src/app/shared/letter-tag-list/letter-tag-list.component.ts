import {Component, ElementRef, Input, OnDestroy, OnInit, Optional, Self, ViewEncapsulation} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {LetterTagListService} from './letter-tag-list.service';


@Component({
  selector: 'app-letter-tag-list',
  templateUrl: './letter-tag-list.component.html',
  styleUrls: ['./letter-tag-list.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: LetterTagListComponent}],
  encapsulation: ViewEncapsulation.None
})
export class LetterTagListComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, OnDestroy {

  nodes: any[];
  childrenField: string = 'items';
  expanded = false;
  btnText = 'More';
  liked = false;
  _value: any;

  static nextId = 0;
  focused = false;
  controlType = 'app-letter-tag-list';
  id = `app-letter-tag-list${LetterTagListComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();
  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private letterTagListService: LetterTagListService,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.getTagList();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value ? value : null;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  changeVal() {
    this.markAsTouched();
    this.value = null;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.touched && this.ngControl.invalid;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return false;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(data: any | null) {
    this._value = data ? data : null;
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {

  }

  toggleRecipe(): void {
    this.expanded = !this.expanded;
    this.btnText = this.expanded ? 'Less' : 'More';
    this.heartIcon();
  }

  heartIcon(): string {
    return this.liked ? 'k-icon k-i-arrow-60-up' : 'k-icon k-i-arrow-60-down';
  }

  toggleLike(): void {
    this.liked = !this.liked;
  }

  private getTagList() {
    this.letterTagListService.getTagList().subscribe(response => {
      try {
        this.nodes = JSON.parse(JSON.stringify(response));
      } catch (e) {

      }
    });
  }

  isNotChild(dataItem: any) {
    return !dataItem.hasOwnProperty('items');
  }

  dragStart($event: DragEvent, dataItem) {

  }
}
