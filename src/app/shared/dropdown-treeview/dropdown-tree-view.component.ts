import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {DropDownTreeComponent} from '@progress/kendo-angular-dropdowns';
import {FilterExpandSettings} from '@progress/kendo-angular-treeview';


@Component({
  selector: 'app-dropdown-tree-view',
  templateUrl: './dropdown-tree-view.component.html',
  styleUrls: ['./dropdown-tree-view.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: DropdownTreeviewComponent}],
  encapsulation: ViewEncapsulation.None
})
export class DropdownTreeviewComponent implements MatFormFieldControl<any>, ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {

  @Input()
  data: any[];
  @Input()
  filterable: boolean = false;
  @Input()
  childrenField: string = 'items';
  @Input()
  parentIdField: string = 'type';
  @Input()
  textField: string = 'text';
  @Input()
  valueField: string = 'id';

  @Output()
  valueChange: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('dTreeView', {static: false}) dTreeView: DropDownTreeComponent;

  public filterExpandSettings: FilterExpandSettings = {
    maxAutoExpandResults: 4,
  };
  expanded = false;
  preventClose = false;
  _value: any;

  static nextId = 0;
  focused = false;
  controlType = 'app-dropdown-tree-view';
  id = `app-dropdown-tree-view${DropdownTreeviewComponent.nextId++}`;
  describedBy = '';
  stateChanges: Subject<void> = new Subject<void>();
  onChange = (value) => {
  };
  onTouched = () => {
  };

  constructor(
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value ? value : null;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      // this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  changeVal(val: any) {
    this.markAsTouched();
    if (this.dTreeView && !this.isNotChild(val)) {
      this.preventClose = true;
      this.dTreeView.text = this.value ? this.value[this.textField] : '';
      return;
    }
    this.preventClose = false;
    if (!this.disabled) {
      this.value = val;
      this.onChange(this.value);
      this.valueChange.emit(this.value);
    }
  }

  onClose($event) {
    if (this.preventClose) {
      $event.preventDefault()
      this.preventClose = false;
      this.dTreeView.togglePopup(true);
      return;
    }
    this.dTreeView.toggle(false);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return control.errors;
  }

  get touched(): boolean {
    return this.ngControl && this.ngControl.touched;
  }

  get errorState(): boolean {
    return this.ngControl && this.ngControl.touched && this.ngControl.invalid;
  }

  get empty() {
    return !this.value;
  }

  get shouldLabelFloat() {
    return false;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _placeholder: string;

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _disabled = false;

  @Input()
  get value(): any | null {
    return this._value;
  }

  set value(data: any | null) {
    this._value = data ? data : null;
    this.stateChanges.next();
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {

  }

  isNotChild(dataItem: any) {
    return !dataItem.hasOwnProperty('items');
  }
}
