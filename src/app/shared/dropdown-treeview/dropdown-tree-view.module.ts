import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DropdownTreeviewComponent} from './dropdown-tree-view.component';
import {DropDownTreesModule} from '@progress/kendo-angular-dropdowns';


@NgModule({
  declarations: [
    DropdownTreeviewComponent
  ],
  imports: [
    CommonModule,
    DropDownTreesModule
  ],
  exports: [
    DropdownTreeviewComponent
  ]
})
export class DropdownTreeViewModule {
}
