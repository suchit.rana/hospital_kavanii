export interface RouterState {
  fromState: boolean,
  fromPage: FromPage,
  for: For,
  data: { [k: string]: any };
}

export enum FromPage {
  Appointment,
  Letter,
  WaitList,
  Patient,
  Scheduler,
  TreatmentRoom,
  CampaignSendEmailSms,
  CommunicationListCreate,
}

export enum For {
  Referral,
  ThirdParty,
  Letter,
  TreatmentNotes,
  AddPatient,
  WaitList,
  Scheduler,
  TreatmentRoom,
  CreateList
}

export interface TextValue {
  text: any;
  value: any;
}
