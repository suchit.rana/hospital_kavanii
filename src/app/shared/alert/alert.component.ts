import {AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MessageType} from 'src/app/models/app.misc';
import {APP_ALERT_DATA} from '../Constant';

export interface AppAlertData {
  message: string;
  type: MessageType;
  closable: boolean;
  isAbsolute: boolean;
  extraClass: string;
  autoClose: boolean;
}

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, AfterViewInit {
  @Input() message: string;
  @Input() type: MessageType;
  /* To show/hide close button */
  @Input() closable: boolean = true;
  /* To make it static/absolute */
  @Input() isAbsolute: boolean = true;
  /* To apply extra css class to message  */
  @Input() extraClass: string = '';
  /* To Check auto close  */
  @Input()
  autoClose: boolean = true;
  @Output()
  detach: EventEmitter<boolean> = new EventEmitter<boolean>();

  isMessage: boolean = true;
  messageType = MessageType;

  constructor(@Inject(APP_ALERT_DATA) public data: AppAlertData) {
    if (data) {
      this.message = data.message;
      this.type = data.type;
      this.closable = data.closable;
      this.isAbsolute = data.isAbsolute;
      this.extraClass = data.extraClass;
      this.autoClose = data.autoClose;
    }
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    if (this.autoClose) {
      setTimeout(() => {
        this.detach.emit(true);
      }, 5000);
    }
  }

  close() {
    this.isMessage = false;
    this.message = '';
    this.detach.emit(true);
  }

  get msg() {
    return this.message && this.message != '' && this.message != "" ? this.message.trim() : '';
  }
}
