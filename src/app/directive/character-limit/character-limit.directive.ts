import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';

@Directive({
  selector: '[appCharLimit]'
})
export class CharacterLimitDirective implements OnInit {

  @Input()
  max: number;
  @Input()
  frmGroup: FormGroup | AbstractControl;
  @Input()
  fcName: string;
  @Input()
  fc: FormControl;

  constructor() {
  }

  ngOnInit(): void {

  }

  @HostListener('input', ['$event'])
  onPaste($event) {
    const pastedText = $event.target.value;

    if (pastedText.length > this.max) {
      if (this.frmGroup && this.fcName) {
        this.frmGroup.get(this.fcName).setValue(pastedText.substring(0, this.max));
        this.frmGroup.get(this.fcName).setErrors({exceedLimit: `You can't type more than ${this.max} characters.`});
        this.frmGroup.get(this.fcName).markAsTouched();
      }
      if (this.fc) {
        this.fc.setValue(pastedText.substring(0, this.max));
        this.fc.setErrors({exceedLimit: `You can't type more than ${this.max} characters.`});
        this.fc.markAsTouched();
      }
    }
  }

  @HostListener('keydown', ['$event'])
  onPress(e) {
    // 110, 190 For . OR ,
    if ([46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    const length = e.target.value ? e.target.value.length : 0;
    if (length >= this.max) {
      if (this.frmGroup && this.fcName) {
        this.frmGroup.get(this.fcName).setErrors({exceedLimit: `You can't type more than ${this.max} characters.`});
        this.frmGroup.get(this.fcName).markAsTouched();
      }
      if (this.fc) {
        this.fc.setErrors({exceedLimit: `You can't type more than ${this.max} characters.`});
        this.fc.markAsTouched();
      }
      e.preventDefault();
    }
  }

  @HostListener('focusout', ['$event'])
  onFocusOut(e) {
    const length = e.target.value ? e.target.value.length : 0;
    if (length >= this.max) {
      if (this.frmGroup && this.fcName) {
        this.frmGroup.get(this.fcName).setErrors(null);
        this.frmGroup.get(this.fcName).markAsTouched();
      }
      if (this.fc) {
        this.fc.setErrors(null);
        this.fc.markAsTouched();
      }
    }
  }
}
