import {Directive, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[appTextOnly]'
})
export class TextOnlyDirective implements OnInit {

  constructor() {
  }

  ngOnInit(): void {

  }

  @HostListener('keydown', ['$event'])
  onPress(ev) {
    if (!/^[a-zA-Z\s'-]*$/.test(ev.key)) {
      ev.preventDefault();
    }
  }
}
