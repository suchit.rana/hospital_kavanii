import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TextOnlyDirective} from './text-only.directive';


@NgModule({
  declarations: [
    TextOnlyDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TextOnlyDirective
  ]
})
export class TextOnlyModule {
}
