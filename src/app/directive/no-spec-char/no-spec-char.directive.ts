import {Directive, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[appNoSpecChar]'
})
export class NoSpecCharDirective implements OnInit {

  constructor() {
  }

  ngOnInit(): void {

  }

  @HostListener('keydown', ['$event'])
  onPress(ev) {
    if (!/^[a-zA-Z0-9]*$/.test(ev.key)) {
      ev.preventDefault();
    }
  }
}
