import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NoSpecCharDirective} from './no-spec-char.directive';


@NgModule({
  declarations: [
    NoSpecCharDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NoSpecCharDirective
  ]
})
export class NoSpecCharModule {
}
