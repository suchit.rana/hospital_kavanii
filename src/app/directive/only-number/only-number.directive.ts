import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[appOnlyNumber]'
})
export class OnlyNumberDirective {

    @Input()
    isMinMax: boolean = false;
    @Input()
    isMin: boolean = false;
    @Input()
    isMax: boolean = false;
    @Input()
    min: number;
    @Input()
    max: number;

    private regexStr = '^[0-9]*$';

    constructor(private el: ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event) {
        let e = <KeyboardEvent> event;
        // 110, 190 For . OR ,
        if ([46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
            // Allow: Cmd+A
            (e.keyCode == 65 && e.metaKey) ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+V
            (e.keyCode == 86 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        let regEx = new RegExp(this.regexStr);
        if (regEx.test(e.key)) {
            this.checkForMinMax(event);
            return;
        } else {
            e.preventDefault();
        }
    }

    private checkForMinMax(e) {
        const value = parseInt(e.target.value + '' + e.key);
        if (this.isMinMax || (this.isMin && this.isMax)) {
            if (value < this.min || value > this.max) {
                e.preventDefault();
            }
        } else if (this.isMin) {
            if (value < this.min) {
                e.preventDefault();
            }
        } else if (this.isMax) {
            if (value > this.max) {
                e.preventDefault();
            }
        }
    }
}
