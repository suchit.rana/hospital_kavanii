import { Pipe, PipeTransform } from '@angular/core';
import {AppoinmentDateUtils} from '../layout/content-full-layout/appointment/AppoinmentDateUtils';
import {DateUtils} from '../shared/DateUtils';

@Pipe({
  name: 'dateDiff'
})
export class DateDiffPipe implements PipeTransform {

  transform(value: any, from: Date | string, _in: string = 'days', isTimeZoneBased?: boolean, format?: string): string {
    if (!value) {
      return value;
    }
    if (isTimeZoneBased) {
      value = AppoinmentDateUtils.toUtc(value);
      from = AppoinmentDateUtils.toUtc(from);
    }
    if (format) {
      return AppoinmentDateUtils.format(AppoinmentDateUtils.getDiff(value, from, _in), format);
    }
    return AppoinmentDateUtils.getDiff(value, from, _in).toString();
  }

}
