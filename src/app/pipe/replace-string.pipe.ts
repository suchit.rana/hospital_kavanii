import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceString'
})
export class ReplaceStringPipe implements PipeTransform {

  transform(value: string, ...strings: string[]): any {
    if (!strings) {
      return value;
    }
    strings.forEach((value1, index) => {
      value = value.replace('{'+index+'}', value1);
    });
    return value;
  }

}
