import { Pipe, PipeTransform } from '@angular/core';
import {AppoinmentDateUtils} from '../layout/content-full-layout/appointment/AppoinmentDateUtils';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: any, format: string, isTime?: boolean, isTimeZoneBased?: boolean): string {
    if (!value) {
      return value;
    }
    if (isTimeZoneBased) {
      return AppoinmentDateUtils.formatDateTime(value, format, isTime);
    }
    return AppoinmentDateUtils.formatDateTimeDefault(value, format, isTime);
  }

}
