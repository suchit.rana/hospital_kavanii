import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {
  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  transform(value: string): string {
    let suffix = 'th';
    let day = (new Date(value).getDate()).toString();
    if (day === '1' || day === '21' || day === '31') {
      suffix = 'st'
    } else if (day === '2' || day === '22') {
      suffix = 'nd';
    } else if (day === '3' || day === '23') {
      suffix = 'rd';
    }
    return day + suffix + ' ' + this.monthNames[new Date(value).getMonth()] + ' ' + new Date(value).getFullYear();
  }
}
