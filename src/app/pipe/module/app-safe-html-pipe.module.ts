import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TimeFormatPipe} from '../time-format.pipe';
import {SafeHtmlPipe} from '../safe-html.pipe';


@NgModule({
  declarations: [
    SafeHtmlPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SafeHtmlPipe
  ]
})
export class AppSafeHtmlPipeModule { }
